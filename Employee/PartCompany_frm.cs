﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class PartCompany_frm : Form
    {
        public PartCompany_frm()
        {
            InitializeComponent();
        }

        PartCompany_cls cls = new PartCompany_cls();
        private void Discound_frm_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingForm(this);
            btnNew_Click(null,null);
        }

      

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                txtID.Text = cls.MaxID_PartCompany();
              txtPartCompanyName.Text = "";
              txtNote.Text = "";
              btnSave.Enabled = true;
              btnUpdate.Enabled = false;
              btnDelete.Enabled = false;
              txtPartCompanyName.Focus();
                if (Application.OpenForms["Employee_frm"] != null)
                {
                    ((Employee_frm)Application.OpenForms["Employee_frm"]).LoadData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                // condetion
                if (txtPartCompanyName.Text.Trim() == "")
                {
                    txtPartCompanyName.BackColor = Color.Pink;
                    txtPartCompanyName.Focus();
                    return;
                }


                // Search where Part Company Name
                DataTable DtSearch = cls.NameSearch_PartCompany( txtPartCompanyName.Text.Trim());
                if (DtSearch.Rows.Count > 0)
                {
                    MessageBox.Show("الاسم المدخل موجود مسبقا", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtPartCompanyName.Focus();
                    return;
                }

                txtID.Text = cls.MaxID_PartCompany();
                cls.InsertPartCompany(txtID.Text, txtPartCompanyName.Text.Trim(), txtNote.Text);
                //Mass.Saved();
                btnNew_Click(null,null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                // condetion
                if (txtPartCompanyName.Text.Trim() == "")
                {
                    txtPartCompanyName.BackColor = Color.Pink;
                    txtPartCompanyName.Focus();
                    return;
                }
                cls.UpdatePartCompany(txtID.Text, txtPartCompanyName.Text, txtNote.Text);
               // Mass.Saved();
                btnNew_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (Mass.Delete()==true)
                {
                    cls.Delete_PartCompany(Convert.ToInt32(txtID.Text));
                    btnNew_Click(null, null);      
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
       

            try
            {
                PartCompanySearch_frm frm = new PartCompanySearch_frm();
                frm.ShowDialog(this);
                if (frm.loaddata == true)
                {
                   // DataTable Dt = cls.Details_PartCompany(int.Parse( frm.DGV1.CurrentRow.Cells["PartCompanyID"].Value.ToString()));
                    txtID.Text = frm.DGV1.CurrentRow.Cells["PartCompanyID"].Value.ToString();
                    txtPartCompanyName.Text = frm.DGV1.CurrentRow.Cells["PartCompanyName"].Value.ToString();
                    txtNote.Text = frm.DGV1.CurrentRow.Cells["Remarks"].Value.ToString();
                    btnSave.Enabled = false;
                    btnUpdate.Enabled =true ;
                    btnDelete.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void AllKeyPress(object sender, KeyPressEventArgs e)
        {
           DataAccessLayer.UseNamberOnly(e);
        }

        private void Discound_frm_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    if (btnDelete.Enabled == true)
                    {
                        btnDelete_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F5)
                {
                    btnGetData_Click(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion

        }

        private void txtPartCompanyName_TextChanged(object sender, EventArgs e)
        {
            txtPartCompanyName.BackColor = Color.White;
        }

      

    }
}
