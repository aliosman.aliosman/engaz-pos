﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class Employee_GOAttendance_frm : Form
    {
        public Employee_GOAttendance_frm()
        {
            InitializeComponent();
        }

        Employee_Attendance_cls cls = new Employee_Attendance_cls();
        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            lblHora.Text = dt.ToString("HH:MM:ss tt");
            D1.Value = dt;
        }

        private void EmployeeAttendance_frm_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingToolStrip(toolStrip1);
            ERB_Setting.SettingForm(this);
            ERB_Setting.SettingDGV(DGV1);


            timer1.Start();
            // FILL Combo Store item
            Employee_cls Employee_cls = new Employee_cls();
            combEmployee.DataSource = Employee_cls.Search_Employees("");
            combEmployee.DisplayMember = "EmployeeName";
            combEmployee.ValueMember = "EmployeeID";
            combEmployee.Text = "";
            txtEmployeeID.Text = "";
            DGV1.AutoGenerateColumns = false;
            DGV1.DataSource = cls.Search_Employee_Attendance(D1.Value, false);
            lblCount.Text = DGV1.Rows.Count.ToString();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            try
            {
                if (combEmployee.Text.Trim()== "" || txtEmployeeID.Text.Trim() == "")
                {
                    combEmployee.BackColor = Color.Pink;
                    combEmployee.Focus();
                    return;
                }
                DataTable dt = cls.NameSearch_Employee_Attendance(txtEmployeeID.Text,true);
                if (dt.Rows.Count >0)
                {

                    DateTime startTime = Convert.ToDateTime(dt.Rows[0]["Hower_Come"]);
                    DateTime endTime = D1.Value;
                    TimeSpan duration = DateTime.Parse(endTime.ToString()).Subtract(DateTime.Parse(startTime.ToString()));


                    cls.Update_Employee_Attendance(dt.Rows[0]["AttendanceID"].ToString(), D1.Value, duration.ToString(), false);
                    Mass.Saved();
                    DGV1.AutoGenerateColumns = false;
                    DGV1.DataSource = cls.Search_Employee_Attendance(D1.Value,false);
                    lblCount.Text = DGV1.Rows.Count.ToString();
                    combEmployee.Text = "";
                    txtEmployeeID.Text = "";
                    combEmployee.Focus();
                }
                else
                {
                    DataTable dtFalse = cls.NameSearch_Employee_Attendance(txtEmployeeID.Text, false);
                    if (dtFalse.Rows.Count>0)
                    {
                        MessageBox.Show("تم تسجيل انصراف الموظف من قبل", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }
                    else
                    {
                    MessageBox.Show("لم يتم تسجيل حضور الموظف", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }

                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }

        private void combEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                combEmployee.BackColor = Color.White;
                txtEmployeeID.Text = combEmployee.SelectedValue.ToString();

            }
            catch
            {

                txtEmployeeID.Text = "";

            }
        }

        private void combEmployee_TextChanged(object sender, EventArgs e)
        {
            try
            {
                combEmployee.BackColor = Color.White;

                if (combEmployee.Text == "")
                {
                    txtEmployeeID.Text = "";
                }
                else
                {
                    txtEmployeeID.Text = combEmployee.SelectedValue.ToString();
                }
            }
            catch
            {
                txtEmployeeID.Text = "";
            }

        }

        private void EmployeeAttendance_frm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.F2)
            {
                btnSave_Click(null,null);
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            try
            {
                if (DGV1.Rows.Count == 0)
                {
                    return;
                }
                if (Mass.Delete() == true)
                {
                    cls.Update_Employee_Attendance(DGV1.CurrentRow.Cells["AttendanceID"].Value.ToString(), D1.Value,"", true);
                    DGV1.AutoGenerateColumns = false;
                    DGV1.DataSource = cls.Search_Employee_Attendance(D1.Value, false);
                    lblCount.Text = DGV1.Rows.Count.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


           
        }
    }
}
