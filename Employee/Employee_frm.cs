﻿using System;
using System.Windows.Forms;
using System.Data;

namespace ByStro.PL
{
    public partial class Employee_frm : Form
    {
        public Employee_frm()
        {
            InitializeComponent();
        }
        Employee_cls cls = new Employee_cls();

        private void Company_Add_Form_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingForm(this);
            LoadData();
            btnNew_Click(null, null);

        }





        private void txtCompanyName_TextChanged(object sender, EventArgs e)
        {
            txtEmployeeName.BackColor = System.Drawing.Color.White;
        }

        public void LoadData()
        {
            WorkPartCompany_cls WorkPartCompany_cls = new WorkPartCompany_cls();
            string WorkPartCompanyName = combWorkPartCompany.Text;
            string WorkPartCompanyID = txtWorkPartCompanyID.Text;
            combWorkPartCompany.DataSource = WorkPartCompany_cls.Search_WorkPartCompany("");
            combWorkPartCompany.DisplayMember = "WorkPartCompanyName";
            combWorkPartCompany.ValueMember = "WorkPartCompanyID";
            combWorkPartCompany.Text = WorkPartCompanyName;
            txtWorkPartCompanyID.Text = WorkPartCompanyID;
            //================================================================================================================
            PartCompany_cls PartCompany_cls = new PartCompany_cls();
            string PartCompanyName = combPartCompany.Text;
            string PartCompanyID = txtPartCompanyID.Text;
            combPartCompany.DataSource = PartCompany_cls.Search_PartCompany("");
            combPartCompany.DisplayMember = "PartCompanyName";
            combPartCompany.ValueMember = "PartCompanyID";
            combPartCompany.Text = PartCompanyName;
            txtPartCompanyID.Text = PartCompanyID;
            //================================================================================================================
        }


        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {

                combWorkPartCompany.Text = "";
                txtWorkPartCompanyID.Text = "";
                //================================================================================================================

                combPartCompany.Text = "";
                txtPartCompanyID.Text = "";
                //================================================================================================================
                txtEmployeeName.Text = "";
                txtEducationalQualification.Text = "";
                chType.Checked = true;
                txtAddress.Text = "";
                combSocialStatus.Text = "";
                txtStartContract.Text = "";
                txtEndContract.Text = "";
                txtNationalID.Text = "";
                txtRemarks.Text = "";
                txtPhone.Text = "";
                txtPhone2.Text = "";
                txtAccountNumber.Text = "";
                txtFax.Text = "";
                txtEmail.Text = "";
                txtWebSite.Text = "";
                txtMilitaryService.Text = "";
                txtPlaceOfService.Text = "";
                txtServiceStartDate.Text = "";
                txtServiceEndDate.Text = "";
                txtWorkPartCompanyID.Text = "";
                txtPartCompanyID.Text = "";
                txtNumberMonth.Text = "";
                txtStartWork.Text = "";
                txtTimeAttendance.Text = "";
                txtTimeToLeave.Text = "";
                txtWorkingHours.Text = "";
                txtSalary.Text = "";

                txtEmployeeID.Text = cls.MaxID_Employees();
                tabControl1.SelectedIndex = 0;
                btnSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
                txtEmployeeName.Focus();


                if (Application.OpenForms["Employee_Deduction_frm"] != null)
                {
                   ((Employee_Deduction_frm) Application.OpenForms["Employee_Deduction_frm"]).LoadData();
                }

                if (Application.OpenForms["Employee_Additions_frm"] != null)
                {
                    ((Employee_Additions_frm)Application.OpenForms["Employee_Additions_frm"]).LoadData();
                }
                if (Application.OpenForms["Employee_Holiday_frm"] != null)
                {
                    ((Employee_Holiday_frm)Application.OpenForms["Employee_Holiday_frm"]).LoadData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtEmployeeName.Text.Trim() == "")
                {
                    txtEmployeeName.BackColor = System.Drawing.Color.Pink;
                    txtEmployeeName.Focus();
                    return;
                }
                else if (txtWorkPartCompanyID.Text.Trim() == "")
                {
                    tabControl1.SelectedIndex = 3;
                    combWorkPartCompany.BackColor = System.Drawing.Color.Pink;
                    combWorkPartCompany.Focus();
                    return;
                }
                else if (txtPartCompanyID.Text.Trim() == "")
                {
                    tabControl1.SelectedIndex = 3;
                    combPartCompany.BackColor = System.Drawing.Color.Pink;
                    combPartCompany.Focus();
                    return;
                }
                else if (txtNumberMonth.Text.Trim() == "")
                {
                    tabControl1.SelectedIndex = 3;
                    txtNumberMonth.BackColor = System.Drawing.Color.Pink;
                    txtNumberMonth.Focus();
                    return;
                }
                else if (txtSalary.Text.Trim() == "")
                {
                    tabControl1.SelectedIndex = 3;
                    txtSalary.BackColor = System.Drawing.Color.Pink;
                    txtSalary.Focus();
                    return;
                }
                else if (txtWorkingHours.Text.Trim() == "")
                {
                    tabControl1.SelectedIndex = 3;
                    txtWorkingHours.BackColor = System.Drawing.Color.Pink;
                    txtWorkingHours.Focus();
                    return;
                }

                // Search where Customer Name

                DataTable DtSearch = cls.NameSearch_Employees(txtEmployeeName.Text.Trim());
                if (DtSearch.Rows.Count > 0)
                {
                    MessageBox.Show("الاسم المدخل موجود مسبقا", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    txtEmployeeName.BackColor = System.Drawing.Color.Pink;
                    txtEmployeeName.Focus();
                    return;
                }



                txtEmployeeID.Text = cls.MaxID_Employees();
                cls.Insert_Employees(txtEmployeeID.Text, txtEmployeeName.Text.Trim(), txtEducationalQualification.Text, chType.Checked, txtAddress.Text, combSocialStatus.Text, txtStartContract.Text, txtEndContract.Text, txtNationalID.Text, txtRemarks.Text, txtPhone.Text, txtPhone2.Text, txtAccountNumber.Text, txtFax.Text, txtEmail.Text, txtWebSite.Text, txtMilitaryService.Text, txtPlaceOfService.Text, txtServiceStartDate.Text, txtServiceEndDate.Text, txtWorkPartCompanyID.Text, txtPartCompanyID.Text, txtNumberMonth.Text, txtStartWork.Value, txtTimeAttendance.Text, txtTimeToLeave.Text, txtWorkingHours.Text, txtSalary.Text);
                btnNew_Click(null, null);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }




        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtEmployeeName.Text.Trim() == "")
                {
                    txtEmployeeName.BackColor = System.Drawing.Color.Pink;
                    txtEmployeeName.Focus();
                    return;
                }
                else if (txtWorkPartCompanyID.Text.Trim() == "")
                {
                    tabControl1.SelectedIndex = 3;
                    combWorkPartCompany.BackColor = System.Drawing.Color.Pink;
                    combWorkPartCompany.Focus();
                    return;
                }
                else if (txtPartCompanyID.Text.Trim() == "")
                {
                    tabControl1.SelectedIndex = 3;
                    combPartCompany.BackColor = System.Drawing.Color.Pink;
                    combPartCompany.Focus();
                    return;
                }
                else if (txtNumberMonth.Text.Trim() == "")
                {
                    tabControl1.SelectedIndex = 3;
                    txtNumberMonth.BackColor = System.Drawing.Color.Pink;
                    txtNumberMonth.Focus();
                    return;
                }
                else if (txtSalary.Text.Trim() == "")
                {
                    tabControl1.SelectedIndex = 3;
                    txtSalary.BackColor = System.Drawing.Color.Pink;
                    txtSalary.Focus();
                    return;
                }
                else if (txtWorkingHours.Text.Trim() == "")
                {
                    tabControl1.SelectedIndex = 3;
                    txtWorkingHours.BackColor = System.Drawing.Color.Pink;
                    txtWorkingHours.Focus();
                    return;
                }

                cls.Update_Employees(txtEmployeeID.Text, txtEmployeeName.Text.Trim(), txtEducationalQualification.Text, chType.Checked, txtAddress.Text, combSocialStatus.Text, txtStartContract.Text, txtEndContract.Text, txtNationalID.Text, txtRemarks.Text, txtPhone.Text, txtPhone2.Text, txtAccountNumber.Text, txtFax.Text, txtEmail.Text, txtWebSite.Text, txtMilitaryService.Text, txtPlaceOfService.Text, txtServiceStartDate.Text, txtServiceEndDate.Text, txtWorkPartCompanyID.Text, txtPartCompanyID.Text, txtNumberMonth.Text, txtStartWork.Value, txtTimeAttendance.Text, txtTimeToLeave.Text, txtWorkingHours.Text, txtSalary.Text);
                btnNew_Click(null, null);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtEmployeeID.Text.Trim() == "")
                {
                    return;
                }




                if (Mass.Delete() == true)
                {
                    cls.Delete_Employees(txtEmployeeID.Text);
                    btnNew_Click(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
            try
            {
                EmployeeSearch_frm frm = new EmployeeSearch_frm();
                frm.ShowDialog(this);
                if (frm.loaddata == false)
                {
                    return;
                }

                if (frm.DGV1.Rows.Count == 0)
                    return;

                DataTable dt = cls.Details_Employees(frm.DGV1.CurrentRow.Cells["EmployeeID"].Value.ToString());
                DataRow Dr = dt.Rows[0];
                txtEmployeeID.Text = Dr["EmployeeID"].ToString();
                txtEmployeeName.Text = Dr["EmployeeName"].ToString();
                txtEducationalQualification.Text = Dr["EducationalQualification"].ToString();
                chType.Checked = Convert.ToBoolean(Dr["Type"]);
                txtAddress.Text = Dr["Adress"].ToString();
                combSocialStatus.Text = Dr["SocialStatus"].ToString();
                txtStartContract.Text = Dr["StartContract"].ToString();
                txtEndContract.Text = Dr["EndContract"].ToString();
                txtNationalID.Text = Dr["NationalID"].ToString();
                txtRemarks.Text = Dr["Remarks"].ToString();
                txtPhone.Text = Dr["Phone"].ToString();
                txtPhone2.Text = Dr["Phone2"].ToString();
                txtAccountNumber.Text = Dr["AccountNumber"].ToString();
                txtFax.Text = Dr["Fax"].ToString();
                txtEmail.Text = Dr["Email"].ToString();
                txtWebSite.Text = Dr["WebSite"].ToString();
                txtMilitaryService.Text = Dr["MilitaryService"].ToString();
                txtPlaceOfService.Text = Dr["PlaceOfService"].ToString();
                txtServiceStartDate.Text = Dr["ServiceStartDate"].ToString();
                txtServiceEndDate.Text = Dr["ServiceEndDate"].ToString();
                txtWorkPartCompanyID.Text = Dr["WorkPartCompanyID"].ToString();
                combWorkPartCompany.Text = Dr["WorkPartCompanyName"].ToString();
                txtPartCompanyID.Text = Dr["PartCompanyID"].ToString();
                combPartCompany.Text = Dr["PartCompanyName"].ToString();
                txtNumberMonth.Text = Dr["NumberMonth"].ToString();
                txtStartWork.Value = Convert.ToDateTime(Dr["StartWork"].ToString());
                txtTimeAttendance.Text = Dr["TimeAttendance"].ToString();
                txtTimeToLeave.Text = Dr["TimeToLeave"].ToString();
                txtWorkingHours.Text = Dr["WorkingHours"].ToString();
                txtSalary.Text = Dr["Salary"].ToString();



                //=========================================================================
                btnSave.Enabled = false;
                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }


        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtMaximum_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void txtPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void txtNationalID_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void Customers_frm_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    if (btnDelete.Enabled == true)
                    {
                        btnDelete_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F5)
                {
                    btnGetData_Click(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void combWorkPartCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                combWorkPartCompany.BackColor = System.Drawing.Color.White;
                txtWorkPartCompanyID.Text = combWorkPartCompany.SelectedValue.ToString();
            }
            catch
            {

                txtWorkPartCompanyID.Text = "";
            }
        }

        private void combWorkPartCompany_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (combWorkPartCompany.Text.Trim() == "")
                {
                    txtWorkPartCompanyID.Text = "";
                }
                else
                {
                    txtWorkPartCompanyID.Text = combWorkPartCompany.SelectedValue.ToString();

                }
            }
            catch
            {

                txtWorkPartCompanyID.Text = "";
            }
        }

        private void combPartCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                combPartCompany.BackColor = System.Drawing.Color.White;

                txtPartCompanyID.Text = combPartCompany.SelectedValue.ToString();
            }
            catch
            {

                txtPartCompanyID.Text = "";
            }
        }

        private void combPartCompany_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (combPartCompany.Text.Trim() == "")
                {
                    txtPartCompanyID.Text = "";
                }
                else
                {
                    txtPartCompanyID.Text = combPartCompany.SelectedValue.ToString();

                }
            }
            catch
            {

                txtPartCompanyID.Text = "";
            }
        }

        private void txtSalary_TextChanged(object sender, EventArgs e)
        {


        }

        private void txtWorkingHours_ValueChanged(object sender, EventArgs e)
        {
            txtWorkingHours.BackColor = System.Drawing.Color.White;

        }

        private void txtNumberMonth_ValueChanged(object sender, EventArgs e)
        {
            txtNumberMonth.BackColor = System.Drawing.Color.White;
        }

        private void txtSalary_ValueChanged(object sender, EventArgs e)
        {
            txtSalary.BackColor = System.Drawing.Color.White;
        }
    }
}
