﻿using System;
using System.Data;

class Employee_DeductionType_cls : DataAccessLayer
{
    //MaxID
    public String MaxID_Employee_DeductionType()
    {
        return Execute_SQL("select ISNULL (MAX(DeductionType_ID)+1,1) from Employee_DeductionType", CommandType.Text);
    }


    // Insert
    public void Insert_Employee_DeductionType(string DeductionType_ID, String DeductionType_Name, string Remarks, int UserAdd)
    {
        Execute_SQL("insert into Employee_DeductionType(DeductionType_ID ,DeductionType_Name ,Remarks ,UserAdd )Values (@DeductionType_ID ,@DeductionType_Name ,@Remarks ,@UserAdd )", CommandType.Text,
        Parameter("@DeductionType_ID", SqlDbType.Int, DeductionType_ID),
        Parameter("@DeductionType_Name", SqlDbType.NVarChar, DeductionType_Name),
        Parameter("@Remarks", SqlDbType.NText, Remarks),
        Parameter("@UserAdd", SqlDbType.Int, UserAdd));
    }

    //Update
    public void Update_Employee_DeductionType(string DeductionType_ID, String DeductionType_Name, string Remarks)
    {
        Execute_SQL("Update Employee_DeductionType Set DeductionType_ID=@DeductionType_ID ,DeductionType_Name=@DeductionType_Name ,Remarks=@Remarks  where DeductionType_ID=@DeductionType_ID", CommandType.Text,
        Parameter("@DeductionType_ID", SqlDbType.Int, DeductionType_ID),
        Parameter("@DeductionType_Name", SqlDbType.NVarChar, DeductionType_Name),
        Parameter("@Remarks", SqlDbType.NText, Remarks));
    }

    //Delete
    public void Delete_Employee_DeductionType(string DeductionType_ID)
    {
        Execute_SQL("Delete  From Employee_DeductionType where DeductionType_ID=@DeductionType_ID", CommandType.Text,
        Parameter("@DeductionType_ID", SqlDbType.Int, DeductionType_ID));
    }

    //Details ID
    public DataTable NODelete_Employee_DeductionType(String DeductionType_ID)
    {
        return ExecteRader("Select DeductionType_ID  from Employee_Deduction Where DeductionType_ID=@DeductionType_ID", CommandType.Text,
        Parameter("@DeductionType_ID", SqlDbType.Int, DeductionType_ID));
    }


    //Details ID
    public DataTable Details_Employee_DeductionType(string DeductionType_ID)
    {
        return ExecteRader("Select *  from Employee_DeductionType Where DeductionType_ID=@DeductionType_ID", CommandType.Text,
        Parameter("@DeductionType_ID", SqlDbType.Int, DeductionType_ID));
    }

    //Details ID
    public DataTable NameSearch__Employee_DeductionType(String DeductionType_Name)
    {
        return ExecteRader("Select *  from Employee_DeductionType Where DeductionType_Name=@DeductionType_Name", CommandType.Text,
        Parameter("@DeductionType_Name", SqlDbType.NVarChar, DeductionType_Name));
    }

    //Search 
    public DataTable Search__Employee_DeductionType(string Search)
    {
        return ExecteRader("Select *  from Employee_DeductionType Where convert(nvarchar,DeductionType_ID)+DeductionType_Name like '%'+@Search+ '%' ", CommandType.Text,
        Parameter("@Search", SqlDbType.NVarChar, Search));
    }


}