﻿using System;
using System.Data;

class Employee_Salary_cls : DataAccessLayer
    {
    //Details ID
    public DataTable Search_Salary_Employee_Additions(string EmployeeID, DateTime MyDate)
    {
        return ExecteRader("select ISNULL(SUM(Addition_Value),0) from Employee_Additions  Where EmployeeID=@EmployeeID and MyDate <=@MyDate", CommandType.Text,
        Parameter("@EmployeeID", SqlDbType.NVarChar, EmployeeID),
        Parameter("@MyDate", SqlDbType.Date, MyDate));
    }

    public DataTable Print_Employee_Additions(string EmployeeID, DateTime MyDate)
    {
        return ExecteRader(@"SELECT        dbo.Employee_AdditionsType.AdditionsType_Name, dbo.Employee_Additions.Addition_Value, dbo.Employee_Additions.Remarks
FROM            dbo.Employee_AdditionsType INNER JOIN
                         dbo.Employee_Additions ON dbo.Employee_AdditionsType.AdditionsType_ID = dbo.Employee_Additions.AdditionsType_ID
                           Where  dbo.Employee_Additions.EmployeeID = @EmployeeID and  dbo.Employee_Additions.MyDate <= @MyDate", CommandType.Text,
        Parameter("@EmployeeID", SqlDbType.NVarChar, EmployeeID),
        Parameter("@MyDate", SqlDbType.Date, MyDate));
    }


    //=================================================================================================

    //Details ID
    public DataTable Search_Salary_Employee_Deduction(string EmployeeID, DateTime MyDate)
    {
        return ExecteRader("  select ISNULL(SUM(Deduction_Value),0) from Employee_Deduction  Where EmployeeID=@EmployeeID and MyDate <=@MyDate ", CommandType.Text,
        Parameter("@EmployeeID", SqlDbType.NVarChar, EmployeeID),
        Parameter("@MyDate", SqlDbType.Date, MyDate));
    }




    //Details ID
    public DataTable Print_Salary_Employee_Deduction(string EmployeeID, DateTime MyDate)
    {
        return ExecteRader(@"SELECT        dbo.Employee_DeductionType.DeductionType_Name, dbo.Employee_Deduction.Deduction_Value, dbo.Employee_Deduction.Remarks
FROM            dbo.Employee_Deduction INNER JOIN
                         dbo.Employee_DeductionType ON dbo.Employee_Deduction.DeductionType_ID = dbo.Employee_DeductionType.DeductionType_ID
            Where dbo.Employee_Deduction.EmployeeID=@EmployeeID and dbo.Employee_Deduction.MyDate <=@MyDate", CommandType.Text,
        Parameter("@EmployeeID", SqlDbType.NVarChar, EmployeeID),
        Parameter("@MyDate", SqlDbType.Date, MyDate));
    }


    



    //Details ID
    public DataTable Print_Salary_Additions_Deduction()
    {
        return ExecteRader(@"select * from Additions_Deduction", CommandType.Text);
    }


    public DataTable Search_NumberHoliday(string EmployeeID, DateTime MyDate)
    {
        return ExecteRader("select ISNULL(SUM(DayNumber),0) as DayNumber  from Employee_Holiday  Where EmployeeID=@EmployeeID and  month(MyDate)='" + MyDate.Month.ToString()+ "' and year(MyDate)='" + MyDate.Year.ToString() + "'", CommandType.Text,
        Parameter("@EmployeeID", SqlDbType.NVarChar, EmployeeID));
    }


    //**********************************************************************************************************************************************************************************

    //MaxID
    public String MaxID_Employee_Salary()
    {
        return Execute_SQL("select ISNULL (MAX(SalaryID)+1,1) from Employee_Salary", CommandType.Text);
    }


    // Insert
    public void Insert_Employee_Salary(string SalaryID, DateTime MyDate, string EmployeeID, string MainSalary, string Additions, string Deduction, string NetSalary,String TreasuryID, int UserAdd)
    {
        Execute_SQL("insert into Employee_Salary(SalaryID ,MyDate ,EmployeeID ,MainSalary ,Additions ,Deduction ,NetSalary,TreasuryID ,UserAdd )Values (@SalaryID ,@MyDate ,@EmployeeID ,@MainSalary ,@Additions ,@Deduction ,@NetSalary,@TreasuryID ,@UserAdd )", CommandType.Text,
        Parameter("@SalaryID", SqlDbType.Int, SalaryID),
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@EmployeeID", SqlDbType.Int, EmployeeID),
        Parameter("@MainSalary", SqlDbType.Float, MainSalary),
        Parameter("@Additions", SqlDbType.Float, Additions),
        Parameter("@Deduction", SqlDbType.Float, Deduction),
        Parameter("@NetSalary", SqlDbType.Float, NetSalary),
          Parameter("@TreasuryID", SqlDbType.NVarChar, TreasuryID),
        Parameter("@UserAdd", SqlDbType.Int, UserAdd));
    }

    //Update
    public void Update_Employee_Salary(string SalaryID, DateTime MyDate, string EmployeeID, string MainSalary, string Additions, string Deduction, string NetSalary)
    {
        Execute_SQL("Update Employee_Salary Set SalaryID=@SalaryID ,MyDate=@MyDate ,EmployeeID=@EmployeeID ,MainSalary=@MainSalary ,Additions=@Additions ,Deduction=@Deduction ,NetSalary=@NetSalary  where SalaryID=@SalaryID", CommandType.Text,
        Parameter("@SalaryID", SqlDbType.Int, SalaryID),
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@EmployeeID", SqlDbType.Int, EmployeeID),
        Parameter("@MainSalary", SqlDbType.Float, MainSalary),
        Parameter("@Additions", SqlDbType.Float, Additions),
        Parameter("@Deduction", SqlDbType.Float, Deduction),
        Parameter("@NetSalary", SqlDbType.Float, NetSalary));
    }

    //Delete
    public void Delete_Employee_Salary(string SalaryID)
    {
        Execute_SQL("Delete  From Employee_Salary where SalaryID=@SalaryID", CommandType.Text,
        Parameter("@SalaryID", SqlDbType.Int, SalaryID));
    }


    public DataTable Name_EmployeeSalary(string EmployeeID, DateTime MyDate)
    {
        return ExecteRader("select EmployeeID  from Employee_Salary  Where EmployeeID=@EmployeeID and  month(MyDate)='" + MyDate.Month.ToString() + "' and year(MyDate)='" + MyDate.Year.ToString() + "'", CommandType.Text,
        Parameter("@EmployeeID", SqlDbType.NVarChar, EmployeeID));
    }


    //Details ID
    public DataTable Details_Employee_Salary(string SalaryID)
    {
        return ExecteRader(@"SELECT        dbo.Employee_Salary.SalaryID, dbo.Employee_Salary.MyDate, dbo.Employee_Salary.EmployeeID, dbo.Employees.EmployeeName, dbo.Employee_Salary.MainSalary, dbo.Employee_Salary.Additions, 
                         dbo.Employee_Salary.Deduction, dbo.Employee_Salary.NetSalary, dbo.Employee_Salary.UserAdd, dbo.Employee_Salary.TreasuryID
FROM            dbo.Employee_Salary INNER JOIN
                         dbo.Employees ON dbo.Employee_Salary.EmployeeID = dbo.Employees.EmployeeID
WHERE(dbo.Employee_Salary.SalaryID =@SalaryID)", CommandType.Text,
        Parameter("@SalaryID", SqlDbType.Int, SalaryID));
    }




    //Details ID
    public DataTable Search_Employee_Salary(DateTime MyDate)
    {
        return ExecteRader(@"SELECT        dbo.Employee_Salary.SalaryID, dbo.Employee_Salary.MyDate, dbo.Employee_Salary.EmployeeID, dbo.Employees.EmployeeName, dbo.Employee_Salary.MainSalary, dbo.Employee_Salary.Additions, 
                         dbo.Employee_Salary.Deduction, dbo.Employee_Salary.NetSalary, dbo.Employee_Salary.UserAdd, dbo.UserPermissions.EmpName
FROM            dbo.Employee_Salary INNER JOIN
                         dbo.Employees ON dbo.Employee_Salary.EmployeeID = dbo.Employees.EmployeeID INNER JOIN
                         dbo.UserPermissions ON dbo.Employee_Salary.UserAdd = dbo.UserPermissions.ID
WHERE   month(dbo.Employee_Salary.MyDate)='" + MyDate.Month.ToString() + "' and year(dbo.Employee_Salary.MyDate)='" + MyDate.Year.ToString() + "'", CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate));
    }

    //Details ID
    public DataTable Search_Employee_Salary(DateTime MyDate,string EmployeeID)
    {
        return ExecteRader(@"SELECT        dbo.Employee_Salary.SalaryID, dbo.Employee_Salary.MyDate, dbo.Employee_Salary.EmployeeID, dbo.Employees.EmployeeName, dbo.Employee_Salary.MainSalary, dbo.Employee_Salary.Additions, 
                         dbo.Employee_Salary.Deduction, dbo.Employee_Salary.NetSalary, dbo.Employee_Salary.UserAdd, dbo.UserPermissions.EmpName
FROM            dbo.Employee_Salary INNER JOIN
                         dbo.Employees ON dbo.Employee_Salary.EmployeeID = dbo.Employees.EmployeeID INNER JOIN
                         dbo.UserPermissions ON dbo.Employee_Salary.UserAdd = dbo.UserPermissions.ID
WHERE   month(dbo.Employee_Salary.MyDate)='" + MyDate.Month.ToString() + "' and year(dbo.Employee_Salary.MyDate)='" + MyDate.Year.ToString() + "' and dbo.Employee_Salary.EmployeeID=@EmployeeID", CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
         Parameter("@EmployeeID", SqlDbType.NVarChar, EmployeeID));
    }


    #region Report
    //Details ID
    public DataTable Search_Employee_Salary(DateTime MyDate, DateTime MyDate2)
    {
        return ExecteRader(@"SELECT        dbo.Employee_Salary.SalaryID, dbo.Employee_Salary.MyDate, dbo.Employee_Salary.EmployeeID, dbo.Employees.EmployeeName, dbo.Employee_Salary.MainSalary, dbo.Employee_Salary.Additions, 
                         dbo.Employee_Salary.Deduction, dbo.Employee_Salary.NetSalary, dbo.Employee_Salary.UserAdd, dbo.UserPermissions.EmpName
FROM            dbo.Employee_Salary INNER JOIN
                         dbo.Employees ON dbo.Employee_Salary.EmployeeID = dbo.Employees.EmployeeID INNER JOIN
                         dbo.UserPermissions ON dbo.Employee_Salary.UserAdd = dbo.UserPermissions.ID
WHERE   month(dbo.Employee_Salary.MyDate)>='" + MyDate.Month.ToString() + "' and year(dbo.Employee_Salary.MyDate)>='" + MyDate.Year.ToString() + "' and  month(dbo.Employee_Salary.MyDate) <='" + MyDate2.Month.ToString() + "' and year(dbo.Employee_Salary.MyDate)<='" + MyDate2.Year.ToString() + "'", CommandType.Text);
    }

    //Details ID
    public DataTable Search_Employee_Salary(DateTime MyDate, DateTime MyDate2, string EmployeeID)
    {
        return ExecteRader(@"SELECT        dbo.Employee_Salary.SalaryID, dbo.Employee_Salary.MyDate, dbo.Employee_Salary.EmployeeID, dbo.Employees.EmployeeName, dbo.Employee_Salary.MainSalary, dbo.Employee_Salary.Additions, 
                         dbo.Employee_Salary.Deduction, dbo.Employee_Salary.NetSalary, dbo.Employee_Salary.UserAdd, dbo.UserPermissions.EmpName
FROM            dbo.Employee_Salary INNER JOIN
                         dbo.Employees ON dbo.Employee_Salary.EmployeeID = dbo.Employees.EmployeeID INNER JOIN
                         dbo.UserPermissions ON dbo.Employee_Salary.UserAdd = dbo.UserPermissions.ID
WHERE   month(dbo.Employee_Salary.MyDate)>='" + MyDate.Month.ToString() + "' and year(dbo.Employee_Salary.MyDate)>='" + MyDate.Year.ToString() + "' and month(dbo.Employee_Salary.MyDate)<='" + MyDate2.Month.ToString() + "' and year(dbo.Employee_Salary.MyDate)<='" + MyDate2.Year.ToString() + "' and dbo.Employee_Salary.EmployeeID=@EmployeeID", CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
         Parameter("@EmployeeID", SqlDbType.NVarChar, EmployeeID));
    }


    #endregion


}

