﻿using System;
using System.Data;

class Employee_Additions_cls : DataAccessLayer
{
    //MaxID
    public String MaxID_Employee_Additions()
    {
        return Execute_SQL("select ISNULL (MAX(Additions_ID)+1,1) from Employee_Additions", CommandType.Text);
    }


    // Insert
    public void Insert_Employee_Additions(String Additions_ID,DateTime MyDate, String EmployeeID, String AdditionsType_ID, String Addition_Value, String Remarks, int UserAdd)
    {
        Execute_SQL("insert into Employee_Additions(Additions_ID ,MyDate,EmployeeID ,AdditionsType_ID ,Addition_Value  ,Remarks ,UserAdd )Values (@Additions_ID,@MyDate ,@EmployeeID ,@AdditionsType_ID ,@Addition_Value ,@Remarks ,@UserAdd )", CommandType.Text,
        Parameter("@Additions_ID", SqlDbType.NVarChar, Additions_ID),
    Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@EmployeeID", SqlDbType.NVarChar, EmployeeID),
        Parameter("@AdditionsType_ID", SqlDbType.NVarChar, AdditionsType_ID),
        Parameter("@Addition_Value", SqlDbType.NVarChar, Addition_Value),
        Parameter("@Remarks", SqlDbType.NVarChar, Remarks),
        Parameter("@UserAdd", SqlDbType.Int, UserAdd));
    }




    //Update
    public void Update_Employee_Additions(String Additions_ID,DateTime MyDate, String EmployeeID, String AdditionsType_ID, String Addition_Value, String Remarks)
    {
        Execute_SQL("Update Employee_Additions Set Additions_ID=@Additions_ID,MyDate=@MyDate ,EmployeeID=@EmployeeID ,AdditionsType_ID=@AdditionsType_ID ,Addition_Value=@Addition_Value ,Remarks=@Remarks  where Additions_ID=@Additions_ID", CommandType.Text,
        Parameter("@Additions_ID", SqlDbType.NVarChar, Additions_ID),
            Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@EmployeeID", SqlDbType.NVarChar, EmployeeID),
        Parameter("@AdditionsType_ID", SqlDbType.NVarChar, AdditionsType_ID),
        Parameter("@Addition_Value", SqlDbType.NVarChar, Addition_Value),
        Parameter("@Remarks", SqlDbType.NVarChar, Remarks));
    }

    //Delete
    public void Delete_Employee_Additions(String Additions_ID)
    {
        Execute_SQL("Delete  From Employee_Additions where Additions_ID=@Additions_ID", CommandType.Text,
        Parameter("@Additions_ID", SqlDbType.NVarChar, Additions_ID));
    }

    //Details ID
    public DataTable Details_Employee_Additions(String Additions_ID)
    {
        return ExecteRader(@"SELECT        dbo.Employee_Additions.Additions_ID, dbo.Employee_Additions.EmployeeID, dbo.Employees.EmployeeName, dbo.Employee_Additions.AdditionsType_ID, dbo.Employee_AdditionsType.AdditionsType_Name, 
                         dbo.Employee_Additions.Addition_Value, dbo.Employee_Additions.Remarks, dbo.Employee_Additions.UserAdd, dbo.Employee_Additions.MyDate
FROM            dbo.Employee_AdditionsType INNER JOIN
                         dbo.Employee_Additions ON dbo.Employee_AdditionsType.AdditionsType_ID = dbo.Employee_Additions.AdditionsType_ID INNER JOIN
                         dbo.Employees ON dbo.Employee_Additions.EmployeeID = dbo.Employees.EmployeeID
WHERE        (dbo.Employee_Additions.Additions_ID = @Additions_ID)", CommandType.Text,
        Parameter("@Additions_ID", SqlDbType.NVarChar, Additions_ID));
    }

    //Search 
    public DataTable Search__Employee_Additions(string EmployeeID, DateTime MyDate, DateTime MyDate2)
    {
        return ExecteRader(@"SELECT        dbo.Employee_Additions.Additions_ID, dbo.Employee_Additions.EmployeeID, dbo.Employees.EmployeeName, dbo.Employee_Additions.AdditionsType_ID, dbo.Employee_AdditionsType.AdditionsType_Name, 
                         dbo.Employee_Additions.Addition_Value, dbo.Employee_Additions.Remarks, dbo.Employee_Additions.UserAdd, dbo.UserPermissions.EmpName
FROM            dbo.Employee_AdditionsType INNER JOIN
                         dbo.Employee_Additions ON dbo.Employee_AdditionsType.AdditionsType_ID = dbo.Employee_Additions.AdditionsType_ID INNER JOIN
                         dbo.Employees ON dbo.Employee_Additions.EmployeeID = dbo.Employees.EmployeeID INNER JOIN
                         dbo.UserPermissions ON dbo.Employee_Additions.UserAdd = dbo.UserPermissions.ID
Where   (dbo.Employee_Additions.EmployeeID = @EmployeeID) and dbo.Employee_Additions.MyDate>= @MyDate And dbo.Employee_Additions.MyDate<= @MyDate2  ", CommandType.Text,
        Parameter("@EmployeeID", SqlDbType.NVarChar, EmployeeID),
                Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2));
    }

    //Search 
    public DataTable Search__Employee_Additions(DateTime MyDate, DateTime MyDate2)
    {
        return ExecteRader(@"SELECT        dbo.Employee_Additions.MyDate, dbo.Employee_Additions.Additions_ID, dbo.Employee_Additions.EmployeeID, dbo.Employees.EmployeeName, dbo.Employee_Additions.AdditionsType_ID, 
                         dbo.Employee_AdditionsType.AdditionsType_Name, dbo.Employee_Additions.Addition_Value, dbo.Employee_Additions.Remarks, dbo.Employee_Additions.UserAdd, 
                         dbo.UserPermissions.EmpName
FROM            dbo.Employee_AdditionsType INNER JOIN
                         dbo.Employee_Additions ON dbo.Employee_AdditionsType.AdditionsType_ID = dbo.Employee_Additions.AdditionsType_ID INNER JOIN
                         dbo.Employees ON dbo.Employee_Additions.EmployeeID = dbo.Employees.EmployeeID INNER JOIN
                         dbo.UserPermissions ON dbo.Employee_Additions.UserAdd = dbo.UserPermissions.ID
Where dbo.Employee_Additions.MyDate>= @MyDate And dbo.Employee_Additions.MyDate<= @MyDate2  ", CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2));

                           
    }




}

