﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class Employee_Holiday_frm : Form
    {
        public Employee_Holiday_frm()
        {
            InitializeComponent();
        }
        Employee_Holiday_cls cls = new Employee_Holiday_cls();


        private void Discound_frm_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingForm(this);
            btnNew_Click(null, null);
        }


        public void LoadData()
        {
            btnNew_Click(null, null);
        }



        DataTable dt_Employee = new DataTable();

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                Employee_cls Employee_cls = new Employee_cls();
                dt_Employee= Employee_cls.Search_Employees("");
                combEmployee.DataSource = dt_Employee;
                combEmployee.DisplayMember = "EmployeeName";
                combEmployee.ValueMember = "EmployeeID";
                combEmployee.Text = "";
                txtEmployeeID.Text = "";
                //======================================================================================
                HolidayType_cls HolidayType_cls = new HolidayType_cls();
                combAdditionsType.DataSource = HolidayType_cls.Search_EmpHoliday("");
                combAdditionsType.DisplayMember = "HolidayName";
                combAdditionsType.ValueMember = "HolidayID";
                combAdditionsType.Text = "";
                txtEmployeeID.Text = "";
                //======================================================================================
                txtAdditions_ID.Text = cls.MaxID_Employee_Holiday();
                txtEmployeeID.Text = "";
                txtAddition_Value.Text = "";
                txtAdditionsType_ID.Text = "";
                txtRemarks.Text = "";
                lblMassege.Text = "";
                radioButton1.Checked = true;
                RbSelectAll.Enabled = true;
                btnSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
                combEmployee.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
  

                // condetion
               if (txtAdditionsType_ID.Text.Trim() == "")
                {
                    combAdditionsType.BackColor = Color.Pink;
                    combAdditionsType.Focus();
                    return;
                }
                // condetion
                else if (txtAddition_Value.Text.Trim() == "")
                {
                    txtAddition_Value.BackColor = Color.Pink;
                    txtAddition_Value.Focus();
                    return;
                }
                else if (Convert.ToDouble(txtAddition_Value.Text.Trim()) == 0)
                {
                    txtAddition_Value.BackColor = Color.Pink;
                    txtAddition_Value.Focus();
                    return;
                }
                if (RbSelectAll.Checked==true)
                {
                    for (int i = 0; i < dt_Employee.Rows.Count; i++)
                    {
                        txtAdditions_ID.Text = cls.MaxID_Employee_Holiday();
                        cls.Insert_Employee_Holiday(txtAdditions_ID.Text, D_MyDate.Value,dt_Employee.Rows[i]["EmployeeID"].ToString(), txtAdditionsType_ID.Text, txtAddition_Value.Text, txtRemarks.Text, Properties.Settings.Default.UserID);
                        lblMassege.Text = " : تم حفظ الاجازة للموظف" + dt_Employee.Rows[i]["EmployeeName"].ToString();
                    }
                    Mass.Saved();
                }
                else
                {
                    // condetion
                    // condetion
                    if (txtEmployeeID.Text.Trim() == "")
                    {
                        combEmployee.BackColor = Color.Pink;
                        combEmployee.Focus();
                        return;
                    }
                    txtAdditions_ID.Text = cls.MaxID_Employee_Holiday();
                    cls.Insert_Employee_Holiday(txtAdditions_ID.Text, D_MyDate.Value, txtEmployeeID.Text, txtAdditionsType_ID.Text, txtAddition_Value.Text, txtRemarks.Text, Properties.Settings.Default.UserID);
                }

                btnNew_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                // condetion
                if (txtEmployeeID.Text.Trim() == "")
                {
                    combEmployee.BackColor = Color.Pink;
                    combEmployee.Focus();
                    return;
                }

                // condetion
                else if (txtAdditionsType_ID.Text.Trim() == "")
                {
                    combAdditionsType.BackColor = Color.Pink;
                    combAdditionsType.Focus();
                    return;
                }
                // condetion
                else if (txtAddition_Value.Text.Trim() == "")
                {
                    txtAddition_Value.BackColor = Color.Pink;
                    txtAddition_Value.Focus();
                    return;
                }
                else if (Convert.ToDouble(txtAddition_Value.Text.Trim()) == 0)
                {
                    txtAddition_Value.BackColor = Color.Pink;
                    txtAddition_Value.Focus();
                    return;
                }


                cls.Update_Employee_Holiday(txtAdditions_ID.Text, D_MyDate.Value, txtEmployeeID.Text, txtAdditionsType_ID.Text, txtAddition_Value.Text, txtRemarks.Text);
                btnNew_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (Mass.Delete() == true)
                {
                    cls.Delete_Employee_Holiday(txtAdditions_ID.Text);
                    btnNew_Click(null, null);
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {


            try
            {
                Employee_Holiday_Search_frm frm = new Employee_Holiday_Search_frm();
                frm.ShowDialog(this);
                if (frm.loaddata == true)
                {
                    DataTable Dt = cls.Details_Employee_Holiday(frm.DGV1.CurrentRow.Cells["ID"].Value.ToString());
                    txtAdditions_ID.Text = Dt.Rows[0]["ID"].ToString();
                    txtEmployeeID.Text = Dt.Rows[0]["EmployeeID"].ToString();
                    combEmployee.Text = Dt.Rows[0]["EmployeeName"].ToString();
                    txtAddition_Value.Text = Dt.Rows[0]["DayNumber"].ToString();
                    txtAdditionsType_ID.Text = Dt.Rows[0]["HolidayID"].ToString();
                    combAdditionsType.Text = Dt.Rows[0]["HolidayName"].ToString();
                    txtRemarks.Text = Dt.Rows[0]["Remarks"].ToString();
                    radioButton1.Checked = true;
                    RbSelectAll.Enabled = false;
                    btnSave.Enabled = false;
                    btnUpdate.Enabled = true;
                    btnDelete.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                txtEmployeeID.Text = "";
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void AllKeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void Discound_frm_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    if (btnDelete.Enabled == true)
                    {
                        btnDelete_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F5)
                {
                    btnGetData_Click(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion

        }

        private void txtValue_TextChanged(object sender, EventArgs e)
        {
            txtAddition_Value.BackColor = Color.White;
        }


        private void combEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                combEmployee.BackColor = Color.White;
                txtEmployeeID.Text = combEmployee.SelectedValue.ToString();
            }
            catch
            {
                txtEmployeeID.Text = "";
                return;
            }
        }

        private void combEmployee_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (combEmployee.Text.Trim() == "")
                {
                    txtEmployeeID.Text = "";

                }
                else
                {
                    combEmployee.BackColor = Color.White;
                    txtEmployeeID.Text = combEmployee.SelectedValue.ToString();
                }

            }
            catch
            {
                txtEmployeeID.Text = "";
                return;
            }
        }

        private void combAdditionsType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                combAdditionsType.BackColor = Color.White;
                txtAdditionsType_ID.Text = combAdditionsType.SelectedValue.ToString();
            }
            catch
            {
                txtAdditionsType_ID.Text = "";
                return;
            }
        }

        private void combAdditionsType_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (combAdditionsType.Text.Trim() == "")
                {
                    txtAdditionsType_ID.Text = "";

                }
                else
                {
                    combAdditionsType.BackColor = Color.White;
                    txtAdditionsType_ID.Text = combAdditionsType.SelectedValue.ToString();
                }

            }
            catch
            {
                txtAdditionsType_ID.Text = "";
                return;
            }
        }

        private void DGV1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void RbSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            if (RbSelectAll.Checked==true)
            {
                combEmployee.Text = "";
                combEmployee.Enabled = false;
            }
            else
            {
                combEmployee.Enabled = true;
                combEmployee.Focus();
            }
        }
    }
}
