﻿using System; 
using System.Data; 
using System.Linq; 
using ByStro.DAL;
using DevExpress.XtraCharts;
using ByStro.Clases;

namespace ByStro.Forms
{
    public partial class frm_CustomerProfit : DevExpress.XtraEditors.XtraForm
    {
        public frm_CustomerProfit()
        {
            InitializeComponent();
        }
        private void dateEdit2_EditValueChanged(object sender, EventArgs e)
        {
            if (dateEdit1.DateTime.Year < 1950 || dateEdit2.DateTime.Year < 1950 || dateEdit2.DateTime < dateEdit1.DateTime)
            {
                // XtraMessageBox.Show("يرجي اختيار التاريخ بشكل صحيح");
                return;
            }
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);

            var Sales = 
               db.Inv_Invoices.Where(s => s.Date.Date >= dateEdit1.DateTime.Date && s.Date.Date <= dateEdit2.DateTime.Date && 
                ( s.InvoiceType == (int)MasterClass.InvoiceType.SalesInvoice)) ;

            var SalesReturn= 
              db.Inv_Invoices.Where(s => s.Date.Date >= dateEdit1.DateTime.Date && s.Date.Date <= dateEdit2.DateTime.Date &&
                s.InvoiceType == (int)MasterClass.InvoiceType.SalesReturn) ;

            var NetSales = Sales.Select(s => new
            {
                s.PartID,
                ProfitIn = s.Net - db.Inv_InvoiceDetails.Where(sd => sd.InvoiceID == s.ID).Sum(x => x.TotalCostValue),
                ProfitOut = (double)0,
            }).ToList();
            NetSales.AddRange(SalesReturn.Select(s => new
            {
                s.PartID,
                ProfitIn = (double)0,
                ProfitOut = s.Net - db.Inv_InvoiceDetails.Where(sd => sd.InvoiceID == s.ID).Sum(x => x.TotalCostValue),
            }).ToList()); 

            var data = NetSales.GroupBy(d => d.PartID ).Select(d => new
            {
                Customer  = db.Customers .Single(p => p.CustomerID  == d.Key).CustomerName ,
                Sum = d.Sum(x => (double?)x.ProfitIn) ?? 0 - d.Sum(X => (double?)X.ProfitOut) ?? 0
            }).OrderByDescending(x => x.Sum).ToList(); 
            Series MostValueCustomer = new Series("", ViewType.Bar); 
            MostValueCustomer.DataSource = data; 
            gridControl1.DataSource = data; 
            MostValueCustomer.ArgumentDataMember = "Customer";
            MostValueCustomer.ValueScaleType = ScaleType.Numerical;
            MostValueCustomer.ValueDataMembers.AddRange(new string[] { "Sum" });
            MostValueCustomer.Label.TextPattern = "{A} = {V:N2} \n ({VP:P0})";
            MostValueCustomer.LegendTextPattern = "{A}";
            MostValueCustomer.TopNOptions.ShowOthers = true;
            MostValueCustomer.TopNOptions.Enabled = true;
            MostValueCustomer.TopNOptions.Count = 5;
            MostValueCustomer.Colorizer = new RangeColorizer(); 
            MostValueCustomer.ShowInLegend = true; 
            chartControl1.AnimationStartMode = ChartAnimationMode.OnDataChanged;
            chartControl1.Series.Clear();
            chartControl1.Series.Add(MostValueCustomer);
        }

      
        private void frm_CustomerProfit_Load(object sender, EventArgs e)
        {
            this.dateEdit1.EditValueChanged += new System.EventHandler(this.dateEdit2_EditValueChanged);
            this.dateEdit2.EditValueChanged += new System.EventHandler(this.dateEdit2_EditValueChanged);
            dateEdit1.DateTime = new DateTime(DateTime.Now.Year, 1, 1);
            dateEdit2.DateTime = DateTime.Now.Date;
        }
    }
}