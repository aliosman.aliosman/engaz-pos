﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ByStro.DAL;

namespace ByStro.Forms
{
    public partial class frm_ResetAdminPassWord : DevExpress.XtraEditors.XtraForm
    {
        public frm_ResetAdminPassWord()
        {
            InitializeComponent();
        }
        int Clicks = 0;
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Clicks++;
            if (Clicks > 3)
                Application.Exit();
            if (textEdit1.Text == "C@4n#89$qb")
            {
                DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
                db.UserPermissions.First().UserPassword = "admin";
                db.SubmitChanges();
                XtraMessageBox.Show("تم استعاده باسورد النظام بنجاح ");
                this.Close();
            }
            else
            {
                textEdit1.ErrorText = "!";
            }
        }
    }
}