﻿using System;
using System.Collections.Generic; 
using System.Data;
using System.Drawing;
using System.Linq; 
using System.Windows.Forms; 
using System.Text; 
using System.Threading.Tasks; 
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors.Controls;
using System.Diagnostics;
using DevExpress.XtraGrid.Views.Base;
using ByStro.Clases;
using static ByStro.Clases.MasterClass;
using ByStro.DAL;

namespace ByStro.Forms
{
   
    public partial class frm_Inv_InvoiceList : frm_Master
    {
        MasterClass.InvoiceType InvoiceType;
        bool IsForSelect;
        static int _SelectedInvoiceID;
        public static int SelectedInvoiceID
        {
            set { _SelectedInvoiceID = value; }
            get {
                int temp = _SelectedInvoiceID;
                _SelectedInvoiceID = 0;
                return temp;
                    
            }
        }
        public frm_Inv_InvoiceList(MasterClass.InvoiceType _InvoiceType,bool _IsForSelect =false )
        {
            InitializeComponent();
            InvoiceType = _InvoiceType;
            IsForSelect = _IsForSelect;
            dt_From.DateTime = DateTime.Now.Date;
        }

        public frm_Inv_InvoiceList(MasterClass.InvoiceType _InvoiceType, PartTypes partType, int PartID)
        {
            InitializeComponent();
            InvoiceType = _InvoiceType;
        }
        
        public override void frm_Load(object sender, EventArgs e)
        {

            btn_Save.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.btn_Clear.Click += new System.EventHandler(this.btn_Clear_Click);
            this.lkp_Store.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.Filter_ButtonClick);
            this.lkp_Store.EditValueChanged += new System.EventHandler(this.Filters_EditValueChanged);
            this.dt_To.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.Filter_ButtonClick);
            this.dt_To.EditValueChanged += new System.EventHandler(this.Filters_EditValueChanged);
            this.dt_From.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.Filter_ButtonClick);
            this.dt_From.EditValueChanged += new System.EventHandler(this.Filters_EditValueChanged);
            this.gridControl1.ViewRegistered += new DevExpress.XtraGrid.ViewOperationEventHandler(this.gridControl1_ViewRegistered);
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            this.printToolStripMenuItem.Click += new System.EventHandler(this.printToolStripMenuItem_Click);

           

            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.MasterRowGetRelationDisplayCaption += new DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventHandler(this.gridView1_MasterRowGetRelationDisplayCaption);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Inv_InvoiceList_FormClosing);
            this.gridControl1.ContextMenuStrip = this.contextMenuStrip1;

            //   btn_Delete.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;

            base.frm_Load(sender, e);

            gridView1.OptionsDetail.DetailMode = DetailMode.Classic;
            gridView1.OptionsDetail.ShowEmbeddedDetailIndent = DevExpress.Utils.DefaultBoolean.True;
            gridView1.OptionsDetail.ShowDetailTabs = true;
            gridView1.DetailVerticalIndent = 25;

            switch (InvoiceType)
            {
         
                case InvoiceType.SalesInvoice:
                    this.Text = "فواتير المبيعات";
                    this.Name = "frm_SalesInvoiceList";
                    gridView1.Columns["PartID"].Caption = "العميل";

                    break;
                case InvoiceType.PurchaseInvoice :
                    this.Text = "فواتير المشتريات";
                    this.Name = "frm_PurchaseInvoiceList";
                    gridView1.Columns["PartID"].Caption = "المورد"; 
                    break;
                case InvoiceType.PurchaseReturn :
                    this.Text = "فواتير مردود المشتريات";
                    this.Name = "frm_PurchaseReturnInvoice";
                    gridView1.Columns["PartID"].Caption = "المورد";

                    break;
                case InvoiceType.SalesReturn:
                    this.Text = "فواتير مردود المبيعات";
                    this.Name = "frm_SalesReturnInvoice";
                    gridView1.Columns["PartID"].Caption = "العميل";

                    break;
                case InvoiceType.ItemOpenBalance :
                    this.Text = "الارصده الافتتاحيه للاصناف ";
                    this.Name = "frm_OpenBalanceInvoiceList";
                    gridView1.Columns["PartID"].OptionsColumn.ShowInCustomizationForm =
                    gridView1.Columns["DiscountVal"].OptionsColumn.ShowInCustomizationForm =
                    gridView1.Columns["Paid"].OptionsColumn.ShowInCustomizationForm =
                    gridView1.Columns["Remains"].OptionsColumn.ShowInCustomizationForm =
                    gridView1.Columns["SourceID"].OptionsColumn.ShowInCustomizationForm =
                    gridView1.Columns["PayStatus"].OptionsColumn.ShowInCustomizationForm =
                    gridView1.Columns["DiscountRatio"].OptionsColumn.ShowInCustomizationForm = false;

                    gridView1.Columns["PartID"].Visible  =
                  gridView1.Columns["DiscountVal"].Visible =
                  gridView1.Columns["Paid"].Visible =
                  gridView1.Columns["Remains"].Visible =
                  gridView1.Columns["SourceID"].Visible =
                  gridView1.Columns["PayStatus"].Visible =
                  gridView1.Columns["DiscountRatio"].Visible = false;
                    break;
                case InvoiceType.ItemDamage:
                    this.Text = "سندات هالك اصناف ";
                    this.Name = "frm_OpenBalanceInvoiceList";
                    gridView1.Columns["PartID"].OptionsColumn.ShowInCustomizationForm =
                    gridView1.Columns["DiscountVal"].OptionsColumn.ShowInCustomizationForm =
                    gridView1.Columns["Paid"].OptionsColumn.ShowInCustomizationForm =
                    gridView1.Columns["Remains"].OptionsColumn.ShowInCustomizationForm =
                    gridView1.Columns["SourceID"].OptionsColumn.ShowInCustomizationForm =
                    gridView1.Columns["PayStatus"].OptionsColumn.ShowInCustomizationForm =
                    gridView1.Columns["DiscountRatio"].OptionsColumn.ShowInCustomizationForm = false; 
                    gridView1.Columns["PartID"].Visible =
                    gridView1.Columns["DiscountVal"].Visible =
                    gridView1.Columns["Paid"].Visible =
                    gridView1.Columns["Remains"].Visible =
                    gridView1.Columns["SourceID"].Visible =
                    gridView1.Columns["PayStatus"].Visible =
                    gridView1.Columns["DiscountRatio"].Visible = false;
                    break; 
                case InvoiceType.ItemStoreMove :
                    this.Text = "سند نقل اصناف ";
                    this.Name = "frm_ItemStoreMoveList";
                    gridView1.Columns["PartID"].OptionsColumn.ShowInCustomizationForm =
                    gridView1.Columns["DiscountVal"].OptionsColumn.ShowInCustomizationForm =
                    gridView1.Columns["Paid"].OptionsColumn.ShowInCustomizationForm =
                    gridView1.Columns["Remains"].OptionsColumn.ShowInCustomizationForm =
                    gridView1.Columns["SourceID"].OptionsColumn.ShowInCustomizationForm =
                    gridView1.Columns["PayStatus"].OptionsColumn.ShowInCustomizationForm =
                    gridView1.Columns["DiscountRatio"].OptionsColumn.ShowInCustomizationForm = false;
                    gridView1.Columns["PartID"].Visible =
                    gridView1.Columns["DiscountVal"].Visible =
                    gridView1.Columns["Paid"].Visible =
                    gridView1.Columns["Remains"].Visible =
                    gridView1.Columns["SourceID"].Visible =
                    gridView1.Columns["PayStatus"].Visible =
                    gridView1.Columns["DiscountRatio"].Visible = false;
                    break;

                default:
                    throw new NotImplementedException();

            }
            RestoreGridLayout(gridView1, this);

            repoSize.PopulateColumns();
            repoSize.Columns["ID"].Visible = false;


            repoColor.PopulateColumns();
            repoColor.Columns["ID"].Visible = false;

            ItemsRepo.ValueMember =
            UOMRepo.ValueMember =
            BranchRepo.ValueMember =
            repoSize.ValueMember =
            repoColor.ValueMember =
            repoPartID.ValueMember = 
            lkp_Store.Properties.ValueMember = "ID";

            ItemsRepo.DisplayMember =
            UOMRepo.DisplayMember =
            BranchRepo.DisplayMember =
            repoSize.DisplayMember =
            repoColor.DisplayMember =
            repoPartID.DisplayMember = 
            lkp_Store.Properties.DisplayMember = "Name";

            gridControl1.RepositoryItems.AddRange(new RepositoryItem[]
            {
                 ItemsRepo      ,
                 UOMRepo        ,
                 BranchRepo     ,
                 repoSize       ,
                 repoColor      ,
                 repoPartID,
            });

            gridView1.Columns["StoreID"].ColumnEdit = BranchRepo;
            gridView1.Columns["PartID"].ColumnEdit = repoPartID;
            gridView1.Columns["DiscountRatio"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            gridView1.Columns["DiscountRatio"].DisplayFormat.FormatString = "p";
            var insta =new  DAL.Inv_Invoice();

            /*
             
              i.ID,
                             i.Code,
                             i.PartID,
                             i.StoreID,
                             i.Date, 
                             i.DiscountRatio, 
                             i.Net,
                             i.Paid ,
                             PayStatus = "",
                             Remains = i.Net - i.Paid,
                             i.SourceID, 
                             UserID = u.UserName,
                             i.Notes, 
             */
            gridView1.Columns["Code"].Caption = "الكود";
            gridView1.Columns["StoreID"].Caption = "المخزن";
            gridView1.Columns["Date"].Caption = "التاريخ";
            gridView1.Columns["DiscountRatio"].Caption = "نسبه الخصم";
            gridView1.Columns["DiscountVal"].Caption = "نسبه الخصم";
            gridView1.Columns["Net"].Caption = "الصافي";
            gridView1.Columns["Paid"].Caption = "المدفوع";
            gridView1.Columns["PayStatus"].Caption = "حاله الدفع";
            gridView1.Columns["Remains"].Caption = "المتبقي";
            gridView1.Columns["SourceID"].Caption = "المصدر";
            gridView1.Columns["UserID"].Caption = "المستخدم";
            gridView1.Columns["Notes"].Caption = "الملاحظات";

        }
        Inv_InvoiceDetail InvoDInsta = new Inv_InvoiceDetail(); // instance of invoice detal to get the names from 
        List<DAL.Prodecut> prodecuts;
        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (gridView1.SelectedRowsCount == 0) return; 
            if (MasterClass.AskForDelete())
            {
                List<int> ids = new List<int>();
                foreach (var item in gridView1.GetSelectedRows())
                {
                    ids.Add(Convert.ToInt32(gridView1.GetRowCellValue(item, "ID"))); 
                }

                frm_Inv_Invoice.Delete(ids, this.Name, InvoiceType);
                RefreshData();
            }
        }

        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (gridView1.SelectedRowsCount == 0) return;
            ////var UserCanPrint = CurrentSession.userPrivilageList.Where(h => h.PrivilageName == "frm_Inv_Invoice" + "_Print");
            ////if (UserCanPrint.Count() > 0) CanPrint = (bool)UserCanPrint.First().PrivilegeValue;
            ////if (!CanPrint) { XtraMessageBox.Show(LangResource.CantEditNoPermission, "", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }
            List<int> ids = new List<int>();
            foreach (var item in gridView1.GetSelectedRows())
            {
                ids.Add(Convert.ToInt32(gridView1.GetRowCellValue(item, "ID")));
            }
            frm_Inv_Invoice.Print(ids, this.Name, InvoiceType);
        }
        private void GridView_Items_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == nameof(InvoDInsta.ItemUnitID))
            {
                GridView view = sender as GridView;
                int index = view.GetRowHandle(e.ListSourceRowIndex);
                DAL.Inv_InvoiceDetail detail = view.GetRow(index) as DAL.Inv_InvoiceDetail;
                if (detail == null) return;
                var db = new DBDataContext(Properties.Settings.Default.Connection_String);
                if (prodecuts == null)
                    prodecuts = db.Prodecuts.ToList();
                var product = prodecuts.SingleOrDefault(x => x.ProdecutID == detail.ItemID);
                if (prodecuts == null) return;
                if (detail.ItemUnitID == 1)
                    e.DisplayText = product.FiestUnit;
                else if (detail.ItemUnitID == 2)
                    e.DisplayText = product.SecoundUnit;
                else if (detail.ItemUnitID == 3)
                    e.DisplayText = product.ThreeUnit;
            }
        }
        RepositoryItemLookUpEdit ItemsRepo = new RepositoryItemLookUpEdit();
        RepositoryItemLookUpEdit UOMRepo = new RepositoryItemLookUpEdit();
        RepositoryItemLookUpEdit BranchRepo = new RepositoryItemLookUpEdit();
        RepositoryItemLookUpEdit repoSize = new RepositoryItemLookUpEdit();
        RepositoryItemLookUpEdit repoColor = new RepositoryItemLookUpEdit();
        RepositoryItemLookUpEdit repoPartID = new RepositoryItemLookUpEdit();

        public override void Delete()
        {
            deleteToolStripMenuItem.PerformClick();
        }
        public override void RefreshData()
        {

            GetGRidData();
            DAL.DBDataContext objDataContext = new DBDataContext(Properties.Settings.Default.Connection_String);
              
            var items = from c in objDataContext.Prodecuts  select new { ID = c.ProdecutID, Name =  c.ProdecutName };
            var branches = from s in objDataContext.Stores  select new { ID =s.StoreID ,  Name  = s.StoreName   };

            repoColor.DataSource = (from c in objDataContext.Inv_Colors select c).ToList();
            repoSize.DataSource = (from s in objDataContext.Inv_Sizes select s).ToList();
           // UOMRepo.DataSource = UOM.ToList();
            ItemsRepo.DataSource = items.ToList();
            lkp_Store.Properties.DataSource = BranchRepo.DataSource = branches.ToList();

            switch (InvoiceType)
            {
            
                case InvoiceType.SalesInvoice:
                case InvoiceType.SalesReturn:
                    repoPartID.DataSource = objDataContext.Customers.Select(x => new { Name = x.CustomerName, ID = x.CustomerID }).ToList();
                    break;
                case InvoiceType.PurchaseInvoice:
                case InvoiceType.PurchaseReturn:
                    repoPartID.DataSource = objDataContext.Suppliers.Select(x => new { Name = x.SupplierName  , ID = x.SupplierID  }).ToList();
                    break;
                case InvoiceType.ItemOpenBalance:
                case InvoiceType.ItemDamage:
                case InvoiceType.ItemStoreMove:
                    break;
                default:
                    throw new NotImplementedException(); 
            }



        }
        void GetGRidData()
        { 
            DAL.DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);

            var result = from i in db.Inv_Invoices
                         from u in db.UserPermissions  .Where(x => x.ID == i.UserID)
                         where   i.InvoiceType == (int)InvoiceType
                         select new
                         {
                             i.ID,
                             i.Code,
                             i.PartID,
                             i.StoreID,
                             i.Date, 
                             i.DiscountRatio,
                             DiscountVal = i.DiscountValue , 
                             i.Net,
                             i.Paid ,
                             PayStatus = "",
                             Remains = i.Net - i.Paid,
                             i.SourceID, 
                             UserID = u.UserName,
                             i.Notes, 
                             Details =  db.Inv_InvoiceDetails.Where(d=>d.InvoiceID == i.ID ).ToList(), 
                         };

             
            if (dt_From.EditValue != null)
                result = result.Where(x => x.Date >= dt_From.DateTime.Date);
            if (dt_To.EditValue != null)
                result = result.Where(x => x.Date <= dt_To.DateTime.Date);
            if (lkp_Store.EditValue != null && lkp_Store.EditValue != DBNull.Value)
                result = result.Where(x => x.StoreID == Convert.ToInt32(lkp_Store.EditValue));
           

            gridControl1.DataSource = result.ToList(); 

        }
        public override void New()
        {
           new frm_Inv_Invoice( InvoiceType).Show();
        }
        public override void Save()
        {

        }
        public override void Print()
        { 
            gridView1.OptionsPrint.PrintFilterInfo = true;
            RPT.rpt_GridReport.Print(gridControl1, this.Text, gridView1.FilterPanelText, (this.RightToLeft == System.Windows.Forms.RightToLeft.Yes) ? true : false);
            base.Print();
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {

            if (gridView1.FocusedRowHandle >= 0)
            {
                if (IsForSelect)
                {
                    SelectedInvoiceID = Convert.ToInt32(gridView1.GetFocusedRowCellValue("ID"));
                    this.Close();
                }
                else 
                    new frm_Inv_Invoice(Convert.ToInt32(gridView1.GetFocusedRowCellValue("ID"))).Show();
            }
        }

       

        private void frm_Inv_InvoiceList_FormClosing(object sender, FormClosingEventArgs e)
        {
            MasterClass.SaveGridLayout(gridView1, this);
        }

        private void gridControl1_ViewRegistered(object sender, DevExpress.XtraGrid.ViewOperationEventArgs e)
        {
            GridView view = e.View as GridView;

            view.OptionsView.ShowFooter = false;
            view.OptionsView.ShowViewCaption = true;
            

            if (view.LevelName == "Details")
            {
                view.ViewCaption = "الاصناف";
                view.Columns["ItemID"].ColumnEdit = ItemsRepo;
                view.Columns["ItemUnitID"].ColumnEdit = UOMRepo;
                view.Columns["Color"].ColumnEdit = repoColor;
                view.Columns["Size"].ColumnEdit = repoSize;
                repoColor.NullText = repoSize.NullText = "";
                view.SynchronizeClones = true;
                view.Name = "Details";
                view.CustomColumnDisplayText += GridView_Items_CustomColumnDisplayText;
                /*
                   d.ItemID,
                                            d.ItemUnitID,
                                            d.ItemQty,
                                            d.Price,
                                            d.TotalPrice, 
                                            l.Size,
                                            l.Color,
                                            l.ExpDate
                 
                 */
          
                view.Columns[nameof(InvoDInsta.Color)].Caption = "اللون";
                view.Columns[nameof(InvoDInsta.Size)].Caption = "الحجم"; 
                view.Columns[nameof(InvoDInsta.ItemID)].Caption = "الصنف";
                view.Columns[nameof(InvoDInsta.ItemUnitID)].Caption = "الوحده";
                view.Columns[nameof(InvoDInsta.ItemQty)].Caption = "الكميه";
                view.Columns[nameof(InvoDInsta.Price)].Caption = "السعر";
                view.Columns[nameof(InvoDInsta.TotalPrice)].Caption = "الاجمالي";
                view.Columns[nameof(InvoDInsta.Discount)].Caption = "ن خصم";
                view.Columns[nameof(InvoDInsta.DiscountValue)].Caption = "ق خصم";
                view.Columns[nameof(InvoDInsta.AddTax)].Caption = "VAT %";
                view.Columns[nameof(InvoDInsta.AddTaxVal)].Caption = "VAT";
                view.Columns[nameof(InvoDInsta.ExpDate)].Caption = "الصلاحيه";
                view.Columns[nameof(InvoDInsta.StoreID)].Caption = "المخزن";
                view.Columns[nameof(InvoDInsta.CostValue)].Caption = "سعر الشراء";
                view.Columns[nameof(InvoDInsta.TotalCostValue)].Caption = "الاجمالي بسعر الشراء";
                view.Columns[nameof(InvoDInsta.Serial)].Visible =
                view.Columns[nameof(InvoDInsta.Serial)].OptionsColumn.ShowInCustomizationForm = false;
                view.Columns[nameof(InvoDInsta.ID)].Visible =
                view.Columns[nameof(InvoDInsta.ID)].OptionsColumn.ShowInCustomizationForm = false;
                view.Columns[nameof(InvoDInsta.InvoiceID)].Visible =
                view.Columns[nameof(InvoDInsta.InvoiceID)].OptionsColumn.ShowInCustomizationForm = false;


                MasterClass.RestoreGridLayout(view, this);
            }

        }
        bool IsClearingFilters;
        private void btn_Clear_Click(object sender, EventArgs e)
        {
            IsClearingFilters = true;
            dt_From.EditValue = dt_To.EditValue =  lkp_Store.EditValue = null;
            IsClearingFilters = false;
            GetGRidData();
        }

        private void Filters_EditValueChanged(object sender, EventArgs e)
        {
            if (!IsClearingFilters)
                GetGRidData();
        }

        private void Filter_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.GetType() != typeof(EditorButton) || e.Button.Tag == null)
                return;

            string btnName = e.Button.Tag.ToString();
            if (btnName == "Clear")
            {

                ((DevExpress.XtraEditors.BaseEdit)sender).EditValue = null;
            }
        }

        private void gridView1_MasterRowGetRelationDisplayCaption(object sender, MasterRowGetRelationNameEventArgs e)
        {
            if (e.RelationName == "Details")
                e.RelationName = "الاصناف";
            
        }

        private void gridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            ColumnView columnView = sender as ColumnView;
            int h = e.RowHandle;
            GridView view = sender as GridView;
            if (e.RowHandle < 0) return;
           
            else if (e.Column.FieldName == "PayStatus")
            {
                //    if(view.GetRowCellValue(e.RowHandle, "PayType") == null ||
                //        view.GetRowCellValue(e.RowHandle, "PayType") == DBNull.Value)
                //        e.Graphics.FillRectangle(new SolidBrush(Color.Orange ), e.Bounds);
                //    else if(Convert.ToBoolean(view.GetRowCellValue(e.RowHandle, "PayType")) == false )
                //        e.Graphics.FillRectangle(new SolidBrush(Color.Red ), e.Bounds);

                if (Convert.ToDouble(view.GetRowCellValue(e.RowHandle, "Remains")) <= 0)
                {
                    e.DisplayText = "مسدده";
                    e.Graphics.FillRectangle(new SolidBrush(Color.Green), e.Bounds);
                }
                else if (Convert.ToDouble(view.GetRowCellValue(e.RowHandle, "Remains")) > 0
                    && Convert.ToDouble(view.GetRowCellValue(e.RowHandle, "Remains")) < Convert.ToDouble(view.GetRowCellValue(e.RowHandle, "Net")))
                {
                    e.DisplayText ="مسدده جزئياً";
                    e.Graphics.FillRectangle(new SolidBrush(Color.Orange), e.Bounds);
                }
                else if (Convert.ToDouble(view.GetRowCellValue(e.RowHandle, "Remains")) == Convert.ToDouble(view.GetRowCellValue(e.RowHandle, "Net")))
                {
                    e.DisplayText = "غير مسدده";
                    e.Graphics.FillRectangle(new SolidBrush(Color.Red), e.Bounds);
                }

            }
            //else if (e.Column.FieldName == "Source")
            //{
            //    e.DisplayText = Master.Prossess[e.CellValue.ToInt()];
            //}

        }

        private void frm_Inv_InvoiceList_Load(object sender, EventArgs e)
        {
            this.Load += (_sender, _e) => { MasterClass.RestoreGridLayout(gridView1, this); };
            this.FormClosing += (_sender, _e) => { MasterClass.SaveGridLayout(gridView1, this); };
        }
    }
}