﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using ByStro.Clases;

namespace ByStro.Forms
{
    public partial class frm_Master : XtraForm 
    {
        internal virtual bool IsNew { get; set; }
        internal bool ChangesMade;
        public frm_Master()
        {
            InitializeComponent();
          
        }

        public virtual void Save()
        {

            MasterClass.Saved(); 
            ChangesMade = false;
            IsNew = false;
            if (PrintAfterSave == true)
                Print();
        }
        public virtual void New()
        {

        }
        public virtual void Delete()
        {

        }
        public virtual void OpenList()
        {

        }
        public virtual void Print()
        {

        }
         
        public virtual void frm_Load(object sender, EventArgs e)
        {
            RefreshData();

        }
        public virtual void RefreshData()
        {

        }
        public bool SaveChangedData()
        {
            switch (XtraMessageBox.Show("يوجد بعض التعديلات التي تمت ولم يتم حفظها , هل تريد حفظها اولا ؟ ", "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
            {

                case DialogResult.Cancel: return false;
                case DialogResult.Yes: Save(); return true;
                case DialogResult.No: return true;
                default: return false;


            }
        }
        public virtual void DataChanged(object sender, EventArgs e)
        {
            ChangesMade = true;

        }
        bool ProssingKey;
        /* 
        If the form is currently prossing a keyDown event 
        used to prevent  The Form.KeyDown event is raised twice when a key is pressed in
        a grid and the Form.KeyPreview option is enabled  
        */

        public virtual void frm_Master_KeyDown(object sender, KeyEventArgs e)
        {
            if (ProssingKey) return;
            ProssingKey = true;
            if (e.KeyCode == Keys.S && e.Modifiers == Keys.Alt)
            {

            }
            if (e.KeyCode == Keys.I && e.Modifiers == Keys.Alt)
            {

            }
            if (e.KeyCode == Keys.S && e.Modifiers == Keys.Control)
            {

                Save();

            }
            if (e.KeyCode == Keys.Delete && e.Modifiers == Keys.Control)
            {

                Delete();
            }
            if (e.KeyCode == Keys.N && e.Modifiers == Keys.Control)
            {
                New();
            }
            if (e.KeyCode == Keys.P && e.Modifiers == Keys.Control)
            {
                if (IsNew || ChangesMade)
                    SaveAndPrint();
                else
                    Print();
            }
            if (e.KeyCode == Keys.R && e.Modifiers == Keys.Control)
            {
                RefreshData();
            }
           
            ProssingKey = false;
        }
        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (IsNew || ChangesMade)
                SaveAndPrint();
            else
                Print();
        }

        public void SaveAndPrint()
        {
            if (XtraMessageBox.Show("يجب حفظ التغييرات اولاً , هل تريد الحفظ ؟", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                PrintAfterSave = true;
                Save();
              //  Print();
                PrintAfterSave = false;

            }
        }
        public void frm_Master_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ChangesMade && !SaveChangedData()) e.Cancel = true;

        }
        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RefreshData();
        }

        private void btn_New_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            New();
        }
        private void bt_Save_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Save();
        }
        private void btn_Delete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Delete();
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OpenList();
        }

        private void btn_Refresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RefreshData();
        }
        bool PrintAfterSave;
        private void btn_SaveAndPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            PrintAfterSave = true;
            Save();
            PrintAfterSave = false;
        }
    }
}