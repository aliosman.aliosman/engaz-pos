﻿namespace ByStro.Forms
{
    partial class frm_EditReportFilters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.cb_DateFilterType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dt_OnDate = new DevExpress.XtraEditors.DateEdit();
            this.dt_End = new DevExpress.XtraEditors.DateEdit();
            this.dt_Start = new DevExpress.XtraEditors.DateEdit();
            this.cb_Products = new ByStro.UControle.MyCheckedComboBoxEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.cb_Stores = new ByStro.UControle.MyCheckedComboBoxEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lyc_Products = new DevExpress.XtraLayout.LayoutControlItem();
            this.lyc_Stores = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lyc_StartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.lyc_EndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.lyc_OnDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.lyc_DatefilterType = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cb_DateFilterType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_OnDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_OnDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_End.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_End.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_Start.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_Start.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Products.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Stores.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_Products)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_Stores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_StartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_EndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_OnDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_DatefilterType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.cb_DateFilterType);
            this.layoutControl1.Controls.Add(this.dt_OnDate);
            this.layoutControl1.Controls.Add(this.dt_End);
            this.layoutControl1.Controls.Add(this.dt_Start);
            this.layoutControl1.Controls.Add(this.cb_Products);
            this.layoutControl1.Controls.Add(this.simpleButton1);
            this.layoutControl1.Controls.Add(this.cb_Stores);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsView.RightToLeftMirroringApplied = true;
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(415, 388);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // cb_DateFilterType
            // 
            this.cb_DateFilterType.Location = new System.Drawing.Point(24, 138);
            this.cb_DateFilterType.Name = "cb_DateFilterType";
            this.cb_DateFilterType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_DateFilterType.Properties.Items.AddRange(new object[] {
            "في فتره محدده",
            "اكبر من ",
            "اقل من ",
            "في يوم محدد",
            "بدون"});
            this.cb_DateFilterType.Size = new System.Drawing.Size(318, 20);
            this.cb_DateFilterType.StyleController = this.layoutControl1;
            this.cb_DateFilterType.TabIndex = 9;
            this.cb_DateFilterType.SelectedIndexChanged += new System.EventHandler(this.cb_DateFilterType_SelectedIndexChanged);
            // 
            // dt_OnDate
            // 
            this.dt_OnDate.EditValue = null;
            this.dt_OnDate.Location = new System.Drawing.Point(24, 210);
            this.dt_OnDate.Name = "dt_OnDate";
            this.dt_OnDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dt_OnDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dt_OnDate.Size = new System.Drawing.Size(318, 20);
            this.dt_OnDate.StyleController = this.layoutControl1;
            this.dt_OnDate.TabIndex = 8;
            // 
            // dt_End
            // 
            this.dt_End.EditValue = null;
            this.dt_End.Location = new System.Drawing.Point(24, 186);
            this.dt_End.Name = "dt_End";
            this.dt_End.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dt_End.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dt_End.Size = new System.Drawing.Size(318, 20);
            this.dt_End.StyleController = this.layoutControl1;
            this.dt_End.TabIndex = 7;
            // 
            // dt_Start
            // 
            this.dt_Start.EditValue = null;
            this.dt_Start.Location = new System.Drawing.Point(24, 162);
            this.dt_Start.Name = "dt_Start";
            this.dt_Start.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dt_Start.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dt_Start.Size = new System.Drawing.Size(318, 20);
            this.dt_Start.StyleController = this.layoutControl1;
            this.dt_Start.TabIndex = 6;
            // 
            // cb_Products
            // 
            this.cb_Products.Location = new System.Drawing.Point(24, 45);
            this.cb_Products.Name = "cb_Products";
            this.cb_Products.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Products.Properties.DisplayMember = "Name";
            this.cb_Products.Properties.NullText = "الكل";
            this.cb_Products.Properties.ValueMember = "ID";
            this.cb_Products.Size = new System.Drawing.Size(318, 20);
            this.cb_Products.StyleController = this.layoutControl1;
            this.cb_Products.TabIndex = 5;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(12, 354);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(84, 22);
            this.simpleButton1.StyleController = this.layoutControl1;
            this.simpleButton1.TabIndex = 4;
            this.simpleButton1.Text = "عرض";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // cb_Stores
            // 
            this.cb_Stores.Location = new System.Drawing.Point(24, 69);
            this.cb_Stores.Name = "cb_Stores";
            this.cb_Stores.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cb_Stores.Properties.DisplayMember = "Name";
            this.cb_Stores.Properties.NullText = "الكل";
            this.cb_Stores.Properties.ValueMember = "ID";
            this.cb_Stores.Size = new System.Drawing.Size(318, 20);
            this.cb_Stores.StyleController = this.layoutControl1;
            this.cb_Stores.TabIndex = 5;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup1,
            this.layoutControlItem1,
            this.layoutControlGroup2,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(415, 388);
            this.Root.TextVisible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lyc_Products,
            this.lyc_Stores});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(395, 93);
            this.layoutControlGroup1.Text = "الفلاتر";
            // 
            // lyc_Products
            // 
            this.lyc_Products.Control = this.cb_Products;
            this.lyc_Products.Location = new System.Drawing.Point(0, 0);
            this.lyc_Products.Name = "lyc_Products";
            this.lyc_Products.Size = new System.Drawing.Size(371, 24);
            this.lyc_Products.Text = "الاصناف";
            this.lyc_Products.TextSize = new System.Drawing.Size(46, 13);
            // 
            // lyc_Stores
            // 
            this.lyc_Stores.Control = this.cb_Stores;
            this.lyc_Stores.ControlAlignment = System.Drawing.ContentAlignment.TopRight;
            this.lyc_Stores.CustomizationFormText = "المخازن";
            this.lyc_Stores.Location = new System.Drawing.Point(0, 24);
            this.lyc_Stores.Name = "lyc_Stores";
            this.lyc_Stores.Size = new System.Drawing.Size(371, 24);
            this.lyc_Stores.Text = "المخازن";
            this.lyc_Stores.TextSize = new System.Drawing.Size(46, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.simpleButton1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 342);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(88, 26);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lyc_StartDate,
            this.lyc_EndDate,
            this.lyc_OnDate,
            this.lyc_DatefilterType});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 93);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(395, 141);
            this.layoutControlGroup2.Text = "فلاتر التاريخ";
            // 
            // lyc_StartDate
            // 
            this.lyc_StartDate.Control = this.dt_Start;
            this.lyc_StartDate.Location = new System.Drawing.Point(0, 24);
            this.lyc_StartDate.Name = "lyc_StartDate";
            this.lyc_StartDate.Size = new System.Drawing.Size(371, 24);
            this.lyc_StartDate.Text = "من تاريخ";
            this.lyc_StartDate.TextSize = new System.Drawing.Size(46, 13);
            // 
            // lyc_EndDate
            // 
            this.lyc_EndDate.Control = this.dt_End;
            this.lyc_EndDate.Location = new System.Drawing.Point(0, 48);
            this.lyc_EndDate.Name = "lyc_EndDate";
            this.lyc_EndDate.Size = new System.Drawing.Size(371, 24);
            this.lyc_EndDate.Text = "حتي تاريخ";
            this.lyc_EndDate.TextSize = new System.Drawing.Size(46, 13);
            // 
            // lyc_OnDate
            // 
            this.lyc_OnDate.Control = this.dt_OnDate;
            this.lyc_OnDate.Location = new System.Drawing.Point(0, 72);
            this.lyc_OnDate.Name = "lyc_OnDate";
            this.lyc_OnDate.Size = new System.Drawing.Size(371, 24);
            this.lyc_OnDate.Text = "التاريخ";
            this.lyc_OnDate.TextSize = new System.Drawing.Size(46, 13);
            // 
            // lyc_DatefilterType
            // 
            this.lyc_DatefilterType.Control = this.cb_DateFilterType;
            this.lyc_DatefilterType.Location = new System.Drawing.Point(0, 0);
            this.lyc_DatefilterType.Name = "lyc_DatefilterType";
            this.lyc_DatefilterType.Size = new System.Drawing.Size(371, 24);
            this.lyc_DatefilterType.Text = "النوع";
            this.lyc_DatefilterType.TextSize = new System.Drawing.Size(46, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(88, 342);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(307, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 234);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(395, 108);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // frm_EditReportFilters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 388);
            this.Controls.Add(this.layoutControl1);
            this.EnableAcrylicAccent = true;
            this.IconOptions.SvgImage = global::ByStro.Properties.Resources.editfilter;
            this.Name = "frm_EditReportFilters";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "فلاتر تقرير";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cb_DateFilterType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_OnDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_OnDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_End.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_End.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_Start.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_Start.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Products.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cb_Stores.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_Products)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_Stores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_StartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_EndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_OnDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_DatefilterType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private UControle.MyCheckedComboBoxEdit cb_Products;
        private DevExpress.XtraLayout.LayoutControlItem lyc_Products;
        private UControle.MyCheckedComboBoxEdit cb_Stores;
        private DevExpress.XtraLayout.LayoutControlItem lyc_Stores;
        private DevExpress.XtraEditors.DateEdit dt_OnDate;
        private DevExpress.XtraEditors.DateEdit dt_End;
        private DevExpress.XtraEditors.DateEdit dt_Start;
        private DevExpress.XtraLayout.LayoutControlItem lyc_StartDate;
        private DevExpress.XtraLayout.LayoutControlItem lyc_EndDate;
        private DevExpress.XtraLayout.LayoutControlItem lyc_OnDate;
        private DevExpress.XtraEditors.ComboBoxEdit cb_DateFilterType;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem lyc_DatefilterType;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
    }
}