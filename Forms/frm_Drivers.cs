﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;  
using System.Linq.Expressions; 
using ByStro.DAL;

namespace ByStro.Forms
{
    public partial class frm_Drivers : XtraForm
    {
        public frm_Drivers()
        {
            InitializeComponent(); 
        }
        
        private void GridView1_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = ExceptionMode.NoAction;
        }
        private void GridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            var row = e.Row as DAL.Driver;
            if (row == null) return;
            if (string.IsNullOrEmpty(row.Code ))
            {
                e.Valid = false;
                gridView1.SetColumnError(gridView1.Columns[nameof(row.Code)], "برجاء ادخال الكود ");

            }
            if (string.IsNullOrEmpty(row.Name))
            {
                e.Valid = false;
                gridView1.SetColumnError(gridView1.Columns[nameof(row.Name)], "برجاء ادخال الاسم ");

            }
        }
        DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
        private void frm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (gridView1.HasColumnErrors)
            {
                e.Cancel = true;
                return;
            }
            db.SubmitChanges();
        }
        private void frm_Load(object sender, EventArgs e)
        {
            this.FormClosing +=   frm_FormClosing;


            gridControl1.DataSource = db.Drivers.Select<DAL.Driver, DAL.Driver>((Expression<Func<DAL.Driver, DAL.Driver>>)
                 (x => x));
            gridView1.ValidateRow += GridView1_ValidateRow;
            gridView1.InvalidRowException += GridView1_InvalidRowException;
            gridView1.Columns["ID"].Visible = false; 
            gridView1.Columns["Name"].Caption = "اسم السائق";
            gridView1.Columns["Code"].Caption = "كود"; 
            gridView1.CellValueChanged += GridView1_CellValueChanged;
        }

        private void GridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {

            db.SubmitChanges();
        }
    }
}
