﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Linq.Expressions;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Grid;
using ByStro.Clases;
using ByStro.PL;
using ByStro.DAL;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid;
namespace ByStro.Forms
{
    public partial class frm_PrintItemBarcode : DevExpress.XtraEditors.XtraForm
    {
        public frm_PrintItemBarcode(BindingList<ProductBarcode> _products = null )
        {
            InitializeComponent();
            Productes = _products;
             
        }
        int SourceType = 0;
        int SourceID = 0;

        private void GridView1_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            var row = gridView1.GetRow(e.RowHandle) as ProductBarcode;
            if (row == null) return;
            row.Count = 1;
        }

        private void GridView1_CustomRowCellEditForEditing(object sender, CustomRowCellEditEventArgs e)
        {
            var view = sender as GridView;
            var row = view.GetRow(e.RowHandle)as  ProductBarcode;
            if (row == null) return;
            if (e.Column.FieldName == "UnitName")
            {
                RepositoryItemComboBox itemTempComboBox = new RepositoryItemComboBox();

                DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
                var item = db.Prodecuts.Where(x => x.ProdecutID == row.ID).Single();

                var datasource = new List<string>() { item.FiestUnit, item.SecoundUnit, item.ThreeUnit };
                datasource.RemoveAll(x => x == "");

                itemTempComboBox.Items.AddRange(datasource);
                e.RepositoryItem = itemTempComboBox;

            }
        }


        private void GridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            var row = gridView1.GetRow(e.RowHandle) as ProductBarcode;
            if (row == null) return;
            var ins = new ProductBarcode();
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            var item = db.Prodecuts.SingleOrDefault(x => x.ProdecutID == row.ID);
            if(item == null )
            {
                gridView1.DeleteRow(e.RowHandle);
                return;
            }
            switch (e.Column.FieldName)
            {
                case nameof(ins.ID):
                    //row.Group = db.Categories.Single(x => x.CategoryID == item.CategoryID).CategoryName;
                    //row.Company = item.Company;
                    row.Name = item.ProdecutName;

                    if (item.UnitDefoult == 1)
                    {
                        row.UnitName  = item.FiestUnit;
                    }
                    else if (item.UnitDefoult == 2)
                    {
                        row.UnitName = item.SecoundUnit;
                    }
                    else if (item.UnitDefoult == 3)
                    {
                        row.UnitName = item.ThreeUnit;
                    }
                    GridView1_CellValueChanged(sender, new DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs(e.RowHandle, gridView1.Columns[nameof(row.UnitName)],row.UnitBarcode ));
                    break;
                case nameof(ins.UnitName):
                    if(row.UnitName == item.FiestUnit)
                    {
                        row.UnitBarcode = item.FiestUnitBarcode;
                        row.Price = item.FiestUnitPrice1.Value;
                    }
                    else if (row.UnitName == item.SecoundUnit)
                    {
                        row.UnitBarcode = item.SecoundUnitBarcode;
                        row.Price = item.SecoundUnitPrice1.Value;
                    }
                    else if (row.UnitName == item.SecoundUnit)
                    {
                        row.UnitBarcode = item.ThreeUnitBarcode;
                        row.Price = item.ThreeUnitPrice1.Value;
                    }
                    else
                    {
                        GridView1_CellValueChanged(sender, new DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs(e.RowHandle, gridView1.Columns[nameof(row.ID )], row.ID ));
                    }

                    break;
            }
        }

        BindingList<ProductBarcode> products;
         public BindingList<ProductBarcode > Productes { get { if (products == null) products = new BindingList<ProductBarcode>(); return products; } set => products = value; }
        public  class ProductBarcode
        {
            public int ID { get; set; }  
            public string Name { get; set; }
            public string UnitBarcode { get; set; }
            public string UnitName { get; set; }
            public double Price { get; set; }
            public int Count { get; set; }
            public int SourceID { get; set; }
            public int SourceType { get; set; }
            public string Color { get; set; }
            public string Size { get; set; }

            public string PrintName { get => Name+" "+ Color +" "+ Size; }
            public string PrintBarcode { get {

                    if (SourceType > 0)
                        return SourceType + "*" + SourceID; 
                   else if (string.IsNullOrEmpty( UnitBarcode) == false)
                       return UnitBarcode; 
                    else
                        return ID.ToString();

                }
            }


        }

        private void frm_PrintItemBarcode_Load(object sender, EventArgs e)
        {
            gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom;
            gridControl1.DataSource = Productes;
            var ins = new ProductBarcode();
            gridView1.Columns[nameof(ins.ID)].Caption = "الصنف";
            //gridView1.Columns[nameof(ins.Group)].Visible =
            //gridView1.Columns[nameof(ins.Company)].Visible =

            gridView1.Columns[nameof(ins.UnitBarcode)].Visible =
            gridView1.Columns[nameof(ins.SourceID)].Visible =
            gridView1.Columns[nameof(ins.SourceType)].Visible =
            gridView1.Columns[nameof(ins.Color)].Visible =
            gridView1.Columns[nameof(ins.Size)].Visible =
            gridView1.Columns[nameof(ins.PrintBarcode)].Visible =
            gridView1.Columns[nameof(ins.PrintName)].Visible =
            gridView1.Columns[nameof(ins.Name)].Visible = false;
            gridView1.Columns[nameof(ins.UnitName)].Caption = "الوحده";
            gridView1.Columns[nameof(ins.Price)].Caption = "السعر";
            gridView1.Columns[nameof(ins.Count)].Caption = "الكميه";

            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            lookUpEdit1.Properties.DataSource = db.BarcodeTemplates.OrderByDescending (x => x.IsDefault).ToList();
            lookUpEdit1.Properties.ValueMember = "ID";
            lookUpEdit1.Properties.DisplayMember = "Name";
            lookUpEdit1.EditValue = db.BarcodeTemplates.OrderByDescending(x => x.IsDefault).FirstOrDefault().ID;

            lookUpEdit1.Properties.PopulateColumns();
            lookUpEdit1.Properties.Columns["Template"].Visible = false;
            lookUpEdit1.Properties.Columns["ID"].Visible = false;
            lookUpEdit1.Properties.Columns["IsDefault"].Visible = false;
            lookUpEdit1.Properties.ShowHeader = false;


            RepositoryItemLookUpEdit repoItems = new RepositoryItemLookUpEdit();
            repoItems.DataSource = db.Prodecuts.Select(x => new { ID = x.ProdecutID, Name = x.ProdecutName }).ToList();
            repoItems.ValueMember = "ID";
            repoItems.DisplayMember = "Name";
            RepositoryItemSpinEdit spinEdit = new RepositoryItemSpinEdit();
            spinEdit.IsFloatValue = false;
            spinEdit.MinValue = 1;
            spinEdit.MaxValue = 100000;

            RepositoryItemSpinEdit repoSpinEdit = new RepositoryItemSpinEdit();


            gridControl1.RepositoryItems.Add(repoItems);
            gridControl1.RepositoryItems.Add(spinEdit);
            gridControl1.RepositoryItems.Add(repoSpinEdit);

            gridView1.Columns[nameof(ins.ID)].ColumnEdit = repoItems;
            gridView1.Columns[nameof(ins.Count)].ColumnEdit = spinEdit;
            gridView1.Columns[nameof(ins.Price)].ColumnEdit = repoSpinEdit;
            RepositoryItemButtonEdit buttonEdit = new RepositoryItemButtonEdit();
            gridControl1.RepositoryItems.Add(buttonEdit);
            buttonEdit.Buttons.Clear();
            buttonEdit.Buttons.Add(new EditorButton(ButtonPredefines.Delete));
            buttonEdit.ButtonClick += ButtonEdit_ButtonClick;
            GridColumn clmnDelete = new GridColumn() { Name = "Delete", FieldName = "Delete", ColumnEdit = buttonEdit, VisibleIndex = 14, Width = 15 };
            buttonEdit.TextEditStyle = TextEditStyles.HideTextEditor;

            GridColumn clmnDeleteBarcode = new GridColumn() { Name = "Delete", ColumnEdit = buttonEdit, VisibleIndex = 100, Width = 15 };
            gridView1.Columns.Add(clmnDelete);



            gridView1.CellValueChanged += GridView1_CellValueChanged;
            gridView1.CustomRowCellEditForEditing += GridView1_CustomRowCellEditForEditing;
            gridView1.InitNewRow += GridView1_InitNewRow;
        }
        private void ButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            GridView view = ((GridControl)((ButtonEdit)sender).Parent).MainView as GridView;

            if (view.FocusedRowHandle >= 0)
            {

                view.DeleteSelectedRows();

            }

        }
        private void btn_Print_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            
            var source = new List<ProductBarcode>();
            foreach (var item in Productes)
            {
                for (int i = 0; i < item.Count; i++)
                {
                    source.Add(item);
                }
            }
            RPT.rtp_Barcode.Print(source, lookUpEdit1.EditValue.ToInt());

        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new frm_Inv_InvoiceList(MasterClass.InvoiceType.PurchaseInvoice , true).ShowDialog();
            int invoiceID = frm_Inv_InvoiceList.SelectedInvoiceID;

            if (invoiceID != 0)
            {
                DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
                var list = db.Inv_InvoiceDetails.Where(x => x.InvoiceID  == invoiceID).ToList();
                list.ForEach(d =>
                {
                    var unitName = db.Prodecuts.Single(x => x.ProdecutID == d.ItemID).FiestUnit ;
                    if (d.ItemUnitID > 1)
                        unitName = (d.ItemUnitID == 2) ? db.Prodecuts.Single(x => x.ProdecutID == d.ItemID).SecoundUnit : db.Prodecuts.Single(x => x.ProdecutID == d.ItemID).ThreeUnit;

                    Productes.Add(new ProductBarcode()
                    {
                        SourceType = (int)MasterClass.InvoiceType.PurchaseInvoice ,
                        SourceID = d.ID,
                        Count = d.ItemQty .ToInt(),
                        ID = d.ItemID, 
                        Name = db.Prodecuts.Single(x=>x.ProdecutID == d.ItemID).ProdecutName ,
                        UnitName = unitName,
                        Color = db.Inv_Colors.Where(x=> x.ID == d.Color).Select(x=>x.Name ??"").FirstOrDefault(),
                        Size = db.Inv_Sizes.Where(x => x.ID == d.Size).Select(x => x.Name ?? "").FirstOrDefault(),
                    });
                });
                for (int i = 0; i < Productes.Count(); i++)
                {
                    GridView1_CellValueChanged(gridView1, new DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs(i, gridView1.Columns["UnitName"], products[i].UnitName));
                }
    
            }
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new frm_Inv_InvoiceList(MasterClass.InvoiceType.ItemOpenBalance, true).ShowDialog();
            int invoiceID = frm_Inv_InvoiceList.SelectedInvoiceID;

            if (invoiceID != 0)
            {
                DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
                var list = db.Inv_InvoiceDetails.Where(x => x.InvoiceID == invoiceID).ToList();
                list.ForEach(d =>
                {
                    var unitName = db.Prodecuts.Single(x => x.ProdecutID == d.ItemID).FiestUnit;
                    if (d.ItemUnitID > 1)
                        unitName = (d.ItemUnitID == 2) ? db.Prodecuts.Single(x => x.ProdecutID == d.ItemID).SecoundUnit : db.Prodecuts.Single(x => x.ProdecutID == d.ItemID).ThreeUnit;

                    Productes.Add(new ProductBarcode()
                    {
                        SourceType = (int)MasterClass.InvoiceType.ItemOpenBalance,
                        SourceID = d.ID,
                        Count = d.ItemQty.ToInt(),
                        ID = d.ItemID,
                        Name = db.Prodecuts.Single(x => x.ProdecutID == d.ItemID).ProdecutName,
                        UnitName = unitName,
                        Color = db.Inv_Colors.Where(x => x.ID == d.Color).Select(x => x.Name ?? "").FirstOrDefault(),
                        Size = db.Inv_Sizes.Where(x => x.ID == d.Size).Select(x => x.Name ?? "").FirstOrDefault(),
                    });
                });
                for (int i = 0; i < Productes.Count(); i++)
                {
                    GridView1_CellValueChanged(gridView1, new DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs(i, gridView1.Columns["UnitName"], products[i].UnitName));
                }

            }
        }
    }
}