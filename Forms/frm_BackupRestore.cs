﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using System.Diagnostics;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;

namespace ByStro.Forms
{
    public partial class frm_BackupRestore : DevExpress.XtraEditors.XtraForm
    {
        public frm_BackupRestore()
        {
            InitializeComponent();
        }

        private void buttonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            var buttonEdit = sender as ButtonEdit;
            using (XtraFolderBrowserDialog dialog = new XtraFolderBrowserDialog()  )
            {
              
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                   
                        buttonEdit.Text = dialog.SelectedPath; 
                }
            }
        }

        private void buttonEdit1_EditValueChanged(object sender, EventArgs e)
        {
            if (System.IO.Directory.Exists(buttonEdit1.Text))
            {
                Properties.Settings.Default.BukupFillPath = buttonEdit1.Text;
                Properties.Settings.Default.Save();
            }
        }

        private void frm_BackupRestore_Load(object sender, EventArgs e)
        {
            buttonEdit1.Text = Properties.Settings.Default.BukupFillPath;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (System.IO.Directory.Exists(buttonEdit1.Text) == false)
            {
                XtraMessageBox.Show("الرجاء التحقق من المسار");
                return;

            }
            simpleButton1.Enabled = false;
            SqlConnection connSQLServer = new SqlConnection(Properties.Settings.Default.Connection_String);
            string filename = buttonEdit1.Text + "\\ByStro" + "_" + DateTime.Now.ToString("yyyy_MM_dd_hh_mm_ss") +   ".bak"; 
     
            Server dbServer = new Server(new ServerConnection(connSQLServer));
            Backup dbBackup = new Backup() { Action = BackupActionType.Database, Database = Properties.Settings.Default.Databasename };
            dbBackup.Devices.AddDevice(filename, DeviceType.File);
            dbBackup.Initialize = true;
            dbBackup.PercentComplete += DbBackup_PercentComplete;
            dbBackup.Complete += DbBackup_Complete;
            dbBackup.SqlBackupAsync(dbServer);

       

        }

        private void DbBackup_Complete(object sender, ServerMessageEventArgs e)
        {
            if (e.Error != null)
            {
                Invoke((MethodInvoker)delegate
                {
                    memoEdit1.Text += e.Error.Message + Environment.NewLine;
                });
            }


            Invoke((MethodInvoker)delegate
            {
                memoEdit1.Text += "تم النسخ بنجاح"
                + Environment.NewLine;
                memoEdit1.SelectionStart = memoEdit1.Text.Length;
                memoEdit1.ScrollToCaret();
                Properties.Settings.Default.LastBackUpTime = DateTime.Now;
                Properties.Settings.Default.Save();
            });

            Invoke((MethodInvoker)delegate
            {

                simpleButton1.Enabled = true;
            });
        }

        private void DbBackup_PercentComplete(object sender, PercentCompleteEventArgs e)
        {
            Invoke((MethodInvoker)delegate
            { 
                progressBarControl1.Position = e.Percent;
                progressBarControl1.Update();
                memoEdit1.Text += e.Message + Environment.NewLine;
                //memoEdit1.SelectionStart = memoEdit1.Text.Length;
                //memoEdit1.ScrollToCaret();
            });
        }

        private void layoutControlItem6_TextChanged(object sender, EventArgs e)
        {
            memoEdit1.SelectionStart = memoEdit1.Text.Length;
            memoEdit1.ScrollToCaret();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            try
            {
                if (System.IO.File.Exists(buttonEdit2.Text) == false)
                {
                    XtraMessageBox.Show("الرجاء التحقق من المسار");
                    return;

                }
                simpleButton2.Enabled = false;
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(Properties.Settings.Default.Connection_String);
                builder.InitialCatalog = "master";
                SqlConnection connSQLServer = new SqlConnection(builder.ConnectionString);

                string filename = buttonEdit2.Text;
                Server dbServer = new Server(new ServerConnection(connSQLServer));
                Restore dbRestore = new Restore()
                {
                    Database = Properties.Settings.Default.Databasename,
                    Action = RestoreActionType.Database , 
                    ReplaceDatabase = true,
                    NoRecovery = false,
                };
                dbRestore.Devices.AddDevice(filename, DeviceType.File);
                dbRestore.PercentComplete += DbRestore_PercentComplete;
                dbRestore.Complete += DbRestore_Complete;

                string dbName = Properties.Settings.Default.Databasename;
                Database database = new Database(dbServer, dbName);
                database.Refresh();
                dbServer.KillAllProcesses(dbName);
                database.DatabaseOptions.UserAccess = DatabaseUserAccess.Single;
                database.Alter(TerminationClause.RollbackTransactionsImmediately);
                dbRestore.SqlRestoreAsync (dbServer);
                
            }
            catch (Exception ex )
            {
                 XtraMessageBox.Show(ex.Message);
                
            }
            
        }

        private void DbRestore_Complete(object sender, ServerMessageEventArgs e)
        {
            if (e.Error != null)
            {
                Invoke((MethodInvoker)delegate
                {
                    memoEdit1.Text += e.Error.Message + Environment.NewLine;
                });
            }


            Invoke((MethodInvoker)delegate
            {
                memoEdit1.Text += "تم الاستعاده بنجاح"
                + Environment.NewLine;
                memoEdit1.SelectionStart = memoEdit1.Text.Length;
                memoEdit1.ScrollToCaret();
            });

            Invoke((MethodInvoker)delegate
            {

                simpleButton2.Enabled = true;
            });
        }

        private void DbRestore_PercentComplete(object sender, PercentCompleteEventArgs e)
        {
            Invoke((MethodInvoker)delegate
            {
                progressBarControl1.Position = e.Percent;
                progressBarControl1.Update();
                memoEdit1.Text += e.Message + Environment.NewLine;

            });
        }

        private void buttonEdit2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            var buttonEdit = sender as ButtonEdit;
            using (XtraOpenFileDialog dialog = new XtraOpenFileDialog() )
            {
                if (System.IO.Directory.Exists(buttonEdit1.Text))
                    dialog.InitialDirectory = buttonEdit1.Text;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    buttonEdit.Text = dialog.FileName;
                }
            }
        }
    }
}