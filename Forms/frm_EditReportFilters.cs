﻿using ByStro.Clases;
using ByStro.DAL;
using DevExpress.Data;
using DevExpress.LookAndFeel;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraLayout;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 
using System.Windows.Forms;
using static ByStro.Forms.frm_Report;

namespace ByStro.Forms
{
    public partial class frm_EditReportFilters : XtraForm 
    {
        ReportType reportType;

        public frm_EditReportFilters(ReportType _reportType)
        {
            InitializeComponent();
            cb_DateFilterType.SelectedIndex = 4;
            reportType = _reportType;
            lyc_Products.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            lyc_Stores.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            lyc_DatefilterType.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;


            switch (reportType)
            {
                case ReportType.ProductMovment:
                    lyc_Products.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lyc_Stores.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lyc_DatefilterType.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                
                    break;
                case ReportType.ProductBalance :
                    lyc_Products.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lyc_Stores.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lyc_DatefilterType.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    cb_DateFilterType.SelectedIndex = 2;
                    cb_DateFilterType.ReadOnly = true;
                    lyc_EndDate.Text = "التاريخ";
                    dt_End.DateTime = DateTime.Now;
                    break;
                case ReportType.ProductReachedReorderLevel:
                    lyc_Products.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lyc_Stores.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lyc_DatefilterType.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    cb_DateFilterType.SelectedIndex = 2;
                    cb_DateFilterType.ReadOnly = true;
                    lyc_EndDate.Text = "التاريخ";
                    dt_End.DateTime = DateTime.Now;
                    break;
                case ReportType.ProductExpire:
                    lyc_Products.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lyc_Stores.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    lyc_DatefilterType.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never ;

                    break;
                default:
                    throw new NotImplementedException();
            }

            GetData();
        }
         DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String); 
        void  GetData()
        { 
            cb_Products.Properties.DataSource =  db.Prodecuts.Select(x => new { ID = x.ProdecutID, Name = x.ProdecutName }).ToList();
            cb_Stores.Properties.DataSource = db.Stores.Select(x => new { ID = x.StoreID, Name = x.StoreName }).ToList();

        }
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            var products = cb_Products.Properties.Items.Where(x => x.CheckState == CheckState.Checked).Select(x => Convert.ToInt32(x.Value)).ToList();
            var stores = cb_Stores.Properties.Items.Where(x => x.CheckState == CheckState.Checked).Select(x => Convert.ToInt32(x.Value)).ToList();


            var filterString = "";
            filterString += lyc_Products.Text + ":" + cb_Products.Text;
            filterString += " ;" + lyc_Stores.Text + ":" + cb_Products.Text;
            if (lyc_StartDate .Visible  && dt_Start .DateTime .Year < 1950)
            {
                dt_Start.ErrorText = "يجب اختيار التاريخ";
                return;
            }
            if (lyc_EndDate .Visible && dt_End.DateTime.Year < 1950)
            {
                dt_End.ErrorText = "يجب اختيار التاريخ";
                return;
            }
            if (lyc_OnDate.Visible && dt_OnDate .DateTime.Year < 1950)
            {
                dt_OnDate .ErrorText = "يجب اختيار التاريخ";
                return;
            }

            var ReportForm =  new frm_Report(reportType);
            switch (reportType)
            {
                #region ProductMovment
                case ReportType.ProductMovment:
                    ReportForm.btn_Refresh.ItemClick += (bs, be) =>
                    {
                        var source = db.Inv_StoreLogs.Select(x => x);
                        if(products.Count > 0)
                            source = source.Where(x => products.Contains(x.ItemID.Value));
                        if (stores.Count > 0)
                            source = source.Where(x => stores.Contains(x.StoreID.Value));
                        if (lyc_EndDate.Visible)
                            source = source.Where(x => x.date.Value.Date <= dt_OnDate.DateTime.Date);
                        if (lyc_StartDate.Visible)
                            source = source.Where(x => x.date.Value.Date >= dt_Start.DateTime.Date);
                        if (lyc_OnDate.Visible)
                            source = source.Where(x => x.date.Value.Date == dt_OnDate.DateTime.Date);




                        ReportForm.gridControl1.DataSource = source.OrderBy(isl => isl.date).Select(s=>new { 
                        ProductName = db.Prodecuts.Single  (x=>x.ProdecutID == s.ItemID ).ProdecutName ,
                        StoreName = db.Stores.Single(x=>x.StoreID == s.StoreID.Value ).StoreName ,
                        Date = s.date, 
                        s.ItemQuIN ,
                        s.ItemQuOut ,
                       BalanceToDate = db.Inv_StoreLogs.Where(isl => isl.ItemID == s.ItemID && isl.Color.GetValueOrDefault() == s.Color.GetValueOrDefault() &&
                                                         isl.Size.GetValueOrDefault() == s.Size.GetValueOrDefault() && isl.date <= s.date).Sum(isl => isl.ItemQuIN)
                                     - db.Inv_StoreLogs.Where(isl => isl.ItemID == s.ItemID && isl.Color.GetValueOrDefault() == s.Color.GetValueOrDefault() && 
                                                        isl.Size.GetValueOrDefault() == s.Size.GetValueOrDefault() && isl.date <= s.date).Sum(isl => isl.ItemQuOut)
,
                        s.BuyPrice, 
                        TotalCost= s.BuyPrice* (s.ItemQuIN + s.ItemQuOut),
                        TotalPrice = s.SellPrice  * (s.ItemQuIN + s.ItemQuOut),

                            s.SellPrice ,
                        s.Serial ,
                        Color = db.Inv_Colors .SingleOrDefault(x=>x.ID ==s.Color).Name ??"",
                        Size =db.Inv_Sizes .SingleOrDefault(x => x.ID == s.Size ).Name ?? "",
                        s.ExpDate ,
                        s.Note ,
                        s.Type ,
                        s.TypeID ,

                        }).ToList();
                  
                   };
                    ReportForm.gridView1.RowCellStyle += (rs ,re) => { 
                        if(re.RowHandle < 0)return ;

                        if (re.Column.FieldName == "ItemQuIN" || re.Column.FieldName == "ItemQuOut")
                        {
                            if (Convert.ToDouble( ReportForm.gridView1.GetRowCellValue(re.RowHandle , "ItemQuIN")) > 0 )
                                re.Appearance.BackColor = DXSkinColors.FillColors.Success;
                            else
                                re.Appearance.BackColor = DXSkinColors.FillColors.Warning; 
                        }else if (re.Column.FieldName == "BalanceToDate"  )
                            {
                            if(re.CellValue is double d && d < 0)
                            {
                                re.Appearance.BackColor = DXSkinColors.FillColors.Warning;

                            }
                            else if (re.CellValue is double dd && dd > 0)
                            {
                                re.Appearance.BackColor = DXSkinColors.FillColors.Success;

                            }
                        }
                        };
                    ReportForm.gridView1.CustomColumnDisplayText += (rs, re) =>
                    {
                        if(re.ListSourceRowIndex<0)return ;
                        if (re.Column.FieldName == "Type")
                            re.DisplayText =
                            MasterClass.SystemProssessCaption[ReportForm.gridView1.GetRowCellValue(ReportForm.gridView1.GetRowHandle( re.ListSourceRowIndex ),"Type").ToInt()];
                    };
                    
                    ReportForm.btn_Refresh.PerformClick();
                    ReportForm.Text = ReportForm.gridView1.ViewCaption = "حركه الاصناف بالمخازن";
                    ReportForm.gridView1.Columns["ProductName"].Caption = "اسم الصنف";
                    ReportForm.gridView1.Columns["StoreName"].Caption = "المخزن";
                    ReportForm.gridView1.Columns["Date"].Caption = "التاريخ";
                    ReportForm.gridView1.Columns["ItemQuIN"].Caption = " وارد ";
                    ReportForm.gridView1.Columns["ItemQuOut"].Caption = "منصرف";
                    ReportForm.gridView1.Columns["BalanceToDate"].Caption = "الرصيد حتي التاريخ";
                    ReportForm.gridView1.Columns["TotalCost"].Caption = "اجمالي التكلفه";
                    ReportForm.gridView1.Columns["TotalPrice"].Caption = "اجمالي السعر";
                    ReportForm.gridView1.Columns["BuyPrice"].Caption = "سعر التكلفه";
                    ReportForm.gridView1.Columns["SellPrice"].Caption = "السعر";
                    ReportForm.gridView1.Columns["Serial"].Caption = "السريال";
                    ReportForm.gridView1.Columns["Color"].Caption = "اللون";
                    ReportForm.gridView1.Columns["Size"].Caption = "الحجم";
                    ReportForm.gridView1.Columns["ExpDate"].Caption = "الصلاحيه";
                    ReportForm.gridView1.Columns["Note"].Caption = "البيان";
                    ReportForm.gridView1.Columns["Type"].Caption = "المصدر";
                    ReportForm.gridView1.Columns["TypeID"].Caption = "كود المصدر";

                    GridColumnSummaryItem BuyPriceAvg = new GridColumnSummaryItem();
                    BuyPriceAvg.SummaryType = SummaryItemType.Average;
                    BuyPriceAvg.FieldName = "BuyPrice";
                    BuyPriceAvg.DisplayFormat = "متوسط التكلفه: {0:#.#}";
                    ReportForm.gridView1.Columns["BuyPrice"].Summary.Clear();
                    ReportForm.gridView1.Columns["BuyPrice"].Summary.Add(BuyPriceAvg);
                    GridColumnSummaryItem SellPriceAvg = new GridColumnSummaryItem();
                    SellPriceAvg.SummaryType = SummaryItemType.Average;
                    SellPriceAvg.FieldName = "SellPrice";
                    SellPriceAvg.DisplayFormat = " متوسط السعر: {0:#.#}";
                    ReportForm.gridView1.Columns["SellPrice"].Summary.Clear();
                    ReportForm.gridView1.Columns["SellPrice"].Summary.Add(SellPriceAvg);
                    GridColumnSummaryItem TottalCostSum = new GridColumnSummaryItem();
                    TottalCostSum.SummaryType = SummaryItemType.Sum;
                    TottalCostSum.FieldName = "TotalCost";
                    TottalCostSum.DisplayFormat = "الاجمالي: {0:#.#}";
                    ReportForm.gridView1.Columns["TotalCost"].Summary.Clear();
                    ReportForm.gridView1.Columns["TotalCost"].Summary.Add(TottalCostSum);
                    GridColumnSummaryItem TotalPriceSum = new GridColumnSummaryItem();
                    TotalPriceSum.SummaryType = SummaryItemType.Sum;
                    TotalPriceSum.FieldName = "TotalPrice";
                    TotalPriceSum.DisplayFormat = "الاجمالي: {0:#.#}";
                    ReportForm.gridView1.Columns["TotalPrice"].Summary.Clear();
                    ReportForm.gridView1.Columns["TotalPrice"].Summary.Add(TotalPriceSum); 
                    break;
                #endregion

                #region ProductBalance
                case ReportType.ProductBalance:
                    ReportForm.btn_Refresh.ItemClick += (bs, be) =>
                    {
                        var source = db.Inv_StoreLogs.Select(x => x);
                        if (products.Count > 0)
                            source = source.Where(x => products.Contains(x.ItemID.Value));
                        if (stores.Count > 0)
                            source = source.Where(x => stores.Contains(x.StoreID.Value));
                        if (lyc_EndDate.Visible)
                            source = source.Where(x => x.date.Value.Date <= dt_End .DateTime.Date);
                        if (lyc_StartDate.Visible)
                            source = source.Where(x => x.date.Value.Date >= dt_Start.DateTime.Date);
                        if (lyc_OnDate.Visible)
                            source = source.Where(x => x.date.Value.Date == dt_OnDate.DateTime.Date);
                        ReportForm.gridControl1.DataSource = source.OrderBy(isl => isl.date).GroupBy(isl => new { isl.ItemID, isl.Color,isl.Size ,isl.StoreID  })
                        .Select(isl => new {
                            ProductName = db.Prodecuts.Single(x => x.ProdecutID ==isl.Key.ItemID).ProdecutName  ,
                            StoreName = db.Stores.Single(x => x.StoreID == isl.Key.StoreID.Value ).StoreName,
                            SellPrice = db.Prodecuts.Single(x => x.ProdecutID == isl.Key.ItemID).FiestUnitPrice1 ,
                            TotalPrice = (isl.Sum(i => i.ItemQuIN) - isl.Sum(i => i.ItemQuOut) )* db.Prodecuts.Single(x => x.ProdecutID == isl.Key.ItemID).FiestUnitPrice1,
                            Color = db.Inv_Colors.SingleOrDefault(x => x.ID == isl.Key.Color).Name ?? "",
                            Size = db.Inv_Sizes.SingleOrDefault(x => x.ID == isl.Key.Size ).Name ?? "", 
                            Balance = isl.Sum(i => i.ItemQuIN) - isl.Sum(i => i.ItemQuOut) ,
                            Date = dt_End.DateTime.Date
                        }).ToList();
                    };
                    ReportForm.gridView1.RowCellStyle += (rs, re) => {
                        if (re.RowHandle < 0) return; 
                        else if (re.Column.FieldName == "Balance")
                        {
                            if (re.CellValue is double d && d < 0)
                            {
                                re.Appearance.BackColor = DXSkinColors.FillColors.Warning;

                            }
                            else if (re.CellValue is double dd && dd > 0)
                            {
                                re.Appearance.BackColor = DXSkinColors.FillColors.Success;

                            }
                        }
                    }; 
                    ReportForm.btn_Refresh.PerformClick();
                    ReportForm.Text = ReportForm.gridView1.ViewCaption = "ارصده الاصناف بالمخازن";
                    ReportForm.gridView1.Columns["ProductName"].Caption = "اسم الصنف";
                    ReportForm.gridView1.Columns["StoreName"].Caption = "المخزن";
                    ReportForm.gridView1.Columns["Date"].Caption = "التاريخ"; 
                    ReportForm.gridView1.Columns["Balance"].Caption = "الرصيد ";
                    ReportForm.gridView1.Columns["TotalPrice"].Caption = "اجمالي السعر";
                    ReportForm.gridView1.Columns["SellPrice"].Caption = "السعر";
                    ReportForm.gridView1.Columns["Color"].Caption = "اللون";
                    ReportForm.gridView1.Columns["Size"].Caption = "الحجم";

                    GridColumnSummaryItem BalanceSum = new GridColumnSummaryItem();
                    BalanceSum.SummaryType = SummaryItemType.Average;
                    BalanceSum.FieldName = "Balance";
                    BalanceSum.DisplayFormat = "الاجمالي: {0:#.#}";
                    ReportForm.gridView1.Columns["Balance"].Summary.Clear();
                    ReportForm.gridView1.Columns["Balance"].Summary.Add(BalanceSum); 
                    TotalPriceSum = new GridColumnSummaryItem();
                    TotalPriceSum.SummaryType = SummaryItemType.Sum;
                    TotalPriceSum.FieldName = "TotalPrice";
                    TotalPriceSum.DisplayFormat = "الاجمالي: {0:#.#}";
                    ReportForm.gridView1.Columns["TotalPrice"].Summary.Clear();
                    ReportForm.gridView1.Columns["TotalPrice"].Summary.Add(TotalPriceSum);
                    break;
                #endregion

                #region ProductExpire
                case ReportType.ProductExpire :
                    ReportForm.btn_Refresh.ItemClick += (bs, be) =>
                    {
                        var source = db.Inv_StoreLogs.Select(x => x);
                        if (products.Count > 0)
                            source = source.Where(x => products.Contains(x.ItemID.Value));
                        if (stores.Count > 0)
                            source = source.Where(x => stores.Contains(x.StoreID.Value));
                        source = source.Where(s => s.ExpDate.HasValue);
                        ReportForm.gridControl1.DataSource = source.OrderBy(isl => isl.date).GroupBy(isl => new { isl.ItemID,ExpireDate = isl.ExpDate .Value.Date , isl.StoreID })
                        .Select(isl => new {
                            ProductName = db.Prodecuts.Single(x => x.ProdecutID == isl.Key.ItemID).ProdecutName,
                            StoreName = db.Stores.Single(x => x.StoreID == isl.Key.StoreID.Value).StoreName,
                            isl.Key.ExpireDate,
                            Balance = isl.Sum(i => i.ItemQuIN) - isl.Sum(i => i.ItemQuOut),
                        }).ToList();
                    };
                    ReportForm.gridView1.RowCellStyle += (rs, re) => {
                        if (re.RowHandle < 0) return;
                        var view = rs as GridView;
                        if (view == null) return;

                        var ExpireDate = view.GetRowCellValue(re.RowHandle, view.Columns["ExpireDate"]) as DateTime?;
                        if (ExpireDate.HasValue == false) return;

                        var Balance = view.GetRowCellValue(re.RowHandle, view.Columns["Balance"]).ToInt();

                        if(Balance == 0)
                        {
                            re.Appearance.BackColor = Color.LightGray ;

                        }else if (Balance < 0)
                        {
                            re.Appearance.BackColor = DXSkinColors.FillColors.Question ;

                        }
                        else
                        {
                            if (ExpireDate.Value  <= DateTime.Now)
                                re.Appearance.BackColor = DXSkinColors.FillColors.Danger;
                            else if (ExpireDate.Value.AddDays(-15) <= DateTime.Now)
                                re.Appearance.BackColor = System.Drawing.Color.DarkOrange;
                            else if (ExpireDate.Value.AddDays(-30) <= DateTime.Now)
                                re.Appearance.BackColor = System.Drawing.Color.Orange;
                            else if (ExpireDate.Value.AddDays(-45) <= DateTime.Now)
                                re.Appearance.BackColor = System.Drawing.Color.Yellow;
                            else if (ExpireDate.Value.AddDays(-60) <= DateTime.Now)
                                re.Appearance.BackColor = System.Drawing.Color.LightYellow;
                        }
                        
                    };
                    ReportForm.btn_Refresh.PerformClick();
                    ReportForm.Text = ReportForm.gridView1.ViewCaption = "صلاحيات الاصناف بالمخازن";
                    ReportForm.gridView1.Columns["ProductName"].Caption = "اسم الصنف";
                    ReportForm.gridView1.Columns["StoreName"].Caption = "المخزن";
                    ReportForm.gridView1.Columns["Balance"].Caption = "الرصيد ";
                    ReportForm.gridView1.Columns["ExpireDate"].Caption = "الصلاحيه";

                    //GridColumnSummaryItem BalanceSum = new GridColumnSummaryItem();
                    //BalanceSum.SummaryType = SummaryItemType.Average;
                    //BalanceSum.FieldName = "Balance";
                    //BalanceSum.DisplayFormat = "الاجمالي: {0:#.#}";
                    //ReportForm.gridView1.Columns["Balance"].Summary.Clear();
                    //ReportForm.gridView1.Columns["Balance"].Summary.Add(BalanceSum);
                    //TotalPriceSum = new GridColumnSummaryItem();
                    //TotalPriceSum.SummaryType = SummaryItemType.Sum;
                    //TotalPriceSum.FieldName = "TotalPrice";
                    //TotalPriceSum.DisplayFormat = "الاجمالي: {0:#.#}";
                    //ReportForm.gridView1.Columns["TotalPrice"].Summary.Clear();
                    //ReportForm.gridView1.Columns["TotalPrice"].Summary.Add(TotalPriceSum);
                    break;
                #endregion
                #region ProductReachedReorderLevel
                case ReportType.ProductReachedReorderLevel:
                    ReportForm.btn_Refresh.ItemClick += (bs, be) =>
                    {
                        var source = db.Inv_StoreLogs.Select(x => x);
                        if (products.Count > 0)
                            source = source.Where(x => products.Contains(x.ItemID.Value));
                        if (stores.Count > 0)
                            source = source.Where(x => stores.Contains(x.StoreID.Value));
                        if (lyc_EndDate.Visible)
                            source = source.Where(x => x.date.Value.Date <= dt_End.DateTime.Date);
                        if (lyc_StartDate.Visible)
                            source = source.Where(x => x.date.Value.Date >= dt_Start.DateTime.Date);
                        if (lyc_OnDate.Visible)
                            source = source.Where(x => x.date.Value.Date == dt_OnDate.DateTime.Date);
                        ReportForm.gridControl1.DataSource = source.OrderBy(isl => isl.date).GroupBy(isl => new { isl.ItemID, isl.Color, isl.Size, isl.StoreID })
                        .Select(isl => new {
                            ProductID = isl.Key.ItemID,
                            ProductName = db.Prodecuts.Single(x => x.ProdecutID == isl.Key.ItemID).ProdecutName,
                            StoreName = db.Stores.Single(x => x.StoreID == isl.Key.StoreID.Value).StoreName,
                            SellPrice = db.Prodecuts.Single(x => x.ProdecutID == isl.Key.ItemID).FiestUnitPrice1,
                            TotalPrice = (isl.Sum(i => i.ItemQuIN) - isl.Sum(i => i.ItemQuOut)) * db.Prodecuts.Single(x => x.ProdecutID == isl.Key.ItemID).FiestUnitPrice1,
                            Color = db.Inv_Colors.SingleOrDefault(x => x.ID == isl.Key.Color).Name ?? "",
                            Size = db.Inv_Sizes.SingleOrDefault(x => x.ID == isl.Key.Size).Name ?? "",
                            Balance = isl.Sum(i => i.ItemQuIN) - isl.Sum(i => i.ItemQuOut),
                            ReOrderLevel = (db.Prodecuts.Single(p=>p.ProdecutID == isl.Key.ItemID).RequestLimit??0),
                            Date = dt_End.DateTime.Date
                        }).Where(x=> x.Balance <=  x.ReOrderLevel).ToList();
                    };
                    ReportForm.gridView1.RowCellStyle += (rs, re) => {
                        if (re.RowHandle < 0) return;
                        else if (re.Column.FieldName == "Balance")
                        {
                            if (re.CellValue is double d && d < 0)
                            {
                                re.Appearance.BackColor = DXSkinColors.FillColors.Warning;

                            }
                            else if (re.CellValue is double dd && dd > 0)
                            {
                                re.Appearance.BackColor = DXSkinColors.FillColors.Success;

                            }
                        }
                    };
                    ReportForm.btn_Refresh.PerformClick();
                    ReportForm.Text = ReportForm.gridView1.ViewCaption = "اصناف وصلت الي حد الطلب";
                    ReportForm.gridView1.Columns["ProductName"].Caption = "اسم الصنف"; 
                    ReportForm.gridView1.Columns["StoreName"].Caption = "المخزن";
                    ReportForm.gridView1.Columns["Date"].Caption = "التاريخ";
                    ReportForm.gridView1.Columns["Balance"].Caption = "الرصيد ";
                    ReportForm.gridView1.Columns["ReOrderLevel"].Caption = "حد الطلب ";
                    ReportForm.gridView1.Columns["TotalPrice"].Caption = "اجمالي السعر";
                    ReportForm.gridView1.Columns["SellPrice"].Caption = "السعر";
                    ReportForm.gridView1.Columns["Color"].Caption = "اللون";
                    ReportForm.gridView1.Columns["Size"].Caption = "الحجم";

                    //GridColumnSummaryItem BalanceSum = new GridColumnSummaryItem();
                    //BalanceSum.SummaryType = SummaryItemType.Average;
                    //BalanceSum.FieldName = "Balance";
                    //BalanceSum.DisplayFormat = "الاجمالي: {0:#.#}";
                    //ReportForm.gridView1.Columns["Balance"].Summary.Clear();
                    //ReportForm.gridView1.Columns["Balance"].Summary.Add(BalanceSum);
                    //TotalPriceSum = new GridColumnSummaryItem();
                    //TotalPriceSum.SummaryType = SummaryItemType.Sum;
                    //TotalPriceSum.FieldName = "TotalPrice";
                    //TotalPriceSum.DisplayFormat = "الاجمالي: {0:#.#}";
                    //ReportForm.gridView1.Columns["TotalPrice"].Summary.Clear();
                    //ReportForm.gridView1.Columns["TotalPrice"].Summary.Add(TotalPriceSum);
                    break;
                #endregion

                default:
                    break;
            }
            ReportForm.FilterString = filterString;
            ReportForm.Show();
        }
        
        private void cb_DateFilterType_SelectedIndexChanged(object sender, EventArgs e)
        {
            lyc_OnDate.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            lyc_EndDate.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            lyc_StartDate.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;

            if (cb_DateFilterType.SelectedIndex  == 0)
            {
                lyc_EndDate.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                lyc_StartDate.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always; 
            }
            else if (cb_DateFilterType.SelectedIndex == 1)
                lyc_StartDate.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always; 
            else if (cb_DateFilterType.SelectedIndex == 2)
                lyc_EndDate.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always ; 
            else if (cb_DateFilterType.SelectedIndex == 3)
                lyc_OnDate .Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;

            
        }
    }
      
}
