﻿namespace ByStro.Forms
{
    partial class frm_Inv_Invoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.lkp_ToStore = new DevExpress.XtraEditors.LookUpEdit();
            this.spn_Remains = new DevExpress.XtraEditors.SpinEdit();
            this.memoEdit_ShipTo = new DevExpress.XtraEditors.MemoEdit();
            this.spn_Net = new DevExpress.XtraEditors.SpinEdit();
            this.spn_Discount = new DevExpress.XtraEditors.SpinEdit();
            this.spn_Paid = new DevExpress.XtraEditors.SpinEdit();
            this.spn_DiscountVal = new DevExpress.XtraEditors.SpinEdit();
            this.GridControl_Items = new DevExpress.XtraGrid.GridControl();
            this.GridView_Items = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.spn_Total = new DevExpress.XtraEditors.SpinEdit();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.spn_TotalRevenue = new DevExpress.XtraEditors.SpinEdit();
            this.txt_LastUpdate = new DevExpress.XtraEditors.TextEdit();
            this.txt_UpdateUser = new DevExpress.XtraEditors.TextEdit();
            this.LookUpEdit_SourceID = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.txt_InsertUser = new DevExpress.XtraEditors.TextEdit();
            this.txt_ID = new DevExpress.XtraEditors.TextEdit();
            this.lkp_Store = new DevExpress.XtraEditors.LookUpEdit();
            this.LookUpEdit_PartID = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.txt_Code = new DevExpress.XtraEditors.TextEdit();
            this.txt_Attn = new DevExpress.XtraEditors.TextEdit();
            this.dt_Date = new DevExpress.XtraEditors.DateEdit();
            this.Button_ShowPayScreen = new DevExpress.XtraEditors.SimpleButton();
            this.LookUpEdit_PartType = new DevExpress.XtraEditors.LookUpEdit();
            this.radioGroup1 = new DevExpress.XtraEditors.RadioGroup();
            this.gridLookUpEdit1 = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit2View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit1 = new DevExpress.XtraEditors.ButtonEdit();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lyg_Totals = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lyc_Remains = new DevExpress.XtraLayout.LayoutControlItem();
            this.lyc_Net = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lyc_Paid = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lyc_Profit = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lyc_PartID = new DevExpress.XtraLayout.LayoutControlItem();
            this.lyc_Attintion = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem53 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lyc_Source = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lyc_PriceLevel = new DevExpress.XtraLayout.LayoutControlItem();
            this.lyc_ToStore = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lyc_Shiping = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lkp_ToStore.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Remains.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_ShipTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Net.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Discount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Paid.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_DiscountVal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_Items)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_Items)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Total.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_TotalRevenue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_LastUpdate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_UpdateUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_SourceID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_InsertUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp_Store.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_PartID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Attn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_Date.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_Date.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_PartType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit2View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyg_Totals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_Remains)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_Net)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_Paid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_Profit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_PartID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_Attintion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_Source)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_PriceLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_ToStore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_Shiping)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.lkp_ToStore);
            this.layoutControl1.Controls.Add(this.spn_Remains);
            this.layoutControl1.Controls.Add(this.memoEdit_ShipTo);
            this.layoutControl1.Controls.Add(this.spn_Net);
            this.layoutControl1.Controls.Add(this.spn_Discount);
            this.layoutControl1.Controls.Add(this.spn_Paid);
            this.layoutControl1.Controls.Add(this.spn_DiscountVal);
            this.layoutControl1.Controls.Add(this.GridControl_Items);
            this.layoutControl1.Controls.Add(this.spn_Total);
            this.layoutControl1.Controls.Add(this.memoEdit1);
            this.layoutControl1.Controls.Add(this.spn_TotalRevenue);
            this.layoutControl1.Controls.Add(this.txt_LastUpdate);
            this.layoutControl1.Controls.Add(this.txt_UpdateUser);
            this.layoutControl1.Controls.Add(this.LookUpEdit_SourceID);
            this.layoutControl1.Controls.Add(this.txt_InsertUser);
            this.layoutControl1.Controls.Add(this.txt_ID);
            this.layoutControl1.Controls.Add(this.lkp_Store);
            this.layoutControl1.Controls.Add(this.LookUpEdit_PartID);
            this.layoutControl1.Controls.Add(this.txt_Code);
            this.layoutControl1.Controls.Add(this.txt_Attn);
            this.layoutControl1.Controls.Add(this.dt_Date);
            this.layoutControl1.Controls.Add(this.Button_ShowPayScreen);
            this.layoutControl1.Controls.Add(this.LookUpEdit_PartType);
            this.layoutControl1.Controls.Add(this.radioGroup1);
            this.layoutControl1.Controls.Add(this.gridLookUpEdit1);
            this.layoutControl1.Controls.Add(this.simpleButton1);
            this.layoutControl1.Controls.Add(this.textEdit1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem20});
            this.layoutControl1.Location = new System.Drawing.Point(0, 24);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsView.RightToLeftMirroringApplied = true;
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(1353, 745);
            this.layoutControl1.TabIndex = 17;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // lkp_ToStore
            // 
            this.lkp_ToStore.Location = new System.Drawing.Point(1037, 165);
            this.lkp_ToStore.Name = "lkp_ToStore";
            this.lkp_ToStore.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkp_ToStore.Properties.NullText = "";
            this.lkp_ToStore.Size = new System.Drawing.Size(206, 20);
            this.lkp_ToStore.StyleController = this.layoutControl1;
            this.lkp_ToStore.TabIndex = 45;
            // 
            // spn_Remains
            // 
            this.spn_Remains.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.spn_Remains.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spn_Remains.Location = new System.Drawing.Point(24, 677);
            this.spn_Remains.Name = "spn_Remains";
            this.spn_Remains.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spn_Remains.Properties.ReadOnly = true;
            this.spn_Remains.Size = new System.Drawing.Size(205, 20);
            this.spn_Remains.StyleController = this.layoutControl1;
            this.spn_Remains.TabIndex = 19;
            // 
            // memoEdit_ShipTo
            // 
            this.memoEdit_ShipTo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEdit_ShipTo.EditValue = "";
            this.memoEdit_ShipTo.Location = new System.Drawing.Point(677, 584);
            this.memoEdit_ShipTo.Name = "memoEdit_ShipTo";
            this.memoEdit_ShipTo.Size = new System.Drawing.Size(246, 137);
            this.memoEdit_ShipTo.StyleController = this.layoutControl1;
            this.memoEdit_ShipTo.TabIndex = 15;
            // 
            // spn_Net
            // 
            this.spn_Net.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.spn_Net.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spn_Net.Location = new System.Drawing.Point(24, 627);
            this.spn_Net.Name = "spn_Net";
            this.spn_Net.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spn_Net.Properties.ReadOnly = true;
            this.spn_Net.Size = new System.Drawing.Size(205, 20);
            this.spn_Net.StyleController = this.layoutControl1;
            this.spn_Net.TabIndex = 19;
            // 
            // spn_Discount
            // 
            this.spn_Discount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.spn_Discount.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            262144});
            this.spn_Discount.Location = new System.Drawing.Point(78, 603);
            this.spn_Discount.Name = "spn_Discount";
            this.spn_Discount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spn_Discount.Size = new System.Drawing.Size(151, 20);
            this.spn_Discount.StyleController = this.layoutControl1;
            this.spn_Discount.TabIndex = 1;
            // 
            // spn_Paid
            // 
            this.spn_Paid.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.spn_Paid.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spn_Paid.Location = new System.Drawing.Point(78, 651);
            this.spn_Paid.Name = "spn_Paid";
            this.spn_Paid.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spn_Paid.Size = new System.Drawing.Size(151, 20);
            this.spn_Paid.StyleController = this.layoutControl1;
            this.spn_Paid.TabIndex = 7;
            // 
            // spn_DiscountVal
            // 
            this.spn_DiscountVal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.spn_DiscountVal.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spn_DiscountVal.Location = new System.Drawing.Point(24, 603);
            this.spn_DiscountVal.Name = "spn_DiscountVal";
            this.spn_DiscountVal.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spn_DiscountVal.Size = new System.Drawing.Size(50, 20);
            this.spn_DiscountVal.StyleController = this.layoutControl1;
            this.spn_DiscountVal.TabIndex = 0;
            // 
            // GridControl_Items
            // 
            this.GridControl_Items.Location = new System.Drawing.Point(15, 36);
            this.GridControl_Items.MainView = this.GridView_Items;
            this.GridControl_Items.Name = "GridControl_Items";
            this.GridControl_Items.Size = new System.Drawing.Size(1003, 479);
            this.GridControl_Items.TabIndex = 0;
            this.GridControl_Items.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GridView_Items});
            // 
            // GridView_Items
            // 
            this.GridView_Items.ColumnPanelRowHeight = 35;
            this.GridView_Items.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.GridView_Items.GridControl = this.GridControl_Items;
            this.GridView_Items.Name = "GridView_Items";
            this.GridView_Items.OptionsBehavior.FocusLeaveOnTab = true;
            this.GridView_Items.OptionsBehavior.KeepFocusedRowOnUpdate = false;
            this.GridView_Items.OptionsMenu.ShowConditionalFormattingItem = true;
            this.GridView_Items.OptionsNavigation.AutoFocusNewRow = true;
            this.GridView_Items.OptionsPrint.ExpandAllDetails = true;
            this.GridView_Items.OptionsView.ColumnAutoWidth = false;
            this.GridView_Items.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.GridView_Items.OptionsView.ShowFooter = true;
            this.GridView_Items.OptionsView.ShowGroupPanel = false;
            // 
            // spn_Total
            // 
            this.spn_Total.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.spn_Total.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spn_Total.Location = new System.Drawing.Point(24, 555);
            this.spn_Total.Name = "spn_Total";
            this.spn_Total.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spn_Total.Properties.ReadOnly = true;
            this.spn_Total.Size = new System.Drawing.Size(205, 20);
            this.spn_Total.StyleController = this.layoutControl1;
            this.spn_Total.TabIndex = 19;
            // 
            // memoEdit1
            // 
            this.memoEdit1.EditValue = "";
            this.memoEdit1.Location = new System.Drawing.Point(1037, 320);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(292, 371);
            this.memoEdit1.StyleController = this.layoutControl1;
            this.memoEdit1.TabIndex = 0;
            // 
            // spn_TotalRevenue
            // 
            this.spn_TotalRevenue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.spn_TotalRevenue.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spn_TotalRevenue.Location = new System.Drawing.Point(24, 579);
            this.spn_TotalRevenue.Name = "spn_TotalRevenue";
            this.spn_TotalRevenue.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spn_TotalRevenue.Size = new System.Drawing.Size(205, 20);
            this.spn_TotalRevenue.StyleController = this.layoutControl1;
            this.spn_TotalRevenue.TabIndex = 19;
            this.spn_TotalRevenue.EditValueChanged += new System.EventHandler(this.spn_TotalRevenue_EditValueChanged);
            // 
            // txt_LastUpdate
            // 
            this.txt_LastUpdate.Location = new System.Drawing.Point(1025, 755);
            this.txt_LastUpdate.Name = "txt_LastUpdate";
            this.txt_LastUpdate.Properties.ReadOnly = true;
            this.txt_LastUpdate.Size = new System.Drawing.Size(206, 20);
            this.txt_LastUpdate.StyleController = this.layoutControl1;
            this.txt_LastUpdate.TabIndex = 2;
            // 
            // txt_UpdateUser
            // 
            this.txt_UpdateUser.Location = new System.Drawing.Point(1025, 731);
            this.txt_UpdateUser.Name = "txt_UpdateUser";
            this.txt_UpdateUser.Properties.ReadOnly = true;
            this.txt_UpdateUser.Size = new System.Drawing.Size(206, 20);
            this.txt_UpdateUser.StyleController = this.layoutControl1;
            this.txt_UpdateUser.TabIndex = 1;
            // 
            // LookUpEdit_SourceID
            // 
            this.LookUpEdit_SourceID.Location = new System.Drawing.Point(1037, 213);
            this.LookUpEdit_SourceID.Name = "LookUpEdit_SourceID";
            this.LookUpEdit_SourceID.Properties.ActionButtonIndex = 1;
            this.LookUpEdit_SourceID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Search, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "Open", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_SourceID.Properties.NullText = "";
            this.LookUpEdit_SourceID.Properties.PopupView = this.gridView1;
            this.LookUpEdit_SourceID.Properties.ReadOnly = true;
            this.LookUpEdit_SourceID.Properties.UseReadOnlyAppearance = false;
            this.LookUpEdit_SourceID.Properties.Click += new System.EventHandler(this.LookUpEdit_SourceID_Properties_Click);
            this.LookUpEdit_SourceID.Size = new System.Drawing.Size(206, 20);
            this.LookUpEdit_SourceID.StyleController = this.layoutControl1;
            this.LookUpEdit_SourceID.TabIndex = 0;
            this.LookUpEdit_SourceID.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.LookUpEdit_SourceID_ButtonClick);
            this.LookUpEdit_SourceID.EditValueChanged += new System.EventHandler(this.LookUpEdit_SourceID_EditValueChanged);
            // 
            // gridView1
            // 
            this.gridView1.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridView1.Appearance.Row.Options.UseTextOptions = true;
            this.gridView1.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsBehavior.AutoSelectAllInEditor = false;
            this.gridView1.OptionsBehavior.AutoUpdateTotalSummary = false;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.BestFitMaxRowCount = 10;
            this.gridView1.OptionsView.ShowAutoFilterRow = true;
            this.gridView1.OptionsView.ShowDetailButtons = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // txt_InsertUser
            // 
            this.txt_InsertUser.Location = new System.Drawing.Point(1025, 707);
            this.txt_InsertUser.Name = "txt_InsertUser";
            this.txt_InsertUser.Properties.ReadOnly = true;
            this.txt_InsertUser.Size = new System.Drawing.Size(206, 20);
            this.txt_InsertUser.StyleController = this.layoutControl1;
            this.txt_InsertUser.TabIndex = 0;
            // 
            // txt_ID
            // 
            this.txt_ID.Location = new System.Drawing.Point(1187, 45);
            this.txt_ID.Name = "txt_ID";
            this.txt_ID.Properties.ReadOnly = true;
            this.txt_ID.Size = new System.Drawing.Size(56, 20);
            this.txt_ID.StyleController = this.layoutControl1;
            this.txt_ID.TabIndex = 22;
            // 
            // lkp_Store
            // 
            this.lkp_Store.Location = new System.Drawing.Point(1037, 141);
            this.lkp_Store.Name = "lkp_Store";
            this.lkp_Store.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkp_Store.Properties.NullText = "";
            this.lkp_Store.Size = new System.Drawing.Size(206, 20);
            this.lkp_Store.StyleController = this.layoutControl1;
            this.lkp_Store.TabIndex = 3;
            // 
            // LookUpEdit_PartID
            // 
            this.LookUpEdit_PartID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LookUpEdit_PartID.EditValue = "";
            this.LookUpEdit_PartID.Location = new System.Drawing.Point(1037, 93);
            this.LookUpEdit_PartID.Name = "LookUpEdit_PartID";
            this.LookUpEdit_PartID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", "Add", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.LookUpEdit_PartID.Properties.NullText = "";
            this.LookUpEdit_PartID.Properties.PopupView = this.gridLookUpEdit1View;
            this.LookUpEdit_PartID.Size = new System.Drawing.Size(206, 20);
            this.LookUpEdit_PartID.StyleController = this.layoutControl1;
            this.LookUpEdit_PartID.TabIndex = 1;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridLookUpEdit1View.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridLookUpEdit1View.Appearance.Row.Options.UseTextOptions = true;
            this.gridLookUpEdit1View.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsBehavior.AutoSelectAllInEditor = false;
            this.gridLookUpEdit1View.OptionsBehavior.AutoUpdateTotalSummary = false;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.BestFitMaxRowCount = 10;
            this.gridLookUpEdit1View.OptionsView.ShowAutoFilterRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowDetailButtons = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // txt_Code
            // 
            this.txt_Code.Location = new System.Drawing.Point(1037, 45);
            this.txt_Code.Name = "txt_Code";
            this.txt_Code.Size = new System.Drawing.Size(126, 20);
            this.txt_Code.StyleController = this.layoutControl1;
            this.txt_Code.TabIndex = 0;
            // 
            // txt_Attn
            // 
            this.txt_Attn.Location = new System.Drawing.Point(1037, 117);
            this.txt_Attn.Name = "txt_Attn";
            this.txt_Attn.Size = new System.Drawing.Size(206, 20);
            this.txt_Attn.StyleController = this.layoutControl1;
            this.txt_Attn.TabIndex = 2;
            // 
            // dt_Date
            // 
            this.dt_Date.EditValue = null;
            this.dt_Date.Location = new System.Drawing.Point(1037, 189);
            this.dt_Date.Name = "dt_Date";
            this.dt_Date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dt_Date.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dt_Date.Size = new System.Drawing.Size(206, 20);
            this.dt_Date.StyleController = this.layoutControl1;
            this.dt_Date.TabIndex = 34;
            // 
            // Button_ShowPayScreen
            // 
            this.Button_ShowPayScreen.Appearance.Options.UseTextOptions = true;
            this.Button_ShowPayScreen.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Button_ShowPayScreen.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Button_ShowPayScreen.ImageOptions.SvgImageSize = new System.Drawing.Size(16, 16);
            this.Button_ShowPayScreen.Location = new System.Drawing.Point(24, 650);
            this.Button_ShowPayScreen.Name = "Button_ShowPayScreen";
            this.Button_ShowPayScreen.Size = new System.Drawing.Size(121, 22);
            this.Button_ShowPayScreen.StyleController = this.layoutControl1;
            this.Button_ShowPayScreen.TabIndex = 38;
            this.Button_ShowPayScreen.Text = "شاشه الدفع F1\r\n ";
            // 
            // LookUpEdit_PartType
            // 
            this.LookUpEdit_PartType.Location = new System.Drawing.Point(1037, 69);
            this.LookUpEdit_PartType.Name = "LookUpEdit_PartType";
            this.LookUpEdit_PartType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_PartType.Properties.NullText = "";
            this.LookUpEdit_PartType.Properties.PopupSizeable = false;
            this.LookUpEdit_PartType.Size = new System.Drawing.Size(206, 20);
            this.LookUpEdit_PartType.StyleController = this.layoutControl1;
            this.LookUpEdit_PartType.TabIndex = 41;
            // 
            // radioGroup1
            // 
            this.radioGroup1.Location = new System.Drawing.Point(1037, 237);
            this.radioGroup1.MenuManager = this.barManager1;
            this.radioGroup1.Name = "radioGroup1";
            this.radioGroup1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "سعر 1"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "سعر 2"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "سعر 3")});
            this.radioGroup1.Size = new System.Drawing.Size(206, 34);
            this.radioGroup1.StyleController = this.layoutControl1;
            this.radioGroup1.TabIndex = 42;
            // 
            // gridLookUpEdit1
            // 
            this.gridLookUpEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gridLookUpEdit1.Location = new System.Drawing.Point(677, 555);
            this.gridLookUpEdit1.Name = "gridLookUpEdit1";
            this.gridLookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.gridLookUpEdit1.Properties.NullText = "";
            this.gridLookUpEdit1.Properties.PopupView = this.gridLookUpEdit2View;
            this.gridLookUpEdit1.Size = new System.Drawing.Size(246, 20);
            this.gridLookUpEdit1.StyleController = this.layoutControl1;
            this.gridLookUpEdit1.TabIndex = 12;
            // 
            // gridLookUpEdit2View
            // 
            this.gridLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit2View.Name = "gridLookUpEdit2View";
            this.gridLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit2View.OptionsView.ShowGroupPanel = false;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(24, 651);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(50, 22);
            this.simpleButton1.StyleController = this.layoutControl1;
            this.simpleButton1.TabIndex = 43;
            this.simpleButton1.Text = "سداد F1";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(24, 701);
            this.textEdit1.MenuManager = this.barManager1;
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.textEdit1.Properties.ReadOnly = true;
            this.textEdit1.Size = new System.Drawing.Size(205, 20);
            this.textEdit1.StyleController = this.layoutControl1;
            this.textEdit1.TabIndex = 44;
            this.textEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.textEdit1_ButtonClick);
            this.textEdit1.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.textEdit1_ButtonPressed);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.Button_ShowPayScreen;
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 98);
            this.layoutControlItem20.MaxSize = new System.Drawing.Size(125, 26);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(125, 26);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(125, 26);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextVisible = false;
            // 
            // Root
            // 
            this.Root.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.Root.AppearanceGroup.Options.UseFont = true;
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup12,
            this.lyg_Totals,
            this.layoutControlGroup1,
            this.layoutControlGroup4,
            this.lyc_Shiping,
            this.layoutControlGroup3});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(1353, 745);
            this.Root.TextVisible = false;
            // 
            // layoutControlGroup12
            // 
            this.layoutControlGroup12.AppearanceGroup.BorderColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Primary;
            this.layoutControlGroup12.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup12.AppearanceGroup.Options.UseBorderColor = true;
            this.layoutControlGroup12.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup12.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem16});
            this.layoutControlGroup12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup12.Name = "layoutControlGroup12";
            this.layoutControlGroup12.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup12.Size = new System.Drawing.Size(1013, 510);
            this.layoutControlGroup12.Text = "الاصناف";
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.GridControl_Items;
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(1007, 483);
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextVisible = false;
            // 
            // lyg_Totals
            // 
            this.lyg_Totals.AppearanceGroup.BorderColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            this.lyg_Totals.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lyg_Totals.AppearanceGroup.Options.UseBorderColor = true;
            this.lyg_Totals.AppearanceGroup.Options.UseFont = true;
            this.lyg_Totals.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.lyg_Totals.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lyc_Remains,
            this.lyc_Net,
            this.layoutControlItem33,
            this.layoutControlItem35,
            this.lyc_Paid,
            this.layoutControlItem40,
            this.layoutControlItem37,
            this.layoutControlItem7,
            this.lyc_Profit});
            this.lyg_Totals.Location = new System.Drawing.Point(0, 510);
            this.lyg_Totals.Name = "lyg_Totals";
            this.lyg_Totals.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.AlignLocal;
            this.lyg_Totals.Size = new System.Drawing.Size(288, 215);
            this.lyg_Totals.Text = "الاجماليات";
            // 
            // lyc_Remains
            // 
            this.lyc_Remains.Control = this.spn_Remains;
            this.lyc_Remains.Location = new System.Drawing.Point(0, 122);
            this.lyc_Remains.Name = "lyc_Remains";
            this.lyc_Remains.Size = new System.Drawing.Size(264, 24);
            this.lyc_Remains.Text = "المتبقي";
            this.lyc_Remains.TextSize = new System.Drawing.Size(52, 13);
            // 
            // lyc_Net
            // 
            this.lyc_Net.Control = this.spn_Net;
            this.lyc_Net.Location = new System.Drawing.Point(0, 72);
            this.lyc_Net.Name = "lyc_Net";
            this.lyc_Net.Size = new System.Drawing.Size(264, 24);
            this.lyc_Net.Text = "الصافي";
            this.lyc_Net.TextSize = new System.Drawing.Size(52, 13);
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.spn_Total;
            this.layoutControlItem33.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem33.MaxSize = new System.Drawing.Size(264, 24);
            this.layoutControlItem33.MinSize = new System.Drawing.Size(264, 24);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(264, 24);
            this.layoutControlItem33.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem33.Text = "الاجمالي";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(52, 13);
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this.spn_TotalRevenue;
            this.layoutControlItem35.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(264, 24);
            this.layoutControlItem35.Text = "مصروفات";
            this.layoutControlItem35.TextSize = new System.Drawing.Size(52, 13);
            // 
            // lyc_Paid
            // 
            this.lyc_Paid.Control = this.spn_Paid;
            this.lyc_Paid.Location = new System.Drawing.Point(54, 96);
            this.lyc_Paid.Name = "lyc_Paid";
            this.lyc_Paid.Size = new System.Drawing.Size(210, 26);
            this.lyc_Paid.Text = "المدفوع";
            this.lyc_Paid.TextSize = new System.Drawing.Size(52, 13);
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.Control = this.spn_Discount;
            this.layoutControlItem40.Location = new System.Drawing.Point(54, 48);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(210, 24);
            this.layoutControlItem40.Text = "خضم نقدي";
            this.layoutControlItem40.TextSize = new System.Drawing.Size(52, 13);
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.Control = this.spn_DiscountVal;
            this.layoutControlItem37.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(54, 24);
            this.layoutControlItem37.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem37.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.simpleButton1;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(54, 26);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // lyc_Profit
            // 
            this.lyc_Profit.Control = this.textEdit1;
            this.lyc_Profit.Location = new System.Drawing.Point(0, 146);
            this.lyc_Profit.Name = "lyc_Profit";
            this.lyc_Profit.Size = new System.Drawing.Size(264, 24);
            this.lyc_Profit.Text = "الربح";
            this.lyc_Profit.TextSize = new System.Drawing.Size(52, 13);
            this.lyc_Profit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.BorderColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Question;
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup1.AppearanceGroup.Options.UseBorderColor = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem1,
            this.lyc_PartID,
            this.lyc_Attintion,
            this.layoutControlItem5,
            this.layoutControlItem53,
            this.lyc_Source,
            this.layoutControlItem42,
            this.lyc_PriceLevel,
            this.lyc_ToStore});
            this.layoutControlGroup1.Location = new System.Drawing.Point(1013, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(320, 275);
            this.layoutControlGroup1.Text = "البيانات الاساسيه";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txt_Code;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "كود";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(15, 13);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txt_ID;
            this.layoutControlItem1.Location = new System.Drawing.Point(150, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(146, 24);
            this.layoutControlItem1.Text = "رقم";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(83, 13);
            // 
            // lyc_PartID
            // 
            this.lyc_PartID.Control = this.LookUpEdit_PartID;
            this.lyc_PartID.Location = new System.Drawing.Point(0, 48);
            this.lyc_PartID.MaxSize = new System.Drawing.Size(296, 24);
            this.lyc_PartID.MinSize = new System.Drawing.Size(296, 24);
            this.lyc_PartID.Name = "lyc_PartID";
            this.lyc_PartID.Size = new System.Drawing.Size(296, 24);
            this.lyc_PartID.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lyc_PartID.Text = ".......";
            this.lyc_PartID.TextSize = new System.Drawing.Size(83, 13);
            // 
            // lyc_Attintion
            // 
            this.lyc_Attintion.Control = this.txt_Attn;
            this.lyc_Attintion.Location = new System.Drawing.Point(0, 72);
            this.lyc_Attintion.Name = "lyc_Attintion";
            this.lyc_Attintion.Size = new System.Drawing.Size(296, 24);
            this.lyc_Attintion.Text = "عنايه";
            this.lyc_Attintion.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.lkp_Store;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(296, 24);
            this.layoutControlItem5.Text = "الفرع";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem53
            // 
            this.layoutControlItem53.Control = this.dt_Date;
            this.layoutControlItem53.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem53.Name = "layoutControlItem53";
            this.layoutControlItem53.Size = new System.Drawing.Size(296, 24);
            this.layoutControlItem53.Text = "التاريخ";
            this.layoutControlItem53.TextSize = new System.Drawing.Size(83, 13);
            // 
            // lyc_Source
            // 
            this.lyc_Source.Control = this.LookUpEdit_SourceID;
            this.lyc_Source.Location = new System.Drawing.Point(0, 168);
            this.lyc_Source.Name = "lyc_Source";
            this.lyc_Source.Size = new System.Drawing.Size(296, 24);
            this.lyc_Source.Text = "كود المصدر";
            this.lyc_Source.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.Control = this.LookUpEdit_PartType;
            this.layoutControlItem42.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(296, 24);
            this.layoutControlItem42.Text = "طرف التعامل";
            this.layoutControlItem42.TextSize = new System.Drawing.Size(83, 13);
            this.layoutControlItem42.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // lyc_PriceLevel
            // 
            this.lyc_PriceLevel.Control = this.radioGroup1;
            this.lyc_PriceLevel.Location = new System.Drawing.Point(0, 192);
            this.lyc_PriceLevel.MaxSize = new System.Drawing.Size(0, 38);
            this.lyc_PriceLevel.MinSize = new System.Drawing.Size(140, 38);
            this.lyc_PriceLevel.Name = "lyc_PriceLevel";
            this.lyc_PriceLevel.Size = new System.Drawing.Size(296, 38);
            this.lyc_PriceLevel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lyc_PriceLevel.Text = "مستوي الاسعار ";
            this.lyc_PriceLevel.TextSize = new System.Drawing.Size(83, 13);
            // 
            // lyc_ToStore
            // 
            this.lyc_ToStore.Control = this.lkp_ToStore;
            this.lyc_ToStore.Location = new System.Drawing.Point(0, 120);
            this.lyc_ToStore.Name = "lyc_ToStore";
            this.lyc_ToStore.Size = new System.Drawing.Size(296, 24);
            this.lyc_ToStore.Text = "الي مخزن";
            this.lyc_ToStore.TextSize = new System.Drawing.Size(83, 13);
            this.lyc_ToStore.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup4.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem15});
            this.layoutControlGroup4.Location = new System.Drawing.Point(1013, 275);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(320, 420);
            this.layoutControlGroup4.Text = "الملاحظات";
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.memoEdit1;
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(296, 375);
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextVisible = false;
            // 
            // lyc_Shiping
            // 
            this.lyc_Shiping.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lyc_Shiping.AppearanceGroup.Options.UseFont = true;
            this.lyc_Shiping.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem21,
            this.layoutControlItem19,
            this.emptySpaceItem1});
            this.lyc_Shiping.Location = new System.Drawing.Point(288, 510);
            this.lyc_Shiping.Name = "lyc_Shiping";
            this.lyc_Shiping.Size = new System.Drawing.Size(725, 215);
            this.lyc_Shiping.Text = "الشحن";
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.memoEdit_ShipTo;
            this.layoutControlItem21.Location = new System.Drawing.Point(365, 29);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(336, 141);
            this.layoutControlItem21.Text = "عنوان الشحن";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.gridLookUpEdit1;
            this.layoutControlItem19.Location = new System.Drawing.Point(365, 0);
            this.layoutControlItem19.MaxSize = new System.Drawing.Size(336, 0);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(336, 25);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(336, 29);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Text = "السائق";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(83, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(365, 170);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup3.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.Expanded = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem13});
            this.layoutControlGroup3.Location = new System.Drawing.Point(1013, 695);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(320, 30);
            this.layoutControlGroup3.Text = "بيانات الادخال";
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.txt_InsertUser;
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(296, 24);
            this.layoutControlItem11.Text = "ادخل بواسطه";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.txt_UpdateUser;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(296, 24);
            this.layoutControlItem12.Text = "اخر تعديل بواسطه";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(83, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.txt_LastUpdate;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(296, 24);
            this.layoutControlItem13.Text = "تاريخ اخر تعديل";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(83, 13);
            // 
            // frm_Inv_Invoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1353, 769);
            this.Controls.Add(this.layoutControl1);
            this.LookAndFeel.TouchScaleFactor = 1F;
            this.Name = "frm_Inv_Invoice";
            this.Text = "frm_Inv_Invoice";
            this.Load += new System.EventHandler(this.frm_Inv_Invoice_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frm_Inv_Invoice_KeyDown);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lkp_ToStore.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Remains.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_ShipTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Net.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Discount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Paid.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_DiscountVal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_Items)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_Items)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_Total.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spn_TotalRevenue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_LastUpdate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_UpdateUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_SourceID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_InsertUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_ID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp_Store.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_PartID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_Attn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_Date.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dt_Date.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_PartType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit2View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyg_Totals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_Remains)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_Net)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_Paid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_Profit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_PartID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_Attintion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_Source)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_PriceLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_ToStore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lyc_Shiping)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SpinEdit spn_Remains;
        private DevExpress.XtraEditors.MemoEdit memoEdit_ShipTo;
        private DevExpress.XtraEditors.SpinEdit spn_Net;
        private DevExpress.XtraEditors.SpinEdit spn_Discount;
        private DevExpress.XtraEditors.SpinEdit spn_Paid;
        private DevExpress.XtraEditors.SpinEdit spn_DiscountVal;
        private DevExpress.XtraGrid.GridControl GridControl_Items;
        private DevExpress.XtraGrid.Views.Grid.GridView GridView_Items;
        private DevExpress.XtraEditors.SpinEdit spn_Total;
        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private DevExpress.XtraEditors.SpinEdit spn_TotalRevenue;
        private DevExpress.XtraEditors.TextEdit txt_LastUpdate;
        private DevExpress.XtraEditors.TextEdit txt_UpdateUser;
        private DevExpress.XtraEditors.GridLookUpEdit LookUpEdit_SourceID;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.TextEdit txt_InsertUser;
        private DevExpress.XtraEditors.TextEdit txt_ID;
        private DevExpress.XtraEditors.LookUpEdit lkp_Store;
        private DevExpress.XtraEditors.GridLookUpEdit LookUpEdit_PartID;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraEditors.TextEdit txt_Code;
        private DevExpress.XtraEditors.TextEdit txt_Attn;
        private DevExpress.XtraEditors.DateEdit dt_Date;
        private DevExpress.XtraEditors.SimpleButton Button_ShowPayScreen;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_PartType;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem lyc_Source;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlGroup lyg_Totals;
        private DevExpress.XtraLayout.LayoutControlItem lyc_Remains;
        private DevExpress.XtraLayout.LayoutControlItem lyc_Net;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem lyc_Paid;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem lyc_PartID;
        private DevExpress.XtraLayout.LayoutControlItem lyc_Attintion;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem53;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraLayout.LayoutControlGroup lyc_Shiping;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.RadioGroup radioGroup1;
        private DevExpress.XtraLayout.LayoutControlItem lyc_PriceLevel;
        private DevExpress.XtraEditors.GridLookUpEdit gridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit2View;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem lyc_Profit;
        private DevExpress.XtraEditors.ButtonEdit textEdit1;
        private DevExpress.XtraEditors.LookUpEdit lkp_ToStore;
        private DevExpress.XtraLayout.LayoutControlItem lyc_ToStore;
    }
}