﻿using System;
using System.Data;
using System.Linq;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;
using ByStro.DAL;
using System.Windows.Forms;
using ByStro.Clases;
using System.Drawing;

namespace ByStro.Forms
{
    public partial class frm_PayInstallmentList : frm_Master
    {
        bool ShowIsPaid;
        public frm_PayInstallmentList(bool _ShoIsPaid)
        {
            InitializeComponent();
            ShowIsPaid = _ShoIsPaid;
        }
        private void frm_PayInstallment_SizeChanged(object sender, EventArgs e)
        {

            if (this.WindowState == FormWindowState.Maximized)
                this.WindowState = FormWindowState.Normal;
        }
        public override void frm_Load(object sender, EventArgs e)
        {
            base.frm_Load(sender, e);
            this.Text = ShowIsPaid ? "دفعات الاقساط المسدده" : "دفعات الاقساط المستحقه";
            this.layoutControlItem2 .Text  = ShowIsPaid ? " المسدده خلال " : " المستحقه خلال ";
            gridView1.OptionsBehavior.Editable = false;
            gridView1.DoubleClick += gridView1_DoubleClick;
            gridView1.CustomDrawCell += GridView1_CustomDrawCell;
            gridView1.Columns["ID"].Caption = "رقم";
            gridView1.Columns["InstalmentID"].Caption = "كود ملف القسط";
            gridView1.Columns["Customer"].Caption = "العميل";
            gridView1.Columns["DueDate"].Caption = "تاريخ الاستحقاق";
            gridView1.Columns["Amount"].Caption = "المبلغ";
            gridView1.Columns["DatePaid"].Caption = "تاريخ السداد";
            gridView1.Columns["Amount"].Summary.Clear();
            gridView1.Columns["Amount"].Summary.Add(DevExpress.Data.SummaryItemType.Sum,"Amount","الاجمالي{0}");
            gridView1.Columns["ID"].Summary.Clear();
            gridView1.Columns["ID"].Summary.Add(DevExpress.Data.SummaryItemType.Count,"ID","العدد{0}"); 
            btn_Delete.Visibility =
            btn_List.Visibility =
            btn_Print.Visibility =
            btn_Save.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            btn_Refresh.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
        }

        private void GridView1_CustomDrawCell(object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e)
        {
            if(ShowIsPaid == false && e.Column .FieldName == "DueDate")
            {
                var date = (DateTime)e.CellValue;
                int period = spinEdit1.EditValue.ToInt()/4;
                if(date  <= DateTime.Now.AddDays(period))
                    e.Appearance.BackColor = Color.Red;
                else if (date <= DateTime.Now.AddDays(period *2))
                    e.Appearance.BackColor = Color.OrangeRed ;
                else if (date <= DateTime.Now.AddDays(period * 3))
                    e.Appearance.BackColor = Color.Yellow ;
                else if (date <= DateTime.Now.AddDays(period * 4))
                    e.Appearance.BackColor = Color.LightYellow ;
            }
        }

        public override void RefreshData()
        {
            DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
            base.RefreshData(); 
            var instalDetails = db.Instalment_Details.Where(x => x.IsPaid == ShowIsPaid);
            var dateDilter = DateTime.Now;

            if (ShowIsPaid)
            {
                dateDilter = DateTime.Now.AddDays(Convert.ToInt32(spinEdit1.EditValue) * -1);
                instalDetails = instalDetails.Where(x => x.DatePaid >= dateDilter);
            }
            else
            {
                dateDilter = DateTime.Now.AddDays(Convert.ToInt32(spinEdit1.EditValue));
                instalDetails = instalDetails.Where(x => x.DueDate <= dateDilter);
            }

            var list = instalDetails.Select(x => new
            {
                x.ID ,
                x.InstalmentID,
                x.Amount ,
                x.DueDate ,
                x.DatePaid ,
                Customer = db.Customers.Single (c=>c.CustomerID == db.Instalments.Single(i=>i.ID == x.InstalmentID ).Customer ).CustomerName ,
            });
            gridControl1.DataSource = list.ToList();
        }
        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs ea = e as DXMouseEventArgs;
            GridView view = sender as GridView;
            GridHitInfo info = view.CalcHitInfo(ea.Location);

            if (info.InRow || info.InRowCell)
            { 
               frm_PayInstallment.Instance.LoadItem(Convert.ToInt32(view.GetRowCellValue(info.RowHandle, "ID")));
               frm_PayInstallment.Instance.Show();
            }

        }

        private void spinEdit1_EditValueChanged(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void spinEdit1_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            if (e.NewValue.ToInt() <= 0)
                e.Cancel = true;
        }
        public override void New()
        {
            frm_PayInstallment.Instance.Show();
            frm_PayInstallment.Instance.LoadItem(null);
        }
    }
}
