﻿using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ByStro.Forms
{
    public partial class frm_SearchItems : DevExpress.XtraBars.FluentDesignSystem.FluentDesignForm
    {
        public static  frm_SearchItems instance ;
        Dictionary<int, string> itemsUnits = new Dictionary<int, string>();
      // RepositoryItemLookUpEdit  itemComboBox = new RepositoryItemLookUpEdit();
        public static  frm_SearchItems Instance
        {
            get
            { 
                if (instance == null || instance.IsDisposed == true)
                    instance = new frm_SearchItems();
                return instance;
            }
            set => instance = value; }

        public frm_SearchItems()
        {
            InitializeComponent();
            
            this.Load += new System.EventHandler(this.frm_SearchItems_Load);
            // this.FormBorderStyle = FormBorderStyle.None;
            //this.gridView1.OptionsView.ShowAutoFilterRow = true; 
            this.TopMost = false; // To keep the window allways on top 

            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;  // 
            this.StartPosition = FormStartPosition.CenterScreen;
           // Root.Padding = new DevExpress.XtraLayout.Utils.Padding(25);
            gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsDetail.ShowDetailTabs = false;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.FindDelay = 500;
            this.gridView1.OptionsFind.FindNullPrompt = "ابحث عن الصنف";
            this.gridView1.OptionsFind.ShowCloseButton = false;
            this.gridView1.OptionsFind.ShowFindButton = false;
            this.gridView1.OptionsPrint.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.ShowIndicator = false;


            gridControl1.ViewRegistered += GridControl1_ViewRegistered;
            gridView1.CustomRowCellEditForEditing += GridView1_CustomRowCellEditForEditing; 
            gridView1.CustomUnboundColumnData += GridView1_CustomUnboundColumnData;
            gridView1.CellValueChanged += GridView1_CellValueChanged;

        }

        private void GridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
             //throw new NotImplementedException();
        }

        private void GridView1_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
        {
          

            var view = sender as GridView;
            var itemID =listSource [ e.ListSourceRowIndex];
            var index = view.GetRowHandle(e.ListSourceRowIndex);
            var Product = products.Where(x => x.ProdecutID == itemID).SingleOrDefault();
            if (Product == null) return;
            var balace = Convert.ToDouble(view.GetRowCellValue(index, "MainBalance"));
            var selectedPrice = Product.FiestUnitPrice1;
            double selectdFactor = 1;
            if (e.Column.FieldName != "Unit")
            {
                if (view.GetRowCellValue(index, "Unit").ToString() == Product.SecoundUnit)
                {
                    selectedPrice = Product.SecoundUnitPrice1;
                    double.TryParse(Product.SecoundUnitOperating , out selectdFactor);
                    
                }
                else if (view.GetRowCellValue(index, "Unit").ToString() == Product.ThreeUnit)
                {
                    selectedPrice = Product.ThreeUnitPrice1;
                    double.TryParse(Product.ThreeUnitOperating , out selectdFactor);
                   
                }
            }

            switch (e.Column.FieldName )

            {
                case "Unit":
                    if (e.IsGetData)
                    {
                        if (itemsUnits.Where(x => x.Key == itemID).Count() == 0)
                            e.Value = view.GetRowCellValue(index, "MainUnit");
                        else
                            e.Value = itemsUnits[itemID];
                    }
                    else
                    {
                        if (itemsUnits.Where(x => x.Key == itemID).Count() == 0)
                            itemsUnits.Add(itemID, e.Value.ToString());
                        else
                            itemsUnits[itemID] = e.Value.ToString();
                    }
                    break; 
                case "Balance":
                    // e.Value =  balace / selectdFactor  ;
                    e.Value = PL.Prodecut_frm.ChangeUnit(balace.ToString(), Product.FiestUnit, Product.FiestUnitFactor, Product.SecoundUnit, Product.SecoundUnitOperating, Product.SecoundUnitFactor, Product.ThreeUnit, Product.ThreeUnitOperating, Product.ThreeUnitFactor);
                    break;
                case "Price":
                     e.Value = selectedPrice;

                    break;


            }
            

        }

        private void GridView1_CustomRowCellEditForEditing(object sender, CustomRowCellEditEventArgs e)
        {
            var view = sender as GridView;

            if (e.Column.FieldName == "Unit")
            {
                RepositoryItemComboBox  itemTempComboBox = new RepositoryItemComboBox();
                DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
                var item = db.Prodecuts.Where(x => x.ProdecutID == Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "ID"))).Single();
                var datasource = new List<string>() { item.FiestUnit, item.SecoundUnit, item.ThreeUnit };
                datasource.RemoveAll(x => x == "");
                //itemTempComboBox.DataSource = new[] { new { Units = new List<string>() { item.FiestUnit, item.SecoundUnit, item.ThreeUnit } } };
                 itemTempComboBox.Items.AddRange (datasource);
                e.RepositoryItem = itemTempComboBox;
            }
        }

        private void GridControl1_ViewRegistered(object sender, DevExpress.XtraGrid.ViewOperationEventArgs e)
        {
            
            var view = e.View as GridView;
            if (view != null && view.LevelName == "DetailBalance")
            {
                view.ViewCaption = "ارصده الصنف في المخازن";
                view.OptionsView.ShowViewCaption = true;
                view.Columns["Store"].Caption = "المخزن";
                view.Columns["BalanceInStore"].Caption = "الرصيد";

            }
        }

        private void frm_SearchItems_Load(object sender, EventArgs e)
        {
            RefreshData();
            gridView1.PopulateColumns(gridControl1.DataSource); 
            gridView1.Columns.Add(new DevExpress.XtraGrid.Columns.GridColumn() { FieldName = "Unit" , Name = "Unit",UnboundType = DevExpress.Data.UnboundColumnType.String , VisibleIndex = 3});
            gridView1.Columns.Add(new DevExpress.XtraGrid.Columns.GridColumn() { FieldName = "Price", Name = "Price", UnboundType = DevExpress.Data.UnboundColumnType.String, VisibleIndex = 4 });
            gridView1.Columns.Add(new DevExpress.XtraGrid.Columns.GridColumn() { FieldName = "Balance", UnboundType = DevExpress.Data.UnboundColumnType.Integer  , VisibleIndex = 5 });

             

           
            gridView1.Columns["ID"]    .Width = 25;
            gridView1.Columns["Code"]   .Width = 40;
            gridView1.Columns["Unit"]   .Width = 40;
            gridView1.Columns["Price"]  .Width = 40;
            gridView1.Columns["Balance"].Width = 75;


            gridView1.Columns["ID"].OptionsColumn.AllowFocus =
            gridView1.Columns["Code"].OptionsColumn.AllowFocus =
            gridView1.Columns["Price"].OptionsColumn.AllowFocus =
            gridView1.Columns["Balance"].OptionsColumn.AllowFocus = false;

            gridView1.Columns["ID"].Caption = "رقم";
            gridView1.Columns["Code"].Caption = "ياركود";
            gridView1.Columns["Name"].Caption = "اسم الصنف";
            gridView1.Columns["Unit"].Caption = "الوحده";
            gridView1.Columns["Price"].Caption = "سعر 1 ";
            gridView1.Columns["Balance"].Caption = "الرصيد الكلي";

            gridView1.Columns["ID"].AppearanceCell.TextOptions.HAlignment =
            gridView1.Columns["Unit"].AppearanceCell.TextOptions.HAlignment =
            //  gridView1.Columns["Price"].AppearanceCell.TextOptions.HAlignment = 
            gridView1.Columns["Balance"].AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;



            gridView1.Columns["MainUnit"].OptionsColumn.ShowInCustomizationForm = gridView1.Columns["MainUnit"].Visible = false;
            gridView1.Columns["MainBalance"].OptionsColumn.ShowInCustomizationForm = gridView1.Columns["MainBalance"].Visible = false;

        }
        List<DAL.Prodecut> products =new  List<DAL.Prodecut>(); 
        /// <summary>
        /// Get data From the database and view it in the GridControl
        /// </summary>
        void RefreshData()
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            products = db.Prodecuts.ToList();
            var data = db.Prodecuts.Select(x => new
            {
                ID = x.ProdecutID,
                Code = x.FiestUnitBarcode ,
                Name = x.ProdecutName,
                MainUnit = x.FiestUnit,
                MainBalance = db.Store_Prodecuts.Where(s => s.ProdecutID == x.ProdecutID).Sum(b => b.Balence) ?? 0,
                DetailBalance = db.Store_Prodecuts.Where(s => s.ProdecutID == x.ProdecutID).Select(s=> new {
                    Store = db.Stores.Where(ss=>ss.StoreID ==s.StoreID ).SingleOrDefault().StoreName ,
                    BalanceInStore =  s.Balence }).ToList()
            }) ;
            listSource = data.Select(x => x.ID).ToList() ;
            gridControl1.DataSource = data.ToList(); 
        }
        List<int> listSource;  
       
        public static  void ShowSearchWindow(object  sender , EventArgs e )
        {
            frm_SearchItems.Instance.ShowDialog();
        }
    }
}
