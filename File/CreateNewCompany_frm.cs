﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class CreateNewCompany_frm : Form
    {
        public CreateNewCompany_frm()
        {
            InitializeComponent();
        }
      public  Boolean Saved = false;
        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }
        int Nuimber = 1;
        private void CreateNewCompany_frm_Load(object sender, EventArgs e)
        {
            LoadServer(combServerName);
            panel_Start.BringToFront();
            btnBake.Enabled = false;
            btnSave.Enabled = false;
            btnNext.Enabled = true;
            comboBox1.SelectedIndex = 0;
            txtPathSaveDatabase.Text = GetTempFolder();
        }





        public void LoadServer(ComboBox cmbServer)
        {
            try
            {
                cmbServer.Items.Clear();
                Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server");
                String[] names = (String[])key.GetValue("InstalledInstances");
                if (names.Length > 0)
                {
                    cmbServer.Items.Clear();
                   // cmbServer.Items.Add(".");
                    foreach (string item in names)
                    {
                        if (item == "MSSQLSERVER")
                            cmbServer.Items.Add(".");//Environment.MachineName
                        else
                            cmbServer.Items.Add(Environment.MachineName + "\\" + item);
                    }
                    cmbServer.SelectedIndex = 0;
                    if (cmbServer.Items.Count>0)
                    {
                        cmbServer.SelectedIndex = 1;
                    }
                }

            }
            catch
            {
                cmbServer.Items.Add(".");
                cmbServer.SelectedIndex = 0;
                if (cmbServer.Items.Count < 0)
                {
                    cmbServer.SelectedIndex = 1;
                }
                //cmbServer.Items.Add(Environment.MachineName);
                //  MessageBox.Show(ex.Message, "Alert", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                return;
            }

        }








        private void button1_Click(object sender, EventArgs e)
        {

           
    
                #region "radioButton1"
               
                    if (Nuimber == 1)
                    {
                        panel_CompanyData.BringToFront();
                        Nuimber = 2;
                btnBake.Enabled = true;

            }
            else if (Nuimber ==2)
                    {
                #region "Condetion"
                if (txtCombanyName.Text.Trim() == "")
                {
                    txtCombanyName.BackColor = Color.Pink;
                    txtCombanyName.Focus();
                    return;
                }
                if (txtCombanyDescriptone.Text.Trim() == "")
                {
                    txtCombanyDescriptone.BackColor = Color.Pink;
                    txtCombanyDescriptone.Focus();
                    return;
                }
                #endregion
                panel_User.BringToFront();
                        Nuimber = 3;
                    }
                    else if (Nuimber == 3)
                    {
                #region MyRegion
                if (txtEmployee.Text.Trim() == "")
                {
                    txtEmployee.BackColor = Color.Pink;
                    txtEmployee.Focus();
                    return;
                }

                if (txtUserName.Text.Trim() == "")
                {
                    txtUserName.BackColor = Color.Pink;
                    txtUserName.Focus();

                    return;
                }
                if (txtUserPassword.Text.Trim() == "")
                {
                    txtUserPassword.BackColor = Color.Pink;
                    txtUserPassword.Focus();

                    return;
                }

                if (txtConfirmPassword.Text.Trim() == "")
                {
                    txtConfirmPassword.BackColor = Color.Pink;
                    txtConfirmPassword.Focus();
                    return;
                }

                if (txtUserPassword.Text != txtConfirmPassword.Text)
                {
                    txtUserPassword.BackColor = Color.Pink;
                    txtUserPassword.Focus();
                    return;
                }

                #endregion

             
                panel_PathSave.BringToFront();
                        Nuimber = 4;
                    }
                    else if (Nuimber == 4)
                    {
                        panel_LastPage.BringToFront();
                        Nuimber = 5;
                        btnSave.Enabled = true;
                        btnNext.Enabled = false;
                    }
          
                #endregion

               

         


            //===================================================================================================================================================================================






        }

        private void btnBake_Click(object sender, EventArgs e)
        {

    
                #region "radioButton1"
               

                    if (Nuimber == 5)
                    {
                        panel_PathSave.BringToFront();
                        Nuimber = 4;
                    }
                    else if (Nuimber == 4)
                    {
                        panel_User.BringToFront();
                        Nuimber = 3;
                    }
                    else if (Nuimber == 3)
                    {
                        panel_CompanyData.BringToFront();
                        Nuimber = 2;
                    }
                    else if (Nuimber == 2)
                    {
                        panel_Start.BringToFront();
                        Nuimber = 1;
                btnBake.Enabled = false;

            }

            #endregion



            btnSave.Enabled = false;
            btnNext.Enabled = true;


        }

     
        private void txtEmployee_TextChanged(object sender, EventArgs e)
        {
            txtEmployee.BackColor = Color.White;
        }

        private void txtCombanyName_TextChanged(object sender, EventArgs e)
        {
            txtCombanyName.BackColor = Color.White;
        }

        private void txtCombanyDescriptone_TextChanged(object sender, EventArgs e)
        {
            txtCombanyDescriptone.BackColor = Color.White;
        }

        private void txtUserName_TextChanged(object sender, EventArgs e)
        {
            txtUserName.BackColor = Color.White;
        }

        private void txtUserPassword_TextChanged(object sender, EventArgs e)
        {
            txtUserPassword.BackColor = Color.White;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            txtConfirmPassword.BackColor = Color.White;
        }

        private void btnBroser_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.FileName = "ByStro";
            if (sfd.ShowDialog() == DialogResult.OK)
            {

                txtDatabaseCreate.Text = System.IO.Path.GetFileName(sfd.FileName);
                txtPathSaveDatabase.Text = System.IO.Path.GetDirectoryName(sfd.FileName);
            }
        }




        private void SaveSetting(Boolean Mode, string SQLUserName, string SQLPassword, string ServerName, string Databasename)
        {
            var p = Properties.Settings.Default;
            string Connection_String = "";
            if (Mode == false)
            {
                Connection_String = "Data Source=" + combServerName.Text + ";Initial Catalog=" + Databasename + ";Integrated security=true";
            }
            else
            {
                Connection_String = "Data Source=" + combServerName.Text + ",1433;Network Library=DBMSSOCN;Initial Catalog =" + Databasename + ";User ID='" + txt_name.Text + "' ;Password='" + txt_password.Text + "'";

            }

            p.SQLUserName = SQLUserName;
            p.SQLPassword = SQLPassword;
            p.ServerName = ServerName;
            p.Databasename = Databasename;
            p.Connection_String = Connection_String;
            p.Mode = Mode;
            p.Save();

      
        }







        private void CreateIfMissing(string path)
        {
            bool folderExists = Directory.Exists(path);
            if (!folderExists)
                Directory.CreateDirectory(path);




        }



        private string GetTempFolder()
        {
            for (char drive = 'D'; drive <= 'Z'; drive++)
            {
                if (Directory.Exists(drive + ":"))
                {
                    Directory.CreateDirectory(drive + ":\\ByStro\\Data");
                    return drive + @":\ByStro\Data";
                }
            }
            Directory.CreateDirectory("C:\\ByStro");
            return @"C:\ByStro";
        }
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                txtPathSaveDatabase.ReadOnly = false;

            }
            else
            {
                txtPathSaveDatabase.ReadOnly = true;

            }
        }

        private void lblLoadServer_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            LoadServer(combServerName);
        }

      

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == 1)
            {
                txt_name.Enabled = true;
                txt_password.Enabled = true;
            }
            else
            {
                txt_name.BackColor = Color.White;
                txt_password.BackColor = Color.White;
                txt_name.Enabled = false;
                txt_password.Enabled = false;
            }
        }

        private void txt_name_TextChanged(object sender, EventArgs e)
        {
            txt_name.BackColor = Color.White;

        }

        private void txt_password_TextChanged(object sender, EventArgs e)
        {
            txt_password.BackColor = Color.White;
        }

      




      



        SqlConnection conn = new SqlConnection();
        string SqlConnectionString = "";
        bool Mode;
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {


                CreateNew_Database_cls CreateNew_Database_cls = new CreateNew_Database_cls();
                #region MyRegion
                if (combServerName.Text == "")
                {
                    combServerName.BackColor = Color.Pink;
                    combServerName.Focus();
                    return;
                }


                if (comboBox1.SelectedIndex == 1)
                {
                    if (txt_name.Text == "")
                    {
                        txt_name.BackColor = Color.Pink;
                        txt_name.Focus();
                        return;
                    }
                    if (txt_password.Text == "")
                    {
                        txt_password.BackColor = Color.Pink;
                        txt_password.Focus();
                        return;
                    }
                }
                #endregion


                if (comboBox1.SelectedIndex == 0)
                {
                    SqlConnectionString = "Data Source=" + combServerName.Text + ";Initial Catalog=master;Integrated security=true";
                    Mode = false;
                }
                else
                {
                    SqlConnectionString = "Data Source=" + combServerName.Text + ",1433;Network Library=DBMSSOCN;Initial Catalog =master;User ID='" + txt_name.Text + "' ;Password='" + txt_password.Text + "'";
                    Mode = true;
                }


                conn = new SqlConnection(SqlConnectionString);



                // create database
                //CreateIfMissing(txtPathSaveDatabase.Text);
                txtPathSaveDatabase.Text = GetTempFolder();
                CreateNew_Database(txtDatabaseCreate.Text, txtPathSaveDatabase.Text);

                    // insert User 

              
              

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }



        protected void CreateNew_Database(string DatabaseName, string Path)
        {
            string sql = @"CREATE DATABASE " + DatabaseName + " On primary (Name = " + DatabaseName + ", FileName =N'" + Path +@"\"+ DatabaseName + ".mdf') LOG ON (Name=" + DatabaseName + "_log, FileName=N'" + Path + @"\" + DatabaseName + "_log.ldf')" + Environment.NewLine;

           
                try
            {

                if (conn.State == ConnectionState.Closed) conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
                //=====================================================================================================================================
                FileInfo file = new FileInfo(Application.StartupPath + "\\script.sql");
                string script = "Use " + DatabaseName + Environment.NewLine;
                script += file.OpenText().ReadToEnd();
                System.Collections.Generic.IEnumerable<string> commandStrings = Regex.Split(script, @"^\s*GO\s*$", RegexOptions.Multiline | RegexOptions.IgnoreCase);
                using (conn)
                {
                    foreach (string commandString in commandStrings)
                    {
                        if (commandString.Trim() != "")
                        {
                            using (var command = new SqlCommand(commandString, conn))
                            {
                                command.ExecuteNonQuery();

                            }
                        }

                    }
                }



                SaveSetting(Mode, txt_name.Text, txt_password.Text, combServerName.Text, txtDatabaseCreate.Text);
                // insert Combany Information
                CombanyInformatuon_cls combanyInformatuon_cls = new CombanyInformatuon_cls();
                combanyInformatuon_cls.InsertNew_CombanyData("1", txtCombanyName.Text, txtCombanyDescriptone.Text, txtMyName.Text, txtPhoneNamber.Text, txtTitle.Text,"","");
                // User
                User_Permetion_frm User_Permetion = new User_Permetion_frm();
                UserPermissions_CLS cls_User_Permetion = new UserPermissions_CLS();




                string sql_User = "select * from UserPermissions ";
                DataAccessLayer dal = new DataAccessLayer(); 
                SqlDataAdapter da2 = new SqlDataAdapter(sql_User, dal.connSQLServer);
                DataTable dt2 = new DataTable();
                da2.Fill(dt2);


                DataRow dr;
           
                    dr = dt2.NewRow();
                    dr["ID"] = cls_User_Permetion.MaxID_UserPermissions();

  

                dr["EmpName"] = txtEmployee.Text;
                dr["UserName"] = txtUserName.Text;
                dr["UserPassword"] = txtUserPassword.Text;

                dr["TypeUser"] = true;
                dr["Status"] = true;
                for (int i = 0; i < User_Permetion.checkedListBox1.Items.Count; i++)
                {
                    dr[i + 6] = true;
                }
          
                    dt2.Rows.Add(dr);
                    SqlCommandBuilder cmdUser = new SqlCommandBuilder(da2);
                    da2.Update(dt2);
            
              
  

                MessageBox.Show("تم حفظ الاعدادت بنجاح", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Saved = true;
                this.Close();
            }
            catch (SqlException ex)
            {
                Saved = false;
                string spError = sql.Length > 100 ? sql.Substring(0, 100) + " ...\n..." : sql;
                MessageBox.Show(string.Format("Please check the SqlServer \nFile: {0} \nLine: {1} \nError: {2} \nSQL Command: \n{3}", "Please check the File path D:", "Please check the SqlServer Script ", ex.Message, spError), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            finally
            {
                if (conn.State == ConnectionState.Open) conn.Close();

            }
        }

 
    }
}
