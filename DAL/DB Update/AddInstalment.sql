
CREATE TABLE [dbo].[Instalment_Details](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[number] [int] NOT NULL,
	[InstalmentID] [int] NOT NULL,
	[Amount] [float] NOT NULL,
	[DueDate] [datetime] NOT NULL,
	[DatePaid] [datetime] NULL,
	[IsPaid] [bit] NOT NULL,
 CONSTRAINT [PK_Instalment_Details] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[Instalments](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Customer] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[InstallPeriod] [int] NOT NULL,
	[Amount] [float] NOT NULL,
	[AdvancedPay] [float] NOT NULL,
	[Net] [float] NOT NULL,
	[BenfitesValue] [float] NOT NULL,
	[InstalmentsCount] [int] NOT NULL,
	[ISPaid] [bit] NULL,
	[Notes] [nvarchar](max) NULL,
	[Invoice] [int] NULL,
 CONSTRAINT [PK_Instalments] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

