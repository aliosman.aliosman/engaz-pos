﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

public class ERB_Setting
{
    public static void UseNamberOnly(KeyPressEventArgs e)
    {
        if (!char.IsDigit(e.KeyChar) && e.KeyChar != 8 && e.KeyChar != Convert.ToChar("."))
        {
            e.Handled = true;
        }

    }

    

    public static void SettingToolStrip(ToolStrip too)
    {
        too.BackColor = Color.FromArgb(203, 210, 239);
    }

    public static void SettingForm(Form frm)
    {
        frm.BackColor = Color.FromArgb(248, 241, 241);
        frm.FormBorderStyle = FormBorderStyle.FixedSingle;
        frm.StartPosition = FormStartPosition.CenterScreen;
        frm.MaximizeBox = false;
        
    }

    public static void SettingDGV(DataGridView dgv)
    {
        //dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.FloralWhite;
        //الديغولت

        dgv.DefaultCellStyle.BackColor = Color.FromArgb(223, 235, 247);
        dgv.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        dgv.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

        // عند الاختيار
        dgv.RowsDefaultCellStyle.SelectionBackColor = Color.FromArgb(93, 190, 250);   //FromArgb(185, 209, 234);
        dgv.RowsDefaultCellStyle.SelectionForeColor = Color.White;

        // الخلفية
        dgv.BackgroundColor = Color.FromArgb(231, 230, 225);// Color.FromArgb(223, 235, 247);

        // header
        dgv.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(83, 119, 145);
        dgv.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
        dgv.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
        dgv.GridColor = Color.Black;
        dgv.ColumnHeadersHeight = 28;
        //
        dgv.RowTemplate.Height = 27;
        // dgv.RowTemplate.DefaultCellStyle.Font = KisaraSystem.Properties.Settings.Default.Font;

    }




}

