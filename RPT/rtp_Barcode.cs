﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using ByStro.Clases;
using System.IO;
using System.Linq;

namespace ByStro.RPT
{
    public partial class rtp_Barcode : DevExpress.XtraReports.UI.XtraReport
    {
        public rtp_Barcode()
        {
            InitializeComponent();
        }
        public   void LoadData()
        {

    

            xrBarCode1.DataBindings.Add("Text", DataSource, "PrintBarcode");
            lbl_Price.DataBindings.Add("Text", DataSource, "Price");
            lbl_ItemName.DataBindings.Add("Text", DataSource, "PrintName");
           // lbl_companyName.DataBindings.Add("Text", DataSource, "CompanyName");

            

            
        }
        public static void Print(object ds , int templateID)
        {
            rtp_Barcode  report = new rtp_Barcode();
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);

            var row = db.BarcodeTemplates.Single(x => x.ID == templateID);
            using (MemoryStream stream = new MemoryStream())
            { 
                stream.Write(row.Template.ToArray(), 0, row.Template.ToArray().Length);
                report.LoadLayout(stream);
            }
             
            report.DataSource = ds;
            report.DataMember = "";
            
            //     report.DetailReport_Expence .DataSource = report.DataSource;
            //  report.DetailReport_Expence.DataMember = "HeadOtherCharges";
            report.LoadData();
            report. lbl_companyName.Text = CurrentSession.Company.CombanyName;
            report.lbl_Price.TextFormatString = "{0:£0.00}";

            //switch (CurrentSession.user.WhenPrintShowMode)
            //{
            //    case 0: report.ShowPreview(); break;
            //    case 1: report.PrintDialog(); break;
            //    case 2: report.Print(); break;
            //    default: report.PrintDialog(); break;
            //}
            report.ShowPreview();

        }

    }
}
