﻿namespace ByStro.PL
{
    partial class RbtProdecuts_Prise_frm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RbtProdecuts_Prise_frm));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.label6 = new System.Windows.Forms.Label();
            this.txtStoreID = new System.Windows.Forms.TextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnView = new System.Windows.Forms.ToolStripButton();
            this.combStore = new System.Windows.Forms.ComboBox();
            this.DGV1 = new System.Windows.Forms.DataGridView();
            this.combProdecut = new System.Windows.Forms.ComboBox();
            this.txtProdecutID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.D1 = new System.Windows.Forms.DateTimePicker();
            this.D2 = new System.Windows.Forms.DateTimePicker();
            this.ProdecutID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MyDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MainID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProdecutName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StoreName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StoreID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SupplierName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnitFactor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UnitOperating = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV1)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.lblCount});
            this.statusStrip1.Location = new System.Drawing.Point(0, 450);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(884, 23);
            this.statusStrip1.TabIndex = 702;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(94, 18);
            this.toolStripStatusLabel1.Text = "عدد الحركات :";
            // 
            // lblCount
            // 
            this.lblCount.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCount.Name = "lblCount";
            this.lblCount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCount.Size = new System.Drawing.Size(17, 18);
            this.lblCount.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(272, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 17);
            this.label6.TabIndex = 703;
            this.label6.Text = "المخزن :";
            // 
            // txtStoreID
            // 
            this.txtStoreID.BackColor = System.Drawing.Color.White;
            this.txtStoreID.Location = new System.Drawing.Point(691, 57);
            this.txtStoreID.Name = "txtStoreID";
            this.txtStoreID.ReadOnly = true;
            this.txtStoreID.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtStoreID.Size = new System.Drawing.Size(10, 24);
            this.txtStoreID.TabIndex = 706;
            this.txtStoreID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtStoreID.Visible = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnView});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(884, 25);
            this.toolStrip1.TabIndex = 712;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnView
            // 
            this.btnView.Image = global::ByStro.Properties.Resources.Search_20;
            this.btnView.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(58, 22);
            this.btnView.Text = "عرض";
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // combStore
            // 
            this.combStore.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combStore.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combStore.FormattingEnabled = true;
            this.combStore.Location = new System.Drawing.Point(336, 57);
            this.combStore.Name = "combStore";
            this.combStore.Size = new System.Drawing.Size(340, 24);
            this.combStore.TabIndex = 714;
            this.combStore.SelectedIndexChanged += new System.EventHandler(this.combStore_SelectedIndexChanged);
            this.combStore.TextChanged += new System.EventHandler(this.combStore_TextChanged);
            // 
            // DGV1
            // 
            this.DGV1.AllowUserToAddRows = false;
            this.DGV1.AllowUserToDeleteRows = false;
            this.DGV1.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FloralWhite;
            this.DGV1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DGV1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGV1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGV1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(146)))), ((int)(((byte)(222)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGV1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DGV1.ColumnHeadersHeight = 24;
            this.DGV1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.DGV1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProdecutID,
            this.MyDate,
            this.MainID,
            this.ProdecutName,
            this.Unit,
            this.Quantity,
            this.Price,
            this.Vat,
            this.TotalPrice,
            this.StoreName,
            this.StoreID,
            this.SupplierName,
            this.UnitFactor,
            this.UnitOperating});
            this.DGV1.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGV1.DefaultCellStyle = dataGridViewCellStyle4;
            this.DGV1.EnableHeadersVisualStyles = false;
            this.DGV1.GridColor = System.Drawing.Color.Silver;
            this.DGV1.Location = new System.Drawing.Point(0, 87);
            this.DGV1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.DGV1.MultiSelect = false;
            this.DGV1.Name = "DGV1";
            this.DGV1.ReadOnly = true;
            this.DGV1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.DGV1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Tahoma", 10F);
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGV1.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.DGV1.RowHeadersWidth = 30;
            this.DGV1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.DGV1.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.DGV1.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DGV1.RowTemplate.Height = 23;
            this.DGV1.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.DGV1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGV1.Size = new System.Drawing.Size(884, 361);
            this.DGV1.TabIndex = 715;
            // 
            // combProdecut
            // 
            this.combProdecut.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combProdecut.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combProdecut.FormattingEnabled = true;
            this.combProdecut.Location = new System.Drawing.Point(336, 30);
            this.combProdecut.Name = "combProdecut";
            this.combProdecut.Size = new System.Drawing.Size(340, 24);
            this.combProdecut.TabIndex = 718;
            this.combProdecut.SelectedIndexChanged += new System.EventHandler(this.combProdecut_SelectedIndexChanged);
            this.combProdecut.TextChanged += new System.EventHandler(this.combProdecut_TextChanged);
            // 
            // txtProdecutID
            // 
            this.txtProdecutID.BackColor = System.Drawing.Color.White;
            this.txtProdecutID.Location = new System.Drawing.Point(691, 30);
            this.txtProdecutID.Name = "txtProdecutID";
            this.txtProdecutID.ReadOnly = true;
            this.txtProdecutID.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtProdecutID.Size = new System.Drawing.Size(10, 24);
            this.txtProdecutID.TabIndex = 717;
            this.txtProdecutID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtProdecutID.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(277, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 17);
            this.label1.TabIndex = 716;
            this.label1.Text = "الصنف :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 17);
            this.label3.TabIndex = 726;
            this.label3.Text = "إلي :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 17);
            this.label2.TabIndex = 725;
            this.label2.Text = "من :";
            // 
            // D1
            // 
            this.D1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.D1.Location = new System.Drawing.Point(60, 29);
            this.D1.Name = "D1";
            this.D1.Size = new System.Drawing.Size(200, 24);
            this.D1.TabIndex = 724;
            // 
            // D2
            // 
            this.D2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.D2.Location = new System.Drawing.Point(60, 57);
            this.D2.Name = "D2";
            this.D2.Size = new System.Drawing.Size(200, 24);
            this.D2.TabIndex = 723;
            // 
            // ProdecutID
            // 
            this.ProdecutID.DataPropertyName = "ProdecutID";
            this.ProdecutID.HeaderText = "كود الصنف";
            this.ProdecutID.Name = "ProdecutID";
            this.ProdecutID.ReadOnly = true;
            this.ProdecutID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ProdecutID.Visible = false;
            // 
            // MyDate
            // 
            this.MyDate.DataPropertyName = "MyDate";
            this.MyDate.FillWeight = 99.77536F;
            this.MyDate.HeaderText = "التاريخ";
            this.MyDate.Name = "MyDate";
            this.MyDate.ReadOnly = true;
            // 
            // MainID
            // 
            this.MainID.DataPropertyName = "MainID";
            this.MainID.HeaderText = "رقم الفاتورة";
            this.MainID.Name = "MainID";
            this.MainID.ReadOnly = true;
            // 
            // ProdecutName
            // 
            this.ProdecutName.DataPropertyName = "ProdecutName";
            this.ProdecutName.FillWeight = 201.2797F;
            this.ProdecutName.HeaderText = "اسم الصنف";
            this.ProdecutName.Name = "ProdecutName";
            this.ProdecutName.ReadOnly = true;
            this.ProdecutName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Unit
            // 
            this.Unit.DataPropertyName = "Unit";
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.Unit.DefaultCellStyle = dataGridViewCellStyle3;
            this.Unit.FillWeight = 100.6327F;
            this.Unit.HeaderText = "الوحدة";
            this.Unit.Name = "Unit";
            this.Unit.ReadOnly = true;
            this.Unit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Quantity
            // 
            this.Quantity.DataPropertyName = "Quantity";
            this.Quantity.FillWeight = 81.95764F;
            this.Quantity.HeaderText = "الكمية";
            this.Quantity.Name = "Quantity";
            this.Quantity.ReadOnly = true;
            this.Quantity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Price
            // 
            this.Price.DataPropertyName = "Price";
            this.Price.FillWeight = 118.9515F;
            this.Price.HeaderText = "سعر الوحدة";
            this.Price.Name = "Price";
            this.Price.ReadOnly = true;
            this.Price.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Vat
            // 
            this.Vat.DataPropertyName = "Vat";
            this.Vat.FillWeight = 103.1879F;
            this.Vat.HeaderText = "خصم %";
            this.Vat.Name = "Vat";
            this.Vat.ReadOnly = true;
            // 
            // TotalPrice
            // 
            this.TotalPrice.DataPropertyName = "TotalPrice";
            this.TotalPrice.FillWeight = 101.2662F;
            this.TotalPrice.HeaderText = "الاجمالي";
            this.TotalPrice.Name = "TotalPrice";
            this.TotalPrice.ReadOnly = true;
            this.TotalPrice.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // StoreName
            // 
            this.StoreName.DataPropertyName = "StoreName";
            this.StoreName.FillWeight = 100.8872F;
            this.StoreName.HeaderText = "المخزن";
            this.StoreName.Name = "StoreName";
            this.StoreName.ReadOnly = true;
            this.StoreName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // StoreID
            // 
            this.StoreID.DataPropertyName = "StoreID";
            this.StoreID.HeaderText = "StoreID";
            this.StoreID.Name = "StoreID";
            this.StoreID.ReadOnly = true;
            this.StoreID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.StoreID.Visible = false;
            // 
            // SupplierName
            // 
            this.SupplierName.DataPropertyName = "SupplierName";
            this.SupplierName.FillWeight = 141.301F;
            this.SupplierName.HeaderText = "المورد";
            this.SupplierName.Name = "SupplierName";
            this.SupplierName.ReadOnly = true;
            // 
            // UnitFactor
            // 
            this.UnitFactor.DataPropertyName = "UnitFactor";
            this.UnitFactor.HeaderText = "UnitFactor";
            this.UnitFactor.Name = "UnitFactor";
            this.UnitFactor.ReadOnly = true;
            this.UnitFactor.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.UnitFactor.Visible = false;
            // 
            // UnitOperating
            // 
            this.UnitOperating.DataPropertyName = "UnitOperating";
            this.UnitOperating.HeaderText = "UnitOperating";
            this.UnitOperating.Name = "UnitOperating";
            this.UnitOperating.ReadOnly = true;
            this.UnitOperating.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.UnitOperating.Visible = false;
            // 
            // RbtProdecuts_Prise_frm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(884, 473);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.D1);
            this.Controls.Add(this.D2);
            this.Controls.Add(this.combProdecut);
            this.Controls.Add(this.txtProdecutID);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DGV1);
            this.Controls.Add(this.combStore);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.txtStoreID);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.statusStrip1);
            this.Font = new System.Drawing.Font("Tahoma", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MinimumSize = new System.Drawing.Size(900, 512);
            this.Name = "RbtProdecuts_Prise_frm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "تقرير فروق اسعار الشراء من الموردين";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.BalanseItemes_frm_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel lblCount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtStoreID;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnView;
        private System.Windows.Forms.ComboBox combStore;
        public System.Windows.Forms.DataGridView DGV1;
        private System.Windows.Forms.ComboBox combProdecut;
        private System.Windows.Forms.TextBox txtProdecutID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker D1;
        private System.Windows.Forms.DateTimePicker D2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProdecutID;
        private System.Windows.Forms.DataGridViewTextBoxColumn MyDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn MainID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProdecutName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Unit;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vat;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn StoreName;
        private System.Windows.Forms.DataGridViewTextBoxColumn StoreID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SupplierName;
        private System.Windows.Forms.DataGridViewTextBoxColumn UnitFactor;
        private System.Windows.Forms.DataGridViewTextBoxColumn UnitOperating;
    }
}