﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ByStro.PL.SMS
{
    public partial class SmsSetting_frm : Form
    {
        public SmsSetting_frm()
        {
            InitializeComponent();
        }

        private void SmsSetting_frm_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingToolStrip(toolStrip1);
            ERB_Setting.SettingForm(this);
        }
    }
}
