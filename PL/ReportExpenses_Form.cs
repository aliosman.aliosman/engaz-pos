﻿
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class ReportExpenses_Form : Form
    {
        public ReportExpenses_Form()
        {
            InitializeComponent();
        }
        RbtExpenses_cls cls = new RbtExpenses_cls();
        private void btnNew_Click(object sender, EventArgs e)
        {

        }
        private void RbtCustomer_frm_Load(object sender, EventArgs e)
        {
            try
            {
                ERB_Setting.SettingToolStrip(toolStrip1);
                ERB_Setting.SettingForm(this);
                ERB_Setting.SettingDGV(DGV1);
                // FILL Combo Store item
                UserPermissions_CLS Store_cls = new UserPermissions_CLS();
                combUser.DataSource = Store_cls.Search_UserPermissions();
                combUser.DisplayMember = "EmpName";
                combUser.ValueMember = "ID";
                combUser.Text = "";
                txtUserID.Text = "";
                //=============================================================================================
                // FILL Combo Store item
                ExpenseType_cls ExpenseType_cls = new ExpenseType_cls();
                combExpenseType.DataSource = ExpenseType_cls.Search_ExpenseType("");
                combExpenseType.DisplayMember = "ExpenseName";
                combExpenseType.ValueMember = "ExpenseID";
                combExpenseType.Text = "";
                txtExpenseID.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {

                DataTable dt = new DataTable();
                if (txtUserID.Text.Trim()!="" && txtExpenseID.Text.Trim()=="")
                {
                    dt = cls.ReportSearch_Expenses(D1.Value, D2.Value,txtUserID.Text);

                }
                else if (txtUserID.Text.Trim() == "" && txtExpenseID.Text.Trim() != "")
                {
                    dt = cls.ReportSearch_Expenses(D1.Value, D2.Value, int.Parse(txtExpenseID.Text));
                }
                else if (txtUserID.Text.Trim() != "" && txtExpenseID.Text.Trim() != "")
                {
                    dt = cls.ReportSearch_Expenses(D1.Value, D2.Value, txtExpenseID.Text,txtUserID.Text);
                }
                else
                {
                dt = cls.ReportSearch_Expenses(D1.Value, D2.Value);

                }
                DGV1.AutoGenerateColumns = false;
                DGV1.DataSource = dt;
                lblCount.Text = DGV1.Rows.Count.ToString();
                SumBalence();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }




        }





        private void SumBalence()
        {
            try
            {
                double Debit = 0;
                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    if (DGV1.Rows[i].Cells["ExpenseValue"].Value.ToString() != "")
                    {
                        Debit += Convert.ToDouble(DGV1.Rows[i].Cells["ExpenseValue"].Value);
                    }
                   
                }
                lblDebit.Text = Debit.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void combUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                combUser.BackColor = Color.White;
                txtUserID.Text = combUser.SelectedValue.ToString();

            }
            catch
            {

                txtUserID.Text = "";

            }
        }

        private void combUser_TextChanged(object sender, EventArgs e)
        {
            try
            {
                combUser.BackColor = Color.White;

                if (combUser.Text == "")
                {
                    txtUserID.Text = "";
                }
                else
                {
                    txtUserID.Text = combUser.SelectedValue.ToString();
                }
            }
            catch
            {
                txtUserID.Text = "";
            }
        
    }

        private void combExpenseType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                combExpenseType.BackColor = Color.White;
                txtExpenseID.Text = combExpenseType.SelectedValue.ToString();

            }
            catch
            {

                txtExpenseID.Text = "";

            }
        }

        private void combExpenseType_TextChanged(object sender, EventArgs e)
        {
            try
            {
                combExpenseType.BackColor = Color.White;

                if (combExpenseType.Text == "")
                {
                    txtExpenseID.Text = "";
                }
                else
                {
                    txtExpenseID.Text = combExpenseType.SelectedValue.ToString();
                }
            }
            catch
            {
                txtExpenseID.Text = "";
            }
        }
    }
}
