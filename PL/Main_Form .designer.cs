﻿namespace ByStro.PL
{
    partial class Main_frm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main_frm));
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame1 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame2 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame3 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame4 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame5 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame6 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame7 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame8 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame9 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame10 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement14 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame11 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement15 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame12 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement16 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement17 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame13 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement18 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement19 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame14 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement20 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame15 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement21 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement22 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame16 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement23 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame17 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement24 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement25 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame18 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement26 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame19 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement27 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame20 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement28 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement29 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame21 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement30 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame22 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement31 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement32 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame23 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement33 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame24 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement34 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame25 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement35 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement36 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame26 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement37 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame27 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement38 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame28 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement39 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement40 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame29 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement41 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame30 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement42 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame31 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement43 = new DevExpress.XtraEditors.TileItemElement();
            this.ToolStrip2 = new System.Windows.Forms.ToolStrip();
            this.CS_01 = new System.Windows.Forms.ToolStripDropDownButton();
            this.CS_02 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_03 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_04 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_05 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_06 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_07 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_09 = new System.Windows.Forms.ToolStripMenuItem();
            this.بحثالاصنافToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.الالوانToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.الاحجامToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.سندهالكاصنافToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_10 = new System.Windows.Forms.ToolStripDropDownButton();
            this.CS_11 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_14 = new System.Windows.Forms.ToolStripMenuItem();
            this.casherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.فاتورةعرضسعرToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.التوصيلToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.السائقونToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.امرتوصيلجديدToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.اوامرالتوصيلالجاريهToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.اوامرالتوصيلالمنتهيةToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_15 = new System.Windows.Forms.ToolStripDropDownButton();
            this.CS11 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_18 = new System.Windows.Forms.ToolStripMenuItem();
            this.فاتورةطلبالشراءToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_29 = new System.Windows.Forms.ToolStripDropDownButton();
            this.CS_30 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_31 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_32 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_33 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_34 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_35 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_36 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_37 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_38 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_39 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_40 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_41 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_19 = new System.Windows.Forms.ToolStripDropDownButton();
            this.CS_20 = new System.Windows.Forms.ToolStripMenuItem();
            this.CustomersFirst_frm = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_21 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_22 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_23 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_24 = new System.Windows.Forms.ToolStripDropDownButton();
            this.CS_25 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS25 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_26 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_27 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_28 = new System.Windows.Forms.ToolStripMenuItem();
            this.Fills = new System.Windows.Forms.ToolStripDropDownButton();
            this.CreateNewCompany_frm = new System.Windows.Forms.ToolStripMenuItem();
            this.OpenNewCompany_frm = new System.Windows.Forms.ToolStripMenuItem();
            this.enuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_75 = new System.Windows.Forms.ToolStripDropDownButton();
            this.MaintenanceCompeny = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_76 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_77 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_78 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_79 = new System.Windows.Forms.ToolStripMenuItem();
            this.BillMaintenance_frm = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_53 = new System.Windows.Forms.ToolStripDropDownButton();
            this.CS_74 = new System.Windows.Forms.ToolStripMenuItem();
            this.Currency_frm = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_54 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_55 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_56 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_57 = new System.Windows.Forms.ToolStripMenuItem();
            this.سندتحويلأموالToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.الاقساطToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.قائمهالاقساطToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.اضافهقسطToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.سداددفعهقسطToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.عرضالاقساطالمستحقهToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.عرضالدفعاتالمسددهToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.واردمنصرفخذنهToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_42 = new System.Windows.Forms.ToolStripDropDownButton();
            this.tripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.صافيالربحاوالخسارهToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.اكثرالاصنافربحاToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.الارباحمنالعملاءToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_58 = new System.Windows.Forms.ToolStripDropDownButton();
            this.CS_59 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_60 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_81 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_61 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_62 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_66 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_67 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_68 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_69 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_80 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_70 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_71 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_72 = new System.Windows.Forms.ToolStripMenuItem();
            this.طباعةباركودToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.حركهصنفتفصيليToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ارصدهالاصنافToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.صلاحياتالاصنافToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.اصنافوصلتاليحدالطلبToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_46 = new System.Windows.Forms.ToolStripDropDownButton();
            this.CS_47 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_48 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_49 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_50 = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_51 = new System.Windows.Forms.ToolStripMenuItem();
            this.CHExelCustomer_frm = new System.Windows.Forms.ToolStripMenuItem();
            this.chExelSuppliers_frm = new System.Windows.Forms.ToolStripMenuItem();
            this.مجموعةاصنافToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.الاصنافToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_52 = new System.Windows.Forms.ToolStripMenuItem();
            this.ChangeFont = new System.Windows.Forms.ToolStripMenuItem();
            this.CHClearAllData = new System.Windows.Forms.ToolStripMenuItem();
            this.CS_73 = new System.Windows.Forms.ToolStripMenuItem();
            this.Maintenance_frm = new System.Windows.Forms.ToolStripMenuItem();
            this.نماذجالباركودToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.اعداداتالقرائهمنالميزانToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.تغييرالخلفيهToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Username = new System.Windows.Forms.ToolStripDropDownButton();
            this.تغيركلمةالمرورToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.تسجيلخروجToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.خروجToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.showToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.ribbonStatusBar1 = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem4 = new DevExpress.XtraBars.BarStaticItem();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.applicationMenu2 = new DevExpress.XtraBars.Ribbon.ApplicationMenu(this.components);
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem10 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem11 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem12 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem13 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem14 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem15 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem16 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem17 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem18 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem19 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem20 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem21 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem22 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem23 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem24 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem25 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem26 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem27 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem28 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem29 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem30 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem31 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem32 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem33 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem34 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem35 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem36 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem37 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem38 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem39 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem40 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem41 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem42 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem43 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem44 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem45 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem46 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem47 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem48 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem49 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem50 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem51 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem52 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem53 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem54 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem55 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem3 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem59 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem60 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem61 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem57 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem58 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem62 = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barButtonItem66 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem67 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem68 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem69 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem70 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem71 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem72 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem73 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem74 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem75 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem76 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem77 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem78 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem56 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem79 = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemHypertextLabel1 = new DevExpress.XtraEditors.Repository.RepositoryItemHypertextLabel();
            this.barEditItem2 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.barEditItem3 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemHypertextLabel2 = new DevExpress.XtraEditors.Repository.RepositoryItemHypertextLabel();
            this.barSubItem4 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem80 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem81 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem82 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem83 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem84 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem5 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem85 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem86 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem87 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem88 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem89 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem90 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem91 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem92 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem93 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem94 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem95 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem96 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem97 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem98 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem99 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem100 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem102 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem103 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem104 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem105 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem106 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem107 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem108 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem109 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem110 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem111 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem112 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem113 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem114 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem115 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem116 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem117 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem118 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem119 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem120 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem121 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem122 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem123 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem124 = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItem4 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTimeEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.skinPaletteRibbonGalleryBarItem1 = new DevExpress.XtraBars.SkinPaletteRibbonGalleryBarItem();
            this.skinRibbonGalleryBarItem1 = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.barButtonItem101 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage5 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup7 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup20 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup25 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup26 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup27 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup28 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup48 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup49 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup50 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup51 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ا = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup11 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup44 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup12 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup13 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup45 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup46 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup52 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage4 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup18 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup19 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup57 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage3 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup85 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup86 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup87 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup14 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup53 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup54 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup15 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup16 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup55 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup56 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage10 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup36 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup37 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup38 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ل = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup32 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup60 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup39 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup40 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup61 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup41 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup42 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup62 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage9 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup30 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup31 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup33 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup34 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage6 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup8 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup21 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup22 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup43 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup47 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup35 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage8 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup10 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup29 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup59 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup66 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup67 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage7 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup9 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup71 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup73 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup74 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup75 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup76 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup79 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup81 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup78 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup82 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup77 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup80 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup83 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup84 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage11 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup23 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup63 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage12 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup24 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup64 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup65 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.tileItem7 = new DevExpress.XtraEditors.TileItem();
            this.tileItem8 = new DevExpress.XtraEditors.TileItem();
            this.tileItem9 = new DevExpress.XtraEditors.TileItem();
            this.tileItem10 = new DevExpress.XtraEditors.TileItem();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileItem5 = new DevExpress.XtraEditors.TileItem();
            this.tileItem6 = new DevExpress.XtraEditors.TileItem();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.tileItem2 = new DevExpress.XtraEditors.TileItem();
            this.tileItem3 = new DevExpress.XtraEditors.TileItem();
            this.tileItem4 = new DevExpress.XtraEditors.TileItem();
            this.tileItem1 = new DevExpress.XtraEditors.TileItem();
            this.barButtonItem63 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem64 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem65 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageGroup68 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup69 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup70 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup17 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup58 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.tileItem11 = new DevExpress.XtraEditors.TileItem();
            this.tileItem12 = new DevExpress.XtraEditors.TileItem();
            this.applicationMenu1 = new DevExpress.XtraBars.Ribbon.ApplicationMenu(this.components);
            this.ToolStrip2.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHypertextLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHypertextLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolStrip2
            // 
            this.ToolStrip2.BackColor = System.Drawing.Color.Tan;
            this.ToolStrip2.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToolStrip2.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.ToolStrip2.ImageScalingSize = new System.Drawing.Size(30, 30);
            this.ToolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CS_01,
            this.CS_10,
            this.CS_15,
            this.CS_29,
            this.CS_19,
            this.CS_24,
            this.Fills,
            this.CS_75,
            this.CS_53,
            this.CS_42,
            this.toolStripDropDownButton1,
            this.CS_58,
            this.CS_46,
            this.Username});
            this.ToolStrip2.Location = new System.Drawing.Point(0, 0);
            this.ToolStrip2.Name = "ToolStrip2";
            this.ToolStrip2.Padding = new System.Windows.Forms.Padding(0);
            this.ToolStrip2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ToolStrip2.Size = new System.Drawing.Size(1761, 37);
            this.ToolStrip2.TabIndex = 110;
            this.ToolStrip2.Text = "StarTec";
            this.ToolStrip2.Visible = false;
            // 
            // CS_01
            // 
            this.CS_01.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CS_02,
            this.CS_03,
            this.CS_04,
            this.CS_05,
            this.CS_06,
            this.CS_07,
            this.CS_09,
            this.بحثالاصنافToolStripMenuItem,
            this.الالوانToolStripMenuItem,
            this.الاحجامToolStripMenuItem,
            this.سندهالكاصنافToolStripMenuItem});
            this.CS_01.ForeColor = System.Drawing.Color.Black;
            this.CS_01.Image = global::ByStro.Properties.Resources.u;
            this.CS_01.Name = "CS_01";
            this.CS_01.Size = new System.Drawing.Size(92, 34);
            this.CS_01.Text = "المخزن";
            this.CS_01.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.CS_01.Click += new System.EventHandler(this.CS_01_Click);
            // 
            // CS_02
            // 
            this.CS_02.ForeColor = System.Drawing.Color.Black;
            this.CS_02.Image = global::ByStro.Properties.Resources.Move_by_Trolley_48px;
            this.CS_02.Name = "CS_02";
            this.CS_02.Size = new System.Drawing.Size(204, 36);
            this.CS_02.Text = " الاصناف";
            this.CS_02.Click += new System.EventHandler(this.Roles1_Click);
            // 
            // CS_03
            // 
            this.CS_03.Name = "CS_03";
            this.CS_03.Size = new System.Drawing.Size(204, 36);
            this.CS_03.Text = " المجموعات";
            this.CS_03.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // CS_04
            // 
            this.CS_04.Image = global::ByStro.Properties.Resources.Add_Tag_48px;
            this.CS_04.Name = "CS_04";
            this.CS_04.Size = new System.Drawing.Size(204, 36);
            this.CS_04.Text = "وحدات القياس";
            this.CS_04.Click += new System.EventHandler(this.unit__Click);
            // 
            // CS_05
            // 
            this.CS_05.ForeColor = System.Drawing.Color.Black;
            this.CS_05.Image = global::ByStro.Properties.Resources.smart_home;
            this.CS_05.Name = "CS_05";
            this.CS_05.Size = new System.Drawing.Size(204, 36);
            this.CS_05.Text = " المخازن";
            this.CS_05.Click += new System.EventHandler(this.Roles3_Click);
            // 
            // CS_06
            // 
            this.CS_06.Image = global::ByStro.Properties.Resources.Ingredients_48px;
            this.CS_06.Name = "CS_06";
            this.CS_06.Size = new System.Drawing.Size(204, 36);
            this.CS_06.Text = "اصناف بداية المدة ";
            this.CS_06.Click += new System.EventHandler(this.بدايةالمدةToolStripMenuItem_Click);
            // 
            // CS_07
            // 
            this.CS_07.Image = global::ByStro.Properties.Resources.Repeat_48px;
            this.CS_07.Name = "CS_07";
            this.CS_07.Size = new System.Drawing.Size(204, 36);
            this.CS_07.Text = "التحويل بين المخازن";
            this.CS_07.Click += new System.EventHandler(this.التحويلبينالمخازنToolStripMenuItem_Click);
            // 
            // CS_09
            // 
            this.CS_09.Image = global::ByStro.Properties.Resources.xbuy;
            this.CS_09.Name = "CS_09";
            this.CS_09.Size = new System.Drawing.Size(204, 36);
            this.CS_09.Text = "تسوية المخزون";
            this.CS_09.Click += new System.EventHandler(this.تسويةالمخازنToolStripMenuItem_Click);
            // 
            // بحثالاصنافToolStripMenuItem
            // 
            this.بحثالاصنافToolStripMenuItem.Image = global::ByStro.Properties.Resources._1;
            this.بحثالاصنافToolStripMenuItem.Name = "بحثالاصنافToolStripMenuItem";
            this.بحثالاصنافToolStripMenuItem.Size = new System.Drawing.Size(204, 36);
            this.بحثالاصنافToolStripMenuItem.Text = "بحث الاصناف";
            this.بحثالاصنافToolStripMenuItem.Click += new System.EventHandler(this.بحثالاصنافToolStripMenuItem_Click);
            // 
            // الالوانToolStripMenuItem
            // 
            this.الالوانToolStripMenuItem.Image = global::ByStro.Properties.Resources.icons8_Plus_301;
            this.الالوانToolStripMenuItem.Name = "الالوانToolStripMenuItem";
            this.الالوانToolStripMenuItem.Size = new System.Drawing.Size(204, 36);
            this.الالوانToolStripMenuItem.Text = "الالوان";
            this.الالوانToolStripMenuItem.Click += new System.EventHandler(this.الالوانToolStripMenuItem_Click);
            // 
            // الاحجامToolStripMenuItem
            // 
            this.الاحجامToolStripMenuItem.Image = global::ByStro.Properties.Resources.Im_Add;
            this.الاحجامToolStripMenuItem.Name = "الاحجامToolStripMenuItem";
            this.الاحجامToolStripMenuItem.Size = new System.Drawing.Size(204, 36);
            this.الاحجامToolStripMenuItem.Text = "الاحجام";
            this.الاحجامToolStripMenuItem.Click += new System.EventHandler(this.الاحجامToolStripMenuItem_Click);
            // 
            // سندهالكاصنافToolStripMenuItem
            // 
            this.سندهالكاصنافToolStripMenuItem.Image = global::ByStro.Properties.Resources.full_trash_bin_icone_8843_128;
            this.سندهالكاصنافToolStripMenuItem.Name = "سندهالكاصنافToolStripMenuItem";
            this.سندهالكاصنافToolStripMenuItem.Size = new System.Drawing.Size(204, 36);
            this.سندهالكاصنافToolStripMenuItem.Text = "سند هالك اصناف";
            this.سندهالكاصنافToolStripMenuItem.Click += new System.EventHandler(this.سندهالكاصنافToolStripMenuItem_Click);
            // 
            // CS_10
            // 
            this.CS_10.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CS_11,
            this.CS_14,
            this.casherToolStripMenuItem,
            this.فاتورةعرضسعرToolStripMenuItem,
            this.التوصيلToolStripMenuItem});
            this.CS_10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CS_10.Image = global::ByStro.Properties.Resources.sales;
            this.CS_10.Name = "CS_10";
            this.CS_10.Size = new System.Drawing.Size(135, 34);
            this.CS_10.Text = "فاتوره المبيعات";
            // 
            // CS_11
            // 
            this.CS_11.Image = global::ByStro.Properties.Resources.sales;
            this.CS_11.Name = "CS_11";
            this.CS_11.Size = new System.Drawing.Size(238, 36);
            this.CS_11.Text = " فاتورة مبيعات    Ctrl + Q ";
            this.CS_11.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // CS_14
            // 
            this.CS_14.Image = global::ByStro.Properties.Resources.sales1;
            this.CS_14.Name = "CS_14";
            this.CS_14.Size = new System.Drawing.Size(238, 36);
            this.CS_14.Text = "مرتجع المبيعات  Ctrl + W";
            this.CS_14.Click += new System.EventHandler(this.toolStripMenuItem8_Click);
            // 
            // casherToolStripMenuItem
            // 
            this.casherToolStripMenuItem.Image = global::ByStro.Properties.Resources.sales;
            this.casherToolStripMenuItem.Name = "casherToolStripMenuItem";
            this.casherToolStripMenuItem.Size = new System.Drawing.Size(238, 36);
            this.casherToolStripMenuItem.Text = "مبيعات تاتش";
            this.casherToolStripMenuItem.Click += new System.EventHandler(this.casherToolStripMenuItem_Click);
            // 
            // فاتورةعرضسعرToolStripMenuItem
            // 
            this.فاتورةعرضسعرToolStripMenuItem.Image = global::ByStro.Properties.Resources.sales1;
            this.فاتورةعرضسعرToolStripMenuItem.Name = "فاتورةعرضسعرToolStripMenuItem";
            this.فاتورةعرضسعرToolStripMenuItem.Size = new System.Drawing.Size(238, 36);
            this.فاتورةعرضسعرToolStripMenuItem.Text = "فاتورة عرض سعر";
            this.فاتورةعرضسعرToolStripMenuItem.Click += new System.EventHandler(this.فاتورةعرضسعرToolStripMenuItem_Click);
            // 
            // التوصيلToolStripMenuItem
            // 
            this.التوصيلToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.السائقونToolStripMenuItem,
            this.امرتوصيلجديدToolStripMenuItem,
            this.اوامرالتوصيلالجاريهToolStripMenuItem,
            this.اوامرالتوصيلالمنتهيةToolStripMenuItem});
            this.التوصيلToolStripMenuItem.Image = global::ByStro.Properties.Resources.Lift_Cart_Here_48px;
            this.التوصيلToolStripMenuItem.Name = "التوصيلToolStripMenuItem";
            this.التوصيلToolStripMenuItem.Size = new System.Drawing.Size(238, 36);
            this.التوصيلToolStripMenuItem.Text = "التوصيل";
            this.التوصيلToolStripMenuItem.Click += new System.EventHandler(this.التوصيلToolStripMenuItem_Click);
            // 
            // السائقونToolStripMenuItem
            // 
            this.السائقونToolStripMenuItem.Image = global::ByStro.Properties.Resources.Payroll_48px;
            this.السائقونToolStripMenuItem.Name = "السائقونToolStripMenuItem";
            this.السائقونToolStripMenuItem.Size = new System.Drawing.Size(214, 36);
            this.السائقونToolStripMenuItem.Text = "السائقون";
            this.السائقونToolStripMenuItem.Click += new System.EventHandler(this.السائقونToolStripMenuItem_Click);
            // 
            // امرتوصيلجديدToolStripMenuItem
            // 
            this.امرتوصيلجديدToolStripMenuItem.Image = global::ByStro.Properties.Resources.Plus_25px;
            this.امرتوصيلجديدToolStripMenuItem.Name = "امرتوصيلجديدToolStripMenuItem";
            this.امرتوصيلجديدToolStripMenuItem.Size = new System.Drawing.Size(214, 36);
            this.امرتوصيلجديدToolStripMenuItem.Text = "امر توصيل جديد";
            this.امرتوصيلجديدToolStripMenuItem.Click += new System.EventHandler(this.امرتوصيلجديدToolStripMenuItem_Click);
            // 
            // اوامرالتوصيلالجاريهToolStripMenuItem
            // 
            this.اوامرالتوصيلالجاريهToolStripMenuItem.Image = global::ByStro.Properties.Resources.Move_by_Trolley_48px;
            this.اوامرالتوصيلالجاريهToolStripMenuItem.Name = "اوامرالتوصيلالجاريهToolStripMenuItem";
            this.اوامرالتوصيلالجاريهToolStripMenuItem.Size = new System.Drawing.Size(214, 36);
            this.اوامرالتوصيلالجاريهToolStripMenuItem.Text = "اوامر التوصيل الجاريه";
            this.اوامرالتوصيلالجاريهToolStripMenuItem.Click += new System.EventHandler(this.اوامرالتوصيلالجاريهToolStripMenuItem_Click);
            // 
            // اوامرالتوصيلالمنتهيةToolStripMenuItem
            // 
            this.اوامرالتوصيلالمنتهيةToolStripMenuItem.Image = global::ByStro.Properties.Resources.money;
            this.اوامرالتوصيلالمنتهيةToolStripMenuItem.Name = "اوامرالتوصيلالمنتهيةToolStripMenuItem";
            this.اوامرالتوصيلالمنتهيةToolStripMenuItem.Size = new System.Drawing.Size(214, 36);
            this.اوامرالتوصيلالمنتهيةToolStripMenuItem.Text = "اوامر التوصيل المنتهية";
            this.اوامرالتوصيلالمنتهيةToolStripMenuItem.Click += new System.EventHandler(this.اوامرالتوصيلالمنتهيةToolStripMenuItem_Click);
            // 
            // CS_15
            // 
            this.CS_15.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CS11,
            this.CS_18,
            this.فاتورةطلبالشراءToolStripMenuItem});
            this.CS_15.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CS_15.Image = global::ByStro.Properties.Resources.xbuy;
            this.CS_15.Name = "CS_15";
            this.CS_15.Size = new System.Drawing.Size(112, 34);
            this.CS_15.Text = "المشتريات";
            // 
            // CS11
            // 
            this.CS11.Image = global::ByStro.Properties.Resources.xbuy;
            this.CS11.Name = "CS11";
            this.CS11.Size = new System.Drawing.Size(246, 36);
            this.CS11.Text = "فاتورة مشتريات     Ctrl + E";
            this.CS11.Click += new System.EventHandler(this.Roles5_Click);
            // 
            // CS_18
            // 
            this.CS_18.Image = global::ByStro.Properties.Resources.xbuy;
            this.CS_18.Name = "CS_18";
            this.CS_18.Size = new System.Drawing.Size(246, 36);
            this.CS_18.Text = "مرتجع المشتريات  Ctrl + R";
            this.CS_18.Click += new System.EventHandler(this.Roles8_Click);
            // 
            // فاتورةطلبالشراءToolStripMenuItem
            // 
            this.فاتورةطلبالشراءToolStripMenuItem.Image = global::ByStro.Properties.Resources.Move_by_Trolley_48px;
            this.فاتورةطلبالشراءToolStripMenuItem.Name = "فاتورةطلبالشراءToolStripMenuItem";
            this.فاتورةطلبالشراءToolStripMenuItem.Size = new System.Drawing.Size(246, 36);
            this.فاتورةطلبالشراءToolStripMenuItem.Text = "فاتورة طلب الشراء";
            this.فاتورةطلبالشراءToolStripMenuItem.Click += new System.EventHandler(this.فاتورةطلبالشراءToolStripMenuItem_Click);
            // 
            // CS_29
            // 
            this.CS_29.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CS_30,
            this.CS_31,
            this.CS_32,
            this.CS_33,
            this.CS_34,
            this.CS_35,
            this.CS_36,
            this.CS_37,
            this.CS_38,
            this.CS_39,
            this.CS_40,
            this.CS_41});
            this.CS_29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CS_29.Image = global::ByStro.Properties.Resources.Customers;
            this.CS_29.Name = "CS_29";
            this.CS_29.Size = new System.Drawing.Size(105, 34);
            this.CS_29.Text = "الموظفين";
            this.CS_29.Click += new System.EventHandler(this.CS_29_Click);
            // 
            // CS_30
            // 
            this.CS_30.Image = global::ByStro.Properties.Resources.Customers;
            this.CS_30.Name = "CS_30";
            this.CS_30.Size = new System.Drawing.Size(237, 36);
            this.CS_30.Text = "الموظفين";
            this.CS_30.Click += new System.EventHandler(this.CHEmployee_frm_Click);
            // 
            // CS_31
            // 
            this.CS_31.Image = ((System.Drawing.Image)(resources.GetObject("CS_31.Image")));
            this.CS_31.Name = "CS_31";
            this.CS_31.Size = new System.Drawing.Size(237, 36);
            this.CS_31.Text = "أنوع الوظائف في الشركة";
            this.CS_31.Click += new System.EventHandler(this.CHWorkPartCompany_frm_Click);
            // 
            // CS_32
            // 
            this.CS_32.Image = global::ByStro.Properties.Resources.account_balance;
            this.CS_32.Name = "CS_32";
            this.CS_32.Size = new System.Drawing.Size(237, 36);
            this.CS_32.Text = "اقسام العمل في الشركة";
            this.CS_32.Click += new System.EventHandler(this.CHPartCompany_frm_Click);
            // 
            // CS_33
            // 
            this.CS_33.Image = global::ByStro.Properties.Resources.Employee_Card_48px;
            this.CS_33.Name = "CS_33";
            this.CS_33.Size = new System.Drawing.Size(237, 36);
            this.CS_33.Text = "انـواع الاجازات في الشركة";
            this.CS_33.Click += new System.EventHandler(this.toolStripMenuItem1_Click_1);
            // 
            // CS_34
            // 
            this.CS_34.Image = global::ByStro.Properties.Resources.Banknotes_96px;
            this.CS_34.Name = "CS_34";
            this.CS_34.Size = new System.Drawing.Size(237, 36);
            this.CS_34.Text = "انواع الاستحقاقات";
            this.CS_34.Click += new System.EventHandler(this.CS28_Click);
            // 
            // CS_35
            // 
            this.CS_35.Image = global::ByStro.Properties.Resources.Pic030;
            this.CS_35.Name = "CS_35";
            this.CS_35.Size = new System.Drawing.Size(237, 36);
            this.CS_35.Text = "انواع الاستقطاعات";
            this.CS_35.ToolTipText = "تعريف الاستحقاقات";
            this.CS_35.Click += new System.EventHandler(this.CS29_Click);
            // 
            // CS_36
            // 
            this.CS_36.Image = global::ByStro.Properties.Resources.Employee_Card_48px;
            this.CS_36.Name = "CS_36";
            this.CS_36.Size = new System.Drawing.Size(237, 36);
            this.CS_36.Text = "تسجيل أجازة";
            this.CS_36.Click += new System.EventHandler(this.chEmployee_Holiday_Click);
            // 
            // CS_37
            // 
            this.CS_37.Image = global::ByStro.Properties.Resources.Realtime_Protection_48px;
            this.CS_37.Name = "CS_37";
            this.CS_37.Size = new System.Drawing.Size(237, 36);
            this.CS_37.Text = "تسجيل حضور ";
            this.CS_37.Click += new System.EventHandler(this.CS25_Click);
            // 
            // CS_38
            // 
            this.CS_38.Image = global::ByStro.Properties.Resources.Pic010;
            this.CS_38.Name = "CS_38";
            this.CS_38.Size = new System.Drawing.Size(237, 36);
            this.CS_38.Text = "تسجيل انصراف";
            this.CS_38.Click += new System.EventHandler(this.CS26_Click);
            // 
            // CS_39
            // 
            this.CS_39.Image = global::ByStro.Properties.Resources.Banknotes_96px;
            this.CS_39.Name = "CS_39";
            this.CS_39.Size = new System.Drawing.Size(237, 36);
            this.CS_39.Text = "تسجيل استحقاق";
            this.CS_39.Click += new System.EventHandler(this.Employee_Additions_frm_Click);
            // 
            // CS_40
            // 
            this.CS_40.Image = global::ByStro.Properties.Resources.Pic030;
            this.CS_40.Name = "CS_40";
            this.CS_40.Size = new System.Drawing.Size(237, 36);
            this.CS_40.Text = "تسجيل استقطاع";
            this.CS_40.Click += new System.EventHandler(this.CHDiscound_frm_Click);
            // 
            // CS_41
            // 
            this.CS_41.Image = global::ByStro.Properties.Resources.Paid_Parking_48px;
            this.CS_41.Name = "CS_41";
            this.CS_41.Size = new System.Drawing.Size(237, 36);
            this.CS_41.Text = "دفع مرتبات";
            this.CS_41.Click += new System.EventHandler(this.CS30_Click);
            // 
            // CS_19
            // 
            this.CS_19.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CS_20,
            this.CustomersFirst_frm,
            this.CS_21,
            this.CS_22,
            this.CS_23});
            this.CS_19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CS_19.Image = global::ByStro.Properties.Resources.customerss;
            this.CS_19.Name = "CS_19";
            this.CS_19.Size = new System.Drawing.Size(89, 34);
            this.CS_19.Text = "العملاء";
            // 
            // CS_20
            // 
            this.CS_20.ForeColor = System.Drawing.Color.Black;
            this.CS_20.Image = global::ByStro.Properties.Resources.customerss;
            this.CS_20.Name = "CS_20";
            this.CS_20.Size = new System.Drawing.Size(220, 36);
            this.CS_20.Text = "العملاء";
            this.CS_20.Click += new System.EventHandler(this.Roles9_Click);
            // 
            // CustomersFirst_frm
            // 
            this.CustomersFirst_frm.Image = global::ByStro.Properties.Resources.account_balance;
            this.CustomersFirst_frm.Name = "CustomersFirst_frm";
            this.CustomersFirst_frm.Size = new System.Drawing.Size(220, 36);
            this.CustomersFirst_frm.Text = "رصيد اول المدة للعملاء";
            this.CustomersFirst_frm.Click += new System.EventHandler(this.CustomersFirst_frm_Click);
            // 
            // CS_21
            // 
            this.CS_21.ForeColor = System.Drawing.Color.Black;
            this.CS_21.Image = global::ByStro.Properties.Resources.Transfer_Between_Users_48px;
            this.CS_21.Name = "CS_21";
            this.CS_21.Size = new System.Drawing.Size(220, 36);
            this.CS_21.Text = "كشف حساب العميل";
            this.CS_21.Click += new System.EventHandler(this.Roles11_Click);
            // 
            // CS_22
            // 
            this.CS_22.ForeColor = System.Drawing.Color.Black;
            this.CS_22.Image = global::ByStro.Properties.Resources.payment_icon;
            this.CS_22.Name = "CS_22";
            this.CS_22.Size = new System.Drawing.Size(220, 36);
            this.CS_22.Text = "سند قبض من عميل";
            this.CS_22.Click += new System.EventHandler(this.CHCustomersPayment_Click);
            // 
            // CS_23
            // 
            this.CS_23.Image = global::ByStro.Properties.Resources.Paid_Parking_48px;
            this.CS_23.Name = "CS_23";
            this.CS_23.Size = new System.Drawing.Size(220, 36);
            this.CS_23.Text = "سند دفع للعميل";
            this.CS_23.Click += new System.EventHandler(this.CHPayCustomer_Click);
            // 
            // CS_24
            // 
            this.CS_24.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CS_25,
            this.CS25,
            this.CS_26,
            this.CS_27,
            this.CS_28});
            this.CS_24.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CS_24.Image = global::ByStro.Properties.Resources.supp;
            this.CS_24.Name = "CS_24";
            this.CS_24.ShowDropDownArrow = false;
            this.CS_24.Size = new System.Drawing.Size(91, 34);
            this.CS_24.Text = "الموردين";
            // 
            // CS_25
            // 
            this.CS_25.Image = global::ByStro.Properties.Resources.supp;
            this.CS_25.Name = "CS_25";
            this.CS_25.Size = new System.Drawing.Size(231, 36);
            this.CS_25.Text = "الموردين";
            this.CS_25.Click += new System.EventHandler(this.Roles14_Click);
            // 
            // CS25
            // 
            this.CS25.Image = global::ByStro.Properties.Resources.account_balance;
            this.CS25.Name = "CS25";
            this.CS25.Size = new System.Drawing.Size(231, 36);
            this.CS25.Text = "رصيد أول المدة للموردين";
            this.CS25.Click += new System.EventHandler(this.SC25_Click);
            // 
            // CS_26
            // 
            this.CS_26.Image = global::ByStro.Properties.Resources.Transfer_Between_Users_48px;
            this.CS_26.Name = "CS_26";
            this.CS_26.Size = new System.Drawing.Size(231, 36);
            this.CS_26.Text = "كشف حساب المورد";
            this.CS_26.Click += new System.EventHandler(this.Roles16_Click);
            // 
            // CS_27
            // 
            this.CS_27.Image = global::ByStro.Properties.Resources.payment_icon;
            this.CS_27.Name = "CS_27";
            this.CS_27.Size = new System.Drawing.Size(231, 36);
            this.CS_27.Text = "سند قبض من المورد";
            this.CS_27.Click += new System.EventHandler(this.SCSuppliersPayment_FormAdd_Click);
            // 
            // CS_28
            // 
            this.CS_28.Image = global::ByStro.Properties.Resources.Paid_Parking_48px;
            this.CS_28.Name = "CS_28";
            this.CS_28.Size = new System.Drawing.Size(231, 36);
            this.CS_28.Text = "سند الدفع للمورد";
            this.CS_28.Click += new System.EventHandler(this.CHSuppliersPayment_Click);
            // 
            // Fills
            // 
            this.Fills.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CreateNewCompany_frm,
            this.OpenNewCompany_frm,
            this.enuItem});
            this.Fills.Image = global::ByStro.Properties.Resources.Open_Folder_29px;
            this.Fills.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Fills.Name = "Fills";
            this.Fills.Size = new System.Drawing.Size(76, 34);
            this.Fills.Text = "ملف";
            this.Fills.Visible = false;
            // 
            // CreateNewCompany_frm
            // 
            this.CreateNewCompany_frm.Image = global::ByStro.Properties.Resources.New;
            this.CreateNewCompany_frm.Name = "CreateNewCompany_frm";
            this.CreateNewCompany_frm.Size = new System.Drawing.Size(221, 36);
            this.CreateNewCompany_frm.Text = "انشاء شركة جديدة";
            this.CreateNewCompany_frm.Click += new System.EventHandler(this.CreateNewCompany_frm_Click);
            // 
            // OpenNewCompany_frm
            // 
            this.OpenNewCompany_frm.Image = global::ByStro.Properties.Resources.Open_Folder_29px;
            this.OpenNewCompany_frm.Name = "OpenNewCompany_frm";
            this.OpenNewCompany_frm.Size = new System.Drawing.Size(221, 36);
            this.OpenNewCompany_frm.Text = "فتح شركة موجودة ";
            this.OpenNewCompany_frm.Click += new System.EventHandler(this.OpenNewCompany_frm_Click);
            // 
            // enuItem
            // 
            this.enuItem.Image = global::ByStro.Properties.Resources.Setting;
            this.enuItem.Name = "enuItem";
            this.enuItem.Size = new System.Drawing.Size(221, 36);
            this.enuItem.Text = "اعداد  الاتصال بالسيرفر";
            this.enuItem.Click += new System.EventHandler(this.enuItem_Click);
            // 
            // CS_75
            // 
            this.CS_75.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MaintenanceCompeny,
            this.CS_76,
            this.CS_77,
            this.CS_78,
            this.CS_79,
            this.BillMaintenance_frm});
            this.CS_75.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CS_75.Image = global::ByStro.Properties.Resources.Pic036;
            this.CS_75.Margin = new System.Windows.Forms.Padding(0, 1, 3, 2);
            this.CS_75.Name = "CS_75";
            this.CS_75.Size = new System.Drawing.Size(90, 34);
            this.CS_75.Text = "الصيانة";
            this.CS_75.Click += new System.EventHandler(this.CS_75_Click);
            // 
            // MaintenanceCompeny
            // 
            this.MaintenanceCompeny.Image = global::ByStro.Properties.Resources._20151113175851481;
            this.MaintenanceCompeny.Name = "MaintenanceCompeny";
            this.MaintenanceCompeny.Size = new System.Drawing.Size(201, 36);
            this.MaintenanceCompeny.Text = "الشركات المصنعة";
            this.MaintenanceCompeny.Click += new System.EventHandler(this.MaintenanceCompeny_Click);
            // 
            // CS_76
            // 
            this.CS_76.Image = ((System.Drawing.Image)(resources.GetObject("CS_76.Image")));
            this.CS_76.Name = "CS_76";
            this.CS_76.Size = new System.Drawing.Size(201, 36);
            this.CS_76.Text = "انواع الاجهزة";
            this.CS_76.Click += new System.EventHandler(this.toolStripMenuItem1_Click_2);
            // 
            // CS_77
            // 
            this.CS_77.Image = ((System.Drawing.Image)(resources.GetObject("CS_77.Image")));
            this.CS_77.Name = "CS_77";
            this.CS_77.Size = new System.Drawing.Size(201, 36);
            this.CS_77.Text = "انواع الاعطال";
            this.CS_77.Click += new System.EventHandler(this.toolStripMenuItem2_Click_1);
            // 
            // CS_78
            // 
            this.CS_78.Image = ((System.Drawing.Image)(resources.GetObject("CS_78.Image")));
            this.CS_78.Name = "CS_78";
            this.CS_78.Size = new System.Drawing.Size(201, 36);
            this.CS_78.Text = "سند استلام صيانة ";
            this.CS_78.Click += new System.EventHandler(this.sasaToolStripMenuItem_Click);
            // 
            // CS_79
            // 
            this.CS_79.Image = ((System.Drawing.Image)(resources.GetObject("CS_79.Image")));
            this.CS_79.Name = "CS_79";
            this.CS_79.Size = new System.Drawing.Size(201, 36);
            this.CS_79.Text = "سند صرف صيانة";
            this.CS_79.Click += new System.EventHandler(this.maintenanceReturnfrmToolStripMenuItem_Click);
            // 
            // BillMaintenance_frm
            // 
            this.BillMaintenance_frm.Image = global::ByStro.Properties.Resources.Write;
            this.BillMaintenance_frm.Name = "BillMaintenance_frm";
            this.BillMaintenance_frm.Size = new System.Drawing.Size(201, 36);
            this.BillMaintenance_frm.Text = "فاتورة صيانة";
            this.BillMaintenance_frm.Click += new System.EventHandler(this.BillMaintenance_frm_Click);
            // 
            // CS_53
            // 
            this.CS_53.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CS_74,
            this.Currency_frm,
            this.CS_54,
            this.CS_55,
            this.CS_56,
            this.CS_57,
            this.سندتحويلأموالToolStripMenuItem,
            this.الاقساطToolStripMenuItem,
            this.واردمنصرفخذنهToolStripMenuItem});
            this.CS_53.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CS_53.Image = global::ByStro.Properties.Resources.Treasury;
            this.CS_53.Name = "CS_53";
            this.CS_53.Size = new System.Drawing.Size(132, 34);
            this.CS_53.Text = "الايداع والصرف";
            this.CS_53.ToolTipText = "الايــداع والصـرف";
            // 
            // CS_74
            // 
            this.CS_74.Image = global::ByStro.Properties.Resources.Paid_Parking_48px;
            this.CS_74.Name = "CS_74";
            this.CS_74.Size = new System.Drawing.Size(248, 36);
            this.CS_74.Text = "انوع الدفع (الخزينة && البنوك)";
            this.CS_74.Click += new System.EventHandler(this.PayType_frm_Click);
            // 
            // Currency_frm
            // 
            this.Currency_frm.Image = global::ByStro.Properties.Resources.profits;
            this.Currency_frm.Name = "Currency_frm";
            this.Currency_frm.Size = new System.Drawing.Size(248, 36);
            this.Currency_frm.Text = "انواع العملات";
            this.Currency_frm.Click += new System.EventHandler(this.Currency_frm_Click);
            // 
            // CS_54
            // 
            this.CS_54.Image = global::ByStro.Properties.Resources.Pic030;
            this.CS_54.Name = "CS_54";
            this.CS_54.Size = new System.Drawing.Size(248, 36);
            this.CS_54.Text = "انواع المصروفات";
            this.CS_54.Click += new System.EventHandler(this.انواعالمصروفاتToolStripMenuItem_Click);
            // 
            // CS_55
            // 
            this.CS_55.Image = global::ByStro.Properties.Resources.q_copy;
            this.CS_55.Name = "CS_55";
            this.CS_55.Size = new System.Drawing.Size(248, 36);
            this.CS_55.Text = "تسجيل مصروف";
            this.CS_55.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // CS_56
            // 
            this.CS_56.Image = global::ByStro.Properties.Resources.Request_Money_48px;
            this.CS_56.Name = "CS_56";
            this.CS_56.Size = new System.Drawing.Size(248, 36);
            this.CS_56.Text = "سند ايداع نقدي";
            this.CS_56.Click += new System.EventHandler(this.توريدنقديةToolStripMenuItem_Click);
            // 
            // CS_57
            // 
            this.CS_57.Image = global::ByStro.Properties.Resources.Initiate_Money_Transfer_48px;
            this.CS_57.Name = "CS_57";
            this.CS_57.Size = new System.Drawing.Size(248, 36);
            this.CS_57.Text = "سند صرف نقدي     ";
            this.CS_57.Click += new System.EventHandler(this.يToolStripMenuItem_Click);
            // 
            // سندتحويلأموالToolStripMenuItem
            // 
            this.سندتحويلأموالToolStripMenuItem.Image = global::ByStro.Properties.Resources.Euro_Price_Tag_48px;
            this.سندتحويلأموالToolStripMenuItem.Name = "سندتحويلأموالToolStripMenuItem";
            this.سندتحويلأموالToolStripMenuItem.Size = new System.Drawing.Size(248, 36);
            this.سندتحويلأموالToolStripMenuItem.Text = "سند تحويل أموال";
            this.سندتحويلأموالToolStripMenuItem.Click += new System.EventHandler(this.سندتحويلأموالToolStripMenuItem_Click);
            // 
            // الاقساطToolStripMenuItem
            // 
            this.الاقساطToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.قائمهالاقساطToolStripMenuItem,
            this.اضافهقسطToolStripMenuItem1,
            this.سداددفعهقسطToolStripMenuItem,
            this.عرضالاقساطالمستحقهToolStripMenuItem,
            this.عرضالدفعاتالمسددهToolStripMenuItem});
            this.الاقساطToolStripMenuItem.Image = global::ByStro.Properties.Resources.Banknotes_96px;
            this.الاقساطToolStripMenuItem.Name = "الاقساطToolStripMenuItem";
            this.الاقساطToolStripMenuItem.Size = new System.Drawing.Size(248, 36);
            this.الاقساطToolStripMenuItem.Text = "الاقساط";
            // 
            // قائمهالاقساطToolStripMenuItem
            // 
            this.قائمهالاقساطToolStripMenuItem.Image = global::ByStro.Properties.Resources.financial_32x32;
            this.قائمهالاقساطToolStripMenuItem.Name = "قائمهالاقساطToolStripMenuItem";
            this.قائمهالاقساطToolStripMenuItem.Size = new System.Drawing.Size(235, 36);
            this.قائمهالاقساطToolStripMenuItem.Text = "قائمه الاقساط";
            this.قائمهالاقساطToolStripMenuItem.Click += new System.EventHandler(this.قائمهالاقساطToolStripMenuItem_Click);
            // 
            // اضافهقسطToolStripMenuItem1
            // 
            this.اضافهقسطToolStripMenuItem1.Image = global::ByStro.Properties.Resources.icons8_Plus_301;
            this.اضافهقسطToolStripMenuItem1.Name = "اضافهقسطToolStripMenuItem1";
            this.اضافهقسطToolStripMenuItem1.Size = new System.Drawing.Size(235, 36);
            this.اضافهقسطToolStripMenuItem1.Text = "اضافه قسط";
            this.اضافهقسطToolStripMenuItem1.Click += new System.EventHandler(this.اضافهقسطToolStripMenuItem_Click);
            // 
            // سداددفعهقسطToolStripMenuItem
            // 
            this.سداددفعهقسطToolStripMenuItem.Image = global::ByStro.Properties.Resources.Donate_48px;
            this.سداددفعهقسطToolStripMenuItem.Name = "سداددفعهقسطToolStripMenuItem";
            this.سداددفعهقسطToolStripMenuItem.Size = new System.Drawing.Size(235, 36);
            this.سداددفعهقسطToolStripMenuItem.Text = "سداد دفعه قسط";
            this.سداددفعهقسطToolStripMenuItem.Click += new System.EventHandler(this.سداددفعهقسطToolStripMenuItem_Click);
            // 
            // عرضالاقساطالمستحقهToolStripMenuItem
            // 
            this.عرضالاقساطالمستحقهToolStripMenuItem.Image = global::ByStro.Properties.Resources.listbox_32x32;
            this.عرضالاقساطالمستحقهToolStripMenuItem.Name = "عرضالاقساطالمستحقهToolStripMenuItem";
            this.عرضالاقساطالمستحقهToolStripMenuItem.Size = new System.Drawing.Size(235, 36);
            this.عرضالاقساطالمستحقهToolStripMenuItem.Text = "عرض الاقساط المستحقه";
            this.عرضالاقساطالمستحقهToolStripMenuItem.Click += new System.EventHandler(this.عرضالاقساطالمستحقهToolStripMenuItem_Click);
            // 
            // عرضالدفعاتالمسددهToolStripMenuItem
            // 
            this.عرضالدفعاتالمسددهToolStripMenuItem.Image = global::ByStro.Properties.Resources.Edit_25;
            this.عرضالدفعاتالمسددهToolStripMenuItem.Name = "عرضالدفعاتالمسددهToolStripMenuItem";
            this.عرضالدفعاتالمسددهToolStripMenuItem.Size = new System.Drawing.Size(235, 36);
            this.عرضالدفعاتالمسددهToolStripMenuItem.Text = "عرض الدفعات المسدده";
            this.عرضالدفعاتالمسددهToolStripMenuItem.Click += new System.EventHandler(this.عرضالدفعاتالمسددهToolStripMenuItem_Click);
            // 
            // واردمنصرفخذنهToolStripMenuItem
            // 
            this.واردمنصرفخذنهToolStripMenuItem.Image = global::ByStro.Properties.Resources.Safe_In_48px;
            this.واردمنصرفخذنهToolStripMenuItem.Name = "واردمنصرفخذنهToolStripMenuItem";
            this.واردمنصرفخذنهToolStripMenuItem.Size = new System.Drawing.Size(248, 36);
            this.واردمنصرفخذنهToolStripMenuItem.Text = "وارد منصرف خذنه";
            this.واردمنصرفخذنهToolStripMenuItem.Click += new System.EventHandler(this.واردمنصرفخذنهToolStripMenuItem_Click);
            // 
            // CS_42
            // 
            this.CS_42.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tripMenuItem,
            this.vToolStripMenuItem,
            this.صافيالربحاوالخسارهToolStripMenuItem,
            this.اكثرالاصنافربحاToolStripMenuItem,
            this.الارباحمنالعملاءToolStripMenuItem});
            this.CS_42.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CS_42.Image = global::ByStro.Properties.Resources.money;
            this.CS_42.Name = "CS_42";
            this.CS_42.Size = new System.Drawing.Size(141, 34);
            this.CS_42.Text = "الارباح والخسائر";
            // 
            // tripMenuItem
            // 
            this.tripMenuItem.Image = global::ByStro.Properties.Resources.Remove_Tag_48px;
            this.tripMenuItem.Name = "tripMenuItem";
            this.tripMenuItem.Size = new System.Drawing.Size(221, 36);
            this.tripMenuItem.Text = "الميزانية";
            this.tripMenuItem.Click += new System.EventHandler(this.tripMenuItem_Click);
            // 
            // vToolStripMenuItem
            // 
            this.vToolStripMenuItem.Image = global::ByStro.Properties.Resources.payment_icon;
            this.vToolStripMenuItem.Name = "vToolStripMenuItem";
            this.vToolStripMenuItem.Size = new System.Drawing.Size(221, 36);
            this.vToolStripMenuItem.Text = "رأس المال";
            this.vToolStripMenuItem.Click += new System.EventHandler(this.vToolStripMenuItem_Click);
            // 
            // صافيالربحاوالخسارهToolStripMenuItem
            // 
            this.صافيالربحاوالخسارهToolStripMenuItem.Image = global::ByStro.Properties.Resources.Banknotes_96px;
            this.صافيالربحاوالخسارهToolStripMenuItem.Name = "صافيالربحاوالخسارهToolStripMenuItem";
            this.صافيالربحاوالخسارهToolStripMenuItem.Size = new System.Drawing.Size(221, 36);
            this.صافيالربحاوالخسارهToolStripMenuItem.Text = "صافي الربح او الخساره";
            this.صافيالربحاوالخسارهToolStripMenuItem.Click += new System.EventHandler(this.صافيالربحاوالخسارهToolStripMenuItem_Click);
            // 
            // اكثرالاصنافربحاToolStripMenuItem
            // 
            this.اكثرالاصنافربحاToolStripMenuItem.Image = global::ByStro.Properties.Resources.Donate_48px;
            this.اكثرالاصنافربحاToolStripMenuItem.Name = "اكثرالاصنافربحاToolStripMenuItem";
            this.اكثرالاصنافربحاToolStripMenuItem.Size = new System.Drawing.Size(221, 36);
            this.اكثرالاصنافربحاToolStripMenuItem.Text = "اكثر الاصناف ربحاً";
            this.اكثرالاصنافربحاToolStripMenuItem.Click += new System.EventHandler(this.اكثرالاصنافربحاToolStripMenuItem_Click);
            // 
            // الارباحمنالعملاءToolStripMenuItem
            // 
            this.الارباحمنالعملاءToolStripMenuItem.Image = global::ByStro.Properties.Resources.businessman;
            this.الارباحمنالعملاءToolStripMenuItem.Name = "الارباحمنالعملاءToolStripMenuItem";
            this.الارباحمنالعملاءToolStripMenuItem.Size = new System.Drawing.Size(221, 36);
            this.الارباحمنالعملاءToolStripMenuItem.Text = "الارباح من العملاء";
            this.الارباحمنالعملاءToolStripMenuItem.Click += new System.EventHandler(this.الارباحمنالعملاءToolStripMenuItem_Click);
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem4,
            this.toolStripMenuItem6});
            this.toolStripDropDownButton1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.toolStripDropDownButton1.Image = global::ByStro.Properties.Resources.sport;
            this.toolStripDropDownButton1.Margin = new System.Windows.Forms.Padding(0, 1, 3, 2);
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(121, 34);
            this.toolStripDropDownButton1.Text = "الدعم الفني";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Image = global::ByStro.Properties.Resources.supp;
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(232, 36);
            this.toolStripMenuItem4.Text = "الدعم الفني والاستفسار";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Image = global::ByStro.Properties.Resources.Untitled_14;
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(232, 36);
            this.toolStripMenuItem6.Text = "تفعيل البرنامج";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.toolStripMenuItem6_Click);
            // 
            // CS_58
            // 
            this.CS_58.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CS_59,
            this.CS_60,
            this.CS_81,
            this.CS_61,
            this.CS_62,
            this.CS_66,
            this.CS_67,
            this.CS_68,
            this.CS_69,
            this.CS_80,
            this.CS_70,
            this.CS_71,
            this.CS_72,
            this.طباعةباركودToolStripMenuItem,
            this.حركهصنفتفصيليToolStripMenuItem,
            this.ارصدهالاصنافToolStripMenuItem,
            this.صلاحياتالاصنافToolStripMenuItem,
            this.اصنافوصلتاليحدالطلبToolStripMenuItem});
            this.CS_58.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CS_58.Image = global::ByStro.Properties.Resources.Reports;
            this.CS_58.Name = "CS_58";
            this.CS_58.Size = new System.Drawing.Size(93, 34);
            this.CS_58.Text = "التقاريـر";
            // 
            // CS_59
            // 
            this.CS_59.ForeColor = System.Drawing.Color.Black;
            this.CS_59.Image = global::ByStro.Properties.Resources.q_copy;
            this.CS_59.Name = "CS_59";
            this.CS_59.Size = new System.Drawing.Size(259, 36);
            this.CS_59.Text = "تقرير المصروفات";
            this.CS_59.Click += new System.EventHandler(this.Roles30_Click);
            // 
            // CS_60
            // 
            this.CS_60.Image = global::ByStro.Properties.Resources.Treasury;
            this.CS_60.Name = "CS_60";
            this.CS_60.Size = new System.Drawing.Size(259, 36);
            this.CS_60.Text = "كشف حساب الخزينة";
            this.CS_60.Click += new System.EventHandler(this.CHTreasuryMovement_Click);
            // 
            // CS_81
            // 
            this.CS_81.Image = global::ByStro.Properties.Resources.Paid_Parking_48px;
            this.CS_81.Name = "CS_81";
            this.CS_81.Size = new System.Drawing.Size(259, 36);
            this.CS_81.Text = "تقرير نوع الدفع";
            this.CS_81.Click += new System.EventHandler(this.PayType_Trans_frm_Click);
            // 
            // CS_61
            // 
            this.CS_61.Image = global::ByStro.Properties.Resources.Box_Filled_96px;
            this.CS_61.Name = "CS_61";
            this.CS_61.Size = new System.Drawing.Size(259, 36);
            this.CS_61.Text = "تقريــر اليومية العامة";
            this.CS_61.Click += new System.EventHandler(this.تقريرمجمعToolStripMenuItem_Click);
            // 
            // CS_62
            // 
            this.CS_62.Image = global::ByStro.Properties.Resources.inventory_icon;
            this.CS_62.Name = "CS_62";
            this.CS_62.Size = new System.Drawing.Size(259, 36);
            this.CS_62.Text = "تقرير اصناف أول المدة";
            this.CS_62.Click += new System.EventHandler(this.CHRbtProdecuts_Firest_Store_Click);
            // 
            // CS_66
            // 
            this.CS_66.Image = global::ByStro.Properties.Resources.customerss;
            this.CS_66.Name = "CS_66";
            this.CS_66.Size = new System.Drawing.Size(259, 36);
            this.CS_66.Text = "تقرير بأرصدة العملاء";
            this.CS_66.Click += new System.EventHandler(this.CSRbtCustomer_frm_Click);
            // 
            // CS_67
            // 
            this.CS_67.Image = global::ByStro.Properties.Resources.customerss;
            this.CS_67.Name = "CS_67";
            this.CS_67.Size = new System.Drawing.Size(259, 36);
            this.CS_67.Text = "كشف حساب عميل ";
            this.CS_67.Click += new System.EventHandler(this.Roles33_Click);
            // 
            // CS_68
            // 
            this.CS_68.Image = global::ByStro.Properties.Resources.supp;
            this.CS_68.Name = "CS_68";
            this.CS_68.Size = new System.Drawing.Size(259, 36);
            this.CS_68.Text = "تقرير بأرصدة الموردين";
            this.CS_68.Click += new System.EventHandler(this.SCRbtSuppliers_frm_Click);
            // 
            // CS_69
            // 
            this.CS_69.Image = global::ByStro.Properties.Resources.supp;
            this.CS_69.Name = "CS_69";
            this.CS_69.Size = new System.Drawing.Size(259, 36);
            this.CS_69.Text = "كشف حساب مورد";
            this.CS_69.Click += new System.EventHandler(this.Roles34_Click);
            // 
            // CS_80
            // 
            this.CS_80.Image = global::ByStro.Properties.Resources.Pic036;
            this.CS_80.Name = "CS_80";
            this.CS_80.Size = new System.Drawing.Size(259, 36);
            this.CS_80.Text = "تقرير  الصيانة ";
            this.CS_80.Click += new System.EventHandler(this.ReportMaintenance_frm_Click);
            // 
            // CS_70
            // 
            this.CS_70.Image = global::ByStro.Properties.Resources.Realtime_Protection_48px;
            this.CS_70.Name = "CS_70";
            this.CS_70.Size = new System.Drawing.Size(259, 36);
            this.CS_70.Text = "تقرير حضور وانصراف الموظفين";
            this.CS_70.Click += new System.EventHandler(this.CHRPTEmployeeAttendance_frm_Click);
            // 
            // CS_71
            // 
            this.CS_71.Image = global::ByStro.Properties.Resources.Pic010;
            this.CS_71.Name = "CS_71";
            this.CS_71.Size = new System.Drawing.Size(259, 36);
            this.CS_71.Text = "تقرير مجمع للحضور والانصراف";
            this.CS_71.Click += new System.EventHandler(this.RPTEmployee_Total_Attendance_frm_Click);
            // 
            // CS_72
            // 
            this.CS_72.Image = global::ByStro.Properties.Resources.account_balance;
            this.CS_72.Name = "CS_72";
            this.CS_72.Size = new System.Drawing.Size(259, 36);
            this.CS_72.Text = "تقرير بمرتبات الموظفين";
            this.CS_72.Click += new System.EventHandler(this.RptEmployee_Salary_frm_Click);
            // 
            // طباعةباركودToolStripMenuItem
            // 
            this.طباعةباركودToolStripMenuItem.Image = global::ByStro.Properties.Resources.barcode_512;
            this.طباعةباركودToolStripMenuItem.Name = "طباعةباركودToolStripMenuItem";
            this.طباعةباركودToolStripMenuItem.Size = new System.Drawing.Size(259, 36);
            this.طباعةباركودToolStripMenuItem.Text = "طباعة باركود";
            this.طباعةباركودToolStripMenuItem.Click += new System.EventHandler(this.طباعةباركودToolStripMenuItem_Click);
            // 
            // حركهصنفتفصيليToolStripMenuItem
            // 
            this.حركهصنفتفصيليToolStripMenuItem.Image = global::ByStro.Properties.Resources.Purchase_Order_48px;
            this.حركهصنفتفصيليToolStripMenuItem.Name = "حركهصنفتفصيليToolStripMenuItem";
            this.حركهصنفتفصيليToolStripMenuItem.Size = new System.Drawing.Size(259, 36);
            this.حركهصنفتفصيليToolStripMenuItem.Text = "حركه صنف تفصيلي ";
            this.حركهصنفتفصيليToolStripMenuItem.Click += new System.EventHandler(this.حركهصنفتفصيليToolStripMenuItem_Click);
            // 
            // ارصدهالاصنافToolStripMenuItem
            // 
            this.ارصدهالاصنافToolStripMenuItem.Image = global::ByStro.Properties.Resources.Request_Money_48px;
            this.ارصدهالاصنافToolStripMenuItem.Name = "ارصدهالاصنافToolStripMenuItem";
            this.ارصدهالاصنافToolStripMenuItem.Size = new System.Drawing.Size(259, 36);
            this.ارصدهالاصنافToolStripMenuItem.Text = "ارصده الاصناف";
            this.ارصدهالاصنافToolStripMenuItem.Click += new System.EventHandler(this.ارصدهالاصنافToolStripMenuItem_Click);
            // 
            // صلاحياتالاصنافToolStripMenuItem
            // 
            this.صلاحياتالاصنافToolStripMenuItem.Image = global::ByStro.Properties.Resources.Tags_48px;
            this.صلاحياتالاصنافToolStripMenuItem.Name = "صلاحياتالاصنافToolStripMenuItem";
            this.صلاحياتالاصنافToolStripMenuItem.Size = new System.Drawing.Size(259, 36);
            this.صلاحياتالاصنافToolStripMenuItem.Text = "صلاحيات الاصناف";
            this.صلاحياتالاصنافToolStripMenuItem.Click += new System.EventHandler(this.صلاحياتالاصنافToolStripMenuItem_Click);
            // 
            // اصنافوصلتاليحدالطلبToolStripMenuItem
            // 
            this.اصنافوصلتاليحدالطلبToolStripMenuItem.Image = global::ByStro.Properties.Resources.u;
            this.اصنافوصلتاليحدالطلبToolStripMenuItem.Name = "اصنافوصلتاليحدالطلبToolStripMenuItem";
            this.اصنافوصلتاليحدالطلبToolStripMenuItem.Size = new System.Drawing.Size(259, 36);
            this.اصنافوصلتاليحدالطلبToolStripMenuItem.Text = "اصناف وصلت الي حد الطلب";
            this.اصنافوصلتاليحدالطلبToolStripMenuItem.Click += new System.EventHandler(this.اصنافوصلتاليحدالطلبToolStripMenuItem_Click);
            // 
            // CS_46
            // 
            this.CS_46.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CS_47,
            this.CS_48,
            this.CS_49,
            this.CS_50,
            this.CS_51,
            this.CS_52,
            this.ChangeFont,
            this.CHClearAllData,
            this.CS_73,
            this.Maintenance_frm,
            this.نماذجالباركودToolStripMenuItem,
            this.اعداداتالقرائهمنالميزانToolStripMenuItem,
            this.تغييرالخلفيهToolStripMenuItem});
            this.CS_46.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CS_46.Image = global::ByStro.Properties.Resources.Setting;
            this.CS_46.Name = "CS_46";
            this.CS_46.Size = new System.Drawing.Size(90, 34);
            this.CS_46.Text = "التحكم";
            // 
            // CS_47
            // 
            this.CS_47.Image = global::ByStro.Properties.Resources._1420761544_Login;
            this.CS_47.Name = "CS_47";
            this.CS_47.Size = new System.Drawing.Size(257, 36);
            this.CS_47.Text = "صلاحيات المستحدمين";
            this.CS_47.Click += new System.EventHandler(this.SCUserPermetion_Click);
            // 
            // CS_48
            // 
            this.CS_48.Image = global::ByStro.Properties.Resources.dbrestore;
            this.CS_48.Name = "CS_48";
            this.CS_48.Size = new System.Drawing.Size(257, 36);
            this.CS_48.Text = "النسخ الاحتياطي والاسترجاع";
            this.CS_48.Click += new System.EventHandler(this.Roles26_Click);
            // 
            // CS_49
            // 
            this.CS_49.Image = global::ByStro.Properties.Resources._20151113175851481;
            this.CS_49.Name = "CS_49";
            this.CS_49.Size = new System.Drawing.Size(257, 36);
            this.CS_49.Text = "بيانات الشركة ";
            this.CS_49.Click += new System.EventHandler(this.Roles27_Click);
            // 
            // CS_50
            // 
            this.CS_50.Image = global::ByStro.Properties.Resources.web_server_clipart_database_clipart_database_server_icon_Ejbd8h_clipart;
            this.CS_50.Name = "CS_50";
            this.CS_50.Size = new System.Drawing.Size(257, 36);
            this.CS_50.Text = "اعدادات الاتصال";
            this.CS_50.Click += new System.EventHandler(this.اعداداتالاتصالToolStripMenuItem_Click);
            // 
            // CS_51
            // 
            this.CS_51.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CHExelCustomer_frm,
            this.chExelSuppliers_frm,
            this.مجموعةاصنافToolStripMenuItem,
            this.الاصنافToolStripMenuItem});
            this.CS_51.Image = global::ByStro.Properties.Resources.Professional_VS_Do_it_Yourself_Data_Recovery;
            this.CS_51.Name = "CS_51";
            this.CS_51.Size = new System.Drawing.Size(257, 36);
            this.CS_51.Text = "تحميل داتا من ملف اكسل";
            // 
            // CHExelCustomer_frm
            // 
            this.CHExelCustomer_frm.Image = global::ByStro.Properties.Resources.Pic055;
            this.CHExelCustomer_frm.Name = "CHExelCustomer_frm";
            this.CHExelCustomer_frm.Size = new System.Drawing.Size(325, 36);
            this.CHExelCustomer_frm.Text = "تحميل العملاء من ملف اكسل";
            this.CHExelCustomer_frm.Click += new System.EventHandler(this.CHExelCustomer_frm_Click);
            // 
            // chExelSuppliers_frm
            // 
            this.chExelSuppliers_frm.Image = global::ByStro.Properties.Resources.Pic055;
            this.chExelSuppliers_frm.Name = "chExelSuppliers_frm";
            this.chExelSuppliers_frm.Size = new System.Drawing.Size(325, 36);
            this.chExelSuppliers_frm.Text = "تحميل الموردين من ملف اكسل";
            this.chExelSuppliers_frm.Click += new System.EventHandler(this.chExelSuppliers_frm_Click);
            // 
            // مجموعةاصنافToolStripMenuItem
            // 
            this.مجموعةاصنافToolStripMenuItem.Image = global::ByStro.Properties.Resources.Pic055;
            this.مجموعةاصنافToolStripMenuItem.Name = "مجموعةاصنافToolStripMenuItem";
            this.مجموعةاصنافToolStripMenuItem.Size = new System.Drawing.Size(325, 36);
            this.مجموعةاصنافToolStripMenuItem.Text = "تحميل مجموعات الاصناف من ملف اكسل";
            this.مجموعةاصنافToolStripMenuItem.Click += new System.EventHandler(this.مجموعةاصنافToolStripMenuItem_Click);
            // 
            // الاصنافToolStripMenuItem
            // 
            this.الاصنافToolStripMenuItem.Image = global::ByStro.Properties.Resources.Pic055;
            this.الاصنافToolStripMenuItem.Name = "الاصنافToolStripMenuItem";
            this.الاصنافToolStripMenuItem.Size = new System.Drawing.Size(325, 36);
            this.الاصنافToolStripMenuItem.Text = "تحميل الاصناف من ملف اكسل";
            this.الاصنافToolStripMenuItem.Visible = false;
            // 
            // CS_52
            // 
            this.CS_52.Image = global::ByStro.Properties.Resources.aaa;
            this.CS_52.Name = "CS_52";
            this.CS_52.Size = new System.Drawing.Size(257, 36);
            this.CS_52.Text = "بداية فترة جديدة";
            this.CS_52.Click += new System.EventHandler(this.CHCloseData_Form_Click);
            // 
            // ChangeFont
            // 
            this.ChangeFont.Image = ((System.Drawing.Image)(resources.GetObject("ChangeFont.Image")));
            this.ChangeFont.Name = "ChangeFont";
            this.ChangeFont.Size = new System.Drawing.Size(257, 36);
            this.ChangeFont.Text = "تغيير خط القائمة";
            this.ChangeFont.Click += new System.EventHandler(this.ChangeFont_Click);
            // 
            // CHClearAllData
            // 
            this.CHClearAllData.Image = global::ByStro.Properties.Resources.edit_clear__1_;
            this.CHClearAllData.Name = "CHClearAllData";
            this.CHClearAllData.Size = new System.Drawing.Size(257, 36);
            this.CHClearAllData.Text = "إعادة ضبط المصنع";
            this.CHClearAllData.Click += new System.EventHandler(this.CHClearAllData_Click);
            // 
            // CS_73
            // 
            this.CS_73.Image = global::ByStro.Properties.Resources.images__1_;
            this.CS_73.Name = "CS_73";
            this.CS_73.Size = new System.Drawing.Size(257, 36);
            this.CS_73.Text = "اعداد الفواتير";
            this.CS_73.Click += new System.EventHandler(this.CS_73_Click);
            // 
            // Maintenance_frm
            // 
            this.Maintenance_frm.Image = global::ByStro.Properties.Resources.backup;
            this.Maintenance_frm.Name = "Maintenance_frm";
            this.Maintenance_frm.Size = new System.Drawing.Size(257, 36);
            this.Maintenance_frm.Text = "صيانة قواعد البيانات";
            this.Maintenance_frm.Click += new System.EventHandler(this.صيانةقواعدالبياناتToolStripMenuItem_Click);
            // 
            // نماذجالباركودToolStripMenuItem
            // 
            this.نماذجالباركودToolStripMenuItem.Image = global::ByStro.Properties.Resources.barcode_512;
            this.نماذجالباركودToolStripMenuItem.Name = "نماذجالباركودToolStripMenuItem";
            this.نماذجالباركودToolStripMenuItem.Size = new System.Drawing.Size(257, 36);
            this.نماذجالباركودToolStripMenuItem.Text = "نماذج الباركود";
            this.نماذجالباركودToolStripMenuItem.Click += new System.EventHandler(this.نماذجالباركودToolStripMenuItem_Click);
            // 
            // اعداداتالقرائهمنالميزانToolStripMenuItem
            // 
            this.اعداداتالقرائهمنالميزانToolStripMenuItem.Name = "اعداداتالقرائهمنالميزانToolStripMenuItem";
            this.اعداداتالقرائهمنالميزانToolStripMenuItem.Size = new System.Drawing.Size(257, 36);
            this.اعداداتالقرائهمنالميزانToolStripMenuItem.Text = "اعدادات القرائه من الميزان";
            this.اعداداتالقرائهمنالميزانToolStripMenuItem.Click += new System.EventHandler(this.اعداداتالقرائهمنالميزانToolStripMenuItem_Click);
            // 
            // تغييرالخلفيهToolStripMenuItem
            // 
            this.تغييرالخلفيهToolStripMenuItem.Image = global::ByStro.Properties.Resources.edit;
            this.تغييرالخلفيهToolStripMenuItem.Name = "تغييرالخلفيهToolStripMenuItem";
            this.تغييرالخلفيهToolStripMenuItem.Size = new System.Drawing.Size(257, 36);
            this.تغييرالخلفيهToolStripMenuItem.Text = "تغيير الخلفيه ";
            this.تغييرالخلفيهToolStripMenuItem.Click += new System.EventHandler(this.تغييرالخلفيهToolStripMenuItem_Click);
            // 
            // Username
            // 
            this.Username.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.تغيركلمةالمرورToolStripMenuItem,
            this.تسجيلخروجToolStripMenuItem,
            this.خروجToolStripMenuItem});
            this.Username.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Username.Image = global::ByStro.Properties.Resources.Userlogin;
            this.Username.Margin = new System.Windows.Forms.Padding(0, 1, 3, 2);
            this.Username.Name = "Username";
            this.Username.Size = new System.Drawing.Size(113, 34);
            this.Username.Text = "المستخدم";
            this.Username.Click += new System.EventHandler(this.Username_Click);
            // 
            // تغيركلمةالمرورToolStripMenuItem
            // 
            this.تغيركلمةالمرورToolStripMenuItem.Image = global::ByStro.Properties.Resources._1420761544_Login;
            this.تغيركلمةالمرورToolStripMenuItem.Name = "تغيركلمةالمرورToolStripMenuItem";
            this.تغيركلمةالمرورToolStripMenuItem.Size = new System.Drawing.Size(182, 36);
            this.تغيركلمةالمرورToolStripMenuItem.Text = "تغير كلمة المرور";
            this.تغيركلمةالمرورToolStripMenuItem.Click += new System.EventHandler(this.تغيركلمةالمرورToolStripMenuItem_Click);
            // 
            // تسجيلخروجToolStripMenuItem
            // 
            this.تسجيلخروجToolStripMenuItem.Image = global::ByStro.Properties.Resources.Safe_In_48px;
            this.تسجيلخروجToolStripMenuItem.Name = "تسجيلخروجToolStripMenuItem";
            this.تسجيلخروجToolStripMenuItem.Size = new System.Drawing.Size(182, 36);
            this.تسجيلخروجToolStripMenuItem.Text = "تسجيل خروج ";
            this.تسجيلخروجToolStripMenuItem.Click += new System.EventHandler(this.تسجيلخروجToolStripMenuItem_Click);
            // 
            // خروجToolStripMenuItem
            // 
            this.خروجToolStripMenuItem.Image = global::ByStro.Properties.Resources.Safe_Out_48px;
            this.خروجToolStripMenuItem.Name = "خروجToolStripMenuItem";
            this.خروجToolStripMenuItem.Size = new System.Drawing.Size(182, 36);
            this.خروجToolStripMenuItem.Text = "خروج";
            this.خروجToolStripMenuItem.Click += new System.EventHandler(this.خروجToolStripMenuItem_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "ByStro";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(111, 56);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // showToolStripMenuItem
            // 
            this.showToolStripMenuItem.Image = global::ByStro.Properties.Resources._44677297_547588689036323_6134909789295083520_n;
            this.showToolStripMenuItem.Name = "showToolStripMenuItem";
            this.showToolStripMenuItem.Size = new System.Drawing.Size(110, 26);
            this.showToolStripMenuItem.Text = "Show ";
            this.showToolStripMenuItem.Click += new System.EventHandler(this.showToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Image = global::ByStro.Properties.Resources.Safe_Out_48px;
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(110, 26);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // ribbonStatusBar1
            // 
            this.ribbonStatusBar1.ItemLinks.Add(this.barStaticItem2);
            this.ribbonStatusBar1.ItemLinks.Add(this.barStaticItem3);
            this.ribbonStatusBar1.ItemLinks.Add(this.barStaticItem4);
            this.ribbonStatusBar1.Location = new System.Drawing.Point(0, 644);
            this.ribbonStatusBar1.Margin = new System.Windows.Forms.Padding(4);
            this.ribbonStatusBar1.Name = "ribbonStatusBar1";
            this.ribbonStatusBar1.Ribbon = this.ribbonControl1;
            this.ribbonStatusBar1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ribbonStatusBar1.Size = new System.Drawing.Size(1761, 27);
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "برنامج انجاز المبيعات ";
            this.barStaticItem2.Id = 71;
            this.barStaticItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barStaticItem2.ImageOptions.Image")));
            this.barStaticItem2.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barStaticItem2.ImageOptions.LargeImage")));
            this.barStaticItem2.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barStaticItem2.ItemAppearance.Normal.Options.UseFont = true;
            this.barStaticItem2.Name = "barStaticItem2";
            this.barStaticItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Caption = "التاريخ";
            this.barStaticItem3.Id = 72;
            this.barStaticItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barStaticItem3.ImageOptions.Image")));
            this.barStaticItem3.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barStaticItem3.ImageOptions.LargeImage")));
            this.barStaticItem3.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barStaticItem3.ItemAppearance.Normal.Options.UseFont = true;
            this.barStaticItem3.Name = "barStaticItem3";
            this.barStaticItem3.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barStaticItem3_ItemClick);
            // 
            // barStaticItem4
            // 
            this.barStaticItem4.Caption = "اسم المستخدم";
            this.barStaticItem4.Id = 73;
            this.barStaticItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barStaticItem4.ImageOptions.Image")));
            this.barStaticItem4.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barStaticItem4.ImageOptions.LargeImage")));
            this.barStaticItem4.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barStaticItem4.ItemAppearance.Normal.Options.UseFont = true;
            this.barStaticItem4.Name = "barStaticItem4";
            this.barStaticItem4.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ApplicationButtonDropDownControl = this.applicationMenu2;
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.ribbonControl1.SearchEditItem,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItem5,
            this.barButtonItem6,
            this.barButtonItem7,
            this.barButtonItem8,
            this.barButtonItem9,
            this.barButtonItem10,
            this.barButtonItem11,
            this.barButtonItem12,
            this.barSubItem1,
            this.barButtonItem13,
            this.barButtonItem14,
            this.barButtonItem15,
            this.barButtonItem16,
            this.barButtonItem17,
            this.barButtonItem18,
            this.barButtonItem19,
            this.barButtonItem20,
            this.barButtonItem21,
            this.barButtonItem22,
            this.barButtonItem23,
            this.barButtonItem24,
            this.barButtonItem25,
            this.barButtonItem26,
            this.barButtonItem27,
            this.barButtonItem28,
            this.barButtonItem29,
            this.barButtonItem30,
            this.barButtonItem31,
            this.barButtonItem32,
            this.barButtonItem33,
            this.barButtonItem34,
            this.barButtonItem35,
            this.barButtonItem36,
            this.barButtonItem37,
            this.barSubItem2,
            this.barButtonItem38,
            this.barButtonItem39,
            this.barButtonItem40,
            this.barButtonItem41,
            this.barButtonItem42,
            this.barButtonItem43,
            this.barButtonItem44,
            this.barButtonItem45,
            this.barButtonItem46,
            this.barButtonItem47,
            this.barButtonItem48,
            this.barButtonItem49,
            this.barButtonItem50,
            this.barButtonItem51,
            this.barButtonItem52,
            this.barButtonItem53,
            this.barButtonItem54,
            this.barButtonItem55,
            this.barSubItem3,
            this.barButtonItem57,
            this.barButtonItem58,
            this.barButtonItem59,
            this.barButtonItem60,
            this.barButtonItem61,
            this.barButtonItem62,
            this.barStaticItem1,
            this.barStaticItem2,
            this.barStaticItem3,
            this.barStaticItem4,
            this.barButtonItem66,
            this.barButtonItem67,
            this.barButtonItem68,
            this.barButtonItem69,
            this.barButtonItem70,
            this.barButtonItem71,
            this.barButtonItem72,
            this.barButtonItem73,
            this.barButtonItem74,
            this.barButtonItem75,
            this.barButtonItem76,
            this.barButtonItem77,
            this.barButtonItem78,
            this.barButtonItem56,
            this.barButtonItem79,
            this.barEditItem1,
            this.barEditItem2,
            this.barEditItem3,
            this.barSubItem4,
            this.barButtonItem80,
            this.barButtonItem81,
            this.barButtonItem82,
            this.barButtonItem83,
            this.barButtonItem84,
            this.barSubItem5,
            this.barButtonItem85,
            this.barButtonItem86,
            this.barButtonItem87,
            this.barButtonItem88,
            this.barButtonItem89,
            this.barButtonItem90,
            this.barButtonItem91,
            this.barButtonItem92,
            this.barButtonItem93,
            this.barButtonItem94,
            this.barButtonItem95,
            this.barButtonItem96,
            this.barButtonItem97,
            this.barButtonItem98,
            this.barButtonItem99,
            this.barButtonItem100,
            this.barButtonItem102,
            this.barButtonItem103,
            this.barButtonItem104,
            this.barButtonItem105,
            this.barButtonItem106,
            this.barButtonItem107,
            this.barButtonItem108,
            this.barButtonItem109,
            this.barButtonItem110,
            this.barButtonItem111,
            this.barButtonItem112,
            this.barButtonItem113,
            this.barButtonItem114,
            this.barButtonItem115,
            this.barButtonItem116,
            this.barButtonItem117,
            this.barButtonItem118,
            this.barButtonItem119,
            this.barButtonItem120,
            this.barButtonItem121,
            this.barButtonItem122,
            this.barButtonItem123,
            this.barButtonItem124,
            this.barEditItem4,
            this.skinPaletteRibbonGalleryBarItem1,
            this.skinRibbonGalleryBarItem1,
            this.barButtonItem101});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.ribbonControl1.MaxItemId = 146;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.PageHeaderItemLinks.Add(this.skinPaletteRibbonGalleryBarItem1);
            this.ribbonControl1.PageHeaderItemLinks.Add(this.skinRibbonGalleryBarItem1);
            this.ribbonControl1.PageHeaderItemLinks.Add(this.barButtonItem101);
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage5,
            this.ا,
            this.ribbonPage4,
            this.ribbonPage3,
            this.ribbonPage1,
            this.ribbonPage2,
            this.ribbonPage10,
            this.ribbonPage9,
            this.ribbonPage6,
            this.ribbonPage8,
            this.ribbonPage7,
            this.ribbonPage11,
            this.ribbonPage12});
            this.ribbonControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHypertextLabel1,
            this.repositoryItemTextEdit1,
            this.repositoryItemHypertextLabel2,
            this.repositoryItemTimeEdit1});
            this.ribbonControl1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2019;
            this.ribbonControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ribbonControl1.Size = new System.Drawing.Size(1761, 155);
            this.ribbonControl1.StatusBar = this.ribbonStatusBar1;
            // 
            // applicationMenu2
            // 
            this.applicationMenu2.Name = "applicationMenu2";
            this.applicationMenu2.Ribbon = this.ribbonControl1;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "اعداد الفواتير";
            this.barButtonItem1.Id = 1;
            this.barButtonItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.Image")));
            this.barButtonItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.LargeImage")));
            this.barButtonItem1.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem1.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem1.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem1.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem1.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem1.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem1.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem1.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem1.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem1.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem1.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem1.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "صلاحيات المستخدمين";
            this.barButtonItem2.Id = 2;
            this.barButtonItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.ImageOptions.Image")));
            this.barButtonItem2.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.ImageOptions.LargeImage")));
            this.barButtonItem2.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem2.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem2.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem2.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem2.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem2.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem2.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem2.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem2.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem2.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem2.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem2.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "اعداد الاتصال";
            this.barButtonItem3.Id = 3;
            this.barButtonItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.ImageOptions.Image")));
            this.barButtonItem3.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.ImageOptions.LargeImage")));
            this.barButtonItem3.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem3.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem3.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem3.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem3.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem3.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem3.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem3.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem3.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem3.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem3.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem3.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem3.Name = "barButtonItem3";
            this.barButtonItem3.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem3_ItemClick);
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "بيانات الشركة";
            this.barButtonItem4.Id = 4;
            this.barButtonItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.ImageOptions.Image")));
            this.barButtonItem4.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.ImageOptions.LargeImage")));
            this.barButtonItem4.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem4.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem4.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem4.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem4.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem4.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem4.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem4.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem4.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem4.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem4.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem4.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem4.Name = "barButtonItem4";
            this.barButtonItem4.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem4.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem4_ItemClick);
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "العملاء";
            this.barButtonItem5.Id = 5;
            this.barButtonItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem5.ImageOptions.Image")));
            this.barButtonItem5.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem5.ImageOptions.LargeImage")));
            this.barButtonItem5.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem5.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem5.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem5.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem5.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem5.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem5.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem5.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem5.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem5.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem5.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem5.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem5.Name = "barButtonItem5";
            this.barButtonItem5.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem5.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem5_ItemClick);
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "كشف حساب عميل";
            this.barButtonItem6.Id = 6;
            this.barButtonItem6.ImageOptions.SvgImage = global::ByStro.Properties.Resources.accounting;
            this.barButtonItem6.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem6.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem6.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem6.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem6.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem6.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem6.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem6.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem6.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem6.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem6.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem6.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem6.Name = "barButtonItem6";
            this.barButtonItem6.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem6.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem6_ItemClick);
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "رصيد اول المده للعملاء";
            this.barButtonItem7.Id = 7;
            this.barButtonItem7.ImageOptions.SvgImage = global::ByStro.Properties.Resources.currency;
            this.barButtonItem7.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem7.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem7.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem7.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem7.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem7.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem7.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem7.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem7.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem7.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem7.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem7.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem7.Name = "barButtonItem7";
            this.barButtonItem7.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem7.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem7_ItemClick);
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "بيانات الموردين";
            this.barButtonItem8.Id = 8;
            this.barButtonItem8.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem8.ImageOptions.Image")));
            this.barButtonItem8.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem8.ImageOptions.LargeImage")));
            this.barButtonItem8.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem8.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem8.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem8.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem8.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem8.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem8.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem8.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem8.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem8.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem8.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem8.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem8.Name = "barButtonItem8";
            this.barButtonItem8.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem8.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem8_ItemClick);
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Caption = "كشف حساب مورد";
            this.barButtonItem9.Id = 9;
            this.barButtonItem9.ImageOptions.SvgImage = global::ByStro.Properties.Resources.accounting1;
            this.barButtonItem9.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem9.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem9.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem9.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem9.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem9.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem9.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem9.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem9.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem9.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem9.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem9.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem9.Name = "barButtonItem9";
            this.barButtonItem9.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem9.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem9_ItemClick);
            // 
            // barButtonItem10
            // 
            this.barButtonItem10.Caption = "رصيد اول المده للمورد";
            this.barButtonItem10.Id = 10;
            this.barButtonItem10.ImageOptions.SvgImage = global::ByStro.Properties.Resources.currency1;
            this.barButtonItem10.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem10.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem10.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem10.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem10.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem10.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem10.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem10.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem10.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem10.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem10.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem10.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem10.Name = "barButtonItem10";
            this.barButtonItem10.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem10.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem10_ItemClick);
            // 
            // barButtonItem11
            // 
            this.barButtonItem11.Caption = "فاتوره مشتريات";
            this.barButtonItem11.Id = 11;
            this.barButtonItem11.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem11.ImageOptions.Image")));
            this.barButtonItem11.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem11.ImageOptions.LargeImage")));
            this.barButtonItem11.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem11.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem11.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem11.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem11.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem11.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem11.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem11.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem11.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem11.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem11.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem11.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem11.Name = "barButtonItem11";
            this.barButtonItem11.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem12
            // 
            this.barButtonItem12.Caption = "فاتوره طلب شراء";
            this.barButtonItem12.Id = 12;
            this.barButtonItem12.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem12.ImageOptions.Image")));
            this.barButtonItem12.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem12.ImageOptions.LargeImage")));
            this.barButtonItem12.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem12.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem12.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem12.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem12.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem12.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem12.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem12.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem12.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem12.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem12.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem12.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem12.Name = "barButtonItem12";
            this.barButtonItem12.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "barSubItem1";
            this.barSubItem1.Id = 13;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barButtonItem13
            // 
            this.barButtonItem13.Caption = "فاتوره مبيعات";
            this.barButtonItem13.Id = 15;
            this.barButtonItem13.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem13.ImageOptions.Image")));
            this.barButtonItem13.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem13.ImageOptions.LargeImage")));
            this.barButtonItem13.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem13.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem13.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem13.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem13.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem13.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem13.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem13.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem13.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem13.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem13.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem13.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem13.Name = "barButtonItem13";
            this.barButtonItem13.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem13.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem13_ItemClick);
            // 
            // barButtonItem14
            // 
            this.barButtonItem14.Caption = "مرتجع المبيعات";
            this.barButtonItem14.Id = 16;
            this.barButtonItem14.ImageOptions.SvgImage = global::ByStro.Properties.Resources.resetlayoutoptions;
            this.barButtonItem14.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem14.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem14.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem14.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem14.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem14.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem14.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem14.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem14.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem14.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem14.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem14.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem14.Name = "barButtonItem14";
            this.barButtonItem14.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem14.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem14_ItemClick);
            // 
            // barButtonItem15
            // 
            this.barButtonItem15.Caption = "فاتوره عرض السعر";
            this.barButtonItem15.Id = 17;
            this.barButtonItem15.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem15.ImageOptions.Image")));
            this.barButtonItem15.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem15.ImageOptions.LargeImage")));
            this.barButtonItem15.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem15.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem15.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem15.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem15.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem15.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem15.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem15.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem15.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem15.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem15.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem15.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem15.Name = "barButtonItem15";
            this.barButtonItem15.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem15.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem15_ItemClick);
            // 
            // barButtonItem16
            // 
            this.barButtonItem16.Caption = "اضافة صنف";
            this.barButtonItem16.Id = 18;
            this.barButtonItem16.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem16.ImageOptions.Image")));
            this.barButtonItem16.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem16.ImageOptions.LargeImage")));
            this.barButtonItem16.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem16.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem16.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem16.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem16.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem16.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem16.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem16.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem16.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem16.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem16.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem16.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem16.Name = "barButtonItem16";
            this.barButtonItem16.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem16.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem16_ItemClick);
            // 
            // barButtonItem17
            // 
            this.barButtonItem17.Caption = "المجموعات";
            this.barButtonItem17.Id = 19;
            this.barButtonItem17.ImageOptions.SvgImage = global::ByStro.Properties.Resources.group1;
            this.barButtonItem17.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem17.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem17.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem17.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem17.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem17.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem17.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem17.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem17.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem17.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem17.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem17.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem17.Name = "barButtonItem17";
            this.barButtonItem17.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem17.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem17_ItemClick);
            // 
            // barButtonItem18
            // 
            this.barButtonItem18.Caption = "الشركات المصنعه";
            this.barButtonItem18.Id = 20;
            this.barButtonItem18.ImageOptions.SvgImage = global::ByStro.Properties.Resources.grouptooltips;
            this.barButtonItem18.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem18.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem18.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem18.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem18.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem18.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem18.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem18.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem18.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem18.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem18.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem18.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem18.Name = "barButtonItem18";
            this.barButtonItem18.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem18.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem18_ItemClick);
            // 
            // barButtonItem19
            // 
            this.barButtonItem19.Caption = "انواع الاجهزه";
            this.barButtonItem19.Id = 21;
            this.barButtonItem19.ImageOptions.SvgImage = global::ByStro.Properties.Resources.office2010;
            this.barButtonItem19.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem19.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem19.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem19.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem19.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem19.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem19.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem19.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem19.Name = "barButtonItem19";
            this.barButtonItem19.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem19.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem19_ItemClick);
            // 
            // barButtonItem20
            // 
            this.barButtonItem20.Caption = "انواع الاعطال";
            this.barButtonItem20.Id = 22;
            this.barButtonItem20.ImageOptions.SvgImage = global::ByStro.Properties.Resources.chartdatalabels_none;
            this.barButtonItem20.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem20.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem20.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem20.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem20.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem20.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem20.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem20.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem20.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem20.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem20.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem20.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem20.Name = "barButtonItem20";
            this.barButtonItem20.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem20.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem20_ItemClick);
            // 
            // barButtonItem21
            // 
            this.barButtonItem21.Caption = "تقرير العملاء";
            this.barButtonItem21.Id = 23;
            this.barButtonItem21.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem21.ImageOptions.Image")));
            this.barButtonItem21.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem21.ImageOptions.LargeImage")));
            this.barButtonItem21.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem21.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem21.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem21.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem21.Name = "barButtonItem21";
            this.barButtonItem21.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem22
            // 
            this.barButtonItem22.Caption = "تقرير الموردين";
            this.barButtonItem22.Id = 24;
            this.barButtonItem22.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem22.ImageOptions.Image")));
            this.barButtonItem22.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem22.ImageOptions.LargeImage")));
            this.barButtonItem22.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem22.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem22.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem22.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem22.Name = "barButtonItem22";
            this.barButtonItem22.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem23
            // 
            this.barButtonItem23.Caption = "تقرير المشتريات";
            this.barButtonItem23.Id = 25;
            this.barButtonItem23.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem23.ImageOptions.Image")));
            this.barButtonItem23.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem23.ImageOptions.LargeImage")));
            this.barButtonItem23.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem23.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem23.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem23.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem23.Name = "barButtonItem23";
            this.barButtonItem23.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem24
            // 
            this.barButtonItem24.Caption = "تقرير المبيعات";
            this.barButtonItem24.Id = 26;
            this.barButtonItem24.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem24.ImageOptions.Image")));
            this.barButtonItem24.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem24.ImageOptions.LargeImage")));
            this.barButtonItem24.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem24.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem24.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem24.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem24.Name = "barButtonItem24";
            this.barButtonItem24.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem25
            // 
            this.barButtonItem25.Caption = "تقرير المرتجعات";
            this.barButtonItem25.Id = 27;
            this.barButtonItem25.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem25.ImageOptions.Image")));
            this.barButtonItem25.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem25.ImageOptions.LargeImage")));
            this.barButtonItem25.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem25.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem25.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem25.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem25.Name = "barButtonItem25";
            this.barButtonItem25.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem26
            // 
            this.barButtonItem26.Caption = "تقرير المصروفات";
            this.barButtonItem26.Id = 28;
            this.barButtonItem26.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem26.ImageOptions.Image")));
            this.barButtonItem26.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem26.ImageOptions.LargeImage")));
            this.barButtonItem26.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem26.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem26.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem26.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem26.Name = "barButtonItem26";
            this.barButtonItem26.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem27
            // 
            this.barButtonItem27.Caption = "صافي البربج والخساره";
            this.barButtonItem27.Id = 29;
            this.barButtonItem27.ImageOptions.SvgImage = global::ByStro.Properties.Resources.currency2;
            this.barButtonItem27.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem27.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem27.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem27.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem27.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem27.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem27.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem27.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem27.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem27.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem27.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem27.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem27.Name = "barButtonItem27";
            this.barButtonItem27.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem27.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem27_ItemClick);
            // 
            // barButtonItem28
            // 
            this.barButtonItem28.Caption = "الربح من العملاء";
            this.barButtonItem28.Id = 30;
            this.barButtonItem28.ImageOptions.SvgImage = global::ByStro.Properties.Resources.bo_department;
            this.barButtonItem28.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem28.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem28.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem28.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem28.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem28.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem28.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem28.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem28.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem28.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem28.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem28.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem28.Name = "barButtonItem28";
            this.barButtonItem28.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem28.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem28_ItemClick);
            // 
            // barButtonItem29
            // 
            this.barButtonItem29.Caption = "وارد ومنصرف خزينه";
            this.barButtonItem29.Id = 31;
            this.barButtonItem29.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem29.ImageOptions.Image")));
            this.barButtonItem29.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem29.ImageOptions.LargeImage")));
            this.barButtonItem29.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem29.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem29.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem29.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem29.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem29.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem29.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem29.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem29.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem29.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem29.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem29.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem29.Name = "barButtonItem29";
            this.barButtonItem29.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem29.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem29_ItemClick);
            // 
            // barButtonItem30
            // 
            this.barButtonItem30.Caption = "ايداع رصيد خزنة";
            this.barButtonItem30.Id = 32;
            this.barButtonItem30.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem30.ImageOptions.Image")));
            this.barButtonItem30.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem30.ImageOptions.LargeImage")));
            this.barButtonItem30.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem30.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem30.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem30.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem30.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem30.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem30.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem30.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem30.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem30.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem30.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem30.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem30.Name = "barButtonItem30";
            this.barButtonItem30.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem31
            // 
            this.barButtonItem31.Caption = "ايداع رصيد بنك";
            this.barButtonItem31.Id = 33;
            this.barButtonItem31.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem31.ImageOptions.Image")));
            this.barButtonItem31.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem31.ImageOptions.LargeImage")));
            this.barButtonItem31.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem31.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem31.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem31.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem31.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem31.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem31.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem31.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem31.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem31.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem31.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem31.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem31.Name = "barButtonItem31";
            this.barButtonItem31.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem32
            // 
            this.barButtonItem32.Caption = "سحب رصيد خزنة";
            this.barButtonItem32.Id = 34;
            this.barButtonItem32.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem32.ImageOptions.Image")));
            this.barButtonItem32.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem32.ImageOptions.LargeImage")));
            this.barButtonItem32.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem32.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem32.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem32.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem32.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem32.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem32.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem32.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem32.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem32.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem32.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem32.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem32.Name = "barButtonItem32";
            this.barButtonItem32.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem33
            // 
            this.barButtonItem33.Caption = "سحب رصيد بنك";
            this.barButtonItem33.Id = 35;
            this.barButtonItem33.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem33.ImageOptions.Image")));
            this.barButtonItem33.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem33.ImageOptions.LargeImage")));
            this.barButtonItem33.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem33.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem33.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem33.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem33.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem33.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem33.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem33.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem33.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem33.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem33.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem33.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem33.Name = "barButtonItem33";
            this.barButtonItem33.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem34
            // 
            this.barButtonItem34.Caption = "تحويل رصيد بين الخزنات";
            this.barButtonItem34.Id = 36;
            this.barButtonItem34.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem34.ImageOptions.Image")));
            this.barButtonItem34.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem34.ImageOptions.LargeImage")));
            this.barButtonItem34.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem34.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem34.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem34.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem34.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem34.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem34.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem34.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem34.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem34.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem34.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem34.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem34.Name = "barButtonItem34";
            this.barButtonItem34.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem35
            // 
            this.barButtonItem35.Caption = "انواع المصروفات";
            this.barButtonItem35.Id = 37;
            this.barButtonItem35.ImageOptions.SvgImage = global::ByStro.Properties.Resources.itemtypechecked;
            this.barButtonItem35.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem35.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem35.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem35.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem35.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem35.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem35.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem35.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem35.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem35.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem35.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem35.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem35.Name = "barButtonItem35";
            this.barButtonItem35.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem35.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem35_ItemClick);
            // 
            // barButtonItem36
            // 
            this.barButtonItem36.Caption = "تسجيل مصروف";
            this.barButtonItem36.Id = 38;
            this.barButtonItem36.ImageOptions.DisabledImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem36.ImageOptions.DisabledImage")));
            this.barButtonItem36.ImageOptions.DisabledLargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem36.ImageOptions.DisabledLargeImage")));
            this.barButtonItem36.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem36.ImageOptions.Image")));
            this.barButtonItem36.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem36.ImageOptions.LargeImage")));
            this.barButtonItem36.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem36.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem36.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem36.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem36.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem36.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem36.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem36.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem36.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem36.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem36.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem36.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem36.Name = "barButtonItem36";
            this.barButtonItem36.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem36.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem36_ItemClick);
            // 
            // barButtonItem37
            // 
            this.barButtonItem37.Caption = "تقارير";
            this.barButtonItem37.Id = 39;
            this.barButtonItem37.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem37.ImageOptions.Image")));
            this.barButtonItem37.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem37.ImageOptions.LargeImage")));
            this.barButtonItem37.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem37.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem37.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem37.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem37.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem37.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem37.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem37.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem37.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem37.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem37.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem37.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem37.Name = "barButtonItem37";
            this.barButtonItem37.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "الاقساط";
            this.barSubItem2.Id = 42;
            this.barSubItem2.ImageOptions.SvgImage = global::ByStro.Properties.Resources.business_cash1;
            this.barSubItem2.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barSubItem2.ItemAppearance.Hovered.Options.UseFont = true;
            this.barSubItem2.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barSubItem2.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barSubItem2.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barSubItem2.ItemAppearance.Normal.Options.UseFont = true;
            this.barSubItem2.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barSubItem2.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barSubItem2.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barSubItem2.ItemAppearance.Pressed.Options.UseFont = true;
            this.barSubItem2.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barSubItem2.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barSubItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem38),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem39),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem40),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem41),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem42)});
            this.barSubItem2.Name = "barSubItem2";
            this.barSubItem2.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem38
            // 
            this.barButtonItem38.Caption = "قائمة الاقساط";
            this.barButtonItem38.Id = 43;
            this.barButtonItem38.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem38.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem38.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem38.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem38.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem38.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem38.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem38.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem38.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem38.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem38.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem38.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem38.Name = "barButtonItem38";
            this.barButtonItem38.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem38_ItemClick);
            // 
            // barButtonItem39
            // 
            this.barButtonItem39.Caption = "اضافة قسط جديد";
            this.barButtonItem39.Id = 44;
            this.barButtonItem39.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem39.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem39.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem39.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem39.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem39.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem39.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem39.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem39.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem39.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem39.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem39.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem39.Name = "barButtonItem39";
            this.barButtonItem39.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem39_ItemClick);
            // 
            // barButtonItem40
            // 
            this.barButtonItem40.Caption = "سداد قسط";
            this.barButtonItem40.Id = 45;
            this.barButtonItem40.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem40.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem40.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem40.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem40.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem40.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem40.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem40.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem40.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem40.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem40.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem40.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem40.Name = "barButtonItem40";
            this.barButtonItem40.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem40_ItemClick);
            // 
            // barButtonItem41
            // 
            this.barButtonItem41.Caption = "عرض الاقساط المستحقه";
            this.barButtonItem41.Id = 46;
            this.barButtonItem41.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem41.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem41.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem41.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem41.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem41.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem41.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem41.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem41.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem41.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem41.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem41.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem41.Name = "barButtonItem41";
            this.barButtonItem41.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem41_ItemClick);
            // 
            // barButtonItem42
            // 
            this.barButtonItem42.Caption = "عرض الاقساط المسدده";
            this.barButtonItem42.Id = 47;
            this.barButtonItem42.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem42.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem42.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem42.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem42.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem42.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem42.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem42.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem42.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem42.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem42.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem42.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem42.Name = "barButtonItem42";
            this.barButtonItem42.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem42_ItemClick);
            // 
            // barButtonItem43
            // 
            this.barButtonItem43.Caption = "تقرير تحويل رصيد من الخزنة للبنك والعكس";
            this.barButtonItem43.Id = 48;
            this.barButtonItem43.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem43.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem43.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem43.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem43.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem43.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem43.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem43.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem43.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem43.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem43.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem43.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem43.Name = "barButtonItem43";
            // 
            // barButtonItem44
            // 
            this.barButtonItem44.Caption = "الموظفين";
            this.barButtonItem44.Id = 49;
            this.barButtonItem44.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem44.ImageOptions.Image")));
            this.barButtonItem44.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem44.ImageOptions.LargeImage")));
            this.barButtonItem44.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem44.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem44.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem44.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem44.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem44.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem44.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem44.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem44.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem44.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem44.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem44.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem44.Name = "barButtonItem44";
            this.barButtonItem44.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem44.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem44_ItemClick);
            // 
            // barButtonItem45
            // 
            this.barButtonItem45.Caption = "انواع الوظائف في الشركة";
            this.barButtonItem45.Id = 50;
            this.barButtonItem45.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem45.ImageOptions.Image")));
            this.barButtonItem45.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem45.ImageOptions.LargeImage")));
            this.barButtonItem45.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem45.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem45.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem45.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem45.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem45.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem45.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem45.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem45.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem45.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem45.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem45.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem45.Name = "barButtonItem45";
            this.barButtonItem45.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem45.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem45_ItemClick);
            // 
            // barButtonItem46
            // 
            this.barButtonItem46.Caption = "دفع مرتبات";
            this.barButtonItem46.Id = 51;
            this.barButtonItem46.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem46.ImageOptions.Image")));
            this.barButtonItem46.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem46.ImageOptions.LargeImage")));
            this.barButtonItem46.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem46.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem46.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem46.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem46.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem46.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem46.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem46.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem46.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem46.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem46.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem46.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem46.Name = "barButtonItem46";
            this.barButtonItem46.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem46.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem46_ItemClick);
            // 
            // barButtonItem47
            // 
            this.barButtonItem47.Caption = "تسجيل حضور";
            this.barButtonItem47.Id = 52;
            this.barButtonItem47.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem47.ImageOptions.Image")));
            this.barButtonItem47.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem47.ImageOptions.LargeImage")));
            this.barButtonItem47.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem47.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem47.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem47.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem47.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem47.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem47.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem47.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem47.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem47.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem47.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem47.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem47.Name = "barButtonItem47";
            this.barButtonItem47.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem47.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem47_ItemClick);
            // 
            // barButtonItem48
            // 
            this.barButtonItem48.Caption = "تسجيل انصراف";
            this.barButtonItem48.Id = 53;
            this.barButtonItem48.ImageOptions.SvgImage = global::ByStro.Properties.Resources.deferred;
            this.barButtonItem48.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem48.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem48.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem48.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem48.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem48.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem48.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem48.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem48.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem48.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem48.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem48.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem48.Name = "barButtonItem48";
            this.barButtonItem48.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem48.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem48_ItemClick);
            // 
            // barButtonItem49
            // 
            this.barButtonItem49.Caption = "اقسام العمل في الشركة";
            this.barButtonItem49.Id = 54;
            this.barButtonItem49.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem49.ImageOptions.Image")));
            this.barButtonItem49.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem49.ImageOptions.LargeImage")));
            this.barButtonItem49.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem49.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem49.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem49.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem49.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem49.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem49.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem49.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem49.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem49.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem49.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem49.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem49.Name = "barButtonItem49";
            this.barButtonItem49.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem49.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem49_ItemClick);
            // 
            // barButtonItem50
            // 
            this.barButtonItem50.Caption = "تسجيل اجازه";
            this.barButtonItem50.Id = 55;
            this.barButtonItem50.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem50.ImageOptions.Image")));
            this.barButtonItem50.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem50.ImageOptions.LargeImage")));
            this.barButtonItem50.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem50.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem50.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem50.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem50.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem50.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem50.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem50.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem50.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem50.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem50.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem50.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem50.Name = "barButtonItem50";
            this.barButtonItem50.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem50.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem50_ItemClick);
            // 
            // barButtonItem51
            // 
            this.barButtonItem51.Caption = "سند استلام صيانه";
            this.barButtonItem51.Id = 56;
            this.barButtonItem51.ImageOptions.SvgImage = global::ByStro.Properties.Resources.shipmentreceived;
            this.barButtonItem51.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem51.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem51.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem51.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem51.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem51.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem51.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem51.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem51.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem51.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem51.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem51.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem51.Name = "barButtonItem51";
            this.barButtonItem51.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem51.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem51_ItemClick);
            // 
            // barButtonItem52
            // 
            this.barButtonItem52.Caption = "سند صرف";
            this.barButtonItem52.Id = 57;
            this.barButtonItem52.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem52.ImageOptions.Image")));
            this.barButtonItem52.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem52.ImageOptions.LargeImage")));
            this.barButtonItem52.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem52.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem52.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem52.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem52.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem52.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem52.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem52.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem52.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem52.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem52.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem52.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem52.Name = "barButtonItem52";
            this.barButtonItem52.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem53
            // 
            this.barButtonItem53.Caption = "فاتوره صيانه";
            this.barButtonItem53.Id = 58;
            this.barButtonItem53.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem53.ImageOptions.Image")));
            this.barButtonItem53.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem53.ImageOptions.LargeImage")));
            this.barButtonItem53.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem53.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem53.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem53.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem53.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem53.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem53.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem53.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem53.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem53.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem53.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem53.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem53.Name = "barButtonItem53";
            this.barButtonItem53.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem53.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem53_ItemClick);
            // 
            // barButtonItem54
            // 
            this.barButtonItem54.Caption = "النسخ الاحتياطي والاسترجاع";
            this.barButtonItem54.Id = 59;
            this.barButtonItem54.ImageOptions.SvgImage = global::ByStro.Properties.Resources.crossdatasourcefiltering;
            this.barButtonItem54.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem54.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem54.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem54.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem54.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem54.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem54.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem54.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem54.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem54.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem54.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem54.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem54.Name = "barButtonItem54";
            this.barButtonItem54.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem54.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem54_ItemClick);
            // 
            // barButtonItem55
            // 
            this.barButtonItem55.Caption = "تغير خط البرنامج";
            this.barButtonItem55.Id = 60;
            this.barButtonItem55.ImageOptions.SvgImage = global::ByStro.Properties.Resources.autoarrange;
            this.barButtonItem55.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem55.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem55.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem55.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem55.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem55.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem55.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem55.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem55.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem55.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem55.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem55.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem55.Name = "barButtonItem55";
            this.barButtonItem55.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem55.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem55_ItemClick);
            // 
            // barSubItem3
            // 
            this.barSubItem3.Caption = "اعداد ميزان الباركود";
            this.barSubItem3.Id = 61;
            this.barSubItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubItem3.ImageOptions.Image")));
            this.barSubItem3.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barSubItem3.ImageOptions.LargeImage")));
            this.barSubItem3.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barSubItem3.ItemAppearance.Hovered.Options.UseFont = true;
            this.barSubItem3.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barSubItem3.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barSubItem3.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barSubItem3.ItemAppearance.Normal.Options.UseFont = true;
            this.barSubItem3.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barSubItem3.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barSubItem3.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barSubItem3.ItemAppearance.Pressed.Options.UseFont = true;
            this.barSubItem3.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barSubItem3.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barSubItem3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem59),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem60),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem61)});
            this.barSubItem3.Name = "barSubItem3";
            this.barSubItem3.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem59
            // 
            this.barButtonItem59.Caption = "تقرير تحويل المنتجات بين المخازن";
            this.barButtonItem59.Id = 65;
            this.barButtonItem59.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem59.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem59.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem59.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem59.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem59.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem59.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem59.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem59.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem59.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem59.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem59.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem59.Name = "barButtonItem59";
            // 
            // barButtonItem60
            // 
            this.barButtonItem60.Caption = "اخراج المنتجات التالفة";
            this.barButtonItem60.Id = 66;
            this.barButtonItem60.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem60.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem60.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem60.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem60.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem60.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem60.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem60.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem60.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem60.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem60.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem60.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem60.Name = "barButtonItem60";
            // 
            // barButtonItem61
            // 
            this.barButtonItem61.Caption = "تقرير المنتجات اخراج المنتجات التالفة";
            this.barButtonItem61.Id = 67;
            this.barButtonItem61.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem61.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem61.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem61.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem61.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem61.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem61.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem61.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem61.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem61.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem61.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem61.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem61.Name = "barButtonItem61";
            // 
            // barButtonItem57
            // 
            this.barButtonItem57.Caption = "جرد المخازن";
            this.barButtonItem57.Id = 63;
            this.barButtonItem57.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem57.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem57.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem57.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem57.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem57.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem57.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem57.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem57.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem57.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem57.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem57.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem57.Name = "barButtonItem57";
            // 
            // barButtonItem58
            // 
            this.barButtonItem58.Caption = "نقل المنتجات بين المخازن";
            this.barButtonItem58.Id = 64;
            this.barButtonItem58.ItemAppearance.Disabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem58.ItemAppearance.Disabled.Options.UseFont = true;
            this.barButtonItem58.ItemAppearance.Disabled.Options.UseTextOptions = true;
            this.barButtonItem58.ItemAppearance.Disabled.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem58.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem58.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem58.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem58.ItemAppearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.barButtonItem58.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.barButtonItem58.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem58.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem58.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem58.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem58.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem58.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem58.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem58.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem58.Name = "barButtonItem58";
            // 
            // barButtonItem62
            // 
            this.barButtonItem62.Caption = "سند صرف صيانه";
            this.barButtonItem62.Id = 68;
            this.barButtonItem62.ImageOptions.SvgImage = global::ByStro.Properties.Resources.shopping_cashvoucher;
            this.barButtonItem62.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem62.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem62.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem62.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem62.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem62.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem62.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem62.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem62.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem62.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem62.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem62.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem62.Name = "barButtonItem62";
            this.barButtonItem62.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem62.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem62_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 69;
            this.barStaticItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barStaticItem1.ImageOptions.Image")));
            this.barStaticItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barStaticItem1.ImageOptions.LargeImage")));
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barButtonItem66
            // 
            this.barButtonItem66.Caption = "وحدات القياس";
            this.barButtonItem66.Id = 75;
            this.barButtonItem66.ImageOptions.SvgImage = global::ByStro.Properties.Resources.removedataitems;
            this.barButtonItem66.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem66.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem66.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem66.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem66.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem66.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem66.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem66.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem66.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem66.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem66.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem66.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem66.Name = "barButtonItem66";
            this.barButtonItem66.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem66_ItemClick);
            // 
            // barButtonItem67
            // 
            this.barButtonItem67.Caption = "المخازن";
            this.barButtonItem67.Id = 76;
            this.barButtonItem67.ImageOptions.SvgImage = global::ByStro.Properties.Resources.shopping_store;
            this.barButtonItem67.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem67.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem67.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem67.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem67.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem67.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem67.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem67.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem67.Name = "barButtonItem67";
            this.barButtonItem67.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem67_ItemClick);
            // 
            // barButtonItem68
            // 
            this.barButtonItem68.Caption = "اصناف بدايه المده";
            this.barButtonItem68.Id = 77;
            this.barButtonItem68.ImageOptions.SvgImage = global::ByStro.Properties.Resources.bo_appointment;
            this.barButtonItem68.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem68.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem68.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem68.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem68.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem68.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem68.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem68.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem68.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem68.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem68.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem68.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem68.Name = "barButtonItem68";
            this.barButtonItem68.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem68_ItemClick);
            // 
            // barButtonItem69
            // 
            this.barButtonItem69.Caption = "سند هالك اصناف";
            this.barButtonItem69.Id = 78;
            this.barButtonItem69.ImageOptions.SvgImage = global::ByStro.Properties.Resources.rotate;
            this.barButtonItem69.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem69.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem69.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem69.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem69.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem69.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem69.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem69.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem69.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem69.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem69.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem69.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem69.Name = "barButtonItem69";
            this.barButtonItem69.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem69_ItemClick);
            // 
            // barButtonItem70
            // 
            this.barButtonItem70.Caption = "تحويل بين المخازن";
            this.barButtonItem70.Id = 79;
            this.barButtonItem70.ImageOptions.SvgImage = global::ByStro.Properties.Resources.convertto;
            this.barButtonItem70.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem70.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem70.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem70.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem70.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem70.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem70.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem70.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem70.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem70.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem70.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem70.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem70.Name = "barButtonItem70";
            this.barButtonItem70.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem70_ItemClick);
            // 
            // barButtonItem71
            // 
            this.barButtonItem71.Caption = "بحث الاصناف";
            this.barButtonItem71.Id = 80;
            this.barButtonItem71.ImageOptions.SvgImage = global::ByStro.Properties.Resources.enablesearch;
            this.barButtonItem71.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem71.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem71.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem71.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem71.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem71.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem71.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem71.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem71.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem71.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem71.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem71.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem71.Name = "barButtonItem71";
            this.barButtonItem71.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem71_ItemClick);
            // 
            // barButtonItem72
            // 
            this.barButtonItem72.Caption = "اضافة لون ";
            this.barButtonItem72.Id = 81;
            this.barButtonItem72.ImageOptions.SvgImage = global::ByStro.Properties.Resources.morecolors;
            this.barButtonItem72.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F);
            this.barButtonItem72.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem72.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem72.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem72.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F);
            this.barButtonItem72.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem72.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem72.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem72.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F);
            this.barButtonItem72.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem72.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem72.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem72.Name = "barButtonItem72";
            this.barButtonItem72.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem72_ItemClick);
            // 
            // barButtonItem73
            // 
            this.barButtonItem73.Caption = "اضافة حجم";
            this.barButtonItem73.Id = 82;
            this.barButtonItem73.ImageOptions.SvgImage = global::ByStro.Properties.Resources.stretch1;
            this.barButtonItem73.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F);
            this.barButtonItem73.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem73.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem73.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem73.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F);
            this.barButtonItem73.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem73.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem73.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem73.Name = "barButtonItem73";
            this.barButtonItem73.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem73_ItemClick);
            // 
            // barButtonItem74
            // 
            this.barButtonItem74.Caption = "اعداد طباعه الباركود";
            this.barButtonItem74.Id = 83;
            this.barButtonItem74.ImageOptions.SvgImage = global::ByStro.Properties.Resources.barcodeshowtext1;
            this.barButtonItem74.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem74.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem74.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem74.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem74.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem74.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem74.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem74.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem74.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem74.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem74.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem74.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem74.Name = "barButtonItem74";
            this.barButtonItem74.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem74_ItemClick);
            // 
            // barButtonItem75
            // 
            this.barButtonItem75.Caption = "سند قبض من عميل";
            this.barButtonItem75.Id = 84;
            this.barButtonItem75.ImageOptions.SvgImage = global::ByStro.Properties.Resources.movedown;
            this.barButtonItem75.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem75.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem75.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem75.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem75.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem75.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem75.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem75.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem75.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem75.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem75.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem75.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem75.Name = "barButtonItem75";
            this.barButtonItem75.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem75_ItemClick);
            // 
            // barButtonItem76
            // 
            this.barButtonItem76.Caption = "سند دفع لعميل";
            this.barButtonItem76.Id = 85;
            this.barButtonItem76.ImageOptions.SvgImage = global::ByStro.Properties.Resources.moveup;
            this.barButtonItem76.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem76.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem76.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem76.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem76.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem76.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem76.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem76.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem76.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem76.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem76.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem76.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem76.Name = "barButtonItem76";
            this.barButtonItem76.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem76_ItemClick);
            // 
            // barButtonItem77
            // 
            this.barButtonItem77.Caption = "سند قبض من مورد";
            this.barButtonItem77.Id = 86;
            this.barButtonItem77.ImageOptions.SvgImage = global::ByStro.Properties.Resources.movedown1;
            this.barButtonItem77.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem77.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem77.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem77.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem77.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem77.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem77.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem77.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem77.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem77.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem77.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem77.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem77.Name = "barButtonItem77";
            this.barButtonItem77.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem77_ItemClick);
            // 
            // barButtonItem78
            // 
            this.barButtonItem78.Caption = "سند دفع للمورد";
            this.barButtonItem78.Id = 87;
            this.barButtonItem78.ImageOptions.SvgImage = global::ByStro.Properties.Resources.moveup1;
            this.barButtonItem78.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem78.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem78.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem78.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem78.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem78.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem78.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem78.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem78.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem78.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem78.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem78.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem78.Name = "barButtonItem78";
            this.barButtonItem78.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem78_ItemClick);
            // 
            // barButtonItem56
            // 
            this.barButtonItem56.Caption = "اعداد ميزان الباركود";
            this.barButtonItem56.Id = 88;
            this.barButtonItem56.ImageOptions.SvgImage = global::ByStro.Properties.Resources.bluedatabargradient;
            this.barButtonItem56.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem56.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem56.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem56.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem56.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem56.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem56.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem56.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem56.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem56.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem56.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem56.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem56.Name = "barButtonItem56";
            this.barButtonItem56.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem56_ItemClick);
            // 
            // barButtonItem79
            // 
            this.barButtonItem79.Caption = "barButtonItem79";
            this.barButtonItem79.Id = 89;
            this.barButtonItem79.Name = "barButtonItem79";
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "التا";
            this.barEditItem1.Edit = this.repositoryItemHypertextLabel1;
            this.barEditItem1.Id = 90;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemHypertextLabel1
            // 
            this.repositoryItemHypertextLabel1.Name = "repositoryItemHypertextLabel1";
            // 
            // barEditItem2
            // 
            this.barEditItem2.Caption = "barEditItem2";
            this.barEditItem2.Edit = this.repositoryItemTextEdit1;
            this.barEditItem2.Id = 91;
            this.barEditItem2.Name = "barEditItem2";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // barEditItem3
            // 
            this.barEditItem3.Caption = "السائقون";
            this.barEditItem3.Edit = this.repositoryItemHypertextLabel2;
            this.barEditItem3.Id = 92;
            this.barEditItem3.Name = "barEditItem3";
            // 
            // repositoryItemHypertextLabel2
            // 
            this.repositoryItemHypertextLabel2.Name = "repositoryItemHypertextLabel2";
            // 
            // barSubItem4
            // 
            this.barSubItem4.Caption = "الديلفري";
            this.barSubItem4.Id = 93;
            this.barSubItem4.ImageOptions.SvgImage = global::ByStro.Properties.Resources.shopping_delivery1;
            this.barSubItem4.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barSubItem4.ItemAppearance.Hovered.Options.UseFont = true;
            this.barSubItem4.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barSubItem4.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barSubItem4.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barSubItem4.ItemAppearance.Normal.Options.UseFont = true;
            this.barSubItem4.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barSubItem4.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barSubItem4.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barSubItem4.ItemAppearance.Pressed.Options.UseFont = true;
            this.barSubItem4.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barSubItem4.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barSubItem4.ItemInMenuAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barSubItem4.ItemInMenuAppearance.Hovered.Options.UseFont = true;
            this.barSubItem4.ItemInMenuAppearance.Hovered.Options.UseTextOptions = true;
            this.barSubItem4.ItemInMenuAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barSubItem4.ItemInMenuAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barSubItem4.ItemInMenuAppearance.Normal.Options.UseFont = true;
            this.barSubItem4.ItemInMenuAppearance.Normal.Options.UseTextOptions = true;
            this.barSubItem4.ItemInMenuAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barSubItem4.ItemInMenuAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barSubItem4.ItemInMenuAppearance.Pressed.Options.UseFont = true;
            this.barSubItem4.ItemInMenuAppearance.Pressed.Options.UseTextOptions = true;
            this.barSubItem4.ItemInMenuAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barSubItem4.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem80),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem81),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem82),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem83)});
            this.barSubItem4.Name = "barSubItem4";
            // 
            // barButtonItem80
            // 
            this.barButtonItem80.Caption = "السائقون";
            this.barButtonItem80.Id = 94;
            this.barButtonItem80.Name = "barButtonItem80";
            this.barButtonItem80.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem80_ItemClick);
            // 
            // barButtonItem81
            // 
            this.barButtonItem81.Caption = "طلب توصيل جديد";
            this.barButtonItem81.Id = 95;
            this.barButtonItem81.Name = "barButtonItem81";
            this.barButtonItem81.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem81_ItemClick);
            // 
            // barButtonItem82
            // 
            this.barButtonItem82.Caption = "اوامر توصيل جارية";
            this.barButtonItem82.Id = 96;
            this.barButtonItem82.Name = "barButtonItem82";
            this.barButtonItem82.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem82_ItemClick);
            // 
            // barButtonItem83
            // 
            this.barButtonItem83.Caption = "اوامر توصيل منتهية";
            this.barButtonItem83.Id = 97;
            this.barButtonItem83.Name = "barButtonItem83";
            this.barButtonItem83.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem83_ItemClick);
            // 
            // barButtonItem84
            // 
            this.barButtonItem84.Caption = "مرتجع مشتريات";
            this.barButtonItem84.Id = 98;
            this.barButtonItem84.ImageOptions.SvgImage = global::ByStro.Properties.Resources.resetlayoutoptions1;
            this.barButtonItem84.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem84.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem84.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem84.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem84.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem84.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem84.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem84.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem84.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem84.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem84.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem84.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem84.Name = "barButtonItem84";
            // 
            // barSubItem5
            // 
            this.barSubItem5.Caption = "barSubItem5";
            this.barSubItem5.Id = 99;
            this.barSubItem5.Name = "barSubItem5";
            // 
            // barButtonItem85
            // 
            this.barButtonItem85.Caption = "انواع الاجازات";
            this.barButtonItem85.Id = 100;
            this.barButtonItem85.ImageOptions.SvgImage = global::ByStro.Properties.Resources.bo_appearance;
            this.barButtonItem85.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem85.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem85.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem85.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem85.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem85.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem85.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem85.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem85.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem85.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem85.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem85.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem85.Name = "barButtonItem85";
            this.barButtonItem85.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem85_ItemClick);
            // 
            // barButtonItem86
            // 
            this.barButtonItem86.Caption = "انواع الاستحققات";
            this.barButtonItem86.Id = 101;
            this.barButtonItem86.ImageOptions.SvgImage = global::ByStro.Properties.Resources.financial1;
            this.barButtonItem86.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem86.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem86.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem86.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem86.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem86.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem86.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem86.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem86.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem86.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem86.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem86.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem86.Name = "barButtonItem86";
            this.barButtonItem86.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem86_ItemClick);
            // 
            // barButtonItem87
            // 
            this.barButtonItem87.Caption = "انواع الاستقطعات";
            this.barButtonItem87.Id = 102;
            this.barButtonItem87.ImageOptions.SvgImage = global::ByStro.Properties.Resources.actions_removecircled;
            this.barButtonItem87.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem87.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem87.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem87.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem87.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem87.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem87.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem87.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem87.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem87.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem87.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem87.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem87.Name = "barButtonItem87";
            this.barButtonItem87.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem87_ItemClick);
            // 
            // barButtonItem88
            // 
            this.barButtonItem88.Caption = "تسجيل استحقاق";
            this.barButtonItem88.Id = 103;
            this.barButtonItem88.ImageOptions.SvgImage = global::ByStro.Properties.Resources.financial2;
            this.barButtonItem88.ItemAppearance.Disabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem88.ItemAppearance.Disabled.Options.UseFont = true;
            this.barButtonItem88.ItemAppearance.Disabled.Options.UseTextOptions = true;
            this.barButtonItem88.ItemAppearance.Disabled.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem88.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem88.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem88.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem88.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem88.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem88.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem88.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem88.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem88.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem88.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem88.Name = "barButtonItem88";
            this.barButtonItem88.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem88_ItemClick);
            // 
            // barButtonItem89
            // 
            this.barButtonItem89.Caption = "تسجيل استقطاع";
            this.barButtonItem89.Id = 104;
            this.barButtonItem89.ImageOptions.SvgImage = global::ByStro.Properties.Resources.actions_removecircled1;
            this.barButtonItem89.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem89.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem89.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem89.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem89.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem89.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem89.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem89.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem89.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem89.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem89.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem89.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem89.Name = "barButtonItem89";
            this.barButtonItem89.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem89_ItemClick);
            // 
            // barButtonItem90
            // 
            this.barButtonItem90.Caption = "الدعم الفني والاستفسارات";
            this.barButtonItem90.Id = 105;
            this.barButtonItem90.ImageOptions.SvgImage = global::ByStro.Properties.Resources.support;
            this.barButtonItem90.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem90.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem90.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem90.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem90.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem90.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem90.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem90.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.barButtonItem90.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem90.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem90.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem90.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem90.Name = "barButtonItem90";
            this.barButtonItem90.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem90_ItemClick);
            // 
            // barButtonItem91
            // 
            this.barButtonItem91.Caption = "barButtonItem91";
            this.barButtonItem91.Id = 106;
            this.barButtonItem91.Name = "barButtonItem91";
            // 
            // barButtonItem92
            // 
            this.barButtonItem92.Caption = "تفعيل البرنامج";
            this.barButtonItem92.Id = 107;
            this.barButtonItem92.ImageOptions.SvgImage = global::ByStro.Properties.Resources.showallfieldcodes;
            this.barButtonItem92.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem92.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem92.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem92.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem92.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem92.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem92.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem92.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem92.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem92.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem92.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem92.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem92.Name = "barButtonItem92";
            this.barButtonItem92.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem92_ItemClick);
            // 
            // barButtonItem93
            // 
            this.barButtonItem93.Caption = "تغير كلمة المرور";
            this.barButtonItem93.Id = 108;
            this.barButtonItem93.ImageOptions.SvgImage = global::ByStro.Properties.Resources.security_key;
            this.barButtonItem93.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem93.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem93.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem93.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem93.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem93.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem93.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem93.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem93.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem93.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem93.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem93.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem93.Name = "barButtonItem93";
            this.barButtonItem93.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem93_ItemClick);
            // 
            // barButtonItem94
            // 
            this.barButtonItem94.Caption = "تسجيل خروج من حسابك";
            this.barButtonItem94.Id = 109;
            this.barButtonItem94.ImageOptions.SvgImage = global::ByStro.Properties.Resources.bo_employee;
            this.barButtonItem94.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem94.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem94.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem94.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem94.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem94.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem94.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem94.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem94.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem94.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem94.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem94.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.barButtonItem94.Name = "barButtonItem94";
            this.barButtonItem94.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem94_ItemClick);
            // 
            // barButtonItem95
            // 
            this.barButtonItem95.Caption = "خروج من البرنامج";
            this.barButtonItem95.Id = 110;
            this.barButtonItem95.ImageOptions.SvgImage = global::ByStro.Properties.Resources.close;
            this.barButtonItem95.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem95.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem95.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem95.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem95.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem95.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem95.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem95.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem95.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem95.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem95.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem95.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem95.Name = "barButtonItem95";
            this.barButtonItem95.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem95_ItemClick);
            // 
            // barButtonItem96
            // 
            this.barButtonItem96.Caption = "اكثر الاصناف ربحا";
            this.barButtonItem96.Id = 111;
            this.barButtonItem96.ImageOptions.SvgImage = global::ByStro.Properties.Resources.business_dollar;
            this.barButtonItem96.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem96.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem96.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem96.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem96.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem96.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem96.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem96.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem96.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem96.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem96.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem96.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem96.Name = "barButtonItem96";
            this.barButtonItem96.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem96_ItemClick);
            // 
            // barButtonItem97
            // 
            this.barButtonItem97.Caption = "راس المال";
            this.barButtonItem97.Id = 112;
            this.barButtonItem97.ImageOptions.SvgImage = global::ByStro.Properties.Resources.financial3;
            this.barButtonItem97.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem97.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem97.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem97.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem97.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem97.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem97.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem97.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem97.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem97.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem97.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem97.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem97.Name = "barButtonItem97";
            this.barButtonItem97.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem97_ItemClick);
            // 
            // barButtonItem98
            // 
            this.barButtonItem98.Caption = "الميزانيه";
            this.barButtonItem98.Id = 113;
            this.barButtonItem98.ImageOptions.Image = global::ByStro.Properties.Resources.financial_16x16;
            this.barButtonItem98.ImageOptions.LargeImage = global::ByStro.Properties.Resources.financial_32x321;
            this.barButtonItem98.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem98.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem98.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem98.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem98.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem98.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem98.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem98.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem98.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.barButtonItem98.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem98.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem98.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem98.Name = "barButtonItem98";
            this.barButtonItem98.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem98_ItemClick);
            // 
            // barButtonItem99
            // 
            this.barButtonItem99.Caption = "كشف حساب خزينه";
            this.barButtonItem99.Id = 114;
            this.barButtonItem99.ImageOptions.SvgImage = global::ByStro.Properties.Resources.business_bank;
            this.barButtonItem99.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem99.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem99.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem99.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem99.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem99.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem99.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem99.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem99.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem99.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem99.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem99.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem99.Name = "barButtonItem99";
            this.barButtonItem99.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem99_ItemClick);
            // 
            // barButtonItem100
            // 
            this.barButtonItem100.Caption = "تقرير اليويمة العامه";
            this.barButtonItem100.Id = 115;
            this.barButtonItem100.ImageOptions.SvgImage = global::ByStro.Properties.Resources.next7days;
            this.barButtonItem100.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem100.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem100.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem100.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem100.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem100.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem100.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem100.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem100.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem100.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem100.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem100.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem100.Name = "barButtonItem100";
            this.barButtonItem100.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem100_ItemClick);
            // 
            // barButtonItem102
            // 
            this.barButtonItem102.Caption = "تقرير بارصده العملاء";
            this.barButtonItem102.Id = 117;
            this.barButtonItem102.ImageOptions.SvgImage = global::ByStro.Properties.Resources.bo_person;
            this.barButtonItem102.ItemAppearance.Disabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem102.ItemAppearance.Disabled.Options.UseFont = true;
            this.barButtonItem102.ItemAppearance.Disabled.Options.UseTextOptions = true;
            this.barButtonItem102.ItemAppearance.Disabled.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem102.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem102.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem102.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem102.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem102.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem102.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem102.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem102.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem102.Name = "barButtonItem102";
            // 
            // barButtonItem103
            // 
            this.barButtonItem103.Caption = "ارصده الموردين";
            this.barButtonItem103.Id = 118;
            this.barButtonItem103.ImageOptions.SvgImage = global::ByStro.Properties.Resources.bo_employee1;
            this.barButtonItem103.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem103.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem103.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem103.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem103.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem103.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem103.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem103.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem103.Name = "barButtonItem103";
            this.barButtonItem103.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem103_ItemClick);
            // 
            // barButtonItem104
            // 
            this.barButtonItem104.Caption = "تقرير الصيانه";
            this.barButtonItem104.Id = 119;
            this.barButtonItem104.ImageOptions.SvgImage = global::ByStro.Properties.Resources.bo_security_permission_model;
            this.barButtonItem104.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem104.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem104.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem104.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem104.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem104.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem104.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem104.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem104.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem104.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem104.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem104.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem104.Name = "barButtonItem104";
            this.barButtonItem104.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem104_ItemClick);
            // 
            // barButtonItem105
            // 
            this.barButtonItem105.Caption = "تقرير حضور وانصراف الموظفين";
            this.barButtonItem105.Id = 120;
            this.barButtonItem105.Name = "barButtonItem105";
            // 
            // barButtonItem106
            // 
            this.barButtonItem106.Caption = "تقرير حضور وانصراف الموظفين";
            this.barButtonItem106.Id = 121;
            this.barButtonItem106.Name = "barButtonItem106";
            this.barButtonItem106.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem106_ItemClick);
            // 
            // barButtonItem107
            // 
            this.barButtonItem107.Caption = "تقرير مجمع حضور وانصراف الموظفين";
            this.barButtonItem107.Id = 122;
            this.barButtonItem107.Name = "barButtonItem107";
            // 
            // barButtonItem108
            // 
            this.barButtonItem108.Caption = "مجمع الحضور والانصراف";
            this.barButtonItem108.Id = 123;
            this.barButtonItem108.ImageOptions.SvgImage = global::ByStro.Properties.Resources.bo_scheduler;
            this.barButtonItem108.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem108.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem108.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem108.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem108.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem108.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem108.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem108.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem108.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem108.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem108.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem108.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem108.Name = "barButtonItem108";
            this.barButtonItem108.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem108_ItemClick);
            // 
            // barButtonItem109
            // 
            this.barButtonItem109.Caption = "تقرير بمرتبات الموظفين";
            this.barButtonItem109.Id = 124;
            this.barButtonItem109.Name = "barButtonItem109";
            // 
            // barButtonItem110
            // 
            this.barButtonItem110.Caption = "تقرير بمرتبات الموظفين";
            this.barButtonItem110.Id = 125;
            this.barButtonItem110.ImageOptions.Image = global::ByStro.Properties.Resources.currency_16x16;
            this.barButtonItem110.ImageOptions.LargeImage = global::ByStro.Properties.Resources.currency_32x32;
            this.barButtonItem110.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem110.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem110.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem110.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem110.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem110.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem110.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem110.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem110.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem110.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem110.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem110.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem110.Name = "barButtonItem110";
            this.barButtonItem110.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem110_ItemClick);
            // 
            // barButtonItem111
            // 
            this.barButtonItem111.Caption = "تقرير حضور وانصراف الموظفين";
            this.barButtonItem111.Id = 126;
            this.barButtonItem111.ImageOptions.SvgImage = global::ByStro.Properties.Resources.security_fingerprint;
            this.barButtonItem111.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem111.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem111.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem111.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem111.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem111.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem111.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem111.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem111.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem111.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem111.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem111.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem111.Name = "barButtonItem111";
            this.barButtonItem111.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem111_ItemClick);
            // 
            // barButtonItem112
            // 
            this.barButtonItem112.Caption = "ارصده الاصناف";
            this.barButtonItem112.Id = 127;
            this.barButtonItem112.ImageOptions.SvgImage = global::ByStro.Properties.Resources.bo_note;
            this.barButtonItem112.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem112.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem112.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem112.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem112.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem112.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem112.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem112.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem112.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem112.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem112.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem112.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem112.Name = "barButtonItem112";
            this.barButtonItem112.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem112_ItemClick);
            // 
            // barButtonItem113
            // 
            this.barButtonItem113.Caption = "حركة صنف تفصيلي";
            this.barButtonItem113.Id = 128;
            this.barButtonItem113.Name = "barButtonItem113";
            // 
            // barButtonItem114
            // 
            this.barButtonItem114.Caption = "barButtonItem114";
            this.barButtonItem114.Id = 129;
            this.barButtonItem114.ImageOptions.Image = global::ByStro.Properties.Resources.barcode_16x16;
            this.barButtonItem114.ImageOptions.LargeImage = global::ByStro.Properties.Resources.barcode_32x32;
            this.barButtonItem114.Name = "barButtonItem114";
            // 
            // barButtonItem115
            // 
            this.barButtonItem115.Caption = "حركة صنف تفصيلي";
            this.barButtonItem115.Id = 130;
            this.barButtonItem115.ImageOptions.SvgImage = global::ByStro.Properties.Resources.bo_opportunity;
            this.barButtonItem115.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem115.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem115.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem115.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem115.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem115.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem115.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem115.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem115.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem115.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem115.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem115.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem115.Name = "barButtonItem115";
            this.barButtonItem115.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem115_ItemClick);
            // 
            // barButtonItem116
            // 
            this.barButtonItem116.Caption = "صلاحيه المنتجات";
            this.barButtonItem116.Id = 131;
            this.barButtonItem116.ImageOptions.SvgImage = global::ByStro.Properties.Resources.longdate;
            this.barButtonItem116.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem116.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem116.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem116.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem116.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem116.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem116.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem116.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem116.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem116.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem116.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem116.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem116.Name = "barButtonItem116";
            this.barButtonItem116.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem116_ItemClick);
            // 
            // barButtonItem117
            // 
            this.barButtonItem117.Caption = "النواقص";
            this.barButtonItem117.Id = 132;
            this.barButtonItem117.ImageOptions.SvgImage = global::ByStro.Properties.Resources.strikeout;
            this.barButtonItem117.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem117.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem117.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem117.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem117.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem117.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem117.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem117.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem117.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem117.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem117.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem117.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem117.Name = "barButtonItem117";
            this.barButtonItem117.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem117_ItemClick);
            // 
            // barButtonItem118
            // 
            this.barButtonItem118.Caption = "تقرير المصروفات";
            this.barButtonItem118.Id = 133;
            this.barButtonItem118.ImageOptions.SvgImage = global::ByStro.Properties.Resources.business_dollar1;
            this.barButtonItem118.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem118.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem118.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem118.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem118.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem118.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem118.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem118.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem118.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem118.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem118.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem118.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem118.Name = "barButtonItem118";
            this.barButtonItem118.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem118_ItemClick);
            // 
            // barButtonItem119
            // 
            this.barButtonItem119.Caption = "طباعه باركود";
            this.barButtonItem119.Id = 134;
            this.barButtonItem119.ImageOptions.SvgImage = global::ByStro.Properties.Resources.shopping_barcode;
            this.barButtonItem119.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem119.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem119.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem119.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem119.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem119.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem119.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem119.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem119.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem119.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem119.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem119.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem119.Name = "barButtonItem119";
            this.barButtonItem119.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem119_ItemClick);
            // 
            // barButtonItem120
            // 
            this.barButtonItem120.Caption = "حضور وانصراف";
            this.barButtonItem120.Id = 135;
            this.barButtonItem120.ImageOptions.SvgImage = global::ByStro.Properties.Resources.security_fingerprint1;
            this.barButtonItem120.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem120.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem120.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem120.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem120.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem120.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem120.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem120.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem120.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem120.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem120.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem120.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem120.Name = "barButtonItem120";
            this.barButtonItem120.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem120_ItemClick);
            // 
            // barButtonItem121
            // 
            this.barButtonItem121.Caption = "ارصده العملاء";
            this.barButtonItem121.Id = 136;
            this.barButtonItem121.ImageOptions.SvgImage = global::ByStro.Properties.Resources.bo_customer;
            this.barButtonItem121.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem121.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem121.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem121.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem121.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem121.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem121.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem121.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem121.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.barButtonItem121.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem121.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem121.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem121.Name = "barButtonItem121";
            this.barButtonItem121.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem121_ItemClick);
            // 
            // barButtonItem122
            // 
            this.barButtonItem122.Caption = "فاتورة مشتريات";
            this.barButtonItem122.Id = 137;
            this.barButtonItem122.ImageOptions.SvgImage = global::ByStro.Properties.Resources.buynow;
            this.barButtonItem122.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.barButtonItem122.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem122.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem122.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem122.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.barButtonItem122.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem122.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem122.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem122.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.barButtonItem122.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem122.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem122.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem122.Name = "barButtonItem122";
            this.barButtonItem122.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem122_ItemClick);
            // 
            // barButtonItem123
            // 
            this.barButtonItem123.Caption = "مرتجع المشتريات";
            this.barButtonItem123.Id = 138;
            this.barButtonItem123.ImageOptions.SvgImage = global::ByStro.Properties.Resources.resetlayoutoptions2;
            this.barButtonItem123.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.barButtonItem123.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem123.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem123.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem123.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.barButtonItem123.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem123.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem123.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem123.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.barButtonItem123.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem123.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem123.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem123.Name = "barButtonItem123";
            this.barButtonItem123.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem123_ItemClick);
            // 
            // barButtonItem124
            // 
            this.barButtonItem124.Caption = "فاتورة طلب شراء";
            this.barButtonItem124.Id = 139;
            this.barButtonItem124.ImageOptions.Image = global::ByStro.Properties.Resources.shoppingcart_16x16;
            this.barButtonItem124.ImageOptions.LargeImage = global::ByStro.Properties.Resources.shoppingcart_32x32;
            this.barButtonItem124.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.barButtonItem124.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem124.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem124.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem124.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.barButtonItem124.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem124.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem124.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem124.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.barButtonItem124.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem124.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem124.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem124.Name = "barButtonItem124";
            this.barButtonItem124.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem124_ItemClick);
            // 
            // barEditItem4
            // 
            this.barEditItem4.Caption = "barEditItem4";
            this.barEditItem4.Edit = this.repositoryItemTimeEdit1;
            this.barEditItem4.Id = 140;
            this.barEditItem4.Name = "barEditItem4";
            // 
            // repositoryItemTimeEdit1
            // 
            this.repositoryItemTimeEdit1.AutoHeight = false;
            this.repositoryItemTimeEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTimeEdit1.Name = "repositoryItemTimeEdit1";
            // 
            // skinPaletteRibbonGalleryBarItem1
            // 
            this.skinPaletteRibbonGalleryBarItem1.Caption = "skinPaletteRibbonGalleryBarItem1";
            this.skinPaletteRibbonGalleryBarItem1.Id = 141;
            this.skinPaletteRibbonGalleryBarItem1.Name = "skinPaletteRibbonGalleryBarItem1";
            // 
            // skinRibbonGalleryBarItem1
            // 
            this.skinRibbonGalleryBarItem1.Caption = "skinRibbonGalleryBarItem1";
            this.skinRibbonGalleryBarItem1.Id = 142;
            this.skinRibbonGalleryBarItem1.Name = "skinRibbonGalleryBarItem1";
            // 
            // barButtonItem101
            // 
            this.barButtonItem101.Caption = "barButtonItem101";
            this.barButtonItem101.Id = 145;
            this.barButtonItem101.ImageOptions.SvgImage = global::ByStro.Properties.Resources.changefontstyle;
            this.barButtonItem101.Name = "barButtonItem101";
            this.barButtonItem101.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem101_ItemClick_2);
            // 
            // ribbonPage5
            // 
            this.ribbonPage5.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ribbonPage5.Appearance.Options.UseFont = true;
            this.ribbonPage5.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup7,
            this.ribbonPageGroup20,
            this.ribbonPageGroup25,
            this.ribbonPageGroup26,
            this.ribbonPageGroup27,
            this.ribbonPageGroup28,
            this.ribbonPageGroup48,
            this.ribbonPageGroup49,
            this.ribbonPageGroup50,
            this.ribbonPageGroup51});
            this.ribbonPage5.Name = "ribbonPage5";
            this.ribbonPage5.Text = "المخزن";
            // 
            // ribbonPageGroup7
            // 
            this.ribbonPageGroup7.AllowTextClipping = false;
            this.ribbonPageGroup7.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup7.ItemLinks.Add(this.barButtonItem16);
            this.ribbonPageGroup7.Name = "ribbonPageGroup7";
            // 
            // ribbonPageGroup20
            // 
            this.ribbonPageGroup20.AllowTextClipping = false;
            this.ribbonPageGroup20.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup20.ItemLinks.Add(this.barButtonItem17);
            this.ribbonPageGroup20.Name = "ribbonPageGroup20";
            // 
            // ribbonPageGroup25
            // 
            this.ribbonPageGroup25.ItemLinks.Add(this.barButtonItem66);
            this.ribbonPageGroup25.Name = "ribbonPageGroup25";
            this.ribbonPageGroup25.Text = " ";
            // 
            // ribbonPageGroup26
            // 
            this.ribbonPageGroup26.ItemLinks.Add(this.barButtonItem67);
            this.ribbonPageGroup26.Name = "ribbonPageGroup26";
            this.ribbonPageGroup26.Text = " ";
            // 
            // ribbonPageGroup27
            // 
            this.ribbonPageGroup27.ItemLinks.Add(this.barButtonItem68);
            this.ribbonPageGroup27.Name = "ribbonPageGroup27";
            this.ribbonPageGroup27.Text = " ";
            // 
            // ribbonPageGroup28
            // 
            this.ribbonPageGroup28.ItemLinks.Add(this.barButtonItem69);
            this.ribbonPageGroup28.Name = "ribbonPageGroup28";
            this.ribbonPageGroup28.Text = " ";
            // 
            // ribbonPageGroup48
            // 
            this.ribbonPageGroup48.ItemLinks.Add(this.barButtonItem70);
            this.ribbonPageGroup48.Name = "ribbonPageGroup48";
            this.ribbonPageGroup48.Text = " ";
            // 
            // ribbonPageGroup49
            // 
            this.ribbonPageGroup49.ItemLinks.Add(this.barButtonItem71);
            this.ribbonPageGroup49.Name = "ribbonPageGroup49";
            this.ribbonPageGroup49.Text = " ";
            // 
            // ribbonPageGroup50
            // 
            this.ribbonPageGroup50.ItemLinks.Add(this.barButtonItem72);
            this.ribbonPageGroup50.Name = "ribbonPageGroup50";
            this.ribbonPageGroup50.Text = " ";
            // 
            // ribbonPageGroup51
            // 
            this.ribbonPageGroup51.ItemLinks.Add(this.barButtonItem73);
            this.ribbonPageGroup51.Name = "ribbonPageGroup51";
            this.ribbonPageGroup51.Text = " ";
            // 
            // ا
            // 
            this.ا.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ا.Appearance.Options.UseFont = true;
            this.ا.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup11,
            this.ribbonPageGroup44,
            this.ribbonPageGroup12,
            this.ribbonPageGroup13,
            this.ribbonPageGroup45,
            this.ribbonPageGroup46,
            this.ribbonPageGroup52});
            this.ا.Name = "ا";
            this.ا.Text = "التحكم";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.AllowTextClipping = false;
            this.ribbonPageGroup1.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem2);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            // 
            // ribbonPageGroup11
            // 
            this.ribbonPageGroup11.AllowTextClipping = false;
            this.ribbonPageGroup11.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup11.ItemLinks.Add(this.barButtonItem1);
            this.ribbonPageGroup11.Name = "ribbonPageGroup11";
            // 
            // ribbonPageGroup44
            // 
            this.ribbonPageGroup44.AllowTextClipping = false;
            this.ribbonPageGroup44.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup44.ItemLinks.Add(this.barButtonItem54, true);
            this.ribbonPageGroup44.Name = "ribbonPageGroup44";
            // 
            // ribbonPageGroup12
            // 
            this.ribbonPageGroup12.AllowTextClipping = false;
            this.ribbonPageGroup12.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup12.ItemLinks.Add(this.barButtonItem3);
            this.ribbonPageGroup12.Name = "ribbonPageGroup12";
            // 
            // ribbonPageGroup13
            // 
            this.ribbonPageGroup13.AllowTextClipping = false;
            this.ribbonPageGroup13.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup13.ItemLinks.Add(this.barButtonItem4);
            this.ribbonPageGroup13.Name = "ribbonPageGroup13";
            // 
            // ribbonPageGroup45
            // 
            this.ribbonPageGroup45.AllowTextClipping = false;
            this.ribbonPageGroup45.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup45.ItemLinks.Add(this.barButtonItem55);
            this.ribbonPageGroup45.Name = "ribbonPageGroup45";
            // 
            // ribbonPageGroup46
            // 
            this.ribbonPageGroup46.AllowTextClipping = false;
            this.ribbonPageGroup46.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup46.ItemLinks.Add(this.barButtonItem56);
            this.ribbonPageGroup46.Name = "ribbonPageGroup46";
            // 
            // ribbonPageGroup52
            // 
            this.ribbonPageGroup52.ItemLinks.Add(this.barButtonItem74);
            this.ribbonPageGroup52.Name = "ribbonPageGroup52";
            this.ribbonPageGroup52.Text = " ";
            // 
            // ribbonPage4
            // 
            this.ribbonPage4.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ribbonPage4.Appearance.Options.UseFont = true;
            this.ribbonPage4.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup6,
            this.ribbonPageGroup18,
            this.ribbonPageGroup19,
            this.ribbonPageGroup57});
            this.ribbonPage4.Name = "ribbonPage4";
            this.ribbonPage4.Text = "المبيعات";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.AllowTextClipping = false;
            this.ribbonPageGroup6.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup6.ItemLinks.Add(this.barButtonItem13);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            // 
            // ribbonPageGroup18
            // 
            this.ribbonPageGroup18.AllowTextClipping = false;
            this.ribbonPageGroup18.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup18.ItemLinks.Add(this.barButtonItem14);
            this.ribbonPageGroup18.Name = "ribbonPageGroup18";
            // 
            // ribbonPageGroup19
            // 
            this.ribbonPageGroup19.AllowTextClipping = false;
            this.ribbonPageGroup19.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup19.ItemLinks.Add(this.barButtonItem15);
            this.ribbonPageGroup19.Name = "ribbonPageGroup19";
            // 
            // ribbonPageGroup57
            // 
            this.ribbonPageGroup57.ItemLinks.Add(this.barSubItem4);
            this.ribbonPageGroup57.Name = "ribbonPageGroup57";
            this.ribbonPageGroup57.Text = " ";
            // 
            // ribbonPage3
            // 
            this.ribbonPage3.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ribbonPage3.Appearance.Options.UseFont = true;
            this.ribbonPage3.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup85,
            this.ribbonPageGroup86,
            this.ribbonPageGroup87});
            this.ribbonPage3.Name = "ribbonPage3";
            this.ribbonPage3.Text = "المشتريات";
            // 
            // ribbonPageGroup85
            // 
            this.ribbonPageGroup85.ItemLinks.Add(this.barButtonItem122);
            this.ribbonPageGroup85.Name = "ribbonPageGroup85";
            this.ribbonPageGroup85.Text = " ";
            // 
            // ribbonPageGroup86
            // 
            this.ribbonPageGroup86.ItemLinks.Add(this.barButtonItem123);
            this.ribbonPageGroup86.Name = "ribbonPageGroup86";
            this.ribbonPageGroup86.Text = " ";
            // 
            // ribbonPageGroup87
            // 
            this.ribbonPageGroup87.ItemLinks.Add(this.barButtonItem124);
            this.ribbonPageGroup87.Name = "ribbonPageGroup87";
            this.ribbonPageGroup87.Text = " ";
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ribbonPage1.Appearance.Options.UseFont = true;
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup2,
            this.ribbonPageGroup3,
            this.ribbonPageGroup14,
            this.ribbonPageGroup53,
            this.ribbonPageGroup54});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "العملاء";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.AllowTextClipping = false;
            this.ribbonPageGroup2.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItem5);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.AllowTextClipping = false;
            this.ribbonPageGroup3.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem6);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            // 
            // ribbonPageGroup14
            // 
            this.ribbonPageGroup14.AllowTextClipping = false;
            this.ribbonPageGroup14.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup14.ItemLinks.Add(this.barButtonItem7);
            this.ribbonPageGroup14.Name = "ribbonPageGroup14";
            // 
            // ribbonPageGroup53
            // 
            this.ribbonPageGroup53.ItemLinks.Add(this.barButtonItem75);
            this.ribbonPageGroup53.Name = "ribbonPageGroup53";
            this.ribbonPageGroup53.Text = " ";
            // 
            // ribbonPageGroup54
            // 
            this.ribbonPageGroup54.ItemLinks.Add(this.barButtonItem76);
            this.ribbonPageGroup54.Name = "ribbonPageGroup54";
            this.ribbonPageGroup54.Text = " ";
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ribbonPage2.Appearance.Options.UseFont = true;
            this.ribbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup4,
            this.ribbonPageGroup15,
            this.ribbonPageGroup16,
            this.ribbonPageGroup55,
            this.ribbonPageGroup56});
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "الموردين";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.AllowTextClipping = false;
            this.ribbonPageGroup4.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup4.ItemLinks.Add(this.barButtonItem8);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            // 
            // ribbonPageGroup15
            // 
            this.ribbonPageGroup15.AllowTextClipping = false;
            this.ribbonPageGroup15.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup15.ItemLinks.Add(this.barButtonItem9);
            this.ribbonPageGroup15.Name = "ribbonPageGroup15";
            // 
            // ribbonPageGroup16
            // 
            this.ribbonPageGroup16.AllowTextClipping = false;
            this.ribbonPageGroup16.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup16.ItemLinks.Add(this.barButtonItem10);
            this.ribbonPageGroup16.Name = "ribbonPageGroup16";
            // 
            // ribbonPageGroup55
            // 
            this.ribbonPageGroup55.ItemLinks.Add(this.barButtonItem77);
            this.ribbonPageGroup55.Name = "ribbonPageGroup55";
            this.ribbonPageGroup55.Text = " ";
            // 
            // ribbonPageGroup56
            // 
            this.ribbonPageGroup56.ItemLinks.Add(this.barButtonItem78);
            this.ribbonPageGroup56.Name = "ribbonPageGroup56";
            this.ribbonPageGroup56.Text = " ";
            // 
            // ribbonPage10
            // 
            this.ribbonPage10.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ribbonPage10.Appearance.Options.UseFont = true;
            this.ribbonPage10.Appearance.Options.UseTextOptions = true;
            this.ribbonPage10.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.ribbonPage10.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup36,
            this.ribbonPageGroup37,
            this.ribbonPageGroup38,
            this.ل,
            this.ribbonPageGroup32,
            this.ribbonPageGroup60,
            this.ribbonPageGroup39,
            this.ribbonPageGroup40,
            this.ribbonPageGroup61,
            this.ribbonPageGroup41,
            this.ribbonPageGroup42,
            this.ribbonPageGroup62});
            this.ribbonPage10.Name = "ribbonPage10";
            this.ribbonPage10.Text = "شؤن الموظفين";
            // 
            // ribbonPageGroup36
            // 
            this.ribbonPageGroup36.AllowTextClipping = false;
            this.ribbonPageGroup36.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup36.ItemLinks.Add(this.barButtonItem44);
            this.ribbonPageGroup36.Name = "ribbonPageGroup36";
            // 
            // ribbonPageGroup37
            // 
            this.ribbonPageGroup37.AllowTextClipping = false;
            this.ribbonPageGroup37.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup37.ItemLinks.Add(this.barButtonItem45);
            this.ribbonPageGroup37.Name = "ribbonPageGroup37";
            // 
            // ribbonPageGroup38
            // 
            this.ribbonPageGroup38.AllowTextClipping = false;
            this.ribbonPageGroup38.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup38.ItemLinks.Add(this.barButtonItem49);
            this.ribbonPageGroup38.Name = "ribbonPageGroup38";
            // 
            // ل
            // 
            this.ل.ItemLinks.Add(this.barButtonItem85);
            this.ل.Name = "ل";
            this.ل.Text = " ";
            // 
            // ribbonPageGroup32
            // 
            this.ribbonPageGroup32.ItemLinks.Add(this.barButtonItem86);
            this.ribbonPageGroup32.Name = "ribbonPageGroup32";
            this.ribbonPageGroup32.Text = " ";
            // 
            // ribbonPageGroup60
            // 
            this.ribbonPageGroup60.ItemLinks.Add(this.barButtonItem87);
            this.ribbonPageGroup60.Name = "ribbonPageGroup60";
            this.ribbonPageGroup60.Text = " ";
            // 
            // ribbonPageGroup39
            // 
            this.ribbonPageGroup39.AllowTextClipping = false;
            this.ribbonPageGroup39.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup39.ItemLinks.Add(this.barButtonItem47);
            this.ribbonPageGroup39.Name = "ribbonPageGroup39";
            // 
            // ribbonPageGroup40
            // 
            this.ribbonPageGroup40.ItemLinks.Add(this.barButtonItem48);
            this.ribbonPageGroup40.Name = "ribbonPageGroup40";
            // 
            // ribbonPageGroup61
            // 
            this.ribbonPageGroup61.ItemLinks.Add(this.barButtonItem88);
            this.ribbonPageGroup61.Name = "ribbonPageGroup61";
            this.ribbonPageGroup61.Text = " ";
            // 
            // ribbonPageGroup41
            // 
            this.ribbonPageGroup41.AllowTextClipping = false;
            this.ribbonPageGroup41.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup41.ItemLinks.Add(this.barButtonItem89);
            this.ribbonPageGroup41.Name = "ribbonPageGroup41";
            // 
            // ribbonPageGroup42
            // 
            this.ribbonPageGroup42.AllowTextClipping = false;
            this.ribbonPageGroup42.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup42.ItemLinks.Add(this.barButtonItem50);
            this.ribbonPageGroup42.Name = "ribbonPageGroup42";
            // 
            // ribbonPageGroup62
            // 
            this.ribbonPageGroup62.ItemLinks.Add(this.barButtonItem46);
            this.ribbonPageGroup62.Name = "ribbonPageGroup62";
            this.ribbonPageGroup62.Text = " ";
            // 
            // ribbonPage9
            // 
            this.ribbonPage9.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ribbonPage9.Appearance.Options.UseFont = true;
            this.ribbonPage9.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup30,
            this.ribbonPageGroup31,
            this.ribbonPageGroup33,
            this.ribbonPageGroup34});
            this.ribbonPage9.Name = "ribbonPage9";
            this.ribbonPage9.Text = "الايداع والصرف";
            // 
            // ribbonPageGroup30
            // 
            this.ribbonPageGroup30.AllowTextClipping = false;
            this.ribbonPageGroup30.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup30.ItemLinks.Add(this.barButtonItem29);
            this.ribbonPageGroup30.Name = "ribbonPageGroup30";
            // 
            // ribbonPageGroup31
            // 
            this.ribbonPageGroup31.ItemLinks.Add(this.barSubItem2);
            this.ribbonPageGroup31.Name = "ribbonPageGroup31";
            this.ribbonPageGroup31.Text = " ";
            // 
            // ribbonPageGroup33
            // 
            this.ribbonPageGroup33.AllowTextClipping = false;
            this.ribbonPageGroup33.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup33.ItemLinks.Add(this.barButtonItem35);
            this.ribbonPageGroup33.Name = "ribbonPageGroup33";
            this.ribbonPageGroup33.Text = "تحويل رصيد";
            // 
            // ribbonPageGroup34
            // 
            this.ribbonPageGroup34.AllowTextClipping = false;
            this.ribbonPageGroup34.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup34.ItemLinks.Add(this.barButtonItem36);
            this.ribbonPageGroup34.Name = "ribbonPageGroup34";
            // 
            // ribbonPage6
            // 
            this.ribbonPage6.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ribbonPage6.Appearance.Options.UseFont = true;
            this.ribbonPage6.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup8,
            this.ribbonPageGroup21,
            this.ribbonPageGroup22,
            this.ribbonPageGroup43,
            this.ribbonPageGroup47,
            this.ribbonPageGroup35});
            this.ribbonPage6.Name = "ribbonPage6";
            this.ribbonPage6.Text = "الصيانه";
            // 
            // ribbonPageGroup8
            // 
            this.ribbonPageGroup8.AllowTextClipping = false;
            this.ribbonPageGroup8.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup8.ItemLinks.Add(this.barButtonItem18);
            this.ribbonPageGroup8.Name = "ribbonPageGroup8";
            // 
            // ribbonPageGroup21
            // 
            this.ribbonPageGroup21.AllowTextClipping = false;
            this.ribbonPageGroup21.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup21.ItemLinks.Add(this.barButtonItem19);
            this.ribbonPageGroup21.Name = "ribbonPageGroup21";
            // 
            // ribbonPageGroup22
            // 
            this.ribbonPageGroup22.AllowTextClipping = false;
            this.ribbonPageGroup22.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup22.ItemLinks.Add(this.barButtonItem20);
            this.ribbonPageGroup22.Name = "ribbonPageGroup22";
            // 
            // ribbonPageGroup43
            // 
            this.ribbonPageGroup43.AllowTextClipping = false;
            this.ribbonPageGroup43.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup43.ItemLinks.Add(this.barButtonItem51);
            this.ribbonPageGroup43.Name = "ribbonPageGroup43";
            this.ribbonPageGroup43.Text = " ";
            // 
            // ribbonPageGroup47
            // 
            this.ribbonPageGroup47.AllowTextClipping = false;
            this.ribbonPageGroup47.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup47.ItemLinks.Add(this.barButtonItem62);
            this.ribbonPageGroup47.Name = "ribbonPageGroup47";
            // 
            // ribbonPageGroup35
            // 
            this.ribbonPageGroup35.ItemLinks.Add(this.barButtonItem53);
            this.ribbonPageGroup35.Name = "ribbonPageGroup35";
            this.ribbonPageGroup35.Text = " ";
            // 
            // ribbonPage8
            // 
            this.ribbonPage8.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ribbonPage8.Appearance.Options.UseFont = true;
            this.ribbonPage8.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup10,
            this.ribbonPageGroup29,
            this.ribbonPageGroup59,
            this.ribbonPageGroup66,
            this.ribbonPageGroup67});
            this.ribbonPage8.Name = "ribbonPage8";
            this.ribbonPage8.Text = "الارباح والخسائر";
            // 
            // ribbonPageGroup10
            // 
            this.ribbonPageGroup10.AllowTextClipping = false;
            this.ribbonPageGroup10.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup10.ItemLinks.Add(this.barButtonItem27);
            this.ribbonPageGroup10.Name = "ribbonPageGroup10";
            // 
            // ribbonPageGroup29
            // 
            this.ribbonPageGroup29.AllowTextClipping = false;
            this.ribbonPageGroup29.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup29.ItemLinks.Add(this.barButtonItem28);
            this.ribbonPageGroup29.Name = "ribbonPageGroup29";
            // 
            // ribbonPageGroup59
            // 
            this.ribbonPageGroup59.ItemLinks.Add(this.barButtonItem96);
            this.ribbonPageGroup59.Name = "ribbonPageGroup59";
            this.ribbonPageGroup59.Text = " ";
            // 
            // ribbonPageGroup66
            // 
            this.ribbonPageGroup66.ItemLinks.Add(this.barButtonItem97);
            this.ribbonPageGroup66.Name = "ribbonPageGroup66";
            // 
            // ribbonPageGroup67
            // 
            this.ribbonPageGroup67.ItemLinks.Add(this.barButtonItem98);
            this.ribbonPageGroup67.Name = "ribbonPageGroup67";
            // 
            // ribbonPage7
            // 
            this.ribbonPage7.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ribbonPage7.Appearance.Options.UseFont = true;
            this.ribbonPage7.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup9,
            this.ribbonPageGroup71,
            this.ribbonPageGroup73,
            this.ribbonPageGroup74,
            this.ribbonPageGroup75,
            this.ribbonPageGroup76,
            this.ribbonPageGroup79,
            this.ribbonPageGroup81,
            this.ribbonPageGroup78,
            this.ribbonPageGroup82,
            this.ribbonPageGroup77,
            this.ribbonPageGroup80,
            this.ribbonPageGroup83,
            this.ribbonPageGroup84});
            this.ribbonPage7.Name = "ribbonPage7";
            this.ribbonPage7.Text = "التقارير";
            // 
            // ribbonPageGroup9
            // 
            this.ribbonPageGroup9.ItemLinks.Add(this.barButtonItem99);
            this.ribbonPageGroup9.Name = "ribbonPageGroup9";
            this.ribbonPageGroup9.Text = " ";
            // 
            // ribbonPageGroup71
            // 
            this.ribbonPageGroup71.ItemLinks.Add(this.barButtonItem100);
            this.ribbonPageGroup71.Name = "ribbonPageGroup71";
            this.ribbonPageGroup71.Text = " ";
            // 
            // ribbonPageGroup73
            // 
            this.ribbonPageGroup73.ItemLinks.Add(this.barButtonItem121);
            this.ribbonPageGroup73.Name = "ribbonPageGroup73";
            this.ribbonPageGroup73.Text = " ";
            // 
            // ribbonPageGroup74
            // 
            this.ribbonPageGroup74.ItemLinks.Add(this.barButtonItem103);
            this.ribbonPageGroup74.Name = "ribbonPageGroup74";
            this.ribbonPageGroup74.Text = " ";
            // 
            // ribbonPageGroup75
            // 
            this.ribbonPageGroup75.ItemLinks.Add(this.barButtonItem104);
            this.ribbonPageGroup75.Name = "ribbonPageGroup75";
            this.ribbonPageGroup75.Text = " ";
            // 
            // ribbonPageGroup76
            // 
            this.ribbonPageGroup76.ItemLinks.Add(this.barButtonItem120);
            this.ribbonPageGroup76.Name = "ribbonPageGroup76";
            this.ribbonPageGroup76.Text = " ";
            // 
            // ribbonPageGroup79
            // 
            this.ribbonPageGroup79.ItemLinks.Add(this.barButtonItem108);
            this.ribbonPageGroup79.Name = "ribbonPageGroup79";
            this.ribbonPageGroup79.Text = " ";
            // 
            // ribbonPageGroup81
            // 
            this.ribbonPageGroup81.ItemLinks.Add(this.barButtonItem110);
            this.ribbonPageGroup81.Name = "ribbonPageGroup81";
            this.ribbonPageGroup81.Text = " ";
            // 
            // ribbonPageGroup78
            // 
            this.ribbonPageGroup78.ItemLinks.Add(this.barButtonItem112);
            this.ribbonPageGroup78.Name = "ribbonPageGroup78";
            this.ribbonPageGroup78.Text = " ";
            // 
            // ribbonPageGroup82
            // 
            this.ribbonPageGroup82.ItemLinks.Add(this.barButtonItem115);
            this.ribbonPageGroup82.Name = "ribbonPageGroup82";
            this.ribbonPageGroup82.Text = " ";
            // 
            // ribbonPageGroup77
            // 
            this.ribbonPageGroup77.ItemLinks.Add(this.barButtonItem116);
            this.ribbonPageGroup77.Name = "ribbonPageGroup77";
            this.ribbonPageGroup77.Text = " ";
            // 
            // ribbonPageGroup80
            // 
            this.ribbonPageGroup80.ItemLinks.Add(this.barButtonItem117);
            this.ribbonPageGroup80.Name = "ribbonPageGroup80";
            this.ribbonPageGroup80.Text = " ";
            // 
            // ribbonPageGroup83
            // 
            this.ribbonPageGroup83.ItemLinks.Add(this.barButtonItem118);
            this.ribbonPageGroup83.Name = "ribbonPageGroup83";
            this.ribbonPageGroup83.Text = " ";
            // 
            // ribbonPageGroup84
            // 
            this.ribbonPageGroup84.ItemLinks.Add(this.barButtonItem119);
            this.ribbonPageGroup84.Name = "ribbonPageGroup84";
            this.ribbonPageGroup84.Text = " ";
            // 
            // ribbonPage11
            // 
            this.ribbonPage11.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ribbonPage11.Appearance.Options.UseFont = true;
            this.ribbonPage11.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup23,
            this.ribbonPageGroup63});
            this.ribbonPage11.Name = "ribbonPage11";
            this.ribbonPage11.Text = "الدعم الفني";
            // 
            // ribbonPageGroup23
            // 
            this.ribbonPageGroup23.ItemLinks.Add(this.barButtonItem90);
            this.ribbonPageGroup23.Name = "ribbonPageGroup23";
            this.ribbonPageGroup23.Text = " ";
            // 
            // ribbonPageGroup63
            // 
            this.ribbonPageGroup63.ItemLinks.Add(this.barButtonItem92);
            this.ribbonPageGroup63.Name = "ribbonPageGroup63";
            this.ribbonPageGroup63.Text = " ";
            // 
            // ribbonPage12
            // 
            this.ribbonPage12.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ribbonPage12.Appearance.Options.UseFont = true;
            this.ribbonPage12.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup24,
            this.ribbonPageGroup64,
            this.ribbonPageGroup65});
            this.ribbonPage12.Name = "ribbonPage12";
            this.ribbonPage12.Text = "المستخدمين";
            // 
            // ribbonPageGroup24
            // 
            this.ribbonPageGroup24.ItemLinks.Add(this.barButtonItem93);
            this.ribbonPageGroup24.Name = "ribbonPageGroup24";
            this.ribbonPageGroup24.Text = " ";
            // 
            // ribbonPageGroup64
            // 
            this.ribbonPageGroup64.ItemLinks.Add(this.barButtonItem94);
            this.ribbonPageGroup64.Name = "ribbonPageGroup64";
            this.ribbonPageGroup64.Text = " ";
            // 
            // ribbonPageGroup65
            // 
            this.ribbonPageGroup65.ItemLinks.Add(this.barButtonItem95);
            this.ribbonPageGroup65.Name = "ribbonPageGroup65";
            this.ribbonPageGroup65.Text = " ";
            // 
            // tileControl1
            // 
            this.tileControl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup3);
            this.tileControl1.Groups.Add(this.tileGroup5);
            this.tileControl1.IndentBetweenGroups = 10;
            this.tileControl1.IndentBetweenItems = 4;
            this.tileControl1.ItemSize = 180;
            this.tileControl1.Location = new System.Drawing.Point(0, 155);
            this.tileControl1.MaxId = 18;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tileControl1.SelectionColor = System.Drawing.Color.Black;
            this.tileControl1.Size = new System.Drawing.Size(1761, 516);
            this.tileControl1.TabIndex = 114;
            this.tileControl1.Text = "tileControl1";
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.tileItem7);
            this.tileGroup2.Items.Add(this.tileItem8);
            this.tileGroup2.Items.Add(this.tileItem9);
            this.tileGroup2.Items.Add(this.tileItem10);
            this.tileGroup2.Name = "tileGroup2";
            // 
            // tileItem7
            // 
            this.tileItem7.CurrentFrameIndex = 1;
            tileItemElement1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            tileItemElement1.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemElement1.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement1.Text = "صلاحيات المستخدمين";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.tileItem7.Elements.Add(tileItemElement1);
            tileItemFrame1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            tileItemFrame1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            tileItemFrame1.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            tileItemFrame1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame1.Appearance.Options.UseBackColor = true;
            tileItemFrame1.Appearance.Options.UseBorderColor = true;
            tileItemFrame1.Appearance.Options.UseFont = true;
            tileItemElement2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            tileItemElement2.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemElement2.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement2.Text = "صلاحيات المستخدمين";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame1.Elements.Add(tileItemElement2);
            tileItemFrame1.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame1.Image")));
            tileItemFrame2.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame2.Appearance.Options.UseFont = true;
            tileItemElement3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            tileItemElement3.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemElement3.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement3.Text = "صلاحيات المستخدمين";
            tileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame2.Elements.Add(tileItemElement3);
            tileItemFrame2.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame2.Image")));
            tileItemFrame3.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(17)))), ((int)(((byte)(98)))));
            tileItemFrame3.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(17)))), ((int)(((byte)(98)))));
            tileItemFrame3.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(17)))), ((int)(((byte)(98)))));
            tileItemFrame3.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame3.Appearance.Options.UseBackColor = true;
            tileItemFrame3.Appearance.Options.UseBorderColor = true;
            tileItemFrame3.Appearance.Options.UseFont = true;
            tileItemElement4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            tileItemElement4.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemElement4.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement4.Text = "صلاحيات المستخدمين";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame3.Elements.Add(tileItemElement4);
            tileItemFrame3.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame3.Image")));
            this.tileItem7.Frames.Add(tileItemFrame1);
            this.tileItem7.Frames.Add(tileItemFrame2);
            this.tileItem7.Frames.Add(tileItemFrame3);
            this.tileItem7.Id = 6;
            this.tileItem7.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.tileItem7.Name = "tileItem7";
            this.tileItem7.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tileItem7_ItemClick);
            // 
            // tileItem8
            // 
            this.tileItem8.CurrentFrameIndex = 1;
            tileItemElement5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            tileItemElement5.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement5.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement5.Text = "حسابات الموردين";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.tileItem8.Elements.Add(tileItemElement5);
            tileItemFrame4.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(152)))), ((int)(((byte)(0)))));
            tileItemFrame4.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(152)))), ((int)(((byte)(0)))));
            tileItemFrame4.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(152)))), ((int)(((byte)(0)))));
            tileItemFrame4.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame4.Appearance.Options.UseBackColor = true;
            tileItemFrame4.Appearance.Options.UseBorderColor = true;
            tileItemFrame4.Appearance.Options.UseFont = true;
            tileItemElement6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            tileItemElement6.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement6.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement6.Text = "حسابات الموردين";
            tileItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame4.Elements.Add(tileItemElement6);
            tileItemFrame4.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame4.Image")));
            tileItemFrame5.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(182)))), ((int)(((byte)(246)))));
            tileItemFrame5.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(182)))), ((int)(((byte)(246)))));
            tileItemFrame5.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(182)))), ((int)(((byte)(246)))));
            tileItemFrame5.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame5.Appearance.Options.UseBackColor = true;
            tileItemFrame5.Appearance.Options.UseBorderColor = true;
            tileItemFrame5.Appearance.Options.UseFont = true;
            tileItemElement7.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            tileItemElement7.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement7.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement7.Text = "حسابات الموردين";
            tileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame5.Elements.Add(tileItemElement7);
            tileItemFrame5.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame5.Image")));
            tileItemFrame6.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(255)))), ((int)(((byte)(65)))));
            tileItemFrame6.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(255)))), ((int)(((byte)(65)))));
            tileItemFrame6.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(255)))), ((int)(((byte)(65)))));
            tileItemFrame6.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame6.Appearance.ForeColor = System.Drawing.Color.Black;
            tileItemFrame6.Appearance.Options.UseBackColor = true;
            tileItemFrame6.Appearance.Options.UseBorderColor = true;
            tileItemFrame6.Appearance.Options.UseFont = true;
            tileItemFrame6.Appearance.Options.UseForeColor = true;
            tileItemElement8.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image7")));
            tileItemElement8.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement8.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement8.Text = "حسابات الموردين";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame6.Elements.Add(tileItemElement8);
            tileItemFrame6.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame6.Image")));
            this.tileItem8.Frames.Add(tileItemFrame4);
            this.tileItem8.Frames.Add(tileItemFrame5);
            this.tileItem8.Frames.Add(tileItemFrame6);
            this.tileItem8.Id = 7;
            this.tileItem8.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.tileItem8.Name = "tileItem8";
            this.tileItem8.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tileItem8_ItemClick);
            // 
            // tileItem9
            // 
            this.tileItem9.CurrentFrameIndex = 1;
            tileItemElement9.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image8")));
            tileItemElement9.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement9.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement9.Text = "اضافه منتجات";
            tileItemElement9.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.tileItem9.Elements.Add(tileItemElement9);
            tileItemFrame7.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(0)))), ((int)(((byte)(13)))));
            tileItemFrame7.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(0)))), ((int)(((byte)(13)))));
            tileItemFrame7.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(0)))), ((int)(((byte)(13)))));
            tileItemFrame7.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame7.Appearance.Options.UseBackColor = true;
            tileItemFrame7.Appearance.Options.UseBorderColor = true;
            tileItemFrame7.Appearance.Options.UseFont = true;
            tileItemElement10.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image9")));
            tileItemElement10.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement10.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement10.Text = "اضافه منتجات";
            tileItemElement10.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame7.Elements.Add(tileItemElement10);
            tileItemFrame7.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame7.Image")));
            tileItemFrame8.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
            tileItemFrame8.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
            tileItemFrame8.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
            tileItemFrame8.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame8.Appearance.Options.UseBackColor = true;
            tileItemFrame8.Appearance.Options.UseBorderColor = true;
            tileItemFrame8.Appearance.Options.UseFont = true;
            tileItemElement11.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image10")));
            tileItemElement11.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement11.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement11.Text = "اضافه منتجات";
            tileItemElement11.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame8.Elements.Add(tileItemElement11);
            tileItemFrame8.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame8.Image")));
            tileItemFrame9.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(121)))), ((int)(((byte)(97)))));
            tileItemFrame9.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(121)))), ((int)(((byte)(97)))));
            tileItemFrame9.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(121)))), ((int)(((byte)(97)))));
            tileItemFrame9.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame9.Appearance.Options.UseBackColor = true;
            tileItemFrame9.Appearance.Options.UseBorderColor = true;
            tileItemFrame9.Appearance.Options.UseFont = true;
            tileItemElement12.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image11")));
            tileItemElement12.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement12.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement12.Text = "اضافه منتجات";
            tileItemElement12.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame9.Elements.Add(tileItemElement12);
            tileItemFrame9.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame9.Image")));
            this.tileItem9.Frames.Add(tileItemFrame7);
            this.tileItem9.Frames.Add(tileItemFrame8);
            this.tileItem9.Frames.Add(tileItemFrame9);
            this.tileItem9.Id = 8;
            this.tileItem9.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.tileItem9.Name = "tileItem9";
            this.tileItem9.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tileItem9_ItemClick);
            // 
            // tileItem10
            // 
            this.tileItem10.CurrentFrameIndex = 1;
            tileItemElement13.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image12")));
            tileItemElement13.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement13.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement13.Text = "حسابات العملاء";
            tileItemElement13.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.tileItem10.Elements.Add(tileItemElement13);
            tileItemFrame10.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(85)))), ((int)(((byte)(72)))));
            tileItemFrame10.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(85)))), ((int)(((byte)(72)))));
            tileItemFrame10.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(121)))), ((int)(((byte)(85)))), ((int)(((byte)(72)))));
            tileItemFrame10.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame10.Appearance.Options.UseBackColor = true;
            tileItemFrame10.Appearance.Options.UseBorderColor = true;
            tileItemFrame10.Appearance.Options.UseFont = true;
            tileItemElement14.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image13")));
            tileItemElement14.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement14.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement14.Text = "حسابات العملاء";
            tileItemElement14.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame10.Elements.Add(tileItemElement14);
            tileItemFrame10.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame10.Image")));
            tileItemFrame11.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(96)))), ((int)(((byte)(125)))), ((int)(((byte)(139)))));
            tileItemFrame11.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(96)))), ((int)(((byte)(125)))), ((int)(((byte)(139)))));
            tileItemFrame11.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(96)))), ((int)(((byte)(125)))), ((int)(((byte)(139)))));
            tileItemFrame11.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame11.Appearance.Options.UseBackColor = true;
            tileItemFrame11.Appearance.Options.UseBorderColor = true;
            tileItemFrame11.Appearance.Options.UseFont = true;
            tileItemElement15.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image14")));
            tileItemElement15.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement15.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement15.Text = "حسابات العملاء";
            tileItemElement15.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame11.Elements.Add(tileItemElement15);
            tileItemFrame11.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame11.Image")));
            tileItemFrame12.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(145)))), ((int)(((byte)(234)))));
            tileItemFrame12.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(145)))), ((int)(((byte)(234)))));
            tileItemFrame12.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(145)))), ((int)(((byte)(234)))));
            tileItemFrame12.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame12.Appearance.Options.UseBackColor = true;
            tileItemFrame12.Appearance.Options.UseBorderColor = true;
            tileItemFrame12.Appearance.Options.UseFont = true;
            tileItemElement16.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image15")));
            tileItemElement16.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement16.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement16.Text = "حسابات العملاء";
            tileItemElement16.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame12.Elements.Add(tileItemElement16);
            tileItemFrame12.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame12.Image")));
            this.tileItem10.Frames.Add(tileItemFrame10);
            this.tileItem10.Frames.Add(tileItemFrame11);
            this.tileItem10.Frames.Add(tileItemFrame12);
            this.tileItem10.Id = 9;
            this.tileItem10.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.tileItem10.Name = "tileItem10";
            this.tileItem10.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tileItem10_ItemClick);
            // 
            // tileGroup3
            // 
            this.tileGroup3.Items.Add(this.tileItem5);
            this.tileGroup3.Items.Add(this.tileItem6);
            this.tileGroup3.Name = "tileGroup3";
            // 
            // tileItem5
            // 
            tileItemElement17.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image16")));
            tileItemElement17.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement17.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement17.Text = "ادارة المشتريات";
            tileItemElement17.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.tileItem5.Elements.Add(tileItemElement17);
            tileItemFrame13.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(7)))));
            tileItemFrame13.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(7)))));
            tileItemFrame13.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(193)))), ((int)(((byte)(7)))));
            tileItemFrame13.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame13.Appearance.Options.UseBackColor = true;
            tileItemFrame13.Appearance.Options.UseBorderColor = true;
            tileItemFrame13.Appearance.Options.UseFont = true;
            tileItemElement18.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image17")));
            tileItemElement18.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement18.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement18.Text = "ادارة المشتريات";
            tileItemElement18.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame13.Elements.Add(tileItemElement18);
            tileItemFrame13.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame13.Image")));
            this.tileItem5.Frames.Add(tileItemFrame13);
            this.tileItem5.Id = 4;
            this.tileItem5.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.tileItem5.Name = "tileItem5";
            this.tileItem5.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tileItem5_ItemClick);
            // 
            // tileItem6
            // 
            this.tileItem6.CurrentFrameIndex = 1;
            tileItemElement19.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image18")));
            tileItemElement19.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement19.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement19.Text = "ادارة المبيعات";
            tileItemElement19.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.tileItem6.Elements.Add(tileItemElement19);
            tileItemFrame14.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(95)))));
            tileItemFrame14.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(95)))));
            tileItemFrame14.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(95)))));
            tileItemFrame14.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame14.Appearance.Options.UseBackColor = true;
            tileItemFrame14.Appearance.Options.UseBorderColor = true;
            tileItemFrame14.Appearance.Options.UseFont = true;
            tileItemElement20.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image19")));
            tileItemElement20.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement20.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement20.Text = "ادارة المبيعات";
            tileItemElement20.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame14.Elements.Add(tileItemElement20);
            tileItemFrame14.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame14.Image")));
            tileItemFrame15.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(150)))), ((int)(((byte)(136)))));
            tileItemFrame15.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(150)))), ((int)(((byte)(136)))));
            tileItemFrame15.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(150)))), ((int)(((byte)(136)))));
            tileItemFrame15.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame15.Appearance.Options.UseBackColor = true;
            tileItemFrame15.Appearance.Options.UseBorderColor = true;
            tileItemFrame15.Appearance.Options.UseFont = true;
            tileItemElement21.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image20")));
            tileItemElement21.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement21.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement21.Text = "ادارة المبيعات";
            tileItemElement21.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame15.Elements.Add(tileItemElement21);
            tileItemFrame15.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame15.Image")));
            this.tileItem6.Frames.Add(tileItemFrame14);
            this.tileItem6.Frames.Add(tileItemFrame15);
            this.tileItem6.Id = 5;
            this.tileItem6.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.tileItem6.Name = "tileItem6";
            this.tileItem6.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tileItem6_ItemClick);
            // 
            // tileGroup5
            // 
            this.tileGroup5.Items.Add(this.tileItem2);
            this.tileGroup5.Items.Add(this.tileItem3);
            this.tileGroup5.Items.Add(this.tileItem4);
            this.tileGroup5.Items.Add(this.tileItem1);
            this.tileGroup5.Name = "tileGroup5";
            // 
            // tileItem2
            // 
            this.tileItem2.CurrentFrameIndex = 1;
            tileItemElement22.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image21")));
            tileItemElement22.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement22.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement22.Text = "حد الطلب من المنتجات";
            tileItemElement22.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.tileItem2.Elements.Add(tileItemElement22);
            tileItemFrame16.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(150)))), ((int)(((byte)(136)))));
            tileItemFrame16.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(150)))), ((int)(((byte)(136)))));
            tileItemFrame16.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(150)))), ((int)(((byte)(136)))));
            tileItemFrame16.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame16.Appearance.Options.UseBackColor = true;
            tileItemFrame16.Appearance.Options.UseBorderColor = true;
            tileItemFrame16.Appearance.Options.UseFont = true;
            tileItemElement23.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image22")));
            tileItemElement23.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement23.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement23.Text = "حد الطلب من المنتجات";
            tileItemElement23.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame16.Elements.Add(tileItemElement23);
            tileItemFrame16.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame16.Image")));
            tileItemFrame17.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame17.Appearance.Options.UseFont = true;
            tileItemElement24.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image23")));
            tileItemElement24.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement24.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement24.Text = "حد الطلب من المنتجات";
            tileItemElement24.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame17.Elements.Add(tileItemElement24);
            tileItemFrame17.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame17.Image")));
            this.tileItem2.Frames.Add(tileItemFrame16);
            this.tileItem2.Frames.Add(tileItemFrame17);
            this.tileItem2.Id = 1;
            this.tileItem2.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.tileItem2.Name = "tileItem2";
            this.tileItem2.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tileItem2_ItemClick);
            // 
            // tileItem3
            // 
            this.tileItem3.CurrentFrameIndex = 1;
            tileItemElement25.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image24")));
            tileItemElement25.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement25.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement25.Text = "مرتجعات";
            tileItemElement25.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.tileItem3.Elements.Add(tileItemElement25);
            tileItemFrame18.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(62)))), ((int)(((byte)(177)))));
            tileItemFrame18.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(62)))), ((int)(((byte)(177)))));
            tileItemFrame18.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(62)))), ((int)(((byte)(177)))));
            tileItemFrame18.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame18.Appearance.Options.UseBackColor = true;
            tileItemFrame18.Appearance.Options.UseBorderColor = true;
            tileItemFrame18.Appearance.Options.UseFont = true;
            tileItemElement26.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image25")));
            tileItemElement26.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement26.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement26.Text = "مرتجعات";
            tileItemElement26.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame18.Elements.Add(tileItemElement26);
            tileItemFrame18.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame18.Image")));
            tileItemFrame19.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(90)))), ((int)(((byte)(254)))));
            tileItemFrame19.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(90)))), ((int)(((byte)(254)))));
            tileItemFrame19.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(61)))), ((int)(((byte)(90)))), ((int)(((byte)(254)))));
            tileItemFrame19.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame19.Appearance.Options.UseBackColor = true;
            tileItemFrame19.Appearance.Options.UseBorderColor = true;
            tileItemFrame19.Appearance.Options.UseFont = true;
            tileItemElement27.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image26")));
            tileItemElement27.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement27.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement27.Text = "مرتجعات";
            tileItemElement27.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame19.Elements.Add(tileItemElement27);
            tileItemFrame19.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame19.Image")));
            tileItemFrame20.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(123)))), ((int)(((byte)(254)))));
            tileItemFrame20.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(123)))), ((int)(((byte)(254)))));
            tileItemFrame20.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(99)))), ((int)(((byte)(123)))), ((int)(((byte)(254)))));
            tileItemFrame20.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame20.Appearance.Options.UseBackColor = true;
            tileItemFrame20.Appearance.Options.UseBorderColor = true;
            tileItemFrame20.Appearance.Options.UseFont = true;
            tileItemElement28.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image27")));
            tileItemElement28.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement28.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement28.Text = "مرتجعات";
            tileItemElement28.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame20.Elements.Add(tileItemElement28);
            tileItemFrame20.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame20.Image")));
            this.tileItem3.Frames.Add(tileItemFrame18);
            this.tileItem3.Frames.Add(tileItemFrame19);
            this.tileItem3.Frames.Add(tileItemFrame20);
            this.tileItem3.Id = 2;
            this.tileItem3.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.tileItem3.Name = "tileItem3";
            this.tileItem3.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tileItem3_ItemClick);
            // 
            // tileItem4
            // 
            this.tileItem4.CurrentFrameIndex = 1;
            tileItemElement29.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image28")));
            tileItemElement29.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement29.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement29.Text = "تقرير ارباح المبيعات";
            tileItemElement29.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.tileItem4.Elements.Add(tileItemElement29);
            tileItemFrame21.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(109)))), ((int)(((byte)(0)))));
            tileItemFrame21.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(109)))), ((int)(((byte)(0)))));
            tileItemFrame21.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(109)))), ((int)(((byte)(0)))));
            tileItemFrame21.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame21.Appearance.Options.UseBackColor = true;
            tileItemFrame21.Appearance.Options.UseBorderColor = true;
            tileItemFrame21.Appearance.Options.UseFont = true;
            tileItemElement30.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image29")));
            tileItemElement30.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement30.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement30.Text = "تقرير ارباح المبيعات";
            tileItemElement30.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame21.Elements.Add(tileItemElement30);
            tileItemFrame21.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame21.Image")));
            tileItemFrame22.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(145)))), ((int)(((byte)(0)))));
            tileItemFrame22.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(145)))), ((int)(((byte)(0)))));
            tileItemFrame22.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(145)))), ((int)(((byte)(0)))));
            tileItemFrame22.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame22.Appearance.Options.UseBackColor = true;
            tileItemFrame22.Appearance.Options.UseBorderColor = true;
            tileItemFrame22.Appearance.Options.UseFont = true;
            tileItemElement31.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image30")));
            tileItemElement31.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement31.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement31.Text = "تقرير ارباح المبيعات";
            tileItemElement31.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame22.Elements.Add(tileItemElement31);
            tileItemFrame22.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame22.Image")));
            this.tileItem4.Frames.Add(tileItemFrame21);
            this.tileItem4.Frames.Add(tileItemFrame22);
            this.tileItem4.Id = 3;
            this.tileItem4.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.tileItem4.Name = "tileItem4";
            this.tileItem4.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tileItem4_ItemClick);
            // 
            // tileItem1
            // 
            this.tileItem1.CurrentFrameIndex = 1;
            tileItemElement32.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image31")));
            tileItemElement32.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement32.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement32.Text = "رصيد الخزنة";
            tileItemElement32.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.tileItem1.Elements.Add(tileItemElement32);
            tileItemFrame23.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(0)))), ((int)(((byte)(60)))));
            tileItemFrame23.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(0)))), ((int)(((byte)(60)))));
            tileItemFrame23.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(0)))), ((int)(((byte)(60)))));
            tileItemFrame23.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame23.Appearance.Options.UseBackColor = true;
            tileItemFrame23.Appearance.Options.UseBorderColor = true;
            tileItemFrame23.Appearance.Options.UseFont = true;
            tileItemElement33.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image32")));
            tileItemElement33.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement33.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement33.Text = "رصيد الخزنة";
            tileItemElement33.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame23.Elements.Add(tileItemElement33);
            tileItemFrame23.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame23.Image")));
            tileItemFrame24.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(0)))), ((int)(((byte)(87)))));
            tileItemFrame24.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(0)))), ((int)(((byte)(87)))));
            tileItemFrame24.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(0)))), ((int)(((byte)(87)))));
            tileItemFrame24.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame24.Appearance.Options.UseBackColor = true;
            tileItemFrame24.Appearance.Options.UseBorderColor = true;
            tileItemFrame24.Appearance.Options.UseFont = true;
            tileItemElement34.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image33")));
            tileItemElement34.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement34.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement34.Text = "رصيد الخزنة";
            tileItemElement34.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame24.Elements.Add(tileItemElement34);
            tileItemFrame24.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame24.Image")));
            tileItemFrame25.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(51)))), ((int)(((byte)(120)))));
            tileItemFrame25.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(51)))), ((int)(((byte)(120)))));
            tileItemFrame25.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(51)))), ((int)(((byte)(120)))));
            tileItemFrame25.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame25.Appearance.Options.UseBackColor = true;
            tileItemFrame25.Appearance.Options.UseBorderColor = true;
            tileItemFrame25.Appearance.Options.UseFont = true;
            tileItemElement35.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image34")));
            tileItemElement35.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement35.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement35.Text = "رصيد الخزنة";
            tileItemElement35.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame25.Elements.Add(tileItemElement35);
            tileItemFrame25.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame25.Image")));
            this.tileItem1.Frames.Add(tileItemFrame23);
            this.tileItem1.Frames.Add(tileItemFrame24);
            this.tileItem1.Frames.Add(tileItemFrame25);
            this.tileItem1.Id = 0;
            this.tileItem1.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.tileItem1.Name = "tileItem1";
            this.tileItem1.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tileItem1_ItemClick);
            // 
            // barButtonItem63
            // 
            this.barButtonItem63.Caption = "المجموعات";
            this.barButtonItem63.Id = 19;
            this.barButtonItem63.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem63.ImageOptions.Image")));
            this.barButtonItem63.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem63.ImageOptions.LargeImage")));
            this.barButtonItem63.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem63.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem63.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem63.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem63.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem63.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem63.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem63.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem63.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem63.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem63.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem63.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem63.Name = "barButtonItem63";
            this.barButtonItem63.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem64
            // 
            this.barButtonItem64.Caption = "المجموعات";
            this.barButtonItem64.Id = 19;
            this.barButtonItem64.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem64.ImageOptions.Image")));
            this.barButtonItem64.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem64.ImageOptions.LargeImage")));
            this.barButtonItem64.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem64.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem64.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem64.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem64.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem64.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem64.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem64.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem64.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem64.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem64.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem64.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem64.Name = "barButtonItem64";
            this.barButtonItem64.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonItem65
            // 
            this.barButtonItem65.Caption = "المجموعات";
            this.barButtonItem65.Id = 19;
            this.barButtonItem65.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem65.ImageOptions.Image")));
            this.barButtonItem65.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem65.ImageOptions.LargeImage")));
            this.barButtonItem65.ItemAppearance.Hovered.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem65.ItemAppearance.Hovered.Options.UseFont = true;
            this.barButtonItem65.ItemAppearance.Hovered.Options.UseTextOptions = true;
            this.barButtonItem65.ItemAppearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem65.ItemAppearance.Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem65.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonItem65.ItemAppearance.Normal.Options.UseTextOptions = true;
            this.barButtonItem65.ItemAppearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem65.ItemAppearance.Pressed.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barButtonItem65.ItemAppearance.Pressed.Options.UseFont = true;
            this.barButtonItem65.ItemAppearance.Pressed.Options.UseTextOptions = true;
            this.barButtonItem65.ItemAppearance.Pressed.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.barButtonItem65.Name = "barButtonItem65";
            this.barButtonItem65.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // ribbonPageGroup68
            // 
            this.ribbonPageGroup68.AllowTextClipping = false;
            this.ribbonPageGroup68.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup68.ItemLinks.Add(this.barButtonItem56);
            this.ribbonPageGroup68.Name = "ribbonPageGroup68";
            // 
            // ribbonPageGroup69
            // 
            this.ribbonPageGroup69.AllowTextClipping = false;
            this.ribbonPageGroup69.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup69.ItemLinks.Add(this.barButtonItem56);
            this.ribbonPageGroup69.Name = "ribbonPageGroup69";
            // 
            // ribbonPageGroup70
            // 
            this.ribbonPageGroup70.AllowTextClipping = false;
            this.ribbonPageGroup70.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup70.ItemLinks.Add(this.barButtonItem56);
            this.ribbonPageGroup70.Name = "ribbonPageGroup70";
            // 
            // ribbonPageGroup17
            // 
            this.ribbonPageGroup17.AllowTextClipping = false;
            this.ribbonPageGroup17.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup17.Name = "ribbonPageGroup17";
            // 
            // ribbonPageGroup58
            // 
            this.ribbonPageGroup58.Name = "ribbonPageGroup58";
            this.ribbonPageGroup58.Text = " ";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.AllowTextClipping = false;
            this.ribbonPageGroup5.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            // 
            // tileItem11
            // 
            tileItemElement36.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image35")));
            tileItemElement36.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemElement36.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement36.Text = "صلاحيات المستخدمين";
            tileItemElement36.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.tileItem11.Elements.Add(tileItemElement36);
            tileItemFrame26.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            tileItemFrame26.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            tileItemFrame26.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(150)))), ((int)(((byte)(243)))));
            tileItemFrame26.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame26.Appearance.Options.UseBackColor = true;
            tileItemFrame26.Appearance.Options.UseBorderColor = true;
            tileItemFrame26.Appearance.Options.UseFont = true;
            tileItemElement37.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image36")));
            tileItemElement37.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemElement37.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement37.Text = "صلاحيات المستخدمين";
            tileItemElement37.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame26.Elements.Add(tileItemElement37);
            tileItemFrame26.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame26.Image")));
            tileItemFrame27.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame27.Appearance.Options.UseFont = true;
            tileItemElement38.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image37")));
            tileItemElement38.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemElement38.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement38.Text = "صلاحيات المستخدمين";
            tileItemElement38.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame27.Elements.Add(tileItemElement38);
            tileItemFrame27.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame27.Image")));
            tileItemFrame28.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(17)))), ((int)(((byte)(98)))));
            tileItemFrame28.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(17)))), ((int)(((byte)(98)))));
            tileItemFrame28.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(17)))), ((int)(((byte)(98)))));
            tileItemFrame28.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame28.Appearance.Options.UseBackColor = true;
            tileItemFrame28.Appearance.Options.UseBorderColor = true;
            tileItemFrame28.Appearance.Options.UseFont = true;
            tileItemElement39.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image38")));
            tileItemElement39.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemElement39.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement39.Text = "صلاحيات المستخدمين";
            tileItemElement39.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame28.Elements.Add(tileItemElement39);
            tileItemFrame28.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame28.Image")));
            this.tileItem11.Frames.Add(tileItemFrame26);
            this.tileItem11.Frames.Add(tileItemFrame27);
            this.tileItem11.Frames.Add(tileItemFrame28);
            this.tileItem11.Id = 6;
            this.tileItem11.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.tileItem11.Name = "tileItem11";
            // 
            // tileItem12
            // 
            tileItemElement40.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image39")));
            tileItemElement40.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement40.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement40.Text = "اضافه منتجات";
            tileItemElement40.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            this.tileItem12.Elements.Add(tileItemElement40);
            tileItemFrame29.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(0)))), ((int)(((byte)(13)))));
            tileItemFrame29.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(0)))), ((int)(((byte)(13)))));
            tileItemFrame29.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(0)))), ((int)(((byte)(13)))));
            tileItemFrame29.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame29.Appearance.Options.UseBackColor = true;
            tileItemFrame29.Appearance.Options.UseBorderColor = true;
            tileItemFrame29.Appearance.Options.UseFont = true;
            tileItemElement41.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image40")));
            tileItemElement41.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement41.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement41.Text = "اضافه منتجات";
            tileItemElement41.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame29.Elements.Add(tileItemElement41);
            tileItemFrame29.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame29.Image")));
            tileItemFrame30.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
            tileItemFrame30.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
            tileItemFrame30.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
            tileItemFrame30.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame30.Appearance.Options.UseBackColor = true;
            tileItemFrame30.Appearance.Options.UseBorderColor = true;
            tileItemFrame30.Appearance.Options.UseFont = true;
            tileItemElement42.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image41")));
            tileItemElement42.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement42.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement42.Text = "اضافه منتجات";
            tileItemElement42.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame30.Elements.Add(tileItemElement42);
            tileItemFrame30.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame30.Image")));
            tileItemFrame31.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(121)))), ((int)(((byte)(97)))));
            tileItemFrame31.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(121)))), ((int)(((byte)(97)))));
            tileItemFrame31.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(121)))), ((int)(((byte)(97)))));
            tileItemFrame31.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tileItemFrame31.Appearance.Options.UseBackColor = true;
            tileItemFrame31.Appearance.Options.UseBorderColor = true;
            tileItemFrame31.Appearance.Options.UseFont = true;
            tileItemElement43.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image42")));
            tileItemElement43.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement43.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Top;
            tileItemElement43.Text = "اضافه منتجات";
            tileItemElement43.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileItemFrame31.Elements.Add(tileItemElement43);
            tileItemFrame31.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame31.Image")));
            this.tileItem12.Frames.Add(tileItemFrame29);
            this.tileItem12.Frames.Add(tileItemFrame30);
            this.tileItem12.Frames.Add(tileItemFrame31);
            this.tileItem12.Id = 8;
            this.tileItem12.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.tileItem12.Name = "tileItem12";
            // 
            // applicationMenu1
            // 
            this.applicationMenu1.Name = "applicationMenu1";
            this.applicationMenu1.Ribbon = this.ribbonControl1;
            // 
            // Main_frm
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1761, 671);
            this.Controls.Add(this.ribbonStatusBar1);
            this.Controls.Add(this.tileControl1);
            this.Controls.Add(this.ribbonControl1);
            this.Controls.Add(this.ToolStrip2);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Tahoma", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.IconOptions.Icon = ((System.Drawing.Icon)(resources.GetObject("Main_frm.IconOptions.Icon")));
            this.ImeMode = System.Windows.Forms.ImeMode.Hangul;
            this.IsMdiContainer = true;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "Main_frm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Engaz POS";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_Form_FormClosing);
            this.Load += new System.EventHandler(this.Main_Form_Load_1);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Main_Form_KeyDown);
            this.Move += new System.EventHandler(this.Main_Form_Move);
            this.ToolStrip2.ResumeLayout(false);
            this.ToolStrip2.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHypertextLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHypertextLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTimeEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        internal System.Windows.Forms.ToolStripMenuItem تغيركلمةالمرورToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem تسجيلخروجToolStripMenuItem;
        public System.Windows.Forms.ToolStrip ToolStrip2;
        private System.Windows.Forms.ToolStripMenuItem خروجToolStripMenuItem;
        public System.Windows.Forms.ToolStripDropDownButton Username;
        public System.Windows.Forms.ToolStripDropDownButton CS_01;
        public System.Windows.Forms.ToolStripDropDownButton CS_15;
        public System.Windows.Forms.ToolStripDropDownButton CS_19;
        public System.Windows.Forms.ToolStripDropDownButton CS_24;
        public System.Windows.Forms.ToolStripDropDownButton CS_42;
        public System.Windows.Forms.ToolStripDropDownButton CS_46;
        public System.Windows.Forms.ToolStripDropDownButton CS_58;
        public System.Windows.Forms.ToolStripDropDownButton CS_53;
        public System.Windows.Forms.ToolStripDropDownButton CS_10;
        public System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        internal System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        internal System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem CS_02;
        public System.Windows.Forms.ToolStripMenuItem CS_03;
        public System.Windows.Forms.ToolStripMenuItem CS_05;
        public System.Windows.Forms.ToolStripMenuItem CS_06;
        public System.Windows.Forms.ToolStripMenuItem CS_07;
        public System.Windows.Forms.ToolStripMenuItem CS_11;
        public System.Windows.Forms.ToolStripMenuItem CS_14;
        public System.Windows.Forms.ToolStripMenuItem CS11;
        public System.Windows.Forms.ToolStripMenuItem CS_18;
        public System.Windows.Forms.ToolStripMenuItem CS_20;
        public System.Windows.Forms.ToolStripMenuItem CS_21;
        public System.Windows.Forms.ToolStripMenuItem CS_22;
        public System.Windows.Forms.ToolStripMenuItem CS_25;
        public System.Windows.Forms.ToolStripMenuItem CS_26;
        public System.Windows.Forms.ToolStripMenuItem CS_69;
        public System.Windows.Forms.ToolStripMenuItem CS_28;
        public System.Windows.Forms.ToolStripMenuItem CS_55;
        public System.Windows.Forms.ToolStripMenuItem CS_56;
        public System.Windows.Forms.ToolStripMenuItem CS_57;
        public System.Windows.Forms.ToolStripMenuItem CS_48;
        public System.Windows.Forms.ToolStripMenuItem CS_49;
        public System.Windows.Forms.ToolStripMenuItem CS_50;
        public System.Windows.Forms.ToolStripMenuItem CS_59;
        public System.Windows.Forms.ToolStripMenuItem CS_60;
        public System.Windows.Forms.ToolStripMenuItem CS_23;
        public System.Windows.Forms.ToolStripMenuItem CS_61;
        public System.Windows.Forms.ToolStripMenuItem CS_47;
        public System.Windows.Forms.ToolStripMenuItem CHClearAllData;
        public System.Windows.Forms.ToolStripMenuItem CS_51;
        public System.Windows.Forms.ToolStripMenuItem CS_52;
        public System.Windows.Forms.ToolStripDropDownButton CS_29;
        public System.Windows.Forms.ToolStripMenuItem CS_30;
        public System.Windows.Forms.ToolStripMenuItem CS_04;
        public System.Windows.Forms.ToolStripMenuItem CS_09;
        public System.Windows.Forms.ToolStripMenuItem CS_27;
        public System.Windows.Forms.ToolStripMenuItem CS_37;
        public System.Windows.Forms.ToolStripMenuItem CS_38;
        public System.Windows.Forms.ToolStripMenuItem CS_35;
        public System.Windows.Forms.ToolStripMenuItem CS_41;
        public System.Windows.Forms.ToolStripMenuItem CS_33;
        public System.Windows.Forms.ToolStripMenuItem CS_40;
        public System.Windows.Forms.ToolStripMenuItem CS_34;
        public System.Windows.Forms.ToolStripMenuItem CS_32;
        public System.Windows.Forms.ToolStripMenuItem CS_31;
        private System.Windows.Forms.ToolStripMenuItem CHExelCustomer_frm;
        private System.Windows.Forms.ToolStripMenuItem chExelSuppliers_frm;
        private System.Windows.Forms.ToolStripMenuItem مجموعةاصنافToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem الاصنافToolStripMenuItem;
        private System.Windows.Forms.Timer timer1;
        public System.Windows.Forms.ToolStripMenuItem CS_39;
        public System.Windows.Forms.ToolStripMenuItem CS_36;
        public System.Windows.Forms.ToolStripMenuItem CS_70;
        public System.Windows.Forms.ToolStripMenuItem CS_67;
        public System.Windows.Forms.ToolStripMenuItem CS_66;
        public System.Windows.Forms.ToolStripMenuItem CS_68;
        public System.Windows.Forms.ToolStripMenuItem CS_54;
        public System.Windows.Forms.ToolStripMenuItem CS_62;
        public System.Windows.Forms.ToolStripMenuItem CS_71;
        public System.Windows.Forms.ToolStripMenuItem CS_72;
        private System.Windows.Forms.ToolStripMenuItem ChangeFont;
        private System.Windows.Forms.ToolStripMenuItem CreateNewCompany_frm;
        private System.Windows.Forms.ToolStripMenuItem enuItem;
        public System.Windows.Forms.ToolStripDropDownButton Fills;
        private System.Windows.Forms.ToolStripMenuItem OpenNewCompany_frm;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ToolStripMenuItem CustomersFirst_frm;
        public System.Windows.Forms.ToolStripMenuItem CS25;
        public System.Windows.Forms.ToolStripMenuItem CS_73;
        public System.Windows.Forms.ToolStripMenuItem Maintenance_frm;
        public System.Windows.Forms.ToolStripDropDownButton CS_75;
        public System.Windows.Forms.ToolStripMenuItem CS_80;
        public System.Windows.Forms.ToolStripMenuItem CS_81;
        public System.Windows.Forms.ToolStripMenuItem CS_76;
        public System.Windows.Forms.ToolStripMenuItem CS_77;
        public System.Windows.Forms.ToolStripMenuItem CS_78;
        public System.Windows.Forms.ToolStripMenuItem CS_79;
        public System.Windows.Forms.ToolStripMenuItem CS_74;
        private System.Windows.Forms.ToolStripMenuItem MaintenanceCompeny;
        private System.Windows.Forms.ToolStripMenuItem BillMaintenance_frm;
        private System.Windows.Forms.ToolStripMenuItem Currency_frm;
        private System.Windows.Forms.ToolStripMenuItem طباعةباركودToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem casherToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem فاتورةعرضسعرToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem سندتحويلأموالToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem فاتورةطلبالشراءToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem بحثالاصنافToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem الاقساطToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem قائمهالاقساطToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem اضافهقسطToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem سداددفعهقسطToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem عرضالاقساطالمستحقهToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem عرضالدفعاتالمسددهToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem نماذجالباركودToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem واردمنصرفخذنهToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem اعداداتالقرائهمنالميزانToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem التوصيلToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem السائقونToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem امرتوصيلجديدToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem اوامرالتوصيلالجاريهToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem اوامرالتوصيلالمنتهيةToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem تغييرالخلفيهToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem الالوانToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem الاحجامToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem حركهصنفتفصيليToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ارصدهالاصنافToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem صافيالربحاوالخسارهToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem اكثرالاصنافربحاToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem صلاحياتالاصنافToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem الارباحمنالعملاءToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem سندهالكاصنافToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem اصنافوصلتاليحدالطلبToolStripMenuItem;
		private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar1;
		private DevExpress.XtraBars.BarStaticItem barStaticItem2;
		private DevExpress.XtraBars.BarStaticItem barStaticItem3;
		private DevExpress.XtraBars.BarStaticItem barStaticItem4;
		private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
		private DevExpress.XtraBars.BarButtonItem barButtonItem1;
		private DevExpress.XtraBars.BarButtonItem barButtonItem2;
		private DevExpress.XtraBars.BarButtonItem barButtonItem3;
		private DevExpress.XtraBars.BarButtonItem barButtonItem4;
		private DevExpress.XtraBars.BarButtonItem barButtonItem5;
		private DevExpress.XtraBars.BarButtonItem barButtonItem6;
		private DevExpress.XtraBars.BarButtonItem barButtonItem7;
		private DevExpress.XtraBars.BarButtonItem barButtonItem8;
		private DevExpress.XtraBars.BarButtonItem barButtonItem9;
		private DevExpress.XtraBars.BarButtonItem barButtonItem10;
		private DevExpress.XtraBars.BarButtonItem barButtonItem11;
		private DevExpress.XtraBars.BarButtonItem barButtonItem12;
		private DevExpress.XtraBars.BarSubItem barSubItem1;
		private DevExpress.XtraBars.BarButtonItem barButtonItem13;
		private DevExpress.XtraBars.BarButtonItem barButtonItem14;
		private DevExpress.XtraBars.BarButtonItem barButtonItem15;
		private DevExpress.XtraBars.BarButtonItem barButtonItem16;
		private DevExpress.XtraBars.BarButtonItem barButtonItem17;
		private DevExpress.XtraBars.BarButtonItem barButtonItem18;
		private DevExpress.XtraBars.BarButtonItem barButtonItem19;
		private DevExpress.XtraBars.BarButtonItem barButtonItem20;
		private DevExpress.XtraBars.BarButtonItem barButtonItem21;
		private DevExpress.XtraBars.BarButtonItem barButtonItem22;
		private DevExpress.XtraBars.BarButtonItem barButtonItem23;
		private DevExpress.XtraBars.BarButtonItem barButtonItem24;
		private DevExpress.XtraBars.BarButtonItem barButtonItem25;
		private DevExpress.XtraBars.BarButtonItem barButtonItem26;
		private DevExpress.XtraBars.BarButtonItem barButtonItem27;
		private DevExpress.XtraBars.BarButtonItem barButtonItem28;
		private DevExpress.XtraBars.BarButtonItem barButtonItem29;
		private DevExpress.XtraBars.BarButtonItem barButtonItem30;
		private DevExpress.XtraBars.BarButtonItem barButtonItem31;
		private DevExpress.XtraBars.BarButtonItem barButtonItem32;
		private DevExpress.XtraBars.BarButtonItem barButtonItem33;
		private DevExpress.XtraBars.BarButtonItem barButtonItem34;
		private DevExpress.XtraBars.BarButtonItem barButtonItem35;
		private DevExpress.XtraBars.BarButtonItem barButtonItem36;
		private DevExpress.XtraBars.BarButtonItem barButtonItem37;
		private DevExpress.XtraBars.BarSubItem barSubItem2;
		private DevExpress.XtraBars.BarButtonItem barButtonItem38;
		private DevExpress.XtraBars.BarButtonItem barButtonItem39;
		private DevExpress.XtraBars.BarButtonItem barButtonItem40;
		private DevExpress.XtraBars.BarButtonItem barButtonItem41;
		private DevExpress.XtraBars.BarButtonItem barButtonItem42;
		private DevExpress.XtraBars.BarButtonItem barButtonItem43;
		private DevExpress.XtraBars.BarButtonItem barButtonItem44;
		private DevExpress.XtraBars.BarButtonItem barButtonItem45;
		private DevExpress.XtraBars.BarButtonItem barButtonItem46;
		private DevExpress.XtraBars.BarButtonItem barButtonItem47;
		private DevExpress.XtraBars.BarButtonItem barButtonItem48;
		private DevExpress.XtraBars.BarButtonItem barButtonItem49;
		private DevExpress.XtraBars.BarButtonItem barButtonItem50;
		private DevExpress.XtraBars.BarButtonItem barButtonItem51;
		private DevExpress.XtraBars.BarButtonItem barButtonItem52;
		private DevExpress.XtraBars.BarButtonItem barButtonItem53;
		private DevExpress.XtraBars.BarButtonItem barButtonItem54;
		private DevExpress.XtraBars.BarButtonItem barButtonItem55;
		private DevExpress.XtraBars.BarSubItem barSubItem3;
		private DevExpress.XtraBars.BarButtonItem barButtonItem57;
		private DevExpress.XtraBars.BarButtonItem barButtonItem58;
		private DevExpress.XtraBars.BarButtonItem barButtonItem59;
		private DevExpress.XtraBars.BarButtonItem barButtonItem60;
		private DevExpress.XtraBars.BarButtonItem barButtonItem61;
		private DevExpress.XtraBars.BarButtonItem barButtonItem62;
		private DevExpress.XtraBars.BarStaticItem barStaticItem1;
		private DevExpress.XtraBars.Ribbon.RibbonPage ا;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup44;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup11;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup12;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup13;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup45;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup46;
		private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup14;
		private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup15;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup16;
		private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage4;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup18;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup19;
		private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage5;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup7;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup20;
		private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage9;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup30;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup31;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup33;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup34;
		private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage10;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup36;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup37;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup38;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup39;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup40;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup41;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup42;
		private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage6;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup8;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup21;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup22;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup43;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup47;
		private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage8;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup10;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup29;
		private DevExpress.XtraEditors.TileControl tileControl1;
		private DevExpress.XtraEditors.TileGroup tileGroup2;
		private DevExpress.XtraEditors.TileItem tileItem7;
		private DevExpress.XtraEditors.TileItem tileItem8;
		private DevExpress.XtraEditors.TileItem tileItem9;
		private DevExpress.XtraEditors.TileItem tileItem10;
		private DevExpress.XtraEditors.TileGroup tileGroup3;
		private DevExpress.XtraEditors.TileItem tileItem5;
		private DevExpress.XtraEditors.TileItem tileItem6;
		private DevExpress.XtraEditors.TileItem tileItem1;
		private DevExpress.XtraEditors.TileItem tileItem2;
		private DevExpress.XtraEditors.TileItem tileItem3;
		private DevExpress.XtraEditors.TileItem tileItem4;
		private DevExpress.XtraBars.BarButtonItem barButtonItem66;
		private DevExpress.XtraBars.BarButtonItem barButtonItem67;
		private DevExpress.XtraBars.BarButtonItem barButtonItem68;
		private DevExpress.XtraBars.BarButtonItem barButtonItem69;
		private DevExpress.XtraBars.BarButtonItem barButtonItem70;
		private DevExpress.XtraBars.BarButtonItem barButtonItem71;
		private DevExpress.XtraBars.BarButtonItem barButtonItem72;
		private DevExpress.XtraBars.BarButtonItem barButtonItem73;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup25;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup26;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup27;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup28;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup48;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup49;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup50;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup51;
		private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage7;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup9;
		private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage11;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup23;
		private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage12;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup24;
		private DevExpress.XtraBars.BarButtonItem barButtonItem63;
		private DevExpress.XtraBars.BarButtonItem barButtonItem64;
		private DevExpress.XtraBars.BarButtonItem barButtonItem65;
		private DevExpress.XtraBars.BarButtonItem barButtonItem74;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup52;
		private DevExpress.XtraBars.BarButtonItem barButtonItem75;
		private DevExpress.XtraBars.BarButtonItem barButtonItem76;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup53;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup54;
		private DevExpress.XtraBars.BarButtonItem barButtonItem77;
		private DevExpress.XtraBars.BarButtonItem barButtonItem78;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup55;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup56;
		private DevExpress.XtraBars.BarButtonItem barButtonItem56;
		private DevExpress.XtraBars.BarEditItem barEditItem1;
		private DevExpress.XtraEditors.Repository.RepositoryItemHypertextLabel repositoryItemHypertextLabel1;
		private DevExpress.XtraBars.BarEditItem barEditItem2;
		private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
		private DevExpress.XtraBars.BarButtonItem barButtonItem79;
		private DevExpress.XtraBars.BarEditItem barEditItem3;
		private DevExpress.XtraEditors.Repository.RepositoryItemHypertextLabel repositoryItemHypertextLabel2;
		private DevExpress.XtraBars.BarSubItem barSubItem4;
		private DevExpress.XtraBars.BarButtonItem barButtonItem80;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup57;
		private DevExpress.XtraBars.BarButtonItem barButtonItem81;
		private DevExpress.XtraBars.BarButtonItem barButtonItem82;
		private DevExpress.XtraBars.BarButtonItem barButtonItem83;
		private DevExpress.XtraBars.BarButtonItem barButtonItem84;
		private DevExpress.XtraBars.BarSubItem barSubItem5;
		private DevExpress.XtraBars.BarButtonItem barButtonItem85;
		private DevExpress.XtraBars.BarButtonItem barButtonItem86;
		private DevExpress.XtraBars.BarButtonItem barButtonItem87;
		private DevExpress.XtraBars.BarButtonItem barButtonItem88;
		private DevExpress.XtraBars.BarButtonItem barButtonItem89;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ل;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup32;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup60;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup61;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup62;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup35;
		private DevExpress.XtraBars.BarButtonItem barButtonItem90;
		private DevExpress.XtraBars.BarButtonItem barButtonItem91;
		private DevExpress.XtraBars.BarButtonItem barButtonItem92;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup63;
		private DevExpress.XtraBars.BarButtonItem barButtonItem93;
		private DevExpress.XtraBars.BarButtonItem barButtonItem94;
		private DevExpress.XtraBars.BarButtonItem barButtonItem95;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup64;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup65;
		private DevExpress.XtraBars.BarButtonItem barButtonItem96;
		private DevExpress.XtraBars.BarButtonItem barButtonItem97;
		private DevExpress.XtraBars.BarButtonItem barButtonItem98;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup59;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup66;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup67;
		private DevExpress.XtraBars.BarButtonItem barButtonItem99;
		private DevExpress.XtraBars.BarButtonItem barButtonItem100;
		private DevExpress.XtraBars.BarButtonItem barButtonItem102;
		private DevExpress.XtraBars.BarButtonItem barButtonItem103;
		private DevExpress.XtraBars.BarButtonItem barButtonItem104;
		private DevExpress.XtraBars.BarButtonItem barButtonItem105;
		private DevExpress.XtraBars.BarButtonItem barButtonItem106;
		private DevExpress.XtraBars.BarButtonItem barButtonItem107;
		private DevExpress.XtraBars.BarButtonItem barButtonItem108;
		private DevExpress.XtraBars.BarButtonItem barButtonItem109;
		private DevExpress.XtraBars.BarButtonItem barButtonItem110;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup71;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup73;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup74;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup75;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup79;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup81;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup76;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup78;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup68;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup69;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup70;
		private DevExpress.XtraBars.BarButtonItem barButtonItem111;
		private DevExpress.XtraBars.BarButtonItem barButtonItem112;
		private DevExpress.XtraBars.BarButtonItem barButtonItem113;
		private DevExpress.XtraBars.BarButtonItem barButtonItem114;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup82;
		private DevExpress.XtraBars.BarButtonItem barButtonItem115;
		private DevExpress.XtraBars.BarButtonItem barButtonItem116;
		private DevExpress.XtraBars.BarButtonItem barButtonItem117;
		private DevExpress.XtraBars.BarButtonItem barButtonItem118;
		private DevExpress.XtraBars.BarButtonItem barButtonItem119;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup77;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup80;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup83;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup84;
		private DevExpress.XtraBars.BarButtonItem barButtonItem120;
		private DevExpress.XtraBars.BarButtonItem barButtonItem121;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup17;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup58;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
		private DevExpress.XtraBars.BarButtonItem barButtonItem122;
		private DevExpress.XtraBars.BarButtonItem barButtonItem123;
		private DevExpress.XtraBars.BarButtonItem barButtonItem124;
		private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage3;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup85;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup86;
		private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup87;
		private DevExpress.XtraEditors.TileGroup tileGroup5;
		private DevExpress.XtraEditors.TileItem tileItem11;
		private DevExpress.XtraEditors.TileItem tileItem12;
		private DevExpress.XtraBars.BarEditItem barEditItem4;
		private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit repositoryItemTimeEdit1;
		private DevExpress.XtraBars.Ribbon.ApplicationMenu applicationMenu2;
		private DevExpress.XtraBars.Ribbon.ApplicationMenu applicationMenu1;
		private DevExpress.XtraBars.SkinPaletteRibbonGalleryBarItem skinPaletteRibbonGalleryBarItem1;
        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGalleryBarItem1;
		private DevExpress.XtraBars.BarButtonItem barButtonItem101;
	}
}