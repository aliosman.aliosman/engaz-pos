﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing;
using ByStro.RPT;
using DevExpress.XtraGrid.Columns;
using System.Collections.Generic;

namespace ByStro.PL
{
    public partial class CustomersTrans_Form : Form
    {
        public CustomersTrans_Form()
        {
             
            InitializeComponent();
        }

        Customers_cls clsCustomer = new Customers_cls();
        DataAccessLayer DataAccessLayer = new DataAccessLayer();
        public string AddEdit = "Add";
        private void CustomersTrans_Form_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingToolStrip(toolStrip1);
            ERB_Setting.SettingForm(this); 
            if (AddEdit == "Add")
            {
                txtAccountID.Text = "";
                txtAccountName.Text = ""; 
                txt_1.Text = "0";
                txt_2.Text = "0";
                txt_3.Text = "0";
            }
            else
            {
                btnNew_Click(null, null);
            }
        }
        DataTable dt_Print = new DataTable();







        private void btnNew_Click(object sender, EventArgs e)
        {



            try
            {
                if (txtAccountName.Text.Trim() == "")
                {
                    txtAccountName.BackColor = Color.Pink;
                    button3_Click(null, null);
                    return;
                }
                Customers_cls Customers_cls = new Customers_cls();
                DataTable Dt_ = new DataTable();
                Dt_ = Customers_cls.Details_Customers(txtAccountID.Text);


                 x = 0;
                 y = 0;
                 SumDebit = 0;
                 SumCredit = 0;


                CustomerTrans();



                Boolean chISCusSupp;
                if (!string.IsNullOrEmpty(Dt_.Rows[0]["ISCusSupp"].ToString()))
                {
                    chISCusSupp = (Boolean)Dt_.Rows[0]["ISCusSupp"];
                    if (chISCusSupp==true)
                    {
                        string suppid = Dt_.Rows[0]["SupplierID"].ToString();
                        SuppliersTrans(suppid);
                    }
         
                }


               txt_1.Text = x.ToString();
               txt_2.Text = y.ToString();
               Decimal D = x - y;
               txt_3.Text = D.ToString(); 
               gridControl1.DataSource = dt_Print;
                gridView1.Columns.Clear();
                int i = 0;
                foreach (var item in IntColumns())
                {
                    gridView1.Columns.Add(item);
                    item.VisibleIndex = i;
                    item.OptionsColumn.AllowEdit = false;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Massage", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        Decimal x = 0;
        Decimal y = 0;

        Decimal SumDebit = 0;
        Decimal SumCredit = 0;

        public void CustomerTrans()
        {

            try
            {
                //Decimal x = 0;
                //Decimal y = 0;
                RbtTreasuryMovement_cls cls = new RbtTreasuryMovement_cls();
                DataTable Dt2 = cls.TreasuryMovement(datefrom.Value, datetto.Value, txtAccountID.Text, "Cus");
                //===========================================================================================
                DataTable Dt = cls.TreasuryMovement2(datefrom.Value, txtAccountID.Text, "Cus");
                if (Dt.Rows[0][0] == DBNull.Value == false)
                    SumDebit = Convert.ToDecimal(Dt.Rows[0][0]);

                if (Dt.Rows[0][1] == DBNull.Value == false)
                    SumCredit = Convert.ToDecimal(Dt.Rows[0][1]);

                //====================================================================================

                SqlDataAdapter da_Print = new SqlDataAdapter(@"SELECT  dbo.TreasuryMovement.*, dbo.UserPermissions.EmpName  FROM  dbo.TreasuryMovement INNER JOIN
                         dbo.UserPermissions ON dbo.TreasuryMovement.UserAdd = dbo.UserPermissions.ID where VoucherID ='0' ORDER BY VoucherDate", DataAccessLayer.connSQLServer);
                //dt_Print = new DataTable();

                da_Print.Fill(dt_Print);
                da_Print.Dispose();
                dt_Print.Clear();
                DataRow DR = dt_Print.NewRow();
                DR["VoucherID"] = "0";
                DR["VoucherType"] = "رصيد سابق";
                DR["VoucherDate"] = DateTime.Now.ToString("yyyy/MM/dd");
                DR["Remark"] = "رصيد الفترة السابقة للعميل";
                DR["Debit"] = SumDebit.ToString();
                DR["Credit"] = SumCredit.ToString();
                dt_Print.Rows.Add(DR);
                x += Convert.ToDecimal(SumDebit.ToString());
                y += Convert.ToDecimal(SumCredit.ToString());



                for (int i = 0; i < Dt2.Rows.Count; i++)
                {
                    DR = dt_Print.NewRow();
                    DR["VoucherID"] = Dt2.Rows[i]["VoucherID"];
                    DR["VoucherType"] = Dt2.Rows[i]["VoucherType"];
                    DR["VoucherDate"] = Dt2.Rows[i]["VoucherDate"];
                    DR["Remark"] = Dt2.Rows[i]["Remark"];
                    DR["Debit"] = Dt2.Rows[i]["Debit"];
                    DR["Credit"] = Dt2.Rows[i]["Credit"];
                    DR["EmpName"] = Dt2.Rows[i]["EmpName"];
                    dt_Print.Rows.Add(DR);
                    x += Convert.ToDecimal(Dt2.Rows[i]["Debit"]);
                    y += Convert.ToDecimal(Dt2.Rows[i]["Credit"]);
                }


                //txt_1.Text = x.ToString();
                //txt_2.Text = y.ToString();
                //Decimal D = x - y;
                //txt_3.Text = D.ToString();
                //DGV1.AutoGenerateColumns = false;
                //DGV1.DataSource = dt_Print;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Massage", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



//            DataTable Dt2 = cls.CustomerTrans(txtAccountID.Text, datefrom.Value, datetto.Value);
//            //===========================================================================================
//            DataTable Dt = cls.CustomerTrans2(txtAccountID.Text, datefrom.Value);

     
//            if (Dt.Rows[0][0] == DBNull.Value == false)
//                SumDebit = Convert.ToDecimal(Dt.Rows[0][0]);

//            if (Dt.Rows[0][1] == DBNull.Value == false)
//                SumCredit = Convert.ToDecimal(Dt.Rows[0][1]);

//            //====================================================================================

//            SqlDataAdapter da_Print = new SqlDataAdapter(@"SELECT    dbo.Customers_trans.*, dbo.UserPermissions.EmpName FROM  dbo.Customers_trans INNER JOIN
//                         dbo.UserPermissions ON dbo.Customers_trans.UserAdd = dbo.UserPermissions.ID where VoucherID ='0' ORDER BY VoucherDate", DataAccessLayer.connSQLServer);
//            //dt_Print = new DataTable();

//            da_Print.Fill(dt_Print);
//            da_Print.Dispose();
//            dt_Print.Clear();
//            DataRow DR = dt_Print.NewRow();
//            DR["VoucherID"] = "0";
//            DR["VoucherType"] = "رصيد سابق";
//            DR["VoucherDate"] = DateTime.Now.ToString("yyyy/MM/dd");
//            DR["Remarks"] = "رصيد الفترة السابقة للعميل";
//            DR["Debit"] = SumDebit.ToString();
//            DR["Credit"] = SumCredit.ToString();
//            dt_Print.Rows.Add(DR);
//            x += Convert.ToDecimal(SumDebit.ToString());
//            y += Convert.ToDecimal(SumCredit.ToString());



//            for (int i = 0; i < Dt2.Rows.Count; i++)
//            {
//                DR = dt_Print.NewRow();
//                //DR["VoucherType"] = Dt2.Rows[i]["VoucherType"];
//                DR["VoucherID"] = Dt2.Rows[i]["VoucherID"];
//                DR["VoucherType"] = Dt2.Rows[i]["VoucherType"];
//                DR["VoucherDate"] = Dt2.Rows[i]["VoucherDate"];
//                DR["Remarks"] = Dt2.Rows[i]["Remarks"];
//                DR["Debit"] = Dt2.Rows[i]["Debit"];
//                DR["Credit"] = Dt2.Rows[i]["Credit"];
//                DR["EmpName"] = Dt2.Rows[i]["EmpName"];
//                dt_Print.Rows.Add(DR);
//                x += Convert.ToDecimal(Dt2.Rows[i]["Debit"]);
//                y += Convert.ToDecimal(Dt2.Rows[i]["Credit"]);
//            }


            //txt_1.Text = x.ToString();
            //txt_2.Text = y.ToString();
            //Decimal D = x - y;
            //txt_3.Text = D.ToString();
            //DGV1.AutoGenerateColumns = false;
            //DGV1.DataSource = dt_Print;
        }


        public void SuppliersTrans(string SuppliersID)
        {

            //Decimal x = 0;
            //Decimal y = 0;
            RbtTreasuryMovement_cls cls = new RbtTreasuryMovement_cls();

            DataTable Dt2 = cls.TreasuryMovement(datefrom.Value, datetto.Value, SuppliersID, "Supp");
            //===========================================================================================
            DataTable Dt = cls.TreasuryMovement2(datefrom.Value, SuppliersID, "Supp");

            if (Dt.Rows[0][0] == DBNull.Value == false)
                SumDebit = Convert.ToDecimal(Dt.Rows[0][0]);

            if (Dt.Rows[0][1] == DBNull.Value == false)
                SumCredit = Convert.ToDecimal(Dt.Rows[0][1]);

            //====================================================================================

//            SqlDataAdapter da_Print = new SqlDataAdapter(@"SELECT        dbo.TreasuryMovement.*, dbo.UserPermissions.EmpName  FROM  dbo.TreasuryMovement INNER JOIN
//                         dbo.UserPermissions ON dbo.TreasuryMovement.UserAdd = dbo.UserPermissions.ID where VoucherID ='0' ORDER BY VoucherDate", DataAccessLayer.connSQLServer);
//            dt_Print = new DataTable();

//            da_Print.Fill(dt_Print);
//            da_Print.Dispose();
//            dt_Print.Clear();
            DataRow DR = dt_Print.NewRow();
            DR["VoucherID"] = "0";
            DR["VoucherType"] = "رصيد سابق";
            DR["VoucherDate"] = DateTime.Now.ToString("yyyy/MM/dd");
            DR["Remark"] = "رصيد الفترة السابقة للمورد";
            DR["Debit"] = SumDebit.ToString();
            DR["Credit"] = SumCredit.ToString();
            dt_Print.Rows.Add(DR);
            x += Convert.ToDecimal(SumDebit.ToString());
            y += Convert.ToDecimal(SumCredit.ToString());



            for (int i = 0; i < Dt2.Rows.Count; i++)
            {
                DR = dt_Print.NewRow();
                //DR["VoucherType"] = Dt2.Rows[i]["VoucherType"];
                DR["VoucherID"] = Dt2.Rows[i]["VoucherID"];
                DR["VoucherType"] = Dt2.Rows[i]["VoucherType"];
                DR["VoucherDate"] = Dt2.Rows[i]["VoucherDate"];
                DR["Remark"] = Dt2.Rows[i]["Remark"];
                DR["Debit"] = Dt2.Rows[i]["Debit"];
                DR["Credit"] = Dt2.Rows[i]["Credit"];
                DR["EmpName"] = Dt2.Rows[i]["EmpName"];
                dt_Print.Rows.Add(DR);
                x += Convert.ToDecimal(Dt2.Rows[i]["Debit"]);
                y += Convert.ToDecimal(Dt2.Rows[i]["Credit"]);
            }


            //txt_1.Text = x.ToString();
            //txt_2.Text = y.ToString();
            //Decimal D = x - y;
            //txt_3.Text = D.ToString();
            //DGV1.AutoGenerateColumns = false;
            //DGV1.DataSource = dt_Print;



        }


        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            
            
            if (gridView1.RowCount == 0)
                btnNew_Click(null, null);
            else 
            RPT.rpt_GridReport.Print(gridControl1, this.Text+" : "  + txtAccountName.Text ,
           
             " " +  stlb1.Text +" " +txt_1.Text +
             " " +  stlb2.Text +" " +txt_2.Text +
             " " +  stlb3.Text +" " +txt_3.Text 


                , true);

                
           





        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                CustomersSearch_frm frm = new CustomersSearch_frm();

                frm.ShowDialog(this);
                if (frm.loaddata == true)
                {
                    txtAccountID.Text = frm.DGV1.CurrentRow.Cells["CustomerID"].Value.ToString();
                    txtAccountName.Text = frm.DGV1.CurrentRow.Cells["CustomerName"].Value.ToString();
                    txtAccountName.BackColor = Color.WhiteSmoke; 
                    txt_1.Text = "0";
                    txt_2.Text = "0";
                    txt_3.Text = "0";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        List<GridColumn > IntColumns()
        {
            var VoucherID = new GridColumn();
            var VoucherType = new GridColumn();
            var VoucherDate = new GridColumn();
            var Remark = new GridColumn();
            var Debit = new GridColumn();
            var Credit = new GridColumn();
            var EmpName = new GridColumn();

             VoucherID.FieldName  = "VoucherID";
             VoucherID.Caption  = "رقم السند";
             VoucherID.Name = "VoucherID";  
             VoucherID.Width = 150;
           
             VoucherType.FieldName = "VoucherType";
             VoucherType.Caption = "نوع الحركة";
             VoucherType.Name = "VoucherType"; 
             VoucherType.Width = 150;
          
             VoucherDate.FieldName = "VoucherDate"; 
             VoucherDate.Caption = "تاريخ السند";
             VoucherDate.Name = "VoucherDate"; 
             VoucherDate.Width = 120;
            // 
            // Remark
            // 
             Remark.FieldName = "Remark"; 
             Remark.Caption = "البيــــــــــان";
             Remark.Name = "Remark"; 
             Remark.Width = 250;
            // 
            // Debit
            // 
             Debit.FieldName = "Debit"; 
             Debit.Caption = "علية / مدين";
             Debit.Name = "Debit"; 
             Debit.Width = 130;
            // 
            // Credit
            // 
             Credit.FieldName = "Credit"; 
             Credit.Caption = "له / دائن";
             Credit.Name = "Credit"; 
             Credit.Width = 130;
            // 
            // EmpName
            // 
             EmpName.FieldName = "EmpName";
            //dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            // EmpName.DefaultCellStyle = dataGridViewCellStyle9;
             EmpName.Caption = "المستخدم";
             EmpName.Name = "EmpName"; 
             EmpName.Width = 150;

          
            return new List<GridColumn>() { VoucherID, VoucherType, VoucherDate, Remark , Debit , Credit , EmpName };
        }


    }
}
