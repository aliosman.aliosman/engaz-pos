﻿namespace ByStro.PL
{
    partial class MoneyTransfer_frm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MoneyTransfer_frm));
            this.D1 = new System.Windows.Forms.DateTimePicker();
            this.Label1 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.txtTransferID = new System.Windows.Forms.TextBox();
            this.txtTypeID = new System.Windows.Forms.TextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnNew = new System.Windows.Forms.ToolStripButton();
            this.btnSave = new System.Windows.Forms.ToolStripButton();
            this.btnUpdate = new System.Windows.Forms.ToolStripButton();
            this.btnDelete = new System.Windows.Forms.ToolStripButton();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.combPayTypeto = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtCurrencyRate = new System.Windows.Forms.TextBox();
            this.combCurrency = new System.Windows.Forms.ComboBox();
            this.combPayTypeFrom = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // D1
            // 
            this.D1.CustomFormat = "";
            this.D1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.D1.Location = new System.Drawing.Point(20, 61);
            this.D1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.D1.Name = "D1";
            this.D1.Size = new System.Drawing.Size(293, 24);
            this.D1.TabIndex = 212;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.ForeColor = System.Drawing.Color.Black;
            this.Label1.Location = new System.Drawing.Point(20, 37);
            this.Label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(97, 17);
            this.Label1.TabIndex = 208;
            this.Label1.Text = "تاريخ الاستلام :";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.ForeColor = System.Drawing.Color.Black;
            this.Label2.Location = new System.Drawing.Point(20, 90);
            this.Label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(84, 17);
            this.Label2.TabIndex = 206;
            this.Label2.Text = "رقم التحويل :";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.ForeColor = System.Drawing.Color.Black;
            this.Label6.Location = new System.Drawing.Point(20, 143);
            this.Label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(78, 17);
            this.Label6.TabIndex = 205;
            this.Label6.Text = "من حساب :";
            // 
            // txtTransferID
            // 
            this.txtTransferID.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txtTransferID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTransferID.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTransferID.ForeColor = System.Drawing.Color.Black;
            this.txtTransferID.Location = new System.Drawing.Point(20, 114);
            this.txtTransferID.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtTransferID.Name = "txtTransferID";
            this.txtTransferID.ReadOnly = true;
            this.txtTransferID.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTransferID.Size = new System.Drawing.Size(307, 24);
            this.txtTransferID.TabIndex = 422;
            this.txtTransferID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTypeID
            // 
            this.txtTypeID.Location = new System.Drawing.Point(661, 61);
            this.txtTypeID.Name = "txtTypeID";
            this.txtTypeID.ReadOnly = true;
            this.txtTypeID.Size = new System.Drawing.Size(94, 24);
            this.txtTypeID.TabIndex = 424;
            this.txtTypeID.Text = "MoneyTransfer";
            this.txtTypeID.Visible = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.toolStrip1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNew,
            this.btnSave,
            this.btnUpdate,
            this.btnDelete});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(781, 27);
            this.toolStrip1.TabIndex = 783;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnNew
            // 
            this.btnNew.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNew.Image = global::ByStro.Properties.Resources.New;
            this.btnNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(68, 24);
            this.btnNew.Text = "جديد ";
            this.btnNew.ToolTipText = "جديد Ctrl+N";
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click_1);
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = global::ByStro.Properties.Resources.Save_and_New;
            this.btnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(84, 24);
            this.btnSave.Text = "حفظ F2";
            this.btnSave.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Image = global::ByStro.Properties.Resources.edit;
            this.btnUpdate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(94, 24);
            this.btnUpdate.Text = "تعديل F3 ";
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click_1);
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = global::ByStro.Properties.Resources.Pic030;
            this.btnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(69, 24);
            this.btnDelete.Text = "حذف ";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click_1);
            // 
            // txtAmount
            // 
            this.txtAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAmount.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold);
            this.txtAmount.ForeColor = System.Drawing.Color.Black;
            this.txtAmount.Location = new System.Drawing.Point(20, 272);
            this.txtAmount.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtAmount.Size = new System.Drawing.Size(307, 24);
            this.txtAmount.TabIndex = 780;
            this.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAmount.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.txtAmount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPayValue_KeyDown);
            this.txtAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // combPayTypeto
            // 
            this.combPayTypeto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combPayTypeto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combPayTypeto.FormattingEnabled = true;
            this.combPayTypeto.Location = new System.Drawing.Point(20, 220);
            this.combPayTypeto.Name = "combPayTypeto";
            this.combPayTypeto.Size = new System.Drawing.Size(307, 24);
            this.combPayTypeto.TabIndex = 779;
            this.combPayTypeto.DropDown += new System.EventHandler(this.combPayType_DropDown);
            this.combPayTypeto.SelectedIndexChanged += new System.EventHandler(this.combPayTypeto_SelectedIndexChanged);
            this.combPayTypeto.TextChanged += new System.EventHandler(this.combPayType_TextChanged);
            this.combPayTypeto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.combPayType_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(19, 301);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 17);
            this.label3.TabIndex = 785;
            this.label3.Text = "البيان :";
            // 
            // txtNote
            // 
            this.txtNote.BackColor = System.Drawing.Color.White;
            this.txtNote.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNote.Location = new System.Drawing.Point(19, 326);
            this.txtNote.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtNote.MaxLength = 150;
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(750, 54);
            this.txtNote.TabIndex = 786;
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(525, 258);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(84, 17);
            this.label21.TabIndex = 799;
            this.label21.Text = "سعر الصرف :";
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(525, 203);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 17);
            this.label17.TabIndex = 798;
            this.label17.Text = "العملة :";
            // 
            // txtCurrencyRate
            // 
            this.txtCurrencyRate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCurrencyRate.BackColor = System.Drawing.Color.White;
            this.txtCurrencyRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrencyRate.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrencyRate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.txtCurrencyRate.Location = new System.Drawing.Point(528, 280);
            this.txtCurrencyRate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCurrencyRate.Name = "txtCurrencyRate";
            this.txtCurrencyRate.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCurrencyRate.Size = new System.Drawing.Size(241, 24);
            this.txtCurrencyRate.TabIndex = 797;
            this.txtCurrencyRate.Text = "1";
            this.txtCurrencyRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCurrencyRate.TextChanged += new System.EventHandler(this.txtCurrencyRate_TextChanged);
            this.txtCurrencyRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCurrencyRate_KeyPress);
            // 
            // combCurrency
            // 
            this.combCurrency.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.combCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combCurrency.FormattingEnabled = true;
            this.combCurrency.Location = new System.Drawing.Point(525, 225);
            this.combCurrency.Name = "combCurrency";
            this.combCurrency.Size = new System.Drawing.Size(244, 24);
            this.combCurrency.TabIndex = 803;
            this.combCurrency.SelectedIndexChanged += new System.EventHandler(this.combCurrenvy_SelectedIndexChanged);
            // 
            // combPayTypeFrom
            // 
            this.combPayTypeFrom.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combPayTypeFrom.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combPayTypeFrom.FormattingEnabled = true;
            this.combPayTypeFrom.Location = new System.Drawing.Point(20, 168);
            this.combPayTypeFrom.Name = "combPayTypeFrom";
            this.combPayTypeFrom.Size = new System.Drawing.Size(307, 24);
            this.combPayTypeFrom.TabIndex = 805;
            this.combPayTypeFrom.SelectedIndexChanged += new System.EventHandler(this.combPayTypeFrom_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(20, 198);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 17);
            this.label4.TabIndex = 806;
            this.label4.Text = "الي حساب :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(20, 249);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 17);
            this.label5.TabIndex = 807;
            this.label5.Text = "المبلغ :";
            // 
            // btnSearch
            // 
            this.btnSearch.BackgroundImage = global::ByStro.Properties.Resources.Search_20;
            this.btnSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSearch.Location = new System.Drawing.Point(332, 111);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(36, 29);
            this.btnSearch.TabIndex = 784;
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // MoneyTransfer_frm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(781, 412);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.combPayTypeFrom);
            this.Controls.Add(this.combCurrency);
            this.Controls.Add(this.combPayTypeto);
            this.Controls.Add(this.txtAmount);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtCurrencyRate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtNote);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.txtTypeID);
            this.Controls.Add(this.Label6);
            this.Controls.Add(this.D1);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.txtTransferID);
            this.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "MoneyTransfer_frm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "سند تحويل اموال";
            this.Load += new System.EventHandler(this.CustomersPayment_FormAdd_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CustomersRecived_frm_KeyDown);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.DateTimePicker D1;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.TextBox txtTransferID;
        private System.Windows.Forms.TextBox txtTypeID;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnNew;
        private System.Windows.Forms.ToolStripButton btnSave;
        private System.Windows.Forms.ToolStripButton btnUpdate;
        private System.Windows.Forms.ToolStripButton btnDelete;
        private System.Windows.Forms.Button btnSearch;
        public System.Windows.Forms.TextBox txtAmount;
        private System.Windows.Forms.ComboBox combPayTypeto;
        internal System.Windows.Forms.Label label3;
        internal System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.TextBox txtCurrencyRate;
        private System.Windows.Forms.ComboBox combCurrency;
        private System.Windows.Forms.ComboBox combPayTypeFrom;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.Label label5;
    }
}