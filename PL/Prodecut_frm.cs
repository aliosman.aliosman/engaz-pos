﻿
using ByStro.Clases;
using ByStro.DAL;
using ByStro.Forms;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using static ByStro.Forms.frm_PrintItemBarcode;
using DevExpress.Data;
using DevExpress.LookAndFeel; 
using DevExpress.XtraGrid;
namespace ByStro.PL
{
    public partial class Prodecut_frm : Form
    {
        public Prodecut_frm()
        {
            InitializeComponent();
        }
        Prodecut_cls cls = new Prodecut_cls();
        public Boolean LoadData = false;
        public void ITEM_FRM_Load(object sender, EventArgs e)
        {
            
            ERB_Setting.SettingForm(this);
           // ERB_Setting.SettingDGV(DGV1);
            if (AddEdit == "Add")
            {
                btnNew_Click(null, null);
            }
         

        }




        public string AddEdit = "Add";

        private void btnSave_Click(object sender, EventArgs e)
        {
          

                if (txtProdecutName.Text.Trim() == "")
                {
                    txtProdecutName.BackColor = Color.Pink;
                    txtProdecutName.Focus();
                    return;
                }
                if (txtCategoryID.Text.Trim() == "")
                {
                    combGroup.BackColor = Color.Pink;
                    tabControl1.SelectedIndex = 0;
                    combGroup.Focus();
                    return;
                }

                DataTable DtSearch = cls.NameSearch_Prodecuts(txtProdecutName.Text.Trim());
                if (DtSearch.Rows.Count > 0)
                {
                    if (MessageBox.Show("الاسم المدخل موجود مسبقا هل تريد اضافتة مرة اخري", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        return;
                    }
                    //txtProdecutName.Focus();

                }
                if (combFiestUnit.Text.Trim() == "")
                {
                    MessageBox.Show("يرجي ادخال الوحدة الاولي " + Environment.NewLine + "الوحدة الاولي من الوحدات الاساسية يجب ادخالها", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    tabControl1.SelectedIndex = 1;
                    combFiestUnit.BackColor = Color.Pink;
                    //tabControl1.SelectTab(1);
                    return;
                }


                // SecoundUnit =====================================================================================================================================================
                else if (combSecoundUnit.Text.Trim() != "")
                {
                    if (txtSecoundUnitOperating.Text.Trim() == "")
                    {
                        MessageBox.Show("يرجي ادخال  معدل التحويل للوحدة الثانية حيث تم ادخال اسم الوحدة   ولم يتم ادخال  معدل التحويل", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        tabControl1.SelectedIndex = 1;
                        txtSecoundUnitOperating.BackColor = Color.Pink;
                        txtSecoundUnitOperating.Focus();
                        return;
                    }
                }

                else if (txtSecoundUnitOperating.Text.Trim() != "")
                {
                    if (combSecoundUnit.Text.Trim() == "")
                    {
                        MessageBox.Show("يرجي ادخال الوحدة حيث تم ادخال معدل التحويل  ولم يتم ادخال اسم الوحدة", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        tabControl1.SelectedIndex = 1;
                        combSecoundUnit.BackColor = Color.Pink;
                        combSecoundUnit.Focus();
                        return;
                    }
                }
                // Three Unit =====================================================================================================================================================
                else if (combThreeUnit.Text.Trim() != "")
                {
                    if (txtThreeUnitOperating.Text.Trim() == "")
                    {
                        MessageBox.Show("يرجي ادخال  معدل التحويل للوحدة الثانية حيث تم ادخال اسم الوحدة   ولم يتم ادخال  معدل التحويل", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        tabControl1.SelectedIndex = 1;
                        txtThreeUnitOperating.BackColor = Color.Pink;
                        txtThreeUnitOperating.Focus();
                        return;
                    }
                }

                if (txtThreeUnitOperating.Text.Trim() != "")
                {
                    if (combThreeUnit.Text.Trim() == "")
                    {
                        MessageBox.Show("يرجي ادخال الوحدة حيث تم ادخال معدل التحويل  ولم يتم ادخال اسم الوحدة", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        tabControl1.SelectedIndex = 1;
                        combThreeUnit.BackColor = Color.Pink;
                        combThreeUnit.Focus();
                        return;
                    }
                }
                //========================================================================================================================================================
                else if (txtThreeUnitOperating.Text.Trim() != "")
                {
                    if (txtSecoundUnitOperating.Text.Trim() == "")
                    {
                        MessageBox.Show("يرجي ادخال الوحدة الثانية قبل الوحدة الثالثة " + Environment.NewLine + " وفي حالة الصنف يحتوي علي وحدتين يرجي ادخال الوحدة الولي والثانية", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        tabControl1.SelectedIndex = 1;
                        txtSecoundUnitOperating.BackColor = Color.Pink;
                        txtSecoundUnitOperating.Focus();
                        return;
                    }
                }


                //==============================================================================================================================
                else if (txtProdecutBayPrice.Text.Trim() == "")
                {
                    txtProdecutBayPrice.BackColor = Color.Pink;
                    tabControl1.SelectedIndex = 1;
                    txtProdecutBayPrice.Focus();
                    return;
                }

                // barcode
                if (txtFiestUnitBarcode.Text.Trim() != "")
                {
                    DataTable dtBarcode1 = cls.NameSearch__ProdecutsBarcode(txtFiestUnitBarcode.Text);
                    if (dtBarcode1.Rows.Count > 0)
                    {
                        tabControl1.SelectedIndex = 1;
                        txtFiestUnitBarcode.BackColor = Color.Pink;
                        txtFiestUnitBarcode.Focus();
                        return;
                    }
                }

                if (txtSecoundUnitBarcode.Text.Trim() != "")
                {
                    DataTable dtBarcode2 = cls.NameSearch__ProdecutsBarcode(txtSecoundUnitBarcode.Text);
                    if (dtBarcode2.Rows.Count > 0)
                    {
                        tabControl1.SelectedIndex = 1;
                        txtSecoundUnitBarcode.BackColor = Color.Pink;
                        txtSecoundUnitBarcode.Focus();
                        return;
                    }
                }

                if (txtThreeUnitBarcode.Text.Trim() != "")
                {
                    DataTable dtBarcode3 = cls.NameSearch__ProdecutsBarcode(txtThreeUnitBarcode.Text);
                    if (dtBarcode3.Rows.Count > 0)
                    {
                        tabControl1.SelectedIndex = 1;
                        txtThreeUnitBarcode.BackColor = Color.Pink;
                        txtThreeUnitBarcode.Focus();
                        return;
                    }
                }




                // Unit Defalut
                int UnitDefoult = 1;
                if (RbUnitDefoult1.Checked == true)
                {
                    UnitDefoult = 1;
                }
                else if (RbUnitDefoult2.Checked == true)
                {
                    UnitDefoult = 2;
                }
                else if (RbUnitDefoult3.Checked == true)
                {
                    UnitDefoult = 3;
                }

                // Price ======================================================================================================================================================================
                Double FiestUnitPrice1 = 0; Double FiestUnitPrice2 = 0; Double FiestUnitPrice3 = 0;
                Double SecoundUnitPrice1 = 0; Double SecoundUnitPrice2 = 0; Double SecoundUnitPrice3 = 0;
                Double ThreeUnitPrice1 = 0; Double ThreeUnitPrice2 = 0; Double ThreeUnitPrice3 = 0;
                // Fiest Unit Price
                if (txtFiestUnitPrice1.Text.Trim() != "")
                {
                    FiestUnitPrice1 = Convert.ToDouble(txtFiestUnitPrice1.Text);
                }
                if (txtFiestUnitPrice2.Text.Trim() != "")
                {
                    FiestUnitPrice2 = Convert.ToDouble(txtFiestUnitPrice2.Text);
                }
                if (txtFiestUnitPrice3.Text.Trim() != "")
                {
                    FiestUnitPrice3 = Convert.ToDouble(txtFiestUnitPrice3.Text);
                }
                // Secound Unit Price
                if (txtSecoundUnitPrice1.Text.Trim() != "")
                {
                    SecoundUnitPrice1 = Convert.ToDouble(txtSecoundUnitPrice1.Text);
                }
                if (txtSecoundUnitPrice2.Text.Trim() != "")
                {
                    SecoundUnitPrice2 = Convert.ToDouble(txtSecoundUnitPrice2.Text);
                }
                if (txtSecoundUnitPrice3.Text.Trim() != "")
                {
                    SecoundUnitPrice3 = Convert.ToDouble(txtSecoundUnitPrice3.Text);
                }
                //  Three Unit Price
                if (txtThreeUnitPrice1.Text.Trim() != "")
                {
                    ThreeUnitPrice1 = Convert.ToDouble(txtThreeUnitPrice1.Text);
                }
                if (txtThreeUnitPrice2.Text.Trim() != "")
                {
                    ThreeUnitPrice2 = Convert.ToDouble(txtThreeUnitPrice2.Text);
                }
                if (txtThreeUnitPrice3.Text.Trim() != "")
                {
                    ThreeUnitPrice3 = Convert.ToDouble(txtThreeUnitPrice3.Text);
                }


                //// Bay Price 
                Double ItemBayPrice = 0;
                if (txtProdecutBayPrice3.Text.Trim() != "")
                {
                    ItemBayPrice = Convert.ToDouble(txtProdecutBayPrice3.Text);
                }
                else if (txtProdecutBayPrice2.Text.Trim() != "")
                {
                    ItemBayPrice = Convert.ToDouble(txtProdecutBayPrice2.Text);
                }
                else if (txtProdecutBayPrice.Text.Trim() != "")
                {
                    ItemBayPrice = Convert.ToDouble(txtProdecutBayPrice.Text);
                }

                #region "التحويل بين الوحدات"
                String FiestUnitFactor = ""; String SecoundUnitFactor = ""; String ThreeUnitFactor = "";
                if (txtThreeUnitOperating.Text.Trim() != "")
                {
                    if (Convert.ToDouble(txtThreeUnitOperating.Text) != 0)
                    {
                        Double y = Convert.ToDouble(txtSecoundUnitOperating.Text) * Convert.ToDouble(txtThreeUnitOperating.Text);
                        FiestUnitFactor = y.ToString();
                        SecoundUnitFactor = txtThreeUnitOperating.Text;
                        ThreeUnitFactor = "1";
                    }

                }
                else if (txtSecoundUnitOperating.Text.Trim() != "")
                {
                    if (Convert.ToDouble(txtSecoundUnitOperating.Text) != 0)
                    {
                        FiestUnitFactor = txtSecoundUnitOperating.Text;
                        SecoundUnitFactor = "1";
                    }

                }
                else
                {
                    FiestUnitFactor = "1";
                }

                //MessageBox.Show("First", FiestUnitFactor);
                //MessageBox.Show("Secound", SecoundUnitFactor);
                //MessageBox.Show("Three", ThreeUnitFactor);
                #endregion


                txtProdecutID.Text = cls.MaxID_Prodecuts();
                cls.InsertProdecuts(txtProdecutID.Text, txtProdecutName.Text.Trim(), txtCategoryID.Text, txtProdecutLocation.Text,
                    txtCompany.Text, DateTime.Now, Convert.ToDouble(txtRequestLimit.Text), combFiestUnit.Text,
                    txtFiestUnitOperating.Text, FiestUnitFactor, txtFiestUnitBarcode.Text, FiestUnitPrice1, FiestUnitPrice2, FiestUnitPrice3
                    , combSecoundUnit.Text, txtSecoundUnitOperating.Text, SecoundUnitFactor, txtSecoundUnitBarcode.Text, SecoundUnitPrice1, 
                    SecoundUnitPrice2, SecoundUnitPrice3, combThreeUnit.Text, txtThreeUnitOperating.Text, ThreeUnitFactor, txtThreeUnitBarcode.Text,
                    ThreeUnitPrice1, ThreeUnitPrice2, ThreeUnitPrice3, ItemBayPrice, Convert.ToDouble(txtDiscoundBay.Text),
                    Convert.ToDouble(txtDiscoundSale.Text), UnitDefoult, chStatus.Checked, chProductService.Checked,chk_HasColor.Checked,chk_HasSize.Checked ,chk_HasSerial.Checked ,chk_HasExpire .Checked );
                Save();
                if (XtraMessageBox.Show (text:"هل تريد طباعه باركود الصنف ؟ ","",MessageBoxButtons.YesNo )== DialogResult.Yes)
                {
                    new frm_PrintItemBarcode(new BindingList<ProductBarcode>() { new ProductBarcode() {

                      Count = 1,
                      ID = txtProdecutID.Text.ToInt(),
                      Price =Convert.ToDouble( txtFiestUnitPrice1.Text ),
                      UnitBarcode = txtFiestUnitBarcode.Text ,
                      Name = txtProdecutName.Text ,
                      UnitName = combFiestUnit.Text ,

                    } }).Show();
                }


            if (spinEdit1.EditValue is decimal d && d > 0)
            {
                var list = new List<Inv_InvoiceDetail>()
                    {
                        new Inv_InvoiceDetail()
                        {
                            ItemID =Convert.ToInt32(txtProdecutID .Text ),
                            ItemQty = Convert.ToDouble(spinEdit1.EditValue ),
                        }
                    };
                var frm = new frm_Inv_Invoice(MasterClass.InvoiceType.ItemOpenBalance, list);
                frm.Show();
                frm.btn_Save.PerformClick();
                frm.Close();

            }
            spinEdit1.EditValue = 0;

            LoadData = true;
                //Mass.Saved();
                btnNew_Click(null, null);
       


        }
        void  Save()
        {
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            var product = db.Prodecuts.Single(x => x.ProdecutID == Convert.ToInt32(txtProdecutID.Text));
            product.HasColor = chk_HasColor.Checked;
            product.HasSize = chk_HasSize.Checked;
            product.HasExpire   = chk_HasExpire .Checked;
            product.HasSerial = chk_HasSerial.Checked;
            db.SubmitChanges();
        }

        //void SaveItemUnits()
        //{
        //    #region "التحويل بين الوحدات"
        //   /Oint i = 1; i < DGV1.Rows.Count; i++)
        //    {
        //        if (i + 1 == DGV1.Rows.Count)
        //        {
        //            eee = 1;
        //        }
        //        else
        //        {
        //            eee = Convert.ToDouble(DGV1.Rows[i + 1].Cells["UnitFactor"].Value);

        //        }

        //        ItemsUnits_CLS.InsertItemsUnits(int.Parse(ItemsUnits_CLS.MaxIDItemsUnits()), int.Parse(txtID.Text), int.Parse(DGV1.Rows[i].Cells["UnitID"].Value.ToString()), DGV1.Rows[i].Cells["UnitBarcode"].Value.ToString(), Convert.ToDouble(DGV1.Rows[i].Cells["UnitFactor"].Value), Convert.ToDouble(DGV1.Rows[i].Cells["UnitBayPrice"].Value), Convert.ToDouble(DGV1.Rows[i].Cells["UnitPrice1"].Value), Convert.ToDouble(DGV1.Rows[i].Cells["UnitPrice2"].Value), Convert.ToDouble(DGV1.Rows[i].Cells["UnitPrice2"].Value), 0, 0, eee, 0);

        //        //MessageBox.Show(eee.ToString());
        //    }

        // #endregion


        //}



        public void LoadCatogory()
        {

            // FILL Combo Group item
            Category_cls Category_CLS = new Category_cls();
            combGroup.DataSource = Category_CLS.Search_Category("");
            combGroup.DisplayMember = "CategoryName";
            combGroup.ValueMember = "CategoryID";

            //==============================================================================================
            Unit_cls Unit_cls = new Unit_cls();
            // FILL Combo unit1 item
            combFiestUnit.DataSource = Unit_cls.Search_Unit("");
            combFiestUnit.DisplayMember = "UnitName";
            combFiestUnit.ValueMember = "UnitID";
            combFiestUnit.Text = null;
            //==============================================================================

            // FILL Combo unit1 item
            combSecoundUnit.DataSource = Unit_cls.Search_Unit("");
            combSecoundUnit.DisplayMember = "UnitName";
            combSecoundUnit.ValueMember = "UnitID";
            combSecoundUnit.Text = null;
            //==============================================================================

            // FILL Combo unit1 item
            combThreeUnit.DataSource = Unit_cls.Search_Unit("");
            combThreeUnit.DisplayMember = "UnitName";
            combThreeUnit.ValueMember = "UnitID";
            combThreeUnit.Text = null;
            //==============================================================================
        }


        private void btnNew_Click(object sender, EventArgs e)
        {


            try
            {
                LoadCatogory();



                txtProdecutID.Text = cls.MaxID_Prodecuts();
                txtProdecutName.Text = "";
                combGroup.Text = null;
                txtCategoryID.Text = "";
                txtProdecutLocation.Text = "";
                txtCompany.Text = "";

                //===========
                txtProdecutName.Text = "";
                txtProdecutBayPrice.Text = "0";
                txtRequestLimit.Text = "0";
                txtFiestUnitOperating.Text = "1";
                txtFiestUnitBarcode.Text = "";
                txtFiestUnitPrice1.Text = "";
                txtFiestUnitPrice2.Text = "";
                txtFiestUnitPrice3.Text = "";
                txtSecoundUnitOperating.Text = "";
                txtSecoundUnitBarcode.Text = "";
                txtSecoundUnitPrice1.Text = "";
                txtSecoundUnitPrice2.Text = "";
                txtSecoundUnitPrice3.Text = "";
                txtThreeUnitOperating.Text = "";
                txtThreeUnitBarcode.Text = "";
                txtThreeUnitPrice1.Text = "";
                txtThreeUnitPrice2.Text = "";
                txtThreeUnitPrice3.Text = "";
                //txtProdecutAvg.Text = "";
                //txtProdecutAvg2.Text = "";
                //txtProdecutAvg3.Text = "";
                txtDiscoundBay.Text = "0";
                txtDiscoundSale.Text = "0";
                txtSecoundUnitOperating.ReadOnly = false;
                txtThreeUnitOperating.ReadOnly = false;

                //   chStatus.Checked = true;
               
                if (BillSetting_cls.UseCategory == true)
                {
                    Category_cls Category_cls = new Category_cls();
                    DataTable dt_category = Category_cls.Details_Category(BillSetting_cls.CategoryId);
                    txtCategoryID.Text = dt_category.Rows[0]["CategoryID"].ToString();
                    combGroup.Text = dt_category.Rows[0]["CategoryName"].ToString();
                }
                else
                {
                    txtCategoryID.Text = "";
                    combGroup.Text = "";
                }

                if (BillSetting_cls.UseUnit == true)
                {
                    Unit_cls Unit_cls = new Unit_cls();
                    DataTable dt_unit = Unit_cls.Details_Unit(int.Parse( BillSetting_cls.UnitId));
                    //TXT.Text = dt_unit.Rows[0]["UnitID"].ToString();
                    combFiestUnit.Text = dt_unit.Rows[0]["UnitName"].ToString();
                }
                else
                {
                    combFiestUnit.Text = "";
                    
                }


                //=============================================================
                txtProdecutName.Focus();
                btnSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
                tabControl1.SelectedIndex = 0;
                chStatus.Checked = true;
                chProductService.Checked = false;
                txtProdecutName.Focus();


                #region "load"
                if (Application.OpenForms["BillSales_frm"] != null)
                {
                   // ((BillSales_frm)Application.OpenForms["BillSales_frm"]).FillProdecut();

                }
                else if (Application.OpenForms["BillBay_frm"] != null)
                {
                   // ((BillBay_frm)Application.OpenForms["BillBay_frm"]).FillProdecut();

                }
                else if (Application.OpenForms["BillFirestItem_frm"] != null)
                {
                    ((BillFirestItem_frm)Application.OpenForms["BillFirestItem_frm"]).FillProdecut();

                }
                else if (Application.OpenForms["BillChangeStore_frm"] != null)
                {
                    ((BillChangeStore_frm)Application.OpenForms["BillChangeStore_frm"]).FillProdecut();

                }
                else if (Application.OpenForms["BillChangeQuantity"] != null)
                {
                    ((BillChangeQuantity)Application.OpenForms["BillChangeQuantity"]).FillProdecut();

                }
                #endregion


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtProdecutName.Text.Trim() == "")
                {
                    txtProdecutName.BackColor = Color.Pink;
                    txtProdecutName.Focus();
                    return;
                }
                else if (txtCategoryID.Text.Trim() == "")
                {
                    combGroup.BackColor = Color.Pink;
                    tabControl1.SelectedIndex = 0;
                    combGroup.Focus();
                    return;
                }
                else if (combFiestUnit.Text.Trim() == "")
                {
                    MessageBox.Show("يرجي ادخال الوحدة الاولي " + Environment.NewLine + "الوحدة الاولي من الوحدات الاساسية يجب ادخالها", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    tabControl1.SelectedIndex = 1;
                    combFiestUnit.BackColor = Color.Pink;
                    //tabControl1.SelectTab(1);
                    return;
                }


                // SecoundUnit =====================================================================================================================================================
                else if (combSecoundUnit.Text.Trim() != "")
                {
                    if (txtSecoundUnitOperating.Text.Trim() == "")
                    {
                        MessageBox.Show("يرجي ادخال  معدل التحويل للوحدة الثانية حيث تم ادخال اسم الوحدة   ولم يتم ادخال  معدل التحويل", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        tabControl1.SelectedIndex = 1;
                        txtSecoundUnitOperating.BackColor = Color.Pink;
                        txtSecoundUnitOperating.Focus();
                        return;
                    }
                }

                else if (txtSecoundUnitOperating.Text.Trim() != "")
                {
                    if (combSecoundUnit.Text.Trim() == "")
                    {
                        MessageBox.Show("يرجي ادخال الوحدة حيث تم ادخال معدل التحويل  ولم يتم ادخال اسم الوحدة", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        tabControl1.SelectedIndex = 1;
                        combSecoundUnit.BackColor = Color.Pink;
                        combSecoundUnit.Focus();
                        return;
                    }
                }
                // Three Unit =====================================================================================================================================================
                else if (combThreeUnit.Text.Trim() != "")
                {
                    if (txtThreeUnitOperating.Text.Trim() == "")
                    {
                        MessageBox.Show("يرجي ادخال  معدل التحويل للوحدة الثانية حيث تم ادخال اسم الوحدة   ولم يتم ادخال  معدل التحويل", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        tabControl1.SelectedIndex = 1;
                        txtThreeUnitOperating.BackColor = Color.Pink;
                        txtThreeUnitOperating.Focus();
                        return;
                    }
                }

                if (txtThreeUnitOperating.Text.Trim() != "")
                {
                    if (combThreeUnit.Text.Trim() == "")
                    {
                        MessageBox.Show("يرجي ادخال الوحدة حيث تم ادخال معدل التحويل  ولم يتم ادخال اسم الوحدة", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        tabControl1.SelectedIndex = 1;
                        combThreeUnit.BackColor = Color.Pink;
                        combThreeUnit.Focus();
                        return;
                    }
                }
                //========================================================================================================================================================
                else if (txtThreeUnitOperating.Text.Trim() != "")
                {
                    if (txtSecoundUnitOperating.Text.Trim() == "")
                    {
                        MessageBox.Show("يرجي ادخال الوحدة الثانية قبل الوحدة الثالثة " + Environment.NewLine + " وفي حالة الصنف يحتوي علي وحدتين يرجي ادخال الوحدة الولي والثانية", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        tabControl1.SelectedIndex = 1;
                        txtSecoundUnitOperating.BackColor = Color.Pink;
                        txtSecoundUnitOperating.Focus();
                        return;
                    }
                }
                //==============================================================================================================================
                else if (txtProdecutBayPrice.Text.Trim() == "")
                {
                    txtProdecutBayPrice.BackColor = Color.Pink;
                    tabControl1.SelectedIndex = 1;
                    txtProdecutBayPrice.Focus();
                    return;
                }



                // Unit Defalut
                int UnitDefoult = 1;
                if (RbUnitDefoult1.Checked == true)
                {
                    UnitDefoult = 1;
                }
                else if (RbUnitDefoult2.Checked == true)
                {
                    UnitDefoult = 2;
                }
                else if (RbUnitDefoult3.Checked == true)
                {
                    UnitDefoult = 3;
                }

                // Price ======================================================================================================================================================================
                Double FiestUnitPrice1 = 0; Double FiestUnitPrice2 = 0; Double FiestUnitPrice3 = 0;
                Double SecoundUnitPrice1 = 0; Double SecoundUnitPrice2 = 0; Double SecoundUnitPrice3 = 0;
                Double ThreeUnitPrice1 = 0; Double ThreeUnitPrice2 = 0; Double ThreeUnitPrice3 = 0;
                // Fiest Unit Price
                if (txtFiestUnitPrice1.Text.Trim() != "")
                {
                    FiestUnitPrice1 = Convert.ToDouble(txtFiestUnitPrice1.Text);
                }
                if (txtFiestUnitPrice2.Text.Trim() != "")
                {
                    FiestUnitPrice2 = Convert.ToDouble(txtFiestUnitPrice2.Text);
                }
                if (txtFiestUnitPrice3.Text.Trim() != "")
                {
                    FiestUnitPrice3 = Convert.ToDouble(txtFiestUnitPrice3.Text);
                }
                // Secound Unit Price
                if (txtSecoundUnitPrice1.Text.Trim() != "")
                {
                    SecoundUnitPrice1 = Convert.ToDouble(txtSecoundUnitPrice1.Text);
                }
                if (txtSecoundUnitPrice2.Text.Trim() != "")
                {
                    SecoundUnitPrice2 = Convert.ToDouble(txtSecoundUnitPrice2.Text);
                }
                if (txtSecoundUnitPrice3.Text.Trim() != "")
                {
                    SecoundUnitPrice3 = Convert.ToDouble(txtSecoundUnitPrice3.Text);
                }
                //  Three Unit Price
                if (txtThreeUnitPrice1.Text.Trim() != "")
                {
                    ThreeUnitPrice1 = Convert.ToDouble(txtThreeUnitPrice1.Text);
                }
                if (txtThreeUnitPrice2.Text.Trim() != "")
                {
                    ThreeUnitPrice2 = Convert.ToDouble(txtThreeUnitPrice2.Text);
                }
                if (txtThreeUnitPrice3.Text.Trim() != "")
                {
                    ThreeUnitPrice3 = Convert.ToDouble(txtThreeUnitPrice3.Text);
                }


                //// Bay Price 
                Double ItemBayPrice = 0;
                if (txtProdecutBayPrice3.Text.Trim() != "")
                {
                    ItemBayPrice = Convert.ToDouble(txtProdecutBayPrice3.Text);
                }
                else if (txtProdecutBayPrice2.Text.Trim() != "")
                {
                    ItemBayPrice = Convert.ToDouble(txtProdecutBayPrice2.Text);
                }
                else if (txtProdecutBayPrice.Text.Trim() != "")
                {
                    ItemBayPrice = Convert.ToDouble(txtProdecutBayPrice.Text);
                }


                #region "التحويل بين الوحدات"
                String FiestUnitFactor = ""; String SecoundUnitFactor = ""; String ThreeUnitFactor = "";
                if (txtThreeUnitOperating.Text.Trim() != "")
                {
                    if (Convert.ToDouble(txtThreeUnitOperating.Text) != 0)
                    {
                        Double y = Convert.ToDouble(txtSecoundUnitOperating.Text) * Convert.ToDouble(txtThreeUnitOperating.Text);
                        FiestUnitFactor = y.ToString();
                        SecoundUnitFactor = txtThreeUnitOperating.Text;
                        ThreeUnitFactor = "1";
                    }

                }
                else if (txtSecoundUnitOperating.Text.Trim() != "")
                {
                    if (Convert.ToDouble(txtSecoundUnitOperating.Text) != 0)
                    {
                        FiestUnitFactor = txtSecoundUnitOperating.Text;
                        SecoundUnitFactor = "1";
                    }

                }
                else
                {
                    FiestUnitFactor = "1";
                }

                //MessageBox.Show("First", FiestUnitFactor);
                //MessageBox.Show("Secound", SecoundUnitFactor);
                //MessageBox.Show("Three", ThreeUnitFactor);
                #endregion


                cls.UpdateProdecuts(txtProdecutID.Text, txtProdecutName.Text.Trim(), txtCategoryID.Text, txtProdecutLocation.Text, txtCompany.Text, DateTime.Now, Convert.ToDouble(txtRequestLimit.Text), combFiestUnit.Text, txtFiestUnitOperating.Text, FiestUnitFactor, txtFiestUnitBarcode.Text, FiestUnitPrice1, FiestUnitPrice2, FiestUnitPrice3, combSecoundUnit.Text, txtSecoundUnitOperating.Text, SecoundUnitFactor, txtSecoundUnitBarcode.Text, SecoundUnitPrice1, SecoundUnitPrice2, SecoundUnitPrice3, combThreeUnit.Text, txtThreeUnitOperating.Text, ThreeUnitFactor, txtThreeUnitBarcode.Text, ThreeUnitPrice1, ThreeUnitPrice2, ThreeUnitPrice3, ItemBayPrice, Convert.ToDouble(txtDiscoundBay.Text), Convert.ToDouble(txtDiscoundSale.Text), UnitDefoult, chStatus.Checked, chProductService.Checked);
                Save();

                //Mass.Saved();
                btnNew_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }




        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {

                DataTable dt_Store = cls.Prodecut_Store(txtProdecutID.Text);

                if (dt_Store.Rows.Count > 0)
                {

                    Mass.NoDelete();
                    return;
                }


                if (Mass.Delete() == true)
                {
                    cls.Delete_Prodecuts(int.Parse(txtProdecutID.Text));
                    btnNew_Click(null, null);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
            
                ProdecutSearch_frm frm = new ProdecutSearch_frm();
                frm.ShowDialog(this);
                if (frm.loaddata == false)
                {
                    return;
                }
                LoadProdecut(int.Parse(frm.ID_Load));
            
            
        }


        public void LoadProdecut(int id)
        {
            DataTable dt = new DataTable();
            dt = cls.Details_Prodecuts(id);
            DataRow Dr = dt.Rows[0];
            txtProdecutID.Text = Dr["ProdecutID"].ToString();
            txtProdecutName.Text = Dr["ProdecutName"].ToString();
            txtCategoryID.Text = Dr["CategoryID"].ToString();
            combGroup.Text = Dr["CategoryName"].ToString();
            txtProdecutLocation.Text = Dr["ProdecutLocation"].ToString();
            txtCompany.Text = Dr["Company"].ToString();
            // D1LastUpdate.Value = Dr["LastUpdate"].ToString();
            txtRequestLimit.Text = Dr["RequestLimit"].ToString();
            combFiestUnit.Text = Dr["FiestUnit"].ToString();
            txtFiestUnitOperating.Text = Dr["FiestUnitOperating"].ToString();
            // txtFiestUnitFactor.Text = Dr["FiestUnitFactor"].ToString();
            txtFiestUnitBarcode.Text = Dr["FiestUnitBarcode"].ToString();
            txtFiestUnitPrice1.Text = Dr["FiestUnitPrice1"].ToString();
            txtFiestUnitPrice2.Text = Dr["FiestUnitPrice2"].ToString();
            txtFiestUnitPrice3.Text = Dr["FiestUnitPrice3"].ToString();
            combSecoundUnit.Text = Dr["SecoundUnit"].ToString();
            txtSecoundUnitOperating.Text = Dr["SecoundUnitOperating"].ToString();
            //txtSecoundUnitFactor.Text = Dr["SecoundUnitFactor"].ToString();
            txtSecoundUnitBarcode.Text = Dr["SecoundUnitBarcode"].ToString();
            txtSecoundUnitPrice1.Text = Dr["SecoundUnitPrice1"].ToString();
            txtSecoundUnitPrice2.Text = Dr["SecoundUnitPrice2"].ToString();
            txtSecoundUnitPrice3.Text = Dr["SecoundUnitPrice3"].ToString();
            combThreeUnit.Text = Dr["ThreeUnit"].ToString();
            txtThreeUnitOperating.Text = Dr["ThreeUnitOperating"].ToString();
            // txtThreeUnitFactor.Text = Dr["ThreeUnitFactor"].ToString();
            txtThreeUnitBarcode.Text = Dr["ThreeUnitBarcode"].ToString();
            txtThreeUnitPrice1.Text = Dr["ThreeUnitPrice1"].ToString();
            txtThreeUnitPrice2.Text = Dr["ThreeUnitPrice2"].ToString();
            txtThreeUnitPrice3.Text = Dr["ThreeUnitPrice3"].ToString();
            txtProdecutBayPrice.Text = (Convert.ToDouble(Dr["ProdecutBayPrice"].ToString()) * Convert.ToDouble(Dr["FiestUnitFactor"].ToString())).ToString();
            //txtProdecutAvg.Text = Dr["ProdecutAvg"].ToString();
            txtDiscoundBay.Text = Dr["DiscoundBay"].ToString();
            txtDiscoundSale.Text = Dr["DiscoundSale"].ToString();

            chStatus.Checked = Convert.ToBoolean(Dr["Status"]);

            chProductService.Checked = Convert.ToBoolean(Dr["ProductService"]);
            



            if (Dr["UnitDefoult"].ToString() == "1")
            {
                RbUnitDefoult1.Checked = true;
            }
            if (Dr["UnitDefoult"].ToString() == "2")
            {
                RbUnitDefoult2.Checked = true;
            }
            if (Dr["UnitDefoult"].ToString() == "3")
            {
                RbUnitDefoult3.Checked = true;
            }



            DataTable dt_Store = cls.Prodecut_Store(txtProdecutID.Text);
            
            for (int i = 0; i < dt_Store.Rows.Count; i++)
            {
                string Change_All_Unit = ChangeUnit(dt_Store.Rows[i]["Balence"].ToString(), Dr["FiestUnit"].ToString(), Dr["FiestUnitFactor"].ToString(), Dr["SecoundUnit"].ToString(), Dr["SecoundUnitOperating"].ToString(), Dr["SecoundUnitFactor"].ToString(), Dr["ThreeUnit"].ToString(), Dr["ThreeUnitOperating"].ToString(), Dr["ThreeUnitFactor"].ToString());
                AddRowDgv(dt_Store.Rows[i]["ProdecutID"].ToString(), dt_Store.Rows[i]["StoreName"].ToString().ToString(), dt_Store.Rows[i]["Balence"].ToString(), Change_All_Unit);
            }

            if (gridView1.RowCount > 0)
            {
                txtSecoundUnitOperating.ReadOnly = true;
                txtThreeUnitOperating.ReadOnly = true;
            }





            btnSave.Enabled = false;
            btnUpdate.Enabled = true;
            btnDelete.Enabled = true;

            //=========================================================================

        }




        // "تحويل القيمة الي  وحدات كبري وصغري"
        public static   string ChangeUnit(string Quentity, string FiestUnit, string FiestUnitFactor, string SecoundUnit, string SecoundUnitOperating, string SecoundUnitFactor, string ThreeUnit, string ThreeUnitOperating, string ThreeUnitFactor)
        {
            if (Quentity == "" || Quentity == "0")
            {
                return "";
            }
            string Resalet = "";
            if (ThreeUnitOperating != "")
            {
                decimal n = Convert.ToDecimal(Quentity) / Convert.ToDecimal(ThreeUnitOperating);
                decimal H = Math.Floor(n);
                decimal b = Convert.ToDecimal(H) * Convert.ToDecimal(ThreeUnitOperating);
                decimal namber3 = Convert.ToDecimal(Quentity) - Convert.ToDecimal(b);
                decimal F = Convert.ToDecimal(H) / Convert.ToDecimal(SecoundUnitOperating);
                decimal namber1 = Math.Floor(F);
                decimal Fd = Convert.ToDecimal(namber1) * Convert.ToDecimal(SecoundUnitOperating);
                decimal namer2 = Convert.ToDecimal(H) - Convert.ToDecimal(Fd);
                Resalet = namber1 + "  " + FiestUnit + "  // " + namer2 + "  " + SecoundUnit + "  // " + namber3 + " " + ThreeUnit;
                //======================================================================================================================================================================================================================================================

            }

            else if (SecoundUnitOperating != "")
            {

                var y = Convert.ToDouble(Quentity) / Convert.ToDouble(SecoundUnitOperating);
                var Hnamber1 = Math.Floor(y);
                var dFd = Convert.ToDouble(Hnamber1) * Convert.ToDouble(SecoundUnitOperating);
                var hnamer2 = Convert.ToDouble(Quentity) - Convert.ToDouble(dFd);
                Resalet = Hnamber1 + " " + FiestUnit + "  // " + hnamer2 + " " + SecoundUnit;
                //======================================================================================================================================================================================================================================================

            }
            else if (FiestUnit != "")
            {
                Resalet = Quentity + "  " + FiestUnit;
                //======================================================================================================================================================================================================================================================
            }

            return Resalet;
        }


        private void AddRowDgv(string ProdecutID, string StoreName, string Balence, string ChangeAllUnit)
        {
            try
            {
                //DGV1.Rows.Add();
                //DGV1.ClearSelection();
                //DGV1.Rows[DGV1.Rows.Count - 1].Selected = true;
                //var lastRows = DGV1.Rows.Count - 1;
                //DGV1.Rows[lastRows].Cells["ProdecutID"].Value = ProdecutID;
                //DGV1.Rows[lastRows].Cells["StoreName"].Value = StoreName;
                //DGV1.Rows[lastRows].Cells["Balence"].Value = Balence;
                //DGV1.Rows[lastRows].Cells["Change_Unit"].Value = ChangeAllUnit;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void combGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                combGroup.BackColor = Color.White;
                txtCategoryID.Text = combGroup.SelectedValue.ToString();

            }
            catch
            {

                txtCategoryID.Text = "";

            }
        }

        private void combGroup_TextChanged(object sender, EventArgs e)
        {
            try
            {
                combGroup.BackColor = Color.White;

                if (combGroup.Text == "")
                {
                    txtCategoryID.Text = "";
                }
                else
                {
                    txtCategoryID.Text = combGroup.SelectedValue.ToString();
                }
            }
            catch
            {

                txtCategoryID.Text = "";

            }
        }












        private void AllKeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Category_frm frm = new Category_frm();
                frm.ShowDialog(this);
                if (frm.LoadData == true)
                {
                    // FILL Combo Group item
                    Category_cls Category_CLS = new Category_cls();
                    combGroup.DataSource = Category_CLS.Search_Category("");
                    combGroup.DisplayMember = "CategoryName";
                    combGroup.ValueMember = "CategoryID";
                    combGroup.Text = frm.txtGroupName.Text;
                    txtCategoryID.Text = frm.txtID.Text;
                    combGroup.Focus();


                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }





        private void txtItemName_TextChanged(object sender, EventArgs e)
        {
            txtProdecutName.BackColor = Color.White;
        }

        private void RbUnitDefoult1_CheckedChanged(object sender, EventArgs e)
        {
            if (RbUnitDefoult1.Checked == true)
            {
                RbUnitDefoult2.Checked = false;
                RbUnitDefoult3.Checked = false;
            }
        }

        private void RbUnitDefoult2_CheckedChanged(object sender, EventArgs e)
        {
            if (RbUnitDefoult2.Checked == true)
            {
                RbUnitDefoult1.Checked = false;
                RbUnitDefoult3.Checked = false;
            }
        }

        private void RbUnitDefoult3_CheckedChanged(object sender, EventArgs e)
        {
            if (RbUnitDefoult3.Checked == true)
            {
                RbUnitDefoult1.Checked = false;
                RbUnitDefoult2.Checked = false;
            }
        }

        private void Allprice(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void txtItemBayPrice1_TextChanged(object sender, EventArgs e)
        {
            UnitPrice();
            txtProdecutBayPrice.BackColor = Color.White;
        }



        private void UnitPrice()
        {
            try
            {
                //  Secound Unit Prise=======================================
                if (txtSecoundUnitOperating.Text.Trim() != "")
                {
                    if (Convert.ToDouble(txtSecoundUnitOperating.Text) != 0)
                    {
                        Double y = Convert.ToDouble(txtSecoundUnitOperating.Text);
                        Double x = Convert.ToDouble(txtProdecutBayPrice.Text);
                        txtProdecutBayPrice2.Text = (x / y).ToString();
                    }

                }
                else
                {
                    txtProdecutBayPrice2.Text = "";
                }
                //  Three Unit Prise===========================================
                if (txtThreeUnitOperating.Text.Trim() != "")
                {
                    if (Convert.ToDouble(txtThreeUnitOperating.Text) != 0)
                    {
                        Double y = Convert.ToDouble(txtThreeUnitOperating.Text);
                        Double x = Convert.ToDouble(txtProdecutBayPrice2.Text);
                        txtProdecutBayPrice3.Text = (x / y).ToString();
                    }

                }
                else
                {
                    txtProdecutBayPrice3.Text = "";
                }
            }
            catch
            {
            }
        }

        private void txtSecoundUnitOperating_TextChanged(object sender, EventArgs e)
        {
            UnitPrice();
            txtSecoundUnitOperating.BackColor = Color.White;
        }

        private void txtThreeUnitOperating_TextChanged(object sender, EventArgs e)
        {
            UnitPrice();
            txtThreeUnitOperating.BackColor = Color.White;
        }

        private void combFiestUnit_VisibleChanged(object sender, EventArgs e)
        {

        }

        private void combFiestUnit_TextChanged(object sender, EventArgs e)
        {
            combFiestUnit.BackColor = Color.White;
        }

        private void combSecoundUnit_TextChanged(object sender, EventArgs e)
        {
            combSecoundUnit.BackColor = Color.White;
        }

        private void combThreeUnit_TextChanged(object sender, EventArgs e)
        {
            combThreeUnit.BackColor = Color.White;
        }


        private void UnitFill()
        {
            try
            {
                Unit_frm frm = new Unit_frm();
                frm.ShowDialog(this);
                if (frm.LoadData == false)
                {
                    return;
                }
                string unit1 = combFiestUnit.Text;
                Unit_cls Unit_cls = new Unit_cls();
                combFiestUnit.DataSource = Unit_cls.Search_Unit("");
                combFiestUnit.DisplayMember = "UnitName";
                combFiestUnit.ValueMember = "UnitID";
                combFiestUnit.Text = unit1;
                //===========================================================================================
                string unit2 = combSecoundUnit.Text;
                combSecoundUnit.DataSource = Unit_cls.Search_Unit("");
                combSecoundUnit.DisplayMember = "UnitName";
                combSecoundUnit.ValueMember = "UnitID";
                combSecoundUnit.Text = unit2;
                //===========================================================================================
                string unit3 = combThreeUnit.Text;
                combThreeUnit.DataSource = Unit_cls.Search_Unit("");
                combThreeUnit.DisplayMember = "UnitName";
                combThreeUnit.ValueMember = "UnitID";
                combThreeUnit.Text = unit3;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void label14_Click(object sender, EventArgs e)
        {
            UnitFill();


        }

        private void label28_Click(object sender, EventArgs e)
        {
            UnitFill();
        }

        private void label36_Click(object sender, EventArgs e)
        {
            UnitFill();
        }

        private void Prodecut_frm_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    if (btnDelete.Enabled == true)
                    {
                        btnDelete_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F5)
                {
                    btnGetData_Click(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion
        }

        private void Prodecut_frm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (txtProdecutName.Text.Trim() != "")
            {
                if (Mass.close_frm() == false)
                {
                    e.Cancel = true;
                }
            }


        }

        private void txtFiestUnitBarcode_TextChanged(object sender, EventArgs e)
        {
            txtFiestUnitBarcode.BackColor = Color.White;

        }

        private void txtSecoundUnitBarcode_TextChanged(object sender, EventArgs e)
        {
            txtSecoundUnitBarcode.BackColor = Color.White;
        }

        private void txtThreeUnitBarcode_TextChanged(object sender, EventArgs e)
        {
            txtThreeUnitBarcode.BackColor = Color.White;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {

        }

        private void txtFiestUnitBarcode_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            var btn = sender as ButtonEdit;
            btn.Text = GetNextBarcode();

        }
        string GetNextBarcode()
        {
            var list = new List<string>() { txtFiestUnitBarcode.Text, txtSecoundUnitBarcode.Text, txtThreeUnitBarcode.Text };
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            db.Prodecuts.Select(x => x.FiestUnitBarcode );
            list.AddRange(db.Prodecuts.Select(x => x.FiestUnitBarcode).ToList());
            list.AddRange(db.Prodecuts.Select(x => x.SecoundUnitBarcode ).ToList());
            list.AddRange(db.Prodecuts.Select(x => x.ThreeUnitBarcode ).ToList());
            list = list.Distinct().ToList() ;
            return MasterClass.GetNextNumberInString(list.Max());


        }

        void UpdateIntialQTY()
        {
            //DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);

            //Prodecut_Firest_cls cls = new Prodecut_Firest_cls(); 
            //AVG_cls AVG_cls = new AVG_cls();

            //for (int i = 0; i < DGV1.Rows.Count; i++)
            //{
            //    cls.InsertFirist_Details(txtProdecutID .Text , txtProdecutID.Text, DGV1.Rows[i].Cells["Unit"].Value.ToString(), DGV1.Rows[i].Cells["UnitFactor"].Value.ToString(), DGV1.Rows[i].Cells["UnitOperating"].Value.ToString(), DGV1.Rows[i].Cells["Quantity"].Value.ToString(), DGV1.Rows[i].Cells["Price"].Value.ToString(), DGV1.Rows[i].Cells["TotalPrice"].Value.ToString(), DGV1.Rows[i].Cells["StoreID"].Value.ToString(), Recived);
            //    AVG_cls.QuantityNow_Avg(DGV1.Rows[i].Cells["ProdecutID"].Value.ToString(), DGV1.Rows[i].Cells["StoreID"].Value.ToString(), true);

            //}
        }

        private void txtProdecutID_TextChanged(object sender, EventArgs e)
        {
            GetItemBalance();
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);
            var product = db.Prodecuts.SingleOrDefault(x => x.ProdecutID == Convert.ToInt32(txtProdecutID.Text));
            if (product == null)
                product = new Prodecut();
           chk_HasColor.Checked =  product.HasColor  ;
           chk_HasSize.Checked =  product.HasSize ;
           chk_HasExpire.Checked =product.HasExpire;
            chk_HasSerial.Checked = product.HasSerial;

        }
        void GetItemBalance()
        {
            int productID = 0;
            int.TryParse(txtProdecutID.Text, out productID);
            DAL.DBDataContext db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String);

            var source = db.Inv_StoreLogs.Where(x=>x.ItemID == productID ).Select(x => x); 
             gridControl1.DataSource = source.OrderBy(isl => isl.date).GroupBy(isl => new { isl.ItemID, isl.Color, isl.Size, isl.StoreID })
                .Select(isl => new {

                 
                    StoreName = db.Stores.Single(x => x.StoreID == isl.Key.StoreID.Value).StoreName,
                    SellPrice = db.Prodecuts.Single(x => x.ProdecutID == isl.Key.ItemID).FiestUnitPrice1,
                    TotalPrice = (isl.Sum(i => i.ItemQuIN) - isl.Sum(i => i.ItemQuOut)) * db.Prodecuts.Single(x => x.ProdecutID == isl.Key.ItemID).FiestUnitPrice1,
                    Color = db.Inv_Colors.SingleOrDefault(x => x.ID == isl.Key.Color).Name ?? "",
                    Size = db.Inv_Sizes.SingleOrDefault(x => x.ID == isl.Key.Size).Name ?? "",
                    Balance = isl.Sum(i => i.ItemQuIN) - isl.Sum(i => i.ItemQuOut), 
                }).ToList();

            
             gridView1.RowCellStyle += (rs, re) => {
                if (re.RowHandle < 0) return;
                else if (re.Column.FieldName == "Balance")
                {
                    if (re.CellValue is double d && d < 0)
                    {
                        re.Appearance.BackColor = DXSkinColors.FillColors.Warning;

                    }
                    else if (re.CellValue is double dd && dd > 0)
                    {
                        re.Appearance.BackColor = DXSkinColors.FillColors.Success;

                    }
                }
            };
         
            gridView1.ViewCaption = "ارصده الصنف بالمخازن";
            gridView1.Columns["StoreName"].Caption = "المخزن";
            gridView1.Columns["Balance"].Caption = "الرصيد ";
            gridView1.Columns["TotalPrice"].Caption = "اجمالي السعر";
            gridView1.Columns["SellPrice"].Caption = "السعر";
            gridView1.Columns["Color"].Caption = "اللون";
            gridView1.Columns["Size"].Caption = "الحجم";

            GridColumnSummaryItem BalanceSum = new GridColumnSummaryItem();
            BalanceSum.SummaryType = SummaryItemType.Average;
            BalanceSum.FieldName = "Balance";
            BalanceSum.DisplayFormat = "الاجمالي: {0:#.#}";
            gridView1.Columns["Balance"].Summary.Clear();
            gridView1.Columns["Balance"].Summary.Add(BalanceSum);
            GridColumnSummaryItem TotalPriceSum = new GridColumnSummaryItem();
            TotalPriceSum.SummaryType = SummaryItemType.Sum;
            TotalPriceSum.FieldName = "TotalPrice";
            TotalPriceSum.DisplayFormat = "الاجمالي: {0:#.#}";
            gridView1.Columns["TotalPrice"].Summary.Clear();
            gridView1.Columns["TotalPrice"].Summary.Add(TotalPriceSum);
        }
    }
}
