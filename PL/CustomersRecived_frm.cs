﻿using DevExpress.XtraEditors;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class CustomersRecived_frm : XtraForm
    {
        public CustomersRecived_frm()
        {
            InitializeComponent();
        }

        Customers_Recived_cls cls = new Customers_Recived_cls();
        TreasuryMovement_cls cls_Treasury_Movement = new TreasuryMovement_cls();
        PayType_trans_cls PayType_trans_cls = new PayType_trans_cls();
        string TreasuryID;
        private void CustomersPayment_FormAdd_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingToolStrip(toolStrip1);
            ERB_Setting.SettingForm(this);
         
            btnNew_Click_1(null, null);
           
        }

       

       







        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                CustomersSearch_frm frm = new CustomersSearch_frm();
                frm.ShowDialog(this);
                if (frm.loaddata == true)
                {
                    txtCustomerName.BackColor = System.Drawing.Color.White;
                    txtCustomerID.Text = frm.DGV1.CurrentRow.Cells["CustomerID"].Value.ToString();
                    txtCustomerName.Text = frm.DGV1.CurrentRow.Cells["CustomerName"].Value.ToString();
                    //DataTable dt = Cus_Trans_cls.Cus_Balence(txtCustomerID.Text);
                    if (btnSave.Enabled==true)
                    {
                        BalenceCustomer(txtCustomerID.Text);
                    }
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }


        }
        TreasuryMovement_cls TreasuryMovement_CLS = new TreasuryMovement_cls();


        private void BalenceCustomer(string CustomerID)
        {
            try
            {
                txtCustomersBlanse.Text = "0";
                if (CustomerID.Trim() != "" || CustomerID != "System.Data.DataRowView")
                {
                    DataTable dt = TreasuryMovement_CLS.Balence_ISCusSupp(CustomerID, "Cus");//Cus_Trans_cls.Cus_Balence(dt_cus.Rows[i]["CustomerID"].ToString());
                    //  = Cus_Trans_cls.Cus_Balence(txtAccountID.Text);
                    txtCustomersBlanse.Text = "0";
                    if (dt.Rows[0]["balence"] == DBNull.Value == false)
                    {
                        txtCustomersBlanse.Text = dt.Rows[0]["balence"].ToString();
                    }
                    //=========================================================================================================
              

                }
            }
            catch
            {

            }

        }










        private void LoadPaymentCustomer()
        {
            try
            {

                CustomersRecivedSearch_frm frm = new CustomersRecivedSearch_frm();
                frm.ShowDialog(this);
                if (frm.LoadData == true)
                {
                    btnSave.Enabled = false;
                    btnUpdate.Enabled = true;
                    btnDelete.Enabled = true;
                    DataTable dt = cls.Details_Customers_Recived(frm.DGV1.CurrentRow.Cells["PayID"].Value.ToString());
                    DataRow Dr = dt.Rows[0];
                    txtPayID.Text = Dr["PayID"].ToString();
                    D1.Value = Convert.ToDateTime(Dr["PayDate"]);
                    //===============================================================================================================================
                    txtCustomerID.Text = Dr["CustomerID"].ToString();
                    txtRemarks.Text = Dr["Remarks"].ToString();
                    //==================================================================================================================================
                    txtCustomerName.Text = Dr["CustomerName"].ToString();
                    TreasuryID = Dr["TreasuryID"].ToString();



                    // PyeType
                    //DataTable dt_paytype_trans = PayType_trans_cls.Details_PayType_trans(txtPayID.Text, txtTypeID.Text);
                    //  for (int i = 0; i < dt_paytype_trans.Rows.Count; i++)
                    //  {
                    //      AddDgv_PayType(DGVPayType, dt_paytype_trans.Rows[i]["PayTypeID"].ToString(), dt_paytype_trans.Rows[i]["PayTypeName"].ToString(), dt_paytype_trans.Rows[i]["Debit"].ToString(), dt_paytype_trans.Rows[i]["Statement"].ToString(), dt_paytype_trans.Rows[i]["CurrencyID"].ToString(), dt_paytype_trans.Rows[i]["CurrencyName"].ToString(), dt_paytype_trans.Rows[i]["CurrencyRate"].ToString(), dt_paytype_trans.Rows[i]["CurrencyPrice"].ToString());
                    //  }
                    //  SumPayType();

                    txtSumPayType.EditValue = Convert.ToDouble(Dr["PayValue"]);






    }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnNew_Click_1(object sender, EventArgs e)
        {
            try
            {
                txtPayID.Text = Convert.ToString(cls.MaxID_Customers_Recived());
                D1.Value = DateTime.Now;
                txtSumPayType.EditValue  = 0;
                txtCustomerID.Text = "";
                txtCustomerName.Text = ""; 
                txtCustomerName.BackColor = System.Drawing.Color.White; 
                txtRemarks.Text = ""; 
                txtSumPayType.Text = "0";
                txtCustomersBlanse.Text = "0";
              
                
                
             

                btnSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if (txtPayID.Text == "")
            {
                MessageBox.Show("حدث خطأ يرجي المحاولة لاحقا", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (txtCustomerID.Text.Trim() == "" || txtCustomerName.Text.Trim() == "")
            {
                txtCustomerName.BackColor = System.Drawing.Color.Pink;
                button1.Focus();
                return;
            }




            if (txtSumPayType.Text == "0")
            {
                MessageBox.Show("يرجي ادخال المبلغ ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }


            if (string.IsNullOrEmpty(txtRemarks.Text.Trim()))
            {
                txtRemarks.Focus();
                MessageBox.Show("يرجي ادخال البيان ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try
            {


             

                TreasuryID = cls_Treasury_Movement.MaxID_TreasuryMovement();
                txtPayID.Text = Convert.ToString(cls.MaxID_Customers_Recived());
                cls.InsertCustomers_Recived(txtPayID.Text, D1.Value, Convert.ToDouble(txtSumPayType.Text), txtRemarks.Text, txtCustomerID.Text, TreasuryID, Properties.Settings.Default.UserID);
                cls_Treasury_Movement.InsertTreasuryMovement(TreasuryID, txtCustomerID.Text, "Cus", txtPayID.Text, txtTypeID.Text, this.Text, D1.Value, txtCustomerName.Text, txtSumPayType.Text, "0", txtRemarks.Text, "0", txtSumPayType.Text, Properties.Settings.Default.UserID);

                //#region // Pay
                //for (int i = 0; i < DGVPayType.Rows.Count; i++)
                //{
                //    string Statement = this.Text + " / " + txtCustomerName.Text;
                //    if (!string.IsNullOrEmpty(DGVPayType.Rows[i].Cells["Statement"].Value.ToString()))
                //    {
                //        Statement = DGVPayType.Rows[i].Cells["Statement"].Value.ToString();
                //    }
                //    PayType_trans_cls.Insert_PayType_trans(txtPayID.Text, D1.Value, DGVPayType.Rows[i].Cells["PayTypeID"].Value.ToString(), txtPayID.Text, this.Text , txtTypeID.Text, double.Parse(DGVPayType.Rows[i].Cells["PayValue"].Value.ToString()),0, Statement, Properties.Settings.Default.UserID, DGVPayType.Rows[i].Cells["CurrencyID2"].Value.ToString(), DGVPayType.Rows[i].Cells["CurrencyRate2"].Value.ToString(), DGVPayType.Rows[i].Cells["CurrencyPrice2"].Value.ToString());
                //}
                //#endregion


                Mass.Saved();
                btnNew_Click_1(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnUpdate_Click_1(object sender, EventArgs e)
        {
            if (txtPayID.Text == "")
            {
                MessageBox.Show("حدث خطأ يرجي المحاولة لاحقا", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (txtCustomerID.Text == "" || txtCustomerName.Text == "")
            {
                txtCustomerName.BackColor = System.Drawing.Color.Pink;
                button1.Focus();
                return;
            }




            if (txtSumPayType.Text == "0")
            {
                MessageBox.Show("يرجي ادخال المبلغ ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (string.IsNullOrEmpty(txtRemarks.Text.Trim()))
            {
                txtRemarks.Focus();
                MessageBox.Show("يرجي ادخال البيان ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }


            try
            {

                cls.UpdateCustomers_Recived(txtPayID.Text, D1.Value, Convert.ToDouble(txtSumPayType.Text), txtRemarks.Text, txtCustomerID.Text);

                cls_Treasury_Movement.UpdateTreasuryMovement(TreasuryID, txtCustomerID.Text, "Cus", txtPayID.Text, txtTypeID.Text, this.Text, D1.Value, txtCustomerName.Text, txtSumPayType.Text, "0", txtRemarks.Text, "0", txtSumPayType.Text);

                //#region // Pay
                //PayType_trans_cls.Delete_PayType_trans(txtPayID.Text, txtTypeID.Text);
                //for (int i = 0; i < DGVPayType.Rows.Count; i++)
                //{

                //    PayType_trans_cls.Insert_PayType_trans(txtPayID.Text, D1.Value, DGVPayType.Rows[i].Cells["PayTypeID"].Value.ToString(), txtPayID.Text, this.Text, txtTypeID.Text,  double.Parse(DGVPayType.Rows[i].Cells["PayValue"].Value.ToString()),0, DGVPayType.Rows[i].Cells["Statement"].Value.ToString(), Properties.Settings.Default.UserID, DGVPayType.Rows[i].Cells["CurrencyID2"].Value.ToString(), DGVPayType.Rows[i].Cells["CurrencyRate2"].Value.ToString(), DGVPayType.Rows[i].Cells["CurrencyPrice2"].Value.ToString());
                //}
                //#endregion

                MessageBox.Show("تم التعديل بنجاح", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnNew_Click_1(null, null);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (Mass.Delete() == true)
                {
                    cls.DeleteCustomers_Recived(txtPayID.Text);
                    cls_Treasury_Movement.DeleteTreasuryMovement(TreasuryID);
                    PayType_trans_cls.Delete_PayType_trans(txtPayID.Text, txtTypeID.Text);

                    btnNew_Click_1(null, null);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                LoadPaymentCustomer();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        



      




     

        private void CustomersRecived_frm_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click_1(null, null);
                }
                if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        toolStripButton3_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click_1(null, null);
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    if (btnDelete.Enabled == true)
                    {
                        btnDelete_Click_1(null, null);
                    }
                }
                //if (e.KeyCode == Keys.F5)
                //{
                //    btnGetData_Click(null, null);
                //}

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion
        }

       

       
    }
}
