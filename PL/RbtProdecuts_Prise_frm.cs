﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class RbtProdecuts_Prise_frm : Form
    {
        public RbtProdecuts_Prise_frm()
        {
            InitializeComponent();
        }
        ProdecutAction_cls cls = new ProdecutAction_cls();
        private void BalanseItemes_frm_Load(object sender, EventArgs e)
        {
            try
            {
                ERB_Setting.SettingToolStrip(toolStrip1);
                ERB_Setting.SettingForm(this);
                ERB_Setting.SettingDGV(DGV1);

                // FILL Combo Store item
                Store_cls Store_cls = new Store_cls();
                combStore.DataSource = Store_cls.Search_Stores("");
                combStore.DisplayMember = "StoreName";
                combStore.ValueMember = "StoreID";
                combStore.Text = "";
                txtStoreID.Text = "";
                //================================================================================
                // FILL Combo Store item
                Prodecut_cls Prodecut_cls = new Prodecut_cls();
                combProdecut.DataSource = Prodecut_cls.Search_Prodecuts("");
                combProdecut.DisplayMember = "ProdecutName";
                combProdecut.ValueMember = "ProdecutID";
                combProdecut.Text = "";
                txtProdecutID.Text = "";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }









 

        

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                DGV1.DataSource = null;
                DGV1.Rows.Clear();
                DataTable dt = new DataTable();
                if (combStore.Text.Trim()==""||txtStoreID.Text.Trim()=="")
                {
                    //===================================================================================================================================================================================================================================================================================================================================================================
                    dt= cls.Search_Prodect_Prise(txtProdecutID.Text,D1.Value,D2.Value);
              

                  
                }
                else
                {
                    dt = cls.Search_Prodect_Prise(txtStoreID.Text,txtProdecutID.Text, D1.Value, D2.Value);
             
                }
                DGV1.AutoGenerateColumns = false;
                DGV1.DataSource = dt;
              
                lblCount.Text = DGV1.Rows.Count.ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }



            }

      
        private void combStore_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                combStore.BackColor = Color.White;
                txtStoreID.Text = combStore.SelectedValue.ToString();

            }
            catch
            {

                txtStoreID.Text = "";

            }
        }

        private void combStore_TextChanged(object sender, EventArgs e)
        {
            try
            {
                combStore.BackColor = Color.White;

                if (combStore.Text == "")
                {
                    txtStoreID.Text = "";
                }
                else
                {
                    txtStoreID.Text = combStore.SelectedValue.ToString();
                }
            }
            catch
            {
                txtStoreID.Text = "";
            }
        }

        private void combProdecut_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                combProdecut.BackColor = Color.White;
                txtProdecutID.Text = combProdecut.SelectedValue.ToString();

            }
            catch
            {

                txtProdecutID.Text = "";

            }
        }

        private void combProdecut_TextChanged(object sender, EventArgs e)
        {
            try
            {
                combProdecut.BackColor = Color.White;

                if (combProdecut.Text == "")
                {
                    txtProdecutID.Text = "";
                }
                else
                {
                    txtProdecutID.Text = combProdecut.SelectedValue.ToString();
                }
            }
            catch
            {
                txtProdecutID.Text = "";
            }
        }
    }
}
