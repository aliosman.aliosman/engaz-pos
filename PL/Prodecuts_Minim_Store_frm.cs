﻿using ByStro.RPT;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class Prodecuts_Minim_Store_frm : Form
    {
        public Prodecuts_Minim_Store_frm()
        {
            InitializeComponent();
        }
        Store_Prodecut_cls cls = new Store_Prodecut_cls();
        private void BalanseItemes_frm_Load(object sender, EventArgs e)
        {
            try
            {
                ERB_Setting.SettingToolStrip(toolStrip1);
                ERB_Setting.SettingForm(this);
                ERB_Setting.SettingDGV(DGV1);
                // FILL Combo Group item
                Category_cls Category_CLS = new Category_cls();
                combGroup.DataSource = Category_CLS.Search_Category("");
                combGroup.DisplayMember = "CategoryName";
                combGroup.ValueMember = "CategoryID";
                combGroup.Text = "";
                txtCategoryID.Text = "";

                // FILL Combo Store item
                Store_cls Store_cls = new Store_cls();
                combStore.DataSource = Store_cls.Search_Stores("");
                combStore.DisplayMember = "StoreName";
                combStore.ValueMember = "StoreID";
                combStore.Text = "";
                txtStoreID.Text = "";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }








        // "تحويل القيمة الي  وحدات كبري وصغري"
        private string ChangeUnit(string Quentity, string FiestUnit, string FiestUnitFactor, string SecoundUnit, string SecoundUnitOperating, string SecoundUnitFactor, string ThreeUnit, string ThreeUnitOperating, string ThreeUnitFactor)
        {
            if (Quentity=="" || Quentity=="0")
            {
                return "";
            }
            string Resalet = "";
            if (ThreeUnitOperating != "")
            {
                decimal n = Convert.ToDecimal(Quentity) / Convert.ToDecimal(ThreeUnitOperating);
                decimal H = Math.Floor(n);
                decimal b = Convert.ToDecimal(H) * Convert.ToDecimal(ThreeUnitOperating);
                decimal namber3 = Convert.ToDecimal(Quentity) - Convert.ToDecimal(b);
                decimal F = Convert.ToDecimal(H) / Convert.ToDecimal(SecoundUnitOperating);
                decimal namber1 = Math.Floor(F);
                decimal Fd = Convert.ToDecimal(namber1) * Convert.ToDecimal(SecoundUnitOperating);
                decimal namer2 = Convert.ToDecimal(H) - Convert.ToDecimal(Fd);
                Resalet = namber1 + "  " + FiestUnit + "  // " + namer2 + "  " + SecoundUnit + "  // " + namber3 + " " + ThreeUnit;
                //======================================================================================================================================================================================================================================================

            }

            else if (SecoundUnitOperating != "")
            {

                var y = Convert.ToDouble(Quentity) / Convert.ToDouble(SecoundUnitOperating);
                var Hnamber1 = Math.Floor(y);
                var dFd = Convert.ToDouble(Hnamber1) * Convert.ToDouble(SecoundUnitOperating);
                var hnamer2 = Convert.ToDouble(Quentity) - Convert.ToDouble(dFd);
                Resalet = Hnamber1 + " " + FiestUnit + "  // " + hnamer2 + " " + SecoundUnit;
                //======================================================================================================================================================================================================================================================

            }
            else if (FiestUnit != "")
            {
                Resalet = Quentity + "  " + FiestUnit;
                //======================================================================================================================================================================================================================================================
            }

            return Resalet;
        }



        private void AddRowDgv(string ProdecutID, string ProdecutName,string RequestLimit, string Balence, string ChangeAllUnit)
        {
            try
            {
                DGV1.Rows.Add();
                DGV1.ClearSelection();
                DGV1.Rows[DGV1.Rows.Count - 1].Selected = true;
                var lastRows = DGV1.Rows.Count - 1;
                DGV1.Rows[lastRows].Cells["ProdecutID"].Value = ProdecutID;
                DGV1.Rows[lastRows].Cells["ProdecutName"].Value = ProdecutName;
                 DGV1.Rows[lastRows].Cells["RequestLimit"].Value = RequestLimit;
                //DGV1.Rows[lastRows].Cells["Import"].Value = Import;
                //DGV1.Rows[lastRows].Cells["Export"].Value = Export;
                DGV1.Rows[lastRows].Cells["Balence"].Value = Balence;
                DGV1.Rows[lastRows].Cells["Change_Unit"].Value = ChangeAllUnit;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }

 

        private void txtCategoryName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Delete||e.KeyCode==Keys.Back)
            {
                txtCategoryID.Text = "";
            }
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                if (combStore.Text.Trim()==""||txtStoreID.Text.Trim()=="")
                {
                    MessageBox.Show("يجب تحديد المخزن", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    combStore.Focus();
                    return;
                }
                DataTable dt = new DataTable();
                DGV1.AutoGenerateColumns = false;
                if (combGroup.Text.Trim() == "")
                {
                    dt = cls.Store_Prodecut_Minim(txtStoreID.Text);
                }
                else
                {
                    dt = cls.Store_Prodecut_Minim(txtStoreID.Text, txtCategoryID.Text);
                }
                DGV1.DataSource = null;
                DGV1.Rows.Clear();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (Convert.ToDouble(dt.Rows[i]["RequestLimit"].ToString()) >= Convert.ToDouble(dt.Rows[i]["Balence"].ToString()))
                    {
                        string Change_All_Unit = ChangeUnit(dt.Rows[i]["Balence"].ToString(), dt.Rows[i]["FiestUnit"].ToString(), dt.Rows[i]["FiestUnitFactor"].ToString(), dt.Rows[i]["SecoundUnit"].ToString(), dt.Rows[i]["SecoundUnitOperating"].ToString(), dt.Rows[i]["SecoundUnitFactor"].ToString(), dt.Rows[i]["ThreeUnit"].ToString(), dt.Rows[i]["ThreeUnitOperating"].ToString(), dt.Rows[i]["ThreeUnitFactor"].ToString());
                        AddRowDgv(dt.Rows[i]["ProdecutID"].ToString(), dt.Rows[i]["ProdecutName"].ToString().ToString(), dt.Rows[i]["RequestLimit"].ToString().ToString(), dt.Rows[i]["Balence"].ToString(), Change_All_Unit);
                    }
                }
                lblCount.Text = DGV1.Rows.Count.ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }



            }

        private void combGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                combGroup.BackColor = Color.White;
                txtCategoryID.Text = combGroup.SelectedValue.ToString();

            }
            catch
            {

                txtCategoryID.Text = "";

            }
        }

        private void combGroup_TextChanged(object sender, EventArgs e)
        {

            try
            {
                combGroup.BackColor = Color.White;

                if (combGroup.Text == "")
                {
                    txtCategoryID.Text = "";
                }
                else
                {
                    txtCategoryID.Text = combGroup.SelectedValue.ToString();
                }
            }
            catch
            {

                txtCategoryID.Text = "";

            }
        }

        private void combStore_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                combStore.BackColor = Color.White;
                txtStoreID.Text = combStore.SelectedValue.ToString();

            }
            catch
            {

                txtStoreID.Text = "";

            }
        }

        private void combStore_TextChanged(object sender, EventArgs e)
        {
            try
            {
                combStore.BackColor = Color.White;

                if (combStore.Text == "")
                {
                    txtStoreID.Text = "";
                }
                else
                {
                    txtStoreID.Text = combStore.SelectedValue.ToString();
                }
            }
            catch
            {
                txtStoreID.Text = "";
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (DGV1.Rows.Count==0)
                {
                    return;
                }
                Prodecut_cls Prodecut_cls = new Prodecut_cls();
                DataTable dt = Prodecut_cls.Search_Prodecuts("0");
                dt.Clear();
                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    DataRow Dr = dt.NewRow();
                    Dr["ProdecutName"] = DGV1.Rows[i].Cells["ProdecutName"].Value;
                    Dr["RequestLimit"] = DGV1.Rows[i].Cells["RequestLimit"].Value;
                    Dr["DiscoundBay"] = DGV1.Rows[i].Cells["Balence"].Value;
                    Dr["Company"] = DGV1.Rows[i].Cells["Change_Unit"].Value;
                    dt.Rows.Add(Dr);
                }
                #region "Print"
                //Reportes_Form frm = new Reportes_Form();
                //CRStorItemQtyLow report = new CRStorItemQtyLow();
                //report.Database.Tables["Prodecuts"].SetDataSource(dt);
                //report.SetParameterValue(0, combStore.Text);
                //report.SetParameterValue(1, DGV1.Rows.Count.ToString());
                //frm.crystalReportViewer1.ReportSource = report;
                //frm.crystalReportViewer1.Refresh();
                //frm.ShowDialog();
                #endregion

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


           

        }
    }
}
