﻿
using System;
using System.Windows.Forms;
namespace ByStro.PL
{
    public partial class CustomersSearch_frm : Form
    {
        public CustomersSearch_frm()
        {
            InitializeComponent();
        }
        public Boolean loaddata = false;

        Customers_cls cls = new Customers_cls();

        private void AccountEnd_Form_Load(object sender, EventArgs e)
        {
            try
            {
                //Load  
                ERB_Setting.SettingForm(this);
                ERB_Setting.SettingDGV(DGV1);
                DGV1.AutoGenerateColumns = false;
                DGV1.DataSource = cls.Search_Customers("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DGV1.AutoGenerateColumns = false;
                DGV1.DataSource = cls.Search_Customers(txtSearch.Text);


            }
            catch
            {
                return;
            }


        }


        private void AccountEnd_Form_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Escape)
            {
                Close();
            }
        }

        private void DGV1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (DGV1.Rows.Count == 0)
                {
                    return;
                }
                String Header = DGV1.Columns[e.ColumnIndex].Name;
                if (Header == "C1")
                {
                    loaddata = true;
                    ID_Load = DGV1.CurrentRow.Cells["CustomerID"].Value.ToString();
                    Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DGV1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            try
            {
                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    DGV1.Rows[i].Cells[0].Value = Properties.Resources.Open_Folder_48px;
                }
            }
            catch
            {


            }
        }
        public String ID_Load = "";
        private void DGV1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    if (DGV1.SelectedRows.Count == 1)
                    {
                        loaddata = true;
                        ID_Load = DGV1.CurrentRow.Cells["CustomerID"].Value.ToString();
                        Close();
                    }


                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }







    }
}
