﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class MoneyTransfer_Search_frm : Form
    {
        public MoneyTransfer_Search_frm()
        {
            InitializeComponent();
        }
        MonyTransfer_cls cls = new MonyTransfer_cls();
       public Boolean LoadData = false;
        private void CustomersPayment_FormAR_Load(object sender, EventArgs e)
        {

            try
            {
                ERB_Setting.SettingToolStrip(toolStrip1);
                ERB_Setting.SettingForm(this);
                ERB_Setting.SettingDGV(DGV1);
                DGV1.AutoGenerateColumns = false;
                DGV1.DataSource = cls.Search_MoneyTransfer( datefrom.Value, datetto.Value);
                txtSearch.Text = "";
                txtSearch.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }



        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DGV1.AutoGenerateColumns = false;
                DGV1.DataSource = cls.Search_MoneyTransfer( datefrom.Value, datetto.Value);
             
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void datefrom_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DGV1.AutoGenerateColumns = false;
                DGV1.DataSource = cls.Search_MoneyTransfer(datefrom.Value, datetto.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void datetto_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                DGV1.AutoGenerateColumns = false;
                DGV1.DataSource = cls.Search_MoneyTransfer(datefrom.Value, datetto.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            if (DGV1.Rows.Count==0)
            {
                return;
            }
            LoadData = true;
            Close();
        }


       

    }
}
