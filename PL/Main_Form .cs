﻿using ByStro.BL;
using ByStro.DataSet;
using ByStro.Forms;
using ByStro.Properties;
using ByStro.UControle;
using DevExpress.LookAndFeel;
using DevExpress.XtraBars.Helpers;
using DevExpress.XtraEditors;
using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using System.Windows.Forms;


namespace ByStro.PL
{
    public partial class Main_frm : XtraForm
    {
        public Main_frm()
        {


            #region"تفعيل البرنامج "
            DataAccessLayer.Finull_Sarial = BL.Final_key.GetHash(Properties.Settings.Default.computerID);


            #endregion
            InitializeComponent();

        }



        private void Sales_Click(object sender, EventArgs e)
        {

        }

        private void Bay_Click(object sender, EventArgs e)
        {
            frm_Inv_Invoice frm = new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.PurchaseInvoice);

            frm.MdiParent = this;
            frm.TopLevel = false;
            frm.Dock = DockStyle.Fill;
            frm.Show();
        }

        private void Stores_Click(object sender, EventArgs e)
        {
            //StorItem_Form frm = new StorItem_Form();
            //frm.MdiParent = this;
            //frm.TopLevel = false;
            //frm.Dock = DockStyle.Fill;
            //frm.Show();
        }



    

        private void Main_Form_Load_1(object sender, EventArgs e)
        {
             
          

            ERB_Setting.SettingToolStrip(ToolStrip2);

            DevExpress.XtraEditors.WindowsFormsSettings.DefaultFont = Settings.Default.SystemFont;


            changBakground();

            if (!backgroundWorker1.IsBusy)
            {
                backgroundWorker1.RunWorkerAsync();
            }
            Active_CHec();

            #region MyRegion
            timer1.Start();

            this.ToolStrip2.Font = Properties.Settings.Default.Font;
            notifyIcon1.Text = DataAccessLayer.Version;
            if (Properties.Settings.Default.BukUPState == "3")
            {
                DataAccessLayer.paukupautomatic = "3";
                Baukup_Form frm = new Baukup_Form();
                frm.ControlBox = true;
                frm.ShowDialog();
            }

            NotifacationProdect();
            NotifacationCustomer();

            //try
            //{

            //    //foreach (Control x in this.Controls)
            //    //{
            //    //    if (Application.OpenForms["Notifications_UC"] != null)
            //    //    {
            //    //        Application.OpenForms["Notifications_UC"].Close();
            //    //    }
            //    //}


            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

            //}


            #region"change Fonts"
            //try
            //{
            //    //PrivateFontCollection mfont = new PrivateFontCollection();
            //    //mfont.AddFontFile("albayan_medium-e9aa1cc3225d60ac44bbc0738243f49b.ttf");

            //    //ToolStrip2.Font = new Font(mfont.Families[0], 12, FontStyle.Bold);

            //    //butsales.Font = new Font(mfont.Families[1], 12);
            //    //butReternSales.Font = new Font(mfont.Families[0], 12);
            //    //butBay.Font = new Font(mfont.Families[0], 12);
            //    //butReturnBay.Font = new Font(mfont.Families[0], 12);
            //    //butStores.Font = new Font(mfont.Families[0], 12);
            //    //butSetting.Font = new Font(mfont.Families[0], 12);
            //    //butReport.Font = new Font(mfont.Families[0], 12);
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "Message Fonts", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}

            #endregion



            MdiClient ctIMDI;
            foreach (Control item in this.Controls)
            {
                try
                {
                    ctIMDI = (MdiClient)item;
                    ctIMDI.BackColor = this.BackColor;
                }
                catch
                {


                }
            }



            #endregion



            Program.CheckForDBUpdate();

        }

        Double countSales = 0;
        private void Active_CHec()
        {
            #region"تفعيل البرنامج "

            try
            {
                Trail_cls sales_CLS = new Trail_cls();
                countSales = Convert.ToDouble(sales_CLS.COUNT_Sales_Main());
            }
            catch
            {

                countSales = 0;
            }

            try
            {
                if (Properties.Settings.Default.Finull_Sarial != DataAccessLayer.Finull_Sarial)
                {

                    if (countSales >= Final_key.SalesNumber)
                    {

                        SerilFull_FORM FrmAc = new SerilFull_FORM();
                        DataAccessLayer.closeMainform = true;
                        this.Close();
                        FrmAc.Show();
                    }
                    else
                    {
                        Properties.Settings.Default.LicenseType = "Trail";
                        Properties.Settings.Default.Finull_Sarial = "1E01-192B-FBD3-75EE-A005-9910-E120-6A30";
                        this.Text = DataAccessLayer.Version + " Trail Version";
                        Properties.Settings.Default.Save();
                    }
                }
                else
                {
                    RegistryKey regkey = default(RegistryKey);
                    regkey = Registry.CurrentUser.OpenSubKey("SOFTWARE\\ByStor", true);
                    string code = "";
                    code = regkey.GetValue("System_Activation").ToString();
                    if (DataAccessLayer.DecryptData(code) == DataAccessLayer.Finull_Sarial)
                    {
                        Properties.Settings.Default.LicenseType = "Full";
                        this.Text = DataAccessLayer.Version + " Full Version";
                        Properties.Settings.Default.Save();
                    }
                    else
                    {
                        if (countSales >= Final_key.SalesNumber)
                        {
                            SerilFull_FORM FrmAc = new SerilFull_FORM();
                            DataAccessLayer.closeMainform = true;
                            this.Close();
                            FrmAc.Show();
                        }

                    }

                }


            }
            catch
            {
                Properties.Settings.Default.LicenseType = "Trail";
                Properties.Settings.Default.Finull_Sarial = "1E01-192B-FBD3-75EE-A005-9910-E120-6A30";
                this.Text = DataAccessLayer.Version + " Trail Version";
                Properties.Settings.Default.Save();
            }
            #endregion

        }



        private void butStores_Click(object sender, EventArgs e)
        {

        }

        private void butBay_Click_1(object sender, EventArgs e)
        {


            if (Application.OpenForms["BillBay_frm"] != null)
            {
                Application.OpenForms["BillBay_frm"].Activate();

            }
            else
            {
                frm_Inv_Invoice frm = new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.PurchaseInvoice);

                frm.MdiParent = this;
                frm.Show();
            }
        }








        private void خروجToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.BukUPState == "2")
            {
                DataAccessLayer.paukupautomatic = "OK";
                Baukup_Form frm = new Baukup_Form();
                frm.ShowDialog();
            }
            else
            {
                DataAccessLayer.closeMainform = true;
                Application.Exit();
            }


        }

        private void تسجيلخروجToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DataAccessLayer.closeMainform = true;
            this.Close();

            if (Application.OpenForms["LoginUser_Form"] != null)
            {
                ((LoginUser_Form)Application.OpenForms["LoginUser_Form"]).Show();
                if (Properties.Settings.Default.RemmberPassword == false)
                {
                    ((LoginUser_Form)Application.OpenForms["LoginUser_Form"]).txtUserName.Text = "";
                    ((LoginUser_Form)Application.OpenForms["LoginUser_Form"]).txtPassword.Text = "";
                }

                ((LoginUser_Form)Application.OpenForms["LoginUser_Form"]).txtUserName.Focus();
                DataAccessLayer.closeMainform = false;
            }

        }

        private void Roles9_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["Customers_Form"] != null)
                {
                    Application.OpenForms["Customers_Form"].Activate();

                }
                else
                {
                    Customers_frm frm = new Customers_frm();
                    
                    frm.Show();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }



        private void Roles11_Click(object sender, EventArgs e)
        {

            try
            {
                if (Application.OpenForms["CustomersTrans_Form"] != null)
                {
                    Application.OpenForms["CustomersTrans_Form"].Activate();

                }
                else
                {
                    CustomersTrans_Form frm = new CustomersTrans_Form();
                    frm.MdiParent = this;
                    frm.Show();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }



        private void Roles14_Click(object sender, EventArgs e)
        {

            try
            {
                if (Application.OpenForms["Suppliers_Form"] != null)
                {
                    Application.OpenForms["Suppliers_Form"].Activate();

                }
                else
                {
                    Suppliers_Form frm = new Suppliers_Form();
                    frm.MdiParent = this;
                    frm.Show();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }



        private void Roles16_Click(object sender, EventArgs e)
        {


            if (Application.OpenForms["SuppliersTrans_frm"] != null)
            {
                Application.OpenForms["SuppliersTrans_frm"].Activate();

            }
            else
            {
                SuppliersTrans_frm frm = new SuppliersTrans_frm();
                frm.MdiParent = this;
                frm.Show();
            }

        }








        private void Roles1_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["Item_Form"] != null)
            {
                Application.OpenForms["Item_Form"].Activate();

            }
            else
            {
                Prodecut_frm frm = new Prodecut_frm();
                frm.Show();
            }

        }



        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

            if (Application.OpenForms["Category_frm"] != null)
            {
                Application.OpenForms["Category_frm"].Activate();

            }
            else
            {
                Category_frm frm = new Category_frm();
                frm.Show();
            }


        }

        private void Roles26_Click(object sender, EventArgs e)
        {

            //DataAccessLayer.paukupautomatic = "no";
            //Baukup_Form frm = new Baukup_Form();
            //frm.ShowDialog();
            new frm_BackupRestore().ShowDialog();
        }




        private void Roles5_Click(object sender, EventArgs e)
        {

            if (Application.OpenForms["BillBay_frm"] != null)
            {
                Application.OpenForms["BillBay_frm"].Activate();

            }
            else
            {
                frm_Inv_Invoice frm = new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.PurchaseInvoice);
                frm.MdiParent = this;
                frm.Show();
            }

        }









        private void Roles30_Click(object sender, EventArgs e)
        {


            ReportExpenses_Form frm = new ReportExpenses_Form();
            frm.ShowDialog();

        }

        private void التحويلبينالمخازنToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (Application.OpenForms["frm_ItemStoreMove"] != null)
            {
                Application.OpenForms["frm_ItemStoreMove"].Activate();

            }
            else
            {

                frm_Inv_Invoice frm = new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.ItemStoreMove);
                frm.MdiParent = this;
                frm.Show();
            }

        }

        private void Roles3_Click(object sender, EventArgs e)
        {

            Store_frm frm = new Store_frm();
            frm.ShowDialog();
        }




        private void اعداداتالاتصالToolStripMenuItem_Click(object sender, EventArgs e)
        {

            ConnectSQLServer_Form frm = new ConnectSQLServer_Form();
            frm.add = "Main";
            frm.ShowDialog(this);


        }



        private void Roles33_Click(object sender, EventArgs e)
        {

            if (Application.OpenForms["CustomersTrans_Form"] != null)
            {
                Application.OpenForms["CustomersTrans_Form"].WindowState = FormWindowState.Maximized;
                Application.OpenForms["CustomersTrans_Form"].Activate();
            }
            else
            {
                CustomersTrans_Form frm = new CustomersTrans_Form();
                frm.MdiParent = this;
                frm.Show();
            }

        }

        private void Roles34_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["SuppliersTrans_frm"] != null)
            {
                Application.OpenForms["SuppliersTrans_frm"].WindowState = FormWindowState.Maximized;
                Application.OpenForms["SuppliersTrans_frm"].Activate();
            }
            else
            {
                SuppliersTrans_frm frm = new SuppliersTrans_frm();
                frm.MdiParent = this;
                frm.Show();
            }

        }



        private void Roles27_Click(object sender, EventArgs e)
        {


            CombanyInformatuon_FormAR frm = new CombanyInformatuon_FormAR();
            frm.ShowDialog();
        }





        private void تغيركلمةالمرورToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangePassword_frm frm = new ChangePassword_frm();
            frm.ShowDialog();
        }

        private void توريدنقديةToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (Application.OpenForms["CashImport_frm"] != null)
            {
                Application.OpenForms["CashImport_frm"].Activate();

            }
            else
            {
                CashImport_frm frm = new CashImport_frm();
                frm.MdiParent = this;
                frm.Show();
            }
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {

            if (Application.OpenForms[nameof(frm_Inv_Invoice)] != null)
            {
                Application.OpenForms[nameof(frm_Inv_Invoice)].Activate();

                Application.OpenForms[nameof(frm_Inv_Invoice)].Show();
                Application.OpenForms[nameof(frm_Inv_Invoice)].BringToFront();
            }
            else
            {

                frm_Inv_Invoice frm = new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.SalesInvoice);

                frm.Show();
            }




        }

        public void OpneNewBill()
        {
            frm_Inv_Invoice frm = new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.SalesInvoice);

            frm.MdiParent = this;
            frm.Show();
        }
        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {

            if (Application.OpenForms["frm_SaleReturnInvoice"] != null)
            {
                Application.OpenForms["frm_SalesReturnInvoice"].Activate();

            }
            else
            {
                frm_Inv_Invoice frm = new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.SalesReturn);

                frm.MdiParent = this;
                frm.Show();
            }


        }

        private void Roles8_Click(object sender, EventArgs e)
        {

            if (Application.OpenForms["frm_PurchaseReturnInvoice"] != null)
            {
                Application.OpenForms["frm_PurchaseReturnInvoice"].Activate();

            }
            else
            {
                frm_Inv_Invoice frm = new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.PurchaseReturn);
               
                frm.Show();
            }
        }

        private void يToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (Application.OpenForms["CashExport_frm"] != null)
            {
                Application.OpenForms["CashExport_frm"].Activate();

            }
            else
            {

                CashExport_frm frm = new CashExport_frm();
                frm.MdiParent = this;
                frm.Show();
            }
        }





        private void Main_Form_KeyDown(object sender, KeyEventArgs e)
        {

            #region "KeyDown"
            try
            {

                if (e.Control == true && e.KeyCode == Keys.Q)
                {
                    if (CS_11.Enabled == true)
                    {
                        toolStripMenuItem2_Click(null, null);
                    }
                }
                else if (e.Control == true && e.KeyCode == Keys.W)
                {
                    if (CS_14.Enabled == true)
                    {
                        toolStripMenuItem8_Click(null, null);
                    }
                }
                else if (e.Control == true && e.KeyCode == Keys.E)
                {
                    if (CS11.Enabled == true)
                    {
                        Roles5_Click(null, null);
                    }
                }
                else if (e.Control == true && e.KeyCode == Keys.R)
                {
                    if (CS_18.Enabled == true)
                    {
                        Roles8_Click(null, null);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion


        }

        private void بدايةالمدةToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (Application.OpenForms["frm_OpenBalanceInvoice"] != null)
            {
                Application.OpenForms["frm_OpenBalanceInvoice"].Activate();

            }
            else
            {

                frm_Inv_Invoice frm = new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.ItemOpenBalance);
                // frm.MdiParent = this;
                frm.Show();
            }


        }



        private void معرفةارباحعميلToolStripMenuItem_Click(object sender, EventArgs e)
        {

            //if (DataAccessLayer.UserRoles("CHgainCustomers") == false)
            //{
            //    MessageBox.Show(" ليس لديك صلاحية للعمل علي هذة الشاشة ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //    return;
            //}

        }

        private void قائمةالدخلToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (Application.OpenForms["IncomStatement_frm"] != null)
            {
                Application.OpenForms["IncomStatement_frm"].Activate();

            }
            else
            {

                IncomStatement_frm frm = new IncomStatement_frm();
                frm.MdiParent = this;
                frm.Show();
            }

        }



        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {

            Connection_Form frm = new Connection_Form();
            frm.ShowDialog();

        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            Info_Form frm = new Info_Form();
            frm.ShowDialog();
        }











        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void butSetting_Click(object sender, EventArgs e)
        {

            if (Application.OpenForms["Bukup_Form"] != null)
            {
                Application.OpenForms["Bukup_Form"].Activate();

            }
            else
            {

                Baukup_Form frm = new Baukup_Form();
                frm.Show();
            }
        }





        private void Main_Form_Move(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {

                //notifyIcon1.ShowBalloonTip(1000, "Sales Manger", "Somthing Important Has Com .Clic This To Know More", ToolTipIcon.Info);
            }
        }

        private void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Maximized;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("سوف يتم الخروج من البرنامج : هل تريد الاستمرار", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {

                if (Properties.Settings.Default.BukUPState == "2")
                {
                    DataAccessLayer.paukupautomatic = "OK";
                    Baukup_Form frm = new Baukup_Form();
                    frm.ShowDialog();
                }
                else
                {
                    DataAccessLayer.closeMainform = true;
                    Application.Exit();
                }

            }



        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Maximized;
        }



        private void Main_Form_FormClosing(object sender, FormClosingEventArgs e)
        {

 
            Settings.Default["ApplicationSkinName"] = UserLookAndFeel.Default.SkinName;
            Settings.Default.Save();

            if (Properties.Settings.Default.LastBackUpTime.Year < 1950 || ((Properties.Settings.Default.LastBackUpTime - DateTime.Now).TotalDays > 3))
            {


                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(Properties.Settings.Default.Connection_String);
                if (builder.IntegratedSecurity)
                {
                    if (XtraMessageBox.Show("هل تريد اخذ نسخه احتياطيه ؟", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        new frm_BackupRestore().ShowDialog();
                    }
                }

            }

            try
            {
                Application.ExitThread();
            }
            catch
            {


            }



        }




        

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["ExpensesAdd_frm"] != null)
            {
                Application.OpenForms["ExpensesAdd_frm"].Activate();

            }
            else
            {

                ExpensesAdd_frm frm = new ExpensesAdd_frm();
                frm.MdiParent = this;
                frm.Show();
            }
        }







        private void تقريرمجمعToolStripMenuItem_Click(object sender, EventArgs e)
        {

            RbtGeneralDay_frm frm = new RbtGeneralDay_frm();
            frm.ShowDialog();
        }




        SqlConnection connSQLServer = new SqlConnection();
        public void conn_string()
        {

            var p = Properties.Settings.Default;
            if (p.Mode == false)
            {
                connSQLServer = new SqlConnection("Data Source=" + p.ServerName + ";Initial Catalog=master;Integrated security=true");
            }
            else
            {
                connSQLServer = new SqlConnection("Data Source=" + p.ServerName + ",1433;Network Library=DBMSSOCN;Initial Catalog =master;User ID='" + p.SQLUserName + "' ;Password='" + p.SQLPassword + "'");
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void CHClearAllData_Click(object sender, EventArgs e)
        {
            B_ClearAllData_Form FRM = new B_ClearAllData_Form();
            FRM.ShowDialog(this);
            if (FRM.LoadData == true)
            {
                DataAccessLayer.closeMainform = true;
                this.Close();

                LoginUser_Form frm = new LoginUser_Form();
                frm.Show();
            }

        }



        private void CHCloseData_Form_Click(object sender, EventArgs e)
        {
            CloseData_Form FRM = new CloseData_Form();
            FRM.ShowDialog();
        }


        private void تسويةالمخازنToolStripMenuItem_Click(object sender, EventArgs e)
        {


            if (Application.OpenForms["BillChangeQuantity"] != null)
            {
                Application.OpenForms["BillChangeQuantity"].Activate();

            }
            else
            {

                BillChangeQuantity frm = new BillChangeQuantity();
                frm.MdiParent = this;
                frm.Show();
            }


        }

        private void CHgainItem_Click(object sender, EventArgs e)
        {

        }




        private void CHCustomersPayment_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["CustomersRecived_frm"] != null)
            {
                Application.OpenForms["CustomersRecived_frm"].Activate();

            }
            else
            {

                CustomersRecived_frm frm = new CustomersRecived_frm();
                frm.MdiParent = this;
                frm.Show();
            }

        }

        private void CHPayCustomer_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["CustomersPay_frm"] != null)
            {
                Application.OpenForms["CustomersPay_frm"].Activate();
            }
            else
            {
                CustomersPay_frm frm = new CustomersPay_frm();
                frm.MdiParent = this;
                frm.Show();
            }

        }

        private void SCSuppliersPayment_FormAdd_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["SuppliersRecived_frm"] != null)
            {
                Application.OpenForms["SuppliersRecived_frm"].Activate();
            }
            else
            {
                SuppliersRecived_frm frm = new SuppliersRecived_frm();
                frm.MdiParent = this;
                frm.Show();
            }

        }

        private void unit__Click(object sender, EventArgs e)
        {

            Unit_frm frm = new Unit_frm();
            frm.ShowDialog();
        }

        private void CHSuppliersPayment_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["SuppliersPay_frm"] != null)
            {
                Application.OpenForms["SuppliersPay_frm"].Activate();
            }
            else
            {
                SuppliersPay_frm frm = new SuppliersPay_frm();
                frm.MdiParent = this;
                frm.Show();
            }

        }

        private void انواعالمصروفاتToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["ExpenseType_frm"] != null)
            {
                Application.OpenForms["ExpenseType_frm"].Activate();
            }
            else
            {
                ExpenseType_frm frm = new ExpenseType_frm();
                frm.MdiParent = this;
                frm.Show();
            }


        }

        private void CSRbtCustomer_frm_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["RbtCustomer_frm"] != null)
            {
                Application.OpenForms["RbtCustomer_frm"].WindowState = FormWindowState.Maximized;
                Application.OpenForms["RbtCustomer_frm"].Activate();
            }
            else
            {
                RbtCustomer_frm frm = new RbtCustomer_frm(true );
                //frm.MdiParent = this;
                frm.Show();
            }

        }

        private void chProdecuts_Minim_Store_frm_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["Prodecuts_Minim_Store_frm"] != null)
            {
                Application.OpenForms["Prodecuts_Minim_Store_frm"].Activate();
            }
            else
            {
                Prodecuts_Minim_Store_frm frm = new Prodecuts_Minim_Store_frm();
                frm.MdiParent = this;
                frm.Show();
            }


        }

        private void CSMoney_Prodects_frm_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["Money_Prodects_frm"] != null)
            {
                Application.OpenForms["Money_Prodects_frm"].Activate();
            }
            else
            {
                Money_Prodects_frm frm = new Money_Prodects_frm();
                //frm.MdiParent = this;
                frm.Show();
            }


        }

        private void SCMoney_Customers_frm_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["Money_Customers_frm"] != null)
            {
                Application.OpenForms["Money_Customers_frm"].Activate();
            }
            else
            {
                Money_Customers_frm frm = new Money_Customers_frm();
                frm.MdiParent = this;
                frm.Show();
            }


        }

        private void SCRbtSuppliers_frm_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["RbtCustomer_frm"] != null)
            {
                Application.OpenForms["RbtCustomer_frm"].WindowState = FormWindowState.Maximized;
                Application.OpenForms["RbtCustomer_frm"].Activate();
            }
            else
            {
                RbtCustomer_frm frm = new RbtCustomer_frm(false );
                //frm.MdiParent = this;
                frm.Show();
            }

        }

        private void CHRbtProdecuts_Firest_Store_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["RbtProdecuts_Firest_Store"] != null)
            {
                Application.OpenForms["RbtProdecuts_Firest_Store"].WindowState = FormWindowState.Maximized;
                Application.OpenForms["RbtProdecuts_Firest_Store"].Activate();
            }
            else
            {
                RbtProdecuts_Firest_Store frm = new RbtProdecuts_Firest_Store();
                //frm.MdiParent = this;
                frm.Show();
            }

        }

        private void SCUserPermetion_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["User_Permetion_frm"] != null)
            {
                Application.OpenForms["User_Permetion_frm"].WindowState = FormWindowState.Maximized;
                Application.OpenForms["User_Permetion_frm"].Activate();
            }
            else
            {
                User_Permetion_frm frm = new User_Permetion_frm();
               
                frm.Show();
            }


        }

        private void CHTreasuryMovement_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["TreasuryMovementTrans_Form"] != null)
            {
                // Application.OpenForms["TreasuryMovementTrans_Form"].WindowState = FormWindowState.Maximized;
                Application.OpenForms["TreasuryMovementTrans_Form"].Activate();
            }
            else
            {
                TreasuryMovementTrans_Form frm = new TreasuryMovementTrans_Form();

                frm.Show();
            }

        }

        private void toolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            if (Application.OpenForms["Holiday_frm"] != null)
            {
                Application.OpenForms["Holiday_frm"].Activate();
            }
            else
            {
                Holiday_frm frm = new Holiday_frm();
                
                frm.Show();
            }

        }

        private void CHDiscound_frm_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["Employee_Deduction_frm"] != null)
            {
                Application.OpenForms["Employee_Deduction_frm"].Activate();
            }
            else
            {
                Employee_Deduction_frm frm = new Employee_Deduction_frm();
               
                frm.Show();
            }

        }

        private void CHPartCompany_frm_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["PartCompany_frm"] != null)
            {
                Application.OpenForms["PartCompany_frm"].Activate();
            }
            else
            {
                PartCompany_frm frm = new PartCompany_frm();
                frm.MdiParent = this;
                frm.Show();
            }

        }

        private void CHWorkPartCompany_frm_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["WorkPartCompany_frm"] != null)
            {
                Application.OpenForms["WorkPartCompany_frm"].WindowState = FormWindowState.Maximized;
                Application.OpenForms["WorkPartCompany_frm"].Activate();
            }
            else
            {
                WorkPartCompany_frm frm = new WorkPartCompany_frm();
                frm.MdiParent = this;
                frm.Show();
            }

        }

        private void CHEmployee_frm_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["Employee_frm"] != null)
            {
                Application.OpenForms["Employee_frm"].Activate();
            }
            else
            {
                Employee_frm frm = new Employee_frm();
                frm.MdiParent = this;
                frm.Show();
            }

        }

        private void CHExelCustomer_frm_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["ExelCustomer_frm"] != null)
            {
                Application.OpenForms["ExelCustomer_frm"].WindowState = FormWindowState.Maximized;
                Application.OpenForms["ExelCustomer_frm"].Activate();
            }
            else
            {
                ExelCustomer_frm frm = new ExelCustomer_frm();
                frm.MdiParent = this;
                frm.Show();
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                //DateTime dt = DateTime.Now;
                //labelTime.Text = dt.ToString("HH:MM:ss");
                if (Properties.Settings.Default.BukUPState == "4")
                {
                    DateTime startTime = Convert.ToDateTime(Properties.Settings.Default.myDate);
                    DateTime endTime = DateTime.Now;
                    TimeSpan duration = DateTime.Parse(endTime.ToString()).Subtract(DateTime.Parse(startTime.ToString()));
                    //MessageBox.Show((duration.Hours).ToString());
                    int ddd = duration.Hours - Convert.ToInt32(Properties.Settings.Default.Hower);
                    if (ddd >= int.Parse(Properties.Settings.Default.BakupTime))
                    {
                        //timer1.Stop();

                        Properties.Settings.Default.Hower = (duration.Hours).ToString();
                        Properties.Settings.Default.Save();
                        DataAccessLayer.paukupautomatic = "3";
                        Baukup_Form frm = new Baukup_Form();
                        frm.ShowDialog();
                    }



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CHRbtProdecuts_Action_frm_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["RbtProdecuts_Action_frm"] != null)
            {
                Application.OpenForms["RbtProdecuts_Action_frm"].WindowState = FormWindowState.Maximized;
                Application.OpenForms["RbtProdecuts_Action_frm"].Activate();
            }
            else
            {
                RbtProdecuts_Action_frm frm = new RbtProdecuts_Action_frm();
                //frm.MdiParent = this;
                frm.Show();
            }

        }

        private void chExelSuppliers_frm_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["ExelSuppliers_frm"] != null)
            {
                Application.OpenForms["ExelSuppliers_frm"].WindowState = FormWindowState.Maximized;
                Application.OpenForms["ExelSuppliers_frm"].Activate();
            }
            else
            {
                ExelSuppliers_frm frm = new ExelSuppliers_frm();
                frm.MdiParent = this;
                frm.Show();
            }

        }

        private void CS28_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["Employee_AdditionsType_frm"] != null)
            {
                //Application.OpenForms["Reward_frm"].WindowState = FormWindowState.Maximized;
                Application.OpenForms["Employee_AdditionsType_frm"].Activate();
            }
            else
            {
                Employee_AdditionsType_frm frm = new Employee_AdditionsType_frm();
                
                frm.Show();
            }

        }


        private void CS25_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["EmployeeAttendance_frm"] != null)
            {
                Application.OpenForms["EmployeeAttendance_frm"].Activate();
            }
            else
            {
                EmployeeAttendance_frm frm = new EmployeeAttendance_frm();
               
                frm.Show();
            }


        }

        private void RbtProdecuts_Prise_frm_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["RbtProdecuts_Prise_frm"] != null)
            {
                Application.OpenForms["RbtProdecuts_Prise_frm"].WindowState = FormWindowState.Maximized;
                Application.OpenForms["RbtProdecuts_Prise_frm"].Activate();
            }
            else
            {
                RbtProdecuts_Prise_frm frm = new RbtProdecuts_Prise_frm();
                // frm.MdiParent = this;
                frm.Show();
            }

        }



        private void CS26_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["Employee_GOAttendance_frm"] != null)
            {
                Application.OpenForms["Employee_GOAttendance_frm"].Activate();
            }
            else
            {
                Employee_GOAttendance_frm frm = new Employee_GOAttendance_frm();
               
                frm.Show();
            }


        }



        private void CS29_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["Employee_DeductionType_frm"] != null)
            {
                Application.OpenForms["Employee_DeductionType_frm"].Activate();
            }
            else
            {
                Employee_DeductionType_frm frm = new Employee_DeductionType_frm();
               
                frm.Show();
            }
        }

        private void CS30_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["Employee_Salary_frm"] != null)
            {
                Application.OpenForms["Employee_Salary_frm"].Activate();
            }
            else
            {
                Employee_Salary_frm frm = new Employee_Salary_frm();
               
                frm.Show();
            }

        }

        private void Employee_Additions_frm_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["Employee_Additions_frm"] != null)
            {
                Application.OpenForms["Employee_Additions_frm"].Activate();
            }
            else
            {
                Employee_Additions_frm frm = new Employee_Additions_frm();
               
                frm.Show();
            }

        }

        private void CHRPTEmployeeAttendance_frm_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["RPTEmployeeAttendance_frm"] != null)
            {
                Application.OpenForms["RPTEmployeeAttendance_frm"].Activate();
            }
            else
            {
                RPTEmployeeAttendance_frm frm = new RPTEmployeeAttendance_frm();

                frm.Show();
            }

        }

        private void RPTEmployee_Total_Attendance_frm_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["RPTEmployee_Total_Attendance_frm"] != null)
            {
                Application.OpenForms["RPTEmployee_Total_Attendance_frm"].Activate();
            }
            else
            {
                RPTEmployee_Total_Attendance_frm frm = new RPTEmployee_Total_Attendance_frm();

                frm.Show();
            }

        }

        private void chEmployee_Holiday_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["Employee_Holiday_frm"] != null)
            {
                Application.OpenForms["Employee_Holiday_frm"].Activate();
            }
            else
            {
                Employee_Holiday_frm frm = new Employee_Holiday_frm();
                 
                frm.Show();
            }


        }

        private void RptEmployee_Salary_frm_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["RptEmployee_Salary_frm"] != null)
            {
                Application.OpenForms["RptEmployee_Salary_frm"].WindowState = FormWindowState.Maximized;
                Application.OpenForms["RptEmployee_Salary_frm"].Activate();

            }
            else
            {
                RptEmployee_Salary_frm frm = new RptEmployee_Salary_frm();
                //frm.MdiParent = this;
                frm.Show();
            }


        }

        private void مجموعةاصنافToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["ExelCategory_frm"] != null)
            {
                Application.OpenForms["ExelCategory_frm"].Activate();

            }
            else
            {
                ExelCategory_frm frm = new ExelCategory_frm();
                frm.MdiParent = this;
                frm.Show();
            }

        }

        private void ChangeFont_Click(object sender, EventArgs e)
        {
            try
            {
                FontDialog fd = new FontDialog();
                if (fd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    this.ToolStrip2.Font = fd.Font;
                    Properties.Settings.Default.Font = fd.Font;
                    Properties.Settings.Default.Save();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ChangeBakground_Click(object sender, EventArgs e)
        {
            ChangBakground_frm frm = new ChangBakground_frm();
            frm.ShowDialog(this);
        }



        public void changBakground()
        {

            //if (System.IO.File.Exists(BackGroundImagePath)) 
            //    this.BackgroundImage = Image.FromFile(BackGroundImagePath);
            //else
            //    this.BackgroundImage = Properties.Resources.SalesManger;
            if (System.IO.File.Exists(BackGroundImagePath))
                using (Stream bmpStream = System.IO.File.Open(BackGroundImagePath, System.IO.FileMode.Open))
                {
                    Bitmap bitmap;
                    Image image = Image.FromStream(bmpStream);
                    bitmap = new Bitmap(image);
                    this.BackgroundImage = bitmap;
                }
            else
                this.BackgroundImage = Properties.Resources.SalesManger;
            //try
            //{
            //    if (Properties.Settings.Default.PhotoNumber == 1)
            //    {


            //        this.BackgroundImage = Properties.Resources.SalesManger;

            //    }

            //    else if (Properties.Settings.Default.PhotoNumber == 4)
            //    {
            //        this.BackgroundImage = Properties.Resources.Background_4;
            //    }
            //    else if (Properties.Settings.Default.PhotoNumber == 5)
            //    {
            //        this.BackgroundImage = Properties.Resources.Background_5;
            //    }
            //    else if (Properties.Settings.Default.PhotoNumber == 6)
            //    {
            //        this.BackgroundImage = Properties.Resources.Background_6;
            //    }

            //    else
            //    {
            //        this.BackgroundImage = Properties.Resources.SalesManger;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}



        }

        private void CreateNewCompany_frm_Click(object sender, EventArgs e)
        {
            frm_CreatNewCompany frm = new frm_CreatNewCompany();
            frm.ShowDialog(this);
            //if (frm.Saved == true)
            //{
            //    DataAccessLayer.closeMainform = true;
            //    this.Close();

            //    LoginUser_Form frmLoginUser = new LoginUser_Form();
            //    frmLoginUser.Show();
            //}
        }

        private void enuItem_Click(object sender, EventArgs e)
        {
            ConnectSQLServer_Form frm = new ConnectSQLServer_Form();
            frm.add = "Main";
            frm.ShowDialog(this);

        }

        private void CS_29_Click(object sender, EventArgs e)
        {

        }

        private void OpenNewCompany_frm_Click(object sender, EventArgs e)
        {
            OpenNewCompany_frm frm = new PL.OpenNewCompany_frm();
            frm.ShowDialog(this);
            if (frm.Saved == true)
            {
                DataAccessLayer.closeMainform = true;
                this.Close();

                LoginUser_Form frmLoginUser = new LoginUser_Form();
                frmLoginUser.Show();
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

            try
            {

                Properties.Settings.Default.computerID = BL.ComputerInfo.GetComputerId();
                Properties.Settings.Default.Save();

            }
            catch
            {

            }


        }

        private void CustomersFirst_frm_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["CustomersFirst_frm"] != null)
            {
                Application.OpenForms["CustomersFirst_frm"].Activate();

            }
            else
            {
                CustomersFirst_frm frm = new CustomersFirst_frm();
                frm.MdiParent = this;
                frm.Show();
            }

        }

        private void SC25_Click(object sender, EventArgs e)
        {

            if (Application.OpenForms["SuppliersFirst_frm"] != null)
            {
                Application.OpenForms["SuppliersFirst_frm"].Activate();

            }
            else
            {
                SuppliersFirst_frm frm = new SuppliersFirst_frm();
                frm.MdiParent = this;
                frm.Show();
            }
        }

        private void CS_73_Click(object sender, EventArgs e)
        {
            SettingInvoice_frm FRM = new SettingInvoice_frm();
            FRM.ShowDialog();
        }

        private void صيانةقواعدالبياناتToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MaintenanceDatabase_frm frm = new MaintenanceDatabase_frm();
            frm.ShowDialog();
        }

        private void toolStripMenuItem1_Click_2(object sender, EventArgs e)
        {
            DeviceType_frm frm = new DeviceType_frm();
            frm.ShowDialog();
        }

        private void toolStripMenuItem2_Click_1(object sender, EventArgs e)
        {

            DamagedType_frm frm = new DamagedType_frm();
            frm.ShowDialog();
        }

        private void sasaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["MaintenanceRecived_frm"] != null)
            {
                Application.OpenForms["MaintenanceRecived_frm"].Activate();

            }
            else
            {
                MaintenanceRecived_frm frm = new MaintenanceRecived_frm();
                frm.MdiParent = this;
                frm.Show();
            }

        }

        private void maintenanceReturnfrmToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["MaintenanceReturn_frm"] != null)
            {
                Application.OpenForms["MaintenanceReturn_frm"].Activate();

            }
            else
            {
                MaintenanceReturn_frm frm = new MaintenanceReturn_frm();
                frm.MdiParent = this;
                frm.Show();
            }

        }

        private void PayType_frm_Click(object sender, EventArgs e)
        {
            PayType_frm frm = new PL.PayType_frm();
            frm.ShowDialog();
        }

        private void ReportMaintenance_frm_Click(object sender, EventArgs e)
        {

            ReportMaintenance_frm frm = new PL.ReportMaintenance_frm();
            frm.ShowDialog();
        }

        private void PayType_Trans_frm_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["PayType_Trans_frm"] != null)
            {
                Application.OpenForms["PayType_Trans_frm"].Activate();

            }
            else
            {
                PayType_Trans_frm frm = new PayType_Trans_frm();
                //frm.MdiParent = this;
                frm.Show();
            }

        }

        private void MaintenanceCompeny_Click(object sender, EventArgs e)
        {

            if (Application.OpenForms["MaintenanceCompeny"] != null)
            {
                Application.OpenForms["MaintenanceCompeny"].Activate();

            }
            else
            {
                MaintenanceCompeny_frm frm = new MaintenanceCompeny_frm();
                frm.MdiParent = this;
                frm.Show();
            }


        }



        private void CS61_Click(object sender, EventArgs e)
        {

            if (Application.OpenForms["CashDrawer_frm"] != null)
            {
                Application.OpenForms["CashDrawer_frm"].Activate();

            }
            else
            {
                CashDrawer_frm frm = new CashDrawer_frm();
                frm.MdiParent = this;
                frm.Show();
            }

        }

        private void BillMaintenance_frm_Click(object sender, EventArgs e)
        {
            if (Application.OpenForms["BillMaintenance_frm"] != null)
            {
                Application.OpenForms["BillMaintenance_frm"].Activate();

            }
            else
            {
                BillMaintenance_frm frm = new BillMaintenance_frm();
                frm.MdiParent = this;
                frm.Show();
            }

        }

        private void Currency_frm_Click(object sender, EventArgs e)
        {
            Currency_frm frm = new PL.Currency_frm();
            frm.ShowDialog();
        }
        Store_Prodecut_cls Store_Prodecut_cls = new Store_Prodecut_cls();


        private void NotifacationProdect()
        {
            if (BillSetting_cls.NotificationProdect == false)
            {
                return;
            }
            try
            {

                DataTable dt = Store_Prodecut_cls.Store_Prodecut_MinimNotifcation();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Notifications_UC US = new Notifications_UC();
                    US.Icon = this.Icon;
                    US.pictureBox1.Image = Properties.Resources.Move_by_Trolley_48px;
                    US.Text = dt.Rows[i]["ProdecutName"].ToString();
                    US.label3.Text = " الرصيد : " + dt.Rows[i]["Balence"].ToString() + Environment.NewLine + " المخزن : " + dt.Rows[i]["StoreName"].ToString();

                    US.MdiParent = this;
                    US.Show();

                }
                // lblCount.Text = DGV1.Rows.Count.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);


            }
        }
        TreasuryMovement_cls TreasuryMovement_CLS = new TreasuryMovement_cls();
        private void NotifacationCustomer()
        {
            try
            {

                if (BillSetting_cls.NotificationCustomers == false)
                {
                    return;
                }

                Double BalencyCus = double.Parse(BillSetting_cls.MaxBalance);

                Customers_cls Customers_cls = new Customers_cls();
                DataTable dt_cus = Customers_cls.Search_Customers("");

                for (int i = 0; i < dt_cus.Rows.Count; i++)
                {
                    double balenceDebit = 0;
                    DataTable dt = TreasuryMovement_CLS.Balence_ISCusSupp(dt_cus.Rows[i]["CustomerID"].ToString(), "Cus");



                    balenceDebit = double.Parse(dt.Rows[0]["balence"].ToString());
                    if (BalencyCus <= balenceDebit)
                    {
                        Notifications_UC US = new Notifications_UC();
                        US.Icon = this.Icon;

                        US.pictureBox1.Image = Properties.Resources.customerss;
                        US.Text = dt_cus.Rows[i]["CustomerName"].ToString();
                        US.label3.Text = " الرصيد : " + balenceDebit.ToString() + Environment.NewLine + " رقم الهاتف : " + dt_cus.Rows[i]["Phone"].ToString();

                        US.MdiParent = this;
                        US.Show();
                    }



                    //  AddRowDgv(dt_cus.Rows[i]["CustomerID"].ToString(), dt_cus.Rows[i][""].ToString(), dt_cus.Rows[i]["Address"].ToString(), dt_cus.Rows[i]["Phone"].ToString(), dt.Rows[0]["Debit"].ToString(), dt.Rows[0]["Credit"].ToString(), balenceDebit, balenceCredit);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CustomersTransProdect_Form_Click(object sender, EventArgs e)
        {

            try
            {
                if (Application.OpenForms["CustomersTransProdect_Form"] != null)
                {
                    Application.OpenForms["CustomersTransProdect_Form"].Activate();

                }
                else
                {
                    CustomersTransProdect_Form frm = new CustomersTransProdect_Form();
                    frm.MdiParent = this;
                    frm.Show();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }



        private void طباعةباركودToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //BarcodePrint_frm frm = new BarcodePrint_frm();
            //frm.ShowDialog();
            frm_PrintItemBarcode frm = new frm_PrintItemBarcode();
            frm.ShowDialog();

        }

        private void casherToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {
                if (Application.OpenForms["BillSales2_frm"] != null)
                {
                    Application.OpenForms["BillSales2_frm"].Activate();

                }
                else
                {
                    BillSales2_frm frm = new BillSales2_frm();
                    // frm.MdiParent = this;
                    frm.Show();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void فاتورةعرضسعرToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["BillOfferPricer_frm"] != null)
                {
                    Application.OpenForms["BillOfferPricer_frm"].Activate();

                }
                else
                {
                    BillOfferPricer_frm frm = new BillOfferPricer_frm();
                    //frm.MdiParent = this;
                    frm.Show();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void سندتحويلأموالToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {
                if (Application.OpenForms["MoneyTransfer_frm"] != null)
                {
                    Application.OpenForms["MoneyTransfer_frm"].Activate();

                }
                else
                {
                    MoneyTransfer_frm frm = new MoneyTransfer_frm();
                    frm.MdiParent = this;
                    frm.Show();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void فاتورةطلبالشراءToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (Application.OpenForms["BillPurchaseOrder_frm"] != null)
                {
                    Application.OpenForms["BillPurchaseOrder_frm"].Activate();

                }
                else
                {
                    BillPurchaseOrder_frm frm = new BillPurchaseOrder_frm();
                    //frm.MdiParent = this;
                    frm.Show();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {
                if (Application.OpenForms["RptIncomMany_frm"] != null)
                {
                    Application.OpenForms["RptIncomMany_frm"].Activate();

                }
                else
                {
                    RptIncomMany_frm frm = new RptIncomMany_frm();
                   
                    frm.Show();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void vToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capital_frm frm = new capital_frm();
            frm.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void Username_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            frm_Inv_Invoice frmsale = new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.SalesInvoice);
            frmsale.Show();


        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button12_Click(object sender, EventArgs e)
        {

        }

        private void بحثالاصنافToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new Forms.frm_SearchItems()).ShowDialog();
        }

        private void اضافهقسطToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new Forms.frm_Instalment()).Show();

        }

        private void قائمهالاقساطToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_InstalmentList.Instance.instalmentInstaice = null;
            Forms.frm_InstalmentList.Instance.Show();
        }

        private void سداددفعهقسطToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_PayInstallment.Instance.Show();
            frm_PayInstallment.Instance.LoadItem(null);
        }

        private void عرضالاقساطالمستحقهToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frm_PayInstallmentList(false).Show();
        }

        private void عرضالدفعاتالمسددهToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frm_PayInstallmentList(true).Show();

        }

        private void CS_75_Click(object sender, EventArgs e)
        {

        }

        private void نماذجالباركودToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frm_BarcodeTemplates().ShowDialog();
        }

        private void واردمنصرفخذنهToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frm_DrawerInOut().Show();
        }

        private void اعداداتالقرائهمنالميزانToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frm_SystemSettings().Show();
        }

        private void السائقونToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frm_Drivers().Show();
        }

        private void امرتوصيلجديدToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frm_DeliveryOrder().Show();
        }

        private void اوامرالتوصيلالجاريهToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frm_DeliveryOrderList(false).Show();
        }

        private void اوامرالتوصيلالمنتهيةToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frm_DeliveryOrderList(true).Show();
        }
        string BackGroundImagePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Bystro\\" + "\\background.Jpeg";
        private void تغييرالخلفيهToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (XtraOpenFileDialog dialog = new XtraOpenFileDialog()
            {
                Filter = "Image files(*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png",
                FilterIndex = 1,
            })
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    Bitmap bitmap;

                    using (Stream bmpStream = System.IO.File.Open(dialog.FileName, System.IO.FileMode.Open))
                    {
                        Image image = Image.FromStream(bmpStream);
                        bitmap = new Bitmap(image);
                        if (System.IO.File.Exists(BackGroundImagePath))
                            System.IO.File.Delete(BackGroundImagePath);
                        bitmap.Save(BackGroundImagePath, ImageFormat.Jpeg);
                        this.BackgroundImage = bitmap;
                    }
                }

            }
        }

        private void الالوانToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frm_Inv_Color().Show();
        }

        private void الاحجامToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frm_Inv_Size().Show();

        }

        private void حركهصنفتفصيليToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frm_EditReportFilters(frm_Report.ReportType.ProductMovment).Show();
        }

        private void ارصدهالاصنافToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frm_EditReportFilters(frm_Report.ReportType.ProductBalance).Show();
        }

        private void صافيالربحاوالخسارهToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frm_ProfitAndLoses().Show();
        }

        private void اكثرالاصنافربحاToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frm_ProductsProfit().Show();

        }

        private void صلاحياتالاصنافToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frm_EditReportFilters(frm_Report.ReportType.ProductExpire).Show();

        }

        private void الارباحمنالعملاءToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frm_CustomerProfit().Show();

        }

        private void سندهالكاصنافToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.ItemDamage).Show();
        }

        private void اصنافوصلتاليحدالطلبToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frm_EditReportFilters(frm_Report.ReportType.ProductReachedReorderLevel).Show();

        }

        private void CS_01_Click(object sender, EventArgs e)
        {

        }

        private void tileItem9_ItemClick(object sender, TileItemEventArgs e)
        {
            if (Application.OpenForms["Item_Form"] != null)
            {
                Application.OpenForms["Item_Form"].Activate();

            }
            else
            {
                Prodecut_frm frm = new Prodecut_frm();
                frm.Show();
            }
        }

        private void barButtonItem54_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //DataAccessLayer.paukupautomatic = "no";
            //Baukup_Form frm = new Baukup_Form();
            //frm.ShowDialog();
            new frm_BackupRestore().ShowDialog();
        }

        private void barButtonItem80_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new frm_Drivers().Show();

        }

        private void التوصيلToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void barButtonItem106_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void barButtonItem111_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void barButtonItem99_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            TreasuryMovementTrans_Form frm = new TreasuryMovementTrans_Form();

            frm.Show();
        }

        private void barButtonItem100_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RbtGeneralDay_frm frm = new RbtGeneralDay_frm();
            frm.ShowDialog();
        }

        private void barButtonItem101_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RbtProdecuts_Firest_Store frm = new RbtProdecuts_Firest_Store();
            //frm.MdiParent = this;
            frm.Show();
        }

        private void barButtonItem121_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RbtCustomer_frm frm = new RbtCustomer_frm(true );
            //frm.MdiParent = this;
            frm.Show();
        }

        private void barButtonItem103_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
         
                RbtCustomer_frm frm = new RbtCustomer_frm(false );
                //frm.MdiParent = this;
                frm.Show(); 
        }

        private void tileItem8_ItemClick(object sender, TileItemEventArgs e)
        {
          
                RbtCustomer_frm frm = new RbtCustomer_frm(false );
                //frm.MdiParent = this;
                frm.Show();
             
        }

        private void barButtonItem104_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ReportMaintenance_frm frm = new PL.ReportMaintenance_frm();
            frm.ShowDialog();
        }

        private void barButtonItem120_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RPTEmployeeAttendance_frm frm = new RPTEmployeeAttendance_frm();

            frm.Show();
        }

        private void barButtonItem108_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RPTEmployee_Total_Attendance_frm frm = new RPTEmployee_Total_Attendance_frm();

            frm.Show();
        }

        private void barButtonItem110_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            RptEmployee_Salary_frm frm = new RptEmployee_Salary_frm();
            //frm.MdiParent = this;
            frm.Show();
        }

        private void barButtonItem112_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new frm_EditReportFilters(frm_Report.ReportType.ProductBalance).Show();
        }

        private void barButtonItem115_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new frm_EditReportFilters(frm_Report.ReportType.ProductMovment).Show();

        }

        private void barButtonItem116_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new frm_EditReportFilters(frm_Report.ReportType.ProductExpire).Show();
        }

        private void barButtonItem117_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new frm_EditReportFilters(frm_Report.ReportType.ProductReachedReorderLevel).Show();
        }

        private void barButtonItem118_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ReportExpenses_Form frm = new ReportExpenses_Form();
            frm.ShowDialog();

        }

        private void barButtonItem119_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //BarcodePrint_frm frm = new BarcodePrint_frm();
            //frm.ShowDialog();
            frm_PrintItemBarcode frm = new frm_PrintItemBarcode();
            frm.ShowDialog();
        }

        private void barButtonItem93_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ChangePassword_frm frm = new ChangePassword_frm();
            frm.ShowDialog();
        }

        private void barButtonItem94_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataAccessLayer.closeMainform = true;
            this.Close();

            if (Application.OpenForms["LoginUser_Form"] != null)
            {
                ((LoginUser_Form)Application.OpenForms["LoginUser_Form"]).Show();
                if (Properties.Settings.Default.RemmberPassword == false)
                {
                    ((LoginUser_Form)Application.OpenForms["LoginUser_Form"]).txtUserName.Text = "";
                    ((LoginUser_Form)Application.OpenForms["LoginUser_Form"]).txtPassword.Text = "";
                }

                ((LoginUser_Form)Application.OpenForms["LoginUser_Form"]).txtUserName.Focus();
                DataAccessLayer.closeMainform = false;
            }
        }

        private void barButtonItem95_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Properties.Settings.Default.BukUPState == "2")
            {
                DataAccessLayer.paukupautomatic = "OK";
                Baukup_Form frm = new Baukup_Form();
                frm.ShowDialog();
            }
            else
            {
                DataAccessLayer.closeMainform = true;
                Application.Exit();
            }
        }

        private void barButtonItem44_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["Employee_frm"] != null)
            {
                Application.OpenForms["Employee_frm"].Activate();
            }
            else
            {
                Employee_frm frm = new Employee_frm();
                
                frm.Show();
            }
        }

        private void barButtonItem45_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["WorkPartCompany_frm"] != null)
            {
                Application.OpenForms["WorkPartCompany_frm"].WindowState = FormWindowState.Maximized;
                Application.OpenForms["WorkPartCompany_frm"].Activate();
            }
            else
            {
                WorkPartCompany_frm frm = new WorkPartCompany_frm();
                
                frm.Show();
            }
        }

        private void barButtonItem49_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["PartCompany_frm"] != null)
            {
                Application.OpenForms["PartCompany_frm"].Activate();
            }
            else
            {
                PartCompany_frm frm = new PartCompany_frm();
               
                frm.Show();
            }
        }

        private void barButtonItem85_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["Holiday_frm"] != null)
            {
                Application.OpenForms["Holiday_frm"].Activate();
            }
            else
            {
                Holiday_frm frm = new Holiday_frm();

                frm.Show();
            }

        }

        private void barButtonItem86_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["Employee_AdditionsType_frm"] != null)
            {
                //Application.OpenForms["Reward_frm"].WindowState = FormWindowState.Maximized;
                Application.OpenForms["Employee_AdditionsType_frm"].Activate();
            }
            else
            {
                Employee_AdditionsType_frm frm = new Employee_AdditionsType_frm();

                frm.Show();
            }
        }

        private void barButtonItem87_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["Employee_DeductionType_frm"] != null)
            {
                Application.OpenForms["Employee_DeductionType_frm"].Activate();
            }
            else
            {
                Employee_DeductionType_frm frm = new Employee_DeductionType_frm();

                frm.Show();
            }
        }

        private void barButtonItem47_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["EmployeeAttendance_frm"] != null)
            {
                Application.OpenForms["EmployeeAttendance_frm"].Activate();
            }
            else
            {
                EmployeeAttendance_frm frm = new EmployeeAttendance_frm();

                frm.Show();
            }

        }

        private void barButtonItem48_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["Employee_GOAttendance_frm"] != null)
            {
                Application.OpenForms["Employee_GOAttendance_frm"].Activate();
            }
            else
            {
                Employee_GOAttendance_frm frm = new Employee_GOAttendance_frm();

                frm.Show();
            }
        }

        private void barButtonItem88_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["Employee_Additions_frm"] != null)
            {
                Application.OpenForms["Employee_Additions_frm"].Activate();
            }
            else
            {
                Employee_Additions_frm frm = new Employee_Additions_frm();

                frm.Show();
            }
        }

        private void barButtonItem89_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["Employee_Deduction_frm"] != null)
            {
                Application.OpenForms["Employee_Deduction_frm"].Activate();
            }
            else
            {
                Employee_Deduction_frm frm = new Employee_Deduction_frm();

                frm.Show();
            }
        }

        private void barButtonItem50_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["Employee_Holiday_frm"] != null)
            {
                Application.OpenForms["Employee_Holiday_frm"].Activate();
            }
            else
            {
                Employee_Holiday_frm frm = new Employee_Holiday_frm();

                frm.Show();
            }
        }

        private void barButtonItem46_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["Employee_Salary_frm"] != null)
            {
                Application.OpenForms["Employee_Salary_frm"].Activate();
            }
            else
            {
                Employee_Salary_frm frm = new Employee_Salary_frm();

                frm.Show();
            }
        }

        private void barButtonItem98_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (Application.OpenForms["RptIncomMany_frm"] != null)
                {
                    Application.OpenForms["RptIncomMany_frm"].Activate();

                }
                else
                {
                    RptIncomMany_frm frm = new RptIncomMany_frm();

                    frm.Show();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void barButtonItem97_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            capital_frm frm = new capital_frm();
            frm.ShowDialog();
        }

        private void barButtonItem96_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new frm_ProductsProfit().Show();

        }

        private void barButtonItem28_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new frm_CustomerProfit().Show();
        }

        private void barButtonItem27_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new frm_ProfitAndLoses().Show();
        }

        private void barButtonItem90_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Connection_Form frm = new Connection_Form();
            frm.ShowDialog();
        }

        private void barButtonItem92_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Info_Form frm = new Info_Form();
            frm.ShowDialog();
        }

        private void barButtonItem18_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            if (Application.OpenForms["MaintenanceCompeny"] != null)
            {
                Application.OpenForms["MaintenanceCompeny"].Activate();

            }
            else
            {
                MaintenanceCompeny_frm frm = new MaintenanceCompeny_frm();
                
                frm.Show();
            }
        }

        private void barButtonItem19_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DeviceType_frm frm = new DeviceType_frm();
            frm.ShowDialog();
        }

        private void barButtonItem20_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DamagedType_frm frm = new DamagedType_frm();
            frm.ShowDialog();
        }

        private void barButtonItem6_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (Application.OpenForms["CustomersTrans_Form"] != null)
                {
                    Application.OpenForms["CustomersTrans_Form"].Activate();

                }
                else
                {
                    CustomersTrans_Form frm = new CustomersTrans_Form();
                    
                    frm.Show();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void barButtonItem51_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["MaintenanceRecived_frm"] != null)
            {
                Application.OpenForms["MaintenanceRecived_frm"].Activate();

            }
            else
            {
                MaintenanceRecived_frm frm = new MaintenanceRecived_frm();
               
                frm.Show();
            }
        }

        private void barButtonItem62_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["MaintenanceReturn_frm"] != null)
            {
                Application.OpenForms["MaintenanceReturn_frm"].Activate();

            }
            else
            {
                MaintenanceReturn_frm frm = new MaintenanceReturn_frm();
                
                frm.Show();
            }
        }

        private void barButtonItem53_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["BillMaintenance_frm"] != null)
            {
                Application.OpenForms["BillMaintenance_frm"].Activate();

            }
            else
            {
                BillMaintenance_frm frm = new BillMaintenance_frm();
               
                frm.Show();
            }
        }

        private void barButtonItem13_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms[nameof(frm_Inv_Invoice)] != null)
            {
                Application.OpenForms[nameof(frm_Inv_Invoice)].Activate();

                Application.OpenForms[nameof(frm_Inv_Invoice)].Show();
                Application.OpenForms[nameof(frm_Inv_Invoice)].BringToFront();
            }
            else
            {

                frm_Inv_Invoice frm = new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.SalesInvoice);

                frm.Show();
            }

        }

        private void barButtonItem14_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["frm_SaleReturnInvoice"] != null)
            {
                Application.OpenForms["frm_SalesReturnInvoice"].Activate();

            }
            else
            {
                frm_Inv_Invoice frm = new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.SalesReturn);

                
                frm.Show();
            }
        }

        private void barButtonItem15_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (Application.OpenForms["BillOfferPricer_frm"] != null)
                {
                    Application.OpenForms["BillOfferPricer_frm"].Activate();

                }
                else
                {
                    BillOfferPricer_frm frm = new BillOfferPricer_frm();
                    //frm.MdiParent = this;
                    frm.Show();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void barButtonItem81_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new frm_DeliveryOrder().Show();
        }

        private void barButtonItem82_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new frm_DeliveryOrderList(false).Show();
        }

        private void barButtonItem83_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new frm_DeliveryOrderList(true).Show();
        }

        private void barButtonItem5_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (Application.OpenForms["Customers_Form"] != null)
                {
                    Application.OpenForms["Customers_Form"].Activate();

                }
                else
                {
                    Customers_frm frm = new Customers_frm();

                    frm.Show();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void barButtonItem7_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["CustomersFirst_frm"] != null)
            {
                Application.OpenForms["CustomersFirst_frm"].Activate();

            }
            else
            {
                CustomersFirst_frm frm = new CustomersFirst_frm();
                
                frm.Show();
            }
        }

        private void barButtonItem75_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            if (Application.OpenForms["CustomersRecived_frm"] != null)
            {
                Application.OpenForms["CustomersRecived_frm"].Activate();

            }
            else
            {

                CustomersRecived_frm frm = new CustomersRecived_frm();
                
                frm.Show();
            }
        }

        private void barButtonItem76_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["CustomersPay_frm"] != null)
            {
                Application.OpenForms["CustomersPay_frm"].Activate();
            }
            else
            {
                CustomersPay_frm frm = new CustomersPay_frm();
                
                frm.Show();
            }
        }

        private void barButtonItem8_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (Application.OpenForms["Suppliers_Form"] != null)
                {
                    Application.OpenForms["Suppliers_Form"].Activate();

                }
                else
                {
                    Suppliers_Form frm = new Suppliers_Form();
                 
                    frm.Show();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void barButtonItem9_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["SuppliersTrans_frm"] != null)
            {
                Application.OpenForms["SuppliersTrans_frm"].Activate();

            }
            else
            {
                SuppliersTrans_frm frm = new SuppliersTrans_frm();
               
                frm.Show();
            }
        }

        private void barButtonItem10_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["SuppliersFirst_frm"] != null)
            {
                Application.OpenForms["SuppliersFirst_frm"].Activate();

            }
            else
            {
                SuppliersFirst_frm frm = new SuppliersFirst_frm();
                
                frm.Show();
            }
        }

        private void barButtonItem77_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["SuppliersRecived_frm"] != null)
            {
                Application.OpenForms["SuppliersRecived_frm"].Activate();
            }
            else
            {
                SuppliersRecived_frm frm = new SuppliersRecived_frm();
                frm.Show();
            }
        }

        private void barButtonItem78_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["SuppliersPay_frm"] != null)
            {
                Application.OpenForms["SuppliersPay_frm"].Activate();
            }
            else
            {
                SuppliersPay_frm frm = new SuppliersPay_frm();
               
                frm.Show();
            }
        }

        private void barButtonItem29_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new frm_DrawerInOut().Show();
        }

        private void barButtonItem38_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frm_InstalmentList.Instance.instalmentInstaice = null;
            Forms.frm_InstalmentList.Instance.Show();
        }

        private void barButtonItem39_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            (new Forms.frm_Instalment()).Show();
        }

        private void barButtonItem40_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            frm_PayInstallment.Instance.Show();
            frm_PayInstallment.Instance.LoadItem(null);
        }

        private void barButtonItem41_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new frm_PayInstallmentList(false).Show();
        }

        private void barButtonItem42_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new frm_PayInstallmentList(true).Show();
        }

        private void barButtonItem35_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["ExpenseType_frm"] != null)
            {
                Application.OpenForms["ExpenseType_frm"].Activate();
            }
            else
            {
                ExpenseType_frm frm = new ExpenseType_frm();
                
                frm.Show();
            }
        }

        private void barButtonItem36_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["ExpensesAdd_frm"] != null)
            {
                Application.OpenForms["ExpensesAdd_frm"].Activate();

            }
            else
            {

                ExpensesAdd_frm frm = new ExpensesAdd_frm();
               
                frm.Show();
            }
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["User_Permetion_frm"] != null)
            {
                Application.OpenForms["User_Permetion_frm"].WindowState = FormWindowState.Maximized;
                Application.OpenForms["User_Permetion_frm"].Activate();
            }
            else
            {
                User_Permetion_frm frm = new User_Permetion_frm();

                frm.Show();
            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SettingInvoice_frm FRM = new SettingInvoice_frm();
            FRM.ShowDialog();
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ConnectSQLServer_Form frm = new ConnectSQLServer_Form();
            frm.add = "Main";
            frm.ShowDialog(this);
        }

        private void barButtonItem4_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            CombanyInformatuon_FormAR frm = new CombanyInformatuon_FormAR();
            frm.ShowDialog();
        }

        private void barButtonItem55_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                FontDialog fd = new FontDialog();
                if (fd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    this.ToolStrip2.Font = fd.Font;
                    Properties.Settings.Default.Font = fd.Font;
                    Properties.Settings.Default.Save();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void barButtonItem56_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new frm_SystemSettings().Show();
        }

        private void barButtonItem74_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new frm_BarcodeTemplates().ShowDialog();
        }

        private void barButtonItem16_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["Item_Form"] != null)
            {
                Application.OpenForms["Item_Form"].Activate();

            }
            else
            {
                Prodecut_frm frm = new Prodecut_frm();
                frm.Show();
            }
        }

        private void barButtonItem17_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["Category_frm"] != null)
            {
                Application.OpenForms["Category_frm"].Activate();

            }
            else
            {
                Category_frm frm = new Category_frm();
                frm.Show();
            }
        }

        private void barButtonItem66_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Unit_frm frm = new Unit_frm();
            frm.ShowDialog();
        }

        private void barButtonItem67_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Store_frm frm = new Store_frm();
            frm.ShowDialog();
        }

        private void barButtonItem68_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["frm_OpenBalanceInvoice"] != null)
            {
                Application.OpenForms["frm_OpenBalanceInvoice"].Activate();

            }
            else
            {

                frm_Inv_Invoice frm = new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.ItemOpenBalance);
                // frm.MdiParent = this;
                frm.Show();
            }

        }

        private void barButtonItem69_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.ItemDamage).Show();
        }

        private void barButtonItem70_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["frm_ItemStoreMove"] != null)
            {
                Application.OpenForms["frm_ItemStoreMove"].Activate();

            }
            else
            {

                frm_Inv_Invoice frm = new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.ItemStoreMove);
                frm.Show();
            }
        }

        private void barButtonItem71_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            (new Forms.frm_SearchItems()).ShowDialog();
        }

        private void barButtonItem72_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new frm_Inv_Color().Show();
        }

        private void barButtonItem73_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new frm_Inv_Size().Show();

        }

        private void tileItem7_ItemClick(object sender, TileItemEventArgs e)
        {
            if (Application.OpenForms["User_Permetion_frm"] != null)
            {
                Application.OpenForms["User_Permetion_frm"].WindowState = FormWindowState.Maximized;
                Application.OpenForms["User_Permetion_frm"].Activate();
            }
            else
            {
                User_Permetion_frm frm = new User_Permetion_frm();

                frm.Show();
            }
        }

        private void tileItem10_ItemClick(object sender, TileItemEventArgs e)
        {
            RbtCustomer_frm frm = new RbtCustomer_frm(true );
            //frm.MdiParent = this;
            frm.Show();
        }

        private void tileItem6_ItemClick(object sender, TileItemEventArgs e)
        {
            if (Application.OpenForms[nameof(frm_Inv_Invoice)] != null)
            {
                Application.OpenForms[nameof(frm_Inv_Invoice)].Activate();

                Application.OpenForms[nameof(frm_Inv_Invoice)].Show();
                Application.OpenForms[nameof(frm_Inv_Invoice)].BringToFront();
            }
            else
            {

                frm_Inv_Invoice frm = new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.SalesInvoice);

                frm.Show();
            }
        }

        private void tileItem5_ItemClick(object sender, TileItemEventArgs e)
        {
            if (Application.OpenForms["BillBay_frm"] != null)
            {
                Application.OpenForms["BillBay_frm"].Activate();

            }
            else
            {
                frm_Inv_Invoice frm = new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.PurchaseInvoice);
                
                frm.Show();
            }
        }

        private void tileItem3_ItemClick(object sender, TileItemEventArgs e)
        {
            if (Application.OpenForms["frm_SaleReturnInvoice"] != null)
            {
                Application.OpenForms["frm_SalesReturnInvoice"].Activate();

            }
            else
            {
                frm_Inv_Invoice frm = new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.SalesReturn);


                frm.Show();
            }
        }

        private void tileItem4_ItemClick(object sender, TileItemEventArgs e)
        {
            new frm_ProfitAndLoses().Show();
        }

        private void tileItem2_ItemClick(object sender, TileItemEventArgs e)
        {
            new frm_EditReportFilters(frm_Report.ReportType.ProductReachedReorderLevel).Show();
        }

        private void tileItem1_ItemClick(object sender, TileItemEventArgs e)
        {
            TreasuryMovementTrans_Form frm = new TreasuryMovementTrans_Form();
        }

        private void barButtonItem122_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["BillBay_frm"] != null)
            {
                Application.OpenForms["BillBay_frm"].Activate();

            }
            else
            {
                frm_Inv_Invoice frm = new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.PurchaseInvoice);
               
                frm.Show();
            }
        }

        private void barButtonItem123_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (Application.OpenForms["frm_PurchaseReturnInvoice"] != null)
            {
                Application.OpenForms["frm_PurchaseReturnInvoice"].Activate();

            }
            else
            {
                frm_Inv_Invoice frm = new frm_Inv_Invoice(Clases.MasterClass.InvoiceType.PurchaseReturn);

                frm.Show();
            }
        }

        private void barButtonItem124_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (Application.OpenForms["BillPurchaseOrder_frm"] != null)
                {
                    Application.OpenForms["BillPurchaseOrder_frm"].Activate();

                }
                else
                {
                    BillPurchaseOrder_frm frm = new BillPurchaseOrder_frm();
                    //frm.MdiParent = this;
                    frm.Show();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tileItem14_ItemClick(object sender, TileItemEventArgs e)
        {
            (new Forms.frm_SearchItems()).ShowDialog();
        }

        private void tileItem13_ItemClick(object sender, TileItemEventArgs e)
        {
            Connection_Form frm = new Connection_Form();
            frm.ShowDialog();
        }

        private void barStaticItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }
 

        private void barButtonItem101_ItemClick_2(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (FontDialog dialog = new FontDialog())
            {
                dialog.Font = DevExpress.XtraEditors.WindowsFormsSettings.DefaultFont;

                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    DevExpress.XtraEditors.WindowsFormsSettings.DefaultFont = dialog.Font;
                    Settings.Default.SystemFont = dialog.Font;
                    Settings.Default.Save();
                }
            }
        }
    }
    
}
