﻿using ByStro.RPT;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.UserDesigner;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class SettingInvoice_frm : Form
    {
        public SettingInvoice_frm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                StoreSearch_frm frm = new StoreSearch_frm();
                frm.ShowDialog();
                if (frm.loaddata == true)
                {
                    txtStoreName.BackColor = Color.WhiteSmoke;
                    txtStoreID.Text = frm.DGV1.CurrentRow.Cells["StoreID"].Value.ToString();
                    txtStoreName.Text = frm.DGV1.CurrentRow.Cells["StoreName"].Value.ToString();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        SettingInvoice_cls cls = new SettingInvoice_cls();
        private void SettingInvoice_frm_Load(object sender, EventArgs e)
        {
            simpleButton1.Tag = nameof(rpt_SalesInvoice);
            simpleButton2.Tag = nameof(rpt_Instalment );

            try
            {
                ERB_Setting.SettingToolStrip(toolStrip1);
                ERB_Setting.SettingForm(this);
                DataTable dt = cls.Details_Setting();
                if (dt.Rows.Count > 0)
                {
                    DataRow Dr = dt.Rows[0];
                    chUseStoreDefault.Checked = (bool)Dr["UseStoreDefault"];

                    txtStoreID.Text = Dr["StoreID"].ToString();
                    if ((bool)Dr["UseStoreDefault"] == true)
                    {
                        Store_cls Store_cls = new Store_cls();
                        DataTable dt_store = Store_cls.Details_Stores(int.Parse(Dr["StoreID"].ToString()));
                        if (dt_store.Rows.Count > 0)
                        {
                            txtStoreID.Text = dt_store.Rows[0]["StoreID"].ToString();
                            txtStoreName.Text = dt_store.Rows[0]["StoreName"].ToString();
                        }
                    }


                    chUseCustomerDefault.Checked = (bool)Dr["UseCustomerDefault"];
                    txtCustomerID.Text = Dr["CustomerID"].ToString();
                    if ((bool)Dr["UseCustomerDefault"] == true)
                    {
                        Customers_cls Customers_cls = new Customers_cls();
                        DataTable dt_cus = Customers_cls.Details_Customers(Dr["CustomerID"].ToString());
                        if (dt_cus.Rows.Count > 0)
                        {
                            txtCustomerID.Text = dt_cus.Rows[0]["CustomerID"].ToString();
                            txtCustomerName.Text = dt_cus.Rows[0]["CustomerName"].ToString();
                        }
                    }


                    //Category
                    chCategory.Checked = (bool)Dr["UseCategory"];
                    //txtCategoryId.Text = Dr["CategoryId"].ToString();
                    if ((bool)Dr["UseCategory"] == true)
                    {
                        Category_cls Category_cls = new Category_cls();
                        DataTable dt_category = Category_cls.Details_Category(Dr["CategoryId"].ToString());
                        if (dt_category.Rows.Count > 0)
                        {
                            txtCategoryId.Text = dt_category.Rows[0]["CategoryID"].ToString();
                            txtCategoryName.Text = dt_category.Rows[0]["CategoryName"].ToString();
                        }
                    }


                    //Unit
                    checkBox2.Checked = (bool)Dr["UseUnit"];
                    //txtCategoryId.Text = Dr["CategoryId"].ToString();
                    if ((bool)Dr["UseUnit"] == true)
                    {
                        Unit_cls Unit_cls = new Unit_cls();
                        DataTable dt_unit = Unit_cls.Details_Unit(int.Parse( Dr["UnitId"].ToString()));
                        if (dt_unit.Rows.Count > 0)
                        {
                            txtUnitId.Text = dt_unit.Rows[0]["UnitID"].ToString();
                            txtUnitName.Text = dt_unit.Rows[0]["UnitName"].ToString();
                        }
                    }




                    chUsingFastInput.Checked = (bool)Dr["UsingFastInput"];
                    chShowMessageQty.Checked = (bool)Dr["ShowMessageQty"];
                    chShowMessageSave.Checked = (bool)Dr["ShowMessageSave"];
                    if (!string.IsNullOrEmpty(Dr["kindPay"].ToString()))
                    {
                        if (Dr["kindPay"].ToString() == "0")
                        {
                            rbkindPay.Checked = true;
                        }
                        else
                        {
                            radioButton2.Checked = true;
                        }
                    }



                    if (!string.IsNullOrEmpty(Dr["UseVat"].ToString()))
                    {
                        chUseVat.Checked = Convert.ToBoolean(Dr["UseVat"].ToString());
                    }

                    if (!string.IsNullOrEmpty(Dr["Vat"].ToString()))
                    {
                        txtVat.Text = Dr["Vat"].ToString();
                    }

                    if (!string.IsNullOrEmpty(Dr["Vat"].ToString()))
                    {
                        txtVat.Text = Dr["Vat"].ToString();
                    }

                    if (!string.IsNullOrEmpty(Dr["PrintSize"].ToString()))
                    {

                        if (Dr["PrintSize"].ToString() == "1")
                        {
                            RBPrintSize1.Checked = true;
                        }
                        else if (Dr["PrintSize"].ToString() == "2")
                        {
                            RBPrintSize2.Checked = true;
                        }
                        else if (Dr["PrintSize"].ToString() == "3")
                        {
                            RBPrintSize3.Checked = true;
                        }
                    }


                    chUseCrrencyDefault.Checked = (bool)Dr["UseCrrencyDefault"];
                    if (!string.IsNullOrEmpty(Dr["CurrencyID"].ToString()))
                    {
                        Currency_cls Currencycls = new Currency_cls();
                        DataTable dtCurrency = Currencycls.Details_Currency(Dr["CurrencyID"].ToString());
                        if (dtCurrency.Rows.Count > 0)
                        {
                            txtCurrencyID.Text = dtCurrency.Rows[0]["CurrencyID"].ToString();
                            txtCurrencyName.Text = dtCurrency.Rows[0]["CurrencyName"].ToString();

                        }
                    }



                    chNotificationProdect.Checked = (bool)Dr["NotificationProdect"];
                    chNotificationCustomers.Checked = (bool)Dr["NotificationCustomers"];
                    txtMaxBalance.Text = Dr["MaxBalance"].ToString();


                    chUsekindPay.Checked = (bool)Dr["UsekindPay"];
                    if (!string.IsNullOrEmpty(Dr["PayTypeID"].ToString()))
                    {
                        PayType_cls PayType_cls = new PayType_cls();
                        DataTable dtPayType = PayType_cls.Details_PayType(Dr["PayTypeID"].ToString());
                        if (dtPayType.Rows.Count > 0)
                        {
                            txtPayTypeID.Text = dtPayType.Rows[0]["PayTypeID"].ToString();
                            txtPayTypeName.Text = dtPayType.Rows[0]["PayTypeName"].ToString();

                        }
                    }




                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                CustomersSearch_frm frm = new CustomersSearch_frm();
                frm.ShowDialog();
                if (frm.loaddata == true)
                {
                    txtCustomerName.BackColor = Color.WhiteSmoke;
                    txtCustomerID.Text = frm.DGV1.CurrentRow.Cells["CustomerID"].Value.ToString();
                    txtCustomerName.Text = frm.DGV1.CurrentRow.Cells["CustomerName"].Value.ToString();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (chUseStoreDefault.Checked)
                {
                    if (string.IsNullOrEmpty(txtStoreID.Text))
                    {
                        MessageBox.Show("يرجي تحديد المخزن الافتراضي", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                if (chUseCustomerDefault.Checked)
                {
                    if (string.IsNullOrEmpty(txtCustomerID.Text))
                    {
                        MessageBox.Show("يرجي تحديد العميل الافتراضي", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }

                if (chUseCustomerDefault.Checked)
                {
                    if (string.IsNullOrEmpty(txtCustomerID.Text))
                    {
                        MessageBox.Show("يرجي تحديد العميل الافتراضي", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }

                if (chUseVat.Checked)
                {
                    if (string.IsNullOrEmpty(txtVat.Text))
                    {
                        MessageBox.Show("يرجي ادخال ضريبة القيمة المضافة", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }


                if (chCategory.Checked)
                {
                    if (string.IsNullOrEmpty(txtCategoryId.Text))
                    {
                        MessageBox.Show("يرجي تحديد المجموعة الافتراضي", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }

                if (checkBox2.Checked)
                {
                    if (string.IsNullOrEmpty(txtUnitId.Text))
                    {
                        MessageBox.Show("يرجي تحديد الوحدة الافتراضي", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }

                int PrintSize = 1;
                if (RBPrintSize1.Checked == true)
                {
                    PrintSize = 1;
                }
                else if (RBPrintSize2.Checked == true)
                {
                    PrintSize = 2;
                }
                else if (RBPrintSize3.Checked == true)
                {
                    PrintSize = 3;
                }


                string kindPay;
                if (rbkindPay.Checked)
                {
                    kindPay = "0";
                }
                else
                {
                    kindPay = "1";
                }

                if (chUseCrrencyDefault.Checked)
                {
                    if (string.IsNullOrEmpty(txtCurrencyID.Text))
                    {
                        MessageBox.Show("يرجي تحديد العملة الافتراضي", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }

                if (chNotificationCustomers.Checked)
                {
                    if (string.IsNullOrEmpty(txtMaxBalance.Text))
                    {
                        MessageBox.Show("يرجي ادخال الحد الاقصي للدين", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        tabControl1.SelectedIndex = 3;
                        txtMaxBalance.Focus();
                        return;
                    }
                }


                if (chUsekindPay.Checked)
                {
                    if (string.IsNullOrEmpty(txtPayTypeID.Text))
                    {
                        MessageBox.Show("يرجي ادخال نوع الدفع الافتراضي", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        tabControl1.SelectedIndex = 2;
                        button4.Focus();
                        return;
                    }
                }



                DataTable dt = cls.Details_Setting();
                if (dt.Rows.Count == 0)
                {
                    cls.InsertSetting(1, chUseStoreDefault.Checked, txtStoreID.Text, chUseCustomerDefault.Checked, txtCustomerID.Text, chUsingFastInput.Checked, chShowMessageQty.Checked, chShowMessageSave.Checked, kindPay, chUseVat.Checked, txtVat.Text, PrintSize, chUseCrrencyDefault.Checked, txtCurrencyID.Text, chNotificationProdect.Checked, chNotificationCustomers.Checked, txtMaxBalance.Text, chUsekindPay.Checked, txtPayTypeID.Text, chCategory.Checked, txtCategoryId.Text, checkBox2.Checked, txtUnitId.Text);
                }
                else
                {
                    cls.UpdateSetting(1, chUseStoreDefault.Checked, txtStoreID.Text, chUseCustomerDefault.Checked, txtCustomerID.Text, chUsingFastInput.Checked, chShowMessageQty.Checked, chShowMessageSave.Checked, kindPay, chUseVat.Checked, txtVat.Text, PrintSize, chUseCrrencyDefault.Checked, txtCurrencyID.Text, chNotificationProdect.Checked, chNotificationCustomers.Checked, txtMaxBalance.Text, chUsekindPay.Checked, txtPayTypeID.Text, chCategory.Checked, txtCategoryId.Text, checkBox2.Checked, txtUnitId.Text);

                }

                DataTable dt_Setting = cls.Details_Setting();
                BillSetting_cls.BillSetting(int.Parse(dt_Setting.Rows[0]["SettingID"].ToString()), (bool)dt_Setting.Rows[0]["UseStoreDefault"], dt_Setting.Rows[0]["StoreID"].ToString(), (bool)dt_Setting.Rows[0]["UseCustomerDefault"], dt_Setting.Rows[0]["CustomerID"].ToString(), (bool)dt_Setting.Rows[0]["UsingFastInput"], (bool)dt_Setting.Rows[0]["ShowMessageQty"], (bool)dt_Setting.Rows[0]["ShowMessageSave"], int.Parse(dt_Setting.Rows[0]["kindPay"].ToString()), (bool)dt_Setting.Rows[0]["UseVat"], dt_Setting.Rows[0]["Vat"].ToString(), PrintSize, (bool)dt_Setting.Rows[0]["UseCrrencyDefault"], dt_Setting.Rows[0]["CurrencyID"].ToString(), (bool)dt_Setting.Rows[0]["NotificationProdect"], (bool)dt_Setting.Rows[0]["NotificationCustomers"], dt_Setting.Rows[0]["MaxBalance"].ToString(), (bool)dt_Setting.Rows[0]["UsekindPay"], dt_Setting.Rows[0]["PayTypeID"].ToString(), (bool)dt_Setting.Rows[0]["UseCategory"], dt_Setting.Rows[0]["CategoryId"].ToString(), (bool)dt_Setting.Rows[0]["UseUnit"], dt_Setting.Rows[0]["UnitId"].ToString());


                MessageBox.Show("تم حفظ الاعدادات بنجاح", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void chUseStoreDefault_CheckedChanged(object sender, EventArgs e)
        {
            if (chUseStoreDefault.Checked)
            {
                button1.Enabled = true;
            }
            else
            {
                button1.Enabled = false;

            }
        }

        private void chUseCustomerDefault_CheckedChanged(object sender, EventArgs e)
        {
            if (chUseCustomerDefault.Checked)
            {
                button2.Enabled = true;
            }
            else
            {
                button2.Enabled = false;

            }
        }

        private void txtVat_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void SettingInvoice_frm_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F2)
                {
                    btnSave_Click(null, null);
                }

                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }

            }
            catch
            {
                return;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                CurrencySearch_frm frm = new CurrencySearch_frm();
                frm.ShowDialog();
                if (frm.loaddata == true)
                {
                    txtCurrencyID.Text = frm.DGV1.CurrentRow.Cells["CurrencyID"].Value.ToString();
                    txtCurrencyName.Text = frm.DGV1.CurrentRow.Cells["CurrencyName"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void chUseCrrencyDefault_CheckedChanged(object sender, EventArgs e)
        {
            if (chUseCrrencyDefault.Checked == true)
            {
                button3.Enabled = true;
            }
            else
            {
                button3.Enabled = false;
            }
        }

        private void txtMaxBalance_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void chUsekindPay_CheckedChanged(object sender, EventArgs e)
        {
            if (chUsekindPay.Checked == true)
            {
                button4.Enabled = true;
            }
            else
            {
                button4.Enabled = false;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                PayTypeSearch_frm frm = new PayTypeSearch_frm();
                frm.ShowDialog();
                if (frm.loaddata == true)
                {
                    txtPayTypeID.Text = frm.DGV1.CurrentRow.Cells["PayTypeID"].Value.ToString();
                    txtPayTypeName.Text = frm.DGV1.CurrentRow.Cells["PayTypeName"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void chCategory_CheckedChanged(object sender, EventArgs e)
        {
            if (chCategory.Checked)
            {
                btnCategory.Enabled = true;
            }
            else
            {
                btnCategory.Enabled = false;

            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
            {
                btnUnit.Enabled = true;
            }
            else
            {
                btnUnit.Enabled = false;

            }
        }

        private void btnCategory_Click(object sender, EventArgs e)
        {
            try
            {
                CategorySearch_frm frm = new CategorySearch_frm();
                frm.ShowDialog();
                if (frm.loaddata == true)
                {
                    txtCategoryName.BackColor = Color.WhiteSmoke;
                    txtCategoryId.Text = frm.DGV1.CurrentRow.Cells["CategoryID"].Value.ToString();
                    txtCategoryName.Text = frm.DGV1.CurrentRow.Cells["CategoryName"].Value.ToString();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnUnit_Click(object sender, EventArgs e)
        {
            try
            {
                Unit1Search_Form frm = new Unit1Search_Form();
                frm.ShowDialog();
                if (frm.loaddata == true)
                {
                    txtUnitName.BackColor = Color.WhiteSmoke;
                    txtUnitId.Text = frm.DGV1.CurrentRow.Cells["UnitID"].Value.ToString();
                    txtUnitName.Text = frm.DGV1.CurrentRow.Cells["UnitName"].Value.ToString();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        XRDesignMdiController mdiController; 
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            rpt_Master rpt = null  ;
            var btn = sender as SimpleButton;
            switch (btn.Tag .ToString())
            {
                case nameof(rpt_SalesInvoice):
                    rpt = new RPT.rpt_Inv_Invoice ();break;
                case nameof(rpt_Instalment):
                    rpt = new RPT.rpt_Instalment();break;


                default:
                    return;
            }
            
            rpt.LoadTemplete();
            if (System.IO.File.Exists(rpt_Master. ReportPath + "\\" + rpt.Name + ".xml")) 
                rpt.LoadLayout(rpt_Master.ReportPath + "\\" + rpt.Name + ".xml");
             
           
            //  rpt.ShowDesigner();
            XRDesignForm form = new XRDesignForm();
            mdiController = form.DesignMdiController;

            // Handle the DesignPanelLoaded event of the MDI controller,
            // to override the SaveCommandHandler for every loaded report.
            mdiController.DesignPanelLoaded +=
                new DesignerLoadedEventHandler(mdiController_DesignPanelLoaded);

            // Open an empty report in the form.
            mdiController.OpenReport(rpt);

            // Show the form.
            form.ShowDialog();
            if (mdiController.ActiveDesignPanel != null)
            {
                mdiController.ActiveDesignPanel.CloseReport();
            }
        }

        void mdiController_DesignPanelLoaded(object sender, DesignerLoadedEventArgs e)
        {
            XRDesignPanel panel = (XRDesignPanel)sender;
            panel.AddCommandHandler(new SaveCommandHandler(panel));
        }
        public class SaveCommandHandler : DevExpress.XtraReports.UserDesigner.ICommandHandler
        {
            XRDesignPanel panel;

            public SaveCommandHandler(XRDesignPanel panel)
            {
                this.panel = panel;
            }

            public void HandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command,
                object[] args)
            {
                // Save the report.
                Save();
            }

            public bool CanHandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command,
                ref bool useNextHandler)
            {
                useNextHandler = !(command == ReportCommand.SaveFile ||
                                   command == ReportCommand.SaveFileAs);
                return !useNextHandler;
            }

            void Save()
            {
                // Write your custom saving here.
                // ... // For instance:
                panel.Report.Name = panel.Report.GetType().Name;
                string Path = rpt_Master.ReportPath + "\\" + panel.Report.Name + ".xml";
                System.IO.Directory.CreateDirectory(rpt_Master.ReportPath);
                panel.Report.SaveLayout(Path);
                // Prevent the "Report has been changed" dialog from being shown.
                panel.ReportState = ReportState.Saved;
            }
        }

    }
}
