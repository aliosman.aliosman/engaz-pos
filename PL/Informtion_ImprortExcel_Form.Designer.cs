﻿namespace ByStro.PL
{
    partial class Informtion_ImprortExcel_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Informtion_ImprortExcel_Form));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.checkBox34 = new System.Windows.Forms.CheckBox();
            this.checkBox35 = new System.Windows.Forms.CheckBox();
            this.checkBox36 = new System.Windows.Forms.CheckBox();
            this.checkBox37 = new System.Windows.Forms.CheckBox();
            this.checkBox39 = new System.Windows.Forms.CheckBox();
            this.checkBox40 = new System.Windows.Forms.CheckBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBox25 = new System.Windows.Forms.CheckBox();
            this.checkBox26 = new System.Windows.Forms.CheckBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.button4 = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox17 = new System.Windows.Forms.CheckBox();
            this.checkBox18 = new System.Windows.Forms.CheckBox();
            this.checkBox21 = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(28, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(819, 410);
            this.panel1.TabIndex = 25;
            this.panel1.Visible = false;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.checkBox21);
            this.panel5.Controls.Add(this.checkBox18);
            this.panel5.Controls.Add(this.checkBox7);
            this.panel5.Controls.Add(this.checkBox10);
            this.panel5.Controls.Add(this.checkBox11);
            this.panel5.Controls.Add(this.checkBox17);
            this.panel5.Controls.Add(this.checkBox34);
            this.panel5.Controls.Add(this.checkBox35);
            this.panel5.Controls.Add(this.checkBox36);
            this.panel5.Controls.Add(this.checkBox37);
            this.panel5.Controls.Add(this.checkBox39);
            this.panel5.Controls.Add(this.checkBox40);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Location = new System.Drawing.Point(540, 68);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(255, 295);
            this.panel5.TabIndex = 25;
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(253, 24);
            this.label6.TabIndex = 16;
            this.label6.Text = "الاصناف";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // checkBox34
            // 
            this.checkBox34.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox34.AutoSize = true;
            this.checkBox34.Checked = true;
            this.checkBox34.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.checkBox34.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox34.ForeColor = System.Drawing.Color.Blue;
            this.checkBox34.Location = new System.Drawing.Point(163, 25);
            this.checkBox34.Name = "checkBox34";
            this.checkBox34.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox34.Size = new System.Drawing.Size(84, 21);
            this.checkBox34.TabIndex = 0;
            this.checkBox34.Text = "اسم الصنف";
            this.checkBox34.UseVisualStyleBackColor = true;
            // 
            // checkBox35
            // 
            this.checkBox35.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox35.AutoSize = true;
            this.checkBox35.Checked = true;
            this.checkBox35.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.checkBox35.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox35.Location = new System.Drawing.Point(162, 47);
            this.checkBox35.Name = "checkBox35";
            this.checkBox35.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox35.Size = new System.Drawing.Size(85, 21);
            this.checkBox35.TabIndex = 10;
            this.checkBox35.Text = "نوع الصنف";
            this.checkBox35.UseVisualStyleBackColor = true;
            // 
            // checkBox36
            // 
            this.checkBox36.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox36.AutoSize = true;
            this.checkBox36.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox36.Location = new System.Drawing.Point(181, 113);
            this.checkBox36.Name = "checkBox36";
            this.checkBox36.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox36.Size = new System.Drawing.Size(66, 21);
            this.checkBox36.TabIndex = 1;
            this.checkBox36.Text = "الباركود";
            this.checkBox36.UseVisualStyleBackColor = true;
            // 
            // checkBox37
            // 
            this.checkBox37.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox37.AutoSize = true;
            this.checkBox37.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox37.Location = new System.Drawing.Point(141, 91);
            this.checkBox37.Name = "checkBox37";
            this.checkBox37.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox37.Size = new System.Drawing.Size(106, 21);
            this.checkBox37.TabIndex = 3;
            this.checkBox37.Text = "الشركة المصنعة";
            this.checkBox37.UseVisualStyleBackColor = true;
            // 
            // checkBox39
            // 
            this.checkBox39.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox39.AutoSize = true;
            this.checkBox39.Checked = true;
            this.checkBox39.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.checkBox39.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox39.Location = new System.Drawing.Point(152, 135);
            this.checkBox39.Name = "checkBox39";
            this.checkBox39.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox39.Size = new System.Drawing.Size(95, 21);
            this.checkBox39.TabIndex = 4;
            this.checkBox39.Text = "الوحدة الاولي";
            this.checkBox39.UseVisualStyleBackColor = true;
            // 
            // checkBox40
            // 
            this.checkBox40.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox40.AutoSize = true;
            this.checkBox40.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox40.Location = new System.Drawing.Point(157, 69);
            this.checkBox40.Name = "checkBox40";
            this.checkBox40.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox40.Size = new System.Drawing.Size(90, 21);
            this.checkBox40.TabIndex = 6;
            this.checkBox40.Text = "مكان التواجد";
            this.checkBox40.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.checkBox25);
            this.panel4.Controls.Add(this.checkBox26);
            this.panel4.Location = new System.Drawing.Point(367, 68);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(164, 295);
            this.panel4.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(162, 30);
            this.label5.TabIndex = 16;
            this.label5.Text = "نوع الصنف";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // checkBox25
            // 
            this.checkBox25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox25.AutoSize = true;
            this.checkBox25.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox25.Location = new System.Drawing.Point(77, 54);
            this.checkBox25.Name = "checkBox25";
            this.checkBox25.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox25.Size = new System.Drawing.Size(71, 21);
            this.checkBox25.TabIndex = 9;
            this.checkBox25.Text = "ملاحظات";
            this.checkBox25.UseVisualStyleBackColor = true;
            // 
            // checkBox26
            // 
            this.checkBox26.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox26.AutoSize = true;
            this.checkBox26.Checked = true;
            this.checkBox26.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.checkBox26.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox26.ForeColor = System.Drawing.Color.Blue;
            this.checkBox26.Location = new System.Drawing.Point(41, 33);
            this.checkBox26.Name = "checkBox26";
            this.checkBox26.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox26.Size = new System.Drawing.Size(107, 21);
            this.checkBox26.TabIndex = 0;
            this.checkBox26.Text = "اسم نوع الصنف";
            this.checkBox26.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.checkBox3);
            this.panel2.Controls.Add(this.checkBox4);
            this.panel2.Controls.Add(this.checkBox13);
            this.panel2.Controls.Add(this.checkBox14);
            this.panel2.Controls.Add(this.checkBox15);
            this.panel2.Controls.Add(this.checkBox16);
            this.panel2.Location = new System.Drawing.Point(195, 68);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(162, 295);
            this.panel2.TabIndex = 23;
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Top;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(160, 30);
            this.label8.TabIndex = 16;
            this.label8.Text = "المـــوردين";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // checkBox3
            // 
            this.checkBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox3.AutoSize = true;
            this.checkBox3.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox3.Location = new System.Drawing.Point(75, 125);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox3.Size = new System.Drawing.Size(71, 21);
            this.checkBox3.TabIndex = 9;
            this.checkBox3.Text = "ملاحظات";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox4.AutoSize = true;
            this.checkBox4.Checked = true;
            this.checkBox4.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.checkBox4.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox4.ForeColor = System.Drawing.Color.Blue;
            this.checkBox4.Location = new System.Drawing.Point(65, 33);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox4.Size = new System.Drawing.Size(81, 21);
            this.checkBox4.TabIndex = 0;
            this.checkBox4.Text = "اسم المورد";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox13
            // 
            this.checkBox13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox13.AutoSize = true;
            this.checkBox13.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox13.Location = new System.Drawing.Point(65, 79);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox13.Size = new System.Drawing.Size(81, 21);
            this.checkBox13.TabIndex = 3;
            this.checkBox13.Text = "رقم الهاتف";
            this.checkBox13.UseVisualStyleBackColor = true;
            // 
            // checkBox14
            // 
            this.checkBox14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox14.AutoSize = true;
            this.checkBox14.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox14.Location = new System.Drawing.Point(91, 102);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox14.Size = new System.Drawing.Size(55, 21);
            this.checkBox14.TabIndex = 7;
            this.checkBox14.Text = "الاميل";
            this.checkBox14.UseVisualStyleBackColor = true;
            // 
            // checkBox15
            // 
            this.checkBox15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox15.AutoSize = true;
            this.checkBox15.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox15.Location = new System.Drawing.Point(35, 148);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox15.Size = new System.Drawing.Size(111, 21);
            this.checkBox15.TabIndex = 4;
            this.checkBox15.Text = "الرصيد الافتتاحي";
            this.checkBox15.UseVisualStyleBackColor = true;
            // 
            // checkBox16
            // 
            this.checkBox16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox16.AutoSize = true;
            this.checkBox16.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox16.Location = new System.Drawing.Point(83, 56);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox16.Size = new System.Drawing.Size(63, 21);
            this.checkBox16.TabIndex = 6;
            this.checkBox16.Text = "العنوان";
            this.checkBox16.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.button4.Image = global::ByStro.Properties.Resources.A_20px;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(348, 369);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(137, 36);
            this.button4.TabIndex = 22;
            this.button4.Text = "OK...";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.label9);
            this.panel7.Controls.Add(this.checkBox12);
            this.panel7.Controls.Add(this.checkBox1);
            this.panel7.Controls.Add(this.checkBox2);
            this.panel7.Controls.Add(this.checkBox6);
            this.panel7.Controls.Add(this.checkBox8);
            this.panel7.Controls.Add(this.checkBox5);
            this.panel7.Controls.Add(this.checkBox9);
            this.panel7.Location = new System.Drawing.Point(22, 68);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(162, 295);
            this.panel7.TabIndex = 21;
            // 
            // label9
            // 
            this.label9.Dock = System.Windows.Forms.DockStyle.Top;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(160, 30);
            this.label9.TabIndex = 16;
            this.label9.Text = "العمــــلاء";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // checkBox12
            // 
            this.checkBox12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox12.AutoSize = true;
            this.checkBox12.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox12.Location = new System.Drawing.Point(75, 153);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox12.Size = new System.Drawing.Size(71, 21);
            this.checkBox12.TabIndex = 9;
            this.checkBox12.Text = "ملاحظات";
            this.checkBox12.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.checkBox1.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.ForeColor = System.Drawing.Color.Blue;
            this.checkBox1.Location = new System.Drawing.Point(67, 33);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox1.Size = new System.Drawing.Size(79, 21);
            this.checkBox1.TabIndex = 0;
            this.checkBox1.Text = "اسم العميل";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox2.AutoSize = true;
            this.checkBox2.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox2.Location = new System.Drawing.Point(65, 105);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox2.Size = new System.Drawing.Size(81, 21);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.Text = "رقم هاتف2";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox6
            // 
            this.checkBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox6.AutoSize = true;
            this.checkBox6.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox6.Location = new System.Drawing.Point(65, 81);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox6.Size = new System.Drawing.Size(81, 21);
            this.checkBox6.TabIndex = 3;
            this.checkBox6.Text = "رقم الهاتف";
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // checkBox8
            // 
            this.checkBox8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox8.AutoSize = true;
            this.checkBox8.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox8.Location = new System.Drawing.Point(91, 129);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox8.Size = new System.Drawing.Size(55, 21);
            this.checkBox8.TabIndex = 7;
            this.checkBox8.Text = "الاميل";
            this.checkBox8.UseVisualStyleBackColor = true;
            // 
            // checkBox5
            // 
            this.checkBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox5.AutoSize = true;
            this.checkBox5.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox5.Location = new System.Drawing.Point(35, 177);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox5.Size = new System.Drawing.Size(111, 21);
            this.checkBox5.TabIndex = 4;
            this.checkBox5.Text = "الرصيد الافتتاحي";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // checkBox9
            // 
            this.checkBox9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox9.AutoSize = true;
            this.checkBox9.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox9.Location = new System.Drawing.Point(83, 57);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox9.Size = new System.Drawing.Size(63, 21);
            this.checkBox9.TabIndex = 6;
            this.checkBox9.Text = "العنوان";
            this.checkBox9.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(817, 65);
            this.label3.TabIndex = 16;
            this.label3.Text = "يجب اتباع التعليمات لاتمام العملية بنجاح\r\nيجب اضافة كل الكولم في ملف الاكسل مع مر" +
    "عاه الترتيب في اسماء الكولم .\r\n الكولم الذي يجب ادخاله ملون بلون الازرق وباقي ال" +
    "كولم اختياري يمكنك اضافتة فارغا";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // checkBox7
            // 
            this.checkBox7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox7.AutoSize = true;
            this.checkBox7.Checked = true;
            this.checkBox7.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.checkBox7.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox7.Location = new System.Drawing.Point(162, 179);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox7.Size = new System.Drawing.Size(85, 21);
            this.checkBox7.TabIndex = 17;
            this.checkBox7.Text = "سعر الشراء";
            this.checkBox7.UseVisualStyleBackColor = true;
            // 
            // checkBox10
            // 
            this.checkBox10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox10.AutoSize = true;
            this.checkBox10.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox10.Location = new System.Drawing.Point(153, 201);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox10.Size = new System.Drawing.Size(94, 21);
            this.checkBox10.TabIndex = 18;
            this.checkBox10.Text = "الوحدة الثانية";
            this.checkBox10.UseVisualStyleBackColor = true;
            // 
            // checkBox11
            // 
            this.checkBox11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox11.AutoSize = true;
            this.checkBox11.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox11.Location = new System.Drawing.Point(152, 248);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox11.Size = new System.Drawing.Size(94, 21);
            this.checkBox11.TabIndex = 19;
            this.checkBox11.Text = "الوحدة الثالثة";
            this.checkBox11.UseVisualStyleBackColor = true;
            // 
            // checkBox17
            // 
            this.checkBox17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox17.AutoSize = true;
            this.checkBox17.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox17.Location = new System.Drawing.Point(173, 157);
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox17.Size = new System.Drawing.Size(74, 21);
            this.checkBox17.TabIndex = 20;
            this.checkBox17.Text = "حد الطلب";
            this.checkBox17.UseVisualStyleBackColor = true;
            // 
            // checkBox18
            // 
            this.checkBox18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox18.AutoSize = true;
            this.checkBox18.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox18.Location = new System.Drawing.Point(84, 223);
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox18.Size = new System.Drawing.Size(163, 21);
            this.checkBox18.TabIndex = 21;
            this.checkBox18.Text = "معدل التحويل الوحده الثانية";
            this.checkBox18.UseVisualStyleBackColor = true;
            // 
            // checkBox21
            // 
            this.checkBox21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox21.AutoSize = true;
            this.checkBox21.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox21.Location = new System.Drawing.Point(83, 270);
            this.checkBox21.Name = "checkBox21";
            this.checkBox21.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox21.Size = new System.Drawing.Size(163, 21);
            this.checkBox21.TabIndex = 23;
            this.checkBox21.Text = "معدل التحويل الوحده الثالثة";
            this.checkBox21.UseVisualStyleBackColor = true;
            // 
            // Informtion_ImprortExcel_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1225, 505);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Informtion_ImprortExcel_Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.panel1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox checkBox34;
        private System.Windows.Forms.CheckBox checkBox35;
        private System.Windows.Forms.CheckBox checkBox36;
        private System.Windows.Forms.CheckBox checkBox37;
        private System.Windows.Forms.CheckBox checkBox39;
        private System.Windows.Forms.CheckBox checkBox40;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkBox25;
        private System.Windows.Forms.CheckBox checkBox26;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBox21;
        private System.Windows.Forms.CheckBox checkBox18;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox17;


    }
}