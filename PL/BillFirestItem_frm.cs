﻿using System;
using System.Data;
using System.Drawing;

using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class BillFirestItem_frm : Form
    {
        public BillFirestItem_frm()
        {
            InitializeComponent();
        }
        Prodecut_Firest_cls cls = new Prodecut_Firest_cls();
        Store_Prodecut_cls Store_Prodecut_cls = new Store_Prodecut_cls();
        AVG_cls AVG_cls = new AVG_cls();
        Boolean Recived = true;
        DataTable dtShow;
        private void PureItemToStoreForm_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingToolStrip(toolStrip1);
            ERB_Setting.SettingForm(this);
            ERB_Setting.SettingDGV(DGV1);
            FillProdecut();
            btnNew_Click(null, null);
        }

        public void FillProdecut()
        {
            loadaccount_CLS.ItemsLoadtxt(txtSearch);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                StoreSearch_frm frm = new StoreSearch_frm();
                frm.ShowDialog();
                if (frm.loaddata == true)
                {
                    txtStoreName.BackColor = Color.WhiteSmoke;
                    txtStoreID.Text = frm.DGV1.CurrentRow.Cells["StoreID"].Value.ToString();
                    txtStoreName.Text = frm.DGV1.CurrentRow.Cells["StoreName"].Value.ToString();
                    if (txtProdecutID.Text.Trim() != "" || txtQuantity.Text.Trim() != "" || txtTotalPrice.Text.Trim() != "")
                    {
                        txtTotalPrice.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private void DGV1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {

        }

        DataTable dt_Prodecut = new DataTable();
        Prodecut_cls Prodecut_CLS = new Prodecut_cls();
        public  void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtSearch.Text.Trim() == "")
                {
                    cleartxt();
                    return;
                }


                #region "تحميل الباركود واكود الصنف واسم الصنف في مكانة  السليم"

                //====================================================================================
                try
                {

                   

                    dt_Prodecut = Prodecut_CLS.SearchBill_Prodecuts(txtSearch.Text);

                    String ProdecutName = txtSearch.Text;
                    txtProdecutID.Text = dt_Prodecut.Rows[0]["ProdecutID"].ToString();
                    txtSearch.Text = dt_Prodecut.Rows[0]["ProdecutName"].ToString();


                    combUnit.Items.Clear();

                    if (dt_Prodecut.Rows[0]["FiestUnit"].ToString() != "")
                    {
                        combUnit.Items.Add(dt_Prodecut.Rows[0]["FiestUnit"].ToString());
                    }
                    if (dt_Prodecut.Rows[0]["SecoundUnit"].ToString() != "")
                    {
                        combUnit.Items.Add(dt_Prodecut.Rows[0]["SecoundUnit"].ToString());
                    }
                    if (dt_Prodecut.Rows[0]["ThreeUnit"].ToString() != "")
                    {
                        combUnit.Items.Add(dt_Prodecut.Rows[0]["ThreeUnit"].ToString());
                    }
                    txtQuantity.Text = "1";



                    #region "ملأ الكومبوبوكس بالوحدات"

                    if (dt_Prodecut.Rows[0]["FiestUnitBarcode"].ToString() == ProdecutName)
                    {
                        combUnit.SelectedIndex = 0;

                    }
                    else if (dt_Prodecut.Rows[0]["SecoundUnitBarcode"].ToString() == ProdecutName)
                    {
                        combUnit.SelectedIndex = 1;

                    }
                    else if (dt_Prodecut.Rows[0]["ThreeUnitBarcode"].ToString() == ProdecutName)
                    {
                        combUnit.SelectedIndex = 2;
                    }
                    else
                    {
                        if (dt_Prodecut.Rows[0]["UnitDefoult"].ToString() == "1")
                        {
                            combUnit.SelectedIndex = 0;

                        }
                        else if (dt_Prodecut.Rows[0]["UnitDefoult"].ToString() == "2")
                        {
                            combUnit.SelectedIndex = 1;

                        }
                        else if (dt_Prodecut.Rows[0]["UnitDefoult"].ToString() == "3")
                        {
                            combUnit.SelectedIndex = 2;
                        }
                        else
                        {
                            combUnit.SelectedIndex = 0;
                        }
                    }

                    combUnit.Focus();
                    combUnit.Select();

                    #endregion
                    if (BillSetting_cls.UsingFastInput == true)
                    {

                        txtSearch.Focus();
                    }


                }
                catch
                {
                    MessageBox.Show("الصنف المدخل غير موجود يرجي مراجعة الاصناف", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtSearch.Text = "";
                    txtSearch.Focus();
                }


                //==================================================================================================================

                #endregion

                //SumitemdDGV();
            }

        }

        private void combUnit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtQuantity.Focus();
            }
        }

        private void txtQty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtPrice.Focus();
            }
        }

        private void txtPrise_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AddItem_DGV();
            }
        }





        #region" اضافة الي الداتا قرد فيو   "
        //string AVG_Price = "0";

        public void AddItem_DGV()
        {
            try
            {
                if (txtSearch.Text == "" || txtProdecutID.Text == "")
                {
                    txtSearch.BackColor = Color.Pink;
                    txtSearch.Focus();
                    return;
                }

                if (combUnit.Text == "")
                {
                    combUnit.BackColor = Color.Pink;
                    combUnit.Focus();
                    return;
                }
                if (txtQuantity.Text == "" || Convert.ToDouble(txtQuantity.Text) <= Convert.ToDouble(0))
                {
                    txtQuantity.BackColor = Color.Pink;
                    txtQuantity.Focus();
                    return;
                }


                if (txtPrice.Text == "" || Convert.ToDouble(txtPrice.Text) <= Convert.ToDouble(0))
                {
                    txtPrice.BackColor = Color.Pink;
                    txtPrice.Focus();
                    return;
                }





                if (txtStoreID.Text == "")
                {
                    txtStoreName.BackColor = Color.Pink;
                    button1_Click(null, null);
                    return;
                }


                //=================Search Prodect use in table or no===============================================================================================

                DataTable Dt_StoreSearch = Store_Prodecut_cls.Details_Store_Prodecut(txtProdecutID.Text, txtStoreID.Text);
                if (Dt_StoreSearch.Rows.Count == 0)
                {
                        Store_Prodecut_cls.InsertStore_Prodecut(txtProdecutID.Text, txtStoreID.Text, "0","0","0");
                }

                //================================================================================================================






                DataTable dt_Search = cls.Search_Firist_Details(txtProdecutID.Text, txtUnitFactor.Text, txtStoreID.Text);
                if (dt_Search.Rows.Count > 0)
                {
                    MessageBox.Show("  سبق وتم اضافة الصنف  " + txtSearch.Text + "  في اصناف أول المدة  ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }


                #region Update Price
                Double ItemBayPrice = 0;
                if (combUnit.SelectedIndex == 0)
                {
                    if (dt_Prodecut.Rows[0]["ThreeUnitOperating"].ToString() != "")
                    {
                        ItemBayPrice = Convert.ToDouble(txtPrice.Text) / Convert.ToDouble(dt_Prodecut.Rows[0]["FiestUnitFactor"].ToString());
                    }
                    else if (dt_Prodecut.Rows[0]["SecoundUnitOperating"].ToString() != "")
                    {
                        ItemBayPrice = Convert.ToDouble(txtPrice.Text) / Convert.ToDouble(dt_Prodecut.Rows[0]["SecoundUnitOperating"].ToString());
                    }
                    else
                    {
                        ItemBayPrice = Convert.ToDouble(txtPrice.Text);
                    }
                }
                else if (combUnit.SelectedIndex == 1)
                {
                    //===========================================================================================================================================
                    if (dt_Prodecut.Rows[0]["ThreeUnitOperating"].ToString() != "")
                    {
                        ItemBayPrice = Convert.ToDouble(txtPrice.Text) / Convert.ToDouble(dt_Prodecut.Rows[0]["ThreeUnitOperating"].ToString());
                    }
                    else
                    {
                        ItemBayPrice = Convert.ToDouble(txtPrice.Text);
                    }
                }
                else if (combUnit.SelectedIndex == 2)
                {
                    ItemBayPrice = Convert.ToDouble(txtPrice.Text);
                }
                //MessageBox.Show(ItemBayPrice.ToString());
                Prodecut_CLS.UpdateProdecutBayPrice(txtProdecutID.Text, ItemBayPrice);
                #endregion


                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    if (DGV1.Rows[i].Cells["ProdecutID"].Value.ToString() == txtProdecutID.Text && DGV1.Rows[i].Cells["Unit"].Value.ToString() == combUnit.Text && DGV1.Rows[i].Cells["StoreID"].Value.ToString() == txtStoreID.Text)
                    {
                        if (BillSetting_cls.ShowMessageQty == true)
                        {
                            Double qty = Convert.ToDouble(DGV1.Rows[i].Cells["Quantity"].Value) + Convert.ToDouble(txtQuantity.Text);
                            DGV1.Rows[i].Cells["Quantity"].Value = +qty;

                            Double x = Convert.ToDouble(DGV1.Rows[i].Cells["Quantity"].Value) * Convert.ToDouble(DGV1.Rows[i].Cells["Price"].Value);

                            DGV1.Rows[i].Cells["TotalPrice"].Value = Convert.ToDouble(x);
                            SumDgv();
                            cleartxt();
                            return;

                        }
                        else
                        {
                            if (MessageBox.Show("هذ الصنف سبق وتم اضافتة ضمن الفاتورة  : هل تريد اضافة  " + txtQuantity.Text + "  الي الكمية ", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                Double qty = Convert.ToDouble(DGV1.Rows[i].Cells["Quantity"].Value) + Convert.ToDouble(txtQuantity.Text);
                                DGV1.Rows[i].Cells["Quantity"].Value = +qty;

                                Double x = Convert.ToDouble(DGV1.Rows[i].Cells["Quantity"].Value) * Convert.ToDouble(DGV1.Rows[i].Cells["Price"].Value);

                                // الكل مظروب في قيمة الخصم وناخد القيمة  ونقسمها علي  100
                                DGV1.Rows[i].Cells["TotalPrice"].Value = Convert.ToDouble(x);
                                SumDgv();
                                cleartxt();
                                return;
                            }
                            else
                            {
                                return;
                            }
                        }


                    }
                }





                AddRowDgv(txtProdecutID.Text, txtSearch.Text, combUnit.Text, txtUnitFactor.Text, txtUnitOperating.Text, txtQuantity.Text, txtPrice.Text, txtTotalPrice.Text, txtStoreID.Text, txtStoreName.Text);
                lblCount.Text = DGV1.Rows.Count.ToString();

                SumDgv();
                cleartxt();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        private void SumDgv()
        {
            double sum = 0;
            for (int i = 0; i < DGV1.Rows.Count; i++)
            {
                sum += Convert.ToDouble(DGV1.Rows[i].Cells["TotalPrice"].Value);
            }
            txtTotalInvoice.Text = sum.ToString();

            //   lblSArbic.Text = "          " + DataAccessLeayer.Horofcls.تحويل_الأرقام_الى_حروف(Convert.ToDecimal(sum), 2, "", "", true, true);



        }
        private void AddRowDgv(string ProdecutID, string ProdecutName, string Unit, string UnitFactor, string UnitOperating, string Quantity, string Price, string TotalPrice, string StoreID, string StoreName)
        {
            try
            {
                DGV1.Rows.Add();
                DGV1.ClearSelection();
                DGV1.Rows[DGV1.Rows.Count - 1].Selected = true;
                var lastRows = DGV1.Rows.Count - 1;
                DGV1.Rows[lastRows].Cells["ProdecutID"].Value = ProdecutID;
                DGV1.Rows[lastRows].Cells["ProdecutName"].Value = ProdecutName;
                DGV1.Rows[lastRows].Cells["Unit"].Value = Unit;
                DGV1.Rows[lastRows].Cells["UnitFactor"].Value = UnitFactor;
                DGV1.Rows[lastRows].Cells["UnitOperating"].Value = UnitOperating;
                DGV1.Rows[lastRows].Cells["Quantity"].Value = Quantity;
                DGV1.Rows[lastRows].Cells["Price"].Value = Price;
                DGV1.Rows[lastRows].Cells["TotalPrice"].Value = TotalPrice;
                DGV1.Rows[lastRows].Cells["StoreID"].Value = StoreID;
                DGV1.Rows[lastRows].Cells["StoreName"].Value = StoreName;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }
        private void cleartxt()
        {
            txtProdecutID.Text = "";
            txtSearch.Text = "";
            txtQuantity.Text = "";
            txtPrice.Text = "";
            txtTotalPrice.Text = "";
            combUnit.Items.Clear();
            txtSearch.Focus();

        }

        private void txtQty_TextChanged(object sender, EventArgs e)
        {
            txtQuantity.BackColor = Color.White;
            SumTotal_txt();

        }


        private void SumTotal_txt()
        {
            try
            {



                if (txtQuantity.Text == "")
                {
                    txtQuantity.Focus();
                    return;
                }

                if (txtPrice.Text == "")
                {
                    txtPrice.Focus();
                    return;
                }
                double x = Convert.ToDouble(txtQuantity.Text) * Convert.ToDouble(txtPrice.Text);
                txtTotalPrice.Text = x.ToString();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


        }
        private void txtPrise_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (DGV1.Rows.Count == 0)
                {
                    MessageBox.Show("لا يمكن حفظ فاتورة خالية من الاصناف", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    if (DGV1.Rows[i].Cells["Quantity"].Value.ToString() == "0" || DGV1.Rows[i].Cells["Quantity"].Value.ToString() == "")
                    {
                        MessageBox.Show("من فضلك يرجي مراجعة الكميات المدخلة في الفاتورة  ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        DGV1.Rows[i].Selected = true;
                        return;
                    }
                }


                txtMainID.Text = cls.MaxID_Firist_Main();
                //====Bay_Main===========================================================================================================================================================================================================================================================
                cls.InsertFirist_Main(txtMainID.Text, D1.Value, txtRemarks.Text, txtTotalInvoice.Text, Properties.Settings.Default.UserID);
                //====Bay_Main===========================================================================================================================================================================================================================================================
                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    cls.InsertFirist_Details(txtMainID.Text, DGV1.Rows[i].Cells["ProdecutID"].Value.ToString(), DGV1.Rows[i].Cells["Unit"].Value.ToString(), DGV1.Rows[i].Cells["UnitFactor"].Value.ToString(), DGV1.Rows[i].Cells["UnitOperating"].Value.ToString(), DGV1.Rows[i].Cells["Quantity"].Value.ToString(), DGV1.Rows[i].Cells["Price"].Value.ToString(), DGV1.Rows[i].Cells["TotalPrice"].Value.ToString(), DGV1.Rows[i].Cells["StoreID"].Value.ToString(), Recived);
                    AVG_cls.QuantityNow_Avg(DGV1.Rows[i].Cells["ProdecutID"].Value.ToString(), DGV1.Rows[i].Cells["StoreID"].Value.ToString(), true);
               
                }
                if (BillSetting_cls.ShowMessageSave == false)
                {
                    Mass.Saved();
                }

                btnNew_Click(null, null);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

        
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {

                if (DGV1.Rows.Count == 0)
                {
                    MessageBox.Show("لا يمكن حفظ فاتورة خالية من الاصناف", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    if (DGV1.Rows[i].Cells["Quantity"].Value.ToString() == "0" || DGV1.Rows[i].Cells["Quantity"].Value.ToString() == "")
                    {
                        MessageBox.Show("من فضلك يرجي مراجعة الكميات المدخلة في الفاتورة  ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        DGV1.Rows[i].Selected = true;
                        return;
                    }

                }

                //====UpdateBay_Main========================================================================================================================================================================================================================================================
                cls.UpdateFirist_Main(txtMainID.Text, D1.Value, txtRemarks.Text, txtTotalInvoice.Text);

                //====Delete Quentity ========================================================================================================================================================================================================================================================
                DataTable dt = cls.Details_Firist_Details(txtMainID.Text);

                //====Delete Bay_Details ========================================================================================================================================================================================================================================================

                cls.DeleteFirist_Details(txtMainID.Text);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    AVG_cls.QuantityNow_Avg(dt.Rows[i]["ProdecutID"].ToString(), dt.Rows[i]["StoreID"].ToString(),true);
                }

                //====Insert Bay_Details ========================================================================================================================================================================================================================================================
                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    cls.InsertFirist_Details(txtMainID.Text, DGV1.Rows[i].Cells["ProdecutID"].Value.ToString(), DGV1.Rows[i].Cells["Unit"].Value.ToString(), DGV1.Rows[i].Cells["UnitFactor"].Value.ToString(), DGV1.Rows[i].Cells["UnitOperating"].Value.ToString(), DGV1.Rows[i].Cells["Quantity"].Value.ToString(), DGV1.Rows[i].Cells["Price"].Value.ToString(), DGV1.Rows[i].Cells["TotalPrice"].Value.ToString(), DGV1.Rows[i].Cells["StoreID"].Value.ToString(), Recived);
                    AVG_cls.QuantityNow_Avg(DGV1.Rows[i].Cells["ProdecutID"].Value.ToString(), DGV1.Rows[i].Cells["StoreID"].Value.ToString(),true);
                }
                if (BillSetting_cls.ShowMessageSave == false)
                {
                    Mass.Update();
                }
                btnNew_Click(null, null);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }


            }





        //======================================================================


            #endregion





        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (txtSearch.Text.Trim() == "")
            {
                cleartxt();

            }
        }


        private void combUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (combUnit.SelectedIndex == 0)
            {
                txtUnitFactor.Text = dt_Prodecut.Rows[0]["FiestUnitFactor"].ToString();
                txtUnitOperating.Text = dt_Prodecut.Rows[0]["FiestUnitOperating"].ToString();

            }
            else if (combUnit.SelectedIndex == 1)
            {
                txtUnitFactor.Text = dt_Prodecut.Rows[0]["SecoundUnitFactor"].ToString();
                txtUnitOperating.Text = dt_Prodecut.Rows[0]["SecoundUnitOperating"].ToString();
            }
            else if (combUnit.SelectedIndex == 2)
            {
                txtUnitFactor.Text = dt_Prodecut.Rows[0]["ThreeUnitFactor"].ToString();
                txtUnitOperating.Text = dt_Prodecut.Rows[0]["ThreeUnitOperating"].ToString();
            }

            txtPrice.Text = (Convert.ToDouble(dt_Prodecut.Rows[0]["ProdecutBayPrice"].ToString()) * Convert.ToDouble(txtUnitFactor.Text)).ToString();

            if (BillSetting_cls.UsingFastInput == true)
            {
                AddItem_DGV();
                txtSearch.Focus();
            }
        }






        private void txtPrice_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AddItem_DGV();
            }
        }

        private void txtPrice_TextChanged(object sender, EventArgs e)
        {
            txtPrice.BackColor = Color.White;
            SumTotal_txt();

        }

        private void AllKeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void toolStripSplitButton1_ButtonClick(object sender, EventArgs e)
        {

        }

        private void OrderProudect_frm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DGV1.Rows.Count > 0)
            {
                if (Mass.close_frm() == false)
                {
                    e.Cancel = true;
                }
            }
        }











        private void txtVAT_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AddItem_DGV();
            }
        }







        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            try
            {

                if (DGV1.SelectedRows.Count >= 1)
                {
                    if (Mass.Delete() == true)
                    {
                        foreach (DataGridViewRow r in DGV1.SelectedRows)
                        {
                            DGV1.Rows.Remove(r);
                        }
                        lblCount.Text = DGV1.Rows.Count.ToString();
                        SumDgv();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void DGV1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Double x = Convert.ToDouble(DGV1.CurrentRow.Cells["Quantity"].Value) * Convert.ToDouble(DGV1.CurrentRow.Cells["Price"].Value);
                DGV1.CurrentRow.Cells["TotalPrice"].Value = x.ToString();

                SumDgv();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            try
            {
                if (Mass.Delete() == true)
                {
                    //====Delete Quentity ========================================================================================================================================================================================================================================================
                    DataTable dt = cls.Details_Firist_Details(txtMainID.Text);
     
                    cls.DeleteFirist_Main(txtMainID.Text);
                    cls.DeleteFirist_Details(txtMainID.Text);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        AVG_cls.QuantityNow_Avg(dt.Rows[i]["ProdecutID"].ToString(), dt.Rows[i]["StoreID"].ToString(),true);            
                    }
                    btnNew_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        int pos = 0;
        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                // Use Fast Inpout

                // store defult ===================================================================================
                txtStoreID.Text = "";
                txtStoreName.Text = "";
                if (BillSetting_cls.UseStoreDefult == true)
                {
                    Store_cls Store_cls = new Store_cls();
                    DataTable dt_store = Store_cls.Details_Stores(int.Parse(BillSetting_cls.StoreID));
                    txtStoreID.Text = dt_store.Rows[0]["StoreID"].ToString();
                    txtStoreName.Text = dt_store.Rows[0]["StoreName"].ToString();
                }
      
                //===============================================================================================================================================================
                txtMainID.Text = cls.MaxID_Firist_Main();
                DGV1.Rows.Clear();
                txtRemarks.Text = "";
                txtProdecutID.Text = "";
                txtTotalInvoice.Text = "0";
                lblCount.Text = "0";
                // Load All table
                dtShow = cls.Details_Firist_Main();
                pos = dtShow.Rows.Count;
                ShowData(pos);
                //===============================================================================================================================================================
                btnSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
                cleartxt();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

;
        }



        private void DGV1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                toolStripButton3_Click(null, null);
            }
        }

        private void BillBay_frm_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                else if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                else if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
                else if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion

        }

        private void DGV1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //ChangeUnit_frm frm = new ChangeUnit_frm();
            //frm.ProdectID = DGV1.CurrentRow.Cells["ProdecutID"].Value.ToString();
            //frm.ShowDialog(this);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {

                pos--;
                if (pos >= 0)
                {
                    ShowData(pos);
                }
                else
                {
                    MessageBox.Show("هذا اضغر سند ");

                }
            }
            catch
            {
                return;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                pos++;
                if (pos < dtShow.Rows.Count)
                {
                    ShowData(pos);
                }
                else
                {
                    MessageBox.Show("هذا اكبر سند  ");
                    pos = dtShow.Rows.Count;
                    //ShowData(pos);
                }
            }
            catch
            {
                return;
            }
        }



        public void ShowData(int index)
        {
            try
            {
                GetDataSearch(dtShow.Rows[index]["MainID"].ToString());
            }
            catch
            {
                return;
            }
        }


        private void GetDataSearch(String ID)
        {
            DataTable dt = cls.Details_Firist_Main(ID);
            DataRow Dr = dt.Rows[0];
            txtMainID.Text = Dr["MainID"].ToString();
            D1.Value = Convert.ToDateTime(Dr["MyDate"]);
            txtRemarks.Text = Dr["Remarks"].ToString();
            //=========================================================================================================================================================================
            dt.Clear();
            DGV1.DataSource = null;
            DGV1.Rows.Clear();
            //=========================================================================================================================================================================
            dt = cls.Details_Firist_Details(txtMainID.Text);
            DataRow Dr2 = dt.Rows[0];
            for (int i = 0; i < dt.Rows.Count; i++)
            {

                AddRowDgv(dt.Rows[i]["ProdecutID"].ToString(), dt.Rows[i]["ProdecutName"].ToString(), dt.Rows[i]["Unit"].ToString(), dt.Rows[i]["UnitFactor"].ToString(), dt.Rows[i]["UnitOperating"].ToString(), dt.Rows[i]["Quantity"].ToString(), dt.Rows[i]["Price"].ToString(), dt.Rows[i]["TotalPrice"].ToString(), dt.Rows[i]["StoreID"].ToString(), dt.Rows[i]["StoreName"].ToString());
            }

            lblCount.Text = DGV1.Rows.Count.ToString();
            SumDgv();
            btnSave.Enabled = false;
            btnUpdate.Enabled = true;
            btnDelete.Enabled = true;
        }



  

        private void اعادةتحميلالاصنافToolStripMenuItem_Click(object sender, EventArgs e)
        {
            loadaccount_CLS.ItemsLoadtxt(txtSearch);
        }

    
    }
}
