﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class ViewQuentityStore_frm : Form
    {
        public ViewQuentityStore_frm()
        {
            InitializeComponent();
        }

        private void ViewQuentityStore_frm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Escape)
            {
                this.Close();
            }
        }

        private void ViewQuentityStore_frm_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingDGV(DGV1);
        }
    }
}
