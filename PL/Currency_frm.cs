﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
namespace ByStro.PL
{
    public partial class Currency_frm : Form
    {
        public Currency_frm()
        {
            InitializeComponent();
        }
        Currency_cls cls = new Currency_cls();
        public Boolean LoadData = false;
        private void AccountEndAdd_Form_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingForm(this);
            btnNew_Click(null, null);

        }


        private void AccountEndAdd_Form_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    if (btnDelete.Enabled == true)
                    {
                        btnDelete_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F5)
                {
                    btnGetData_Click(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion

        }

        private void txtAccountName_TextChanged(object sender, EventArgs e)
        {
            txtCurrencyName.BackColor = Color.White;
        }

        private void txtCurrencyVal_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }



        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //"Conditional"
                if (txtCurrencyName.Text.Trim() == "")
                {
                    txtCurrencyName.BackColor = Color.Pink;
                    txtCurrencyName.Focus();
                    return;
                }

                if (txtCurrencyShortcut.Text.Trim() == "")
                {
                    txtCurrencyShortcut.BackColor = Color.Pink;
                    txtCurrencyShortcut.Focus();
                    return;
                }

                if (txtCurrencyRate.Text.Trim() == "")
                {
                    txtCurrencyRate.BackColor = Color.Pink;
                    txtCurrencyRate.Focus();
                    return;
                }
                if (txtCurrencyPartName.Text.Trim() == "")
                {
                    txtCurrencyPartName.BackColor = Color.Pink;
                    txtCurrencyPartName.Focus();
                    return;
                }

                

                                // " Search TextBox "
                                DataTable DtSearch = cls.NameSearch__Currency(txtCurrencyName.Text.Trim());
                if (DtSearch.Rows.Count > 0)
                {
                    MessageBox.Show("الاسم المدخل موجود مسبقا", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtCurrencyName.BackColor = Color.Pink;
                    txtCurrencyName.Focus();
                    return;
                }

                txtCurrencyID.Text = cls.MaxID_Currency();
                cls.Insert_Currency(txtCurrencyID.Text, txtCurrencyName.Text.Trim(), txtCurrencyShortcut.Text, txtCurrencyPartName.Text, txtCurrencyRate.Text, txtRemark.Text);
                btnNew_Click(null, null);
                LoadData = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                //"Conditional"
                if (txtCurrencyName.Text.Trim() == "")
                {
                    txtCurrencyName.BackColor = Color.Pink;
                    txtCurrencyName.Focus();
                    return;
                }

                if (txtCurrencyShortcut.Text.Trim() == "")
                {
                    txtCurrencyShortcut.BackColor = Color.Pink;
                    txtCurrencyShortcut.Focus();
                    return;
                }

                if (txtCurrencyRate.Text.Trim() == "")
                {
                    txtCurrencyRate.BackColor = Color.Pink;
                    txtCurrencyRate.Focus();
                    return;
                }
                if (txtCurrencyPartName.Text.Trim() == "")
                {
                    txtCurrencyPartName.BackColor = Color.Pink;
                    txtCurrencyPartName.Focus();
                    return;
                }



                //"Conditional"
                cls.Update_Currency(txtCurrencyID.Text, txtCurrencyName.Text.Trim(), txtCurrencyShortcut.Text, txtCurrencyPartName.Text, txtCurrencyRate.Text, txtRemark.Text);
                btnNew_Click(null, null);
                LoadData = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {

                // fill combBox combParent

                //=============================================================================================

                txtCurrencyID.Text = cls.MaxID_Currency();
                txtCurrencyName.Text = "";
                txtCurrencyShortcut.Text = "";
                txtCurrencyRate.Text = "";
                txtCurrencyPartName.Text = "";
                txtRemark.Text = "";
                btnSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;

                if (Application.OpenForms["BillSales_frm"] != null)
                {
                   // ((BillSales_frm)Application.OpenForms["BillSales_frm"]).FillPayType();

                }






                txtCurrencyName.Focus();



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {





                #region " Search TextBox "


                #endregion

                if (Mass.Delete() == true)
                {
                    cls.Delete_Currency(txtCurrencyID.Text);
                    btnNew_Click(null, null);
                    LoadData = true;
                }






            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {


            try
            {
                CurrencySearch_frm frm = new CurrencySearch_frm();
                frm.ShowDialog(this);
                if (frm.loaddata == false)
                {
                    return;
                }

                DataTable dt = cls.Details_Currency(frm.DGV1.CurrentRow.Cells["CurrencyID"].Value.ToString());
                DataRow Dr = dt.Rows[0];
                txtCurrencyID.Text = Dr["CurrencyID"].ToString();
                txtCurrencyName.Text = Dr["CurrencyName"].ToString();
                txtCurrencyShortcut.Text = Dr["CurrencyShortcut"].ToString();
                txtCurrencyRate.Text = Dr["CurrencyRate"].ToString();
                txtCurrencyPartName.Text = Dr["CurrencyPartName"].ToString();
                txtRemark.Text = Dr["Remark"].ToString();

                btnSave.Enabled = false;
                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }


        }

        private void txtRemark_TextChanged(object sender, EventArgs e)
        {


        }

        private void txtCurrencyRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void txtCurrencyShortcut_TextChanged(object sender, EventArgs e)
        {
            txtCurrencyShortcut.BackColor = Color.White;

        }

        private void txtCurrencyRate_TextChanged(object sender, EventArgs e)
        {
            txtCurrencyRate.BackColor = Color.White;

        }

        private void txtCurrencyPartName_TextChanged(object sender, EventArgs e)
        {
            txtCurrencyPartName.BackColor = Color.White;

        }
    }
}
