﻿using System;
using System.Data;
using System.Windows.Forms;
namespace ByStro.PL
{
    public partial class UserSearch_frm : Form
    {
        public UserSearch_frm()
        {
            InitializeComponent();
        }
        public Boolean loaddata = false;

        UserPermissions_CLS cls = new UserPermissions_CLS();
        private void AccountEnd_Form_Load(object sender, EventArgs e)
        {
            try
            {
                ERB_Setting.SettingForm(this);
                ERB_Setting.SettingDGV(DGV1);
                //Load 
                DGV1.AutoGenerateColumns = false;
              DGV1.DataSource=  cls.Search_UserPermissions("");      
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
     
        }


        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DGV1.AutoGenerateColumns = false;
                DGV1.DataSource = cls.Search_UserPermissions(txtSearch.Text);


            }
            catch
            {
                return;
            }


        }


        private void AccountEnd_Form_KeyDown(object sender, KeyEventArgs e)
        {
        
            if (e.KeyCode == Keys.Escape)
            {
                Close();
            }
        }

        private void DGV1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (DGV1.Rows.Count == 0)
                {
                    return;
                }
                String Header = DGV1.Columns[e.ColumnIndex].Name;
                if (Header == "C1")
                {
                    loaddata = true;
                    ID_Load = DGV1.CurrentRow.Cells["ID"].Value.ToString();
                    Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DGV1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            try
            {
                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    DGV1.Rows[i].Cells[0].Value = Properties.Resources.Open_Folder_48px;
                }
            }
            catch
            {


            }
        }
        public String ID_Load = "";
        private void DGV1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Enter)
            {
              try
            {
                if (DGV1.SelectedRows.Count==1)
                {
                    loaddata = true;
                    ID_Load = DGV1.CurrentRow.Cells["ID"].Value.ToString();
                    Close();
                }

                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }   
            }
        }

     


    }
}
