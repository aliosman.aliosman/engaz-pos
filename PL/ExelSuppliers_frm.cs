﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Threading;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class ExelSuppliers_frm : Form
    {
        public ExelSuppliers_frm()
        {
            InitializeComponent();
        }
        Suppliers_cls cls = new Suppliers_cls();
        private void toolStripButton3_Click(object sender, EventArgs e)
        {

        }

        private void ExelCustomer_frm_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingToolStrip(toolStrip1);
            ERB_Setting.SettingForm(this);
        }

        private void butBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "Select Fill Excle|*.xls";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    txtFillePath.Text = ofd.FileName;
                    DGV1.DataSource = null;
                    DGV1.Rows.Clear();
                    String pathconn = "Provider=Microsoft.jet.oledb.4.0;Data Source=" + ofd.FileName + ";Extended Properties=\"Excel 8.0;HDR=Yes;\";";
                    OleDbConnection conn = new OleDbConnection(pathconn);
                    OleDbDataAdapter da = new OleDbDataAdapter("Select * from [" + "Suppliers" + "$]", conn);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    DGV1.DataSource = dt;
                    lblCount.Text = DGV1.Rows.Count.ToString();



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DGV1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            try
            {

                if (DGV1.SelectedRows.Count >= 1)
                {
                    if (Mass.Delete() == true)
                    {
                        foreach (DataGridViewRow r in DGV1.SelectedRows)
                        {
                            DGV1.Rows.Remove(r);
                        }
                        lblCount.Text = DGV1.Rows.Count.ToString();
                      
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (DGV1.Rows.Count==0)
            {
                return;
            }
            for (int i = 0; i < DGV1.Rows.Count; i++)
            {
                if (cls.NameSearch_Supplier(DGV1.Rows[i].Cells[0].Value.ToString()).Rows.Count>0)
                {
                    MessageBox.Show("اسم العميل المدخل موجود مسبقا", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    DGV1.Rows[i].Selected = true;
                    return;
                }  

            }
            if (!backgroundWorker1.IsBusy)
            {

                backgroundWorker1.RunWorkerAsync();
            }
        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {

            save();
        }

        private void save ()
        {
            try
            {
                int x = 2;
                int count = DGV1.Rows.Count;
                for (int i = 0; i < DGV1.Rows.Count; i++)
                {

                    cls.InsertSuppliers(cls.MaxID_Suppliers(), DGV1.Rows[i].Cells[0].Value.ToString(), "", DGV1.Rows[i].Cells[1].Value.ToString(),"", "", "", "", "تحميل من ملف اكسل", DGV1.Rows[i].Cells[2].Value.ToString(), DGV1.Rows[i].Cells[3].Value.ToString(), "", "", "", "", "", "1",false, Properties.Settings.Default.UserID, true);

                    if (!backgroundWorker1.CancellationPending)
                    {
                        backgroundWorker1.ReportProgress(x++ * 100 / count, string.Format("{0} %", i));
                        Thread.Sleep(1);
                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            try
            {
                toolStripProgressBar1.Value = e.ProgressPercentage;
                // lblPrcent.Text = string.Format("{0} %", e.ProgressPercentage);
                // progressBar.Update();
            }
            catch (Exception)
            {


            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            Mass.Saved();
            DGV1.DataSource = null;
            DGV1.Rows.Clear();
            toolStripProgressBar1.Value = 0;
            lblCount.Text = "0";
        }
    }
}
