﻿using DevExpress.XtraEditors;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class ExpensesAdd_frm : XtraForm
    {
        public ExpensesAdd_frm()
        {
            InitializeComponent();
          //  this.button3.Click += new System.EventHandler(this.Button3_Click);
        }


        PayType_trans_cls PayType_trans_cls = new PayType_trans_cls();
        Expense_cls cls = new Expense_cls();
        TreasuryMovement_cls cls_Treasury_Movement = new TreasuryMovement_cls();
        DataTable dtShow;

        string TreasuryID;
        private void CustomersPayment_FormAdd_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingToolStrip(toolStrip1);
            btnNew_Click_1(null, null);
            LoadExpenseType();
            

        }

       


        public void LoadExpenseType()
        {
            // FILL Combo Store item
            string name = combExpenseType.Text;
            ExpenseType_cls ExpenseType_cls = new ExpenseType_cls();
            combExpenseType.DataSource = ExpenseType_cls.Search_ExpenseType("");
            combExpenseType.DisplayMember = "ExpenseName";
            combExpenseType.ValueMember = "ExpenseID";
            combExpenseType.Text = name;
            
        }



        private void btnNew_Click_1(object sender, EventArgs e)
        {
            try
            {

                txtExpenseID.Text = cls.MaxID_Expenses();
                combExpenseType.Text = "";
                txtRecivedName.Text = "";
                D1.Value = DateTime.Now;
                txtExpenseValue.EditValue  = 0;
                txtRemarks.Text = "";
                //==============================================================================================================================
                dtShow = cls.Search_Expenses();
                pos = dtShow.Rows.Count;
                //==============================================================================================================================
               
                btnSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
                combExpenseType.Focus();
              
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if (txtExpenseID.Text == "")
            {
                MessageBox.Show("حدث خطأ يرجي المحاولة لاحقا", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }


            if (txtExpenseValue.Text == "0")
            {
                MessageBox.Show("يرجي ادخال المبلغ ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }



            if (string.IsNullOrEmpty(txtRecivedName.Text.Trim()))
            {
                MessageBox.Show("يرجي ادخال اسم مستلم المصروف ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtRecivedName.Focus();
                return;
            }

            if (combExpenseType.SelectedIndex < 0 || combExpenseType.Text.Trim()=="")
            {
                MessageBox.Show("يرجي ادخال نوع المصروف", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                combExpenseType.Focus();
                return;
            }
            if (txtRemarks.Text.Trim() == "")
            {
                MessageBox.Show("يرجي ادخال نوع البيان", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning); 
                txtRemarks.Focus();
                return;
            }



            try
            {

                string Remarks = this.Text;
                if (!string.IsNullOrEmpty(txtRemarks.Text.Trim()))
                {
                    Remarks = txtRemarks.Text;
                }
                txtExpenseID.Text = cls.MaxID_Expenses();
                TreasuryID = cls_Treasury_Movement.MaxID_TreasuryMovement();
                cls.InsertExpenses(txtExpenseID.Text, combExpenseType.SelectedValue.ToString(), D1.Value, txtRecivedName.Text, txtExpenseValue.Text, txtRemarks.Text, TreasuryID, Properties.Settings.Default.UserID);
                cls_Treasury_Movement.InsertTreasuryMovement(TreasuryID, "", "Exp", txtExpenseID.Text, txtTypeID.Text, combExpenseType.Text, D1.Value, txtRemarks.Text, "0", txtExpenseValue.Text, txtRemarks.Text, "0", "0", Properties.Settings.Default.UserID);

                 


                Mass.Saved();
                btnNew_Click_1(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnUpdate_Click_1(object sender, EventArgs e)
        {

            if (txtExpenseID.Text == "")
            {
                MessageBox.Show("حدث خطأ يرجي المحاولة لاحقا", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }


            if (txtExpenseValue.Text == "0")
            {
                MessageBox.Show("يرجي ادخال المبلغ ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }



            if (string.IsNullOrEmpty(txtRecivedName.Text.Trim()))
            {
                MessageBox.Show("يرجي ادخال اسم مستلم المصروف ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtRecivedName.Focus();
                return;
            }

            if (combExpenseType.SelectedIndex < 0 || combExpenseType.Text.Trim() == "")
            {
                MessageBox.Show("يرجي ادخال نوع المصروف", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                combExpenseType.Focus();
                return;
            }
            if (txtRemarks.Text.Trim() == "")
            {
                MessageBox.Show("يرجي ادخال نوع البيان", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtRemarks.Focus();
                return;
            }


            try
            {
               
                cls.UpdateExpenses(txtExpenseID.Text, combExpenseType.SelectedValue.ToString(), D1.Value, txtRecivedName.Text, txtExpenseValue.Text, txtRemarks.Text, TreasuryID);
                cls_Treasury_Movement.UpdateTreasuryMovement(TreasuryID, "", "Exp", txtExpenseID.Text, txtTypeID.Text, combExpenseType.Text, D1.Value, txtRemarks.Text, "0", txtExpenseValue.Text, txtRemarks.Text, "0", "0");
 
                MessageBox.Show("تم التعديل بنجاح", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnNew_Click_1(null, null);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (Mass.Delete() == true)
                {
                    cls.DeleteExpenses(txtExpenseID.Text);
                    cls_Treasury_Movement.DeleteTreasuryMovement(TreasuryID);
                    PayType_trans_cls.Delete_PayType_trans(txtExpenseID.Text, txtTypeID.Text);

                    btnNew_Click_1(null, null);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

      

       
        int pos = 0;
        private void button6_Click(object sender, EventArgs e)
        {
            try
            {

                pos--;
                if (pos >= 0)
                {
                    ShowData(pos);
                }
                else
                {
                    MessageBox.Show("هذا اضغر سند ");

                }
            }
            catch
            {
                return;
            }
        }




        public void ShowData(int index)
        {
            try
            {

                DataTable dt = cls.Details_Expenses(dtShow.Rows[index]["ExpenseID"].ToString());
                DataRow Dr = dt.Rows[0];
                txtExpenseID.Text = Dr["ExpenseID"].ToString();
                D1.Value = Convert.ToDateTime(Dr["MyDate"]);
                combExpenseType.Text = Dr["ExpenseName"].ToString();
                txtRecivedName.Text = Dr["RecivedName"].ToString();
                txtExpenseValue.Text = Dr["ExpenseValue"].ToString();
                txtRemarks.Text = Dr["Remarks"].ToString();
                TreasuryID = Dr["TreasuryID"].ToString();




                 

                btnSave.Enabled = false;
                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;
            }
            catch
            {
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                pos++;
                if (pos < dtShow.Rows.Count)
                {
                    ShowData(pos);
                }
                else
                {
                    MessageBox.Show("هذا اكبر سند  ");
                    pos = dtShow.Rows.Count;
                    //ShowData(pos);
                }
            }
            catch
            {
                return;
            }
        }

       

        private void txtRemarks_TextChanged(object sender, EventArgs e)
        {
            txtRemarks.BackColor = Color.White;
        }

        private void CashExport_frm_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click_1(null, null);
                }
                if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        toolStripButton3_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click_1(null, null);
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    if (btnDelete.Enabled == true)
                    {
                        btnDelete_Click_1(null, null);
                    }
                }
                //if (e.KeyCode == Keys.F5)
                //{
                //    btnGetData_Click(null, null);
                //}

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion
        }

       
    }
}
