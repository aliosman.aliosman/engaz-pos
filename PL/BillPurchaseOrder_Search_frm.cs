﻿using ByStro.RPT;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class BillPurchaseOrder_Search_frm : Form
    {
        public BillPurchaseOrder_Search_frm()
        {
            InitializeComponent();
        }
        PurchaseOrder_cls cls = new PurchaseOrder_cls();
        public Boolean LoadData = false;
        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                DGV1.DataSource = null;
                DGV1.Rows.Clear();
                DataTable dt = new DataTable();
                if (DataAccessLayer.TypeUser == true)
                {
                    dt = cls.Search_PurchaseOrder_Main(D1.Value, D2.Value, txtSearch.Text);
                }
                else
                {
                    dt = cls.Search_PurchaseOrder_Main(D1.Value, D2.Value, Properties.Settings.Default.UserID, txtSearch.Text);
                }


                DGV2.AutoGenerateColumns = false;
                DGV2.DataSource = dt;
                sumbills();
            }
            catch
            {
                return;
            }

        }

        private void SalesReturn_Form_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingToolStrip(toolStrip1);
            ERB_Setting.SettingForm(this);
            ERB_Setting.SettingDGV(DGV2);


            LoadBill();
            ERB_Setting.SettingDGV(DGV1);


        }





        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            LoadBill();
        }


        private void LoadBill()
        {
            try
            {

                DataTable dt = new DataTable();
                if (DataAccessLayer.TypeUser == true)
                {
                    dt = cls.Search_PurchaseOrder_Main(D1.Value, D2.Value,"");
                }
                else
                {
                    dt = cls.Search_PurchaseOrder_Main(D1.Value, D2.Value, Properties.Settings.Default.UserID,"");
                }

                DGV2.AutoGenerateColumns = false;
                DGV2.DataSource = dt;
                sumbills();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            LoadBill();
        }


        public void sumbills()
        {


            try
            {


                Decimal NetInvoice = 0;
   
                lblCount.Text = "0";

                for (int i = 0; i < DGV2.Rows.Count; i++)
                {
            
                    NetInvoice += Convert.ToDecimal(DGV2.Rows[i].Cells["NetInvoice"].Value);


                }

                lblSum.Text = NetInvoice.ToString();
        
                lblCount.Text = DGV2.Rows.Count.ToString(); ;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }




        }



        private void DGV2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (DGV2.Rows.Count == 0)
                {
                    return;
                }
                DGV1.DataSource = null;
                DGV1.Rows.Clear();
                DataTable dt = cls.Details_PurchaseOrder_Details(long.Parse( DGV2.CurrentRow.Cells["MainID"].Value.ToString()));
                DGV1.AutoGenerateColumns = false;
                DGV1.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        public Boolean ReturnInvoice = false;
        private void btnView_Click(object sender, EventArgs e)
        {

            if (DGV2.Rows.Count == 0)
            {
                return;
            }
        
            LoadData = true;




            Close();
        }

        private void BillBay_Search_frm_Resize(object sender, EventArgs e)
        {
            try
            {
                if (this.WindowState == FormWindowState.Maximized)
                {
                    DGV2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                }
                else
                {
                    DGV2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
                    DGV2.Columns[0].Width = 150;
                    DGV2.Columns[1].Width = 150;
                    DGV2.Columns[2].Width = 250;
                    DGV2.Columns[3].Width = 150;
                    DGV2.Columns[4].Width = 150;
                    DGV2.Columns[5].Width = 100;
                    DGV2.Columns[6].Width = 150;
                    DGV2.Columns[7].Width = 100;
                    DGV2.Columns[8].Width = 150;
                    DGV2.Columns[9].Width = 150;
                    DGV2.Columns[10].Width = 300;
                    DGV2.Columns[11].Width = 200;
                }

                panel3.Height = this.Height / 2;
            }
            catch
            {

            }

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (DGV2.Rows.Count == 0) return;
  
           // PrintSize1_2A4();

        }


        //private void PrintSize1_2A4()
        //{




        //    BillSetting_cls cls_print = new BillSetting_cls();
        //    DataTable dt_Print = cls_print.BillPrintPurchaseOrder(DGV2.CurrentRow.Cells["MainID"].Value.ToString());

        //    Reportes_Form frm = new Reportes_Form();

        //    var report = new CRBillPurchaseOrder_A4();
        //    CombanyInformatuon_cls CombanyInformatuon_cls = new CombanyInformatuon_cls();
        //    report.Database.Tables["CombanyData"].SetDataSource(CombanyInformatuon_cls.Details_CombanyData());

        //    report.Database.Tables["Bill_Print"].SetDataSource(dt_Print);





        //    string note="في حال وجود أي أسئلة أو ملاحظات يرجى الاتصال بنا";
        //    if (DGV2.CurrentRow.Cells["Remarks"].Value.ToString() != "")
        //    {
        //        note = DGV2.CurrentRow.Cells["Remarks"].Value.ToString();
        //    }

        //    report.SetParameterValue(0, DGV2.CurrentRow.Cells["MainID"].Value.ToString());
        //    report.SetParameterValue(1, note);
        //    report.SetParameterValue(2, DGV2.CurrentRow.Cells["TotalInvoice"].Value.ToString());
        //    report.SetParameterValue(3, DGV2.CurrentRow.Cells["Discount"].Value.ToString());
        //    report.SetParameterValue(4, DGV2.CurrentRow.Cells["NetInvoice"].Value.ToString());
        //    report.SetParameterValue(5, DGV2.CurrentRow.Cells["VatValue"].Value.ToString());
        //    report.SetParameterValue(6, D1.Value);


        //    frm.crystalReportViewer1.ReportSource = report;
        //    frm.crystalReportViewer1.Refresh();

        //    frm.ShowDialog();

        //    #region"Crystal Report | Printing | Default Printer"
        //    //var dialog = new PrintDialog();
        //    //report.PrintOptions.PrinterName = dialog.PrinterSettings.PrinterName;
        //    //report.PrintToPrinter(1, false, 0, 0);



        //    #endregion


        //}







        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
          
        }









    }
}