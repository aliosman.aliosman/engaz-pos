﻿namespace ByStro.PL
{
    partial class CustomersFirst_frm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomersFirst_frm));
            this.txtCustomerID = new System.Windows.Forms.Label();
            this.D1 = new System.Windows.Forms.DateTimePicker();
            this.Label1 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.txtCredit = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtPaymentValueArbic = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.txtDebit = new System.Windows.Forms.TextBox();
            this.txtPayID = new System.Windows.Forms.TextBox();
            this.txtTypeID = new System.Windows.Forms.TextBox();
            this.combCustomers = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // txtCustomerID
            // 
            this.txtCustomerID.BackColor = System.Drawing.Color.White;
            this.txtCustomerID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCustomerID.Location = new System.Drawing.Point(571, 46);
            this.txtCustomerID.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.txtCustomerID.Name = "txtCustomerID";
            this.txtCustomerID.Size = new System.Drawing.Size(85, 22);
            this.txtCustomerID.TabIndex = 214;
            this.txtCustomerID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtCustomerID.Visible = false;
            this.txtCustomerID.TextChanged += new System.EventHandler(this.txtCustomerID_TextChanged);
            this.txtCustomerID.Click += new System.EventHandler(this.txtCustomerID_Click);
            // 
            // D1
            // 
            this.D1.CustomFormat = "";
            this.D1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.D1.Location = new System.Drawing.Point(25, 46);
            this.D1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.D1.Name = "D1";
            this.D1.Size = new System.Drawing.Size(133, 24);
            this.D1.TabIndex = 212;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.ForeColor = System.Drawing.Color.Black;
            this.Label1.Location = new System.Drawing.Point(25, 25);
            this.Label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(97, 17);
            this.Label1.TabIndex = 208;
            this.Label1.Text = "تاريخ الاستلام :";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.ForeColor = System.Drawing.Color.Black;
            this.Label2.Location = new System.Drawing.Point(165, 26);
            this.Label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(79, 17);
            this.Label2.TabIndex = 206;
            this.Label2.Text = "كود الحركة :";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.ForeColor = System.Drawing.Color.Black;
            this.Label6.Location = new System.Drawing.Point(25, 77);
            this.Label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(86, 17);
            this.Label6.TabIndex = 205;
            this.Label6.Text = "اسم العميل :";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.ForeColor = System.Drawing.Color.Black;
            this.Label4.Location = new System.Drawing.Point(25, 231);
            this.Label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(63, 17);
            this.Label4.TabIndex = 202;
            this.Label4.Text = "البيــــان :";
            // 
            // txtRemarks
            // 
            this.txtRemarks.BackColor = System.Drawing.Color.White;
            this.txtRemarks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRemarks.Location = new System.Drawing.Point(25, 255);
            this.txtRemarks.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtRemarks.MaxLength = 150;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRemarks.Size = new System.Drawing.Size(543, 66);
            this.txtRemarks.TabIndex = 2;
            this.txtRemarks.TextChanged += new System.EventHandler(this.txtStatement_TextChanged);
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.ForeColor = System.Drawing.Color.Black;
            this.Label3.Location = new System.Drawing.Point(25, 179);
            this.Label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(67, 17);
            this.Label3.TabIndex = 201;
            this.Label3.Text = "دائن / له :";
            // 
            // txtCredit
            // 
            this.txtCredit.BackColor = System.Drawing.Color.White;
            this.txtCredit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCredit.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.txtCredit.ForeColor = System.Drawing.Color.Green;
            this.txtCredit.Location = new System.Drawing.Point(25, 202);
            this.txtCredit.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtCredit.Name = "txtCredit";
            this.txtCredit.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCredit.Size = new System.Drawing.Size(133, 24);
            this.txtCredit.TabIndex = 1;
            this.txtCredit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCredit.TextChanged += new System.EventHandler(this.txtPaymentValue_TextChanged);
            this.txtCredit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPaymentValue_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(25, 128);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 17);
            this.label9.TabIndex = 225;
            this.label9.Text = "مدين / علية :";
            // 
            // txtPaymentValueArbic
            // 
            this.txtPaymentValueArbic.BackColor = System.Drawing.Color.White;
            this.txtPaymentValueArbic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPaymentValueArbic.ForeColor = System.Drawing.Color.Black;
            this.txtPaymentValueArbic.Location = new System.Drawing.Point(161, 202);
            this.txtPaymentValueArbic.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.txtPaymentValueArbic.Name = "txtPaymentValueArbic";
            this.txtPaymentValueArbic.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtPaymentValueArbic.Size = new System.Drawing.Size(407, 24);
            this.txtPaymentValueArbic.TabIndex = 226;
            this.txtPaymentValueArbic.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.White;
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(162, 151);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label10.Size = new System.Drawing.Size(406, 24);
            this.label10.TabIndex = 229;
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDelete.Enabled = false;
            this.btnDelete.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.White;
            this.btnDelete.Location = new System.Drawing.Point(593, 196);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(91, 33);
            this.btnDelete.TabIndex = 416;
            this.btnDelete.Text = "حذف";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(593, 231);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(91, 33);
            this.btnClose.TabIndex = 418;
            this.btnClose.Text = "اغلاق";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdate.Enabled = false;
            this.btnUpdate.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.ForeColor = System.Drawing.Color.White;
            this.btnUpdate.Location = new System.Drawing.Point(593, 161);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(91, 33);
            this.btnUpdate.TabIndex = 415;
            this.btnUpdate.Text = "تعديل";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(593, 126);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(91, 33);
            this.btnSave.TabIndex = 414;
            this.btnSave.Text = "حفظ";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnNew
            // 
            this.btnNew.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnNew.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNew.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNew.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNew.ForeColor = System.Drawing.Color.White;
            this.btnNew.Location = new System.Drawing.Point(593, 91);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(91, 33);
            this.btnNew.TabIndex = 413;
            this.btnNew.Text = "جديد";
            this.btnNew.UseVisualStyleBackColor = false;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // txtDebit
            // 
            this.txtDebit.BackColor = System.Drawing.Color.White;
            this.txtDebit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDebit.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDebit.ForeColor = System.Drawing.Color.Green;
            this.txtDebit.Location = new System.Drawing.Point(25, 151);
            this.txtDebit.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtDebit.Name = "txtDebit";
            this.txtDebit.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtDebit.Size = new System.Drawing.Size(133, 24);
            this.txtDebit.TabIndex = 421;
            this.txtDebit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtDebit.TextChanged += new System.EventHandler(this.txtCustomersBlanse_TextChanged);
            // 
            // txtPayID
            // 
            this.txtPayID.BackColor = System.Drawing.Color.White;
            this.txtPayID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPayID.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPayID.ForeColor = System.Drawing.Color.Black;
            this.txtPayID.Location = new System.Drawing.Point(162, 46);
            this.txtPayID.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtPayID.Name = "txtPayID";
            this.txtPayID.ReadOnly = true;
            this.txtPayID.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtPayID.Size = new System.Drawing.Size(406, 24);
            this.txtPayID.TabIndex = 422;
            this.txtPayID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTypeID
            // 
            this.txtTypeID.Location = new System.Drawing.Point(619, 21);
            this.txtTypeID.Name = "txtTypeID";
            this.txtTypeID.ReadOnly = true;
            this.txtTypeID.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTypeID.Size = new System.Drawing.Size(37, 24);
            this.txtTypeID.TabIndex = 424;
            this.txtTypeID.Text = "First";
            this.txtTypeID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtTypeID.Visible = false;
            this.txtTypeID.TextChanged += new System.EventHandler(this.txtBillTypeID_TextChanged);
            // 
            // combCustomers
            // 
            this.combCustomers.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combCustomers.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combCustomers.FormattingEnabled = true;
            this.combCustomers.Location = new System.Drawing.Point(25, 101);
            this.combCustomers.Name = "combCustomers";
            this.combCustomers.Size = new System.Drawing.Size(543, 24);
            this.combCustomers.TabIndex = 425;
            this.combCustomers.SelectedIndexChanged += new System.EventHandler(this.combCustomers_SelectedIndexChanged);
            this.combCustomers.TextChanged += new System.EventHandler(this.combCustomers_TextChanged);
            // 
            // CustomersFirst_frm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(707, 348);
            this.Controls.Add(this.combCustomers);
            this.Controls.Add(this.txtTypeID);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtPaymentValueArbic);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.Label6);
            this.Controls.Add(this.D1);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.txtRemarks);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.txtCredit);
            this.Controls.Add(this.txtCustomerID);
            this.Controls.Add(this.txtPayID);
            this.Controls.Add(this.txtDebit);
            this.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "CustomersFirst_frm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "رصيد أول المدة للعملاء";
            this.Load += new System.EventHandler(this.CustomersPayment_FormAdd_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.DateTimePicker D1;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.TextBox txtRemarks;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.TextBox txtCredit;
        internal System.Windows.Forms.Label label9;
        internal System.Windows.Forms.Label txtPaymentValueArbic;
        public System.Windows.Forms.Label txtCustomerID;
        internal System.Windows.Forms.Label label10;
        internal System.Windows.Forms.Button btnDelete;
        internal System.Windows.Forms.Button btnClose;
        internal System.Windows.Forms.Button btnUpdate;
        internal System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Button btnNew;
        internal System.Windows.Forms.TextBox txtDebit;
        internal System.Windows.Forms.TextBox txtPayID;
        private System.Windows.Forms.TextBox txtTypeID;
        private System.Windows.Forms.ComboBox combCustomers;
    }
}