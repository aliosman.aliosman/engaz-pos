﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing;
using ByStro.RPT;

namespace ByStro.PL
{
    public partial class CustomersTransProdect_Form : Form
    {
        public CustomersTransProdect_Form()
        {
            InitializeComponent();
        }

        Customers_cls clsCustomer = new Customers_cls();
        DataAccessLayer DataAccessLayer = new DataAccessLayer();
        public string AddEdit = "Add";
        private void CustomersTrans_Form_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingToolStrip(toolStrip1);
            ERB_Setting.SettingForm(this);
            ERB_Setting.SettingDGV(DGV1);
            if (AddEdit == "Add")
            {
                txtAccountID.Text = "";
                txtAccountName.Text = "";
                DGV1.DataSource = null;
                DGV1.Rows.Clear();
                txt_1.Text = "0";
            }
            else
            {
                btnNew_Click(null, null);
            }
        }
        DataTable dt_Print = new DataTable();







        private void btnNew_Click(object sender, EventArgs e)
        {



            try
            {
                if (txtAccountName.Text.Trim() == "")
                {
                    txtAccountName.BackColor = Color.Pink;
                    button3_Click(null, null);
                    return;
                }

                Customers_cls cls = new Customers_cls();
                DataTable dt =cls.RptDetails_Customers(datefrom.Value,datetto.Value,txtAccountID.Text);
               DGV1.AutoGenerateColumns = false;
               DGV1.DataSource = dt;
               txt_1.Text = DGV1.Rows.Count.ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Massage", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

     


        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                CustomersSearch_frm frm = new CustomersSearch_frm();

                frm.ShowDialog(this);
                if (frm.loaddata == true)
                {
                    txtAccountID.Text = frm.DGV1.CurrentRow.Cells["CustomerID"].Value.ToString();
                    txtAccountName.Text = frm.DGV1.CurrentRow.Cells["CustomerName"].Value.ToString();
                    txtAccountName.BackColor = Color.WhiteSmoke;
                    DGV1.DataSource = null;
                    DGV1.Rows.Clear();
                    txt_1.Text = "0";
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
