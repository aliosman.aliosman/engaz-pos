﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing;
using ByStro.RPT;
using DevExpress.XtraGrid.Columns;
using System.Collections.Generic;
using DevExpress.XtraEditors;
using System.Linq;

namespace ByStro.PL
{
    public partial class SuppliersTrans_frm : XtraForm
    {
        public SuppliersTrans_frm()
        {
            InitializeComponent();
        }

        Suppliers_cls clsSuppliers = new Suppliers_cls();
       // SuppliersTrans_cls cls = new SuppliersTrans_cls();
        DataAccessLayer DataAccessLayer = new DataAccessLayer();
        public string AddEdit = "Add";
        private void CustomersTrans_Form_Load(object sender, EventArgs e)
        { 
           
            using (var db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {
                  var supins = new DAL.Supplier();

                LookUpEdit_PartID.Properties.DataSource = db.Suppliers.Select(c =>
                new { ID = c.SupplierID, Name = c.SupplierName, c.Address, c.Phone }).ToList();
                LookUpEdit_PartID.Properties.DisplayMember = "Name";
                LookUpEdit_PartID.Properties.ValueMember = "ID";

            }
            LookUpEdit_PartID.Properties.PopupFormWidth = 300;
            gridLookUpEdit1View.Appearance.HeaderPanel.Options.UseTextOptions = true;
            gridLookUpEdit1View.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            gridLookUpEdit1View.Appearance.Row.Options.UseTextOptions = true;
            gridLookUpEdit1View.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            gridLookUpEdit1View.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            gridLookUpEdit1View.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            gridLookUpEdit1View.OptionsBehavior.AutoSelectAllInEditor = false;
            gridLookUpEdit1View.OptionsBehavior.AutoUpdateTotalSummary = false;
            gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            gridLookUpEdit1View.OptionsView.BestFitMaxRowCount = 10;
            gridLookUpEdit1View.OptionsView.ShowAutoFilterRow = true;
            gridLookUpEdit1View.OptionsView.ShowDetailButtons = false;
            gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            LookUpEdit_PartID.Properties.View.PopulateColumns(LookUpEdit_PartID.Properties.DataSource);
            gridLookUpEdit1View.Columns["ID"].Visible = false;
            gridLookUpEdit1View.Columns["Name"].Caption = "الاسم";
            gridLookUpEdit1View.Columns["Address"].Caption = "العنوان";
            gridLookUpEdit1View.Columns["Phone"].Caption = "الهاتف";

            datefrom.DateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            datetto.DateTime = DateTime.Now.Date;
            if (AddEdit == "Add")
            {
            
                txt_1.Caption  = "0";
                txt_2.Caption = "0";
                txt_3.Caption = "0";
            }
            else
            {
                btnNew_Click(null,null);
            }
        }
        DataTable dt_Print = new DataTable();

        List<GridColumn> IntColumns()
        {


        GridColumn  VoucherID =new  GridColumn();
        GridColumn  VoucherType   =new  GridColumn();
        GridColumn  VoucherDate   =new  GridColumn();
        GridColumn  Remark        =new  GridColumn();
        GridColumn  Debit         =new  GridColumn();
        GridColumn  Credit        =new  GridColumn();
        GridColumn EmpName = new GridColumn();


            VoucherID.FieldName = "VoucherID";
            VoucherID.Caption = "رقم السند";
            VoucherID.Name = "VoucherID";
            VoucherID.Width = 150;

            VoucherType.FieldName = "VoucherType";
            VoucherType.Caption = "نوع الحركة";
            VoucherType.Name = "VoucherType";
            VoucherType.Width = 150;

            VoucherDate.FieldName = "VoucherDate";
            VoucherDate.Caption = "تاريخ السند";
            VoucherDate.Name = "VoucherDate";
            VoucherDate.Width = 120;
            // 
            // Remark
            // 
            Remark.FieldName = "Remark";
            Remark.Caption = "البيــــــــــان";
            Remark.Name = "Remark";
            Remark.Width = 250;
            // 
            // Debit
            // 
            Debit.FieldName = "Debit";
            Debit.Caption = "علية / مدين";
            Debit.Name = "Debit";
            Debit.Width = 130;
            // 
            // Credit
            // 
            Credit.FieldName = "Credit";
            Credit.Caption = "له / دائن";
            Credit.Name = "Credit";
            Credit.Width = 130;
            // 
            // EmpName
            // 
            EmpName.FieldName = "EmpName";
            //dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            // EmpName.DefaultCellStyle = dataGridViewCellStyle9;
            EmpName.Caption = "المستخدم";
            EmpName.Name = "EmpName";
            EmpName.Width = 150; 
            return new List<GridColumn>()
            { VoucherID, VoucherType, VoucherDate, Remark, Debit, Credit, EmpName };
             

        }
         
 

        private void btnNew_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if ((LookUpEdit_PartID.EditValue is int id && id > 0) == false)
            {
                LookUpEdit_PartID.ErrorText = "يجب اختيار الحساب";

                return;
            }


            try
            {



                Decimal x = 0;
                Decimal y = 0;
                RbtTreasuryMovement_cls cls = new RbtTreasuryMovement_cls();

                DataTable Dt2 = cls.TreasuryMovement(datefrom.DateTime, datetto.DateTime, LookUpEdit_PartID.EditValue.ToString(), "Supp");
                //===========================================================================================
                DataTable Dt = cls.TreasuryMovement2(datefrom.DateTime, LookUpEdit_PartID.EditValue.ToString(), "Supp");

                Decimal Sumincome = 0;
                Decimal SumExport = 0;
                if (Dt.Rows[0][0] == DBNull.Value == false)
                    Sumincome = Convert.ToDecimal(Dt.Rows[0][0]);

                if (Dt.Rows[0][1] == DBNull.Value == false)
                    SumExport = Convert.ToDecimal(Dt.Rows[0][1]);

                //====================================================================================

                SqlDataAdapter da_Print = new SqlDataAdapter(@"SELECT        dbo.TreasuryMovement.*, dbo.UserPermissions.EmpName  FROM  dbo.TreasuryMovement INNER JOIN
                         dbo.UserPermissions ON dbo.TreasuryMovement.UserAdd = dbo.UserPermissions.ID where VoucherID ='0' ORDER BY VoucherDate", DataAccessLayer.connSQLServer);
                dt_Print = new DataTable();

                da_Print.Fill(dt_Print);
                da_Print.Dispose();
                dt_Print.Clear();
                DataRow DR = dt_Print.NewRow();
                DR["VoucherID"] = "0";
                DR["VoucherType"] = "رصيد سابق";
                DR["VoucherDate"] = DateTime.Now.ToString("yyyy/MM/dd");
                DR["Remark"] = "رصيد الفترة السابقة";
                DR["Debit"] = Sumincome.ToString();
                DR["Credit"] = SumExport.ToString();
                dt_Print.Rows.Add(DR);
                x += Convert.ToDecimal(Sumincome.ToString());
                y += Convert.ToDecimal(SumExport.ToString());



                for (int i = 0; i < Dt2.Rows.Count; i++)
                {
                    DR = dt_Print.NewRow();
                    //DR["VoucherType"] = Dt2.Rows[i]["VoucherType"];
                    DR["VoucherID"] = Dt2.Rows[i]["VoucherID"];
                    DR["VoucherType"] = Dt2.Rows[i]["VoucherType"];
                    DR["VoucherDate"] = Dt2.Rows[i]["VoucherDate"];
                    DR["Remark"] = Dt2.Rows[i]["Remark"];
                    DR["Debit"] = Dt2.Rows[i]["Debit"];
                    DR["Credit"] = Dt2.Rows[i]["Credit"];
                    DR["EmpName"] = Dt2.Rows[i]["EmpName"];
                    dt_Print.Rows.Add(DR);
                    x += Convert.ToDecimal(Dt2.Rows[i]["Debit"]);
                    y += Convert.ToDecimal(Dt2.Rows[i]["Credit"]);
                }


                txt_1.Caption = x.ToString();
                txt_2.Caption = y.ToString();
                Decimal D = y - x;
                txt_3.Caption = D.ToString();
                gridControl1.DataSource = dt_Print;
                gridView1.Columns.Clear();
                int Index = 0;
                foreach (var item in IntColumns())
                {
                    gridView1.Columns.Add(item);
                    item.VisibleIndex = Index;
                    item.OptionsColumn.AllowEdit = false;
                    Index++;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Massage", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } 

        } 
        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount == 0)
                btnNew_Click(null, null);
            else
                RPT.rpt_GridReport.Print(gridControl1, this.Text + " : " + LookUpEdit_PartID.Text,

                 " " + stlb1.Caption + " " + txt_1.Caption +
                 " " + stlb2.Caption + " " + txt_2.Caption +
                 " " + stlb3.Caption + " " + txt_3.Caption  
                    , true);

        }
    }
}
