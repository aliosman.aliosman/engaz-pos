﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class Baukup_Form : Form
    {
        public Baukup_Form()
        {
            InitializeComponent();
        }
        //SqlConnection connSQLServer = new SqlConnection(@"Server=" + Properties.Settings.Default.ServerName + ";  Initial Catalog = master; Integrated Security = True");
        SqlCommand cmd;
        public string database;
        private void Bukup_Form_Load(object sender, EventArgs e)
        {
            database=Properties.Settings.Default.Databasename;
             errorbackup = true;


             try
             {
                ERB_Setting.SettingForm(this);



                if (DataAccessLayer.paukupautomatic == "3")
                 {
                     button7_Click(null,null);
                 }
                 else if (DataAccessLayer.paukupautomatic == "OK")
                 {
                     button7_Click(null, null);
                 }


                 if (Properties.Settings.Default.BukUPState == "1")
                 {
                     radioButton1.Checked = true;
                 }
                 else if (Properties.Settings.Default.BukUPState == "2")
                 {
                     radioButton2.Checked = true;
                 }
                 else if (Properties.Settings.Default.BukUPState == "3")
                 {
                     radioButton3.Checked = true;
                 }
                 else if (Properties.Settings.Default.BukUPState == "4")
                 {
                     radioButton4.Checked = true;
                     numericUpDown1.Value = int.Parse(Properties.Settings.Default.BakupTime);
                 }
                if (string.IsNullOrEmpty(Properties.Settings.Default.BukupFillPath))
                {
                    string path = @"D:\ByStro\Backup";
                         if (!(System.IO.Directory.Exists(path)))
                    {
                        System.IO.Directory.CreateDirectory(path);

                    }
                    Properties.Settings.Default.BukupFillPath = path;
                    Properties.Settings.Default.Save();
                }
                 txtPath.Text = Properties.Settings.Default.BukupFillPath;
             }
             catch (Exception ex)
             {
                 MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
             }




        }





        private void button5_Click(object sender, EventArgs e)
        {
            try
            {

              

           if (textBox2.Text=="")
            {
                MessageBox.Show("يرجي تحديد المسار", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                button2_Click(null,null);
                return; 
            }
           this.Cursor = Cursors.WaitCursor;
           cmd = new SqlCommand("ALTER Database " + database + " SET OFFLINE WITH ROLLBACK IMMEDIATE; Restore database " + database + " from Disk='" + textBox2.Text + "'  with replace, recovery ", connSQLServer);
           connSQLServer.Open();
            cmd.ExecuteNonQuery();
           connSQLServer.Close();
           this.Cursor = Cursors.Default; 
           MessageBox.Show("تم استرجاع النسخة لاحتياطية بنجاح", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            textBox2.Text = "";

            }
            catch (Exception ex)
            {
                connSQLServer.Close();
             MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
             try
             {
           this.Cursor = Cursors.Default; 
            SqlCommand cmd2;
            string strQuery = "ALTER Database " + database + " SET ONLINE "; cmd2 = new SqlCommand(strQuery, connSQLServer);
            connSQLServer.Open();
            cmd2.ExecuteNonQuery();
            connSQLServer.Close();
             }
             catch (Exception)
             {
                 try
                 {
                     connSQLServer.Close();
                           SqlConnection con = new SqlConnection(@"Server=.;  Initial Catalog = master; Integrated Security = True");
                          SqlCommand cmd;
                          string strQuery = "ALTER Database " + database + " SET ONLINE "; cmd = new SqlCommand(strQuery, con);
                             con.Open();
                          cmd.ExecuteNonQuery();
                          con.Close();
                 }
                 catch (Exception)
                 {
                     return;
                 
                 }
                 return;
             }
           

            }
 

        }

        public SqlConnection connSQLServer = new SqlConnection();
        public void conn_string()
        {

            var p = Properties.Settings.Default;
            if (p.Mode == false)
            {
                connSQLServer = new SqlConnection("Data Source=" + p.ServerName + ";Initial Catalog=master;Integrated security=true");
            }
            else
            {
                connSQLServer = new SqlConnection("Data Source=" + p.ServerName + ",1433;Network Library=DBMSSOCN;Initial Catalog =master;User ID='" + p.SQLUserName + "' ;Password='" + p.SQLPassword + "'");
            }
        }

        bool errorbackup = false;

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            #region "xxxxxxxxxxxxxxx"
            try
            {


                conn_string();


                string filename = Properties.Settings.Default.BukupFillPath + "\\ByStro" + "_" + DateTime.Now.ToString("yyyy_MM_dd")
                   + " - " + DateTime.Now.ToLongTimeString().Replace(':', '-') + ".bak";
                SqlCommand cmd = new SqlCommand("Backup DataBase " + Properties.Settings.Default.Databasename + " to Disk='" + filename + "' ", connSQLServer);
                connSQLServer.Open();
                cmd.ExecuteNonQuery();
                connSQLServer.Close();
                int x = 2;
                for (int i = 0; i < 100; i++)
                {
                    if (!backgroundWorker.CancellationPending)
                    {
                        backgroundWorker.ReportProgress(x++ * 100 / 100, string.Format("{0} %", i));
                        Thread.Sleep(1);
                    }
                }




            }
            catch (Exception ex)
            {
                backgroundWorker.CancelAsync();
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                errorbackup = false;
                if (connSQLServer.State != ConnectionState.Closed)
                {
                    connSQLServer.Close(); 
                }
            
           

            }
            #endregion
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (!backgroundWorker.IsBusy)
            {

                lblMessage.Text = "Please Wait...";
                backgroundWorker.RunWorkerAsync();
            }

        }

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                progressBar.Value = e.ProgressPercentage;
                lblPrcent.Text = string.Format("{0} %", e.ProgressPercentage);
                progressBar.Update();
            }
            catch (Exception)
            {


            }
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (errorbackup != false)
                {

                    if (DataAccessLayer.paukupautomatic == "OK")
                    {
                        DataAccessLayer.closeMainform = true;
                        Application.Exit();
                    }
                    else if (DataAccessLayer.paukupautomatic == "3")
                    {

                        Application.OpenForms["Bukup_Form"].Close();
                    }
                    DataAccessLayer.paukupautomatic = "Close";
                    lblMessage.Text = "Successfully Database Backup Completed ...";
                }
            }
            catch
            {


            }

        }

        private void button8_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog Ffd = new FolderBrowserDialog();
            if (Ffd.ShowDialog() == DialogResult.OK)
            {
                String File_Path = Ffd.SelectedPath;
                txtPath.Text = File_Path;
            }


            //SaveFileDialog sfd = new SaveFileDialog();
            //sfd.FileName = "Records_Manager2018";
            //sfd.Filter = "SQL BackUp Fille|";
            //sfd.Title = "مسار نسخ قاعدة البيانات";
            //if (sfd.ShowDialog() == DialogResult.OK)
            //{
            //    String File_Path = System.IO.Path.GetDirectoryName(sfd.FileName);

            //    //textBox1.Text = sfd.FileName;
            //    //Properties.Settings.Default.BukupFillPath = sfd.FileName;



            //    txtPath.Text = File_Path;
            //    //Properties.Settings.Default.BukupFillPath = File_Path;
            //    //Properties.Settings.Default.Save();
            //}

        }

        private void txtFillPathCopy_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton4.Checked == true)
            {
                numericUpDown1.Enabled = true;

            }
            else
            {
                numericUpDown1.Enabled = false;
            }

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
        //    Properties.Settings.Default.BakupTime = (numericUpDown1.Value).ToString();
        //    Properties.Settings.Default.Save();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtPath.Text.Trim()=="")
            {
                MessageBox.Show("يرجي تحدد مسار النسخ الاحتياطي ","Message",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                return;
               
            }

            if (radioButton1.Checked == true)
            {
                Properties.Settings.Default.BukUPState = "1";
                Properties.Settings.Default.Save();
            }
            if (radioButton2.Checked == true)
            {
                Properties.Settings.Default.BukUPState = "2";
                Properties.Settings.Default.Save();
            }
            if (radioButton3.Checked == true)
            {
                Properties.Settings.Default.BukUPState = "3";
                Properties.Settings.Default.Save();
            }

            if (radioButton4.Checked == true)
            {
                numericUpDown1.Enabled = true;
                Properties.Settings.Default.BukUPState = "4";
                Properties.Settings.Default.BakupTime = (numericUpDown1.Value).ToString();
                Properties.Settings.Default.Save();
            }
            else
            {
                numericUpDown1.Enabled = false;
            }
            Properties.Settings.Default.BukupFillPath = txtPath.Text;
            Properties.Settings.Default.Save();

            MessageBox.Show("تم الحفظ التغيرات بنجاح", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);





        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog pfd = new OpenFileDialog();
            pfd.FileName = "ByStro.bak";
            pfd.Filter = "SQL BackUp Fille|*.bak";
            pfd.Title = "مسار استرجاع قاعدة البيانات";
            if (pfd.ShowDialog() == DialogResult.OK)
            {
                textBox2.Text = pfd.FileName;
            }

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked==true)
            {
   
              txtPath.ReadOnly = false;
              txtPath.BackColor = Color.White;
            
            }
            else
            {
                txtPath.ReadOnly = true;
                txtPath.BackColor = Color.WhiteSmoke;
            }
        }











    }
}
