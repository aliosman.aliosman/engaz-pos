﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class CashImport_frm : Form
    {
        public CashImport_frm()
        {
            InitializeComponent();
        }


        PayType_trans_cls PayType_trans_cls = new PayType_trans_cls();
        CashImport_Cls cls = new CashImport_Cls();
        TreasuryMovement_cls cls_Treasury_Movement = new TreasuryMovement_cls();
        DataTable dtShow;

        string TreasuryID;
        private void CustomersPayment_FormAdd_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingToolStrip(toolStrip1);
            ERB_Setting.SettingForm(this);
            ERB_Setting.SettingDGV(DGVPayType);



            FillCurrency();
            FillPayType();
            btnNew_Click_1(null, null);
           

        }
        public void FillCurrency()
        {
            string cusName = combCurrency.Text;
            Currency_cls Currency_cls = new Currency_cls();
            combCurrency.DataSource = Currency_cls.Search__Currency("");
            combCurrency.DisplayMember = "CurrencyName";
            combCurrency.ValueMember = "CurrencyID";
            combCurrency.Text = cusName;

        }
        public void FillPayType()
        {
            //string cusName = combPayType.Text;
            PayType_cls PayType_cls = new PayType_cls();
            combPayType.DataSource = PayType_cls.Search__PayType("");
            combPayType.DisplayMember = "PayTypeName";
            combPayType.ValueMember = "PayTypeID";
           // combPayType.Text = cusName;

        }


        private void txtPaymentValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }






        private void btnNew_Click_1(object sender, EventArgs e)
        {
            try
            {
                txtImportID.Text = Convert.ToString(cls.MaxID_Cash_Import());
                D1.Value = DateTime.Now;
                txtPayValue.BackColor = System.Drawing.Color.White;
                DGVPayType.DataSource = null;
                DGVPayType.Rows.Clear();
                txtSumPayType.Text = "0";
                txtRemarks.Text = "";
                if (BillSetting_cls.UseCrrencyDefault == true)
                {
                    Currency_cls Currency = new Currency_cls();
                    DataTable DtCurrency = Currency.Details_Currency(BillSetting_cls.CurrencyID.ToString());
                    combCurrency.Text = DtCurrency.Rows[0]["CurrencyName"].ToString();
                    txtCurrencyRate.Text = DtCurrency.Rows[0]["CurrencyRate"].ToString();
                }
                else
                {
                    combCurrency.Text = null;
                    txtCurrencyRate.Text = "";

                }
                ClearPayType();
                #region UsekindPay

                //===============================================================================================================================================================


                if (BillSetting_cls.UsekindPay == true)
                {
                    PayType_cls PayType_cls = new PayType_cls();
                    DataTable dtPayType = PayType_cls.Details_PayType(BillSetting_cls.PayTypeID);
                    if (dtPayType.Rows.Count > 0)
                    {
                        combPayType.Text = dtPayType.Rows[0]["PayTypeName"].ToString();
                    }
                }
                else
                {
                    combPayType.Text = null;
                }



                //===============================================================================================================================================================

                #endregion

                btnSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
                dtShow = cls.Search_Cash_Import();
                pos = dtShow.Rows.Count;
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if (txtImportID.Text == "")
            {
                MessageBox.Show("حدث خطأ يرجي المحاولة لاحقا", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

          



            if (txtSumPayType.Text == "0")
            {
                MessageBox.Show("يرجي ادخال المبلغ ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }


            if (string.IsNullOrEmpty(txtRemarks.Text.Trim()))
            {
                txtRemarks.Focus();
                MessageBox.Show("يرجي ادخال البيان ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }


            try
            {

               

                txtImportID.Text = cls.MaxID_Cash_Import();
                TreasuryID = cls_Treasury_Movement.MaxID_TreasuryMovement();
                cls.Insert_Cash_Import(txtImportID.Text, D1.Value, txtRemarks.Text, txtSumPayType.Text, TreasuryID, Properties.Settings.Default.UserID);
                cls_Treasury_Movement.InsertTreasuryMovement(TreasuryID,"","no", txtImportID.Text, txtTypeID.Text, this.Text, D1.Value, txtRemarks.Text, txtSumPayType.Text, "0",txtRemarks.Text,"0","0", Properties.Settings.Default.UserID);

                #region // Pay
                for (int i = 0; i < DGVPayType.Rows.Count; i++)
                {
                    string Statement = this.Text ;
                    if (!string.IsNullOrEmpty(DGVPayType.Rows[i].Cells["Statement"].Value.ToString()))
                    {
                        Statement = DGVPayType.Rows[i].Cells["Statement"].Value.ToString();
                    }
                    PayType_trans_cls.Insert_PayType_trans(txtImportID.Text,D1.Value, DGVPayType.Rows[i].Cells["PayTypeID"].Value.ToString(), txtImportID.Text, this.Text , txtTypeID.Text,  double.Parse(DGVPayType.Rows[i].Cells["PayValue"].Value.ToString()),0, Statement, Properties.Settings.Default.UserID, DGVPayType.Rows[i].Cells["CurrencyID2"].Value.ToString(), DGVPayType.Rows[i].Cells["CurrencyRate2"].Value.ToString(), DGVPayType.Rows[i].Cells["CurrencyPrice2"].Value.ToString());
                }
                #endregion


                Mass.Saved();
                btnNew_Click_1(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnUpdate_Click_1(object sender, EventArgs e)
        {
            if (txtImportID.Text == "")
            {
                MessageBox.Show("حدث خطأ يرجي المحاولة لاحقا", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

       



            if (txtSumPayType.Text == "0")
            {
                MessageBox.Show("يرجي ادخال المبلغ ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }


            if (string.IsNullOrEmpty(txtRemarks.Text.Trim()))
            {
                txtRemarks.Focus();
                MessageBox.Show("يرجي ادخال البيان ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try
            {
               
                cls.Update_Cash_Import(txtImportID.Text, D1.Value, txtRemarks.Text, txtSumPayType.Text);

                cls_Treasury_Movement.UpdateTreasuryMovement(TreasuryID, "", "no", txtImportID.Text, txtTypeID.Text, this.Text, D1.Value, txtRemarks.Text, txtSumPayType.Text,"0", txtRemarks.Text, "0", "0");

                #region // Pay
                PayType_trans_cls.Delete_PayType_trans(txtImportID.Text,txtTypeID.Text);
                for (int i = 0; i < DGVPayType.Rows.Count; i++)
                {
                    PayType_trans_cls.Insert_PayType_trans(txtImportID.Text, D1.Value, DGVPayType.Rows[i].Cells["PayTypeID"].Value.ToString(), txtImportID.Text, this.Text, txtTypeID.Text,  double.Parse(DGVPayType.Rows[i].Cells["PayValue"].Value.ToString()),0, DGVPayType.Rows[i].Cells["Statement"].Value.ToString(), Properties.Settings.Default.UserID, DGVPayType.Rows[i].Cells["CurrencyID2"].Value.ToString(), DGVPayType.Rows[i].Cells["CurrencyRate2"].Value.ToString(), DGVPayType.Rows[i].Cells["CurrencyPrice2"].Value.ToString());
                }
                #endregion

                MessageBox.Show("تم التعديل بنجاح", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnNew_Click_1(null, null);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (Mass.Delete() == true)
                {
                    cls.Delete_Cash_Import(txtImportID.Text);
                    cls_Treasury_Movement.DeleteTreasuryMovement(TreasuryID);
                    PayType_trans_cls.Delete_PayType_trans(txtImportID.Text, txtTypeID.Text);

                    btnNew_Click_1(null, null);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

      

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            try
            {
                if (DGVPayType.SelectedRows.Count == 0)
                {
                    return;
                }
                if (Mass.Delete() == true)
                {
                    foreach (DataGridViewRow r in DGVPayType.SelectedRows)
                    {

                        DGVPayType.Rows.Remove(r);

                    }
                    SumPayType();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void combPayType_DropDown(object sender, EventArgs e)
        {

        }

        private void combPayType_TextChanged(object sender, EventArgs e)
        {
            combPayType.BackColor = Color.White;
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void combPayType_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (combPayType.SelectedIndex >= 0)
                {
                    txtPayValue.Focus();
                }

            }
        }

        private void txtStatement_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {

                if (e.KeyCode == Keys.Enter)
                {

                    if (combCurrency.SelectedIndex < 0)
                    {
                        MessageBox.Show("يرجي تحديد العملة", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        combCurrency.Focus();
                        return;
                    }
                    if (string.IsNullOrEmpty(txtCurrencyRate.Text.Trim()))
                    {
                        MessageBox.Show("يرجي تحديد سعر التعادل", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtCurrencyRate.Focus();
                        return;
                    }
                    if (combPayType.SelectedIndex < 0)
                    {
                        combPayType.BackColor = Color.Pink;
                        combPayType.Focus();
                        return;
                    }
                    if (txtPayValue.Text.Trim() == "")
                    {
                        txtPayValue.BackColor = Color.Pink;
                        txtPayValue.Focus();
                        return;
                    }
                    if (double.Parse(txtPayValue.Text) == 0)
                    {
                        txtPayValue.BackColor = Color.Pink;
                        txtPayValue.Focus();
                        return;
                    }
                    for (int i = 0; i < DGVPayType.Rows.Count; i++)
                    {
                        if (DGVPayType.Rows[i].Cells["PayTypeID"].Value.ToString() == combPayType.SelectedValue.ToString())
                        {
                            combPayType.BackColor = Color.Pink;
                            combPayType.Focus();
                            return;
                        }
                    }
                    AddDgv_PayType(DGVPayType, combPayType.SelectedValue.ToString(), combPayType.Text, (double.Parse(txtPayValue.Text) * double.Parse(txtCurrencyRate.Text)).ToString(), txtStatement.Text, combCurrency.SelectedValue.ToString(), combCurrency.Text, txtCurrencyRate.Text, txtPayValue.Text);
                    ClearPayType();
                    SumPayType();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



        private void AddDgv_PayType(DataGridView dgv, string PayTypeID, string PayTypeName, string PayValue, string Statement, string CurrencyID2, string CurrencyName2, string CurrencyRate2, string CurrencyPrice2)
        {
            try
            {
                dgv.Rows.Add();
                dgv.ClearSelection();
                dgv.Rows[dgv.Rows.Count - 1].Selected = true;
                var lastRows = dgv.Rows.Count - 1;
                dgv.Rows[lastRows].Cells["PayTypeID"].Value = PayTypeID;
                dgv.Rows[lastRows].Cells["PayTypeName"].Value = PayTypeName;
                dgv.Rows[lastRows].Cells["PayValue"].Value = PayValue;
                dgv.Rows[lastRows].Cells["Statement"].Value = Statement;
                dgv.Rows[lastRows].Cells["CurrencyID2"].Value = CurrencyID2;
                dgv.Rows[lastRows].Cells["CurrencyName2"].Value = CurrencyName2;
                dgv.Rows[lastRows].Cells["CurrencyRate2"].Value = CurrencyRate2;
                dgv.Rows[lastRows].Cells["CurrencyPrice2"].Value = CurrencyPrice2;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private void ClearPayType()
        {
            combPayType.Text = null;
            txtPayValue.Text = "";
            txtStatement.Text = "";
            combPayType.Focus();
        }

        private void SumPayType()
        {
            try
            {

                double Sum = 0;
                for (int i = 0; i < DGVPayType.Rows.Count; i++)
                {
                    Sum += Convert.ToDouble(DGVPayType.Rows[i].Cells["PayValue"].Value.ToString());
                }
                txtSumPayType.Text = Sum.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }





        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            txtPayValue.BackColor = Color.White;
        }

        private void txtPayValue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!string.IsNullOrEmpty(txtPayValue.Text.Trim()))
                {
                    txtStatement.Focus();
                }

            }
        }
        int pos = 0;
        private void button6_Click(object sender, EventArgs e)
        {
            try
            {

                pos--;
                if (pos >= 0)
                {
                    ShowData(pos);
                }
                else
                {
                    MessageBox.Show("هذا اضغر سند ");

                }
            }
            catch
            {
                return;
            }
        }




        public void ShowData(int index)
        {
            try
            {
                DataTable dt = cls.Search_Cash_Import(dtShow.Rows[index]["ImportID"].ToString());
                DataRow Dr = dt.Rows[0];
                txtImportID.Text = Dr["ImportID"].ToString();
                D1.Value = Convert.ToDateTime(Dr["MayDate"]);
                txtRemarks.Text = Dr["Remarks"].ToString();
                TreasuryID = Dr["TreasuryID"].ToString();


                DGVPayType.DataSource = null;
                DGVPayType.Rows.Clear();
                DataTable dt_paytype_trans = PayType_trans_cls.Details_PayType_trans(txtImportID.Text, txtTypeID.Text);
                for (int i = 0; i < dt_paytype_trans.Rows.Count; i++)
                {
                    AddDgv_PayType(DGVPayType, dt_paytype_trans.Rows[i]["PayTypeID"].ToString(), dt_paytype_trans.Rows[i]["PayTypeName"].ToString(), dt_paytype_trans.Rows[i]["Debit"].ToString(), dt_paytype_trans.Rows[i]["Statement"].ToString(), dt_paytype_trans.Rows[i]["CurrencyID"].ToString(), dt_paytype_trans.Rows[i]["CurrencyName"].ToString(), dt_paytype_trans.Rows[i]["CurrencyRate"].ToString(), dt_paytype_trans.Rows[i]["CurrencyPrice"].ToString());
                }
                SumPayType();






                btnSave.Enabled = false;
                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;
            }
            catch
            {
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                pos++;
                if (pos < dtShow.Rows.Count)
                {
                    ShowData(pos);
                }
                else
                {
                    MessageBox.Show("هذا اكبر سند  ");
                    pos = dtShow.Rows.Count;
                    //ShowData(pos);
                }
            }
            catch
            {
                return;
            }
        }

        private void DGVPayType_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

            try
            {
                if (DGVPayType.Rows.Count == 0)
                {
                    return;
                }
                double pay = double.Parse(DGVPayType.CurrentRow.Cells["CurrencyPrice2"].Value.ToString());
                double Rat = double.Parse(DGVPayType.CurrentRow.Cells["CurrencyRate2"].Value.ToString());
                DGVPayType.CurrentRow.Cells["PayValue"].Value = pay * Rat;
                SumPayType();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtRemarks_TextChanged(object sender, EventArgs e)
        {
            txtRemarks.BackColor = Color.White;
        }

        private void CashImport_frm_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click_1(null, null);
                }
                if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        toolStripButton3_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click_1(null, null);
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    if (btnDelete.Enabled == true)
                    {
                        btnDelete_Click_1(null, null);
                    }
                }
                //if (e.KeyCode == Keys.F5)
                //{
                //    btnGetData_Click(null, null);
                //}

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion
        }

        private void combCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (combCurrency.SelectedIndex > -1)
                {
                    Currency_cls crrcls = new Currency_cls();
                    DataTable dt = crrcls.Details_Currency(combCurrency.SelectedValue.ToString());
                    if (dt.Rows.Count > 0)
                    {
                        txtCurrencyRate.Text = dt.Rows[0]["CurrencyRate"].ToString();
                    }
                }
            }
            catch
            {

                return;
            }

        }

        private void txtCurrencyRate_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCurrencyRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }
    }
}
