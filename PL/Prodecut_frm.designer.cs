﻿namespace ByStro.PL
{
    partial class Prodecut_frm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Prodecut_frm));
            this.txtBarcode3 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtThreeUnitBarcode = new DevExpress.XtraEditors.ButtonEdit();
            this.RbUnitDefoult3 = new System.Windows.Forms.RadioButton();
            this.label19 = new System.Windows.Forms.Label();
            this.txtProdecutBayPrice3 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtThreeUnitOperating = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.txtThreeUnitPrice3 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtThreeUnitPrice2 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtThreeUnitPrice1 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.combThreeUnit = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.spinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.txtFiestUnitBarcode = new DevExpress.XtraEditors.ButtonEdit();
            this.RbUnitDefoult1 = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtProdecutBayPrice = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtFiestUnitOperating = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtFiestUnitPrice3 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtFiestUnitPrice2 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtFiestUnitPrice1 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.combFiestUnit = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtSecoundUnitBarcode = new DevExpress.XtraEditors.ButtonEdit();
            this.RbUnitDefoult2 = new System.Windows.Forms.RadioButton();
            this.label29 = new System.Windows.Forms.Label();
            this.txtProdecutBayPrice2 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtSecoundUnitOperating = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtSecoundUnitPrice3 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtSecoundUnitPrice2 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtSecoundUnitPrice1 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.combSecoundUnit = new System.Windows.Forms.ComboBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.chk_HasSize = new DevExpress.XtraEditors.CheckEdit();
            this.chk_HasSerial = new DevExpress.XtraEditors.CheckEdit();
            this.chk_HasExpire = new DevExpress.XtraEditors.CheckEdit();
            this.chk_HasColor = new DevExpress.XtraEditors.CheckEdit();
            this.chProductService = new System.Windows.Forms.CheckBox();
            this.chStatus = new System.Windows.Forms.CheckBox();
            this.txtDiscoundSale = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtDiscoundBay = new System.Windows.Forms.TextBox();
            this.txtRequestLimit = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.txtProdecutName = new System.Windows.Forms.TextBox();
            this.txtCategoryID = new System.Windows.Forms.TextBox();
            this.txtProdecutLocation = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCompany = new System.Windows.Forms.TextBox();
            this.combGroup = new System.Windows.Forms.ComboBox();
            this.txtProdecutID = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.btnGetData = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tabPage3.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtThreeUnitBarcode.Properties)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFiestUnitBarcode.Properties)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecoundUnitBarcode.Properties)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk_HasSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_HasSerial.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_HasExpire.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_HasColor.Properties)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtBarcode3
            // 
            this.txtBarcode3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBarcode3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBarcode3.Location = new System.Drawing.Point(450, 748);
            this.txtBarcode3.MaxLength = 80;
            this.txtBarcode3.Name = "txtBarcode3";
            this.txtBarcode3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtBarcode3.Size = new System.Drawing.Size(234, 23);
            this.txtBarcode3.TabIndex = 516;
            this.txtBarcode3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(690, 750);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label13.Size = new System.Drawing.Size(56, 16);
            this.label13.TabIndex = 517;
            this.label13.Text = "الباركود :";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.White;
            this.tabPage3.Controls.Add(this.groupBox6);
            this.tabPage3.Controls.Add(this.groupBox4);
            this.tabPage3.Controls.Add(this.groupBox3);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(712, 453);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "وحدات الصنف واسعارها";
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.White;
            this.groupBox6.Controls.Add(this.txtThreeUnitBarcode);
            this.groupBox6.Controls.Add(this.RbUnitDefoult3);
            this.groupBox6.Controls.Add(this.label19);
            this.groupBox6.Controls.Add(this.txtProdecutBayPrice3);
            this.groupBox6.Controls.Add(this.label25);
            this.groupBox6.Controls.Add(this.txtThreeUnitOperating);
            this.groupBox6.Controls.Add(this.label30);
            this.groupBox6.Controls.Add(this.label31);
            this.groupBox6.Controls.Add(this.txtThreeUnitPrice3);
            this.groupBox6.Controls.Add(this.label34);
            this.groupBox6.Controls.Add(this.txtThreeUnitPrice2);
            this.groupBox6.Controls.Add(this.label35);
            this.groupBox6.Controls.Add(this.txtThreeUnitPrice1);
            this.groupBox6.Controls.Add(this.label36);
            this.groupBox6.Controls.Add(this.combThreeUnit);
            this.groupBox6.Location = new System.Drawing.Point(3, 291);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(703, 142);
            this.groupBox6.TabIndex = 2;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "الوحدة الثالثه";
            // 
            // txtThreeUnitBarcode
            // 
            this.txtThreeUnitBarcode.Location = new System.Drawing.Point(386, 79);
            this.txtThreeUnitBarcode.Name = "txtThreeUnitBarcode";
            this.txtThreeUnitBarcode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Search, "", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "توليد براكود تلقائي", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.txtThreeUnitBarcode.Size = new System.Drawing.Size(209, 20);
            this.txtThreeUnitBarcode.TabIndex = 538;
            this.txtThreeUnitBarcode.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.txtFiestUnitBarcode_ButtonClick);
            this.txtThreeUnitBarcode.TextChanged += new System.EventHandler(this.txtThreeUnitBarcode_TextChanged);
            // 
            // RbUnitDefoult3
            // 
            this.RbUnitDefoult3.AutoSize = true;
            this.RbUnitDefoult3.Location = new System.Drawing.Point(383, 26);
            this.RbUnitDefoult3.Name = "RbUnitDefoult3";
            this.RbUnitDefoult3.Size = new System.Drawing.Size(74, 21);
            this.RbUnitDefoult3.TabIndex = 537;
            this.RbUnitDefoult3.TabStop = true;
            this.RbUnitDefoult3.Text = "افتراضي";
            this.RbUnitDefoult3.UseVisualStyleBackColor = true;
            this.RbUnitDefoult3.CheckedChanged += new System.EventHandler(this.RbUnitDefoult3_CheckedChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(303, 57);
            this.label19.Name = "label19";
            this.label19.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label19.Size = new System.Drawing.Size(70, 16);
            this.label19.TabIndex = 532;
            this.label19.Text = "سعر الشراء";
            // 
            // txtProdecutBayPrice3
            // 
            this.txtProdecutBayPrice3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtProdecutBayPrice3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProdecutBayPrice3.Location = new System.Drawing.Point(195, 52);
            this.txtProdecutBayPrice3.MaxLength = 80;
            this.txtProdecutBayPrice3.Name = "txtProdecutBayPrice3";
            this.txtProdecutBayPrice3.ReadOnly = true;
            this.txtProdecutBayPrice3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtProdecutBayPrice3.Size = new System.Drawing.Size(102, 23);
            this.txtProdecutBayPrice3.TabIndex = 531;
            this.txtProdecutBayPrice3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtProdecutBayPrice3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Allprice);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(605, 54);
            this.label25.Name = "label25";
            this.label25.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label25.Size = new System.Drawing.Size(80, 16);
            this.label25.TabIndex = 530;
            this.label25.Text = "معدل التحويل";
            // 
            // txtThreeUnitOperating
            // 
            this.txtThreeUnitOperating.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtThreeUnitOperating.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThreeUnitOperating.Location = new System.Drawing.Point(386, 52);
            this.txtThreeUnitOperating.MaxLength = 10;
            this.txtThreeUnitOperating.Name = "txtThreeUnitOperating";
            this.txtThreeUnitOperating.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtThreeUnitOperating.Size = new System.Drawing.Size(209, 23);
            this.txtThreeUnitOperating.TabIndex = 1;
            this.txtThreeUnitOperating.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtThreeUnitOperating.TextChanged += new System.EventHandler(this.txtThreeUnitOperating_TextChanged);
            this.txtThreeUnitOperating.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Allprice);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(605, 80);
            this.label30.Name = "label30";
            this.label30.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label30.Size = new System.Drawing.Size(47, 16);
            this.label30.TabIndex = 526;
            this.label30.Text = "الباركود";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(113, 78);
            this.label31.Name = "label31";
            this.label31.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label31.Size = new System.Drawing.Size(64, 16);
            this.label31.TabIndex = 524;
            this.label31.Text = "سعر بيع 3";
            // 
            // txtThreeUnitPrice3
            // 
            this.txtThreeUnitPrice3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtThreeUnitPrice3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThreeUnitPrice3.Location = new System.Drawing.Point(6, 78);
            this.txtThreeUnitPrice3.MaxLength = 80;
            this.txtThreeUnitPrice3.Name = "txtThreeUnitPrice3";
            this.txtThreeUnitPrice3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtThreeUnitPrice3.Size = new System.Drawing.Size(102, 23);
            this.txtThreeUnitPrice3.TabIndex = 5;
            this.txtThreeUnitPrice3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtThreeUnitPrice3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Allprice);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(113, 52);
            this.label34.Name = "label34";
            this.label34.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label34.Size = new System.Drawing.Size(64, 16);
            this.label34.TabIndex = 522;
            this.label34.Text = "سعر بيع 2";
            // 
            // txtThreeUnitPrice2
            // 
            this.txtThreeUnitPrice2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtThreeUnitPrice2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThreeUnitPrice2.Location = new System.Drawing.Point(6, 52);
            this.txtThreeUnitPrice2.MaxLength = 80;
            this.txtThreeUnitPrice2.Name = "txtThreeUnitPrice2";
            this.txtThreeUnitPrice2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtThreeUnitPrice2.Size = new System.Drawing.Size(102, 23);
            this.txtThreeUnitPrice2.TabIndex = 4;
            this.txtThreeUnitPrice2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtThreeUnitPrice2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Allprice);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(113, 30);
            this.label35.Name = "label35";
            this.label35.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label35.Size = new System.Drawing.Size(64, 16);
            this.label35.TabIndex = 520;
            this.label35.Text = "سعر بيع 1";
            // 
            // txtThreeUnitPrice1
            // 
            this.txtThreeUnitPrice1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtThreeUnitPrice1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThreeUnitPrice1.Location = new System.Drawing.Point(6, 26);
            this.txtThreeUnitPrice1.MaxLength = 80;
            this.txtThreeUnitPrice1.Name = "txtThreeUnitPrice1";
            this.txtThreeUnitPrice1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtThreeUnitPrice1.Size = new System.Drawing.Size(102, 23);
            this.txtThreeUnitPrice1.TabIndex = 3;
            this.txtThreeUnitPrice1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtThreeUnitPrice1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Allprice);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label36.Location = new System.Drawing.Point(605, 28);
            this.label36.Name = "label36";
            this.label36.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label36.Size = new System.Drawing.Size(78, 16);
            this.label36.TabIndex = 514;
            this.label36.Text = "وحدة القياس";
            this.toolTip1.SetToolTip(this.label36, "اضافة وحدة قياس جديدة");
            this.label36.Click += new System.EventHandler(this.label36_Click);
            // 
            // combThreeUnit
            // 
            this.combThreeUnit.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combThreeUnit.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combThreeUnit.FormattingEnabled = true;
            this.combThreeUnit.Location = new System.Drawing.Point(463, 26);
            this.combThreeUnit.Name = "combThreeUnit";
            this.combThreeUnit.Size = new System.Drawing.Size(132, 24);
            this.combThreeUnit.TabIndex = 0;
            this.combThreeUnit.TextChanged += new System.EventHandler(this.combThreeUnit_TextChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.White;
            this.groupBox4.Controls.Add(this.spinEdit1);
            this.groupBox4.Controls.Add(this.txtFiestUnitBarcode);
            this.groupBox4.Controls.Add(this.RbUnitDefoult1);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.txtProdecutBayPrice);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.txtFiestUnitOperating);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.txtFiestUnitPrice3);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.txtFiestUnitPrice2);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.txtFiestUnitPrice1);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.combFiestUnit);
            this.groupBox4.Location = new System.Drawing.Point(3, 10);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(703, 137);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "الوحدة الأولي";
            // 
            // spinEdit1
            // 
            this.spinEdit1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit1.Location = new System.Drawing.Point(386, 103);
            this.spinEdit1.Name = "spinEdit1";
            this.spinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit1.Size = new System.Drawing.Size(209, 20);
            this.spinEdit1.TabIndex = 539;
            // 
            // txtFiestUnitBarcode
            // 
            this.txtFiestUnitBarcode.Location = new System.Drawing.Point(386, 79);
            this.txtFiestUnitBarcode.Name = "txtFiestUnitBarcode";
            this.txtFiestUnitBarcode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Search, "", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "توليد براكود تلقائي", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.txtFiestUnitBarcode.Size = new System.Drawing.Size(209, 20);
            this.txtFiestUnitBarcode.TabIndex = 538;
            this.txtFiestUnitBarcode.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.txtFiestUnitBarcode_ButtonClick);
            this.txtFiestUnitBarcode.TextChanged += new System.EventHandler(this.txtFiestUnitBarcode_TextChanged);
            // 
            // RbUnitDefoult1
            // 
            this.RbUnitDefoult1.AutoSize = true;
            this.RbUnitDefoult1.Checked = true;
            this.RbUnitDefoult1.Location = new System.Drawing.Point(383, 26);
            this.RbUnitDefoult1.Name = "RbUnitDefoult1";
            this.RbUnitDefoult1.Size = new System.Drawing.Size(74, 21);
            this.RbUnitDefoult1.TabIndex = 537;
            this.RbUnitDefoult1.TabStop = true;
            this.RbUnitDefoult1.Text = "افتراضي";
            this.RbUnitDefoult1.UseVisualStyleBackColor = true;
            this.RbUnitDefoult1.CheckedChanged += new System.EventHandler(this.RbUnitDefoult1_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(601, 106);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(98, 16);
            this.label1.TabIndex = 532;
            this.label1.Text = "الرصيد الافتتاحي";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(303, 55);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(70, 16);
            this.label7.TabIndex = 532;
            this.label7.Text = "سعر الشراء";
            // 
            // txtProdecutBayPrice
            // 
            this.txtProdecutBayPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtProdecutBayPrice.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProdecutBayPrice.Location = new System.Drawing.Point(195, 52);
            this.txtProdecutBayPrice.MaxLength = 80;
            this.txtProdecutBayPrice.Name = "txtProdecutBayPrice";
            this.txtProdecutBayPrice.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtProdecutBayPrice.Size = new System.Drawing.Size(102, 23);
            this.txtProdecutBayPrice.TabIndex = 2;
            this.txtProdecutBayPrice.Text = "0";
            this.txtProdecutBayPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtProdecutBayPrice.TextChanged += new System.EventHandler(this.txtItemBayPrice1_TextChanged);
            this.txtProdecutBayPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Allprice);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(601, 54);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(80, 16);
            this.label8.TabIndex = 530;
            this.label8.Text = "معدل التحويل";
            // 
            // txtFiestUnitOperating
            // 
            this.txtFiestUnitOperating.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFiestUnitOperating.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFiestUnitOperating.Location = new System.Drawing.Point(386, 52);
            this.txtFiestUnitOperating.MaxLength = 10;
            this.txtFiestUnitOperating.Name = "txtFiestUnitOperating";
            this.txtFiestUnitOperating.ReadOnly = true;
            this.txtFiestUnitOperating.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtFiestUnitOperating.Size = new System.Drawing.Size(209, 23);
            this.txtFiestUnitOperating.TabIndex = 529;
            this.txtFiestUnitOperating.Text = "1";
            this.txtFiestUnitOperating.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFiestUnitOperating.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Allprice);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(601, 80);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(47, 16);
            this.label9.TabIndex = 526;
            this.label9.Text = "الباركود";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(114, 78);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(64, 16);
            this.label10.TabIndex = 524;
            this.label10.Text = "سعر بيع 3";
            // 
            // txtFiestUnitPrice3
            // 
            this.txtFiestUnitPrice3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFiestUnitPrice3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFiestUnitPrice3.Location = new System.Drawing.Point(6, 78);
            this.txtFiestUnitPrice3.MaxLength = 80;
            this.txtFiestUnitPrice3.Name = "txtFiestUnitPrice3";
            this.txtFiestUnitPrice3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtFiestUnitPrice3.Size = new System.Drawing.Size(102, 23);
            this.txtFiestUnitPrice3.TabIndex = 5;
            this.txtFiestUnitPrice3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtFiestUnitPrice3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Allprice);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(114, 52);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(64, 16);
            this.label11.TabIndex = 522;
            this.label11.Text = "سعر بيع 2";
            // 
            // txtFiestUnitPrice2
            // 
            this.txtFiestUnitPrice2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFiestUnitPrice2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFiestUnitPrice2.Location = new System.Drawing.Point(6, 52);
            this.txtFiestUnitPrice2.MaxLength = 80;
            this.txtFiestUnitPrice2.Name = "txtFiestUnitPrice2";
            this.txtFiestUnitPrice2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtFiestUnitPrice2.Size = new System.Drawing.Size(102, 23);
            this.txtFiestUnitPrice2.TabIndex = 4;
            this.txtFiestUnitPrice2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtFiestUnitPrice2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Allprice);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(114, 30);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label12.Size = new System.Drawing.Size(64, 16);
            this.label12.TabIndex = 520;
            this.label12.Text = "سعر بيع 1";
            // 
            // txtFiestUnitPrice1
            // 
            this.txtFiestUnitPrice1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFiestUnitPrice1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFiestUnitPrice1.Location = new System.Drawing.Point(6, 26);
            this.txtFiestUnitPrice1.MaxLength = 80;
            this.txtFiestUnitPrice1.Name = "txtFiestUnitPrice1";
            this.txtFiestUnitPrice1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtFiestUnitPrice1.Size = new System.Drawing.Size(102, 23);
            this.txtFiestUnitPrice1.TabIndex = 3;
            this.txtFiestUnitPrice1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtFiestUnitPrice1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Allprice);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label14.Location = new System.Drawing.Point(601, 28);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(78, 16);
            this.label14.TabIndex = 514;
            this.label14.Text = "وحدة القياس";
            this.toolTip1.SetToolTip(this.label14, "اضافة وحدة قياس جديدة");
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // combFiestUnit
            // 
            this.combFiestUnit.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combFiestUnit.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combFiestUnit.FormattingEnabled = true;
            this.combFiestUnit.Location = new System.Drawing.Point(463, 26);
            this.combFiestUnit.Name = "combFiestUnit";
            this.combFiestUnit.Size = new System.Drawing.Size(132, 24);
            this.combFiestUnit.TabIndex = 0;
            this.combFiestUnit.TextChanged += new System.EventHandler(this.combFiestUnit_TextChanged);
            this.combFiestUnit.VisibleChanged += new System.EventHandler(this.combFiestUnit_VisibleChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.White;
            this.groupBox3.Controls.Add(this.txtSecoundUnitBarcode);
            this.groupBox3.Controls.Add(this.RbUnitDefoult2);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.txtProdecutBayPrice2);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.txtSecoundUnitOperating);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.txtSecoundUnitPrice3);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.txtSecoundUnitPrice2);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.txtSecoundUnitPrice1);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.combSecoundUnit);
            this.groupBox3.Location = new System.Drawing.Point(3, 149);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(703, 138);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "الوحدة الثانية";
            // 
            // txtSecoundUnitBarcode
            // 
            this.txtSecoundUnitBarcode.Location = new System.Drawing.Point(386, 79);
            this.txtSecoundUnitBarcode.Name = "txtSecoundUnitBarcode";
            this.txtSecoundUnitBarcode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Search, "", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "توليد براكود تلقائي", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.txtSecoundUnitBarcode.Size = new System.Drawing.Size(209, 20);
            this.txtSecoundUnitBarcode.TabIndex = 538;
            this.txtSecoundUnitBarcode.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.txtFiestUnitBarcode_ButtonClick);
            this.txtSecoundUnitBarcode.TextChanged += new System.EventHandler(this.txtSecoundUnitBarcode_TextChanged);
            // 
            // RbUnitDefoult2
            // 
            this.RbUnitDefoult2.AutoSize = true;
            this.RbUnitDefoult2.Location = new System.Drawing.Point(383, 26);
            this.RbUnitDefoult2.Name = "RbUnitDefoult2";
            this.RbUnitDefoult2.Size = new System.Drawing.Size(74, 21);
            this.RbUnitDefoult2.TabIndex = 537;
            this.RbUnitDefoult2.TabStop = true;
            this.RbUnitDefoult2.Text = "افتراضي";
            this.RbUnitDefoult2.UseVisualStyleBackColor = true;
            this.RbUnitDefoult2.CheckedChanged += new System.EventHandler(this.RbUnitDefoult2_CheckedChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(303, 56);
            this.label29.Name = "label29";
            this.label29.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label29.Size = new System.Drawing.Size(70, 16);
            this.label29.TabIndex = 532;
            this.label29.Text = "سعر الشراء";
            // 
            // txtProdecutBayPrice2
            // 
            this.txtProdecutBayPrice2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtProdecutBayPrice2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProdecutBayPrice2.Location = new System.Drawing.Point(195, 52);
            this.txtProdecutBayPrice2.MaxLength = 80;
            this.txtProdecutBayPrice2.Name = "txtProdecutBayPrice2";
            this.txtProdecutBayPrice2.ReadOnly = true;
            this.txtProdecutBayPrice2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtProdecutBayPrice2.Size = new System.Drawing.Size(102, 23);
            this.txtProdecutBayPrice2.TabIndex = 3;
            this.txtProdecutBayPrice2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtProdecutBayPrice2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Allprice);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(603, 55);
            this.label21.Name = "label21";
            this.label21.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label21.Size = new System.Drawing.Size(80, 16);
            this.label21.TabIndex = 530;
            this.label21.Text = "معدل التحويل";
            // 
            // txtSecoundUnitOperating
            // 
            this.txtSecoundUnitOperating.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSecoundUnitOperating.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecoundUnitOperating.Location = new System.Drawing.Point(386, 52);
            this.txtSecoundUnitOperating.MaxLength = 10;
            this.txtSecoundUnitOperating.Name = "txtSecoundUnitOperating";
            this.txtSecoundUnitOperating.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSecoundUnitOperating.Size = new System.Drawing.Size(209, 23);
            this.txtSecoundUnitOperating.TabIndex = 1;
            this.txtSecoundUnitOperating.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSecoundUnitOperating.TextChanged += new System.EventHandler(this.txtSecoundUnitOperating_TextChanged);
            this.txtSecoundUnitOperating.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Allprice);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(603, 81);
            this.label22.Name = "label22";
            this.label22.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label22.Size = new System.Drawing.Size(47, 16);
            this.label22.TabIndex = 526;
            this.label22.Text = "الباركود";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(114, 78);
            this.label23.Name = "label23";
            this.label23.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label23.Size = new System.Drawing.Size(64, 16);
            this.label23.TabIndex = 524;
            this.label23.Text = "سعر بيع 3";
            // 
            // txtSecoundUnitPrice3
            // 
            this.txtSecoundUnitPrice3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSecoundUnitPrice3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecoundUnitPrice3.Location = new System.Drawing.Point(6, 78);
            this.txtSecoundUnitPrice3.MaxLength = 80;
            this.txtSecoundUnitPrice3.Name = "txtSecoundUnitPrice3";
            this.txtSecoundUnitPrice3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSecoundUnitPrice3.Size = new System.Drawing.Size(102, 23);
            this.txtSecoundUnitPrice3.TabIndex = 6;
            this.txtSecoundUnitPrice3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtSecoundUnitPrice3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Allprice);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(114, 52);
            this.label24.Name = "label24";
            this.label24.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label24.Size = new System.Drawing.Size(64, 16);
            this.label24.TabIndex = 522;
            this.label24.Text = "سعر بيع 2";
            // 
            // txtSecoundUnitPrice2
            // 
            this.txtSecoundUnitPrice2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSecoundUnitPrice2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecoundUnitPrice2.Location = new System.Drawing.Point(6, 52);
            this.txtSecoundUnitPrice2.MaxLength = 80;
            this.txtSecoundUnitPrice2.Name = "txtSecoundUnitPrice2";
            this.txtSecoundUnitPrice2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSecoundUnitPrice2.Size = new System.Drawing.Size(102, 23);
            this.txtSecoundUnitPrice2.TabIndex = 5;
            this.txtSecoundUnitPrice2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtSecoundUnitPrice2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Allprice);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(114, 30);
            this.label26.Name = "label26";
            this.label26.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label26.Size = new System.Drawing.Size(64, 16);
            this.label26.TabIndex = 520;
            this.label26.Text = "سعر بيع 1";
            // 
            // txtSecoundUnitPrice1
            // 
            this.txtSecoundUnitPrice1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSecoundUnitPrice1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecoundUnitPrice1.Location = new System.Drawing.Point(6, 26);
            this.txtSecoundUnitPrice1.MaxLength = 80;
            this.txtSecoundUnitPrice1.Name = "txtSecoundUnitPrice1";
            this.txtSecoundUnitPrice1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSecoundUnitPrice1.Size = new System.Drawing.Size(102, 23);
            this.txtSecoundUnitPrice1.TabIndex = 4;
            this.txtSecoundUnitPrice1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtSecoundUnitPrice1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Allprice);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label28.Location = new System.Drawing.Point(603, 29);
            this.label28.Name = "label28";
            this.label28.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label28.Size = new System.Drawing.Size(78, 16);
            this.label28.TabIndex = 514;
            this.label28.Text = "وحدة القياس";
            this.toolTip1.SetToolTip(this.label28, "اضافة وحدة قياس جديدة");
            this.label28.Click += new System.EventHandler(this.label28_Click);
            // 
            // combSecoundUnit
            // 
            this.combSecoundUnit.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combSecoundUnit.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combSecoundUnit.FormattingEnabled = true;
            this.combSecoundUnit.Location = new System.Drawing.Point(463, 26);
            this.combSecoundUnit.Name = "combSecoundUnit";
            this.combSecoundUnit.Size = new System.Drawing.Size(132, 24);
            this.combSecoundUnit.TabIndex = 0;
            this.combSecoundUnit.TextChanged += new System.EventHandler(this.combSecoundUnit_TextChanged);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.chk_HasSize);
            this.groupBox8.Controls.Add(this.chk_HasSerial);
            this.groupBox8.Controls.Add(this.chk_HasExpire);
            this.groupBox8.Controls.Add(this.chk_HasColor);
            this.groupBox8.Controls.Add(this.chProductService);
            this.groupBox8.Controls.Add(this.chStatus);
            this.groupBox8.Controls.Add(this.txtDiscoundSale);
            this.groupBox8.Controls.Add(this.label40);
            this.groupBox8.Controls.Add(this.label18);
            this.groupBox8.Controls.Add(this.txtDiscoundBay);
            this.groupBox8.Controls.Add(this.txtRequestLimit);
            this.groupBox8.Controls.Add(this.label39);
            this.groupBox8.Controls.Add(this.txtProdecutName);
            this.groupBox8.Controls.Add(this.txtCategoryID);
            this.groupBox8.Controls.Add(this.txtProdecutLocation);
            this.groupBox8.Controls.Add(this.label3);
            this.groupBox8.Controls.Add(this.label6);
            this.groupBox8.Controls.Add(this.label5);
            this.groupBox8.Controls.Add(this.txtCompany);
            this.groupBox8.Controls.Add(this.combGroup);
            this.groupBox8.Controls.Add(this.txtProdecutID);
            this.groupBox8.Controls.Add(this.label15);
            this.groupBox8.Controls.Add(this.Label2);
            this.groupBox8.Controls.Add(this.button1);
            this.groupBox8.Location = new System.Drawing.Point(6, 16);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(703, 429);
            this.groupBox8.TabIndex = 472;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "بيانات عاملة للصنف";
            // 
            // chk_HasSize
            // 
            this.chk_HasSize.Location = new System.Drawing.Point(575, 358);
            this.chk_HasSize.Name = "chk_HasSize";
            this.chk_HasSize.Properties.Caption = "له حجم";
            this.chk_HasSize.Size = new System.Drawing.Size(75, 20);
            this.chk_HasSize.TabIndex = 478;
            // 
            // chk_HasSerial
            // 
            this.chk_HasSerial.Location = new System.Drawing.Point(489, 402);
            this.chk_HasSerial.Name = "chk_HasSerial";
            this.chk_HasSerial.Properties.Caption = "له رقم تسلسلي";
            this.chk_HasSerial.Size = new System.Drawing.Size(161, 20);
            this.chk_HasSerial.TabIndex = 478;
            // 
            // chk_HasExpire
            // 
            this.chk_HasExpire.Location = new System.Drawing.Point(575, 380);
            this.chk_HasExpire.Name = "chk_HasExpire";
            this.chk_HasExpire.Properties.Caption = "له صلاحيه";
            this.chk_HasExpire.Size = new System.Drawing.Size(75, 20);
            this.chk_HasExpire.TabIndex = 478;
            // 
            // chk_HasColor
            // 
            this.chk_HasColor.Location = new System.Drawing.Point(575, 336);
            this.chk_HasColor.Name = "chk_HasColor";
            this.chk_HasColor.Properties.Caption = "له لون";
            this.chk_HasColor.Size = new System.Drawing.Size(75, 20);
            this.chk_HasColor.TabIndex = 478;
            // 
            // chProductService
            // 
            this.chProductService.AutoSize = true;
            this.chProductService.Location = new System.Drawing.Point(78, 52);
            this.chProductService.Name = "chProductService";
            this.chProductService.Size = new System.Drawing.Size(105, 21);
            this.chProductService.TabIndex = 477;
            this.chProductService.Text = "الصنف خدمي";
            this.chProductService.UseVisualStyleBackColor = true;
            // 
            // chStatus
            // 
            this.chStatus.AutoSize = true;
            this.chStatus.Checked = true;
            this.chStatus.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chStatus.Location = new System.Drawing.Point(212, 52);
            this.chStatus.Name = "chStatus";
            this.chStatus.Size = new System.Drawing.Size(96, 21);
            this.chStatus.TabIndex = 476;
            this.chStatus.Text = "الصنف نشط";
            this.chStatus.UseVisualStyleBackColor = true;
            // 
            // txtDiscoundSale
            // 
            this.txtDiscoundSale.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDiscoundSale.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiscoundSale.Location = new System.Drawing.Point(78, 302);
            this.txtDiscoundSale.MaxLength = 10;
            this.txtDiscoundSale.Name = "txtDiscoundSale";
            this.txtDiscoundSale.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtDiscoundSale.Size = new System.Drawing.Size(267, 23);
            this.txtDiscoundSale.TabIndex = 7;
            this.txtDiscoundSale.Text = "0";
            this.txtDiscoundSale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(227, 279);
            this.label40.Name = "label40";
            this.label40.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label40.Size = new System.Drawing.Size(114, 16);
            this.label40.TabIndex = 464;
            this.label40.Text = "خصم عند البيع % :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(270, 228);
            this.label18.Name = "label18";
            this.label18.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label18.Size = new System.Drawing.Size(70, 16);
            this.label18.TabIndex = 475;
            this.label18.Text = "حد الطلب :";
            // 
            // txtDiscoundBay
            // 
            this.txtDiscoundBay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDiscoundBay.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiscoundBay.Location = new System.Drawing.Point(371, 302);
            this.txtDiscoundBay.MaxLength = 10;
            this.txtDiscoundBay.Name = "txtDiscoundBay";
            this.txtDiscoundBay.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtDiscoundBay.Size = new System.Drawing.Size(279, 23);
            this.txtDiscoundBay.TabIndex = 6;
            this.txtDiscoundBay.Text = "0";
            this.txtDiscoundBay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtRequestLimit
            // 
            this.txtRequestLimit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRequestLimit.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRequestLimit.Location = new System.Drawing.Point(78, 248);
            this.txtRequestLimit.MaxLength = 80;
            this.txtRequestLimit.Name = "txtRequestLimit";
            this.txtRequestLimit.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtRequestLimit.Size = new System.Drawing.Size(267, 23);
            this.txtRequestLimit.TabIndex = 5;
            this.txtRequestLimit.Text = "0";
            this.txtRequestLimit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(526, 279);
            this.label39.Name = "label39";
            this.label39.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label39.Size = new System.Drawing.Size(123, 16);
            this.label39.TabIndex = 462;
            this.label39.Text = "خصم عند الشراء % :";
            // 
            // txtProdecutName
            // 
            this.txtProdecutName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtProdecutName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProdecutName.Location = new System.Drawing.Point(78, 100);
            this.txtProdecutName.Margin = new System.Windows.Forms.Padding(4);
            this.txtProdecutName.Name = "txtProdecutName";
            this.txtProdecutName.Size = new System.Drawing.Size(572, 23);
            this.txtProdecutName.TabIndex = 0;
            this.txtProdecutName.TextChanged += new System.EventHandler(this.txtItemName_TextChanged);
            // 
            // txtCategoryID
            // 
            this.txtCategoryID.BackColor = System.Drawing.Color.White;
            this.txtCategoryID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCategoryID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCategoryID.Location = new System.Drawing.Point(660, 100);
            this.txtCategoryID.Margin = new System.Windows.Forms.Padding(4);
            this.txtCategoryID.Name = "txtCategoryID";
            this.txtCategoryID.ReadOnly = true;
            this.txtCategoryID.Size = new System.Drawing.Size(10, 23);
            this.txtCategoryID.TabIndex = 470;
            this.txtCategoryID.Visible = false;
            // 
            // txtProdecutLocation
            // 
            this.txtProdecutLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtProdecutLocation.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProdecutLocation.Location = new System.Drawing.Point(78, 200);
            this.txtProdecutLocation.MaxLength = 80;
            this.txtProdecutLocation.Name = "txtProdecutLocation";
            this.txtProdecutLocation.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtProdecutLocation.Size = new System.Drawing.Size(572, 23);
            this.txtProdecutLocation.TabIndex = 3;
            this.txtProdecutLocation.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(564, 179);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(85, 16);
            this.label3.TabIndex = 439;
            this.label3.Text = "مكان التواجد :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(572, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 17);
            this.label6.TabIndex = 444;
            this.label6.Text = "رقم الصنف :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(544, 228);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(103, 16);
            this.label5.TabIndex = 443;
            this.label5.Text = "الشركة المصنعة :";
            // 
            // txtCompany
            // 
            this.txtCompany.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCompany.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCompany.Location = new System.Drawing.Point(371, 250);
            this.txtCompany.MaxLength = 80;
            this.txtCompany.Name = "txtCompany";
            this.txtCompany.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCompany.Size = new System.Drawing.Size(279, 23);
            this.txtCompany.TabIndex = 4;
            this.txtCompany.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // combGroup
            // 
            this.combGroup.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combGroup.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combGroup.FormattingEnabled = true;
            this.combGroup.Location = new System.Drawing.Point(231, 150);
            this.combGroup.Name = "combGroup";
            this.combGroup.Size = new System.Drawing.Size(419, 24);
            this.combGroup.TabIndex = 1;
            this.combGroup.SelectedIndexChanged += new System.EventHandler(this.combGroup_SelectedIndexChanged);
            this.combGroup.TextChanged += new System.EventHandler(this.combGroup_TextChanged);
            // 
            // txtProdecutID
            // 
            this.txtProdecutID.BackColor = System.Drawing.Color.White;
            this.txtProdecutID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtProdecutID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProdecutID.Location = new System.Drawing.Point(371, 50);
            this.txtProdecutID.Margin = new System.Windows.Forms.Padding(4);
            this.txtProdecutID.Name = "txtProdecutID";
            this.txtProdecutID.ReadOnly = true;
            this.txtProdecutID.Size = new System.Drawing.Size(279, 23);
            this.txtProdecutID.TabIndex = 409;
            this.txtProdecutID.TextChanged += new System.EventHandler(this.txtProdecutID_TextChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(581, 128);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(69, 17);
            this.label15.TabIndex = 408;
            this.label15.Text = "مجموعة  :";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(566, 78);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(84, 17);
            this.Label2.TabIndex = 407;
            this.Label2.Text = "اسم الصنف :";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(78, 149);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(150, 25);
            this.button1.TabIndex = 2;
            this.button1.Text = "اضافة مجموعة جديدة";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnGetData
            // 
            this.btnGetData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnGetData.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGetData.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnGetData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGetData.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetData.ForeColor = System.Drawing.Color.White;
            this.btnGetData.Location = new System.Drawing.Point(731, 244);
            this.btnGetData.Name = "btnGetData";
            this.btnGetData.Size = new System.Drawing.Size(91, 31);
            this.btnGetData.TabIndex = 506;
            this.btnGetData.Text = "بحث";
            this.btnGetData.UseVisualStyleBackColor = false;
            this.btnGetData.Click += new System.EventHandler(this.btnGetData_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDelete.Enabled = false;
            this.btnDelete.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.White;
            this.btnDelete.Location = new System.Drawing.Point(731, 211);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(91, 31);
            this.btnDelete.TabIndex = 505;
            this.btnDelete.Text = "حذف";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(731, 277);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(91, 31);
            this.btnClose.TabIndex = 507;
            this.btnClose.Text = "اغلاق";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(731, 145);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(91, 31);
            this.btnSave.TabIndex = 503;
            this.btnSave.Text = "حفظ";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(5, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeftLayout = true;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(720, 482);
            this.tabControl1.TabIndex = 508;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.groupBox8);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(712, 453);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "البيانات الاساسية";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.gridControl1);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(712, 453);
            this.tabPage2.TabIndex = 3;
            this.tabPage2.Text = "رصيد الصنف في المخازن";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(3, 3);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(706, 447);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsBehavior.ReadOnly = true;
            this.gridView1.OptionsView.ShowFooter = true;
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdate.Enabled = false;
            this.btnUpdate.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.ForeColor = System.Drawing.Color.White;
            this.btnUpdate.Location = new System.Drawing.Point(731, 178);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(91, 31);
            this.btnUpdate.TabIndex = 504;
            this.btnUpdate.Text = "تعديل";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnNew
            // 
            this.btnNew.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(160)))), ((int)(((byte)(124)))));
            this.btnNew.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNew.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNew.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNew.ForeColor = System.Drawing.Color.White;
            this.btnNew.Location = new System.Drawing.Point(731, 112);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(91, 31);
            this.btnNew.TabIndex = 502;
            this.btnNew.Text = "جديد";
            this.btnNew.UseVisualStyleBackColor = false;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // Prodecut_frm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(830, 486);
            this.Controls.Add(this.txtBarcode3);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.btnGetData);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnNew);
            this.Font = new System.Drawing.Font("Tahoma", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "Prodecut_frm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "شاشة الاصناف";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Prodecut_frm_FormClosing);
            this.Load += new System.EventHandler(this.ITEM_FRM_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Prodecut_frm_KeyDown);
            this.tabPage3.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtThreeUnitBarcode.Properties)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFiestUnitBarcode.Properties)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSecoundUnitBarcode.Properties)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk_HasSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_HasSerial.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_HasExpire.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_HasColor.Properties)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.TextBox txtBarcode3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox8;
        public System.Windows.Forms.TextBox txtProdecutName;
        public System.Windows.Forms.TextBox txtCategoryID;
        public System.Windows.Forms.TextBox txtProdecutLocation;
        internal System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtProdecutID;
        internal System.Windows.Forms.Label Label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox combGroup;
        private System.Windows.Forms.Button button1;
        internal System.Windows.Forms.Label label15;
        public System.Windows.Forms.TextBox txtCompany;
        internal System.Windows.Forms.Button btnGetData;
        internal System.Windows.Forms.Button btnDelete;
        internal System.Windows.Forms.Button btnClose;
        internal System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        internal System.Windows.Forms.Button btnUpdate;
        internal System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Label label18;
        public System.Windows.Forms.TextBox txtRequestLimit;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label19;
        public System.Windows.Forms.TextBox txtProdecutBayPrice3;
        private System.Windows.Forms.Label label25;
        public System.Windows.Forms.TextBox txtThreeUnitOperating;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        public System.Windows.Forms.TextBox txtThreeUnitPrice3;
        private System.Windows.Forms.Label label34;
        public System.Windows.Forms.TextBox txtThreeUnitPrice2;
        private System.Windows.Forms.Label label35;
        public System.Windows.Forms.TextBox txtThreeUnitPrice1;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox combThreeUnit;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox txtProdecutBayPrice;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtFiestUnitOperating;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox txtFiestUnitPrice3;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.TextBox txtFiestUnitPrice2;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.TextBox txtFiestUnitPrice1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox combFiestUnit;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label29;
        public System.Windows.Forms.TextBox txtProdecutBayPrice2;
        private System.Windows.Forms.Label label21;
        public System.Windows.Forms.TextBox txtSecoundUnitOperating;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        public System.Windows.Forms.TextBox txtSecoundUnitPrice3;
        private System.Windows.Forms.Label label24;
        public System.Windows.Forms.TextBox txtSecoundUnitPrice2;
        private System.Windows.Forms.Label label26;
        public System.Windows.Forms.TextBox txtSecoundUnitPrice1;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox combSecoundUnit;
        private System.Windows.Forms.CheckBox chStatus;
        private System.Windows.Forms.ToolTip toolTip1;
        public System.Windows.Forms.TextBox txtDiscoundBay;
        private System.Windows.Forms.Label label39;
        public System.Windows.Forms.TextBox txtDiscoundSale;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.RadioButton RbUnitDefoult3;
        private System.Windows.Forms.RadioButton RbUnitDefoult1;
        private System.Windows.Forms.RadioButton RbUnitDefoult2;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.CheckBox chProductService;
        private DevExpress.XtraEditors.ButtonEdit txtFiestUnitBarcode;
        private DevExpress.XtraEditors.ButtonEdit txtThreeUnitBarcode;
        private DevExpress.XtraEditors.ButtonEdit txtSecoundUnitBarcode;
        private DevExpress.XtraEditors.SpinEdit spinEdit1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit chk_HasSize;
        private DevExpress.XtraEditors.CheckEdit chk_HasExpire;
        private DevExpress.XtraEditors.CheckEdit chk_HasColor;
        private DevExpress.XtraEditors.CheckEdit chk_HasSerial;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
    }
}