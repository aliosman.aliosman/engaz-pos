﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class CombanyInformatuon_FormAR : Form
    {
        public CombanyInformatuon_FormAR()
        {
            InitializeComponent();
        }
        CombanyInformatuon_cls cls = new CombanyInformatuon_cls();
        private void CombanyInformatuon_FormAR_Load(object sender, EventArgs e)
        {
            try
            {
                ERB_Setting.SettingToolStrip(toolStrip1);
                ERB_Setting.SettingForm(this);
                DataTable   Dt_CombanyData =cls .Details_CombanyData();
            if (Dt_CombanyData.Rows.Count> 0)
            {
                txtCombanyName.Text = Dt_CombanyData.Rows[0]["CombanyName"].ToString();
                txtCombanyDescriptone.Text = Dt_CombanyData.Rows[0]["CombanyDescriptone"].ToString();
                txtMyName.Text = Dt_CombanyData.Rows[0]["MyName"].ToString();
                txtPhoneNamber.Text = Dt_CombanyData.Rows[0]["PhoneNamber"].ToString();
                txtTitle.Text = Dt_CombanyData.Rows[0]["Title"].ToString();
                txtBillRemark.Text = Dt_CombanyData.Rows[0]["BillRemark"].ToString();
                txtMaintenanceRemark.Text = Dt_CombanyData.Rows[0]["MaintenanceRemark"].ToString();
                if (!string.IsNullOrEmpty(Dt_CombanyData.Rows[0]["Imge"].ToString()))
                {
                    byte[] pic = (byte[])Dt_CombanyData.Rows[0]["Imge"];
                    MemoryStream ms = new MemoryStream(pic);
                    p_box.Image = Image.FromStream(ms);
                }
              
             }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

 

     







           
         
            


           private void p_box_Click(object sender, EventArgs e)
           {
               OpenFileDialog ofd = new OpenFileDialog();
               ofd.Filter = "Image Files | *.jpg; *.BMP;*.png; *.bmp; *.gif | All Files (*.*) | *.*";
               if (ofd.ShowDialog() == DialogResult.OK)
               {
                   p_box.Image = Image.FromFile(ofd.FileName);
               }
           }

      

           private void toolStripButton2_Click(object sender, EventArgs e)
           {
               try
               {

                   if (txtCombanyName.Text.Trim()=="")
                   {
                       txtCombanyName.BackColor = Color.Pink;
                       txtCombanyName.Focus();
                       return;
                   }
                   if (txtCombanyDescriptone.Text.Trim() == "")
                   {
                       txtCombanyDescriptone.BackColor = Color.Pink;
                       txtCombanyDescriptone.Focus();
                       return;
                   }

                   MemoryStream ms = new MemoryStream();
                   p_box.Image.Save(ms, p_box.Image.RawFormat);
                   byte[] byteimge = ms.ToArray();

                DataTable Dt_CombanyData = cls.Details_CombanyData();
                   if (Dt_CombanyData.Rows.Count == 0)
                   {
                       cls.Insert_CombanyData("1", txtCombanyName.Text, txtCombanyDescriptone.Text, txtMyName.Text, txtPhoneNamber.Text, byteimge, txtTitle.Text, txtBillRemark.Text, txtMaintenanceRemark.Text);
                    Mass.Saved();          
                   }
                   else
                   {
                       cls.Update_CombanyData("1", txtCombanyName.Text, txtCombanyDescriptone.Text, txtMyName.Text, txtPhoneNamber.Text, byteimge, txtTitle.Text, txtBillRemark.Text, txtMaintenanceRemark.Text);
                    Mass.Update();
                   }

               }
               catch (Exception EX)
               {
                   MessageBox.Show(EX.Message);
               }
          
           }

           private void button2_Click_1(object sender, EventArgs e)
           {
               p_box.Image = Properties.Resources.no_photo_icon_big_ar;
           }

           private void button1_Click_1(object sender, EventArgs e)
           {
               OpenFileDialog ofd = new OpenFileDialog();
               ofd.Filter = "Image Files | *.jpg; *.BMP;*.png; *.bmp; *.gif | All Files (*.*) | *.*";
               if (ofd.ShowDialog() == DialogResult.OK)
               {
                   p_box.Image = Image.FromFile(ofd.FileName);
               }
           }

           private void txtCombanyName_TextChanged(object sender, EventArgs e)
           {
               txtCombanyName.BackColor = Color.White;
           }

           private void txtCombanyDescriptone_TextChanged(object sender, EventArgs e)
           {
               txtCombanyDescriptone.BackColor = Color.White;
           }

        private void CombanyInformatuon_FormAR_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.F2)
            {
                toolStripButton2_Click(null,null);
            }
        }
    }
}
