﻿using DevExpress.XtraEditors;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class SuppliersPay_frm : XtraForm
    {
        public SuppliersPay_frm()
        {
            InitializeComponent();
        }


        PayType_trans_cls PayType_trans_cls = new PayType_trans_cls();
        Suppliers_Pay_cls cls = new Suppliers_Pay_cls();
        TreasuryMovement_cls cls_Treasury_Movement = new TreasuryMovement_cls();

        string TreasuryID;
        private void CustomersPayment_FormAdd_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingToolStrip(toolStrip1);  
            btnNew_Click_1(null, null);
           

        }

       



      

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {


                Suppliers_Search_Form frm = new Suppliers_Search_Form();
                frm.ShowDialog(this);
                if (frm.loaddata == true)
                {
                    txtCustomerName.BackColor = System.Drawing.Color.White;
                    txtCustomerID.Text = frm.DGV1.CurrentRow.Cells["SupplierID"].Value.ToString();
                    txtCustomerName.Text = frm.DGV1.CurrentRow.Cells["SupplierName"].Value.ToString();
                    if (btnSave.Enabled == true)
                    {
                        BalenceCustomer(txtCustomerID.Text);
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }


        }
        TreasuryMovement_cls TreasuryMovement_CLS = new TreasuryMovement_cls();

        private void BalenceCustomer(string CustomerID)
        {
            try
            {
                txtCustomersBlanse.Text = "0";
                if (CustomerID.Trim() != "" || CustomerID != "System.Data.DataRowView")
                {
                    DataTable dt = TreasuryMovement_CLS.Balence_ISCusSupp(CustomerID, "Supp");//Cus_Trans_cls.Cus_Balence(dt_cus.Rows[i]["CustomerID"].ToString());
                    //  = Cus_Trans_cls.Cus_Balence(txtAccountID.Text);
                    txtCustomersBlanse.Text = "0";
                    if (dt.Rows[0]["balence"] == DBNull.Value == false)
                    {
                        txtCustomersBlanse.Text = dt.Rows[0]["balence"].ToString();
                    }
                    //=========================================================================================================


                }
            }
            catch
            {

            }

        }







        private void LoadPaymentCustomer()
        {
            try
            {

                SupplierPaySearch_frm frm = new SupplierPaySearch_frm();
                frm.Text = this.Text;
                frm.ShowDialog(this);
                if (frm.LoadData == true)
                {
                    btnSave.Enabled = false;
                    btnUpdate.Enabled = true;
                    btnDelete.Enabled = true;


                    DataTable dt = cls.Details_Supplier_Pay(frm.DGV1.CurrentRow.Cells["PayID"].Value.ToString());
                    DataRow Dr = dt.Rows[0];
                    txtPayID.Text = Dr["PayID"].ToString();
                    D1.Value = Convert.ToDateTime(Dr["PayDate"]);
                    //===============================================================================================================================
                    txtCustomerID.Text = Dr["SupplierID"].ToString();      
                    //==================================================================================================================================
                    txtRemarks.Text = Dr["Remarks"].ToString();
                    txtCustomerName.Text = Dr["SupplierName"].ToString();
                    TreasuryID = Dr["TreasuryID"].ToString();

                    txtSumPayType.EditValue  = Convert.ToDouble(Dr["PayValue"]);








                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnNew_Click_1(object sender, EventArgs e)
        {
            try
            {
                txtPayID.Text = Convert.ToString(cls.MaxID_Supplier_Pay());
                D1.Value = DateTime.Now; 
                txtCustomerID.Text = "";
                txtCustomerName.Text = "";
                txtRemarks.Text = "";
                txtCustomerName.BackColor = System.Drawing.Color.White; 
                txtSumPayType.EditValue  = 0;
                txtCustomersBlanse.Text = "0"; 
                

                btnSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if (txtPayID.Text == "")
            {
                MessageBox.Show("حدث خطأ يرجي المحاولة لاحقا", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (txtCustomerID.Text.Trim() == "" || txtCustomerName.Text.Trim() == "")
            {
                txtCustomerName.BackColor = System.Drawing.Color.Pink;
                button1.Focus();
                return;
            }




            if (txtSumPayType.Text == "0")
            {
                MessageBox.Show("يرجي ادخال المبلغ ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (string.IsNullOrEmpty(txtRemarks.Text.Trim()))
            {
                txtRemarks.Focus();
                MessageBox.Show("يرجي ادخال البيان ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }


            try
            {

               
                TreasuryID = cls_Treasury_Movement.MaxID_TreasuryMovement();
                txtPayID.Text = cls.MaxID_Supplier_Pay();
                cls.InsertSupplier_Pay(txtPayID.Text, D1.Value, Convert.ToDouble(txtSumPayType.Text), txtRemarks.Text, txtCustomerID.Text, TreasuryID, Properties.Settings.Default.UserID);
                cls_Treasury_Movement.InsertTreasuryMovement(TreasuryID, txtCustomerID.Text, "Supp", txtPayID.Text, txtTypeID.Text, this.Text, D1.Value, txtCustomerName.Text, "0", txtSumPayType.Text, txtRemarks.Text, txtSumPayType.Text, "0", Properties.Settings.Default.UserID);

              


                Mass.Saved();
                btnNew_Click_1(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnUpdate_Click_1(object sender, EventArgs e)
        {
            if (txtPayID.Text == "")
            {
                MessageBox.Show("حدث خطأ يرجي المحاولة لاحقا", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (txtCustomerID.Text == "" || txtCustomerName.Text == "")
            {
                txtCustomerName.BackColor = System.Drawing.Color.Pink;
                button1.Focus();
                return;
            }




            if (txtSumPayType.Text == "0")
            {
                MessageBox.Show("يرجي ادخال المبلغ ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }


            if (string.IsNullOrEmpty(txtRemarks.Text.Trim()))
            {
                txtRemarks.Focus();
                MessageBox.Show("يرجي ادخال البيان ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try
            {

                cls.UpdateSupplier_Pay(txtPayID.Text, D1.Value, Convert.ToDouble(txtSumPayType.Text), txtRemarks.Text, txtCustomerID.Text);

                cls_Treasury_Movement.UpdateTreasuryMovement(TreasuryID, txtCustomerID.Text, "Supp", txtPayID.Text, txtTypeID.Text, this.Text, D1.Value, txtCustomerName.Text, "0", txtSumPayType.Text, txtRemarks.Text, txtSumPayType.Text, "0");


             
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (Mass.Delete() == true)
                {
                    cls.DeleteSupplier_Pay(txtPayID.Text);
                    cls_Treasury_Movement.DeleteTreasuryMovement(TreasuryID);
                    PayType_trans_cls.Delete_PayType_trans(txtPayID.Text, txtTypeID.Text);

                    btnNew_Click_1(null, null);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                LoadPaymentCustomer();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

     




     

        private void SuppliersPay_frm_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click_1(null, null);
                }
                if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        toolStripButton3_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click_1(null, null);
                    }
                }
                if (e.KeyCode == Keys.Delete)
                {
                    if (btnDelete.Enabled == true)
                    {
                        btnDelete_Click_1(null, null);
                    }
                }
                //if (e.KeyCode == Keys.F5)
                //{
                //    btnGetData_Click(null, null);
                //}

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion
        }

        
    }
}
