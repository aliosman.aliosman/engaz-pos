﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class MessageEdit_frm : Form
    {
        public MessageEdit_frm()
        {
            InitializeComponent();
        }

        public Boolean ISDelete = false;
      

        private void button2_Click(object sender, EventArgs e)
        {
            ISDelete = true;
            this.Close();
        }

        private void MessageSaved_frm_KeyDown(object sender, KeyEventArgs e)
        {
            this.Close();
        }
    }
}
