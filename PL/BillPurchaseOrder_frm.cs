﻿using ByStro.RPT;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class BillPurchaseOrder_frm : Form
    {
        public BillPurchaseOrder_frm()
        {
            InitializeComponent();
        }
        PurchaseOrder_cls cls = new PurchaseOrder_cls();
        Boolean Recived = true;
        private void PureItemToStoreForm_Load(object sender, EventArgs e)
        {


            ERB_Setting.SettingToolStrip(toolStrip1);
            ERB_Setting.SettingForm(this);
            this.MaximizeBox = true;
            ERB_Setting.SettingDGV(DGV1);

            FillProdecut();
            btnNew_Click(null, null);
            Search_cls.FillcombSoreUsers(combStore, false);
            chUseVat.Checked = BillSetting_cls.UseVat;

        }



        public void FillProdecut()
        {
            loadaccount_CLS.ItemsLoadtxt(txtSearch);


        }




















        private void DGV1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {

        }

        Prodecut_cls Prodecut_CLS = new Prodecut_cls();
        DataTable dt_Prodecut = new DataTable();
        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtSearch.Text.Trim() == "")
                {
                    cleartxt();
                    return;
                }


                #region "تحميل الباركود واكود الصنف واسم الصنف في مكانة  السليم"

                //====================================================================================
                try
                {

                    dt_Prodecut = Prodecut_CLS.SearchBill_Prodecuts(txtSearch.Text);

                    String ProdecutName = txtSearch.Text;
                    txtProdecutID.Text = dt_Prodecut.Rows[0]["ProdecutID"].ToString();
                    txtSearch.Text = dt_Prodecut.Rows[0]["ProdecutName"].ToString();


                    combUnit.Items.Clear();

                    if (dt_Prodecut.Rows[0]["FiestUnit"].ToString() != "")
                    {
                        combUnit.Items.Add(dt_Prodecut.Rows[0]["FiestUnit"].ToString());
                    }
                    if (dt_Prodecut.Rows[0]["SecoundUnit"].ToString() != "")
                    {
                        combUnit.Items.Add(dt_Prodecut.Rows[0]["SecoundUnit"].ToString());
                    }
                    if (dt_Prodecut.Rows[0]["ThreeUnit"].ToString() != "")
                    {
                        combUnit.Items.Add(dt_Prodecut.Rows[0]["ThreeUnit"].ToString());
                    }
                    txtQuantity.Text = "1";


                    #region "ملأ الكومبوبوكس بالوحدات"

                    if (dt_Prodecut.Rows[0]["FiestUnitBarcode"].ToString() == ProdecutName)
                    {
                        combUnit.SelectedIndex = 0;

                    }
                    else if (dt_Prodecut.Rows[0]["SecoundUnitBarcode"].ToString() == ProdecutName)
                    {
                        combUnit.SelectedIndex = 1;

                    }
                    else if (dt_Prodecut.Rows[0]["ThreeUnitBarcode"].ToString() == ProdecutName)
                    {
                        combUnit.SelectedIndex = 2;
                    }
                    else
                    {
                        if (dt_Prodecut.Rows[0]["UnitDefoult"].ToString() == "1")
                        {
                            combUnit.SelectedIndex = 0;

                        }
                        else if (dt_Prodecut.Rows[0]["UnitDefoult"].ToString() == "2")
                        {
                            combUnit.SelectedIndex = 1;

                        }
                        else if (dt_Prodecut.Rows[0]["UnitDefoult"].ToString() == "3")
                        {
                            combUnit.SelectedIndex = 2;
                        }
                        else
                        {
                            combUnit.SelectedIndex = 0;
                        }
                    }

                    combUnit.Focus();
                    combUnit.Select();

                    #endregion

                    if (BillSetting_cls.UsingFastInput == true)
                    {

                        txtSearch.Focus();
                    }


                }
                catch
                {
                    MessageBox.Show("الصنف المدخل غير موجود يرجي مراجعة الاصناف", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtSearch.Text = "";
                    txtSearch.Focus();
                }


                //==================================================================================================================

                #endregion

                //SumitemdDGV();
            }

        }

        private void combUnit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtQuantity.Focus();
            }
        }

        private void txtQty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtPrice.Focus();
            }
        }

        private void txtPrise_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AddItem_DGV();
            }
        }





        #region" اضافة الي الداتا قرد فيو   "
        //string AVG_Price = "0";

        public void AddItem_DGV()
        {
            try
            {
                if (txtMainVat.Text.Trim()=="")
                {
                       txtMainVat.BackColor = Color.Pink;
                    txtMainVat.Focus();
                    return;
                }

                if (txtSearch.Text == "" || txtProdecutID.Text == "")
                {
                    txtSearch.BackColor = Color.Pink;
                    txtSearch.Focus();
                    return;
                }

                if (combUnit.Text == "")
                {
                    combUnit.BackColor = Color.Pink;
                    combUnit.Focus();
                    return;
                }
                if (txtQuantity.Text == "" || Convert.ToDouble(txtQuantity.Text) <= Convert.ToDouble(0))
                {
                    txtQuantity.BackColor = Color.Pink;
                    txtQuantity.Focus();
                    return;
                }


                if (txtPrice.Text == "" || Convert.ToDouble(txtPrice.Text) <= Convert.ToDouble(0))
                {
                    txtPrice.BackColor = Color.Pink;
                    txtPrice.Focus();
                    return;
                }



                if (txtVAT.Text == "")
                {
                    txtVAT.BackColor = Color.Pink;
                    txtVAT.Focus();
                    return;
                }








                //================================================================================================================





                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    if (DGV1.Rows[i].Cells["ProdecutID"].Value.ToString() == txtProdecutID.Text && DGV1.Rows[i].Cells["Unit"].Value.ToString() == combUnit.Text)
                    {
                        if (BillSetting_cls.ShowMessageQty == true)
                        {

                            Double qty = Convert.ToDouble(DGV1.Rows[i].Cells["Quantity"].Value) + Convert.ToDouble(txtQuantity.Text);
                            DGV1.Rows[i].Cells["Quantity"].Value = +qty;
                            DGV1.Rows[i].Selected = true;

                            Double x = Convert.ToDouble(DGV1.Rows[i].Cells["Quantity"].Value) * Convert.ToDouble(DGV1.Rows[i].Cells["Price"].Value);
                            Double MainVat_ = Convert.ToDouble(DGV1.Rows[i].Cells["MainVat"].Value.ToString());
                            Double total = x * MainVat_;
                            double vat_ = total / 100;

                            DGV1.Rows[i].Cells["Vat"].Value = vat_;
                            DGV1.Rows[i].Cells["TotalPrice"].Value = double.Parse(DGV1.Rows[i].Cells["Vat"].Value.ToString()) + x;
                            SumDgv();
                            cleartxt();
                            return;

                        }
                        else
                        {
                            if (MessageBox.Show("هذ الصنف سبق وتم اضافتة ضمن الفاتورة  : هل تريد اضافة  " + txtQuantity.Text + "  الي الكمية ", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                Double qty = Convert.ToDouble(DGV1.Rows[i].Cells["Quantity"].Value) + Convert.ToDouble(txtQuantity.Text);
                                DGV1.Rows[i].Cells["Quantity"].Value = +qty;
                                DGV1.Rows[i].Selected = true;

                                Double x = Convert.ToDouble(DGV1.Rows[i].Cells["Quantity"].Value) * Convert.ToDouble(DGV1.Rows[i].Cells["Price"].Value);
                                Double MainVat_ = Convert.ToDouble(DGV1.Rows[i].Cells["MainVat"].Value.ToString());
                                Double total = x * MainVat_;
                                double vat_ = total / 100;

                                DGV1.Rows[i].Cells["Vat"].Value = vat_;
                                DGV1.Rows[i].Cells["TotalPrice"].Value = double.Parse(DGV1.Rows[i].Cells["Vat"].Value.ToString()) + x;
                                SumDgv();
                                cleartxt();
                                return;
                            }
                            else
                            {
                                return;
                            }
                        }


                    }
                }





                AddRowDgv(txtProdecutID.Text, txtSearch.Text, combUnit.Text, txtUnitFactor.Text, txtUnitOperating.Text, txtQuantity.Text, txtPrice.Text, txtVAT.Text, txtMainVat.Text, txtTotalPrice.Text);
                lblCount.Text = DGV1.Rows.Count.ToString();

                SumDgv();
                cleartxt();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        private void SumDgv()
        {
            double sum = 0; double m_vat = 0;
            double QTY = 0;


            for (int i = 0; i < DGV1.Rows.Count; i++)
            {
                sum += Convert.ToDouble(DGV1.Rows[i].Cells["TotalPrice"].Value);
                m_vat += Convert.ToDouble(DGV1.Rows[i].Cells["Vat"].Value);
                QTY += double.Parse(DGV1.Rows[i].Cells["Quantity"].Value.ToString());
            }
            txtTotalInvoice.Text = sum.ToString();
            txtVatValue.Text = m_vat.ToString();
            //   lblSArbic.Text = "          " + DataAccessLeayer.Horofcls.تحويل_الأرقام_الى_حروف(Convert.ToDecimal(sum), 2, "", "", true, true);
            lblQTY.Text = QTY.ToString();



        }
        private void AddRowDgv(string ProdecutID, string ProdecutName, string Unit, string UnitFactor, string UnitOperating, string Quantity, string Price, string Vat, string MainVat, string TotalPrice)
        {
            try
            {
                DGV1.Rows.Add();
                DGV1.ClearSelection();
                DGV1.Rows[DGV1.Rows.Count - 1].Selected = true;
                var lastRows = DGV1.Rows.Count - 1;
                DGV1.Rows[lastRows].Cells["ProdecutID"].Value = ProdecutID;
                DGV1.Rows[lastRows].Cells["ProdecutName"].Value = ProdecutName;
                DGV1.Rows[lastRows].Cells["Unit"].Value = Unit;
                DGV1.Rows[lastRows].Cells["UnitFactor"].Value = UnitFactor;
                DGV1.Rows[lastRows].Cells["UnitOperating"].Value = UnitOperating;
                DGV1.Rows[lastRows].Cells["Quantity"].Value = Quantity;
                DGV1.Rows[lastRows].Cells["Price"].Value = Price;
                DGV1.Rows[lastRows].Cells["Vat"].Value = Vat;
                DGV1.Rows[lastRows].Cells["MainVat"].Value = MainVat;
                DGV1.Rows[lastRows].Cells["TotalPrice"].Value = TotalPrice;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }
        private void cleartxt()
        {
            txtProdecutID.Text = "";
            txtSearch.Text = "";
            txtQuantity.Text = "";
            txtPrice.Text = "";
            txtTotalPrice.Text = "";
            combUnit.Items.Clear();
            txtVAT.Text = "";
            txtSearch.Focus();

        }

        private void txtQty_TextChanged(object sender, EventArgs e)
        {
            txtQuantity.BackColor = Color.White;
            SumTotal_txt();

        }


        private void SumTotal_txt()
        {
            try
            {



                if (txtQuantity.Text == "")
                {
                    txtVAT.Text = "0";
                    txtTotalPrice.Text = "0";
                    txtQuantity.Focus();
                    return;
                }

                if (txtPrice.Text == "")
                {
                    txtVAT.Text = "0";
                    txtTotalPrice.Text = "0";
                    txtPrice.Focus();
                    return;
                }

                if (txtVAT.Text == "")
                {
                    txtVAT.Text = "0";
                    txtTotalPrice.Text = "0";
                    txtVAT.Focus();
                    return;
                }

                double x = Convert.ToDouble(txtQuantity.Text) * Convert.ToDouble(txtPrice.Text);

                // MainVat


                Double total = x * Convert.ToDouble(txtMainVat.Text);
                double vat_ = total / 100;
                txtVAT.Text = (vat_).ToString();




                //   x = x * Convert.ToDouble(txtVAT.Text) / 100;
                txtTotalPrice.Text = Convert.ToString(x + vat_);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


        }
        private void txtPrise_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            save(true);

        }


        void save(Boolean print)
        {


            try
            {

                if (lblCount.Text == "0")
                {
                    MessageBox.Show("لا يمكن حفظ فاتورة خالية من الاصناف", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }





                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    if (DGV1.Rows[i].Cells["Quantity"].Value.ToString() == "0" || DGV1.Rows[i].Cells["Quantity"].Value.ToString() == "")
                    {
                        MessageBox.Show("من فضلك يرجي مراجعة الكميات المدخلة في الفاتورة  ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        DGV1.Rows[i].Selected = true;
                        return;
                    }

                }



                txtMainID.Text = cls.MaxID_PurchaseOrder_Main();




                cls.Insert_PurchaseOrder_Main(long.Parse(txtMainID.Text), D1.Value, txtRemarks.Text, Convert.ToDouble(txtTotalInvoice.Text), Convert.ToDouble(txtTotalDescound.Text), Convert.ToDouble(txtNetInvoice.Text), double.Parse(txtMainVat.Text), double.Parse(txtVatValue.Text), Properties.Settings.Default.UserID);
                //====Bay_Main===========================================================================================================================================================================================================================================================
                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    cls.Insert_PurchaseOrder_Details(long.Parse(txtMainID.Text), int.Parse(DGV1.Rows[i].Cells["ProdecutID"].Value.ToString()), DGV1.Rows[i].Cells["Unit"].Value.ToString(), double.Parse(DGV1.Rows[i].Cells["UnitFactor"].Value.ToString()), double.Parse(DGV1.Rows[i].Cells["UnitOperating"].Value.ToString()), double.Parse(DGV1.Rows[i].Cells["Quantity"].Value.ToString()), double.Parse(DGV1.Rows[i].Cells["Price"].Value.ToString()), double.Parse(DGV1.Rows[i].Cells["Vat"].Value.ToString()), double.Parse(DGV1.Rows[i].Cells["MainVat"].Value.ToString()), double.Parse(DGV1.Rows[i].Cells["TotalPrice"].Value.ToString()), int.Parse(combStore.SelectedValue.ToString()), Recived);

                }







                if (BillSetting_cls.ShowMessageSave == false)
                {
                    Mass.Saved();
                }

                if (print == true)
                {
                 //   PrintSize1_2A4();
                }
                btnNew_Click(null, null);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            
        }



     //   private void PrintSize1_2A4()
     //   {



     //
     //       BillSetting_cls cls_print = new BillSetting_cls();
     //       DataTable dt_Print = cls_print.BillPrintPurchaseOrder(txtMainID.Text);

     //       Reportes_Form frm = new Reportes_Form();

     //       var report = new CRBillPurchaseOrder_A4();
     //       CombanyInformatuon_cls CombanyInformatuon_cls = new CombanyInformatuon_cls();
     //       report.Database.Tables["CombanyData"].SetDataSource(CombanyInformatuon_cls.Details_CombanyData());

     //       report.Database.Tables["Bill_Print"].SetDataSource(dt_Print);
     //       if (txtRemarks.Text.Trim()=="")
     //       {
     //           txtRemarks.Text = "في حال وجود أي أسئلة أو ملاحظات يرجى الاتصال بنا";  
     //       }

     //       report.SetParameterValue(0, txtMainID.Text);
     //       report.SetParameterValue(1, txtRemarks.Text);
     //       report.SetParameterValue(2, txtTotalInvoice.Text);
     //       report.SetParameterValue(3, txtTotalDescound.Text);
     //       report.SetParameterValue(4,txtNetInvoice .Text);
     //       report.SetParameterValue(5, txtVatValue.Text);
     //       report.SetParameterValue(6, D1.Value);

     //  
     //       frm.crystalReportViewer1.ReportSource = report;
     //       frm.crystalReportViewer1.Refresh();

     //       frm.ShowDialog();

     //       #region"Crystal Report | Printing | Default Printer"
     //       //var dialog = new PrintDialog();
     //       //report.PrintOptions.PrinterName = dialog.PrinterSettings.PrinterName;
     //       //report.PrintToPrinter(1, false, 0, 0);



     //       #endregion


     //   }












        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {

                if (lblCount.Text == "0")
                {
                    MessageBox.Show("لا يمكن حفظ فاتورة خالية من الاصناف", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }




                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    if (DGV1.Rows[i].Cells["Quantity"].Value.ToString() == "0" || DGV1.Rows[i].Cells["Quantity"].Value.ToString() == "")
                    {
                        MessageBox.Show("من فضلك يرجي مراجعة الكميات المدخلة في الفاتورة  ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        DGV1.Rows[i].Selected = true;
                        return;
                    }

                }


                cls.Update_PurchaseOrder_Main(long.Parse(txtMainID.Text), D1.Value, txtRemarks.Text, Convert.ToDouble(txtTotalInvoice.Text), Convert.ToDouble(txtTotalDescound.Text), Convert.ToDouble(txtNetInvoice.Text), double.Parse(txtMainVat.Text), double.Parse(txtVatValue.Text));


                // DELETE  PurchaseOrder_Details
                cls.Delete_PurchaseOrder_Details(long.Parse(txtMainID.Text));

                // INSERT  PurchaseOrder_Details
                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    cls.Insert_PurchaseOrder_Details(long.Parse(txtMainID.Text), int.Parse(DGV1.Rows[i].Cells["ProdecutID"].Value.ToString()), DGV1.Rows[i].Cells["Unit"].Value.ToString(), double.Parse(DGV1.Rows[i].Cells["UnitFactor"].Value.ToString()), double.Parse(DGV1.Rows[i].Cells["UnitOperating"].Value.ToString()), double.Parse(DGV1.Rows[i].Cells["Quantity"].Value.ToString()), double.Parse(DGV1.Rows[i].Cells["Price"].Value.ToString()), double.Parse(DGV1.Rows[i].Cells["Vat"].Value.ToString()), double.Parse(DGV1.Rows[i].Cells["MainVat"].Value.ToString()), double.Parse(DGV1.Rows[i].Cells["TotalPrice"].Value.ToString()), int.Parse(combStore.SelectedValue.ToString()), Recived);

                }


                //====clear ========================================================================================================================================================================================================================================================
                if (BillSetting_cls.ShowMessageSave == false)
                {
                    Mass.Update();
                }
             //   PrintSize1_2A4();
                btnNew_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }





        //======================================================================


        #endregion




        private void txtSum_TextChanged(object sender, EventArgs e)
        {

            //try
            //{

            //    if (txtSum.Text.Trim() != "")
            //    {
            //        txtArbic.Text = DAL.DataAccessLeayr.Horofcls.تحويل_الأرقام_الى_حروف(Convert.ToDecimal(txtSum.Text), 2, "", "", true, true);

            //    }
            //    else
            //    {
            //        txtArbic.Text = "";
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}

        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (txtSearch.Text.Trim() == "")
            {
                cleartxt();

            }
        }


        private void combUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (combUnit.SelectedIndex == 0)
            {
                txtUnitFactor.Text = dt_Prodecut.Rows[0]["FiestUnitFactor"].ToString();
                txtUnitOperating.Text = dt_Prodecut.Rows[0]["FiestUnitOperating"].ToString();
                //==========Know Bay prodect price====================================================================================================
                double price = Convert.ToDouble(dt_Prodecut.Rows[0]["ProdecutBayPrice"].ToString()) * Convert.ToDouble(txtUnitFactor.Text);
                txtPrice.Text = (price).ToString("0.00");

                //==============================================================================================================
            }
            else if (combUnit.SelectedIndex == 1)
            {
                txtUnitFactor.Text = dt_Prodecut.Rows[0]["SecoundUnitFactor"].ToString();
                txtUnitOperating.Text = dt_Prodecut.Rows[0]["SecoundUnitOperating"].ToString();
                double price = Convert.ToDouble(dt_Prodecut.Rows[0]["ProdecutBayPrice"].ToString()) * Convert.ToDouble(txtUnitFactor.Text);

                txtPrice.Text = (price).ToString("0.00");


            }
            else if (combUnit.SelectedIndex == 2)
            {
                txtUnitFactor.Text = dt_Prodecut.Rows[0]["ThreeUnitFactor"].ToString();
                txtUnitOperating.Text = dt_Prodecut.Rows[0]["ThreeUnitOperating"].ToString();
                double price = Convert.ToDouble(dt_Prodecut.Rows[0]["ProdecutBayPrice"].ToString()) * Convert.ToDouble(txtUnitFactor.Text);
                txtPrice.Text = (price).ToString("0.00");

            }
            // txtVAT.Text = dt_Prodecut.Rows[0]["DiscoundBay"].ToString();
            txtProdecutAvg.Text = dt_Prodecut.Rows[0]["ProdecutAvg"].ToString();

            if (BillSetting_cls.UsingFastInput == true)
            {
                AddItem_DGV();
                txtSearch.Focus();
            }
        }



        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                BillPurchaseOrder_Search_frm frm = new BillPurchaseOrder_Search_frm();
                frm.Icon = this.Icon;
                frm.ShowDialog(this);
                if (frm.LoadData == false)
                {
                    return;
                }

                btnSave.Enabled = false;
                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;
                DataTable dt = cls.Details_PurchaseOrder_Main(long.Parse( frm.DGV2.CurrentRow.Cells["MainID"].Value.ToString()));
                DataRow Dr = dt.Rows[0];
                txtMainID.Text = Dr["MainID"].ToString();
                D1.Value = Convert.ToDateTime(Dr["MyDate"]);

                // Balense==========================================================================================
                txtRemarks.Text = Dr["Remarks"].ToString();
                txtTotalInvoice.Text = Dr["TotalInvoice"].ToString();
                txtMainVat.Text = Dr["vat"].ToString();
                txtTotalDescound.Text = Dr["Discount"].ToString();
                txtNetInvoice.Text = Dr["NetInvoice"].ToString();
       
                //=========================================================================================================================================================================
                dt.Clear();
                DGV1.DataSource = null;
                DGV1.Rows.Clear();
                //=========================================================================================================================================================================
              DataTable  dt_Details = cls.Details_PurchaseOrder_Details(long.Parse(frm.DGV2.CurrentRow.Cells["MainID"].Value.ToString()));
                //DGV1.AutoGenerateColumns = false;
                //DGV1.DataSource = dt;
                for (int i = 0; i < dt_Details.Rows.Count; i++)
                {
                    AddRowDgv(dt_Details.Rows[i]["ProdecutID"].ToString(), dt_Details.Rows[i]["ProdecutName"].ToString(), dt_Details.Rows[i]["Unit"].ToString(), dt_Details.Rows[i]["UnitFactor"].ToString(), dt_Details.Rows[i]["UnitOperating"].ToString(), dt_Details.Rows[i]["Quantity"].ToString(), dt_Details.Rows[i]["Price"].ToString(), dt_Details.Rows[i]["Vat"].ToString(), dt_Details.Rows[i]["MainVat"].ToString(), dt_Details.Rows[i]["TotalPrice"].ToString()); // dt.Rows[i]["StoreID"].ToString(), dt.Rows[i]["StoreName"].ToString());
                }

                lblCount.Text = DGV1.Rows.Count.ToString();
                SumDgv();










            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }



        private void txtPrice_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtVAT.Focus();
            }
        }

        private void txtPrice_TextChanged(object sender, EventArgs e)
        {
            txtPrice.BackColor = Color.White;
            SumTotal_txt();

        }

        private void AllKeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void toolStripSplitButton1_ButtonClick(object sender, EventArgs e)
        {

        }

        private void OrderProudect_frm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DGV1.Rows.Count > 0)
            {
                if (Mass.close_frm() == false)
                {
                    e.Cancel = true;
                }
            }
        }









        private void txtVAT_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AddItem_DGV();
            }
        }







        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            try
            {

                if (DGV1.SelectedRows.Count >= 1)
                {
                    if (Mass.Delete() == true)
                    {
                        foreach (DataGridViewRow r in DGV1.SelectedRows)
                        {
                            DGV1.Rows.Remove(r);
                        }
                        lblCount.Text = DGV1.Rows.Count.ToString();
                        SumDgv();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtChange(object sender, EventArgs e)
        {
            try
            {

                Double total = Convert.ToDouble(txtTotalInvoice.Text);
                //  txtVatValue.Text = (total / 100).ToString();


                txtNetInvoice.Text = (Convert.ToDouble(txtTotalInvoice.Text) - Convert.ToDouble(txtTotalDescound.Text)).ToString();


                //if (combPayKind.Text == "نقدي")
                //{
                //    txtpaid_Invoice.Text = txtNetInvoice.Text;

                //}


                //if (btnSave.Enabled)
                //{
                //    if (!string.IsNullOrEmpty(txtCurrencyRate.Text.Trim()))
                //    {
                //        txtPayValue.Text = (double.Parse(txtNetInvoice.Text) / double.Parse(txtCurrencyRate.Text)).ToString();

                //    }
                //    else
                //    {
                //        txtPayValue.Text = txtNetInvoice.Text;
                //    }
                //}
            }
            catch
            {
                return;
            }

        }

        private void combPayKind_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (combPayKind.SelectedIndex == 0)
            //{
            //    // txtpaid_Invoice.ReadOnly = true;
            //    txtpaid_Invoice.Text = txtNetInvoice.Text;

            //}
            //else
            //{
            //    //txtpaid_Invoice.ReadOnly = false;
            //    txtpaid_Invoice.Text = "0";
            //}
        }

        private void DGV1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {



                Double x = Convert.ToDouble(DGV1.CurrentRow.Cells["Quantity"].Value) * Convert.ToDouble(DGV1.CurrentRow.Cells["Price"].Value);

                Double MainVat_ = Convert.ToDouble(DGV1.CurrentRow.Cells["MainVat"].Value.ToString());
                Double total = x * MainVat_;
                double vat_ = total / 100;

                DGV1.CurrentRow.Cells["Vat"].Value = (vat_ ).ToString();

                Double Q = Convert.ToDouble(DGV1.CurrentRow.Cells["Quantity"].Value) * Convert.ToDouble(DGV1.CurrentRow.Cells["Price"].Value);

                DGV1.CurrentRow.Cells["TotalPrice"].Value = double.Parse(DGV1.CurrentRow.Cells["Vat"].Value.ToString()) + Q;
                SumDgv();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtVAT_TextChanged(object sender, EventArgs e)
        {
            txtVAT.BackColor = Color.White;
            SumTotal_txt();
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {

            try
            {
               
                if (Mass.Delete() == true)
                {
                    cls.Delete_PurchaseOrder_Details(long.Parse(txtMainID.Text));
                    cls.Delete_PurchaseOrder_Main(long.Parse(txtMainID.Text));

                    btnNew_Click(null, null);
               
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {

                //===============================================================================================================================================================
                txtMainID.Text = cls.MaxID_PurchaseOrder_Main();
                DGV1.DataSource = null;
                DGV1.Rows.Clear();
                txtRemarks.Text = "";
                txtProdecutID.Text = "";
                txtTotalInvoice.Text = "0";
                lblCount.Text = DGV1.Rows.Count.ToString();
                cleartxt();
                //===============================================================================================================================================
           

                txtVatValue.Text = "0";

                lblQTY.Text = "0";
          
           



                btnSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;

   

                txtSearch.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private void DGV1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                toolStripButton3_Click(null, null);
            }
        }

        private void BillBay_frm_KeyDown(object sender, KeyEventArgs e)
        {


            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                else if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                else if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
                else if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }

                else if (e.KeyCode == Keys.F5)
                {
                    button5_Click(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion

        }



        private void اعادةتحميلالاصنافToolStripMenuItem_Click(object sender, EventArgs e)
        {
            loadaccount_CLS.ItemsLoadtxt(txtSearch);
        }







        private void btnEditProdect_Click(object sender, EventArgs e)
        {
            try
            {

                if (DGV1.SelectedRows.Count >= 1)
                {
                    Prodecut_frm frm = new Prodecut_frm();
                    frm.AddEdit = "Edit";
                    frm.LoadCatogory();
                    frm.LoadProdecut(int.Parse(DGV1.CurrentRow.Cells["ProdecutID"].Value.ToString()));
                    frm.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtMainVat_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtMainVat.BackColor = Color.White;
                Double total = Convert.ToDouble(txtTotalInvoice.Text) * Convert.ToDouble(txtMainVat.Text);
                txtVatValue.Text = (total / 100).ToString();
                txtNetInvoice.Text = ((Convert.ToDouble(txtTotalInvoice.Text) + Convert.ToDouble(txtVatValue.Text)) - Convert.ToDouble(txtTotalDescound.Text)).ToString();
          
            
            
            }
            catch
            {

                return;
            }


        }



        private void txtPayValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }






        private void AddDgv_PayType(DataGridView dgv, string PayTypeID, string PayTypeName, string PayValue, string Statement, string CurrencyID2, string CurrencyName2, string CurrencyRate2, string CurrencyPrice2)
        {
            try
            {
                dgv.Rows.Add();
                dgv.ClearSelection();
                dgv.Rows[dgv.Rows.Count - 1].Selected = true;
                var lastRows = dgv.Rows.Count - 1;
                dgv.Rows[lastRows].Cells["PayTypeID"].Value = PayTypeID;
                dgv.Rows[lastRows].Cells["PayTypeName"].Value = PayTypeName;
                dgv.Rows[lastRows].Cells["PayValue"].Value = PayValue;
                dgv.Rows[lastRows].Cells["Statement"].Value = Statement;

                dgv.Rows[lastRows].Cells["CurrencyID2"].Value = CurrencyID2;
                dgv.Rows[lastRows].Cells["CurrencyName2"].Value = CurrencyName2;
                dgv.Rows[lastRows].Cells["CurrencyRate2"].Value = CurrencyRate2;
                dgv.Rows[lastRows].Cells["CurrencyPrice2"].Value = CurrencyPrice2;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }







        private void txtCurrencyRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void ChUseVat_CheckedChanged(object sender, EventArgs e)
        {
            if (chUseVat.Checked == true)
            {
                txtMainVat.Text = BillSetting_cls.Vat;
            }
            else
            {
                txtMainVat.Text = "0";
            }


        }

        private void txtMainVat_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }
    }
}
