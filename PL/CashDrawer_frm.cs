﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class CashDrawer_frm : Form
    {
        public CashDrawer_frm()
        {
            InitializeComponent();
        }
        CashDrawer_cls cls = new CashDrawer_cls();
        private void CashDrawer_frm_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingToolStrip(toolStrip1);
            ERB_Setting.SettingForm(this);
            ERB_Setting.SettingDGV(DGV1);

            UserPermissions_CLS Store_cls = new UserPermissions_CLS();
            combUser.DataSource = Store_cls.Search_UserPermissions();
            combUser.DisplayMember = "EmpName";
            combUser.ValueMember = "ID";
            combUser.Text = null;

            DGV1.DataSource = null;
            DGV1.Rows.Clear();

        }


        private void Expense()
        {
            try
            {
                DataTable dt = new DataTable();
                if (combUser.SelectedIndex == -1)
                {
                    dt = cls.Search_Expenses(dateTimePicker1.Value);
                }
                else
                {
                    dt = cls.Search_Expenses(dateTimePicker1.Value, combUser.SelectedValue.ToString());
                }

                String ValueDebit = dt.Rows[0]["ExpenseValue"].ToString();
                if (ValueDebit == "0")
                {
                    return;
                }
                AddRowDgv("المصروفات", "0", ValueDebit);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void ImportCustomer()
        {
            try
            {
                DataTable dt = new DataTable();
                if (combUser.SelectedIndex == -1)
                {
                    dt = cls.Search_ImportCustomer(dateTimePicker1.Value);
                }
                else
                {
                    dt = cls.Search_ImportCustomer(dateTimePicker1.Value, combUser.SelectedValue.ToString());
                }

                String ValueDebit = dt.Rows[0]["PayValue"].ToString();
                if (ValueDebit == "0")
                {
                    return;
                }
                AddRowDgv("سند قبض عميل", ValueDebit, "0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void ExportCustomer()
        {
            try
            {
                DataTable dt = new DataTable();
                if (combUser.SelectedIndex == -1)
                {
                    dt = cls.Search_ExportCustomer(dateTimePicker1.Value);
                }
                else
                {
                    dt = cls.Search_ExportCustomer(dateTimePicker1.Value, combUser.SelectedValue.ToString());
                }
                String ValueDebit = dt.Rows[0]["PayValue"].ToString();
                if (ValueDebit == "0")
                {
                    return;
                }
                AddRowDgv("سند صرف عميل", "0", ValueDebit);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void ImportSupp()
        {
            try
            {
                DataTable dt = new DataTable();
                if (combUser.SelectedIndex == -1)
                {
                    dt = cls.Search_ImportSuppliers(dateTimePicker1.Value);
                }
                else
                {
                    dt = cls.Search_ImportSuppliers(dateTimePicker1.Value, combUser.SelectedValue.ToString());
                }
                String ValueDebit = dt.Rows[0]["PayValue"].ToString();
                if (ValueDebit == "0")
                {
                    return;
                }
                AddRowDgv("سند قبض مورد", ValueDebit, "0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void ExportSupp()
        {
            try
            {
                DataTable dt = new DataTable();
                if (combUser.SelectedIndex == -1)
                {
                    dt = cls.Search_ExportSuppliers(dateTimePicker1.Value);
                }
                else
                {
                    dt = cls.Search_ExportSuppliers(dateTimePicker1.Value, combUser.SelectedValue.ToString());
                }
                String ValueDebit = dt.Rows[0]["PayValue"].ToString();
                if (ValueDebit == "0")
                {
                    return;
                }
                AddRowDgv("سند دفع مورد", "0", ValueDebit);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private void Cash_Import()
        {
            try
            {
                DataTable dt = new DataTable();
                if (combUser.SelectedIndex == -1)
                {
                    dt = cls.Search_Cash_Import(dateTimePicker1.Value);
                }
                else
                {
                    dt = cls.Search_Cash_Import(dateTimePicker1.Value, combUser.SelectedValue.ToString());
                }
                String ValueDebit = dt.Rows[0]["Value"].ToString();
                if (ValueDebit == "0")
                {
                    return;
                }
                AddRowDgv("سند ايداع نقدي", ValueDebit, "0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void Cash_Export()
        {
            try
            {
                DataTable dt = new DataTable();
                if (combUser.SelectedIndex == -1)
                {
                    dt = cls.Search_Cash_Export(dateTimePicker1.Value);
                }
                else
                {
                    dt = cls.Search_Cash_Export(dateTimePicker1.Value, combUser.SelectedValue.ToString());
                }
                String ValueDebit = dt.Rows[0]["Value"].ToString();
                if (ValueDebit == "0")
                {
                    return;
                }
                AddRowDgv("سند صرف نقدي", "0", ValueDebit);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }



        private void Maintenance()
        {
            try
            {
                DataTable dt = new DataTable();
                if (combUser.SelectedIndex == -1)
                {
                    dt = cls.Search_Maintenance(dateTimePicker1.Value);
                }
                else
                {
                    dt = cls.Search_Maintenance(dateTimePicker1.Value, combUser.SelectedValue.ToString());
                }
                String ValueDebit = dt.Rows[0]["Paid"].ToString();
                if (ValueDebit != "0")
                {
                    AddRowDgv("الصيانة", ValueDebit, "0");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void Sales()
        {
            try
            {
                DataTable dt = new DataTable();
                if (combUser.SelectedIndex == -1)
                {
                    dt = cls.Search_Sales(dateTimePicker1.Value);
                }
                else
                {
                    dt = cls.Search_Sales(dateTimePicker1.Value, combUser.SelectedValue.ToString());
                }
                String ValueDebit = dt.Rows[0]["PaidInvoice"].ToString();
                String ValueCridet = dt.Rows[0]["PaidReturnInvoice"].ToString();
                if (ValueDebit != "0")
                {
                    AddRowDgv("المبيعات", ValueDebit,"0");
                }
                if (ValueCridet != "0")
                {
                    AddRowDgv("مرتجع المبيعات", "0", ValueCridet);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }



        private void Bay()
        {
            try
            {
                DataTable dt = new DataTable();
                if (combUser.SelectedIndex == -1)
                {
                    dt = cls.Search_Bay(dateTimePicker1.Value);
                }
                else
                {
                    dt = cls.Search_Bay(dateTimePicker1.Value, combUser.SelectedValue.ToString());
                }
                String ValueDebit = dt.Rows[0]["PaidInvoice"].ToString();
                String ValueCridet = dt.Rows[0]["PaidReturnInvoice"].ToString();
                if (ValueDebit != "0")
                {
                    AddRowDgv("المشتريات", "0", ValueDebit);
                }
                if (ValueCridet != "0")
                {
                    AddRowDgv("مرتجع المشتريات", ValueCridet, "0" );
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }





        private void sum()
        {
            try
            {
                double debit = 0;
                double crdit = 0;
                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    debit += Convert.ToDouble(DGV1.Rows[i].Cells[1].Value.ToString());
                    crdit += Convert.ToDouble(DGV1.Rows[i].Cells[2].Value.ToString());
                }

                textBox1.Text = debit.ToString();
                textBox2.Text = crdit.ToString();
                textBox3.Text = (debit - crdit).ToString();
            }
            catch
            {


            }




        }


        private void AddRowDgv(string name, string debit, string cridet)
        {
            try
            {
                DGV1.Rows.Add();
                DGV1.ClearSelection();
                DGV1.Rows[DGV1.Rows.Count - 1].Selected = true;
                var lastRows = DGV1.Rows.Count - 1;
                DGV1.Rows[lastRows].Cells[0].Value = name;
                DGV1.Rows[lastRows].Cells[1].Value = debit;
                DGV1.Rows[lastRows].Cells[2].Value = cridet;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            CashDrawer_frm_Load(null, null);
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            DGV1.DataSource = null;
            DGV1.Rows.Clear();


            Sales();
            Bay();
            ImportCustomer();
            ExportCustomer();
            ImportSupp();
            ExportSupp();
            Expense();
            Maintenance();
            Cash_Import();
            Cash_Export();
            sum();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (DGV1.Rows.Count == 0)
            {
                return;
            }
            RbtGeneralDay_frm frm = new RbtGeneralDay_frm();

            if (combUser.SelectedIndex == -1)
            {
                frm.searshFrm = false;
            }
            else
            {
                frm.searshFrm = true;
                frm.FillUsers();
                frm.LookUpEdit_PartID .Text = combUser.Text;

            }
            frm.datefrom .DateTime  = dateTimePicker1.Value;
            frm.datetto .DateTime  = dateTimePicker1.Value;
            frm.btnView_Click(null, null);
            frm.ShowDialog();
        }






    }
}
