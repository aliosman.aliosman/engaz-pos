﻿namespace ByStro.PL
{
    partial class CombanyInformatuon_FormAR
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CombanyInformatuon_FormAR));
            this.txtCombanyDescriptone = new System.Windows.Forms.TextBox();
            this.Label14 = new System.Windows.Forms.Label();
            this.txtPhoneNamber = new System.Windows.Forms.TextBox();
            this.txtCombanyName = new System.Windows.Forms.TextBox();
            this.Label12 = new System.Windows.Forms.Label();
            this.Label16 = new System.Windows.Forms.Label();
            this.Label13 = new System.Windows.Forms.Label();
            this.txtMyName = new System.Windows.Forms.TextBox();
            this.p_box = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.txtBillRemark = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMaintenanceRemark = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.p_box)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtCombanyDescriptone
            // 
            this.txtCombanyDescriptone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCombanyDescriptone.Location = new System.Drawing.Point(22, 107);
            this.txtCombanyDescriptone.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtCombanyDescriptone.Name = "txtCombanyDescriptone";
            this.txtCombanyDescriptone.Size = new System.Drawing.Size(463, 24);
            this.txtCombanyDescriptone.TabIndex = 1;
            this.txtCombanyDescriptone.TextChanged += new System.EventHandler(this.txtCombanyDescriptone_TextChanged);
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Location = new System.Drawing.Point(275, 135);
            this.Label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label14.Name = "Label14";
            this.Label14.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label14.Size = new System.Drawing.Size(78, 17);
            this.Label14.TabIndex = 16;
            this.Label14.Text = ": رقم الهاتف";
            // 
            // txtPhoneNamber
            // 
            this.txtPhoneNamber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPhoneNamber.Location = new System.Drawing.Point(269, 158);
            this.txtPhoneNamber.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtPhoneNamber.Name = "txtPhoneNamber";
            this.txtPhoneNamber.Size = new System.Drawing.Size(216, 24);
            this.txtPhoneNamber.TabIndex = 3;
            // 
            // txtCombanyName
            // 
            this.txtCombanyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCombanyName.Location = new System.Drawing.Point(22, 60);
            this.txtCombanyName.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtCombanyName.Name = "txtCombanyName";
            this.txtCombanyName.Size = new System.Drawing.Size(463, 24);
            this.txtCombanyName.TabIndex = 0;
            this.txtCombanyName.TextChanged += new System.EventHandler(this.txtCombanyName_TextChanged);
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Location = new System.Drawing.Point(22, 134);
            this.Label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label12.Name = "Label12";
            this.Label12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label12.Size = new System.Drawing.Size(101, 17);
            this.Label12.TabIndex = 18;
            this.Label12.Text = ": صاحب الشركة";
            // 
            // Label16
            // 
            this.Label16.AutoSize = true;
            this.Label16.Location = new System.Drawing.Point(22, 87);
            this.Label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label16.Name = "Label16";
            this.Label16.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label16.Size = new System.Drawing.Size(100, 17);
            this.Label16.TabIndex = 14;
            this.Label16.Text = ": نشاط الشركة ";
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Location = new System.Drawing.Point(27, 31);
            this.Label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label13.Name = "Label13";
            this.Label13.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Label13.Size = new System.Drawing.Size(95, 17);
            this.Label13.TabIndex = 12;
            this.Label13.Text = ": اسم الشركة ";
            // 
            // txtMyName
            // 
            this.txtMyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMyName.Location = new System.Drawing.Point(19, 158);
            this.txtMyName.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtMyName.Name = "txtMyName";
            this.txtMyName.Size = new System.Drawing.Size(246, 24);
            this.txtMyName.TabIndex = 2;
            // 
            // p_box
            // 
            this.p_box.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.p_box.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.p_box.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.p_box.Cursor = System.Windows.Forms.Cursors.Hand;
            this.p_box.Image = global::ByStro.Properties.Resources.no_photo_icon_big_ar;
            this.p_box.Location = new System.Drawing.Point(505, 30);
            this.p_box.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.p_box.Name = "p_box";
            this.p_box.Size = new System.Drawing.Size(178, 178);
            this.p_box.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.p_box.TabIndex = 20;
            this.p_box.TabStop = false;
            this.p_box.Click += new System.EventHandler(this.p_box_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 187);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label1.Size = new System.Drawing.Size(66, 17);
            this.label1.TabIndex = 16;
            this.label1.Text = ":العنــــوان";
            // 
            // txtTitle
            // 
            this.txtTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTitle.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtTitle.Location = new System.Drawing.Point(19, 208);
            this.txtTitle.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtTitle.Size = new System.Drawing.Size(466, 24);
            this.txtTitle.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(505, 213);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(91, 27);
            this.button1.TabIndex = 692;
            this.button1.Text = "اضافة صورة";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(596, 213);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(85, 27);
            this.button2.TabIndex = 693;
            this.button2.Text = "حذف الصورة";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = global::ByStro.Properties.Resources.Save_30px;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(77, 22);
            this.toolStripButton2.Text = "حفظ F2 ";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.toolStrip1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(691, 25);
            this.toolStrip1.TabIndex = 691;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // txtBillRemark
            // 
            this.txtBillRemark.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBillRemark.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtBillRemark.Location = new System.Drawing.Point(19, 256);
            this.txtBillRemark.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtBillRemark.Multiline = true;
            this.txtBillRemark.Name = "txtBillRemark";
            this.txtBillRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtBillRemark.Size = new System.Drawing.Size(658, 40);
            this.txtBillRemark.TabIndex = 694;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 235);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label2.Size = new System.Drawing.Size(97, 17);
            this.label2.TabIndex = 695;
            this.label2.Text = ": ملاحظة فاتورة";
            // 
            // txtMaintenanceRemark
            // 
            this.txtMaintenanceRemark.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMaintenanceRemark.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtMaintenanceRemark.Location = new System.Drawing.Point(21, 320);
            this.txtMaintenanceRemark.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtMaintenanceRemark.Multiline = true;
            this.txtMaintenanceRemark.Name = "txtMaintenanceRemark";
            this.txtMaintenanceRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtMaintenanceRemark.Size = new System.Drawing.Size(657, 42);
            this.txtMaintenanceRemark.TabIndex = 696;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 299);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label3.Size = new System.Drawing.Size(104, 17);
            this.label3.TabIndex = 697;
            this.label3.Text = ": ملاحظة الصيانة";
            // 
            // CombanyInformatuon_FormAR
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(691, 370);
            this.Controls.Add(this.txtMaintenanceRemark);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBillRemark);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.txtTitle);
            this.Controls.Add(this.txtCombanyDescriptone);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Label14);
            this.Controls.Add(this.txtPhoneNamber);
            this.Controls.Add(this.txtCombanyName);
            this.Controls.Add(this.Label12);
            this.Controls.Add(this.Label16);
            this.Controls.Add(this.Label13);
            this.Controls.Add(this.txtMyName);
            this.Controls.Add(this.p_box);
            this.Font = new System.Drawing.Font("Tahoma", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "CombanyInformatuon_FormAR";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "بيانات الشركة ";
            this.Load += new System.EventHandler(this.CombanyInformatuon_FormAR_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CombanyInformatuon_FormAR_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.p_box)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.TextBox txtCombanyDescriptone;
        internal System.Windows.Forms.Label Label14;
        internal System.Windows.Forms.TextBox txtPhoneNamber;
        internal System.Windows.Forms.TextBox txtCombanyName;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.Label Label16;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.TextBox txtMyName;
        internal System.Windows.Forms.PictureBox p_box;
        internal System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        public System.Windows.Forms.ToolStrip toolStrip1;
        public System.Windows.Forms.TextBox txtBillRemark;
        internal System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtMaintenanceRemark;
        internal System.Windows.Forms.Label label3;
    }
}