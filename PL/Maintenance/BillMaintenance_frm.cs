﻿using ByStro.RPT;
using System;
using System.Data;
using System.Drawing;

using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class BillMaintenance_frm : Form
    {
        public BillMaintenance_frm()
        {
            InitializeComponent();
        }
        BillMaintenance_cls cls = new BillMaintenance_cls();
        TreasuryMovement_cls TreasuryMovement_CLS = new TreasuryMovement_cls();
        PayType_trans_cls PayType_trans_cls = new PayType_trans_cls();

        string TreasuryID = "0";
        private void PureItemToStoreForm_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingToolStrip(toolStrip1);
            ERB_Setting.SettingForm(this);
            ERB_Setting.SettingDGV(DGV1);
            FillCombAccount();
            FillProdecut();
            FillPayType();
            FillCurrency();
            btnNew_Click(null, null);

        }


        public void FillCurrency()
        {
            string cusName = combCurrency.Text;
            Currency_cls Currency_cls = new Currency_cls();
            combCurrency.DataSource = Currency_cls.Search__Currency("");
            combCurrency.DisplayMember = "CurrencyName";
            combCurrency.ValueMember = "CurrencyID";
            combCurrency.Text = cusName;

        }

        public void FillPayType()
        {
            string cusName = combPayType.Text;
            PayType_cls PayType_cls = new PayType_cls();
            combPayType.DataSource = PayType_cls.Search__PayType("");
            combPayType.DisplayMember = "PayTypeName";
            combPayType.ValueMember = "PayTypeID";
            combPayType.Text = cusName;

        }



        public void FillProdecut()
        {
            loadaccount_CLS.MaintenanceLoadtxt(txtSearch);

        }
        // fill comboxe All Suppliers
        public void FillCombAccount()
        {
            Customers_cls Customers_cls = new Customers_cls();
            string supName = combAccount.Text;
            string supID = txtAccountID.Text;
            combAccount.DataSource = Customers_cls.Search_Customers("");
            combAccount.DisplayMember = "CustomerName";
            combAccount.ValueMember = "CustomerID";
            combAccount.Text = supName;
            txtAccountID.Text = supID;
        }



















        private void DGV1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {

        }

        DamagedType_cls DamagedType_cls = new DamagedType_cls();
        DataTable dt_Prodecut = new DataTable();
        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtSearch.Text.Trim() == "")
                {
                    cleartxt();
                    return;
                }


                #region "تحميل الباركود واكود الصنف واسم الصنف في مكانة  السليم"

                //====================================================================================
                try
                {

                    dt_Prodecut = DamagedType_cls.Search__DamagedType(txtSearch.Text);

                    String ProdecutName = txtSearch.Text;
                    txtProdecutID.Text = dt_Prodecut.Rows[0]["DamagedTypeID"].ToString();
                    txtSearch.Text = dt_Prodecut.Rows[0]["DamagedTypeName"].ToString();


                    txtQuantity.Text = "1";

                    txtQuantity.Focus();
                }
                catch
                {
                    MessageBox.Show("الصنف المدخل غير موجود يرجي مراجعة الاصناف", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtSearch.Text = "";
                    txtSearch.Focus();
                }


                //==================================================================================================================

                #endregion

                //SumitemdDGV();
            }

        }

        private void combUnit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtQuantity.Focus();
            }
        }

        private void txtQty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtPrice.Focus();
            }
        }

        private void txtPrise_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AddItem_DGV();
            }
        }





        #region" اضافة الي الداتا قرد فيو   "
        //string AVG_Price = "0";

        public void AddItem_DGV()
        {
            try
            {
                if (txtSearch.Text == "" || txtProdecutID.Text == "")
                {
                    txtSearch.BackColor = Color.Pink;
                    txtSearch.Focus();
                    return;
                }


                if (txtQuantity.Text == "" || Convert.ToDouble(txtQuantity.Text) <= Convert.ToDouble(0))
                {
                    txtQuantity.BackColor = Color.Pink;
                    txtQuantity.Focus();
                    return;
                }


                if (txtPrice.Text == "" || Convert.ToDouble(txtPrice.Text) <= Convert.ToDouble(0))
                {
                    txtPrice.BackColor = Color.Pink;
                    txtPrice.Focus();
                    return;
                }




                #region Update Price



                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    if (DGV1.Rows[i].Cells["DamagedTypeID"].Value.ToString() == txtProdecutID.Text)
                    {
                        if (MessageBox.Show("هذ الصنف سبق وتم اضافتة ضمن الفاتورة  : هل تريد اضافة  " + txtQuantity.Text + "  الي الكمية ", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            Double qty = Convert.ToDouble(DGV1.Rows[i].Cells["Quantity"].Value) + Convert.ToDouble(txtQuantity.Text);
                            DGV1.Rows[i].Cells["Quantity"].Value = +qty;
                            Double x = Convert.ToDouble(DGV1.Rows[i].Cells["Quantity"].Value) * Convert.ToDouble(DGV1.Rows[i].Cells["Price"].Value);
                            DGV1.Rows[i].Cells["TotalPrice"].Value = x;
                            SumDgv();
                            cleartxt();
                            return;
                        }
                        else
                        {
                            return;
                        }
                    }
                }





                AddRowDgv(txtProdecutID.Text, txtSearch.Text, txtQuantity.Text, (double.Parse(txtPrice.Text) * double.Parse(txtCurrencyRate.Text)).ToString() , (double.Parse(txtTotalPrice.Text) * double.Parse(txtCurrencyRate.Text)).ToString() ,combCurrency.SelectedValue.ToString(),combCurrency.Text,txtCurrencyRate.Text,txtPrice.Text,txtTotalPrice.Text);
                lblCount.Text = DGV1.Rows.Count.ToString();

                SumDgv();
                cleartxt();

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        private void SumDgv()
        {
            double sum = 0;
            for (int i = 0; i < DGV1.Rows.Count; i++)
            {
                sum += Convert.ToDouble(DGV1.Rows[i].Cells["TotalPrice"].Value);

            }
            txtTotalInvoice.Text = sum.ToString();



        }
        private void AddRowDgv(string DamagedTypeID, string DamagedTypeName, string Quantity, string Price, string TotalPrice, string CurrencyID, string CurrencyName, string CurrencyRate, string CurrencyPrice,string CurrencyTotal)
        {
            try
            {
                DGV1.Rows.Add();
                DGV1.ClearSelection();
                DGV1.Rows[DGV1.Rows.Count - 1].Selected = true;
                var lastRows = DGV1.Rows.Count - 1;
                DGV1.Rows[lastRows].Cells["DamagedTypeID"].Value = DamagedTypeID;
                DGV1.Rows[lastRows].Cells["DamagedTypeName"].Value = DamagedTypeName;
                DGV1.Rows[lastRows].Cells["Quantity"].Value = Quantity;
                DGV1.Rows[lastRows].Cells["Price"].Value = Price;
                DGV1.Rows[lastRows].Cells["TotalPrice"].Value = TotalPrice;
                DGV1.Rows[lastRows].Cells["CurrencyID"].Value = CurrencyID;
                DGV1.Rows[lastRows].Cells["CurrencyName"].Value = CurrencyName;
                DGV1.Rows[lastRows].Cells["CurrencyRate"].Value = CurrencyRate;
                DGV1.Rows[lastRows].Cells["CurrencyPrice"].Value = CurrencyPrice;
                DGV1.Rows[lastRows].Cells["CurrencyTotal"].Value = CurrencyTotal;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }
        private void cleartxt()
        {
            txtProdecutID.Text = "";
            txtSearch.Text = "";
            txtQuantity.Text = "";
            txtPrice.Text = "";
            txtTotalPrice.Text = "";

            txtSearch.Focus();

        }

        private void txtQty_TextChanged(object sender, EventArgs e)
        {
            txtQuantity.BackColor = Color.White;
            SumTotal_txt();

        }


        private void SumTotal_txt()
        {
            try
            {



                if (txtQuantity.Text == "")
                {
                    txtTotalPrice.Text = "0";
                    txtQuantity.Focus();
                    return;
                }

                if (txtPrice.Text == "")
                {
                    txtTotalPrice.Text = "0";
                    txtPrice.Focus();
                    return;
                }


                txtTotalPrice.Text = Convert.ToString(Convert.ToDouble(txtQuantity.Text) * Convert.ToDouble(txtPrice.Text));
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


        }
        private void txtPrise_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveBill(false);
        }
        private void SaveBill(Boolean print)
        {
            try
            {

                if (lblCount.Text == "0")
                {
                    MessageBox.Show("لا يمكن حفظ فاتورة خالية من الاصناف", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else if (combAccount.Text.Trim() == "" || txtAccountID.Text.Trim() == "")
                {
                    combAccount.BackColor = Color.Pink;
                    combAccount.Focus();
                    return;
                }






                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    if (DGV1.Rows[i].Cells["Quantity"].Value.ToString() == "0" || DGV1.Rows[i].Cells["Quantity"].Value.ToString() == "")
                    {
                        MessageBox.Show("من فضلك يرجي مراجعة الكميات المدخلة في الفاتورة  ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        DGV1.Rows[i].Selected = true;
                        return;
                    }

                }

                if (combPayKind.SelectedIndex == 0)
                {
                    if (double.Parse(txtpaid_Invoice.Text) < double.Parse(txtNetInvoice.Text))
                    {
                        MessageBox.Show(string.Format("{0}\n{1}\n", "المبلغ المدفوع اقل من صافي الفاتورة ", "في حالة نوع الدفع كاش لا يمكنك دفع اقل من صافي الفاتورة"), "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                }



                txtMainID.Text = cls.MaxID_Maintenance_Main();

                string paidInvoice = txtNetInvoice.Text;
                if (combPayKind.SelectedIndex == 1)
                {
                    paidInvoice = txtpaid_Invoice.Text;
                }

                TreasuryID = TreasuryMovement_CLS.MaxID_TreasuryMovement();
                TreasuryMovement_CLS.InsertTreasuryMovement(TreasuryID, txtAccountID.Text, "Cus", txtMainID.Text, txtBillTypeID.Text, this.Text, D1.Value, this.Text + " : " + combAccount.Text, txtpaid_Invoice.Text, "0", this.Text + " : " + combPayKind.Text, txtNetInvoice.Text, paidInvoice, Properties.Settings.Default.UserID);

                //====Bay_Main===========================================================================================================================================================================================================================================================


                cls.Insert_Maintenance_Main(txtMainID.Text, D1.Value, txtAccountID.Text, txtRemarks.Text, combPayKind.Text, txtTotalInvoice.Text, txtTotalDescound.Text, txtNetInvoice.Text, txtpaid_Invoice.Text, txtRestInvoise.Text, TreasuryID, Properties.Settings.Default.UserID);
                //====Bay_Main===========================================================================================================================================================================================================================================================
                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    cls.Insert_Maintenance_Details(txtMainID.Text, DGV1.Rows[i].Cells["DamagedTypeID"].Value.ToString(), DGV1.Rows[i].Cells["Quantity"].Value.ToString(), DGV1.Rows[i].Cells["Price"].Value.ToString(), DGV1.Rows[i].Cells["TotalPrice"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyID"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyRate"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyPrice"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyTotal"].Value.ToString());

                }




                #region // Pay
                for (int i = 0; i < DGVPayType.Rows.Count; i++)
                {
                    string Statement = "عميل" + " / " + combAccount.Text;
                    if (!string.IsNullOrEmpty(DGVPayType.Rows[i].Cells["Statement"].Value.ToString()))
                    {
                        Statement = DGVPayType.Rows[i].Cells["Statement"].Value.ToString();
                    }
                    PayType_trans_cls.Insert_PayType_trans(txtMainID.Text, D1.Value, DGVPayType.Rows[i].Cells["PayTypeID"].Value.ToString(), txtMainID.Text, this.Text, txtBillTypeID.Text, 0, double.Parse(DGVPayType.Rows[i].Cells["PayValue"].Value.ToString()), Statement, Properties.Settings.Default.UserID, DGVPayType.Rows[i].Cells["CurrencyID2"].Value.ToString(), DGVPayType.Rows[i].Cells["CurrencyRate2"].Value.ToString(), DGVPayType.Rows[i].Cells["CurrencyPrice2"].Value.ToString());
                }
                #endregion

                if (print==true)
                {
                  //  PrintBill();
                }

                if (BillSetting_cls.ShowMessageSave == false)
                {
                    Mass.Saved();
                }

                btnNew_Click(null, null);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }
        //=======================================================================================================

        //public void PrintBill()
        //{
        //    try
        //    {

        //        if (BillSetting_cls.PrintSize == 1)
        //        {
        //            PrintSizeA4();
        //        }
        //        else if (BillSetting_cls.PrintSize == 2)
        //        {
        //            PrintSize1_2A4();
        //        }
        //        else
        //        {
        //            PrintSize80MM();

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //}

   //     private void PrintSizeA4()
   //     {

   //         DataTable dt_cus = TreasuryMovement_CLS.Balence_ISCusSupp(txtAccountID.Text, "Cus");//Cus_Trans_cls.Cus_Balence(txtAccountID.Text);
   //         BillMaintenance_cls cls_print = new BillMaintenance_cls();
   //         DataTable dt_Print = cls_print.Details_Maintenance_Details(txtMainID.Text);

   //         Reportes_Form frm = new Reportes_Form();

   //         var report = new CRBill_Maintenance_A4();
   //         report.Database.Tables["Bill_Maintenance"].SetDataSource(dt_Print);
   //         report.SetParameterValue(0, combAccount.Text);
   //         report.SetParameterValue(1, txtPhone.Text);
   //         report.SetParameterValue(2, txtMainID.Text);
   //         report.SetParameterValue(3, combPayKind.Text);
   //         report.SetParameterValue(4, txtRemarks.Text);
   //         report.SetParameterValue(5, txtTotalInvoice.Text);
   //         report.SetParameterValue(6, txtTotalDescound.Text);
   //         report.SetParameterValue(7, txtNetInvoice.Text);
   //         report.SetParameterValue(8, txtpaid_Invoice.Text);
   //         report.SetParameterValue(9, txtRestInvoise.Text);
   //         report.SetParameterValue(10, dt_cus.Rows[0]["balence"].ToString());
   //         report.SetParameterValue(11, D1.Value);
   //         frm.crystalReportViewer1.ReportSource = report;
   //         frm.crystalReportViewer1.Refresh();
   //         #region"Crystal Report | Printing | Default Printer"
   //PrintDialog dialog = new PrintDialog();
   //         if (dialog.ShowDialog() == DialogResult.OK)
   //         {
   //             int copies = dialog.PrinterSettings.Copies;
   //             int fromPage = dialog.PrinterSettings.FromPage;
   //             int toPage = dialog.PrinterSettings.ToPage;
   //             bool collate = dialog.PrinterSettings.Collate;

   //             report.PrintOptions.PrinterName = dialog.PrinterSettings.PrinterName;
   //             report.PrintToPrinter(copies, collate, fromPage, toPage);
   //         }


   //         //report.PrintOptions.PrinterName = dialog.PrinterSettings.PrinterName;
   //         //report.PrintToPrinter(1, false, 0, 0);
   //         #endregion

   //         //#region"Crystal Report | Printing | Default Printer"
   //         //var dialog = new PrintDialog();
   //         //report.PrintOptions.PrinterName = dialog.PrinterSettings.PrinterName;
   //         //report.PrintToPrinter(1, false, 0, 0);

   //         ////});



   //         //#endregion

   //         //frm.ShowDialog();
   //         #endregion

   //     }

        //private void PrintSize1_2A4()
        //{

        //    DataTable dt_cus = TreasuryMovement_CLS.Balence_ISCusSupp(txtAccountID.Text, "Cus");
        //    BillMaintenance_cls cls_print = new BillMaintenance_cls();
        //    DataTable dt_Print = cls_print.Details_Maintenance_Details(txtMainID.Text);
        //    Reportes_Form frm = new Reportes_Form();
        //    var report = new CRBill_Maintenance();
        //    report.Database.Tables["Bill_Maintenance"].SetDataSource(dt_Print);
        //    report.SetParameterValue(0, combAccount.Text);
        //    report.SetParameterValue(1, txtPhone.Text);
        //    report.SetParameterValue(2, txtMainID.Text);
        //    report.SetParameterValue(3, combPayKind.Text);
        //    report.SetParameterValue(4, txtRemarks.Text);
        //    report.SetParameterValue(5, txtTotalInvoice.Text);
        //    report.SetParameterValue(6, txtTotalDescound.Text);
        //    report.SetParameterValue(7, txtNetInvoice.Text);
        //    report.SetParameterValue(8, txtpaid_Invoice.Text);
        //    report.SetParameterValue(9, txtRestInvoise.Text);
        //    report.SetParameterValue(10, dt_cus.Rows[0]["balence"].ToString());
        //    report.SetParameterValue(11, D1.Value);
        //    frm.crystalReportViewer1.ReportSource = report;
        //    frm.crystalReportViewer1.Refresh();
        //    #region"Crystal Report | Printing | Default Printer"
        //    PrintDialog dialog = new PrintDialog();
        //    if (dialog.ShowDialog() == DialogResult.OK)
        //    {
        //        int copies = dialog.PrinterSettings.Copies;
        //        int fromPage = dialog.PrinterSettings.FromPage;
        //        int toPage = dialog.PrinterSettings.ToPage;
        //        bool collate = dialog.PrinterSettings.Collate;

        //        report.PrintOptions.PrinterName = dialog.PrinterSettings.PrinterName;
        //        report.PrintToPrinter(copies, collate, fromPage, toPage);
        //    }


        //    //report.PrintOptions.PrinterName = dialog.PrinterSettings.PrinterName;
        //    //report.PrintToPrinter(1, false, 0, 0);
        //    #endregion

        //    //#region"Crystal Report | Printing | Default Printer"
        //    //var dialog = new PrintDialog();
        //    //report.PrintOptions.PrinterName = dialog.PrinterSettings.PrinterName;
        //    //report.PrintToPrinter(1, false, 0, 0);
        //    //#endregion

        //    //frm.ShowDialog();

        //}

        //private void PrintSize80MM()
        //{

        //    DataTable dt_cus = TreasuryMovement_CLS.Balence_ISCusSupp(txtAccountID.Text, "Cus");
        //    BillMaintenance_cls cls_print = new BillMaintenance_cls();
        //    DataTable dt_Print = cls_print.Details_Maintenance_Details(txtMainID.Text);
        //    Reportes_Form frm = new Reportes_Form();
        //    var report = new CRBill_Maintenance80MM();
        //    report.Database.Tables["Bill_Maintenance"].SetDataSource(dt_Print);
        //    report.SetParameterValue(0, combAccount.Text);
        //    report.SetParameterValue(1, txtPhone.Text);
        //    report.SetParameterValue(2, txtMainID.Text);
        //    report.SetParameterValue(3, combPayKind.Text);
        //    report.SetParameterValue(4, txtRemarks.Text);
        //    report.SetParameterValue(5, txtTotalInvoice.Text);
        //    report.SetParameterValue(6, txtTotalDescound.Text);
        //    report.SetParameterValue(7, txtNetInvoice.Text);
        //    report.SetParameterValue(8, txtpaid_Invoice.Text);
        //    report.SetParameterValue(9, txtRestInvoise.Text);
        //    report.SetParameterValue(10, dt_cus.Rows[0]["balence"].ToString());
        //    report.SetParameterValue(11, D1.Value);
        //    frm.crystalReportViewer1.ReportSource = report;
        //    frm.crystalReportViewer1.Refresh();
        //    #region"Crystal Report | Printing | Default Printer"
        //    PrintDialog dialog = new PrintDialog();
        //    if (dialog.ShowDialog() == DialogResult.OK)
        //    {
        //        int copies = dialog.PrinterSettings.Copies;
        //        int fromPage = dialog.PrinterSettings.FromPage;
        //        int toPage = dialog.PrinterSettings.ToPage;
        //        bool collate = dialog.PrinterSettings.Collate;

        //        report.PrintOptions.PrinterName = dialog.PrinterSettings.PrinterName;
        //        report.PrintToPrinter(copies, collate, fromPage, toPage);
        //    }


        //    //report.PrintOptions.PrinterName = dialog.PrinterSettings.PrinterName;
        //    //report.PrintToPrinter(1, false, 0, 0);
        //    #endregion

        //    //#region"Crystal Report | Printing | Default Printer"
        //    //var dialog = new PrintDialog();
        //    //report.PrintOptions.PrinterName = dialog.PrinterSettings.PrinterName;
        //    //report.PrintToPrinter(1, false, 0, 0);
        //    //#endregion

        //    //frm.ShowDialog();

        //}








        //=====================================================================================================

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (DataAccessLayer.CS_16 == false)
                {
                    MessageBox.Show("ليس لديك صلاحية", "صلاحيات ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                if (lblCount.Text == "0")
                {
                    MessageBox.Show("لا يمكن حفظ فاتورة خالية من الاصناف", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (combAccount.Text.Trim() == "" || txtAccountID.Text.Trim() == "")
                {
                    combAccount.BackColor = Color.Pink;
                    combAccount.Focus();
                    return;
                }



                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    if (DGV1.Rows[i].Cells["Quantity"].Value.ToString() == "0" || DGV1.Rows[i].Cells["Quantity"].Value.ToString() == "")
                    {
                        MessageBox.Show("من فضلك يرجي مراجعة الكميات المدخلة في الفاتورة  ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        DGV1.Rows[i].Selected = true;
                        return;
                    }

                }


                if (combPayKind.SelectedIndex == 0)
                {
                    if (double.Parse(txtpaid_Invoice.Text) < double.Parse(txtNetInvoice.Text))
                    {
                        MessageBox.Show(string.Format("{0}\n{1}\n", "المبلغ المدفوع اقل من صافي الفاتورة ", "في حالة نوع الدفع كاش لا يمكنك دفع اقل من صافي الفاتورة"), "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                }

                //===Treasury Movement_============================================================================================================================================================================================================================================================
                double paidInvoice = Convert.ToDouble(txtNetInvoice.Text);
                if (combPayKind.SelectedIndex == 1)
                {
                    paidInvoice = Convert.ToDouble(txtpaid_Invoice.Text);
                }

                TreasuryMovement_CLS.UpdateTreasuryMovement(TreasuryID, txtAccountID.Text, "Cus", txtMainID.Text, txtBillTypeID.Text, this.Text, D1.Value, this.Text + " : " + combAccount.Text, txtpaid_Invoice.Text,"0", txtRemarks.Text, txtNetInvoice.Text, paidInvoice.ToString());

                cls.Update_Maintenance_Main(txtMainID.Text, D1.Value, txtAccountID.Text, txtRemarks.Text, combPayKind.Text, txtTotalInvoice.Text, txtTotalDescound.Text, txtNetInvoice.Text, txtpaid_Invoice.Text, txtRestInvoise.Text);

                cls.Delete_Maintenance_Details(txtMainID.Text);
                for (int i = 0; i < DGV1.Rows.Count; i++)
                {
                    cls.Insert_Maintenance_Details(txtMainID.Text, DGV1.Rows[i].Cells["DamagedTypeID"].Value.ToString(), DGV1.Rows[i].Cells["Quantity"].Value.ToString(), DGV1.Rows[i].Cells["Price"].Value.ToString(), DGV1.Rows[i].Cells["TotalPrice"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyID"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyRate"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyPrice"].Value.ToString(), DGV1.Rows[i].Cells["CurrencyTotal"].Value.ToString());
                }


                #region // Pay
                PayType_trans_cls.Delete_PayType_trans(txtMainID.Text, txtBillTypeID.Text);
                for (int i = 0; i < DGVPayType.Rows.Count; i++)
                {
                    string Statement = "مورد" + " / " + combAccount.Text;
                    if (!string.IsNullOrEmpty(DGVPayType.Rows[i].Cells["Statement"].Value.ToString()))
                    {
                        Statement = DGVPayType.Rows[i].Cells["Statement"].Value.ToString();
                    }
                    PayType_trans_cls.Insert_PayType_trans(txtMainID.Text, D1.Value, DGVPayType.Rows[i].Cells["PayTypeID"].Value.ToString(), txtMainID.Text, this.Text, txtBillTypeID.Text, 0, double.Parse(DGVPayType.Rows[i].Cells["PayValue"].Value.ToString()), Statement, Properties.Settings.Default.UserID, DGVPayType.Rows[i].Cells["CurrencyID2"].Value.ToString(), DGVPayType.Rows[i].Cells["CurrencyRate2"].Value.ToString(), DGVPayType.Rows[i].Cells["CurrencyPrice2"].Value.ToString());
                }
                #endregion











                //====clear ========================================================================================================================================================================================================================================================
                if (BillSetting_cls.ShowMessageSave == false)
                {
                    Mass.Update();
                }
                btnNew_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }





        //======================================================================


        #endregion




        private void txtSum_TextChanged(object sender, EventArgs e)
        {

            //try
            //{

            //    if (txtSum.Text.Trim() != "")
            //    {
            //        txtArbic.Text = DAL.DataAccessLeayr.Horofcls.تحويل_الأرقام_الى_حروف(Convert.ToDecimal(txtSum.Text), 2, "", "", true, true);

            //    }
            //    else
            //    {
            //        txtArbic.Text = "";
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}

        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (txtSearch.Text.Trim() == "")
            {
                cleartxt();

            }
        }






        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                BillMaintenance_Search_frm frm = new BillMaintenance_Search_frm();
                frm.Icon = this.Icon;
                frm.ShowDialog(this);
                if (frm.LoadData == false)
                {
                    return;
                }
                DataTable dt = cls.Details_Maintenance_Main(frm.DGV2.CurrentRow.Cells["MainID"].Value.ToString());
                DataRow Dr = dt.Rows[0];
                txtMainID.Text = Dr["MainID"].ToString();
                D1.Value = Convert.ToDateTime(Dr["MyDate"]);
                txtAccountID.Text = Dr["CustomerID"].ToString();
                // Balense==========================================================================================
                combAccount.Text = Dr["CustomerName"].ToString();
                txtMainID.Text = Dr["MainID"].ToString();
                txtRemarks.Text = Dr["Remarks"].ToString();
                combPayKind.Text = Dr["TypeKind"].ToString();
                txtTotalInvoice.Text = Dr["TotalInvoice"].ToString();
                txtTotalDescound.Text = Dr["Discount"].ToString();
                txtNetInvoice.Text = Dr["NetInvoice"].ToString();
                txtpaid_Invoice.Text = Dr["PaidInvoice"].ToString();
                txtRestInvoise.Text = Dr["RestInvoise"].ToString();
                TreasuryID = Dr["TreasuryID"].ToString();
                //=========================================================================================================================================================================
                dt.Clear();
                DGV1.DataSource = null;
                DGV1.Rows.Clear();
                //=========================================================================================================================================================================
                dt = cls.Details_Maintenance_Details(frm.DGV2.CurrentRow.Cells["MainID"].Value.ToString());
                for (int i = 0; i < dt.Rows.Count; i++)
                {
              AddRowDgv(dt.Rows[i]["DamagedTypeID"].ToString(), dt.Rows[i]["DamagedTypeName"].ToString(),dt.Rows[i]["Quantity"].ToString(), dt.Rows[i]["Price"].ToString(), dt.Rows[i]["TotalPrice"].ToString(), dt.Rows[i]["CurrencyID"].ToString(), dt.Rows[i]["CurrencyName"].ToString(), dt.Rows[i]["CurrencyRate"].ToString(), dt.Rows[i]["CurrencyPrice"].ToString(), dt.Rows[i]["CurrencyTotal"].ToString());
                }

                lblCount.Text = DGV1.Rows.Count.ToString();
                SumDgv();


                // PyeType
                DGVPayType.DataSource = null;
                DGVPayType.Rows.Clear();
                DataTable dt_paytype_trans = PayType_trans_cls.Details_PayType_trans(frm.DGV2.CurrentRow.Cells["MainID"].Value.ToString(), txtBillTypeID.Text);
                for (int i = 0; i < dt_paytype_trans.Rows.Count; i++)
                {
                    AddDgv_PayType(DGVPayType, dt_paytype_trans.Rows[i]["PayTypeID"].ToString(), dt_paytype_trans.Rows[i]["PayTypeName"].ToString(), dt_paytype_trans.Rows[i]["Credit"].ToString(), dt_paytype_trans.Rows[i]["Statement"].ToString(), dt_paytype_trans.Rows[i]["CurrencyID"].ToString(), dt_paytype_trans.Rows[i]["CurrencyName"].ToString(), dt_paytype_trans.Rows[i]["CurrencyRate"].ToString(), dt_paytype_trans.Rows[i]["CurrencyPrice"].ToString());
                }
                SumPayType();






                btnSave.Enabled = false;
              // btnPrintAndSave.Enabled = false;
                btnUpdate.Enabled = true;
                btnDelete.Enabled = true;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }



        private void txtPrice_KeyDown(object sender, KeyEventArgs e)
        {
            if (txtPrice.Text.Trim() != "")
            {
                if (e.KeyCode==Keys.Enter)
                {
                    txtTotalPrice.Focus();
                }
               
            }
        }

        private void txtPrice_TextChanged(object sender, EventArgs e)
        {
            txtPrice.BackColor = Color.White;
            SumTotal_txt();

        }

        private void AllKeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void toolStripSplitButton1_ButtonClick(object sender, EventArgs e)
        {

        }

        private void OrderProudect_frm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DGV1.Rows.Count > 0)
            {
                if (Mass.close_frm() == false)
                {
                    e.Cancel = true;
                }
            }
        }







        private void txtVAT_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AddItem_DGV();
            }
        }



        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            try
            {

                if (DGV1.SelectedRows.Count >= 1)
                {
                    if (Mass.Delete() == true)
                    {
                        foreach (DataGridViewRow r in DGV1.SelectedRows)
                        {
                            DGV1.Rows.Remove(r);
                        }
                        lblCount.Text = DGV1.Rows.Count.ToString();
                        SumDgv();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtChange(object sender, EventArgs e)
        {
            try
            {

                Double total = Convert.ToDouble(txtTotalInvoice.Text);
                //  txtVatValue.Text = (total / 100).ToString();


                txtNetInvoice.Text = (Convert.ToDouble(txtTotalInvoice.Text) - Convert.ToDouble(txtTotalDescound.Text)).ToString();


                //if (combPayKind.Text == "نقدي")
                //{
                //    txtpaid_Invoice.Text = txtNetInvoice.Text;

                //}

                txtRestInvoise.Text = (Convert.ToDouble(txtNetInvoice.Text) - Convert.ToDouble(txtpaid_Invoice.Text)).ToString();

            }
            catch
            {
                return;
            }

        }

        private void txtpaid_Invoice_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtRestInvoise.Text = (Convert.ToDouble(txtNetInvoice.Text) - Convert.ToDouble(txtpaid_Invoice.Text)).ToString();
            }
            catch
            {
                return;
            }
        }

        private void combPayKind_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (combPayKind.SelectedIndex == 0)
            //{
            //    // txtpaid_Invoice.ReadOnly = true;
            //    txtpaid_Invoice.Text = txtNetInvoice.Text;

            //}
            //else
            //{
            //    //txtpaid_Invoice.ReadOnly = false;
            //    txtpaid_Invoice.Text = "0";
            //}
        }

        private void DGV1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

            try
            {
                Double x = Convert.ToDouble(DGV1.CurrentRow.Cells["Quantity"].Value) * Convert.ToDouble(DGV1.CurrentRow.Cells["CurrencyPrice"].Value);
                DGV1.CurrentRow.Cells["CurrencyTotal"].Value = x;
                DGV1.CurrentRow.Cells["Price"].Value = (Convert.ToDouble(DGV1.CurrentRow.Cells["CurrencyPrice"].Value) * double.Parse(DGV1.CurrentRow.Cells["CurrencyRate"].Value.ToString())).ToString();
                DGV1.CurrentRow.Cells["TotalPrice"].Value = Convert.ToDouble(DGV1.CurrentRow.Cells["Quantity"].Value) * double.Parse(DGV1.CurrentRow.Cells["Price"].Value.ToString());


                SumDgv();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



        private void toolStripButton5_Click(object sender, EventArgs e)
        {

            try
            {

                if (Mass.Delete() == true)
                {
                    //====Delete Quentity ========================================================================================================================================================================================================================================================
                    cls.Delete_Maintenance_Details(txtMainID.Text);
                    cls.Delete_Maintenance_Main(txtMainID.Text);
                    // Pay
                    PayType_trans_cls.Delete_PayType_trans(txtMainID.Text, txtBillTypeID.Text);

                    TreasuryMovement_CLS.DeleteTreasuryMovement(TreasuryID);

                    btnNew_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                txtMainID.Text = cls.MaxID_Maintenance_Main();
                DGV1.DataSource = null;
                DGV1.Rows.Clear();
                txtRemarks.Text = "";
                txtProdecutID.Text = "";
                txtTotalInvoice.Text = "0";
                txtSumPayType.Text = "0";
                lblCount.Text = DGV1.Rows.Count.ToString();
                cleartxt();
                //===============================================================================================================================================
                combPayKind.SelectedIndex = BillSetting_cls.kindPay;
                combAccount.Text = "";
                txtAccountID.Text = "";
                txtpaid_Invoice.Text = "0";

           
                TreasuryID = "0";
                // store defult
                if (BillSetting_cls.UseCrrencyDefault == true)
                {
                    Currency_cls Currency = new Currency_cls();
                    DataTable DtCurrency = Currency.Details_Currency(BillSetting_cls.CurrencyID.ToString());
                    combCurrency.Text = DtCurrency.Rows[0]["CurrencyName"].ToString();
                    txtCurrencyRate.Text = DtCurrency.Rows[0]["CurrencyRate"].ToString();
                }
                else
                {
                    combCurrency.Text = null;
                    txtCurrencyRate.Text = "";

                }


                btnSave.Enabled = true;
               // btnPrintAndSave.Enabled = true;
                btnUpdate.Enabled = false;
                btnDelete.Enabled = false;
                ClearPayType();

                #region UsekindPay

                //===============================================================================================================================================================


                if (BillSetting_cls.UsekindPay == true)
                {
                    PayType_cls PayType_cls = new PayType_cls();
                    DataTable dtPayType = PayType_cls.Details_PayType(BillSetting_cls.PayTypeID);
                    if (dtPayType.Rows.Count > 0)
                    {
                        combPayType.Text = dtPayType.Rows[0]["PayTypeName"].ToString();
                    }
                }
                else
                {
                    combPayType.Text = null;
                }



                //===============================================================================================================================================================

                #endregion
                DGVPayType.DataSource = null;
                DGVPayType.Rows.Clear();
                txtSearch.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private void DGV1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                toolStripButton3_Click(null, null);
            }
        }

        private void BillBay_frm_KeyDown(object sender, KeyEventArgs e)
        {


            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                else if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                else if (e.KeyCode == Keys.F2)
                {
                    //if (btnPrintAndSave.Enabled == true)
                    //{
                    //    btnPrintAndSave_Click(null, null);
                    //}
                }
                else if (e.KeyCode == Keys.F2)
                {
                    //if (btnPrintAndSave.Enabled == true)
                    //{
                    //    btnPrintAndSave_Click(null, null);
                    //}
                }
                else if (e.KeyCode == Keys.F3)
                {
                    if (btnUpdate.Enabled == true)
                    {
                        btnUpdate_Click(null, null);
                    }
                }

                else if (e.KeyCode == Keys.F5)
                {
                    button5_Click(null, null);
                }

                if (e.KeyCode == Keys.F4)
                {
                    tabControl1.SelectedIndex = 1;
                    txtPayValue.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion

        }



        private void اعادةتحميلالاصنافToolStripMenuItem_Click(object sender, EventArgs e)
        {
            loadaccount_CLS.ItemsLoadtxt(txtSearch);
        }

        private void ToolStripMenuItemSupplers_Click(object sender, EventArgs e)
        {
            try
            {
                if (combAccount.Text.Trim() == "" || txtAccountID.Text.Trim() == "")
                {
                    combAccount.BackColor = Color.Pink;
                    button1_Click_1(null, null);
                    return;
                }

                CustomersTrans_Form frm = new CustomersTrans_Form();
                frm.txtAccountID.Text = txtAccountID.Text;
                frm.txtAccountName.Text = combAccount.Text;
                frm.AddEdit = "Bay";
                frm.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }



        private void combAccount_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (combAccount.SelectedIndex > -1)
                {
                    combAccount.BackColor = Color.White;
                    txtAccountID.Text = combAccount.SelectedValue.ToString();
                }


            }
            catch
            {
                txtAccountID.Text = "";
            }
        }

        private void combAccount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                combAccount.BackColor = Color.White;
                if (combAccount.Text.Trim() == "")
                {
                    txtAccountID.Text = "";
                }
                else
                {
                    txtAccountID.Text = combAccount.SelectedValue.ToString();
                }
            }
            catch
            {
                txtAccountID.Text = "";
            }
        }

        private void txtAccountID_TextChanged(object sender, EventArgs e)
        {
        }




        private void combPayType_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (combPayType.SelectedIndex >= 0)
                {
                    txtPayValue.Focus();
                }

            }
        }

        private void combPayType_TextChanged(object sender, EventArgs e)
        {
            combPayType.BackColor = Color.White;

        }

        private void txtPayValue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!string.IsNullOrEmpty(txtPayValue.Text.Trim()))
                {
                    txtStatement.Focus();
                }

            }
        }

        private void txtPayValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void txtPayValue_TextChanged(object sender, EventArgs e)
        {
            txtPayValue.BackColor = Color.White;
        }

        private void txtStatement_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {

                if (e.KeyCode == Keys.Enter)
                {



                    if (combCurrency.SelectedIndex < 0)
                    {
                        MessageBox.Show("يرجي تحديد العملة", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        combCurrency.Focus();
                        return;
                    }
                    if (string.IsNullOrEmpty(txtCurrencyRate.Text.Trim()))
                    {
                        MessageBox.Show("يرجي تحديد سعر التعادل", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtCurrencyRate.Focus();
                        return;
                    }

                    if (combPayType.SelectedIndex < 0)
                    {
                        combPayType.BackColor = Color.Pink;
                        combPayType.Focus();
                        return;
                    }
                    if (txtPayValue.Text.Trim() == "")
                    {
                        txtPayValue.BackColor = Color.Pink;
                        txtPayValue.Focus();
                        return;
                    }
                    if (double.Parse(txtPayValue.Text) == 0)
                    {
                        txtPayValue.BackColor = Color.Pink;
                        txtPayValue.Focus();
                        return;
                    }
                    for (int i = 0; i < DGVPayType.Rows.Count; i++)
                    {
                        if (DGVPayType.Rows[i].Cells["PayTypeID"].Value.ToString() == combPayType.SelectedValue.ToString())
                        {
                            combPayType.BackColor = Color.Pink;
                            combPayType.Focus();
                            return;
                        }
                    }
                    AddDgv_PayType(DGVPayType, combPayType.SelectedValue.ToString(), combPayType.Text, (double.Parse(txtPayValue.Text)* double.Parse(txtCurrencyRate.Text)).ToString(), txtStatement.Text,combCurrency.SelectedValue.ToString(),combCurrency.Text,txtCurrencyRate.Text,txtPayValue.Text);
                    ClearPayType();
                    SumPayType();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



        private void AddDgv_PayType(DataGridView dgv, string PayTypeID, string PayTypeName, string PayValue, string Statement, string CurrencyID2, string CurrencyName2, string CurrencyRate2, string CurrencyPrice2)
        {
            try
            {
                dgv.Rows.Add();
                dgv.ClearSelection();
                dgv.Rows[dgv.Rows.Count - 1].Selected = true;
                var lastRows = dgv.Rows.Count - 1;
                dgv.Rows[lastRows].Cells["PayTypeID"].Value = PayTypeID;
                dgv.Rows[lastRows].Cells["PayTypeName"].Value = PayTypeName;
                dgv.Rows[lastRows].Cells["PayValue"].Value = PayValue;
                dgv.Rows[lastRows].Cells["Statement"].Value = Statement;
                dgv.Rows[lastRows].Cells["CurrencyID2"].Value = CurrencyID2;
                dgv.Rows[lastRows].Cells["CurrencyName2"].Value = CurrencyName2;
                dgv.Rows[lastRows].Cells["CurrencyRate2"].Value = CurrencyRate2;
                dgv.Rows[lastRows].Cells["CurrencyPrice2"].Value = CurrencyPrice2;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private void ClearPayType()
        {
            combPayType.Text = null;
            txtPayValue.Text = "";
            txtStatement.Text = "";
            combPayType.Focus();
        }

        private void SumPayType()
        {
            try
            {

                double Sum = 0;
                for (int i = 0; i < DGVPayType.Rows.Count; i++)
                {
                    Sum += Convert.ToDouble(DGVPayType.Rows[i].Cells["PayValue"].Value.ToString());
                }
                txtSumPayType.Text = Sum.ToString();
                txtpaid_Invoice.Text = txtSumPayType.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            try
            {
                if (DGVPayType.SelectedRows.Count == 0)
                {
                    return;
                }
                if (Mass.Delete() == true)
                {
                    foreach (DataGridViewRow r in DGVPayType.SelectedRows)
                    {

                        DGVPayType.Rows.Remove(r);

                    }
                    SumPayType();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DGVPayType_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

            try
            {
                if (DGVPayType.Rows.Count == 0)
                {
                    return;
                }
                double pay = double.Parse(DGVPayType.CurrentRow.Cells["CurrencyPrice2"].Value.ToString());
                double Rat = double.Parse(DGVPayType.CurrentRow.Cells["CurrencyRate2"].Value.ToString());
                DGVPayType.CurrentRow.Cells["PayValue"].Value = pay * Rat;
                SumPayType();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
 

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
         

        }

        private void combAccount_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (string.IsNullOrEmpty(combAccount.Text.Trim()))
                    {
                        return;
                    }
                    Customers_cls cls = new Customers_cls();
                    DataTable dt = cls.Search_Customers(combAccount.Text);
                    if (dt.Rows.Count > 0)
                    {
                        combAccount.Text = dt.Rows[0]["CustomerName"].ToString();
                    }
                    else
                    {

                        Customers_frm cus = new Customers_frm();
                        cus.txtPhone.Text = combAccount.Text; //txtCustomerName.Text = "";
                        cus.ClearData = false;
                        cus.ShowDialog(this);
                        if (cus.SaveData == true)
                        {
                            combAccount.Text = cus.txtCustomerName.Text;
                        }

                    }
                }
            }
            catch
            {


            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Customers_frm cus = new Customers_frm();
            cus.ShowDialog();
        }

        private void btnPrintAndSave_Click(object sender, EventArgs e)
        {
            SaveBill(true);
        }

        private void combCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (combCurrency.SelectedIndex > -1)
                {
                    Currency_cls crrcls = new Currency_cls();
                    DataTable dt = crrcls.Details_Currency(combCurrency.SelectedValue.ToString());
                    if (dt.Rows.Count > 0)
                    {
                        txtCurrencyRate.Text = dt.Rows[0]["CurrencyRate"].ToString();
                    }
                }
            }
            catch
            {

                return;
            }

        }

        private void txtCurrencyRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }
    }
}
#endregion