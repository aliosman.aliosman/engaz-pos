﻿using ByStro.RPT;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ByStro.PL
{
    public partial class MaintenanceReturn_frm : Form
    {
        public MaintenanceReturn_frm()
        {
            InitializeComponent();
        }
        Maintenance_cls cls = new Maintenance_cls();
        private void MaintenanceRecived_frm_Load(object sender, EventArgs e)
        {
            ERB_Setting.SettingForm(this);
            btnNew_Click(null, null);
        }


    

        private void MaintenanceRecived_frm_KeyDown(object sender, KeyEventArgs e)
        {
            #region "KeyDown"
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    Close();
                }
                if (e.Control == true && e.KeyCode == Keys.N)
                {
                    btnNew_Click(null, null);
                }
                if (e.KeyCode == Keys.F2)
                {
                    if (btnSave.Enabled == true)
                    {
                        btnSave_Click(null, null);
                    }
                }
               
                if (e.KeyCode == Keys.Delete)
                {
                    if (btnDelete.Enabled == true)
                    {
                        btnDelete_Click(null, null);
                    }
                }
                if (e.KeyCode == Keys.F5)
                {
                    btnGetData_Click(null, null);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
     
                txtMaintenanceID.Text ="";
                txtDeviceTypeName.Text = "";
                txtDamagedTypeName.Text = "";
                D_ReturnData.Value = DateTime.Now;
                txtSerialNumber.Text = "";
                combDeviceStatus.Text = null;
                txtCustomerName.Text = "";
                txtTotalPrice.Text = "";
                txtRemark.Text = "";
             
              
                btnSave.Enabled = true;
                //btnUpdate.Enabled = false;
               // btnDelete.Enabled = false;
                txtSerialNumber.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtMaintenanceID.Text.Trim()))
                {
                    MessageBox.Show("يرجي تحديد  الجهاز", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                   
                    return;
                }

                if (combDeviceStatus.SelectedIndex < 0)
                {
                    MessageBox.Show("يرجي تحديد حالة الجهاز", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    combDeviceStatus.Focus();
                    return;
                }
                if (txtCustomerName.Text.Trim() == "")
                {
                    MessageBox.Show("يرجي ادخال اسم العميل ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtCustomerName.Focus();
                    return;
                }
                if (txtTotalPrice.Text.Trim() == "0")
                {
                    MessageBox.Show("يرجي تحديد المبلغ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtTotalPrice.Focus();
                    return;
                }

                int DeviceStatus = combDeviceStatus.SelectedIndex ;

                Double total = 0;

                if (!string.IsNullOrEmpty(txtTotalPrice.Text))
                {
                    total = Convert.ToDouble(txtTotalPrice.Text);
                }
               
                cls.Update_Maintenance(txtMaintenanceID.Text,D_ReturnData.Value, DeviceStatus, total, txtRemark.Text);


           
                
                
                MessageBox.Show("تم الحفظ بنجاح", "بنجاح", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnNew_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

     
        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtMaintenanceID.Text))
                {
                    MessageBox.Show("يرجي تحديد الجهاز الذي تريد حذفة", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }


                if (Mass.Delete() == true)
                {
                    cls.Delete_Maintenance(txtMaintenanceID.Text);

                    btnNew_Click(null, null);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
            try
            {
                MaintenanceReturnSearch_frm frm = new MaintenanceReturnSearch_frm();
                frm.ShowDialog(this);
                if (frm.LoadData == false)
                {
                    return;
                }

                DataTable dt = cls.Details_Maintenance(frm.DGV1.CurrentRow.Cells["MaintenanceID"].Value.ToString());
                DataRow Dr = dt.Rows[0];
                txtMaintenanceID.Text = Dr["MaintenanceID"].ToString();
                //D_RecivedData.Value = Convert.ToDateTime(Dr["RecivedData"].ToString());
                D_ReturnData.Value = Convert.ToDateTime(Dr["ReturnData"].ToString());
                txtSerialNumber.Text = Dr["SerialNumber"].ToString();

                txtDeviceTypeName.Text = Dr["DeviceTypeName"].ToString();
                txtDamagedTypeName.Text = Dr["DamagedTypeName"].ToString();


                combDeviceStatus.SelectedIndex = int.Parse(Dr["DeviceStatus"].ToString());


                txtCustomerName.Text = Dr["CustomerName"].ToString();
                txtTotalPrice.Text = Dr["TotalPrice"].ToString();
                txtRemark.Text = Dr["Remark"].ToString();

               // btnSave.Enabled = false;
                //btnUpdate.Enabled = true;
                //btnDelete.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }


        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtRest_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

        private void txtTotalPrice_TextChanged(object sender, EventArgs e)
        {
            //if (!string.IsNullOrEmpty(txtTotalPrice.Text))
            //{

            //    txttotal.Text = txtTotalPrice.Text;
            //}
        }

        private void txtPaid_TextChanged(object sender, EventArgs e)
        {
       
            }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                MaintenanceRecivedSearch_frm frm = new MaintenanceRecivedSearch_frm();
                frm.Text = "الاجهزة قيد الصيانة";
                frm.ShowDialog(this);
                if (frm.LoadData == false)
                {
                    return;
                }

                DataTable dt = cls.Details_Maintenance(frm.DGV1.CurrentRow.Cells["MaintenanceID"].Value.ToString());
                DataRow Dr = dt.Rows[0];
                txtMaintenanceID.Text = Dr["MaintenanceID"].ToString();
                D_RecivedData.Value = Convert.ToDateTime(Dr["RecivedData"].ToString());
                D_ReturnData.Value = Convert.ToDateTime(Dr["ReturnData"].ToString());
                txtSerialNumber.Text = Dr["SerialNumber"].ToString();

                txtDeviceTypeName.Text = Dr["DeviceTypeName"].ToString();
                txtDamagedTypeName.Text = Dr["DamagedTypeName"].ToString();


                combDeviceStatus.SelectedIndex = int.Parse(Dr["DeviceStatus"].ToString());


                txtCustomerName.Text = Dr["CustomerName"].ToString();
                txtTotalPrice.Text = Dr["TotalPrice"].ToString();
                txtRemark.Text = Dr["Remark"].ToString();


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

      

     
     
     
        private void txtPayValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataAccessLayer.UseNamberOnly(e);
        }

      

        private void AddDgv_PayType(DataGridView dgv, string PayTypeID, string PayTypeName, string PayValue, string Statement)
        {
            try
            {
                dgv.Rows.Add();
                dgv.ClearSelection();
                dgv.Rows[dgv.Rows.Count - 1].Selected = true;
                var lastRows = dgv.Rows.Count - 1;
                dgv.Rows[lastRows].Cells["PayTypeID"].Value = PayTypeID;
                dgv.Rows[lastRows].Cells["PayTypeName"].Value = PayTypeName;
                dgv.Rows[lastRows].Cells["PayValue"].Value = PayValue;
                dgv.Rows[lastRows].Cells["Statement"].Value = Statement;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


       
   
  



        string DeviceStatusName;
        //private void button3_Click(object sender, EventArgs e)
        //{
        //    btnSave_Click(null, null);

        //    DeviceStatusName = combDeviceStatus.Text;


        //    try
        //    {

        //        if (BillSetting_cls.PrintSize == 1)
        //        {
        //            PrintSizeA4();
        //        }
        //        else if (BillSetting_cls.PrintSize == 2)
        //        {
        //            PrintSize1_2A4();
        //        }
        //        else
        //        {
        //            PrintSize80MM();

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //}








        //private void PrintSizeA4()
        //{


        //    BillSetting_cls cls_print = new BillSetting_cls();

        //    DataTable dt_Print = cls.Details_Maintenance(txtMaintenanceID.Text);

        //    Reportes_Form frm = new Reportes_Form();

        //    var report = new RptMaintenance_A4();
        //    report.Database.Tables["RptMaintenance"].SetDataSource(dt_Print);
        //    report.SetParameterValue(0, this.Text);
        //    report.SetParameterValue(1, DeviceStatusName);

        //    frm.crystalReportViewer1.ReportSource = report;
        //    frm.crystalReportViewer1.Refresh();
        //    #region"Crystal Report | Printing | Default Printer"
        //    var dialog = new PrintDialog();
        //    report.PrintOptions.PrinterName = dialog.PrinterSettings.PrinterName;
        //    report.PrintToPrinter(1, false, 0, 0);
        //    #endregion

        //    // frm.ShowDialog();


        //}

        //private void PrintSize1_2A4()
        //{


        //    BillSetting_cls cls_print = new BillSetting_cls();

        //    DataTable dt_Print = cls.Details_Maintenance(txtMaintenanceID.Text);

        //    Reportes_Form frm = new Reportes_Form();

        //    var report = new RptMaintenance();
        //    report.Database.Tables["RptMaintenance"].SetDataSource(dt_Print);
        //    report.SetParameterValue(0, this.Text);
        //    report.SetParameterValue(1, DeviceStatusName);

        //    frm.crystalReportViewer1.ReportSource = report;
        //    frm.crystalReportViewer1.Refresh();
        //    #region"Crystal Report | Printing | Default Printer"
        //    var dialog = new PrintDialog();
        //    report.PrintOptions.PrinterName = dialog.PrinterSettings.PrinterName;
        //    report.PrintToPrinter(1, false, 0, 0);
        //    #endregion

        //    // frm.ShowDialog();


        //}


        //private void PrintSize80MM()
        //{


        //    BillSetting_cls cls_print = new BillSetting_cls();

        //    DataTable dt_Print = cls.Details_Maintenance(txtMaintenanceID.Text);

        //    Reportes_Form frm = new Reportes_Form();

        //    var report = new RptMaintenance80MM();
        //    report.Database.Tables["RptMaintenance"].SetDataSource(dt_Print);
        //    report.SetParameterValue(0, this.Text);
        //    report.SetParameterValue(1, DeviceStatusName);

        //    frm.crystalReportViewer1.ReportSource = report;
        //    frm.crystalReportViewer1.Refresh();
        //    #region"Crystal Report | Printing | Default Printer"
        //    var dialog = new PrintDialog();
        //    report.PrintOptions.PrinterName = dialog.PrinterSettings.PrinterName;
        //    report.PrintToPrinter(1, false, 0, 0);
        //    #endregion

        //    // frm.ShowDialog();


        //}

       
    }
}
