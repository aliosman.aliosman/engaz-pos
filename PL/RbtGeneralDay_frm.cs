﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing;
using ByStro.RPT;
using DevExpress.XtraGrid.Columns;
using System.Collections.Generic;
using DevExpress.XtraEditors;
using System.Linq;
using ByStro.Clases;

namespace ByStro.PL
{
    public partial class RbtGeneralDay_frm : XtraForm
    {
        public RbtGeneralDay_frm()
        {
            InitializeComponent();
        }
        TreasuryMovement_cls cls = new TreasuryMovement_cls();
      
        public Boolean searshFrm = false;
        private void RbtCustomer_frm_Load(object sender, EventArgs e)
        {
            this.Load += (_sender, _e) => { MasterClass.RestoreGridLayout(gridView1, this); };
            this.FormClosing += (_sender, _e) => { MasterClass.SaveGridLayout(gridView1, this); };

            FillUsers();
            LookUpEdit_PartID.Properties.PopupFormWidth = 300;
            gridLookUpEdit1View.Appearance.HeaderPanel.Options.UseTextOptions = true;
            gridLookUpEdit1View.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            gridLookUpEdit1View.Appearance.Row.Options.UseTextOptions = true;
            gridLookUpEdit1View.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            gridLookUpEdit1View.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            gridLookUpEdit1View.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            gridLookUpEdit1View.OptionsBehavior.AutoSelectAllInEditor = false;
            gridLookUpEdit1View.OptionsBehavior.AutoUpdateTotalSummary = false;
            gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            gridLookUpEdit1View.OptionsView.BestFitMaxRowCount = 10;
            gridLookUpEdit1View.OptionsView.ShowAutoFilterRow = true;
            gridLookUpEdit1View.OptionsView.ShowDetailButtons = false;
            gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            LookUpEdit_PartID.Properties.View.PopulateColumns(LookUpEdit_PartID.Properties.DataSource);
            gridLookUpEdit1View.Columns["ID"].Visible = false;
            gridLookUpEdit1View.Columns["EmpName"].Caption = "الاسم";
            
            datefrom.DateTime = DateTime.Now.Date;
            datetto.DateTime = DateTime.Now.Date;

            barButtonItem1.PerformClick();

        }

        public void FillUsers()
        {

            UserPermissions_CLS Store_cls = new UserPermissions_CLS();
            using (var db = new DAL.DBDataContext(Properties.Settings.Default.Connection_String))
            {
                LookUpEdit_PartID.Properties.DataSource =db.UserPermissions.Where(x=>x.Status == true  ).Select(x=>new { x.ID ,x.EmpName } ).ToList();
                var supins = new DAL.Supplier(); 
                LookUpEdit_PartID.Properties.DisplayMember = "EmpName";
                LookUpEdit_PartID.Properties.ValueMember = "ID";

            }

            //combUser.DataSource = Store_cls.Search_UserPermissions();
            //combUser.DisplayMember = "EmpName";
            //combUser.ValueMember = "ID"; 
        }
        DataTable dt = new DataTable();
        List<GridColumn> IntColumns()
        {


            GridColumn VoucherID = new GridColumn();
            GridColumn VoucherType = new GridColumn();
            GridColumn VoucherDate = new GridColumn();
            GridColumn Remark = new GridColumn();
            GridColumn Debit = new GridColumn();
            GridColumn Credit = new GridColumn();
            GridColumn EmpName = new GridColumn();


            VoucherID.FieldName = "VoucherID";
            VoucherID.Caption = "رقم السند";
            VoucherID.Name = "VoucherID";
            VoucherID.Width = 150;

            VoucherType.FieldName = "VoucherType";
            VoucherType.Caption = "نوع الحركة";
            VoucherType.Name = "VoucherType";
            VoucherType.Width = 150;

            VoucherDate.FieldName = "VoucherDate";
            VoucherDate.Caption = "تاريخ السند";
            VoucherDate.Name = "VoucherDate";
            VoucherDate.Width = 120;
            // 
            // Remark
            // 
            Remark.FieldName = "Description";
            Remark.Caption = "البيــــــــــان";
            Remark.Name = "Remark";
            Remark.Width = 250;
            // 
            // Debit
            // 
            Debit.FieldName = "Export";
            Debit.Caption = "مدين";
            Debit.Name = "Export";
            Debit.Width = 130;
            // 
            // Credit
            // 
            Credit.FieldName = "income";
            Credit.Caption = " دائن";
            Credit.Name = "income";
            Credit.Width = 130;
            // 
            // EmpName
            // 
            EmpName.FieldName = "EmpName";
            //dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            // EmpName.DefaultCellStyle = dataGridViewCellStyle9;
            EmpName.Caption = "المستخدم";
            EmpName.Name = "EmpName";
            EmpName.Width = 150;
            return new List<GridColumn>()
            { VoucherID, VoucherType, VoucherDate, Remark, Debit, Credit, EmpName };


        }
        public void btnView_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           
                if ((LookUpEdit_PartID.EditValue is int id && id > 0) == true )
                {
                   dt = cls.ReportSearch_TreasuryMovement(datefrom.DateTime, datetto.DateTime, LookUpEdit_PartID.EditValue.ToString());

                } 
                else
                {
                dt = cls.ReportSearch_TreasuryMovement(datefrom.DateTime, datetto.DateTime);

                }
                gridControl1.DataSource = dt;
                gridView1.Columns.Clear();
                int Index = 0;
                foreach (var item in IntColumns())
                {
                    gridView1.Columns.Add(item);
                    item.VisibleIndex = Index;
                    item.OptionsColumn.AllowEdit = false;
                }
                SumBalence();



        }





        private void SumBalence()
        {
            try
            {
                double Debit = 0;
                double Credit = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["income"].ToString() != "")
                    {
                        Debit += Convert.ToDouble(dt.Rows[i]["income"]);
                    }
                    if (dt.Rows[i]["Export"].ToString() != "")
                    {
                        Credit += Convert.ToDouble(dt.Rows[i]["Export"]);
                    }
                }
                txt_1.Caption  = Debit.ToString();
                txt_2.Caption  = Credit.ToString();
                txt_3.Caption  = (Debit - Credit).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }




        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gridView1.RowCount == 0)
                btnView_Click(null, null);
            else
                RPT.rpt_GridReport.Print(gridControl1, this.Text + "  " + LookUpEdit_PartID.Text,
                 " " + stlb1.Caption + " " + txt_1.Caption +
                 " " + stlb2.Caption + " " + txt_2.Caption +
                 " " + stlb3.Caption + " " + txt_3.Caption
                    , true);

        }

        
    }
}
