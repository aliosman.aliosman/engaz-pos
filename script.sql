
CREATE TABLE [dbo].[Bay_Details](
	[ID] [bigint] NOT NULL,
	[MainID] [bigint] NULL,
	[ProdecutID] [int] NULL,
	[Unit] [nvarchar](50) NULL,
	[UnitFactor] [float] NULL,
	[UnitOperating] [float] NULL,
	[Quantity] [float] NULL,
	[ReturnQuantity] [float] NULL,
	[Price] [float] NULL,
	[Vat] [float] NULL,
	[MainVat] [float] NULL,
	[ReturnVat] [float] NULL,
	[TotalPrice] [float] NULL,
	[ReturnTotalPrice] [float] NULL,
	[ReturnCurrencyVat] [float] NULL,
	[ReturnCurrencyTotal] [float] NULL,
	[StoreID] [int] NULL,
	[Recived] [bit] NULL,
	[CurrencyID] [int] NULL,
	[CurrencyRate] [float] NULL,
	[CurrencyPrice] [float] NULL,
	[CurrencyVat] [float] NULL,
	[CurrencyTotal] [float] NULL,
 CONSTRAINT [PK_Bill_Details] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Bay_Main]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bay_Main](
	[MainID] [bigint] NOT NULL,
	[MyDate] [date] NULL,
	[InvoiceNumber] [nvarchar](50) NULL,
	[SupplierID] [int] NULL,
	[Remarks] [ntext] NULL,
	[TypeKind] [nvarchar](50) NULL,
	[TotalInvoice] [float] NULL,
	[Discount] [float] NULL,
	[NetInvoice] [float] NULL,
	[PaidInvoice] [float] NULL,
	[RestInvoise] [float] NULL,
	[ReturnInvoise] [float] NULL,
	[PaidReturnInvoice] [float] NULL,
	[TransID] [nvarchar](50) NULL,
	[TreasuryID] [nvarchar](50) NULL,
	[Vat] [float] NOT NULL CONSTRAINT [DF__Bay_Main__Vat__47A6A41B]  DEFAULT ((0)),
	[VatValue] [float] NOT NULL CONSTRAINT [DF__Bay_Main__VatVal__489AC854]  DEFAULT ((0)),
	[Expenses] [float] NULL,
	[UserAdd] [int] NULL,
 CONSTRAINT [PK_Bill_Bay] PRIMARY KEY CLUSTERED 
(
	[MainID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Capital]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Capital](
	[Id] [int] NOT NULL,
	[Notes] [nvarchar](255) NULL,
	[Debit] [float] NULL,
	[Credit] [float] NULL,
 CONSTRAINT [PK_Capital] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cash_Export]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cash_Export](
	[ExportID] [bigint] NOT NULL,
	[MayDate] [date] NULL,
	[Remarks] [ntext] NULL,
	[Value] [float] NULL,
	[TreasuryID] [nvarchar](50) NULL,
	[UserAdd] [int] NULL,
 CONSTRAINT [PK_Cash_Export_1] PRIMARY KEY CLUSTERED 
(
	[ExportID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Cash_Import]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cash_Import](
	[ImportID] [bigint] NOT NULL,
	[MayDate] [date] NULL,
	[Remarks] [ntext] NULL,
	[Value] [float] NULL,
	[TreasuryID] [nvarchar](50) NULL,
	[UserAdd] [int] NULL,
 CONSTRAINT [PK_Cash_Import] PRIMARY KEY CLUSTERED 
(
	[ImportID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Category]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[CategoryID] [int] NOT NULL,
	[CategoryName] [nvarchar](50) NOT NULL,
	[Remark] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ChangeQuantity_Details]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChangeQuantity_Details](
	[ID] [bigint] NOT NULL,
	[MainID] [bigint] NOT NULL,
	[ProdecutID] [int] NULL,
	[Unit] [nvarchar](50) NULL,
	[UnitFactor] [float] NULL,
	[UnitOperating] [float] NULL,
	[Quantity] [float] NULL,
	[Price] [float] NULL,
	[TotalPrice] [float] NULL,
	[StoreID] [int] NULL,
	[SumType] [bit] NULL,
	[Recived] [bit] NULL,
 CONSTRAINT [PK_ChangeQuantity_Details] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ChangeQuantity_Main]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChangeQuantity_Main](
	[MainID] [bigint] NOT NULL,
	[MyDate] [date] NULL,
	[Remarks] [ntext] NULL,
	[UserAdd] [int] NULL,
 CONSTRAINT [PK_ChangeQuentity_Main] PRIMARY KEY CLUSTERED 
(
	[MainID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ChangeStore_Details]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChangeStore_Details](
	[ID] [bigint] NOT NULL,
	[MainID] [bigint] NOT NULL,
	[ProdecutID] [int] NULL,
	[Unit] [nvarchar](50) NULL,
	[UnitFactor] [float] NULL,
	[UnitOperating] [float] NULL,
	[Quantity] [float] NULL,
	[Price] [float] NULL,
	[TotalPrice] [float] NULL,
	[StoreID] [int] NULL,
	[SumType] [bit] NULL,
	[Recived] [bit] NULL,
 CONSTRAINT [PK_ChangeStore_Details] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ChangeStore_Main]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChangeStore_Main](
	[MainID] [bigint] NOT NULL,
	[MyDate] [date] NULL,
	[Remarks] [ntext] NULL,
	[StoreID] [int] NULL,
	[StoreID2] [int] NULL,
	[UserAdd] [int] NULL,
 CONSTRAINT [PK_ChangeStore_Main] PRIMARY KEY CLUSTERED 
(
	[MainID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CombanyData]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CombanyData](
	[ID] [int] NOT NULL,
	[CombanyName] [nvarchar](250) NULL,
	[CombanyDescriptone] [nvarchar](550) NULL,
	[MyName] [nvarchar](250) NULL,
	[PhoneNamber] [nvarchar](50) NULL,
	[Imge] [image] NULL,
	[Title] [nvarchar](350) NULL,
	[BillRemark] [ntext] NULL,
	[MaintenanceRemark] [ntext] NULL,
 CONSTRAINT [PK_Des_Cription] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Currency]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currency](
	[CurrencyID] [int] NOT NULL,
	[CurrencyName] [nvarchar](255) NOT NULL,
	[CurrencyShortcut] [nvarchar](255) NULL,
	[CurrencyPartName] [nvarchar](255) NULL,
	[CurrencyRate] [float] NOT NULL,
	[Remark] [nvarchar](150) NOT NULL CONSTRAINT [DF_Currency_Partity]  DEFAULT ((100)),
 CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED 
(
	[CurrencyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customers]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[CustomerID] [int] NOT NULL,
	[CustomerName] [nvarchar](60) NULL,
	[SalesLavel] [nvarchar](6) NULL,
	[NationalID] [nvarchar](50) NULL,
	[Address] [nvarchar](60) NULL,
	[TaxFileNumber] [nvarchar](50) NULL,
	[RegistrationNo] [nvarchar](50) NULL,
	[StartContract] [nvarchar](20) NULL,
	[EndContract] [nvarchar](20) NULL,
	[Remarks] [ntext] NULL,
	[Phone] [nvarchar](20) NULL,
	[Phone2] [nvarchar](20) NULL,
	[AccountNumber] [nvarchar](50) NULL,
	[Fax] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[WebSite] [nvarchar](50) NULL,
	[Maximum] [nvarchar](10) NULL,
	[CreditLimit] [smallint] NULL,
	[TransID] [bigint] NULL,
	[ISCusSupp] [bit] NULL,
	[SupplierID] [nvarchar](50) NULL,
	[Income] [float] NULL,
	[Export] [float] NULL,
	[UserAdd] [int] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customers_Pay]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers_Pay](
	[PayID] [bigint] NOT NULL,
	[PayDate] [date] NULL,
	[PayValue] [float] NULL CONSTRAINT [DF_Customers_Pay_PaymentValue]  DEFAULT ((0.0)),
	[Remarks] [ntext] NULL,
	[CustomerID] [int] NULL,
	[TreasuryID] [nvarchar](50) NULL,
	[UserAdd] [int] NULL,
 CONSTRAINT [PK_Customers_Pay] PRIMARY KEY CLUSTERED 
(
	[PayID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customers_Recived]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers_Recived](
	[PayID] [bigint] NOT NULL,
	[PayDate] [date] NULL,
	[PayValue] [float] NULL CONSTRAINT [DF_Customers_Recived_PayValue]  DEFAULT ((0.0)),
	[Remarks] [ntext] NULL,
	[CustomerID] [int] NULL,
	[TreasuryID] [nvarchar](50) NULL,
	[UserAdd] [int] NULL,
 CONSTRAINT [PK_Customers_Recived_1] PRIMARY KEY CLUSTERED 
(
	[PayID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DamagedType]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DamagedType](
	[DamagedTypeID] [int] NOT NULL,
	[DamagedTypeName] [nvarchar](150) NULL,
	[Remark] [ntext] NULL,
 CONSTRAINT [PK_HolidaysType] PRIMARY KEY CLUSTERED 
(
	[DamagedTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DeviceType]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeviceType](
	[DeviceTypeID] [int] NOT NULL,
	[DeviceTypeName] [nvarchar](150) NULL,
	[CompenyID] [int] NULL,
	[Remark] [ntext] NULL,
 CONSTRAINT [PK_DeviceType] PRIMARY KEY CLUSTERED 
(
	[DeviceTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employee_Additions]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee_Additions](
	[Additions_ID] [int] NOT NULL,
	[MyDate] [date] NULL,
	[EmployeeID] [int] NULL,
	[AdditionsType_ID] [int] NULL,
	[Addition_Value] [float] NULL,
	[Remarks] [ntext] NULL,
	[UserAdd] [int] NULL,
 CONSTRAINT [PK_Employee_Additions] PRIMARY KEY CLUSTERED 
(
	[Additions_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employee_AdditionsType]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee_AdditionsType](
	[AdditionsType_ID] [int] NOT NULL,
	[AdditionsType_Name] [nvarchar](100) NULL,
	[Remarks] [ntext] NULL,
	[UserAdd] [int] NULL,
 CONSTRAINT [PK_Employee_AdditionsType] PRIMARY KEY CLUSTERED 
(
	[AdditionsType_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employee_Attendance]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee_Attendance](
	[AttendanceID] [int] NOT NULL,
	[EmployeeID] [int] NULL,
	[MyDate] [date] NULL,
	[Hower_Come] [datetime] NULL,
	[Hower_Go] [datetime] NULL,
	[Hower_Work] [nvarchar](50) NULL,
	[States] [bit] NULL,
 CONSTRAINT [PK_Employee_Attendance] PRIMARY KEY CLUSTERED 
(
	[AttendanceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employee_Deduction]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee_Deduction](
	[Deduction_ID] [int] NOT NULL,
	[MyDate] [date] NULL,
	[EmployeeID] [int] NULL,
	[DeductionType_ID] [int] NULL,
	[Deduction_Value] [float] NULL,
	[TreasuryID] [nvarchar](50) NULL,
	[Remarks] [ntext] NULL,
	[UserAdd] [int] NULL,
 CONSTRAINT [PK_Employee_Deduction] PRIMARY KEY CLUSTERED 
(
	[Deduction_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employee_DeductionType]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee_DeductionType](
	[DeductionType_ID] [int] NOT NULL,
	[DeductionType_Name] [nvarchar](100) NULL,
	[Remarks] [ntext] NULL,
	[UserAdd] [int] NULL,
 CONSTRAINT [PK_Employee_DeductionType] PRIMARY KEY CLUSTERED 
(
	[DeductionType_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employee_Holiday]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee_Holiday](
	[ID] [int] NOT NULL,
	[MyDate] [date] NULL,
	[EmployeeID] [int] NULL,
	[HolidayID] [int] NULL,
	[DayNumber] [smallint] NULL,
	[Remarks] [ntext] NULL,
	[UserAdd] [int] NULL,
 CONSTRAINT [PK_Employee_Holiday] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employee_Salary]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee_Salary](
	[SalaryID] [int] NOT NULL,
	[MyDate] [date] NULL,
	[EmployeeID] [int] NULL,
	[MainSalary] [float] NULL,
	[Additions] [float] NULL,
	[Deduction] [float] NULL,
	[NetSalary] [float] NULL,
	[TreasuryID] [nvarchar](50) NULL,
	[UserAdd] [int] NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[SalaryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employees]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employees](
	[EmployeeID] [int] NOT NULL,
	[EmployeeName] [nvarchar](100) NULL,
	[EducationalQualification] [nvarchar](50) NULL,
	[Type] [bit] NULL,
	[Adress] [ntext] NULL,
	[SocialStatus] [nvarchar](50) NULL,
	[StartContract] [nvarchar](20) NULL,
	[EndContract] [nvarchar](20) NULL,
	[NationalID] [nvarchar](50) NULL,
	[Remarks] [ntext] NULL,
	[Phone] [nvarchar](20) NULL,
	[Phone2] [nvarchar](20) NULL,
	[AccountNumber] [nvarchar](50) NULL,
	[Fax] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[WebSite] [nvarchar](50) NULL,
	[MilitaryService] [nvarchar](50) NULL,
	[PlaceOfService] [nvarchar](50) NULL,
	[ServiceStartDate] [nvarchar](50) NULL,
	[ServiceEndDate] [nvarchar](50) NULL,
	[WorkPartCompanyID] [int] NULL,
	[PartCompanyID] [int] NULL,
	[NumberMonth] [smallint] NULL,
	[StartWork] [date] NULL,
	[TimeAttendance] [nvarchar](50) NULL,
	[TimeToLeave] [nvarchar](50) NULL,
	[WorkingHours] [int] NULL,
	[Salary] [float] NULL,
 CONSTRAINT [PK_Employees] PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Expenses]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Expenses](
	[ExpenseID] [bigint] NOT NULL,
	[ExpenseTypeID] [int] NULL,
	[MyDate] [date] NULL,
	[RecivedName] [nvarchar](50) NULL,
	[ExpenseValue] [float] NULL,
	[Remarks] [ntext] NULL,
	[TreasuryID] [nvarchar](50) NULL,
	[UserAdd] [int] NULL,
 CONSTRAINT [PK_Expenses_1] PRIMARY KEY CLUSTERED 
(
	[ExpenseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ExpenseType]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExpenseType](
	[ExpenseID] [int] NOT NULL,
	[ExpenseName] [nvarchar](50) NULL,
	[Remarks] [ntext] NULL,
 CONSTRAINT [PK_ExpenseType] PRIMARY KEY CLUSTERED 
(
	[ExpenseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Firist_Details]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Firist_Details](
	[ID] [bigint] NOT NULL,
	[MainID] [bigint] NOT NULL,
	[ProdecutID] [int] NULL,
	[Unit] [nvarchar](50) NULL,
	[UnitFactor] [float] NULL,
	[UnitOperating] [float] NULL,
	[Quantity] [float] NULL,
	[Price] [float] NULL,
	[TotalPrice] [float] NULL,
	[StoreID] [int] NULL,
	[Recived] [bit] NULL,
 CONSTRAINT [PK_ProdecutsFirist_Details] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Firist_Main]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Firist_Main](
	[MainID] [bigint] NOT NULL,
	[MyDate] [date] NULL,
	[Remarks] [ntext] NULL,
	[TotalInvoice] [float] NULL,
	[UserAdd] [int] NULL,
 CONSTRAINT [PK_ProdecutsFirist_Main] PRIMARY KEY CLUSTERED 
(
	[MainID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HolidayType]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HolidayType](
	[HolidayID] [int] NOT NULL,
	[HolidayName] [nvarchar](100) NULL,
	[DayNumber] [smallint] NULL,
	[Note] [ntext] NULL,
 CONSTRAINT [PK_HolidayType] PRIMARY KEY CLUSTERED 
(
	[HolidayID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Maintenance]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Maintenance](
	[MaintenanceID] [int] NOT NULL,
	[RecivedData] [date] NULL,
	[ReturnData] [date] NULL,
	[SerialNumber] [nvarchar](50) NULL,
	[DeviceTypeID] [int] NULL,
	[DamagedTypeID] [int] NULL,
	[DeviceStatus] [smallint] NULL,
	[CustomerID] [int] NULL,
	[TotalPrice] [float] NULL,
	[Remark] [ntext] NULL,
	[UserID] [int] NULL,
 CONSTRAINT [PK_Maintenance] PRIMARY KEY CLUSTERED 
(
	[MaintenanceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Maintenance_Details]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Maintenance_Details](
	[ID] [int] NOT NULL,
	[MainID] [bigint] NULL,
	[DamagedTypeID] [int] NULL,
	[Quantity] [float] NULL,
	[Price] [float] NULL,
	[TotalPrice] [float] NULL,
	[CurrencyID] [int] NULL,
	[CurrencyRate] [float] NULL,
	[CurrencyPrice] [float] NULL,
	[CurrencyTotal] [float] NULL,
 CONSTRAINT [PK_aaa] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Maintenance_Main]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Maintenance_Main](
	[MainID] [bigint] NOT NULL,
	[MyDate] [date] NULL,
	[CustomerID] [int] NULL,
	[Remarks] [ntext] NULL,
	[TypeKind] [nvarchar](50) NULL,
	[TotalInvoice] [float] NULL,
	[Discount] [float] NULL,
	[NetInvoice] [float] NULL,
	[PaidInvoice] [float] NULL,
	[RestInvoise] [float] NULL,
	[TreasuryID] [nvarchar](50) NULL,
	[UserAdd] [int] NULL,
 CONSTRAINT [PK_Maintenance_Main] PRIMARY KEY CLUSTERED 
(
	[MainID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MaintenanceCompeny]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MaintenanceCompeny](
	[CompenyID] [int] NOT NULL,
	[CompenyName] [nvarchar](250) NULL,
	[Remark] [ntext] NULL,
 CONSTRAINT [PK_MaintenanceCompeny] PRIMARY KEY CLUSTERED 
(
	[CompenyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MoneyTransfer]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MoneyTransfer](
	[TransferID] [int] NOT NULL,
	[MyDate] [date] NOT NULL,
	[AccountID_From] [int] NOT NULL,
	[AccountID_To] [int] NOT NULL,
	[Amount] [float] NOT NULL,
	[CurrencyID] [int] NOT NULL,
	[CurrencyRate] [float] NOT NULL,
	[Note] [nvarchar](max) NULL,
 CONSTRAINT [PK_MoneyTransfer] PRIMARY KEY CLUSTERED 
(
	[TransferID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OfferPrice_Detalis]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OfferPrice_Detalis](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[MainID] [bigint] NULL,
	[ProdecutID] [int] NULL,
	[Unit] [nvarchar](50) NULL,
	[UnitFactor] [float] NULL,
	[UnitOperating] [float] NULL,
	[Quantity] [float] NULL,
	[Price] [float] NULL,
	[Vat] [float] NULL,
	[MainVat] [float] NULL,
	[TotalPrice] [float] NULL,
	[StoreID] [int] NULL,
	[Recived] [bit] NULL,
 CONSTRAINT [PK_OfferPrice_Detalis] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OfferPrice_Main]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OfferPrice_Main](
	[MainID] [bigint] NOT NULL,
	[MyDate] [date] NOT NULL,
	[Remarks] [nvarchar](max) NULL,
	[TotalInvoice] [float] NOT NULL,
	[Discount] [float] NOT NULL,
	[NetInvoice] [float] NOT NULL,
	[Vat] [float] NOT NULL CONSTRAINT [DF_OfferPrice_Main_Vat]  DEFAULT ((0)),
	[VatValue] [float] NOT NULL CONSTRAINT [DF_OfferPrice_Main_VatValue]  DEFAULT ((0)),
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_OfferPrice_Main] PRIMARY KEY CLUSTERED 
(
	[MainID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PartCompany]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PartCompany](
	[PartCompanyID] [int] NOT NULL,
	[PartCompanyName] [nvarchar](100) NOT NULL,
	[Remarks] [ntext] NULL,
 CONSTRAINT [PK_PartCompany] PRIMARY KEY CLUSTERED 
(
	[PartCompanyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PayType]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayType](
	[PayTypeID] [int] NOT NULL,
	[PayTypeName] [nvarchar](150) NULL,
	[AccountNumber] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[Phone] [nvarchar](50) NULL,
	[Fax] [nvarchar](50) NULL,
	[Remark] [ntext] NULL,
 CONSTRAINT [PK_PayType] PRIMARY KEY CLUSTERED 
(
	[PayTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PayType_trans]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayType_trans](
	[PayID] [int] NOT NULL,
	[MainID] [bigint] NOT NULL,
	[VoucherDate] [date] NULL,
	[PayTypeID] [int] NOT NULL,
	[VoucherCode] [nvarchar](50) NULL,
	[VoucherType] [nvarchar](50) NOT NULL,
	[TypeID] [nvarchar](50) NOT NULL,
	[Debit] [float] NOT NULL,
	[Credit] [float] NOT NULL,
	[Statement] [ntext] NOT NULL,
	[UserID] [int] NOT NULL,
	[CurrencyID] [int] NULL,
	[CurrencyRate] [float] NULL,
	[CurrencyPrice] [float] NULL,
 CONSTRAINT [PK_Sales_Pay_1] PRIMARY KEY CLUSTERED 
(
	[PayID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Prodecuts]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Prodecuts](
	[ProdecutID] [int] NOT NULL,
	[ProdecutName] [nvarchar](60) NOT NULL,
	[CategoryID] [int] NULL,
	[ProdecutLocation] [nvarchar](50) NULL,
	[Company] [nvarchar](50) NULL,
	[LastUpdate] [datetime] NULL,
	[RequestLimit] [float] NULL,
	[FiestUnit] [nvarchar](50) NULL,
	[FiestUnitOperating] [nvarchar](10) NULL,
	[FiestUnitFactor] [nvarchar](10) NULL,
	[FiestUnitBarcode] [nvarchar](50) NULL,
	[FiestUnitPrice1] [float] NULL,
	[FiestUnitPrice2] [float] NULL,
	[FiestUnitPrice3] [float] NULL,
	[SecoundUnit] [nvarchar](50) NULL,
	[SecoundUnitOperating] [nvarchar](10) NULL,
	[SecoundUnitFactor] [nvarchar](10) NULL,
	[SecoundUnitBarcode] [nvarchar](50) NULL,
	[SecoundUnitPrice1] [float] NULL,
	[SecoundUnitPrice2] [float] NULL,
	[SecoundUnitPrice3] [float] NULL,
	[ThreeUnit] [nvarchar](50) NULL,
	[ThreeUnitOperating] [nvarchar](50) NULL,
	[ThreeUnitFactor] [nvarchar](10) NULL,
	[ThreeUnitBarcode] [nvarchar](50) NULL,
	[ThreeUnitPrice1] [float] NULL,
	[ThreeUnitPrice2] [float] NULL,
	[ThreeUnitPrice3] [float] NULL,
	[ProdecutBayPrice] [float] NULL,
	[ProdecutAvg] [float] NULL,
	[DiscoundBay] [float] NULL,
	[DiscoundSale] [float] NULL,
	[UnitDefoult] [int] NULL,
	[ProductService] [bit] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_Prodecuts] PRIMARY KEY CLUSTERED 
(
	[ProdecutID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PurchaseOrder_Details]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PurchaseOrder_Details](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[MainID] [bigint] NULL,
	[ProdecutID] [int] NULL,
	[Unit] [nvarchar](50) NULL,
	[UnitFactor] [float] NULL,
	[UnitOperating] [float] NULL,
	[Quantity] [float] NULL,
	[Price] [float] NULL,
	[Vat] [float] NULL,
	[MainVat] [float] NULL,
	[TotalPrice] [float] NULL,
	[StoreID] [int] NULL,
	[Recived] [bit] NULL,
 CONSTRAINT [PK_PurchaseOrder_Details] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PurchaseOrder_Main]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PurchaseOrder_Main](
	[MainID] [bigint] NOT NULL,
	[MyDate] [date] NOT NULL,
	[Remarks] [nvarchar](max) NULL,
	[TotalInvoice] [float] NOT NULL,
	[Discount] [float] NOT NULL,
	[NetInvoice] [float] NOT NULL,
	[Vat] [float] NOT NULL CONSTRAINT [DF_PurchaseOrder_Main_Vat]  DEFAULT ((0)),
	[VatValue] [float] NOT NULL CONSTRAINT [DF_PurchaseOrder_Main_VatValue]  DEFAULT ((0)),
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_PurchaseOrder_Main] PRIMARY KEY CLUSTERED 
(
	[MainID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sales_Details]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sales_Details](
	[ID] [bigint] NOT NULL,
	[MainID] [bigint] NOT NULL,
	[ProdecutID] [int] NULL,
	[Unit] [nvarchar](50) NULL,
	[UnitFactor] [float] NULL,
	[UnitOperating] [float] NULL,
	[Quantity] [float] NULL,
	[ReturnQuantity] [float] NULL,
	[BayPrice] [float] NULL,
	[TotalBayPrice] [float] NULL,
	[Price] [float] NULL,
	[Vat] [float] NULL,
	[MainVat] [float] NULL,
	[ReturnVat] [float] NULL,
	[TotalPrice] [float] NULL,
	[ReturnTotalPrice] [float] NULL,
	[ReturnCurrencyVat] [float] NULL,
	[ReturnCurrencyTotal] [float] NULL,
	[StoreID] [int] NULL,
	[Recived] [bit] NULL,
	[CurrencyID] [int] NULL,
	[CurrencyRate] [float] NULL,
	[CurrencyPrice] [float] NULL,
	[CurrencyVat] [float] NULL,
	[CurrencyTotal] [float] NULL,
 CONSTRAINT [PK_Sales_Details_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sales_Main]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sales_Main](
	[MainID] [bigint] NOT NULL,
	[MyDate] [date] NULL,
	[CustomerID] [int] NULL,
	[Note] [nvarchar](50) NULL,
	[TypeKind] [nvarchar](50) NULL,
	[TotalInvoice] [float] NULL,
	[Discount] [float] NULL,
	[NetInvoice] [float] NULL,
	[PaidInvoice] [float] NULL,
	[RestInvoise] [float] NULL,
	[ReturnInvoise] [float] NULL,
	[PaidReturnInvoice] [float] NULL,
	[TotalBayPrice] [float] NULL,
	[ReturnBayPrice] [float] NULL,
	[TotalProfits] [float] NULL,
	[TransID] [nvarchar](50) NULL,
	[TreasuryID] [nvarchar](50) NULL,
	[Vat] [float] NULL,
	[VatValue] [float] NULL,
	[Extra] [float] NULL,
	[UserAdd] [int] NULL,
 CONSTRAINT [PK_BillMain_1] PRIMARY KEY CLUSTERED 
(
	[MainID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Setting]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Setting](
	[SettingID] [int] NOT NULL,
	[UseStoreDefault] [bit] NULL,
	[StoreID] [nvarchar](50) NULL,
	[UseCustomerDefault] [bit] NULL,
	[CustomerID] [nvarchar](50) NULL,
	[UsingFastInput] [bit] NULL,
	[ShowMessageQty] [bit] NULL,
	[ShowMessageSave] [bit] NULL,
	[kindPay] [nvarchar](50) NULL,
	[UseVat] [bit] NULL,
	[Vat] [float] NULL,
	[PrintSize] [int] NULL,
	[UseCrrencyDefault] [bit] NULL,
	[CurrencyID] [nvarchar](50) NULL,
	[NotificationProdect] [bit] NULL,
	[NotificationCustomers] [bit] NULL,
	[MaxBalance] [nvarchar](50) NULL,
	[UsekindPay] [bit] NULL,
	[PayTypeID] [nvarchar](50) NULL,
	[UseUnit] [bit] NULL,
	[UnitId] [nvarchar](50) NULL,
	[UseCategory] [bit] NULL,
	[CategoryId] [nvarchar](50) NULL,
 CONSTRAINT [PK_Setting] PRIMARY KEY CLUSTERED 
(
	[SettingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SmsSetting]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SmsSetting](
	[ID] [int] NOT NULL,
	[SentName] [nvarchar](255) NULL,
	[Password] [nvarchar](50) NULL,
	[Phone] [nvarchar](50) NULL,
 CONSTRAINT [PK_SmsSetting] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Store_Prodecut]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Store_Prodecut](
	[ID] [bigint] NOT NULL,
	[ProdecutID] [int] NULL,
	[StoreID] [int] NULL,
	[Balence] [float] NULL,
	[Price] [float] NULL,
	[ProdecutAvg] [float] NULL,
 CONSTRAINT [PK_Store_Prodecut] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Stores]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Stores](
	[StoreID] [int] NOT NULL,
	[StoreName] [nvarchar](50) NOT NULL,
	[Responsible] [nvarchar](50) NULL,
	[phone] [nvarchar](30) NULL,
	[phone2] [nvarchar](30) NULL,
	[Place] [nvarchar](100) NULL,
	[Note] [nvarchar](50) NULL,
 CONSTRAINT [PK_Stores] PRIMARY KEY CLUSTERED 
(
	[StoreID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[StoreUsers]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StoreUsers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StoreID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_StoreUsers] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Supplier_Pay]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Supplier_Pay](
	[PayID] [bigint] NOT NULL,
	[PayDate] [date] NULL,
	[PayValue] [float] NULL CONSTRAINT [DF_Supplier_Pay_PayValue_1]  DEFAULT ((0.0)),
	[Remarks] [ntext] NULL,
	[SupplierID] [int] NULL,
	[TreasuryID] [nvarchar](50) NULL,
	[UserAdd] [int] NULL,
 CONSTRAINT [PK_Supplier_Pay_1] PRIMARY KEY CLUSTERED 
(
	[PayID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Supplier_Recived]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Supplier_Recived](
	[PayID] [bigint] NOT NULL,
	[PayDate] [date] NULL,
	[PayValue] [float] NULL CONSTRAINT [DF_Supplier_Recived_PayValue]  DEFAULT ((0.0)),
	[Remarks] [ntext] NULL,
	[SupplierID] [int] NULL,
	[TreasuryID] [nvarchar](50) NULL,
	[UserAdd] [int] NULL,
 CONSTRAINT [PK_Supplier_Recived] PRIMARY KEY CLUSTERED 
(
	[PayID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Suppliers]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Suppliers](
	[SupplierID] [int] NOT NULL,
	[SupplierName] [nvarchar](60) NULL,
	[NationalID] [nvarchar](50) NULL,
	[Address] [nvarchar](60) NULL,
	[TaxFileNumber] [nvarchar](50) NULL,
	[RegistrationNo] [nvarchar](50) NULL,
	[StartContract] [nvarchar](20) NULL,
	[EndContract] [nvarchar](20) NULL,
	[Remarks] [ntext] NULL,
	[Phone] [nvarchar](20) NULL,
	[Phone2] [nvarchar](20) NULL,
	[AccountNumber] [nvarchar](50) NULL,
	[Fax] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[WebSite] [nvarchar](50) NULL,
	[Maximum] [nvarchar](10) NULL,
	[CreditLimit] [smallint] NULL,
	[UseVat] [bit] NULL,
	[Income] [float] NULL,
	[Export] [float] NULL,
	[UserAdd] [int] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_Suppliers] PRIMARY KEY CLUSTERED 
(
	[SupplierID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TreasuryMovement]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TreasuryMovement](
	[ID] [bigint] NOT NULL,
	[AccountID] [nvarchar](50) NULL,
	[ISCusSupp] [nvarchar](50) NULL,
	[VoucherID] [nvarchar](50) NOT NULL,
	[Type] [nvarchar](50) NULL,
	[VoucherType] [nvarchar](50) NULL,
	[VoucherDate] [date] NULL,
	[Description] [ntext] NULL,
	[Income] [float] NULL,
	[Export] [float] NULL,
	[Remark] [nvarchar](max) NULL,
	[Debit] [float] NULL,
	[Credit] [float] NULL,
	[UserAdd] [int] NULL,
	[ISShow] [bit] NULL,
 CONSTRAINT [PK_TreasuryMovement] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Unit]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Unit](
	[UnitID] [int] NOT NULL,
	[UnitName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Unit] PRIMARY KEY CLUSTERED 
(
	[UnitID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserPermissions]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserPermissions](
	[ID] [int] NOT NULL,
	[EmpName] [nvarchar](100) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[UserPassword] [nvarchar](50) NOT NULL,
	[TypeUser] [bit] NOT NULL,
	[Status] [bit] NOT NULL,
	[CS1] [bit] NOT NULL,
	[CS2] [bit] NOT NULL,
	[CS3] [bit] NOT NULL,
	[CS4] [bit] NOT NULL,
	[CS5] [bit] NOT NULL,
	[CS6] [bit] NOT NULL,
	[CS7] [bit] NOT NULL,
	[CS8] [bit] NOT NULL,
	[CS9] [bit] NOT NULL,
	[CS10] [bit] NOT NULL,
	[CS11] [bit] NOT NULL,
	[CS12] [bit] NOT NULL,
	[CS13] [bit] NOT NULL,
	[CS14] [bit] NOT NULL,
	[CS15] [bit] NOT NULL,
	[CS16] [bit] NOT NULL,
	[CS17] [bit] NOT NULL,
	[CS18] [bit] NOT NULL,
	[CS19] [bit] NOT NULL,
	[CS20] [bit] NOT NULL,
	[CS21] [bit] NOT NULL,
	[CS22] [bit] NOT NULL,
	[CS23] [bit] NOT NULL,
	[CS24] [bit] NOT NULL,
	[CS25] [bit] NOT NULL,
	[CS26] [bit] NOT NULL,
	[CS27] [bit] NOT NULL,
	[CS28] [bit] NOT NULL,
	[CS29] [bit] NOT NULL,
	[CS30] [bit] NOT NULL,
	[CS31] [bit] NOT NULL,
	[CS32] [bit] NOT NULL,
	[CS33] [bit] NOT NULL,
	[CS34] [bit] NOT NULL,
	[CS35] [bit] NOT NULL,
	[CS36] [bit] NOT NULL,
	[CS37] [bit] NOT NULL,
	[CS38] [bit] NOT NULL,
	[CS39] [bit] NOT NULL,
	[CS40] [bit] NOT NULL,
	[CS41] [bit] NOT NULL,
	[CS42] [bit] NOT NULL,
	[CS43] [bit] NOT NULL,
	[CS44] [bit] NOT NULL,
	[CS45] [bit] NOT NULL,
	[CS46] [bit] NOT NULL,
	[CS47] [bit] NOT NULL,
	[CS48] [bit] NOT NULL,
	[CS49] [bit] NOT NULL,
	[CS50] [bit] NOT NULL,
	[CS51] [bit] NOT NULL,
	[CS52] [bit] NOT NULL,
	[CS53] [bit] NOT NULL,
	[CS54] [bit] NOT NULL,
	[CS55] [bit] NOT NULL,
	[CS56] [bit] NOT NULL,
	[CS57] [bit] NOT NULL,
	[CS58] [bit] NOT NULL,
	[CS59] [bit] NOT NULL,
	[CS60] [bit] NOT NULL,
	[CS61] [bit] NOT NULL,
	[CS62] [bit] NOT NULL,
	[CS63] [bit] NOT NULL,
	[CS64] [bit] NOT NULL,
	[CS65] [bit] NOT NULL,
	[CS66] [bit] NOT NULL,
	[CS67] [bit] NOT NULL,
	[CS68] [bit] NOT NULL,
	[CS69] [bit] NOT NULL,
	[CS70] [bit] NOT NULL,
	[CS71] [bit] NOT NULL,
	[CS72] [bit] NOT NULL,
	[CS73] [bit] NOT NULL,
	[CS74] [bit] NOT NULL CONSTRAINT [DF__UserPermis__CS74__2739D489]  DEFAULT ((1)),
	[CS75] [bit] NOT NULL CONSTRAINT [DF_UserPermissions_CS741]  DEFAULT ((1)),
	[CS76] [bit] NOT NULL CONSTRAINT [DF_UserPermissions_CS751]  DEFAULT ((1)),
	[CS77] [bit] NOT NULL CONSTRAINT [DF_UserPermissions_CS752]  DEFAULT ((1)),
	[CS78] [bit] NOT NULL CONSTRAINT [DF_UserPermissions_CS753]  DEFAULT ((1)),
	[CS79] [bit] NOT NULL CONSTRAINT [DF_UserPermissions_CS754]  DEFAULT ((1)),
	[CS80] [bit] NOT NULL CONSTRAINT [DF_UserPermissions_CS755]  DEFAULT ((1)),
	[CS81] [bit] NOT NULL CONSTRAINT [DF_UserPermissions_CS756]  DEFAULT ((1)),
 CONSTRAINT [PK_UserPermissions_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WorkPartCompany]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkPartCompany](
	[WorkPartCompanyID] [int] NOT NULL,
	[WorkPartCompanyName] [nvarchar](100) NULL,
	[Remarks] [ntext] NULL,
 CONSTRAINT [PK_WorkPartCompany] PRIMARY KEY CLUSTERED 
(
	[WorkPartCompanyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  View [dbo].[Additions_Deduction]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Additions_Deduction]
AS
SELECT        dbo.Employee_Additions.Additions_ID, dbo.Employee_AdditionsType.AdditionsType_Name, dbo.Employee_Additions.Addition_Value, dbo.Employee_Additions.Remarks, dbo.Employee_Deduction.Deduction_ID, 
                         dbo.Employee_DeductionType.DeductionType_Name, dbo.Employee_Deduction.Deduction_Value, dbo.Employee_Deduction.Remarks AS Remarks2
FROM            dbo.Employee_DeductionType INNER JOIN
                         dbo.Employee_Deduction ON dbo.Employee_DeductionType.DeductionType_ID = dbo.Employee_Deduction.DeductionType_ID CROSS JOIN
                         dbo.Employee_AdditionsType INNER JOIN
                         dbo.Employee_Additions ON dbo.Employee_AdditionsType.AdditionsType_ID = dbo.Employee_Additions.AdditionsType_ID
WHERE        (dbo.Employee_Additions.Additions_ID = 0)

--=====================================================================================================================================================

GO
/****** Object:  View [dbo].[Attendance_Report]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Attendance_Report]
AS
SELECT        dbo.Employee_Attendance.AttendanceID, dbo.Employee_Attendance.EmployeeID, dbo.Employees.EmployeeName, dbo.Employee_Attendance.MyDate, dbo.Employee_Attendance.Hower_Come, 
                         dbo.Employee_Attendance.Hower_Go, dbo.Employee_Attendance.Hower_Work, dbo.Employee_Attendance.States
FROM            dbo.Employee_Attendance INNER JOIN
                         dbo.Employees ON dbo.Employee_Attendance.EmployeeID = dbo.Employees.EmployeeID

--=====================================================================================================================================================

GO
/****** Object:  View [dbo].[Bill_Maintenance]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Bill_Maintenance]
AS
SELECT        dbo.Maintenance_Details.*, dbo.DamagedType.DamagedTypeName
FROM            dbo.DamagedType INNER JOIN
                         dbo.Maintenance_Details ON dbo.DamagedType.DamagedTypeID = dbo.Maintenance_Details.DamagedTypeID

GO
/****** Object:  View [dbo].[Bill_Print]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Bill_Print]
AS
SELECT        dbo.Sales_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Sales_Details.Unit, dbo.Sales_Details.Quantity, dbo.Sales_Details.Price, dbo.Sales_Details.Vat, dbo.Sales_Details.TotalPrice, 
                         dbo.Sales_Details.MainID
FROM            dbo.Sales_Details INNER JOIN
                         dbo.Prodecuts ON dbo.Sales_Details.ProdecutID = dbo.Prodecuts.ProdecutID


GO
/****** Object:  View [dbo].[Employee_Print]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Employee_Print]
AS
SELECT        dbo.Employees.EmployeeID, dbo.Employees.EmployeeName, dbo.Employees.Phone, dbo.Employees.WorkPartCompanyID, dbo.WorkPartCompany.WorkPartCompanyName, dbo.Employees.PartCompanyID, 
                         dbo.PartCompany.PartCompanyName, dbo.Employees.TimeAttendance, dbo.Employees.TimeToLeave, dbo.Employees.WorkingHours, dbo.Employees.Salary, dbo.Employees.Adress, dbo.Employees.SocialStatus, 
                         dbo.Employees.StartContract, dbo.Employees.EndContract
FROM            dbo.Employees INNER JOIN
                         dbo.WorkPartCompany ON dbo.Employees.WorkPartCompanyID = dbo.WorkPartCompany.WorkPartCompanyID INNER JOIN
                         dbo.PartCompany ON dbo.Employees.PartCompanyID = dbo.PartCompany.PartCompanyID

GO
/****** Object:  View [dbo].[RptMaintenance]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[RptMaintenance]
AS
SELECT        dbo.Maintenance.MaintenanceID, dbo.Maintenance.RecivedData, dbo.Maintenance.ReturnData, dbo.Maintenance.SerialNumber, 
                         dbo.Maintenance.DeviceTypeID, dbo.DeviceType.DeviceTypeName, dbo.DeviceType.CompenyID, dbo.MaintenanceCompeny.CompenyName, 
                         dbo.Maintenance.DamagedTypeID, dbo.DamagedType.DamagedTypeName, dbo.Maintenance.DeviceStatus, dbo.Maintenance.CustomerID, 
                         dbo.Customers.CustomerName, dbo.Customers.Phone, dbo.Maintenance.TotalPrice, dbo.Maintenance.Remark
FROM            dbo.DeviceType INNER JOIN
                         dbo.MaintenanceCompeny ON dbo.DeviceType.CompenyID = dbo.MaintenanceCompeny.CompenyID INNER JOIN
                         dbo.Maintenance ON dbo.DeviceType.DeviceTypeID = dbo.Maintenance.DeviceTypeID INNER JOIN
                         dbo.DamagedType ON dbo.Maintenance.DamagedTypeID = dbo.DamagedType.DamagedTypeID INNER JOIN
                         dbo.Customers ON dbo.Maintenance.CustomerID = dbo.Customers.CustomerID

GO
ALTER TABLE [dbo].[Bay_Details]  WITH CHECK ADD  CONSTRAINT [FK_Bay_Details_Bay_Main1] FOREIGN KEY([MainID])
REFERENCES [dbo].[Bay_Main] ([MainID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Bay_Details] CHECK CONSTRAINT [FK_Bay_Details_Bay_Main1]
GO
ALTER TABLE [dbo].[Bay_Details]  WITH CHECK ADD  CONSTRAINT [FK_Bay_Details_Stores1] FOREIGN KEY([StoreID])
REFERENCES [dbo].[Stores] ([StoreID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Bay_Details] CHECK CONSTRAINT [FK_Bay_Details_Stores1]
GO
ALTER TABLE [dbo].[Bay_Main]  WITH CHECK ADD  CONSTRAINT [FK_Bay_Main_Suppliers] FOREIGN KEY([SupplierID])
REFERENCES [dbo].[Suppliers] ([SupplierID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Bay_Main] CHECK CONSTRAINT [FK_Bay_Main_Suppliers]
GO
ALTER TABLE [dbo].[ChangeQuantity_Details]  WITH CHECK ADD  CONSTRAINT [FK_ChangeQuantity_Details_ChangeQuantity_Main] FOREIGN KEY([MainID])
REFERENCES [dbo].[ChangeQuantity_Main] ([MainID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ChangeQuantity_Details] CHECK CONSTRAINT [FK_ChangeQuantity_Details_ChangeQuantity_Main]
GO
ALTER TABLE [dbo].[ChangeQuantity_Details]  WITH CHECK ADD  CONSTRAINT [FK_ChangeQuantity_Details_Stores] FOREIGN KEY([StoreID])
REFERENCES [dbo].[Stores] ([StoreID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ChangeQuantity_Details] CHECK CONSTRAINT [FK_ChangeQuantity_Details_Stores]
GO
ALTER TABLE [dbo].[ChangeStore_Details]  WITH CHECK ADD  CONSTRAINT [FK_ChangeStore_Details_ChangeStore_Main] FOREIGN KEY([MainID])
REFERENCES [dbo].[ChangeStore_Main] ([MainID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ChangeStore_Details] CHECK CONSTRAINT [FK_ChangeStore_Details_ChangeStore_Main]
GO
ALTER TABLE [dbo].[Customers_Pay]  WITH CHECK ADD  CONSTRAINT [FK_Customers_Pay_Customers] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[Customers_Pay] CHECK CONSTRAINT [FK_Customers_Pay_Customers]
GO
ALTER TABLE [dbo].[Customers_Recived]  WITH CHECK ADD  CONSTRAINT [FK_Customers_Recived_Customers] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[Customers_Recived] CHECK CONSTRAINT [FK_Customers_Recived_Customers]
GO
ALTER TABLE [dbo].[DeviceType]  WITH CHECK ADD  CONSTRAINT [FK_DeviceType_MaintenanceCompeny] FOREIGN KEY([CompenyID])
REFERENCES [dbo].[MaintenanceCompeny] ([CompenyID])
GO
ALTER TABLE [dbo].[DeviceType] CHECK CONSTRAINT [FK_DeviceType_MaintenanceCompeny]
GO
ALTER TABLE [dbo].[Employee_Additions]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Additions_Employee_AdditionsType] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employees] ([EmployeeID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Employee_Additions] CHECK CONSTRAINT [FK_Employee_Additions_Employee_AdditionsType]
GO
ALTER TABLE [dbo].[Employee_Attendance]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Attendance_Employees] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employees] ([EmployeeID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Employee_Attendance] CHECK CONSTRAINT [FK_Employee_Attendance_Employees]
GO
ALTER TABLE [dbo].[Employee_Deduction]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Deduction_Employee_Deduction] FOREIGN KEY([Deduction_ID])
REFERENCES [dbo].[Employee_Deduction] ([Deduction_ID])
GO
ALTER TABLE [dbo].[Employee_Deduction] CHECK CONSTRAINT [FK_Employee_Deduction_Employee_Deduction]
GO
ALTER TABLE [dbo].[Employee_Deduction]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Deduction_Employee_DeductionType] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employees] ([EmployeeID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Employee_Deduction] CHECK CONSTRAINT [FK_Employee_Deduction_Employee_DeductionType]
GO
ALTER TABLE [dbo].[Employee_Holiday]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Holiday_HolidayType] FOREIGN KEY([HolidayID])
REFERENCES [dbo].[HolidayType] ([HolidayID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Employee_Holiday] CHECK CONSTRAINT [FK_Employee_Holiday_HolidayType]
GO
ALTER TABLE [dbo].[Employee_Salary]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Salary_Employees] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employees] ([EmployeeID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Employee_Salary] CHECK CONSTRAINT [FK_Employee_Salary_Employees]
GO
ALTER TABLE [dbo].[Firist_Details]  WITH CHECK ADD  CONSTRAINT [FK_Firist_Details_Firist_Main] FOREIGN KEY([MainID])
REFERENCES [dbo].[Firist_Main] ([MainID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Firist_Details] CHECK CONSTRAINT [FK_Firist_Details_Firist_Main]
GO
ALTER TABLE [dbo].[Maintenance]  WITH CHECK ADD  CONSTRAINT [FK_Maintenance_Customers] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[Maintenance] CHECK CONSTRAINT [FK_Maintenance_Customers]
GO
ALTER TABLE [dbo].[Maintenance]  WITH CHECK ADD  CONSTRAINT [FK_Maintenance_DeviceType] FOREIGN KEY([DeviceTypeID])
REFERENCES [dbo].[DeviceType] ([DeviceTypeID])
GO
ALTER TABLE [dbo].[Maintenance] CHECK CONSTRAINT [FK_Maintenance_DeviceType]
GO
ALTER TABLE [dbo].[Maintenance_Details]  WITH CHECK ADD  CONSTRAINT [FK_Maintenance_Details_DamagedType] FOREIGN KEY([DamagedTypeID])
REFERENCES [dbo].[DamagedType] ([DamagedTypeID])
GO
ALTER TABLE [dbo].[Maintenance_Details] CHECK CONSTRAINT [FK_Maintenance_Details_DamagedType]
GO
ALTER TABLE [dbo].[Maintenance_Main]  WITH CHECK ADD  CONSTRAINT [FK_Maintenance_Main_Customers] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[Maintenance_Main] CHECK CONSTRAINT [FK_Maintenance_Main_Customers]
GO
ALTER TABLE [dbo].[PayType_trans]  WITH CHECK ADD  CONSTRAINT [FK_PayType_trans_PayType] FOREIGN KEY([PayTypeID])
REFERENCES [dbo].[PayType] ([PayTypeID])
GO
ALTER TABLE [dbo].[PayType_trans] CHECK CONSTRAINT [FK_PayType_trans_PayType]
GO
ALTER TABLE [dbo].[Sales_Details]  WITH CHECK ADD  CONSTRAINT [FK_Sales_Details_Currency] FOREIGN KEY([CurrencyID])
REFERENCES [dbo].[Currency] ([CurrencyID])
GO
ALTER TABLE [dbo].[Sales_Details] CHECK CONSTRAINT [FK_Sales_Details_Currency]
GO
ALTER TABLE [dbo].[Sales_Details]  WITH CHECK ADD  CONSTRAINT [FK_Sales_Details_Stores] FOREIGN KEY([StoreID])
REFERENCES [dbo].[Stores] ([StoreID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Sales_Details] CHECK CONSTRAINT [FK_Sales_Details_Stores]
GO
ALTER TABLE [dbo].[Sales_Main]  WITH CHECK ADD  CONSTRAINT [FK_Sales_Main_Customers] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customers] ([CustomerID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Sales_Main] CHECK CONSTRAINT [FK_Sales_Main_Customers]
GO
ALTER TABLE [dbo].[Supplier_Pay]  WITH CHECK ADD  CONSTRAINT [FK_Supplier_Pay_Suppliers] FOREIGN KEY([SupplierID])
REFERENCES [dbo].[Suppliers] ([SupplierID])
GO
ALTER TABLE [dbo].[Supplier_Pay] CHECK CONSTRAINT [FK_Supplier_Pay_Suppliers]
GO
ALTER TABLE [dbo].[Supplier_Pay]  WITH CHECK ADD  CONSTRAINT [FK_Supplier_Pay_UserPermissions] FOREIGN KEY([UserAdd])
REFERENCES [dbo].[UserPermissions] ([ID])
GO
ALTER TABLE [dbo].[Supplier_Pay] CHECK CONSTRAINT [FK_Supplier_Pay_UserPermissions]
GO
ALTER TABLE [dbo].[Supplier_Recived]  WITH CHECK ADD  CONSTRAINT [FK_Supplier_Recived_Suppliers] FOREIGN KEY([SupplierID])
REFERENCES [dbo].[Suppliers] ([SupplierID])
GO
ALTER TABLE [dbo].[Supplier_Recived] CHECK CONSTRAINT [FK_Supplier_Recived_Suppliers]
GO
ALTER TABLE [dbo].[Supplier_Recived]  WITH CHECK ADD  CONSTRAINT [FK_Supplier_Recived_UserPermissions] FOREIGN KEY([UserAdd])
REFERENCES [dbo].[UserPermissions] ([ID])
GO
ALTER TABLE [dbo].[Supplier_Recived] CHECK CONSTRAINT [FK_Supplier_Recived_UserPermissions]
GO
/****** Object:  StoredProcedure [dbo].[ProductProfiles]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE	PROCEDURE [dbo].[ProductProfiles]
    @MyDate	date=NULL,
    @MyDate2	date=NULL,
    @StoreID int = NULL,
    @CategoryID int = NULL,
    @ProdecutID int = NULL
	AS

SELECT        dbo.Sales_Details.ID, dbo.Sales_Details.MainID, dbo.Sales_Main.MyDate, dbo.Sales_Main.CustomerID, dbo.Customers.CustomerName, 
                         dbo.Sales_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Sales_Details.Unit, dbo.Sales_Details.UnitFactor, dbo.Sales_Details.UnitOperating, 
                         dbo.Sales_Details.Quantity, dbo.Sales_Details.ReturnQuantity, dbo.Sales_Details.BayPrice, dbo.Sales_Details.Price, dbo.Sales_Details.Vat, 
                         dbo.Sales_Details.TotalPrice, dbo.Sales_Details.ReturnTotalPrice, dbo.Sales_Details.StoreID, dbo.Sales_Main.UserAdd, dbo.UserPermissions.EmpName, 
                         dbo.Sales_Details.TotalBayPrice, dbo.Stores.StoreName, dbo.Prodecuts.CategoryID
FROM            dbo.Sales_Main INNER JOIN
                         dbo.Sales_Details ON dbo.Sales_Main.MainID = dbo.Sales_Details.MainID INNER JOIN
                         dbo.Prodecuts ON dbo.Sales_Details.ProdecutID = dbo.Prodecuts.ProdecutID INNER JOIN
                         dbo.Customers ON dbo.Sales_Main.CustomerID = dbo.Customers.CustomerID INNER JOIN
                         dbo.UserPermissions ON dbo.Sales_Main.UserAdd = dbo.UserPermissions.ID INNER JOIN
                         dbo.Stores ON dbo.Sales_Details.StoreID = dbo.Stores.StoreID

		 where	 dbo.Sales_Main. MyDate >=@MyDate AND   dbo.Sales_Main. MyDate<=@MyDate2
			  
	AND  (@StoreID IS NULL OR  dbo.Sales_Details.StoreID = @StoreID)
	AND  (@CategoryID IS NULL OR  dbo.Prodecuts.CategoryID = @CategoryID)
	AND  (@ProdecutID IS NULL OR  dbo.Prodecuts.ProdecutID = @ProdecutID)
GO
/****** Object:  StoredProcedure [dbo].[ProductStores]    Script Date: 1/25/2020 9:18:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create	PROCEDURE [dbo].[ProductStores]
    @StoreID int = NULL,
    @CategoryID int = NULL,
    @ProdecutID int = NULL
	AS

SELECT        dbo.Store_Prodecut.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Prodecuts.CategoryID, dbo.Prodecuts.RequestLimit, dbo.Prodecuts.FiestUnit, 
                         dbo.Prodecuts.FiestUnitOperating, dbo.Prodecuts.FiestUnitFactor, dbo.Prodecuts.FiestUnitBarcode, dbo.Prodecuts.SecoundUnit, 
                         dbo.Prodecuts.SecoundUnitOperating, dbo.Prodecuts.SecoundUnitFactor, dbo.Prodecuts.SecoundUnitBarcode, dbo.Prodecuts.ThreeUnit, 
                         dbo.Prodecuts.ThreeUnitOperating, dbo.Prodecuts.ThreeUnitFactor, dbo.Prodecuts.ThreeUnitBarcode, dbo.Store_Prodecut.StoreID, dbo.Store_Prodecut.Balence, 
                         dbo.Store_Prodecut.Price, dbo.Store_Prodecut.ProdecutAvg, dbo.Prodecuts.FiestUnitPrice1
FROM            dbo.Store_Prodecut INNER JOIN
                         dbo.Prodecuts ON dbo.Store_Prodecut.ProdecutID = dbo.Prodecuts.ProdecutID

		 where	(@StoreID IS NULL OR  dbo.Store_Prodecut.StoreID = @StoreID)
	AND  (@CategoryID IS NULL OR  dbo.Prodecuts.CategoryID = @CategoryID)
	AND  (@ProdecutID IS NULL OR  dbo.Store_Prodecut.ProdecutID = @ProdecutID)
GO
USE [master]
GO
ALTER DATABASE [ByStro] SET  READ_WRITE 
GO

