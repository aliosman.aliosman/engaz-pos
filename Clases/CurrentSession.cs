﻿using ByStro.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByStro 
{
    class CurrentSession
    {

        public static DAL.CombanyData Company { get {
                DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
                return db.CombanyDatas.Single(x=>x.ID == 1 );

            }
        }
        public static DAL.UserPermission  user
        {
            get
            {
                DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
                return db.UserPermissions.Where(x=>x.ID == Properties.Settings.Default.UserID).First();

            }
        }
        public static DAL.SystemSetting SystemSettings
        {
            get
            {
                DBDataContext db = new DBDataContext(Properties.Settings.Default.Connection_String);
                return db.SystemSettings.First();

            }
        }
    }
}
