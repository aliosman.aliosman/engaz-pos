﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;


    class PurchaseOrder_cls : DataAccessLayer
    {
    //MaxID
    public String MaxID_PurchaseOrder_Main()
    {
        return Execute_SQL("select ISNULL (MAX(MainID)+1,1) from PurchaseOrder_Main", CommandType.Text);
    }

    // Insert
    public void Insert_PurchaseOrder_Main(long MainID, DateTime MyDate, string Remarks, Double TotalInvoice, Double Discount, Double NetInvoice, Double Vat, Double VatValue, int UserID)
    {
        Execute_SQL("insert into PurchaseOrder_Main(MainID ,MyDate ,Remarks ,TotalInvoice ,Discount ,NetInvoice ,Vat ,VatValue ,UserID )Values (@MainID ,@MyDate ,@Remarks ,@TotalInvoice ,@Discount ,@NetInvoice ,@Vat ,@VatValue ,@UserID )", CommandType.Text,
        Parameter("@MainID", SqlDbType.BigInt, MainID),
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@Remarks", SqlDbType.NText, Remarks),
        Parameter("@TotalInvoice", SqlDbType.Float, TotalInvoice),
        Parameter("@Discount", SqlDbType.Float, Discount),
        Parameter("@NetInvoice", SqlDbType.Float, NetInvoice),
        Parameter("@Vat", SqlDbType.Float, Vat),
        Parameter("@VatValue", SqlDbType.Float, VatValue),
        Parameter("@UserID", SqlDbType.Int, UserID));
    }


    //Update
    public void Update_PurchaseOrder_Main(long MainID, DateTime MyDate, string Remarks, Double TotalInvoice, Double Discount, Double NetInvoice, Double Vat, Double VatValue)
    {
        Execute_SQL("Update PurchaseOrder_Main Set MainID=@MainID ,MyDate=@MyDate ,Remarks=@Remarks ,TotalInvoice=@TotalInvoice ,Discount=@Discount ,NetInvoice=@NetInvoice ,Vat=@Vat ,VatValue=@VatValue  where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.BigInt, MainID),
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@Remarks", SqlDbType.NText, Remarks),
        Parameter("@TotalInvoice", SqlDbType.Float, TotalInvoice),
        Parameter("@Discount", SqlDbType.Float, Discount),
        Parameter("@NetInvoice", SqlDbType.Float, NetInvoice),
        Parameter("@Vat", SqlDbType.Float, Vat),
        Parameter("@VatValue", SqlDbType.Float, VatValue));
    }

    //Delete
    public void Delete_PurchaseOrder_Main(long MainID)
    {
        Execute_SQL("Delete  From PurchaseOrder_Main where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.BigInt, MainID));
    }

    //Details ID
    public DataTable Details_PurchaseOrder_Main(long MainID)
    {
        return ExecteRader("Select *  from PurchaseOrder_Main Where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.BigInt, MainID));
    }
    // Insert
    public void Insert_PurchaseOrder_Details(long MainID, int ProdecutID, string Unit, Double UnitFactor, Double UnitOperating, Double Quantity, Double Price, Double Vat, Double MainVat, Double TotalPrice, int StoreID, Boolean Recived)
    {
        Execute_SQL2("insert into PurchaseOrder_Details(MainID ,ProdecutID ,Unit ,UnitFactor ,UnitOperating ,Quantity ,Price ,Vat ,MainVat ,TotalPrice ,StoreID ,Recived )Values (@MainID ,@ProdecutID ,@Unit ,@UnitFactor ,@UnitOperating ,@Quantity ,@Price ,@Vat ,@MainVat ,@TotalPrice ,@StoreID ,@Recived )", CommandType.Text,
        Parameter("@MainID", SqlDbType.BigInt, MainID),
        Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
        Parameter("@Unit", SqlDbType.NVarChar, Unit),
        Parameter("@UnitFactor", SqlDbType.Float, UnitFactor),
        Parameter("@UnitOperating", SqlDbType.Float, UnitOperating),
        Parameter("@Quantity", SqlDbType.Float, Quantity),
        Parameter("@Price", SqlDbType.Float, Price),
        Parameter("@Vat", SqlDbType.Float, Vat),
        Parameter("@MainVat", SqlDbType.Float, MainVat),
        Parameter("@TotalPrice", SqlDbType.Float, TotalPrice),
        Parameter("@StoreID", SqlDbType.Int, StoreID),
        Parameter("@Recived", SqlDbType.Bit, Recived));
    }

    //Delete
    public void Delete_PurchaseOrder_Details(long MainID)
    {
        Execute_SQL("Delete  From PurchaseOrder_Details where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.BigInt, MainID));
    }


    //Details ID
    public DataTable Details_PurchaseOrder_Details(long MainID)
    {
        return ExecteRader(@"SELECT        dbo.PurchaseOrder_Details.*, dbo.Prodecuts.ProdecutName, dbo.Stores.StoreName
FROM            dbo.PurchaseOrder_Details INNER JOIN
                         dbo.Prodecuts ON dbo.PurchaseOrder_Details.ProdecutID = dbo.Prodecuts.ProdecutID INNER JOIN
                         dbo.Stores ON dbo.PurchaseOrder_Details.StoreID = dbo.Stores.StoreID Where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.BigInt, MainID));
    }




    public DataTable Search_PurchaseOrder_Main(DateTime MyDate, DateTime MyDate2, int UserID, string Search)
    {
        string str = @"SELECT        dbo.PurchaseOrder_Main.*, dbo.UserPermissions.EmpName
FROM            dbo.UserPermissions INNER JOIN
                         dbo.PurchaseOrder_Main ON dbo.UserPermissions.ID = dbo.PurchaseOrder_Main.UserID
WHERE        (dbo.PurchaseOrder_Main.MyDate >=@MyDate ) AND (dbo.PurchaseOrder_Main.MyDate <=@MyDate2) AND (dbo.PurchaseOrder_Main.UserID =@UserID) and convert(nvarchar,dbo.PurchaseOrder_Main.MainID) like '%'+@Search+ '%'";
        return ExecteRader(str, CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2),
        Parameter("@UserID", SqlDbType.Int, UserID),
        Parameter("@Search", SqlDbType.NVarChar, Search));

    }


    public DataTable Search_PurchaseOrder_Main(DateTime MyDate, DateTime MyDate2, string Search)
    {
        string str = @"SELECT        dbo.PurchaseOrder_Main.*, dbo.UserPermissions.EmpName
FROM            dbo.UserPermissions INNER JOIN
                         dbo.PurchaseOrder_Main ON dbo.UserPermissions.ID = dbo.PurchaseOrder_Main.UserID
WHERE        (dbo.PurchaseOrder_Main.MyDate >=@MyDate ) AND (dbo.PurchaseOrder_Main.MyDate <=@MyDate2) AND convert(nvarchar,dbo.PurchaseOrder_Main.MainID) like '%'+@Search+ '%'";
        return ExecteRader(str, CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2),
        Parameter("@Search", SqlDbType.NVarChar, Search));

    }














}

