﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;


class PayType_cls : DataAccessLayer
    {
    //MaxID
    public String MaxID_PayType()
    {
        return Execute_SQL("select ISNULL (MAX(PayTypeID)+1,1) from PayType", CommandType.Text);
    }


    // Insert
    public void Insert_PayType(string PayTypeID, String PayTypeName, String AccountNumber, String Address, String Phone, String Fax, string Remark)
    {
        Execute_SQL("insert into PayType(PayTypeID ,PayTypeName ,AccountNumber ,Address ,Phone ,Fax ,Remark )Values (@PayTypeID ,@PayTypeName ,@AccountNumber ,@Address ,@Phone ,@Fax ,@Remark )", CommandType.Text,
        Parameter("@PayTypeID", SqlDbType.Int, PayTypeID),
        Parameter("@PayTypeName", SqlDbType.NVarChar, PayTypeName),
        Parameter("@AccountNumber", SqlDbType.NVarChar, AccountNumber),
        Parameter("@Address", SqlDbType.NVarChar, Address),
        Parameter("@Phone", SqlDbType.NVarChar, Phone),
        Parameter("@Fax", SqlDbType.NVarChar, Fax),
        Parameter("@Remark", SqlDbType.NText, Remark));
    }

    //Update
    public void Update_PayType(string PayTypeID, String PayTypeName, String AccountNumber, String Address, String Phone, String Fax, string Remark)
    {
        Execute_SQL("Update PayType Set PayTypeID=@PayTypeID ,PayTypeName=@PayTypeName ,AccountNumber=@AccountNumber ,Address=@Address ,Phone=@Phone ,Fax=@Fax ,Remark=@Remark  where PayTypeID=@PayTypeID", CommandType.Text,
        Parameter("@PayTypeID", SqlDbType.Int, PayTypeID),
        Parameter("@PayTypeName", SqlDbType.NVarChar, PayTypeName),
        Parameter("@AccountNumber", SqlDbType.NVarChar, AccountNumber),
        Parameter("@Address", SqlDbType.NVarChar, Address),
        Parameter("@Phone", SqlDbType.NVarChar, Phone),
        Parameter("@Fax", SqlDbType.NVarChar, Fax),
        Parameter("@Remark", SqlDbType.NText, Remark));
    }

    //Delete
    public void Delete_PayType(string PayTypeID)
    {
        Execute_SQL("Delete  From PayType where PayTypeID=@PayTypeID", CommandType.Text,
        Parameter("@PayTypeID", SqlDbType.Int, PayTypeID));
    }

    //Details ID
    public DataTable Details_PayType(string PayTypeID)
    {
        return ExecteRader("Select *  from PayType Where PayTypeID=@PayTypeID", CommandType.Text,
        Parameter("@PayTypeID", SqlDbType.Int, PayTypeID));
    }

    //Details ID
    public DataTable NameSearch__PayType(String PayTypeName)
    {
        return ExecteRader("Select PayTypeName  from PayType Where PayTypeName=@PayTypeName", CommandType.Text,
        Parameter("@PayTypeName", SqlDbType.NVarChar, PayTypeName));
    }

    //Search 
    public DataTable Search__PayType(string Search)
    {
        return ExecteRader("Select *  from PayType Where convert(nvarchar,PayTypeID)+PayTypeName+AccountNumber+Phone+Fax like '%'+@Search+ '%' ", CommandType.Text,
        Parameter("@Search", SqlDbType.NVarChar, Search));
    }


    //=======================================================================================================================================================================

    //Details ID
    public DataTable PayType_trans(DateTime VoucherDate, DateTime VoucherDate2,string PayTypeID)
    {
        return ExecteRader(@" SELECT  dbo.PayType_trans.*, dbo.UserPermissions.EmpName FROM   dbo.UserPermissions INNER JOIN  dbo.PayType_trans ON dbo.UserPermissions.ID = dbo.PayType_trans.UserID
             Where VoucherDate >= @VoucherDate And VoucherDate <= @VoucherDate2 and PayTypeID=@PayTypeID", CommandType.Text,
              Parameter("@VoucherDate", SqlDbType.Date, VoucherDate),
            Parameter("@VoucherDate2", SqlDbType.Date, VoucherDate2),
             Parameter("@PayTypeID", SqlDbType.NVarChar, PayTypeID));
    }

    public DataTable PayType_trans(DateTime VoucherDate, DateTime VoucherDate2, string PayTypeID, string UserID)
    {
        return ExecteRader(@"SELECT  dbo.PayType_trans.*, dbo.UserPermissions.EmpName FROM   dbo.UserPermissions INNER JOIN  dbo.PayType_trans ON dbo.UserPermissions.ID = dbo.PayType_trans.UserID
             Where VoucherDate >= @VoucherDate And VoucherDate <= @VoucherDate2 and PayTypeID=@PayTypeID and UserID=@UserID", CommandType.Text,
              Parameter("@VoucherDate", SqlDbType.Date, VoucherDate),
            Parameter("@VoucherDate2", SqlDbType.Date, VoucherDate2),
                   Parameter("@PayTypeID", SqlDbType.NVarChar, PayTypeID),
            Parameter("@UserID", SqlDbType.NVarChar, UserID));
    }

    public DataTable PayType_trans(DateTime VoucherDate  ,string PayTypeID)
    {
        string sql = "select sum(Debit) as Debit , sum(Credit) as Credit from PayType_trans where  VoucherDate < @VoucherDate and PayTypeID=@PayTypeID";

        return ExecteRader(sql, CommandType.Text,
            Parameter("@VoucherDate", SqlDbType.Date, VoucherDate),
              Parameter("@PayTypeID", SqlDbType.NVarChar, PayTypeID));
    }


    public DataTable PayType_trans(DateTime VoucherDate, string PayTypeID, string UserID)
    {
        string sql = "select sum(Debit) as Debit , sum(Credit) as Credit from PayType_trans where  VoucherDate < @VoucherDate and PayTypeID=@PayTypeID and UserID=@UserID";

        return ExecteRader(sql, CommandType.Text,
            Parameter("@VoucherDate", SqlDbType.Date, VoucherDate),
                 Parameter("@PayTypeID", SqlDbType.NVarChar, PayTypeID),
             Parameter("@UserID", SqlDbType.NVarChar, UserID));
    }






}

