﻿
using System;
using System.Data;

class ProdecutAction_cls : DataAccessLayer
    {


    //Details ID
    public DataTable Search_Bay_Main(string StoreID,string ProdecutID, DateTime MyDate, DateTime MyDate2)
    {
        string sql = @"SELECT        dbo.Bay_Main.MyDate, dbo.Bay_Details.MainID, dbo.Bay_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Bay_Details.Unit, dbo.Bay_Details.Quantity, dbo.Bay_Details.ReturnQuantity, dbo.Bay_Details.Price, 
                         dbo.Bay_Details.Vat, dbo.Bay_Details.TotalPrice, dbo.Bay_Details.ReturnTotalPrice, dbo.Bay_Details.StoreID, dbo.Stores.StoreName, dbo.Bay_Details.Recived, dbo.UserPermissions.EmpName
FROM            dbo.Bay_Details INNER JOIN
                         dbo.Bay_Main ON dbo.Bay_Details.MainID = dbo.Bay_Main.MainID INNER JOIN
                         dbo.Prodecuts ON dbo.Bay_Details.ProdecutID = dbo.Prodecuts.ProdecutID INNER JOIN
                         dbo.Stores ON dbo.Bay_Details.StoreID = dbo.Stores.StoreID INNER JOIN
                         dbo.UserPermissions ON dbo.Bay_Main.UserAdd = dbo.UserPermissions.ID
WHERE        (dbo.Bay_Details.Recived = 1) And  (dbo.Bay_Details.StoreID = @StoreID) AND (dbo.Bay_Details.ProdecutID = @ProdecutID) and dbo.Bay_Main.MyDate>= @MyDate And dbo.Bay_Main.MyDate<= @MyDate2";
        return ExecteRader(sql, CommandType.Text,
             Parameter("@StoreID", SqlDbType.NVarChar, StoreID),
              Parameter("@ProdecutID", SqlDbType.NVarChar, ProdecutID),
               Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2));
    }

    //Details ID
    public DataTable Search_Bay_Main(string ProdecutID, DateTime MyDate, DateTime MyDate2)
    {
        string sql = @"SELECT        dbo.Bay_Main.MyDate, dbo.Bay_Details.MainID, dbo.Bay_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Bay_Details.Unit, dbo.Bay_Details.Quantity, dbo.Bay_Details.ReturnQuantity, dbo.Bay_Details.Price, 
                         dbo.Bay_Details.Vat, dbo.Bay_Details.TotalPrice, dbo.Bay_Details.ReturnTotalPrice, dbo.Bay_Details.StoreID, dbo.Stores.StoreName, dbo.Bay_Details.Recived, dbo.UserPermissions.EmpName
FROM            dbo.Bay_Details INNER JOIN
                         dbo.Bay_Main ON dbo.Bay_Details.MainID = dbo.Bay_Main.MainID INNER JOIN
                         dbo.Prodecuts ON dbo.Bay_Details.ProdecutID = dbo.Prodecuts.ProdecutID INNER JOIN
                         dbo.Stores ON dbo.Bay_Details.StoreID = dbo.Stores.StoreID INNER JOIN
                         dbo.UserPermissions ON dbo.Bay_Main.UserAdd = dbo.UserPermissions.ID
WHERE        (dbo.Bay_Details.Recived = 1) And (dbo.Bay_Details.ProdecutID = @ProdecutID) and dbo.Bay_Main.MyDate>= @MyDate And dbo.Bay_Main.MyDate<= @MyDate2";
        return ExecteRader(sql, CommandType.Text,
              Parameter("@ProdecutID", SqlDbType.NVarChar, ProdecutID),
               Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2));
    }

    //==================================================================================================================================================================
    public DataTable Search_Sales_Main(string StoreID, string ProdecutID, DateTime MyDate, DateTime MyDate2)
    {
        string sql = @"SELECT        dbo.Sales_Main.MyDate, dbo.Sales_Details.MainID, dbo.Sales_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Sales_Details.Unit, dbo.Sales_Details.Quantity, dbo.Sales_Details.ReturnQuantity, 
                         dbo.Sales_Details.Price, dbo.Sales_Details.Vat, dbo.Sales_Details.TotalPrice, dbo.Sales_Details.ReturnTotalPrice, dbo.Sales_Details.StoreID, dbo.Sales_Details.Recived, dbo.Stores.StoreName, 
                         dbo.UserPermissions.EmpName
FROM            dbo.Sales_Details INNER JOIN
                         dbo.Sales_Main ON dbo.Sales_Details.MainID = dbo.Sales_Main.MainID INNER JOIN
                         dbo.Prodecuts ON dbo.Sales_Details.ProdecutID = dbo.Prodecuts.ProdecutID INNER JOIN
                         dbo.Stores ON dbo.Sales_Details.StoreID = dbo.Stores.StoreID INNER JOIN
                         dbo.UserPermissions ON dbo.Sales_Main.UserAdd = dbo.UserPermissions.ID
WHERE        (dbo.Sales_Details.ProdecutID = @ProdecutID) AND (dbo.Sales_Details.StoreID = @StoreID) AND (dbo.Sales_Details.Recived = 1) and dbo.Sales_Main.MyDate>= @MyDate And dbo.Sales_Main.MyDate<= @MyDate2";
        return ExecteRader(sql, CommandType.Text,
             Parameter("@StoreID", SqlDbType.NVarChar, StoreID),
              Parameter("@ProdecutID", SqlDbType.NVarChar, ProdecutID),
               Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2));
    }

    //Details ID
    public DataTable Search_Sales_Main(string ProdecutID, DateTime MyDate, DateTime MyDate2)
    {
        string sql = @"SELECT        dbo.Sales_Main.MyDate, dbo.Sales_Details.MainID, dbo.Sales_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Sales_Details.Unit, dbo.Sales_Details.Quantity, dbo.Sales_Details.ReturnQuantity, 
                         dbo.Sales_Details.Price, dbo.Sales_Details.Vat, dbo.Sales_Details.TotalPrice, dbo.Sales_Details.ReturnTotalPrice, dbo.Sales_Details.StoreID, dbo.Sales_Details.Recived, dbo.Stores.StoreName, 
                         dbo.UserPermissions.EmpName
FROM            dbo.Sales_Details INNER JOIN
                         dbo.Sales_Main ON dbo.Sales_Details.MainID = dbo.Sales_Main.MainID INNER JOIN
                         dbo.Prodecuts ON dbo.Sales_Details.ProdecutID = dbo.Prodecuts.ProdecutID INNER JOIN
                         dbo.Stores ON dbo.Sales_Details.StoreID = dbo.Stores.StoreID INNER JOIN
                         dbo.UserPermissions ON dbo.Sales_Main.UserAdd = dbo.UserPermissions.ID
WHERE      (dbo.Sales_Details.ProdecutID = @ProdecutID) and dbo.Sales_Main.MyDate>= @MyDate And dbo.Sales_Main.MyDate<= @MyDate2";
        return ExecteRader(sql, CommandType.Text,
              Parameter("@ProdecutID", SqlDbType.NVarChar, ProdecutID),
               Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2));
    }

    //==================================================================================================================================================================
    public DataTable Search_ChangeQuantity_Main(string StoreID, string ProdecutID, DateTime MyDate, DateTime MyDate2)
    {
        string sql = @"SELECT        dbo.ChangeQuantity_Details.MainID, dbo.ChangeQuantity_Main.MyDate, dbo.ChangeQuantity_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Prodecuts.CategoryID, dbo.ChangeQuantity_Details.Unit, 
                         dbo.ChangeQuantity_Details.UnitFactor, dbo.ChangeQuantity_Details.UnitOperating, dbo.ChangeQuantity_Details.Quantity, dbo.ChangeQuantity_Details.Price, dbo.ChangeQuantity_Details.TotalPrice, 
                         dbo.ChangeQuantity_Details.StoreID, dbo.Stores.StoreName, dbo.ChangeQuantity_Details.SumType, dbo.ChangeQuantity_Details.Recived, dbo.ChangeQuantity_Main.UserAdd, dbo.UserPermissions.EmpName
FROM            dbo.Prodecuts INNER JOIN
                         dbo.ChangeQuantity_Details ON dbo.Prodecuts.ProdecutID = dbo.ChangeQuantity_Details.ProdecutID INNER JOIN
                         dbo.ChangeQuantity_Main ON dbo.ChangeQuantity_Details.MainID = dbo.ChangeQuantity_Main.MainID INNER JOIN
                         dbo.Stores ON dbo.ChangeQuantity_Details.StoreID = dbo.Stores.StoreID INNER JOIN
                         dbo.UserPermissions ON dbo.ChangeQuantity_Main.UserAdd = dbo.UserPermissions.ID
WHERE        (dbo.ChangeQuantity_Details.ProdecutID = @ProdecutID) AND (dbo.ChangeQuantity_Details.StoreID = @StoreID) AND (dbo.ChangeQuantity_Details.Recived = 1) and dbo.ChangeQuantity_Main.MyDate>= @MyDate And dbo.ChangeQuantity_Main.MyDate<= @MyDate2";
        return ExecteRader(sql, CommandType.Text,
             Parameter("@StoreID", SqlDbType.NVarChar, StoreID),
              Parameter("@ProdecutID", SqlDbType.NVarChar, ProdecutID),
               Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2));
    }

    //Details ID
    public DataTable Search_ChangeQuantity_Main(string ProdecutID, DateTime MyDate, DateTime MyDate2)
    {
        string sql = @"SELECT        dbo.ChangeQuantity_Details.MainID, dbo.ChangeQuantity_Main.MyDate, dbo.ChangeQuantity_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Prodecuts.CategoryID, dbo.ChangeQuantity_Details.Unit, 
                         dbo.ChangeQuantity_Details.UnitFactor, dbo.ChangeQuantity_Details.UnitOperating, dbo.ChangeQuantity_Details.Quantity, dbo.ChangeQuantity_Details.Price, dbo.ChangeQuantity_Details.TotalPrice, 
                         dbo.ChangeQuantity_Details.StoreID, dbo.Stores.StoreName, dbo.ChangeQuantity_Details.SumType, dbo.ChangeQuantity_Details.Recived, dbo.ChangeQuantity_Main.UserAdd, dbo.UserPermissions.EmpName
FROM            dbo.Prodecuts INNER JOIN
                         dbo.ChangeQuantity_Details ON dbo.Prodecuts.ProdecutID = dbo.ChangeQuantity_Details.ProdecutID INNER JOIN
                         dbo.ChangeQuantity_Main ON dbo.ChangeQuantity_Details.MainID = dbo.ChangeQuantity_Main.MainID INNER JOIN
                         dbo.Stores ON dbo.ChangeQuantity_Details.StoreID = dbo.Stores.StoreID INNER JOIN
                         dbo.UserPermissions ON dbo.ChangeQuantity_Main.UserAdd = dbo.UserPermissions.ID
WHERE      (dbo.ChangeQuantity_Details.ProdecutID = @ProdecutID) and dbo.ChangeQuantity_Main.MyDate>= @MyDate And dbo.ChangeQuantity_Main.MyDate<= @MyDate2";
        return ExecteRader(sql, CommandType.Text,
              Parameter("@ProdecutID", SqlDbType.NVarChar, ProdecutID),
               Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2));
    }

    //==================================================================================================================================================================
    public DataTable Search_ChangeStore_Main(string StoreID, string ProdecutID, DateTime MyDate, DateTime MyDate2)
    {
        string sql = @"SELECT        dbo.ChangeStore_Main.MyDate, dbo.ChangeStore_Details.MainID, dbo.ChangeStore_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.ChangeStore_Details.Unit, dbo.ChangeStore_Details.Quantity, 
                         dbo.ChangeStore_Details.Price, dbo.ChangeStore_Details.TotalPrice, dbo.ChangeStore_Details.StoreID, dbo.Stores.StoreName, dbo.ChangeStore_Details.SumType, dbo.ChangeStore_Details.Recived, 
                         dbo.ChangeStore_Main.UserAdd, dbo.UserPermissions.EmpName
FROM            dbo.ChangeStore_Details INNER JOIN
                         dbo.ChangeStore_Main ON dbo.ChangeStore_Details.MainID = dbo.ChangeStore_Main.MainID INNER JOIN
                         dbo.Stores ON dbo.ChangeStore_Details.StoreID = dbo.Stores.StoreID INNER JOIN
                         dbo.UserPermissions ON dbo.ChangeStore_Main.UserAdd = dbo.UserPermissions.ID INNER JOIN
                         dbo.Prodecuts ON dbo.ChangeStore_Details.ProdecutID = dbo.Prodecuts.ProdecutID
WHERE        (dbo.ChangeStore_Details.ProdecutID = @ProdecutID) AND (dbo.ChangeStore_Details.StoreID = @StoreID) AND (dbo.ChangeStore_Details.Recived = 1) and dbo.ChangeStore_Main.MyDate>= @MyDate And dbo.ChangeStore_Main.MyDate<= @MyDate2";
        return ExecteRader(sql, CommandType.Text,
             Parameter("@StoreID", SqlDbType.NVarChar, StoreID),
              Parameter("@ProdecutID", SqlDbType.NVarChar, ProdecutID),
               Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2));
    }

    //Details ID
    public DataTable Search_ChangeStore_Main(string ProdecutID, DateTime MyDate, DateTime MyDate2)
    {
        string sql = @"SELECT        dbo.ChangeStore_Main.MyDate, dbo.ChangeStore_Details.MainID, dbo.ChangeStore_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.ChangeStore_Details.Unit, dbo.ChangeStore_Details.Quantity, 
                         dbo.ChangeStore_Details.Price, dbo.ChangeStore_Details.TotalPrice, dbo.ChangeStore_Details.StoreID, dbo.Stores.StoreName, dbo.ChangeStore_Details.SumType, dbo.ChangeStore_Details.Recived, 
                         dbo.ChangeStore_Main.UserAdd, dbo.UserPermissions.EmpName
FROM            dbo.ChangeStore_Details INNER JOIN
                         dbo.ChangeStore_Main ON dbo.ChangeStore_Details.MainID = dbo.ChangeStore_Main.MainID INNER JOIN
                         dbo.Stores ON dbo.ChangeStore_Details.StoreID = dbo.Stores.StoreID INNER JOIN
                         dbo.UserPermissions ON dbo.ChangeStore_Main.UserAdd = dbo.UserPermissions.ID INNER JOIN
                         dbo.Prodecuts ON dbo.ChangeStore_Details.ProdecutID = dbo.Prodecuts.ProdecutID
WHERE      (dbo.ChangeStore_Details.ProdecutID = @ProdecutID) and dbo.ChangeStore_Main.MyDate>= @MyDate And dbo.ChangeStore_Main.MyDate<= @MyDate2";
        return ExecteRader(sql, CommandType.Text,
              Parameter("@ProdecutID", SqlDbType.NVarChar, ProdecutID),
               Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2));
    }


    public DataTable Search_Firist_Main(string ProdecutID, DateTime MyDate, DateTime MyDate2)
    {
        string sql = @"SELECT        dbo.Firist_Main.MyDate, dbo.Firist_Details.ID, dbo.Firist_Details.MainID, dbo.Firist_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Firist_Details.Unit, dbo.Firist_Details.UnitFactor, dbo.Firist_Details.Quantity, 
                         dbo.Firist_Details.UnitOperating, dbo.Firist_Details.Price, dbo.Firist_Details.TotalPrice, dbo.Firist_Details.StoreID, dbo.Stores.StoreName, dbo.Firist_Details.Recived, dbo.UserPermissions.EmpName
FROM            dbo.Firist_Details INNER JOIN
                         dbo.Firist_Main ON dbo.Firist_Details.MainID = dbo.Firist_Main.MainID INNER JOIN
                         dbo.Stores ON dbo.Firist_Details.StoreID = dbo.Stores.StoreID INNER JOIN
                         dbo.Prodecuts ON dbo.Firist_Details.ProdecutID = dbo.Prodecuts.ProdecutID INNER JOIN
                         dbo.UserPermissions ON dbo.Firist_Main.UserAdd = dbo.UserPermissions.ID
WHERE      (dbo.Firist_Details.ProdecutID = @ProdecutID) and dbo.Firist_Main.MyDate>= @MyDate And dbo.Firist_Main.MyDate<= @MyDate2";
        return ExecteRader(sql, CommandType.Text,
              Parameter("@ProdecutID", SqlDbType.NVarChar, ProdecutID),
               Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2));
    }

    public DataTable Search_Firist_Main(String StoreID, string ProdecutID, DateTime MyDate, DateTime MyDate2)
    {
        string sql = @"SELECT        dbo.Firist_Main.MyDate, dbo.Firist_Details.ID, dbo.Firist_Details.MainID, dbo.Firist_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Firist_Details.Unit, dbo.Firist_Details.UnitFactor, dbo.Firist_Details.Quantity, 
                         dbo.Firist_Details.UnitOperating, dbo.Firist_Details.Price, dbo.Firist_Details.TotalPrice, dbo.Firist_Details.StoreID, dbo.Stores.StoreName, dbo.Firist_Details.Recived, dbo.UserPermissions.EmpName
FROM            dbo.Firist_Details INNER JOIN
                         dbo.Firist_Main ON dbo.Firist_Details.MainID = dbo.Firist_Main.MainID INNER JOIN
                         dbo.Stores ON dbo.Firist_Details.StoreID = dbo.Stores.StoreID INNER JOIN
                         dbo.Prodecuts ON dbo.Firist_Details.ProdecutID = dbo.Prodecuts.ProdecutID INNER JOIN
                         dbo.UserPermissions ON dbo.Firist_Main.UserAdd = dbo.UserPermissions.ID
WHERE (dbo.Firist_Details.StoreID = @StoreID)   AND    (dbo.Firist_Details.ProdecutID = @ProdecutID) and dbo.Firist_Main.MyDate>= @MyDate And dbo.Firist_Main.MyDate<= @MyDate2";
        return ExecteRader(sql, CommandType.Text,
            Parameter("@StoreID", SqlDbType.NVarChar, StoreID),
              Parameter("@ProdecutID", SqlDbType.NVarChar, ProdecutID),
               Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2));
    }
     

    #region "تقرير فروق اسعار الشراء"


    //Details ID
    public DataTable Search_Prodect_Prise(string StoreID, string ProdecutID, DateTime MyDate, DateTime MyDate2)
    {
        string sql = @"SELECT        dbo.Bay_Details.MainID, dbo.Bay_Main.MyDate, dbo.Bay_Main.SupplierID, dbo.Suppliers.SupplierName, dbo.Bay_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Bay_Details.Quantity, dbo.Bay_Details.Unit, 
                         dbo.Bay_Details.UnitFactor, dbo.Bay_Details.UnitOperating, dbo.Bay_Details.ReturnQuantity, dbo.Bay_Details.Price, dbo.Bay_Details.Vat, dbo.Bay_Details.TotalPrice, dbo.Bay_Details.ReturnTotalPrice, 
                         dbo.Bay_Details.StoreID, dbo.Stores.StoreName, dbo.Bay_Details.Recived
FROM            dbo.Bay_Details INNER JOIN
                         dbo.Bay_Main ON dbo.Bay_Details.MainID = dbo.Bay_Main.MainID INNER JOIN
                         dbo.Stores ON dbo.Bay_Details.StoreID = dbo.Stores.StoreID INNER JOIN
                         dbo.Suppliers ON dbo.Bay_Main.SupplierID = dbo.Suppliers.SupplierID INNER JOIN
                         dbo.Prodecuts ON dbo.Bay_Details.ProdecutID = dbo.Prodecuts.ProdecutID
WHERE        (dbo.Bay_Details.Recived = 1) AND(dbo.Bay_Details.ProdecutID =@ProdecutID) AND(dbo.Bay_Details.StoreID =@StoreID) And dbo.Bay_Main.MyDate>= @MyDate And dbo.Bay_Main.MyDate<= @MyDate2";
        return ExecteRader(sql, CommandType.Text,
             Parameter("@StoreID", SqlDbType.NVarChar, StoreID),
              Parameter("@ProdecutID", SqlDbType.NVarChar, ProdecutID),
               Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2));
    }

    //Details ID
    public DataTable Search_Prodect_Prise(string ProdecutID, DateTime MyDate, DateTime MyDate2)
    {
        string sql = @"SELECT        dbo.Bay_Details.MainID, dbo.Bay_Main.MyDate, dbo.Bay_Main.SupplierID, dbo.Suppliers.SupplierName, dbo.Bay_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Bay_Details.Quantity, dbo.Bay_Details.Unit, 
                         dbo.Bay_Details.UnitFactor, dbo.Bay_Details.UnitOperating, dbo.Bay_Details.ReturnQuantity, dbo.Bay_Details.Price, dbo.Bay_Details.Vat, dbo.Bay_Details.TotalPrice, dbo.Bay_Details.ReturnTotalPrice, 
                         dbo.Bay_Details.StoreID, dbo.Stores.StoreName, dbo.Bay_Details.Recived
FROM            dbo.Bay_Details INNER JOIN
                         dbo.Bay_Main ON dbo.Bay_Details.MainID = dbo.Bay_Main.MainID INNER JOIN
                         dbo.Stores ON dbo.Bay_Details.StoreID = dbo.Stores.StoreID INNER JOIN
                         dbo.Suppliers ON dbo.Bay_Main.SupplierID = dbo.Suppliers.SupplierID INNER JOIN
                         dbo.Prodecuts ON dbo.Bay_Details.ProdecutID = dbo.Prodecuts.ProdecutID
WHERE        (dbo.Bay_Details.Recived = 1) AND(dbo.Bay_Details.ProdecutID =@ProdecutID) And dbo.Bay_Main.MyDate>= @MyDate And dbo.Bay_Main.MyDate<= @MyDate2";
        return ExecteRader(sql, CommandType.Text,
              Parameter("@ProdecutID", SqlDbType.NVarChar, ProdecutID),
               Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2));
    }





    #endregion











}

