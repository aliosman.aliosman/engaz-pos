﻿
using System;
using System.Data;

class Expense_cls : DataAccessLayer
{
    //MaxID
    public String MaxID_Expenses()
    {
        return Execute_SQL("select ISNULL (MAX(ExpenseID)+1,1) from Expenses", CommandType.Text);
    }
    // Insert
    public void InsertExpenses(string ExpenseID, string ExpenseTypeID, DateTime MyDate, string RecivedName, string ExpenseValue, string Remarks, string TreasuryID, int UserAdd)
    {
        Execute_SQL("insert into Expenses(ExpenseID ,ExpenseTypeID ,MyDate ,RecivedName ,ExpenseValue ,Remarks ,TreasuryID ,UserAdd )Values (@ExpenseID ,@ExpenseTypeID ,@MyDate ,@RecivedName ,@ExpenseValue ,@Remarks ,@TreasuryID ,@UserAdd )", CommandType.Text,
        Parameter("@ExpenseID", SqlDbType.NVarChar, ExpenseID),
        Parameter("@ExpenseTypeID", SqlDbType.NVarChar, ExpenseTypeID),
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@RecivedName", SqlDbType.NVarChar, RecivedName),
        Parameter("@ExpenseValue", SqlDbType.NVarChar, ExpenseValue),
        Parameter("@Remarks", SqlDbType.NText, Remarks),
        Parameter("@TreasuryID", SqlDbType.NVarChar, TreasuryID),
        Parameter("@UserAdd", SqlDbType.NVarChar, UserAdd));
    }

    //Update
    public void UpdateExpenses(string ExpenseID, string ExpenseTypeID, DateTime MyDate, string RecivedName, string ExpenseValue, string Remarks, string TreasuryID)
    {
        Execute_SQL("Update Expenses Set ExpenseID=@ExpenseID ,ExpenseTypeID=@ExpenseTypeID ,MyDate=@MyDate ,RecivedName=@RecivedName ,ExpenseValue=@ExpenseValue ,Remarks=@Remarks ,TreasuryID=@TreasuryID  where ExpenseID=@ExpenseID", CommandType.Text,
        Parameter("@ExpenseID", SqlDbType.NVarChar, ExpenseID),
        Parameter("@ExpenseTypeID", SqlDbType.NVarChar, ExpenseTypeID),
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@RecivedName", SqlDbType.NVarChar, RecivedName),
        Parameter("@ExpenseValue", SqlDbType.NVarChar, ExpenseValue),
        Parameter("@Remarks", SqlDbType.NText, Remarks),
        Parameter("@TreasuryID", SqlDbType.NVarChar, TreasuryID));
    }

    //Delete
    public void DeleteExpenses(string ExpenseID)
    {
        Execute_SQL("Delete  From Expenses where ExpenseID=@ExpenseID", CommandType.Text,
        Parameter("@ExpenseID", SqlDbType.NVarChar, ExpenseID));
    }





    //Details ID
    public DataTable Details_Expenses(String ExpenseID)
    {
        return ExecteRader(@"SELECT        dbo.Expenses.*, dbo.ExpenseType.ExpenseName
FROM            dbo.Expenses INNER JOIN
                         dbo.ExpenseType ON dbo.Expenses.ExpenseTypeID = dbo.ExpenseType.ExpenseID where dbo.Expenses.ExpenseID =@ExpenseID", CommandType.Text,
        Parameter("@ExpenseID", SqlDbType.NVarChar, ExpenseID));
    }




    //Details ID
    public DataTable Search_Expenses()
    {
        return ExecteRader(@"SELECT        dbo.Expenses.*, dbo.ExpenseType.ExpenseName
FROM            dbo.Expenses INNER JOIN
                         dbo.ExpenseType ON dbo.Expenses.ExpenseTypeID = dbo.ExpenseType.ExpenseID ", CommandType.Text);
    }









}

