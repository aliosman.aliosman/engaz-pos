﻿
using System;
using System.Data;

class Menu_Meny_cls : DataAccessLayer
    {

    // Firest,bay,ChaneQuentity true ,chane store true
    private DataSet stror_ProdectPlus(string ProdecutID, Boolean SumType, DateTime MyDate)
    {
        string sql = @"
SELECT ISNULL(SUM(dbo.Firist_Details.Quantity * dbo.Firist_Details.UnitFactor), 0) AS Quantity, ISNULL(SUM(dbo.Firist_Details.TotalPrice), 0) AS TotalPrice FROM  dbo.Firist_Details INNER JOIN dbo.Firist_Main ON dbo.Firist_Details.MainID = dbo.Firist_Main.MainID where ProdecutID=@ProdecutID and Recived='1' and  MyDate <=@MyDate
SELECT ISNULL(SUM(dbo.Bay_Details.Quantity * dbo.Bay_Details.UnitFactor) - SUM(dbo.Bay_Details.ReturnQuantity * dbo.Bay_Details.UnitFactor), 0) AS Quantity, ISNULL(SUM(dbo.Bay_Details.TotalPrice) - SUM(dbo.Bay_Details.ReturnTotalPrice), 0) AS TotalPrice FROM  dbo.Bay_Details INNER JOIN dbo.Bay_Main ON dbo.Bay_Details.MainID = dbo.Bay_Main.MainID where ProdecutID=@ProdecutID and Recived='1' and  MyDate <=@MyDate
SELECT ISNULL(SUM(dbo.ChangeQuantity_Details.Quantity * dbo.ChangeQuantity_Details.UnitFactor), 0) AS Quantity, ISNULL(SUM(dbo.ChangeQuantity_Details.TotalPrice), 0) AS TotalPrice FROM  dbo.ChangeQuantity_Details INNER JOIN dbo.ChangeQuantity_Main ON dbo.ChangeQuantity_Details.MainID = dbo.ChangeQuantity_Main.MainID where ProdecutID=@ProdecutID  and SumType=@SumType and Recived='1' and  MyDate <=@MyDate
SELECT ISNULL(SUM(dbo.ChangeStore_Details.Quantity * dbo.ChangeStore_Details.UnitFactor), 0) AS Quantity, ISNULL(SUM(dbo.ChangeStore_Details.TotalPrice), 0) AS TotalPrice FROM  dbo.ChangeStore_Details INNER JOIN dbo.ChangeStore_Main ON dbo.ChangeStore_Details.MainID = dbo.ChangeStore_Main.MainID where ProdecutID=@ProdecutID and SumType=@SumType and Recived='1' and  MyDate <=@MyDate";
        return ExecteRader_DataSet(sql, CommandType.Text,
        Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
        Parameter("@SumType", SqlDbType.Bit, SumType),
         Parameter("@MyDate", SqlDbType.Date, MyDate));
    }

    // Sales,ChaneQuentity False ,chane store False
    private DataSet stror_ProdectMains(string ProdecutID, Boolean SumType,DateTime MyDate)
    {
        string sql = @"
SELECT  ISNULL(SUM(dbo.Sales_Details.Quantity * dbo.Sales_Details.UnitFactor) - SUM(dbo.Sales_Details.ReturnQuantity * dbo.Sales_Details.UnitFactor), 0) AS Quantity, ISNULL(SUM(dbo.Sales_Details.TotalBayPrice)  - SUM(dbo.Sales_Details.ReturnQuantity) * SUM(dbo.Sales_Details.TotalBayPrice), 0) AS TotalPrice FROM dbo.Sales_Details INNER JOIN dbo.Sales_Main ON dbo.Sales_Details.MainID = dbo.Sales_Main.MainID where ProdecutID=@ProdecutID and Recived='1' and  MyDate <=@MyDate
SELECT ISNULL(SUM(dbo.ChangeQuantity_Details.Quantity * dbo.ChangeQuantity_Details.UnitFactor), 0) AS Quantity, ISNULL(SUM(dbo.ChangeQuantity_Details.TotalPrice), 0) AS TotalPrice FROM  dbo.ChangeQuantity_Details INNER JOIN dbo.ChangeQuantity_Main ON dbo.ChangeQuantity_Details.MainID = dbo.ChangeQuantity_Main.MainID where ProdecutID=@ProdecutID and SumType=@SumType and Recived='1' and  MyDate <=@MyDate
SELECT ISNULL(SUM(dbo.ChangeStore_Details.Quantity * dbo.ChangeStore_Details.UnitFactor), 0) AS Quantity, ISNULL(SUM(dbo.ChangeStore_Details.TotalPrice), 0) AS TotalPrice FROM  dbo.ChangeStore_Details INNER JOIN dbo.ChangeStore_Main ON dbo.ChangeStore_Details.MainID = dbo.ChangeStore_Main.MainID where ProdecutID=@ProdecutID and SumType=@SumType and Recived='1' and  MyDate <=@MyDate";
        return ExecteRader_DataSet(sql, CommandType.Text,
        Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
        Parameter("@SumType", SqlDbType.Bit, SumType),
        Parameter("@MyDate", SqlDbType.Date, MyDate) );
    }






    //Details ID
    public DataTable Search_Sales(DateTime MyDate)
    {
        return ExecteRader("select isnull( SUM(NetInvoice),0) as NetInvoice, isnull(SUM(ReturnInvoise),0) as ReturnInvoise ,isnull(SUM(TotalBayPrice),0) as TotalBayPrice , isnull(SUM(ReturnBayPrice),0) as ReturnBayPrice  from Sales_Main  where  MyDate <=@MyDate", CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate));
    }

    
    //Details ID
    public DataTable Search_Bay(DateTime MyDate)
    {
        return ExecteRader("select isnull (SUM(Discount),0) as Discount from Bay_Main  where  MyDate <=@MyDate", CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate));
    }


    //Details ID
    public DataTable Search_FirestProdecut()
    {
        return ExecteRader("select isnull (SUM(TotalPrice),0) as TotalPrice from Firist_Details", CommandType.Text);
    }

    // Expenses
    public DataTable Search_Expenses(DateTime MyDate)
    {
        return ExecteRader("select isnull (SUM(ExpenseValue),0) as ExpenseValue from Expenses where MyDate<=@MyDate", CommandType.Text,
             Parameter("@MyDate", SqlDbType.Date, MyDate));
    }

    // Expenses
    public DataTable Search_Salary(DateTime MyDate)
    {
        return ExecteRader("select isnull (SUM(MainSalary),0)+isnull(SUM(Additions),0) as NetSalary from Employee_Salary where  MONTH(MyDate)<='" + (MyDate.ToString("MM")) + "' And YEAR(MyDate)<='" + (MyDate.ToString("yyyy")) + "'", CommandType.Text);
    }

    // Expenses
    public DataTable Search_Maintenance(DateTime MyDate)
    {
        return ExecteRader("select isnull (SUM(PaidInvoice),0) as Paid from Maintenance_Main where  MONTH(MyDate)<='" + (MyDate.ToString("MM")) + "' And YEAR(MyDate)<='" + (MyDate.ToString("yyyy")) + "'", CommandType.Text);
    }
    //Details ID
    public DataTable Details_ALL_Prodecuts()
    {
        return ExecteRader("Select ProdecutID  from Prodecuts", CommandType.Text);
    }




    #region "جرد المخزن لمعرفة مخزون اخر المدة"
    //Details ID
    private DataTable Details_Firist(string ProdecutID, DateTime MyDate)
    {
        return ExecteRader(@"SELECT   ISNULL(SUM(dbo.Firist_Details.Quantity * dbo.Firist_Details.UnitFactor), 0) AS Quantity, ISNULL(SUM(dbo.Firist_Details.TotalPrice), 0) AS TotalPrice
FROM            dbo.Firist_Details INNER JOIN
                         dbo.Firist_Main ON dbo.Firist_Details.MainID = dbo.Firist_Main.MainID where ProdecutID=@ProdecutID and Recived='1' And MyDate <=@MyDate", CommandType.Text,
        Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
         Parameter("@MyDate", SqlDbType.Date, MyDate));
    }


    private DataTable Details_Bay(string ProdecutID, DateTime MyDate)
    {
        return ExecteRader(@"SELECT        ISNULL(SUM(dbo.Bay_Details.Quantity * dbo.Bay_Details.UnitFactor) - SUM(dbo.Bay_Details.ReturnQuantity * dbo.Bay_Details.UnitFactor), 0) AS Quantity, ISNULL(SUM(dbo.Bay_Details.TotalPrice) 
                         - SUM(dbo.Bay_Details.ReturnTotalPrice), 0) AS TotalPrice
FROM dbo.Bay_Details INNER JOIN
                         dbo.Bay_Main ON dbo.Bay_Details.MainID = dbo.Bay_Main.MainID where ProdecutID=@ProdecutID and  Recived='1' And MyDate <=@MyDate", CommandType.Text,
        Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
         Parameter("@MyDate", SqlDbType.Date, MyDate));
    }
    private DataTable Details_Sales(string ProdecutID, DateTime MyDate)
    {
        return ExecteRader(@"SELECT        ISNULL(SUM(dbo.Sales_Details.Quantity * dbo.Sales_Details.UnitFactor) - SUM(dbo.Sales_Details.ReturnQuantity * dbo.Sales_Details.UnitFactor), 0) AS Quantity, ISNULL(SUM(dbo.Sales_Details.TotalBayPrice) 
                         - SUM(dbo.Sales_Details.ReturnQuantity) * SUM(dbo.Sales_Details.TotalBayPrice), 0) AS TotalPrice
FROM            dbo.Sales_Details INNER JOIN
                         dbo.Sales_Main ON dbo.Sales_Details.MainID = dbo.Sales_Main.MainID where ProdecutID=@ProdecutID and Recived='1' And MyDate <=@MyDate", CommandType.Text,
        Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
         Parameter("@MyDate", SqlDbType.Date, MyDate));
    }

    private DataTable Details_ChangeQuantity(string ProdecutID, Boolean SumType, DateTime MyDate)
    {
        return ExecteRader(@"SELECT        ISNULL(SUM(dbo.ChangeQuantity_Details.Quantity * dbo.ChangeQuantity_Details.UnitFactor), 0) AS Quantity, ISNULL(SUM(dbo.ChangeQuantity_Details.TotalPrice), 0) AS TotalPrice
FROM            dbo.ChangeQuantity_Details INNER JOIN
                         dbo.ChangeQuantity_Main ON dbo.ChangeQuantity_Details.MainID = dbo.ChangeQuantity_Main.MainID where ProdecutID=@ProdecutID and SumType=@SumType and Recived='1' And MyDate <=@MyDate", CommandType.Text,
        Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@SumType", SqlDbType.Bit, SumType));
    }

    private DataTable Details_ChangeStore(string ProdecutID, Boolean SumType, DateTime MyDate)
    {
        return ExecteRader(@"SELECT  ISNULL(SUM(dbo.ChangeStore_Details.Quantity * dbo.ChangeStore_Details.UnitFactor), 0) AS Quantity, ISNULL(SUM(dbo.ChangeStore_Details.TotalPrice), 0) AS TotalPrice
FROM            dbo.ChangeStore_Details INNER JOIN
                         dbo.ChangeStore_Main ON dbo.ChangeStore_Details.MainID = dbo.ChangeStore_Main.MainID where ProdecutID=@ProdecutID  and SumType=@SumType and Recived='1' And MyDate<=@MyDate", CommandType.Text,
        Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@SumType", SqlDbType.Bit, SumType));
    }




    public Double QuantityNow_Avg(string ProdecutID, DateTime MyDate)
    {
        DataSet Ds_Plus = new DataSet();
         Ds_Plus = stror_ProdectPlus(ProdecutID, true, MyDate);
        DataSet Ds_Mains = stror_ProdectMains(ProdecutID, false, MyDate);

        Double Plus_Quentity = Convert.ToDouble(Ds_Plus.Tables[0].Rows[0][0]) + Convert.ToDouble(Ds_Plus.Tables[1].Rows[0][0]) + Convert.ToDouble(Ds_Plus.Tables[2].Rows[0][0]) + Convert.ToDouble(Ds_Plus.Tables[3].Rows[0][0]);
        Double Plus_TotalPrice = Convert.ToDouble(Ds_Plus.Tables[0].Rows[0][1]) + Convert.ToDouble(Ds_Plus.Tables[1].Rows[0][1]) + Convert.ToDouble(Ds_Plus.Tables[2].Rows[0][1]) + Convert.ToDouble(Ds_Plus.Tables[3].Rows[0][1]);

        Double Mains_Quentity = Convert.ToDouble(Ds_Mains.Tables[0].Rows[0][0]) + Convert.ToDouble(Ds_Mains.Tables[1].Rows[0][0]) + Convert.ToDouble(Ds_Mains.Tables[2].Rows[0][0]);
        Double Mains_TotalPrice = Convert.ToDouble(Ds_Mains.Tables[0].Rows[0][1]) + Convert.ToDouble(Ds_Mains.Tables[1].Rows[0][1]) + Convert.ToDouble(Ds_Mains.Tables[2].Rows[0][1]);


        Double Avg = 0;
        Double x = 0;
        if ((Plus_TotalPrice - Mains_TotalPrice) != 0 && (Plus_Quentity - Mains_Quentity) != 0)
        {
            Avg = (Plus_TotalPrice - Mains_TotalPrice) / (Plus_Quentity - Mains_Quentity);
        }

      return   x = (Plus_Quentity - Mains_Quentity) * Avg;
    }


    //public Double QuantityNow_Avg(string ProdecutID, DateTime MyDate)
    //{
    //    Double Avg = 0;
    //    Double Quantity_Firist = 0;
    //    Double Quantity_Bay = 0;
    //    Double Quantity_Sales = 0;
    //    Double Quantity_ChangeQuantityTrue = 0;
    //    Double Quantity_ChangeStoreTrue = 0;
    //    Double Quantity_ChangeQuantityFalse = 0;
    //    Double Quantity_ChangeStoreFalse = 0;
    //    //==========================================================================================
    //    Double TotalPrise_Firist = 0;
    //    Double TotalPrise_Bay = 0;
    //    Double TotalPrise_Sales = 0;
    //    Double TotalPrise_ChangeQuantityTrue = 0;
    //    Double TotalPrise_ChangeStoreTrue = 0;
    //    Double TotalPrise_ChangeQuantityFalse = 0;
    //    Double TotalPrise_ChangeStoreFalse = 0;
    //    //==========================================================================================
    //    //DataTable dtFirist = Details_Firist( ProdecutID,  StoreID);
    //    Quantity_Firist = Convert.ToDouble(Details_Firist(ProdecutID, MyDate).Rows[0][0].ToString());
    //    Quantity_Bay = Convert.ToDouble(Details_Bay(ProdecutID, MyDate).Rows[0][0].ToString());
    //    Quantity_Sales = Convert.ToDouble(Details_Sales(ProdecutID, MyDate).Rows[0][0].ToString());
    //    Quantity_ChangeQuantityTrue = Convert.ToDouble(Details_ChangeQuantity(ProdecutID, true, MyDate).Rows[0][0].ToString());
    //    Quantity_ChangeStoreTrue = Convert.ToDouble(Details_ChangeStore(ProdecutID, true, MyDate).Rows[0][0].ToString());
    //    Quantity_ChangeQuantityFalse = Convert.ToDouble(Details_ChangeQuantity(ProdecutID, false, MyDate).Rows[0][0].ToString());
    //    Quantity_ChangeStoreFalse = Convert.ToDouble(Details_ChangeStore(ProdecutID, false, MyDate).Rows[0][0].ToString());
    //    //==========================================================================================================================================
    //    TotalPrise_Firist = Convert.ToDouble(Details_Firist(ProdecutID, MyDate).Rows[0][1].ToString());
    //    TotalPrise_Bay = Convert.ToDouble(Details_Bay(ProdecutID, MyDate).Rows[0][1].ToString());
    //    TotalPrise_Sales = Convert.ToDouble(Details_Sales(ProdecutID, MyDate).Rows[0][1].ToString());
    //    TotalPrise_ChangeQuantityTrue = Convert.ToDouble(Details_ChangeQuantity(ProdecutID, true, MyDate).Rows[0][1].ToString());
    //    TotalPrise_ChangeStoreTrue = Convert.ToDouble(Details_ChangeStore(ProdecutID, true, MyDate).Rows[0][1].ToString());
    //    TotalPrise_ChangeQuantityFalse = Convert.ToDouble(Details_ChangeQuantity(ProdecutID, false, MyDate).Rows[0][1].ToString());
    //    TotalPrise_ChangeStoreFalse = Convert.ToDouble(Details_ChangeStore(ProdecutID, false, MyDate).Rows[0][1].ToString());
    //    double sum_Quntity = Quantity_Firist + Quantity_Bay + Quantity_ChangeQuantityTrue + Quantity_ChangeStoreTrue;
    //    double Mines_Quntity = Quantity_Sales + Quantity_ChangeQuantityFalse + Quantity_ChangeQuantityFalse + Quantity_ChangeStoreFalse;
    //    double sum_Total = TotalPrise_Firist + TotalPrise_Bay + TotalPrise_ChangeQuantityTrue + TotalPrise_ChangeStoreTrue;
    //    double Mines_Total = TotalPrise_Sales + TotalPrise_ChangeQuantityFalse + TotalPrise_ChangeQuantityFalse + TotalPrise_ChangeStoreFalse;
    //    if ((sum_Total - Mines_Total) != 0 || (sum_Quntity - Mines_Quntity) != 0)
    //    {
    //        Avg = (sum_Total - Mines_Total) / (sum_Quntity - Mines_Quntity);

    //    }
    //      Double x=         (sum_Quntity - Mines_Quntity) * Avg;
    //    return x;
    //}


    #endregion
}

