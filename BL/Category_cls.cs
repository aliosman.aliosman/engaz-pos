﻿using System;
using System.Data;
class Category_cls : DataAccessLayer
{
    //MaxID
    public String MaxID_Category()
    {
     
        return Execute_SQL("select ISNULL (MAX(CategoryID)+1,1) from Category", CommandType.Text);
    }

    // Insert
    public void Insert_Category(String CategoryID, String CategoryName, string Remark)
    {
        Execute_SQL("insert into Category(CategoryID ,CategoryName ,Remark )Values (@CategoryID ,@CategoryName ,@Remark )", CommandType.Text,
        Parameter("@CategoryID", SqlDbType.Int, CategoryID),
        Parameter("@CategoryName", SqlDbType.NVarChar, CategoryName),
        Parameter("@Remark", SqlDbType.NVarChar, Remark));
    }

    //Update
    public void Update_Category(string CategoryID, String CategoryName, string Remark)
    {
        Execute_SQL("Update Category Set CategoryID=@CategoryID ,CategoryName=@CategoryName ,Remark=@Remark  where CategoryID=@CategoryID", CommandType.Text,
          Parameter("@CategoryID", SqlDbType.Int, CategoryID),
        Parameter("@CategoryName", SqlDbType.NVarChar, CategoryName),
        Parameter("@Remark", SqlDbType.NVarChar, Remark));
    }

    //Delete
    public void Delete_Category(string CategoryID)
    {
        Execute_SQL("Delete  From Category where CategoryID=@CategoryID", CommandType.Text,
         Parameter("@CategoryID", SqlDbType.Int, CategoryID));
    }

    //Details ID
    public DataTable Details_Category(string CategoryID)
    {
        return ExecteRader("select * From Category where CategoryID=@CategoryID", CommandType.Text,
        Parameter("@CategoryID", SqlDbType.Int, CategoryID));
    }

    //Details ID
    public DataTable NameSearch_Category(String CategoryName)
    {
        return ExecteRader("select CategoryName From Category where CategoryName=@CategoryName", CommandType.Text,
        Parameter("@CategoryName", SqlDbType.NVarChar, CategoryName));
    }

    public DataTable Search_Category_Delete(String CategoryID)
    {
        return ExecteRader("Select CategoryID  from Prodecuts where CategoryID=@CategoryID", CommandType.Text,
        Parameter("@CategoryID", SqlDbType.NVarChar, CategoryID));
    }
    
    //Search
    public DataTable Search_Category(String Search)
    {
        return ExecteRader("Select *  from Category where convert(nvarchar, CategoryID) + CategoryName like '%' + @Search + '%'", CommandType.Text,
        Parameter("@Search", SqlDbType.NVarChar, Search));
    }

    // NoDelete
    public DataTable NoDelete_Prodecuts(String CategoryID)
    {
        return ExecteRader("Select CategoryID  from Prodecuts Where CategoryID=@CategoryID", CommandType.Text,
        Parameter("@CategoryID", SqlDbType.Int, CategoryID));
    }



}

