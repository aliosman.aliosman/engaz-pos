﻿using System;
using System.Data;

class BillStore_cls : DataAccessLayer
{

    //MaxID
    public String MaxID_ChangeStore_Main()
    {
        return Execute_SQL("select ISNULL (MAX(MainID)+1,1) from ChangeStore_Main", CommandType.Text);
    }

    //Details ID
    public DataTable Details_Stores(string StoreID)
    {
        return ExecteRader("Select StoreName  from Stores where StoreID = @StoreID", CommandType.Text,
        Parameter("@StoreID", SqlDbType.NVarChar, StoreID));
    }


    

    // Insert
    public void InsertChangeStore_Main(string MainID, DateTime MyDate, string Remarks, string StoreID, string StoreID2, int UserAdd)
    {
        Execute_SQL("insert into ChangeStore_Main(MainID ,MyDate ,Remarks ,StoreID ,StoreID2 ,UserAdd )Values (@MainID ,@MyDate ,@Remarks ,@StoreID ,@StoreID2 ,@UserAdd )", CommandType.Text,
        Parameter("@MainID", SqlDbType.BigInt, MainID),
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@Remarks", SqlDbType.NText, Remarks),
        Parameter("@StoreID", SqlDbType.Int, StoreID),
        Parameter("@StoreID2", SqlDbType.Int, StoreID2),
        Parameter("@UserAdd", SqlDbType.Int, UserAdd));
    }



    //Update
    public void UpdateChangeStore_Main(string MainID, DateTime MyDate, string Remarks, string StoreID, string StoreID2)
    {
        Execute_SQL("Update ChangeStore_Main Set MainID=@MainID ,MyDate=@MyDate ,Remarks=@Remarks ,StoreID=@StoreID ,StoreID2=@StoreID2  where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.BigInt, MainID),
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@Remarks", SqlDbType.NText, Remarks),
        Parameter("@StoreID", SqlDbType.Int, StoreID),
        Parameter("@StoreID2", SqlDbType.Int, StoreID2));
    }



    //Delete
    public void DeleteChangeStore_Main(string MainID)
    {
        Execute_SQL("Delete  From ChangeStore_Main where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.NVarChar, MainID));
    }





    //Details ID
    public DataTable Details_ChangeStore_Main(string MainID)
    {
        return ExecteRader("Select *  from ChangeStore_Main where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.NVarChar, MainID));
    }


    //Search 
    public DataTable Search_ChangeStore_Main()
    {
        return ExecteRader("Select *  from ChangeStore_Main", CommandType.Text);
    }

    //=================================================================================================================================


    //MaxID
    private String MaxID_ChangeStore_Details()
    {
        return Execute_SQL("select ISNULL (MAX(ID)+1,1) from ChangeStore_Details", CommandType.Text);
    }

    // Insert
    public void InsertChangeStore_Details(string MainID, string ProdecutID, string Unit, string UnitFactor, string UnitOperating, string Quantity,string Price,string TotalPrice, string StoreID, Boolean SumType, Boolean Recived)
    {
        Execute_SQL("insert into ChangeStore_Details(ID ,MainID ,ProdecutID ,Unit ,UnitFactor ,UnitOperating ,Quantity,Price,TotalPrice ,StoreID ,SumType ,Recived )Values (@ID ,@MainID ,@ProdecutID ,@Unit ,@UnitFactor ,@UnitOperating ,@Quantity,@Price,@TotalPrice ,@StoreID ,@SumType ,@Recived )", CommandType.Text,
        Parameter("@ID", SqlDbType.BigInt, MaxID_ChangeStore_Details()),
        Parameter("@MainID", SqlDbType.BigInt, MainID),
        Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
        Parameter("@Unit", SqlDbType.NVarChar, Unit),
        Parameter("@UnitFactor", SqlDbType.Float, UnitFactor),
        Parameter("@UnitOperating", SqlDbType.Float, UnitOperating),
        Parameter("@Quantity", SqlDbType.Float, Quantity),
        Parameter("@Price", SqlDbType.Float, Price),
        Parameter("@TotalPrice", SqlDbType.Float, TotalPrice),
        Parameter("@StoreID", SqlDbType.Int, StoreID),
        Parameter("@SumType", SqlDbType.Bit, SumType),
        Parameter("@Recived", SqlDbType.Bit, Recived));
    }



    //Delete
    public void Delete_ChangeStore_Details(string MainID)
    {
        Execute_SQL("delete from ChangeStore_Details where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.NVarChar, MainID));
    }



    //Details ID
    public DataTable Details_ChangeStore_Details(string MainID)
    {
        return ExecteRader(@"SELECT        dbo.ChangeStore_Details.ID, dbo.ChangeStore_Details.MainID, dbo.ChangeStore_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.ChangeStore_Details.Unit, dbo.ChangeStore_Details.UnitFactor, 
                         dbo.ChangeStore_Details.UnitOperating, dbo.ChangeStore_Details.Quantity, dbo.ChangeStore_Details.StoreID, dbo.ChangeStore_Details.SumType, dbo.ChangeStore_Details.Recived, dbo.ChangeStore_Details.Price, 
                         dbo.ChangeStore_Details.TotalPrice
FROM            dbo.ChangeStore_Details INNER JOIN
                         dbo.Prodecuts ON dbo.ChangeStore_Details.ProdecutID = dbo.Prodecuts.ProdecutID
                      where dbo.ChangeStore_Details.MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.NVarChar, MainID));
    }


    //Details ID
    public DataTable Details_ChangeStore_Details(string MainID,Boolean SumType)
    {
        return ExecteRader(@"SELECT        dbo.ChangeStore_Details.ID, dbo.ChangeStore_Details.MainID, dbo.ChangeStore_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.ChangeStore_Details.Unit, dbo.ChangeStore_Details.UnitFactor, 
                         dbo.ChangeStore_Details.UnitOperating, dbo.ChangeStore_Details.Quantity, dbo.ChangeStore_Details.StoreID, dbo.ChangeStore_Details.SumType, dbo.ChangeStore_Details.Recived, dbo.ChangeStore_Details.Price, 
                         dbo.ChangeStore_Details.TotalPrice
FROM            dbo.ChangeStore_Details INNER JOIN
                         dbo.Prodecuts ON dbo.ChangeStore_Details.ProdecutID = dbo.Prodecuts.ProdecutID
                      where dbo.ChangeStore_Details.MainID=@MainID and dbo.ChangeStore_Details.SumType = @SumType", CommandType.Text,
        Parameter("@MainID", SqlDbType.NVarChar, MainID),
                Parameter("@SumType", SqlDbType.Bit, SumType));
    }





}

