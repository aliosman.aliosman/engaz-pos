﻿
using System;
using System.Data;

class AVG_cls : DataAccessLayer
{
    //**************************************************************************************************************************************************************************************************************************************************************************

        // Firest,bay,ChaneQuentity true ,chane store true
    private DataSet stror_ProdectPlus(string ProdecutID, string StoreID, Boolean SumType)
    {
        string sql = @"
Select ISNULL(SUM(Quantity*UnitFactor),0) as Quantity,ISNULL(SUM(TotalPrice),0)as TotalPrice from [dbo].[Firist_Details] where ProdecutID=@ProdecutID and StoreID=@StoreID and Recived='1'
Select ISNULL(SUM(Quantity*UnitFactor)-SUM(ReturnQuantity*UnitFactor),0) as Quantity,ISNULL(SUM(TotalPrice)-SUM(ReturnTotalPrice),0)as TotalPrice from Bay_Details where ProdecutID=@ProdecutID and StoreID=@StoreID and Recived='1'
Select ISNULL(SUM(Quantity*UnitFactor),0) as Quantity,ISNULL(SUM(TotalPrice),0)as TotalPrice from ChangeQuantity_Details where ProdecutID=@ProdecutID and StoreID=@StoreID and SumType=@SumType and Recived='1'
Select ISNULL(SUM(Quantity*UnitFactor),0) as Quantity,ISNULL(SUM(TotalPrice),0)as TotalPrice from ChangeStore_Details where ProdecutID=@ProdecutID and StoreID=@StoreID and SumType=@SumType and Recived='1'";
        return ExecteRader_DataSet(sql, CommandType.Text,
        Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
        Parameter("@StoreID", SqlDbType.Int, StoreID),
        Parameter("@SumType", SqlDbType.Bit, SumType));
    }

    // Sales,ChaneQuentity False ,chane store False
    private DataSet stror_ProdectMains(string ProdecutID, string StoreID, Boolean SumType)
    {
        string sql = @"
Select ISNULL(SUM(Quantity*UnitFactor)-SUM(ReturnQuantity*UnitFactor),0) as Quantity,ISNULL(SUM(TotalBayPrice)-(SUM(ReturnQuantity) * SUM(TotalBayPrice)),0)as TotalPrice from Sales_Details where ProdecutID=@ProdecutID and StoreID=@StoreID and Recived='1'
Select ISNULL(SUM(Quantity*UnitFactor),0) as Quantity,ISNULL(SUM(TotalPrice),0)as TotalPrice from ChangeQuantity_Details where ProdecutID=@ProdecutID and StoreID=@StoreID and SumType=@SumType and Recived='1'
Select ISNULL(SUM(Quantity*UnitFactor),0) as Quantity,ISNULL(SUM(TotalPrice),0)as TotalPrice from ChangeStore_Details where ProdecutID=@ProdecutID and StoreID=@StoreID and SumType=@SumType and Recived='1'";
        return ExecteRader_DataSet(sql, CommandType.Text,
        Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
        Parameter("@StoreID", SqlDbType.Int, StoreID),
        Parameter("@SumType", SqlDbType.Bit, SumType));
    }









    //**************************************************************************************************************************************************************************************************************************************************************************
    //Details ID
    private DataTable Details_Firist(string ProdecutID, string StoreID)
    {
        return ExecteRader("Select ISNULL(SUM(Quantity*UnitFactor),0) as Quantity,ISNULL(SUM(TotalPrice),0)as TotalPrice from [dbo].[Firist_Details] where ProdecutID=@ProdecutID and StoreID=@StoreID and Recived='1'", CommandType.Text,
        Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
        Parameter("@StoreID", SqlDbType.Int, StoreID));
    }


    private DataTable Details_Bay(string ProdecutID, string StoreID)
    {
        return ExecteRader("Select ISNULL(SUM(Quantity*UnitFactor)-SUM(ReturnQuantity*UnitFactor),0) as Quantity,ISNULL(SUM(TotalPrice)-SUM(ReturnTotalPrice),0)as TotalPrice from Bay_Details where ProdecutID=@ProdecutID and StoreID=@StoreID and Recived='1'", CommandType.Text,
        Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
        Parameter("@StoreID", SqlDbType.Int, StoreID));
    }
    private DataTable Details_Sales(string ProdecutID, string StoreID)
    {
        return ExecteRader("Select ISNULL(SUM(Quantity*UnitFactor)-SUM(ReturnQuantity*UnitFactor),0) as Quantity,ISNULL(SUM(TotalBayPrice)-(SUM(ReturnQuantity) * SUM(TotalBayPrice)),0)as TotalPrice from Sales_Details where ProdecutID=@ProdecutID and StoreID=@StoreID and Recived='1'", CommandType.Text,
        Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
        Parameter("@StoreID", SqlDbType.Int, StoreID));
    }

    private DataTable Details_ChangeQuantity(string ProdecutID, string StoreID, Boolean SumType)
    {
        return ExecteRader("Select ISNULL(SUM(Quantity*UnitFactor),0) as Quantity,ISNULL(SUM(TotalPrice),0)as TotalPrice from ChangeQuantity_Details where ProdecutID=@ProdecutID and StoreID=@StoreID and SumType=@SumType and Recived='1'", CommandType.Text,
        Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
        Parameter("@StoreID", SqlDbType.Int, StoreID),
        Parameter("@SumType", SqlDbType.Bit, SumType));
    }

    private DataTable Details_ChangeStore(string ProdecutID, string StoreID, Boolean SumType)
    {
        return ExecteRader("Select ISNULL(SUM(Quantity*UnitFactor),0) as Quantity,ISNULL(SUM(TotalPrice),0)as TotalPrice from ChangeStore_Details where ProdecutID=@ProdecutID and StoreID=@StoreID and SumType=@SumType and Recived='1'", CommandType.Text,
        Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
        Parameter("@StoreID", SqlDbType.Int, StoreID),
        Parameter("@SumType", SqlDbType.Bit, SumType));
    }


    private void Update_Quantity_TotalPrise_Avg( string ProdecutID, string StoreID, Double Balence, Double Price,Double ProdecutAvg)
    {
        Execute_SQL("Update Store_Prodecut Set  Balence=@Balence,Price=@Price,ProdecutAvg=@ProdecutAvg Where ProdecutID=@ProdecutID  And StoreID=@StoreID", CommandType.Text,
        Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
        Parameter("@StoreID", SqlDbType.Int, StoreID),
        Parameter("@Balence", SqlDbType.Float, Balence),
                 Parameter("@Price", SqlDbType.Float, Price),
         Parameter("@ProdecutAvg", SqlDbType.Float, ProdecutAvg));
    }

    private void Update_Quantity_TotalPrise_Avg(string ProdecutID, string StoreID, Double Balence, Double Price)
    {
        Execute_SQL("Update Store_Prodecut Set  Balence=@Balence,Price=@Price Where ProdecutID=@ProdecutID  And StoreID=@StoreID", CommandType.Text,
        Parameter("@ProdecutID", SqlDbType.Int, ProdecutID),
        Parameter("@StoreID", SqlDbType.Int, StoreID),
        Parameter("@Balence", SqlDbType.Float, Balence),
                 Parameter("@Price", SqlDbType.Float, Price));
    }

    public void QuantityNow_Avg(string ProdecutID, string StoreID,Boolean Yes_Avg)
    {
        //DataSet Ds_Plus= stror_ProdectPlus(ProdecutID, StoreID,true);
        //DataSet Ds_Mains = stror_ProdectMains(ProdecutID, StoreID, false);

        //Double Plus_Quentity = Convert.ToDouble(Ds_Plus.Tables[0].Rows[0][0]) + Convert.ToDouble(Ds_Plus.Tables[1].Rows[0][0]) + Convert.ToDouble(Ds_Plus.Tables[2].Rows[0][0]) + Convert.ToDouble(Ds_Plus.Tables[3].Rows[0][0]);
        //Double Plus_TotalPrice = Convert.ToDouble(Ds_Plus.Tables[0].Rows[0][1]) + Convert.ToDouble(Ds_Plus.Tables[1].Rows[0][1]) + Convert.ToDouble(Ds_Plus.Tables[2].Rows[0][1]) + Convert.ToDouble(Ds_Plus.Tables[3].Rows[0][1]);

        //Double Mains_Quentity = Convert.ToDouble(Ds_Mains.Tables[0].Rows[0][0]) + Convert.ToDouble(Ds_Mains.Tables[1].Rows[0][0]) + Convert.ToDouble(Ds_Mains.Tables[2].Rows[0][0]) ;
        //Double Mains_TotalPrice = Convert.ToDouble(Ds_Mains.Tables[0].Rows[0][1]) + Convert.ToDouble(Ds_Mains.Tables[1].Rows[0][1]) + Convert.ToDouble(Ds_Mains.Tables[2].Rows[0][1]);


        //Double Avg = 0;

        //if ((Plus_TotalPrice - Mains_TotalPrice) != 0 && (Plus_Quentity - Mains_Quentity) != 0)
        //{
        //    Avg = (Plus_TotalPrice - Mains_TotalPrice) / (Plus_Quentity - Mains_Quentity);
        //}
         
        //if (Yes_Avg==true)
        //{
        //Update_Quantity_TotalPrise_Avg(ProdecutID,  StoreID, (Plus_Quentity - Mains_Quentity), (Plus_TotalPrice - Mains_TotalPrice),Avg);
        //}
        //else
        //{
        //    Update_Quantity_TotalPrise_Avg(ProdecutID, StoreID, (Plus_Quentity - Mains_Quentity), (Plus_TotalPrice - Mains_TotalPrice));
        //}

    }


}

