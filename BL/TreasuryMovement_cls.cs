﻿
using System;
using System.Data;
class TreasuryMovement_cls : DataAccessLayer
{


    public String MaxID_TreasuryMovement()
    {
        return Execute_SQL("select ISNULL (MAX(ID)+1,1) from TreasuryMovement", CommandType.Text);
    }

    public void InsertTreasuryMovement(string ID, String AccountID, String ISCusSupp, String VoucherID, String Type, String VoucherType, DateTime VoucherDate, string Description, string income, string Export, string Remark, string Debit, string Credit, int UserAdd, Boolean ISShow = true)
    {
        Execute_SQL("insert into TreasuryMovement(ID ,AccountID ,ISCusSupp ,VoucherID ,Type ,VoucherType ,VoucherDate ,Description ,income ,Export,Remark ,Debit ,Credit ,UserAdd,ISShow )Values (@ID ,@AccountID ,@ISCusSupp ,@VoucherID ,@Type ,@VoucherType ,@VoucherDate ,@Description ,@income ,@Export,@Remark ,@Debit ,@Credit ,@UserAdd,@ISShow )", CommandType.Text,
        Parameter("@ID", SqlDbType.NVarChar, ID),
        Parameter("@AccountID", SqlDbType.NVarChar, AccountID),
        Parameter("@ISCusSupp", SqlDbType.NVarChar, ISCusSupp),
        Parameter("@VoucherID", SqlDbType.NVarChar, VoucherID),
        Parameter("@Type", SqlDbType.NVarChar, Type),
        Parameter("@VoucherType", SqlDbType.NVarChar, VoucherType),
        Parameter("@VoucherDate", SqlDbType.Date, VoucherDate),
        Parameter("@Description", SqlDbType.NText, Description),
        Parameter("@income", SqlDbType.Float, income),
        Parameter("@Export", SqlDbType.Float, Export),
        Parameter("@Remark", SqlDbType.NVarChar, Remark),
        Parameter("@Debit", SqlDbType.Float, Debit),
        Parameter("@Credit", SqlDbType.Float, Credit), 
        Parameter("@UserAdd", SqlDbType.Int, UserAdd),
        Parameter("@ISShow", SqlDbType.Bit, ISShow));
    }


    // Insert
    //public void InsertTreasuryMovement(string ID, String AccountID, String ISCusSupp, String VoucherID, String Type, String VoucherType, DateTime VoucherDate, string Description, string CurrencyID, string CurrencyRate, string income, string CurrencyIncome, string Export, string CurrencyExport, string Remark, string Debit, string CurrencyDebit, string Credit, string CurrencyCredit, int UserAdd, Boolean ISShow)
    //{
    //    Execute_SQL("insert into TreasuryMovement(ID ,AccountID ,ISCusSupp ,VoucherID ,Type ,VoucherType ,VoucherDate ,Description ,CurrencyID ,CurrencyRate ,income ,CurrencyIncome ,Export ,CurrencyExport ,Remark ,Debit ,CurrencyDebit ,Credit ,CurrencyCredit ,UserAdd ,ISShow )Values (@ID ,@AccountID ,@ISCusSupp ,@VoucherID ,@Type ,@VoucherType ,@VoucherDate ,@Description ,@CurrencyID ,@CurrencyRate ,@income ,@CurrencyIncome ,@Export ,@CurrencyExport ,@Remark ,@Debit ,@CurrencyDebit ,@Credit ,@CurrencyCredit ,@UserAdd ,@ISShow )", CommandType.Text,
    //    Parameter("@ID", SqlDbType.BigInt, ID),
    //    Parameter("@AccountID", SqlDbType.NVarChar, AccountID),
    //    Parameter("@ISCusSupp", SqlDbType.NVarChar, ISCusSupp),
    //    Parameter("@VoucherID", SqlDbType.NVarChar, VoucherID),
    //    Parameter("@Type", SqlDbType.NVarChar, Type),
    //    Parameter("@VoucherType", SqlDbType.NVarChar, VoucherType),
    //    Parameter("@VoucherDate", SqlDbType.Date, VoucherDate),
    //    Parameter("@Description", SqlDbType.NText, Description),
    //    Parameter("@CurrencyID", SqlDbType.Int, CurrencyID),
    //    Parameter("@CurrencyRate", SqlDbType.Float, CurrencyRate),
    //    Parameter("@income", SqlDbType.Float, income),
    //    Parameter("@CurrencyIncome", SqlDbType.Float, CurrencyIncome),
    //    Parameter("@Export", SqlDbType.Float, Export),
    //    Parameter("@CurrencyExport", SqlDbType.Float, CurrencyExport),
    //    Parameter("@Remark", SqlDbType.NText, Remark),
    //    Parameter("@Debit", SqlDbType.Float, Debit),
    //    Parameter("@CurrencyDebit", SqlDbType.Float, CurrencyDebit),
    //    Parameter("@Credit", SqlDbType.Float, Credit),
    //    Parameter("@CurrencyCredit", SqlDbType.Float, CurrencyCredit),
    //    Parameter("@UserAdd", SqlDbType.Int, UserAdd),
    //    Parameter("@ISShow", SqlDbType.Bit, ISShow));
    //}







    //Update

    public void UpdateTreasuryMovement(string ID, String AccountID, String ISCusSupp, String VoucherID, String Type, String VoucherType, DateTime VoucherDate, string Description, string income, string Export,string Remark, string Debit, string Credit)
    {
        Execute_SQL("Update TreasuryMovement Set AccountID=@AccountID ,ISCusSupp=@ISCusSupp ,VoucherID=@VoucherID ,Type=@Type ,VoucherType=@VoucherType ,VoucherDate=@VoucherDate ,Description=@Description ,income=@income ,Export=@Export,Remark=@Remark ,Debit=@Debit ,Credit=@Credit  where ID=@ID", CommandType.Text,
        Parameter("@ID", SqlDbType.BigInt, ID),
        Parameter("@AccountID", SqlDbType.NVarChar, AccountID),
        Parameter("@ISCusSupp", SqlDbType.NVarChar, ISCusSupp),
        Parameter("@VoucherID", SqlDbType.NVarChar, VoucherID),
        Parameter("@Type", SqlDbType.NVarChar, Type),
        Parameter("@VoucherType", SqlDbType.NVarChar, VoucherType),
        Parameter("@VoucherDate", SqlDbType.Date, VoucherDate),
        Parameter("@Description", SqlDbType.NText, Description),
        Parameter("@income", SqlDbType.Float, income),
        Parameter("@Export", SqlDbType.Float, Export),
      Parameter("@Remark", SqlDbType.NVarChar, Remark),
        Parameter("@Debit", SqlDbType.Float, Debit),
        Parameter("@Credit", SqlDbType.Float, Credit));
    }


    //Details ID
    public DataTable First_Balence_trans(String Type,string ISCusSupp, string AccountID)
    {
        return ExecteRader("Select *  from TreasuryMovement Where Type=@Type and ISCusSupp=@ISCusSupp and AccountID=@AccountID", CommandType.Text,
        Parameter("@Type", SqlDbType.NVarChar, Type),
        Parameter("@ISCusSupp", SqlDbType.NVarChar, ISCusSupp),
        Parameter("@AccountID", SqlDbType.NVarChar, AccountID));
    }


    
    public void DeleteTreasuryMovement(string ID)
    {
        Execute_SQL("Delete  From TreasuryMovement where ID=@ID", CommandType.Text,
        Parameter("@ID", SqlDbType.NVarChar, ID));
    }


    //Details ID
    public DataTable Details_TreasuryMovement_Return(String VoucherID, String Type)
    {
        return ExecteRader("Select * from TreasuryMovement Where VoucherID=@VoucherID and Type=@Type", CommandType.Text,
        Parameter("@VoucherID", SqlDbType.NVarChar, VoucherID),
        Parameter("@Type", SqlDbType.NVarChar, Type));
    }



    public DataTable Balence_ISCusSupp( string AccountID, string ISCusSupp)
    {
        string sql = "Select ISNULL( sum(Debit),0) as Debit,ISNULL( sum(Credit),0) AS Credit, isnull (sum(Debit)-sum(Credit),0) AS balence from TreasuryMovement where   AccountID=@AccountID and ISCusSupp=@ISCusSupp";

        return ExecteRader(sql, CommandType.Text,
                         Parameter("@AccountID", SqlDbType.NVarChar, AccountID),
              Parameter("@ISCusSupp", SqlDbType.NVarChar, ISCusSupp));
    }

    public DataTable Balence_ISCusSuppBay(string AccountID,String ISCusSupp)
    {
        string sql = "Select  isnull (sum(Credit)-sum( Debit),0) AS balence from TreasuryMovement where   AccountID=@AccountID and ISCusSupp=@ISCusSupp";

        return ExecteRader(sql, CommandType.Text,
                         Parameter("@AccountID", SqlDbType.NVarChar, AccountID),
              Parameter("@ISCusSupp", SqlDbType.NVarChar, ISCusSupp));
    }

    //Details ID
    public DataTable ReportSearch_TreasuryMovement(DateTime VoucherDate, DateTime VoucherDate2)
    {
        return ExecteRader(@"SELECT        dbo.TreasuryMovement.ID, dbo.TreasuryMovement.VoucherID, dbo.TreasuryMovement.Type, dbo.TreasuryMovement.VoucherType, dbo.TreasuryMovement.VoucherDate, dbo.TreasuryMovement.Description, 
                         dbo.TreasuryMovement.income, dbo.TreasuryMovement.Export, dbo.TreasuryMovement.UserAdd, dbo.UserPermissions.EmpName
FROM            dbo.UserPermissions INNER JOIN
                         dbo.TreasuryMovement ON dbo.UserPermissions.ID = dbo.TreasuryMovement.UserAdd Where VoucherDate>= @VoucherDate And VoucherDate<= @VoucherDate2", CommandType.Text,
        Parameter("@VoucherDate", SqlDbType.Date, VoucherDate),
                         Parameter("@VoucherDate2", SqlDbType.Date, VoucherDate2));
    }



    //Details ID
    public DataTable ReportSearch_TreasuryMovement(DateTime VoucherDate, DateTime VoucherDate2,string UserAdd)
    {
        return ExecteRader(@"SELECT        dbo.TreasuryMovement.ID, dbo.TreasuryMovement.VoucherID, dbo.TreasuryMovement.Type, dbo.TreasuryMovement.VoucherType, dbo.TreasuryMovement.VoucherDate, dbo.TreasuryMovement.Description, 
                         dbo.TreasuryMovement.income, dbo.TreasuryMovement.Export, dbo.TreasuryMovement.UserAdd, dbo.UserPermissions.EmpName
FROM            dbo.UserPermissions INNER JOIN
                         dbo.TreasuryMovement ON dbo.UserPermissions.ID = dbo.TreasuryMovement.UserAdd Where VoucherDate>= @VoucherDate And VoucherDate<= @VoucherDate2 and dbo.TreasuryMovement.UserAdd=@UserAdd", CommandType.Text,
        Parameter("@VoucherDate", SqlDbType.Date, VoucherDate),
            Parameter("@VoucherDate2", SqlDbType.Date, VoucherDate2),
                         Parameter("@UserAdd", SqlDbType.NVarChar, UserAdd));
    }







}
