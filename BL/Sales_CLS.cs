﻿using System;
using System.Data;

class Sales_CLS : DataAccessLayer
{

    //MaxID
    public String MaxID_Sales_Main()
    {
        return Execute_SQL("select ISNULL (MAX(MainID)+1,1) from Sales_Main", CommandType.Text);
    }


    



    // Insert
    public void InsertSales_Main(string MainID, DateTime MyDate, string CustomerID, string Note, string TypeKind, Double TotalInvoice, Double Discount, Double NetInvoice, Double PaidInvoice, Double RestInvoise, String ReturnInvoise, string TotalBayPrice, string ReturnBayPrice, string TotalProfits, string TransID, string TreasuryID, double Vat, double VatValue, string Extra, int UserAdd)
    {
        Execute_SQL("insert into Sales_Main(MainID ,MyDate ,CustomerID ,Note ,TypeKind ,TotalInvoice ,Discount ,NetInvoice ,PaidInvoice ,RestInvoise ,ReturnInvoise,PaidReturnInvoice,TotalBayPrice,ReturnBayPrice,TotalProfits ,TransID ,TreasuryID,Vat,VatValue,Extra ,UserAdd )Values (@MainID ,@MyDate ,@CustomerID ,@Note ,@TypeKind ,@TotalInvoice ,@Discount ,@NetInvoice ,@PaidInvoice ,@RestInvoise ,@ReturnInvoise,@PaidReturnInvoice,@TotalBayPrice,@ReturnBayPrice,@TotalProfits ,@TransID ,@TreasuryID,@Vat,@VatValue,@Extra ,@UserAdd )", CommandType.Text,
        Parameter("@MainID", SqlDbType.BigInt, MainID),
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@CustomerID", SqlDbType.Int, CustomerID),
        Parameter("@Note", SqlDbType.NVarChar, Note),
        Parameter("@TypeKind", SqlDbType.NVarChar, TypeKind),
        Parameter("@TotalInvoice", SqlDbType.Float, TotalInvoice),
        Parameter("@Discount", SqlDbType.Float, Discount),
        Parameter("@NetInvoice", SqlDbType.Float, NetInvoice),
        Parameter("@PaidInvoice", SqlDbType.Float, PaidInvoice),
        Parameter("@RestInvoise", SqlDbType.Float, RestInvoise),
        Parameter("@ReturnInvoise", SqlDbType.Float, ReturnInvoise),
          Parameter("@PaidReturnInvoice", SqlDbType.Float, 0),
        Parameter("@TotalBayPrice", SqlDbType.Float, TotalBayPrice),
        Parameter("@ReturnBayPrice", SqlDbType.Float, ReturnBayPrice),
        Parameter("@TotalProfits", SqlDbType.Float, TotalProfits),
        Parameter("@TransID", SqlDbType.NVarChar, TransID),
        Parameter("@TreasuryID", SqlDbType.NVarChar, TreasuryID),

                Parameter("@Vat", SqlDbType.Float, Vat),
        Parameter("@VatValue", SqlDbType.Float, VatValue),
           Parameter("@Extra", SqlDbType.Float, Extra),
        Parameter("@UserAdd", SqlDbType.Int, UserAdd));
    }



    //Update
    public void UpdateSales_Main(string MainID, DateTime MyDate, string CustomerID, string Note, string TypeKind, Double TotalInvoice, Double Discount, Double NetInvoice, Double PaidInvoice, Double RestInvoise, string TotalBayPrice, string TotalProfits, String TreasuryID, double Vat, double VatValue, string Extra)
    {
        Execute_SQL("Update Sales_Main Set MainID=@MainID ,MyDate=@MyDate ,CustomerID=@CustomerID ,Note=@Note ,TypeKind=@TypeKind ,TotalInvoice=@TotalInvoice ,Discount=@Discount ,NetInvoice=@NetInvoice ,PaidInvoice=@PaidInvoice ,RestInvoise=@RestInvoise,TotalBayPrice=@TotalBayPrice,TotalProfits=@TotalProfits,TreasuryID=@TreasuryID,Vat=@Vat,VatValue=@VatValue,Extra=@Extra where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.BigInt, MainID),
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@CustomerID", SqlDbType.Int, CustomerID),
        Parameter("@Note", SqlDbType.NVarChar, Note),
        Parameter("@TypeKind", SqlDbType.NVarChar, TypeKind),
        Parameter("@TotalInvoice", SqlDbType.Float, TotalInvoice),
        Parameter("@Discount", SqlDbType.Float, Discount),
        Parameter("@NetInvoice", SqlDbType.Float, NetInvoice),
        Parameter("@PaidInvoice", SqlDbType.Float, PaidInvoice),
        Parameter("@RestInvoise", SqlDbType.Float, RestInvoise),
          Parameter("@TotalBayPrice", SqlDbType.Float, TotalBayPrice),
        Parameter("@TotalProfits", SqlDbType.Float, TotalProfits),
        Parameter("@TreasuryID", SqlDbType.NVarChar, TreasuryID),
        Parameter("@Vat", SqlDbType.Float, Vat),
        Parameter("@VatValue", SqlDbType.Float, VatValue),
             Parameter("@Extra", SqlDbType.Float, Extra));
    }



    //Update Return
    public void UpdateSales_Main(string MainID, string ReturnInvoise, string PaidReturnInvoice, string ReturnBayPrice, string TotalProfits)
    {
        Execute_SQL("Update Sales_Main Set MainID=@MainID ,ReturnInvoise=@ReturnInvoise,PaidReturnInvoice=@PaidReturnInvoice,ReturnBayPrice=@ReturnBayPrice ,TotalProfits=@TotalProfits  where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.BigInt, MainID),
        Parameter("@ReturnInvoise", SqlDbType.Float, ReturnInvoise),
         Parameter("@ReturnBayPrice", SqlDbType.Float, ReturnBayPrice),
           Parameter("@PaidReturnInvoice", SqlDbType.Float, PaidReturnInvoice),
         Parameter("@TotalProfits", SqlDbType.Float, TotalProfits));
    }






    //Delete
    public void Delete_BillStoreMain(string MainID)
    {
        Execute_SQL("delete from Sales_Main where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.NVarChar, MainID));
    }

    //Details ID
    public DataTable Details_Sales_Main(string MainID)
    {
        return ExecteRader(@"SELECT dbo.Sales_Main.*, dbo.Customers.CustomerName, dbo.Customers.Phone FROM  dbo.Customers INNER JOIN
                         dbo.Sales_Main ON dbo.Customers.CustomerID = dbo.Sales_Main.CustomerID where dbo.Sales_Main.MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.NVarChar, MainID));
    }


    public DataTable Details_Sales_MainChangeQuentity(string MainID)
    {
        return ExecteRader("SELECT  * from Sales_Main  where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.NVarChar, MainID));
    }

    //=================================================================================================================================


    //MaxID
    private String MaxID_Sales_Details()
    {
        return Execute_SQL("select ISNULL (MAX(ID)+1,1) from Sales_Details", CommandType.Text);
    }

    // Insert
    public void InsertSales_Details(string MainID, string ProdecutID, string Unit, string UnitFactor, string UnitOperating, string Quantity, string ReturnQuantity, string BayPrice, string TotalBayPrice, string Price, string Vat, string MainVat, string TotalPrice, string ReturnTotalPrice, string StoreID, Boolean Recived, string CurrencyID, string CurrencyRate, string CurrencyPrice, String CurrencyVat, string CurrencyTotal)
    {
        Execute_SQL("insert into Sales_Details(ID ,MainID ,ProdecutID ,Unit ,UnitFactor ,UnitOperating ,Quantity ,ReturnQuantity ,BayPrice,TotalBayPrice ,Price ,Vat,MainVat,ReturnVat ,TotalPrice ,ReturnTotalPrice,ReturnCurrencyVat,ReturnCurrencyTotal,StoreID ,Recived ,CurrencyID ,CurrencyRate ,CurrencyPrice,CurrencyVat ,CurrencyTotal )Values (@ID ,@MainID ,@ProdecutID ,@Unit ,@UnitFactor ,@UnitOperating ,@Quantity ,@ReturnQuantity ,@BayPrice,@TotalBayPrice ,@Price ,@Vat,@MainVat,@ReturnVat ,@TotalPrice ,@ReturnTotalPrice ,@ReturnCurrencyVat,@ReturnCurrencyTotal,@StoreID ,@Recived  ,@CurrencyID ,@CurrencyRate ,@CurrencyPrice,@CurrencyVat ,@CurrencyTotal)", CommandType.Text,
        Parameter("@ID", SqlDbType.NVarChar, MaxID_Sales_Details()),
        Parameter("@MainID", SqlDbType.NVarChar, MainID),
        Parameter("@ProdecutID", SqlDbType.NVarChar, ProdecutID),
        Parameter("@Unit", SqlDbType.NVarChar, Unit),
        Parameter("@UnitFactor", SqlDbType.NVarChar, UnitFactor),
        Parameter("@UnitOperating", SqlDbType.NVarChar, UnitOperating),
        Parameter("@Quantity", SqlDbType.NVarChar, Quantity),
        Parameter("@ReturnQuantity", SqlDbType.NVarChar, ReturnQuantity),
        Parameter("@BayPrice", SqlDbType.NVarChar, BayPrice),
        Parameter("@TotalBayPrice", SqlDbType.NVarChar, TotalBayPrice),
        Parameter("@Price", SqlDbType.NVarChar, Price),
        Parameter("@Vat", SqlDbType.NVarChar, Vat),
        Parameter("@MainVat", SqlDbType.NVarChar, MainVat),
         Parameter("@ReturnVat", SqlDbType.NVarChar, 0),
        Parameter("@TotalPrice", SqlDbType.NVarChar, TotalPrice),
        Parameter("@ReturnTotalPrice", SqlDbType.NVarChar, ReturnTotalPrice),
         Parameter("@ReturnCurrencyVat", SqlDbType.NVarChar, 0),
          Parameter("@ReturnCurrencyTotal", SqlDbType.NVarChar, 0),
         
        Parameter("@StoreID", SqlDbType.NVarChar, StoreID),
        Parameter("@Recived", SqlDbType.Bit, Recived),
                Parameter("@CurrencyID", SqlDbType.Int, CurrencyID),
        Parameter("@CurrencyRate", SqlDbType.Float, CurrencyRate),
        Parameter("@CurrencyPrice", SqlDbType.Float, CurrencyPrice),
          Parameter("@CurrencyVat", SqlDbType.Float, CurrencyVat),
        Parameter("@CurrencyTotal", SqlDbType.Float, CurrencyTotal));
    }



    //Update
    public void UpdateSales_Details(string ID, Double ReturnQuantity, Double ReturnTotalPrice,double TotalBayPrice)
    {
        Execute_SQL("Update Sales_Details Set ID=@ID ,ReturnQuantity=@ReturnQuantity ,ReturnTotalPrice=@ReturnTotalPrice,TotalBayPrice=@TotalBayPrice  where ID=@ID", CommandType.Text,
        Parameter("@ID", SqlDbType.NVarChar, ID),
        Parameter("@ReturnQuantity", SqlDbType.Float, ReturnQuantity),
        Parameter("@ReturnTotalPrice", SqlDbType.Float, ReturnTotalPrice),
        Parameter("@TotalBayPrice", SqlDbType.Float, TotalBayPrice));
    }

    //Update
    public void Update_SalesReturn_Details(string ID, string ReturnQuantity, string ReturnVat, string ReturnTotalPrice,string ReturnCurrencyVat,string ReturnCurrencyTotal, string TotalBayPrice)
    {
        Execute_SQL("Update Sales_Details Set ReturnQuantity=@ReturnQuantity ,ReturnVat=@ReturnVat ,ReturnTotalPrice=@ReturnTotalPrice,ReturnCurrencyVat=@ReturnCurrencyVat,ReturnCurrencyTotal=@ReturnCurrencyTotal,TotalBayPrice=@TotalBayPrice  where ID=@ID", CommandType.Text,
        Parameter("@ID", SqlDbType.BigInt, ID),
        Parameter("@ReturnQuantity", SqlDbType.NVarChar, ReturnQuantity),
        Parameter("@ReturnVat", SqlDbType.NVarChar, ReturnVat),
        Parameter("@ReturnTotalPrice", SqlDbType.NVarChar, ReturnTotalPrice),
         Parameter("@ReturnCurrencyVat", SqlDbType.NVarChar, ReturnCurrencyVat),
              Parameter("@ReturnCurrencyTotal", SqlDbType.NVarChar, ReturnCurrencyTotal),
           Parameter("@TotalBayPrice", SqlDbType.Float, TotalBayPrice));
    }


    //Delete
    public void Delete_Sales_Details(string MainID)
    {
        Execute_SQL("delete from Sales_Details where MainID=@MainID", CommandType.Text,
        Parameter("@MainID", SqlDbType.NVarChar, MainID));
    }
    //Delete Table  Bill Details where ID 
    public void DeleteSales_Details_UseID(string ID)
    {
        Execute_SQL("Delete  From Sales_Details where ID=@ID", CommandType.Text,
        Parameter("@ID", SqlDbType.NVarChar, ID));
    }


    //Details ID

    public DataTable Details_Sales_Details(String MainID)
    {

        string str = @"SELECT        dbo.Sales_Details.*, dbo.Prodecuts.ProdecutName, dbo.Stores.StoreName, dbo.Currency.CurrencyName
FROM            dbo.Prodecuts INNER JOIN
                         dbo.Sales_Details ON dbo.Prodecuts.ProdecutID = dbo.Sales_Details.ProdecutID INNER JOIN
                         dbo.Stores ON dbo.Sales_Details.StoreID = dbo.Stores.StoreID INNER JOIN
                         dbo.Currency ON dbo.Sales_Details.CurrencyID = dbo.Currency.CurrencyID
where  dbo.Sales_Details.MainID=@MainID";
        return ExecteRader(str, CommandType.Text,
            Parameter("@MainID", SqlDbType.NVarChar, MainID));
    }

    //Details ID

    public DataTable Details_Sales_DetailsRecived(Boolean Recived)
    {

        string str = @"SELECT        dbo.Sales_Details.*, dbo.Prodecuts.ProdecutName, dbo.Stores.StoreName, dbo.Currency.CurrencyName
FROM            dbo.Prodecuts INNER JOIN
                         dbo.Sales_Details ON dbo.Prodecuts.ProdecutID = dbo.Sales_Details.ProdecutID INNER JOIN
                         dbo.Stores ON dbo.Sales_Details.StoreID = dbo.Stores.StoreID INNER JOIN
                         dbo.Currency ON dbo.Sales_Details.CurrencyID = dbo.Currency.CurrencyID
where  dbo.Sales_Details.Recived=@Recived";
        return ExecteRader(str, CommandType.Text,
            Parameter("@Recived", SqlDbType.Bit, Recived));
    }



    //========================================================================================================================

    //Report1

    public DataTable Report1_BillStoreDetails(DateTime MyDate, DateTime MyDate2, string StoreID)
    {
        return ExecteRader("BillStoreDetails_Report1", CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2),
                Parameter("@StoreID", SqlDbType.NVarChar, StoreID));
    }
    //Report2

    public DataTable Report1_BillStoreDetails(DateTime MyDate, DateTime MyDate2, string StoreID, string ProdecutID)
    {
        return ExecteRader("BillStoreDetails_Report2", CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2),
        Parameter("@StoreID", SqlDbType.NVarChar, StoreID),
        Parameter("@ProdecutID", SqlDbType.NVarChar, ProdecutID));
    }



    //Report2

    public DataTable Report1_BillStoreDetails(string StoreID, string CategoryID)
    {
        return ExecteRader(@"SELECT        dbo.Sales_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Prodecuts.CategoryID, dbo.Prodecuts.RequestLimit, dbo.Prodecuts.FiestUnit, dbo.Prodecuts.FiestUnitOperating, dbo.Prodecuts.FiestUnitFactor, 
                         dbo.Prodecuts.SecoundUnit, dbo.Prodecuts.SecoundUnitOperating, dbo.Prodecuts.SecoundUnitFactor, dbo.Prodecuts.ThreeUnit, dbo.Prodecuts.ThreeUnitOperating, dbo.Prodecuts.ThreeUnitFactor, 
                         SUM(CASE Sales_Details.SumType WHEN 1 THEN (Sales_Details.Quantity * Sales_Details.UnitFactor) END) AS Import, SUM(CASE Sales_Details.SumType WHEN 0 THEN (Sales_Details.Quantity * Sales_Details.UnitFactor) END) 
                         AS Export, SUM(CASE Sales_Details.SumType WHEN 1 THEN (Sales_Details.Quantity * Sales_Details.UnitFactor) END) - SUM(CASE Sales_Details.SumType WHEN 0 THEN (Sales_Details.Quantity * Sales_Details.UnitFactor) END) 
                         AS Balense
FROM            dbo.Sales_Details INNER JOIN
                         dbo.Prodecuts ON dbo.Sales_Details.ProdecutID = dbo.Prodecuts.ProdecutID
WHERE        (dbo.Sales_Details.StoreID =@StoreID)
GROUP BY dbo.Sales_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Prodecuts.CategoryID, dbo.Prodecuts.RequestLimit, dbo.Prodecuts.FiestUnit, dbo.Prodecuts.FiestUnitOperating, dbo.Prodecuts.FiestUnitFactor, 
                         dbo.Prodecuts.SecoundUnit, dbo.Prodecuts.SecoundUnitOperating, dbo.Prodecuts.SecoundUnitFactor, dbo.Prodecuts.ThreeUnit, dbo.Prodecuts.ThreeUnitOperating, dbo.Prodecuts.ThreeUnitFactor", CommandType.Text,
        Parameter("@StoreID", SqlDbType.NVarChar, StoreID),
        Parameter("@CategoryID", SqlDbType.NVarChar, CategoryID));
    }

    public DataTable LoadAllSales_Main(string BillTypeID)
    {
        string sql = "SELECT * FROM  Sales_Main WHERE BillTypeID=@BillTypeID";
        return ExecteRader(sql, CommandType.Text,
        Parameter("@BillTypeID", SqlDbType.NVarChar, BillTypeID));
    }

    //=====================================================================================
    // Update 
    public void UpdateSales_Details(string ID, string Quantity, string Price, string Vat, string MainVat, string TotalPrice, string TotalBayPrice, string CurrencyID, string CurrencyRate, string CurrencyPrice, String CurrencyVat, string CurrencyTotal)
    {
        Execute_SQL("Update Sales_Details Set ID=@ID ,Quantity=@Quantity,Price=@Price ,Vat=@Vat,MainVat=@MainVat ,TotalPrice=@TotalPrice ,TotalBayPrice=@TotalBayPrice,CurrencyID=@CurrencyID ,CurrencyRate=@CurrencyRate ,CurrencyPrice=@CurrencyPrice ,CurrencyVat=@CurrencyVat ,CurrencyTotal=@CurrencyTotal  where ID=@ID", CommandType.Text,
        Parameter("@ID", SqlDbType.BigInt, ID),
        Parameter("@Quantity", SqlDbType.Float, Quantity),
        Parameter("@Price", SqlDbType.Float, Price),
        Parameter("@Vat", SqlDbType.Float, Vat),
         Parameter("@MainVat", SqlDbType.Float, MainVat),
        Parameter("@TotalPrice", SqlDbType.Float, TotalPrice),
        Parameter("@TotalBayPrice", SqlDbType.Float, TotalBayPrice),
        Parameter("@CurrencyID", SqlDbType.Int, CurrencyID),
        Parameter("@CurrencyRate", SqlDbType.Float, CurrencyRate),
        Parameter("@CurrencyPrice", SqlDbType.Float, CurrencyPrice),
          Parameter("@CurrencyVat", SqlDbType.Float, CurrencyVat),
        Parameter("@CurrencyTotal", SqlDbType.Float, CurrencyTotal));
    }

    // 
    //Update Recived
    public void UpdateSales_Details(string ID, string MainID, Boolean Recived)
    {
        Execute_SQL("Update Sales_Details Set ID=@ID ,MainID=@MainID ,Recived=@Recived  where ID=@ID", CommandType.Text,
        Parameter("@ID", SqlDbType.NVarChar, ID),
        Parameter("@MainID", SqlDbType.NVarChar, MainID),
        Parameter("@Recived", SqlDbType.Bit, Recived));
    }


    

    //==================== Return=========================================================================================================================
    public DataTable LoadBillSalesWhereCustomerID(DateTime MyDate,DateTime MyDate2,string CustomerID)
    {
        string sql = @"SELECT        dbo.Sales_Main.MyDate, dbo.Sales_Main.TypeKind, dbo.Sales_Main.CustomerID, dbo.Customers.CustomerName, dbo.Customers.Phone, dbo.Sales_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Sales_Details.Unit, 
                         dbo.Sales_Details.Quantity, dbo.Sales_Details.ReturnQuantity, dbo.Sales_Details.Price, dbo.Sales_Details.Vat, dbo.Sales_Details.TotalPrice, dbo.Stores.StoreName, dbo.Sales_Main.MainID, dbo.Sales_Details.ID, 
                         dbo.Sales_Details.ReturnTotalPrice, dbo.Sales_Details.UnitFactor, dbo.Sales_Details.UnitOperating, dbo.Sales_Details.BayPrice, dbo.Sales_Details.StoreID, dbo.Sales_Details.Recived, dbo.Sales_Details.TotalBayPrice, 
                         dbo.Sales_Main.Vat AS MainVat
FROM            dbo.Sales_Details INNER JOIN
                         dbo.Sales_Main ON dbo.Sales_Details.MainID = dbo.Sales_Main.MainID INNER JOIN
                         dbo.Customers ON dbo.Sales_Main.CustomerID = dbo.Customers.CustomerID INNER JOIN
                         dbo.Prodecuts ON dbo.Sales_Details.ProdecutID = dbo.Prodecuts.ProdecutID INNER JOIN
                         dbo.Stores ON dbo.Sales_Details.StoreID = dbo.Stores.StoreID
WHERE dbo.Sales_Main.MyDate>= @MyDate And dbo.Sales_Main.MyDate<= @MyDate2 and dbo.Sales_Main.CustomerID=@CustomerID";
        return ExecteRader(sql, CommandType.Text,
           Parameter("@MyDate", SqlDbType.Date, MyDate),
            Parameter("@MyDate2", SqlDbType.Date, MyDate2),
            Parameter("@CustomerID", SqlDbType.NVarChar, CustomerID));
    }


    public DataTable LoadBillSalesWhereBillNumber(DateTime MyDate, DateTime MyDate2, string CustomerID,string Search)
    {
        string sql = @"SELECT        dbo.Sales_Main.MyDate, dbo.Sales_Main.TypeKind, dbo.Sales_Main.CustomerID, dbo.Customers.CustomerName, dbo.Customers.Phone, dbo.Sales_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Sales_Details.Unit, 
                         dbo.Sales_Details.Quantity, dbo.Sales_Details.ReturnQuantity, dbo.Sales_Details.Price, dbo.Sales_Details.Vat, dbo.Sales_Details.TotalPrice, dbo.Stores.StoreName, dbo.Sales_Main.MainID, dbo.Sales_Details.ID, 
                         dbo.Sales_Details.ReturnTotalPrice, dbo.Sales_Details.UnitFactor, dbo.Sales_Details.UnitOperating, dbo.Sales_Details.BayPrice, dbo.Sales_Details.StoreID, dbo.Sales_Details.Recived, dbo.Sales_Details.TotalBayPrice, 
                         dbo.Sales_Main.Vat AS MainVat
FROM            dbo.Sales_Details INNER JOIN
                         dbo.Sales_Main ON dbo.Sales_Details.MainID = dbo.Sales_Main.MainID INNER JOIN
                         dbo.Customers ON dbo.Sales_Main.CustomerID = dbo.Customers.CustomerID INNER JOIN
                         dbo.Prodecuts ON dbo.Sales_Details.ProdecutID = dbo.Prodecuts.ProdecutID INNER JOIN
                         dbo.Stores ON dbo.Sales_Details.StoreID = dbo.Stores.StoreID
WHERE dbo.Sales_Main.MyDate>= @MyDate And dbo.Sales_Main.MyDate<= @MyDate2 and dbo.Sales_Main.CustomerID=@CustomerID and convert(nvarchar,dbo.Sales_Main.MainID)like @Search";
        return ExecteRader(sql, CommandType.Text,
           Parameter("@MyDate", SqlDbType.Date, MyDate),
            Parameter("@MyDate2", SqlDbType.Date, MyDate2),
            Parameter("@CustomerID", SqlDbType.NVarChar, CustomerID),
                 Parameter("@Search", SqlDbType.NVarChar, Search));
    }

    public DataTable LoadBillSalesWhereProdect(DateTime MyDate, DateTime MyDate2, string CustomerID, string Search)
    {
        string sql = @"SELECT        dbo.Sales_Main.MyDate, dbo.Sales_Main.TypeKind, dbo.Sales_Main.CustomerID, dbo.Customers.CustomerName, dbo.Customers.Phone, dbo.Sales_Details.ProdecutID, dbo.Prodecuts.ProdecutName, dbo.Sales_Details.Unit, 
                         dbo.Sales_Details.Quantity, dbo.Sales_Details.ReturnQuantity, dbo.Sales_Details.Price, dbo.Sales_Details.Vat, dbo.Sales_Details.TotalPrice, dbo.Stores.StoreName, dbo.Sales_Main.MainID, dbo.Sales_Details.ID, 
                         dbo.Sales_Details.ReturnTotalPrice, dbo.Sales_Details.UnitFactor, dbo.Sales_Details.UnitOperating, dbo.Sales_Details.BayPrice, dbo.Sales_Details.StoreID, dbo.Sales_Details.Recived, dbo.Sales_Details.TotalBayPrice, 
                         dbo.Sales_Main.Vat AS MainVat
FROM            dbo.Sales_Details INNER JOIN
                         dbo.Sales_Main ON dbo.Sales_Details.MainID = dbo.Sales_Main.MainID INNER JOIN
                         dbo.Customers ON dbo.Sales_Main.CustomerID = dbo.Customers.CustomerID INNER JOIN
                         dbo.Prodecuts ON dbo.Sales_Details.ProdecutID = dbo.Prodecuts.ProdecutID INNER JOIN
                         dbo.Stores ON dbo.Sales_Details.StoreID = dbo.Stores.StoreID
WHERE dbo.Sales_Main.MyDate>= @MyDate And dbo.Sales_Main.MyDate<= @MyDate2 and dbo.Sales_Main.CustomerID=@CustomerID and dbo.Prodecuts.ProdecutName+dbo.Prodecuts.FiestUnitBarcode+dbo.Prodecuts.SecoundUnitBarcode+dbo.Prodecuts.ThreeUnitBarcode like  '%'+@Search+ '%' ";
        return ExecteRader(sql, CommandType.Text,
           Parameter("@MyDate", SqlDbType.Date, MyDate),
            Parameter("@MyDate2", SqlDbType.Date, MyDate2),
            Parameter("@CustomerID", SqlDbType.NVarChar, CustomerID),
                 Parameter("@Search", SqlDbType.NVarChar, Search));
    }

    public DataTable BillSalesCustomer(string CustomerID)
    {
        string sql = @"SELECT  MainID, CustomerID FROM  Sales_Main WHERE CustomerID =@CustomerID";
        return ExecteRader(sql, CommandType.Text,
           Parameter("@CustomerID", SqlDbType.NVarChar, CustomerID));
    }


    public DataTable BillChangeQuantity()
    {
        string sql = "SELECT  MainID FROM  Sales_Main ";
        return ExecteRader(sql, CommandType.Text);
    }


    //===Search==============================================================================================

    public DataTable Load_Sales_Main(DateTime MyDate, DateTime MyDate2, int UserAdd)
    {
        string str = @"SELECT        dbo.Sales_Main.MainID, dbo.Sales_Main.MyDate, dbo.Sales_Main.CustomerID, dbo.Customers.CustomerName, dbo.Sales_Main.Note, dbo.Sales_Main.TypeKind, dbo.Sales_Main.TotalInvoice, dbo.Sales_Main.Discount, 
                         dbo.Sales_Main.NetInvoice, dbo.Sales_Main.PaidInvoice, dbo.Sales_Main.RestInvoise, dbo.Sales_Main.UserAdd, dbo.UserPermissions.EmpName, dbo.Sales_Main.ReturnInvoise, dbo.Customers.Phone, 
                         dbo.Sales_Main.TotalBayPrice, dbo.Sales_Main.ReturnBayPrice, dbo.Sales_Main.TotalProfits, dbo.Sales_Main.TransID, dbo.Sales_Main.TreasuryID, dbo.Sales_Main.Vat, dbo.Sales_Main.VatValue
FROM            dbo.Sales_Main INNER JOIN
                         dbo.Customers ON dbo.Sales_Main.CustomerID = dbo.Customers.CustomerID INNER JOIN
                         dbo.UserPermissions ON dbo.Sales_Main.UserAdd = dbo.UserPermissions.ID
WHERE        (dbo.Sales_Main.MyDate >=@MyDate ) AND (dbo.Sales_Main.MyDate <=@MyDate2) AND (dbo.Sales_Main.UserAdd =@UserAdd)";
        return ExecteRader(str, CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2),
        Parameter("@UserAdd", SqlDbType.Int, UserAdd));

    }

    public DataTable Load_Sales_Main(DateTime MyDate, DateTime MyDate2)
    {
        string str = @"SELECT        dbo.Sales_Main.MainID, dbo.Sales_Main.MyDate, dbo.Sales_Main.CustomerID, dbo.Customers.CustomerName, dbo.Sales_Main.Note, dbo.Sales_Main.TypeKind, dbo.Sales_Main.TotalInvoice, dbo.Sales_Main.Discount, 
                         dbo.Sales_Main.NetInvoice, dbo.Sales_Main.PaidInvoice, dbo.Sales_Main.RestInvoise, dbo.Sales_Main.UserAdd, dbo.UserPermissions.EmpName, dbo.Sales_Main.ReturnInvoise, dbo.Customers.Phone, 
                         dbo.Sales_Main.TotalBayPrice, dbo.Sales_Main.ReturnBayPrice, dbo.Sales_Main.TotalProfits, dbo.Sales_Main.TransID, dbo.Sales_Main.TreasuryID, dbo.Sales_Main.Vat, dbo.Sales_Main.VatValue
FROM            dbo.Sales_Main INNER JOIN
                         dbo.Customers ON dbo.Sales_Main.CustomerID = dbo.Customers.CustomerID INNER JOIN
                         dbo.UserPermissions ON dbo.Sales_Main.UserAdd = dbo.UserPermissions.ID
WHERE        (dbo.Sales_Main.MyDate >=@MyDate ) AND (dbo.Sales_Main.MyDate <=@MyDate2)";
        return ExecteRader(str, CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2));

    }
    public DataTable Search_Sales_Main(DateTime MyDate, DateTime MyDate2, int UserAdd, string Search)
    {
        string str = @"SELECT        dbo.Sales_Main.MainID, dbo.Sales_Main.MyDate, dbo.Sales_Main.CustomerID, dbo.Customers.CustomerName, dbo.Sales_Main.Note, dbo.Sales_Main.TypeKind, dbo.Sales_Main.TotalInvoice, 
                         dbo.Sales_Main.Discount, dbo.Sales_Main.NetInvoice, dbo.Sales_Main.PaidInvoice, dbo.Sales_Main.RestInvoise, dbo.Sales_Main.UserAdd, dbo.UserPermissions.EmpName, dbo.Sales_Main.ReturnInvoise
FROM            dbo.Sales_Main INNER JOIN
                         dbo.Customers ON dbo.Sales_Main.CustomerID = dbo.Customers.CustomerID INNER JOIN
                         dbo.UserPermissions ON dbo.Sales_Main.UserAdd = dbo.UserPermissions.ID
WHERE        (dbo.Sales_Main.MyDate >=@MyDate ) AND (dbo.Sales_Main.MyDate <=@MyDate2) AND (dbo.Sales_Main.UserAdd =@UserAdd) and convert(nvarchar,dbo.Sales_Main.MainID)+dbo.Customers.CustomerName like '%'+@Search+ '%'";
        return ExecteRader(str, CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2),
        Parameter("@UserAdd", SqlDbType.Int, UserAdd),
        Parameter("@Search", SqlDbType.NVarChar, Search));

    }

        public DataTable Search_Sales_Main(DateTime MyDate, DateTime MyDate2,string Search)
    {
        string str = @"SELECT        dbo.Sales_Main.MainID, dbo.Sales_Main.MyDate, dbo.Sales_Main.CustomerID, dbo.Customers.CustomerName, dbo.Sales_Main.Note, dbo.Sales_Main.TypeKind, dbo.Sales_Main.TotalInvoice, dbo.Sales_Main.Discount, 
                         dbo.Sales_Main.NetInvoice, dbo.Sales_Main.PaidInvoice, dbo.Sales_Main.RestInvoise, dbo.Sales_Main.UserAdd, dbo.UserPermissions.EmpName, dbo.Sales_Main.ReturnInvoise, dbo.Sales_Main.TotalBayPrice, 
                         dbo.Sales_Main.ReturnBayPrice, dbo.Sales_Main.TotalProfits, dbo.Sales_Main.TransID, dbo.Sales_Main.TreasuryID, dbo.Sales_Main.Vat, dbo.Sales_Main.VatValue
FROM            dbo.Sales_Main INNER JOIN
                         dbo.Customers ON dbo.Sales_Main.CustomerID = dbo.Customers.CustomerID INNER JOIN
                         dbo.UserPermissions ON dbo.Sales_Main.UserAdd = dbo.UserPermissions.ID
WHERE        (dbo.Sales_Main.MyDate >=@MyDate ) AND (dbo.Sales_Main.MyDate <=@MyDate2)  and convert(nvarchar,dbo.Sales_Main.MainID)+dbo.Customers.CustomerName like '%'+@Search+ '%'";
        return ExecteRader(str, CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
        Parameter("@MyDate2", SqlDbType.Date, MyDate2),
        Parameter("@Search", SqlDbType.NVarChar, Search));

    }






    public DataTable Search_Sales_Main()
    {
        string str = "SELECT * from  Sales_Main";
        return ExecteRader(str, CommandType.Text);

    }


    public DataTable Search_Sales_Main(int UserAdd)
    {
        string str = "SELECT * from  Sales_Main WHERE UserAdd=@UserAdd";
        return ExecteRader(str, CommandType.Text,
        Parameter("@UserAdd", SqlDbType.Int, UserAdd));

    }



}

