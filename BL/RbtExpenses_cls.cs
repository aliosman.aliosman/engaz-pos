﻿
using System;
using System.Data;

class RbtExpenses_cls : DataAccessLayer
    {
    //Details ID
    public DataTable ReportSearch_Expenses(DateTime MyDate, DateTime MyDate2)
    {
        return ExecteRader(@"SELECT        dbo.Expenses.ExpenseID, dbo.Expenses.ExpenseTypeID, dbo.ExpenseType.ExpenseName, dbo.Expenses.MyDate, dbo.Expenses.RecivedName, dbo.Expenses.ExpenseValue, dbo.Expenses.Remarks, 
                         dbo.Expenses.TreasuryID, dbo.UserPermissions.ID, dbo.Expenses.UserAdd, dbo.UserPermissions.EmpName
FROM            dbo.Expenses INNER JOIN
                         dbo.ExpenseType ON dbo.Expenses.ExpenseTypeID = dbo.ExpenseType.ExpenseID INNER JOIN
                         dbo.UserPermissions ON dbo.Expenses.UserAdd = dbo.UserPermissions.ID
   Where MyDate>= @MyDate And MyDate<= @MyDate2", CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
          Parameter("@MyDate2", SqlDbType.Date, MyDate2));
    }


    public DataTable ReportSearch_Expenses(DateTime MyDate, DateTime MyDate2,int ExpenseTypeID)
    {
        return ExecteRader(@"SELECT        dbo.Expenses.ExpenseID, dbo.Expenses.ExpenseTypeID, dbo.ExpenseType.ExpenseName, dbo.Expenses.MyDate, dbo.Expenses.RecivedName, dbo.Expenses.ExpenseValue, dbo.Expenses.Remarks, 
                         dbo.Expenses.TreasuryID, dbo.UserPermissions.ID, dbo.Expenses.UserAdd, dbo.UserPermissions.EmpName
FROM            dbo.Expenses INNER JOIN
                         dbo.ExpenseType ON dbo.Expenses.ExpenseTypeID = dbo.ExpenseType.ExpenseID INNER JOIN
                         dbo.UserPermissions ON dbo.Expenses.UserAdd = dbo.UserPermissions.ID
   Where MyDate>= @MyDate And MyDate<= @MyDate2 and dbo.Expenses.ExpenseTypeID=@ExpenseTypeID", CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
          Parameter("@MyDate2", SqlDbType.Date, MyDate2),
           Parameter("@ExpenseTypeID", SqlDbType.Int, ExpenseTypeID) );
    }




    //Details ID
    public DataTable ReportSearch_Expenses(DateTime MyDate, DateTime MyDate2, string UserAdd)
    {
        return ExecteRader(@"SELECT        dbo.Expenses.ExpenseID, dbo.Expenses.ExpenseTypeID, dbo.ExpenseType.ExpenseName, dbo.Expenses.MyDate, dbo.Expenses.RecivedName, dbo.Expenses.ExpenseValue, dbo.Expenses.Remarks, 
                         dbo.Expenses.TreasuryID, dbo.UserPermissions.ID, dbo.Expenses.UserAdd, dbo.UserPermissions.EmpName
FROM            dbo.Expenses INNER JOIN
                         dbo.ExpenseType ON dbo.Expenses.ExpenseTypeID = dbo.ExpenseType.ExpenseID INNER JOIN
                         dbo.UserPermissions ON dbo.Expenses.UserAdd = dbo.UserPermissions.ID
   Where MyDate>= @MyDate And MyDate<= @MyDate2 and   dbo.Expenses.UserAdd=@UserAdd", CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
            Parameter("@MyDate2", SqlDbType.Date, MyDate2),
                         Parameter("@UserAdd", SqlDbType.NVarChar, UserAdd));
    }




    //Details ID
    public DataTable ReportSearch_Expenses(DateTime MyDate, DateTime MyDate2,string ExpenseTypeID, string UserAdd)
    {
        return ExecteRader(@"SELECT        dbo.Expenses.ExpenseID, dbo.Expenses.ExpenseTypeID, dbo.ExpenseType.ExpenseName, dbo.Expenses.MyDate, dbo.Expenses.RecivedName, dbo.Expenses.ExpenseValue, dbo.Expenses.Remarks, 
                         dbo.Expenses.TreasuryID, dbo.UserPermissions.ID, dbo.Expenses.UserAdd, dbo.UserPermissions.EmpName
FROM            dbo.Expenses INNER JOIN
                         dbo.ExpenseType ON dbo.Expenses.ExpenseTypeID = dbo.ExpenseType.ExpenseID INNER JOIN
                         dbo.UserPermissions ON dbo.Expenses.UserAdd = dbo.UserPermissions.ID
   Where MyDate>= @MyDate And MyDate<= @MyDate2 and  dbo.Expenses.ExpenseTypeID=@ExpenseTypeID and  dbo.Expenses.UserAdd=@UserAdd", CommandType.Text,
        Parameter("@MyDate", SqlDbType.Date, MyDate),
            Parameter("@MyDate2", SqlDbType.Date, MyDate2),
               Parameter("@ExpenseTypeID", SqlDbType.Int, ExpenseTypeID),
                         Parameter("@UserAdd", SqlDbType.NVarChar, UserAdd));
    }




}

