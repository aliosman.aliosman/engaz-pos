﻿
using System.Data;

class ReturnSales_cls : DataAccessLayer
{

    public DataTable LoadBillSalesWhereCustomerID(string MainID)
    {
        string sql = @"SELECT dbo.BillMain.InvoiceID, dbo.BillMain.MyDate, dbo.BillMain.TypeKind, dbo.BillMain.AccountID, dbo.Customers.CustomerName, dbo.Customers.Phone, dbo.BillDetails.ProdecutID, dbo.Prodecuts.ProdecutName, 
                         dbo.BillDetails.Unit, dbo.BillDetails.Quantity, dbo.BillDetails.ReturnQuantity, dbo.BillDetails.BayPrice, dbo.BillDetails.Price, dbo.BillDetails.Vat, dbo.BillDetails.TotalPrice, dbo.Stores.StoreName, 
                         dbo.BillMain.BillTypeID, dbo.BillMain.MainID, dbo.BillDetails.ID, dbo.BillDetails.ReturnTotalPrice
FROM            dbo.BillDetails INNER JOIN
                         dbo.BillMain ON dbo.BillDetails.MainID = dbo.BillMain.MainID INNER JOIN
                         dbo.Customers ON dbo.BillMain.AccountID = dbo.Customers.CustomerID INNER JOIN
                         dbo.Prodecuts ON dbo.BillDetails.ProdecutID = dbo.Prodecuts.ProdecutID INNER JOIN
                         dbo.Stores ON dbo.BillDetails.StoreID = dbo.Stores.StoreID
WHERE (dbo.BillMain.MainID =@MainID)";
        return ExecteRader(sql, CommandType.Text,
           Parameter("@MainID", SqlDbType.NVarChar, MainID));
    }


    public DataTable BillSalesCustomer(int BillTypeID, string AccountID)
    {
        string sql = @"SELECT  MainID, BillTypeID, AccountID FROM  BillMain WHERE (BillTypeID=@BillTypeID) AND (AccountID =@AccountID)";
        return ExecteRader(sql, CommandType.Text,
                       Parameter("@BillTypeID", SqlDbType.Int, BillTypeID),
           Parameter("@AccountID", SqlDbType.NVarChar, AccountID));
    }



    public DataTable BillChangeQuantity(string BillTypeID)
    {
        string sql = "SELECT  MainID, BillTypeID FROM  BillMain WHERE (BillTypeID=@BillTypeID)";
        return ExecteRader(sql, CommandType.Text,
        Parameter("@BillTypeID", SqlDbType.NVarChar, BillTypeID));
    }










}

